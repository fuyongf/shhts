﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;


namespace WPFPageControlLib
{
    /// <summary>
    /// PageControl.xaml 的交互逻辑
    /// </summary>
    public partial class PageControl : UserControl
    {
        private int displayFirstPageNum=1,displayLastPageNum=1;
        private int pageCount = 0;
        private int currentPageNum = 1;//from 1 start
        public int PageCount {
            set
            {
                pageCount = value;   
                if (currentPageNum > pageCount) currentPageNum = pageCount;
                pageCountLabel.Content = "共 " + pageCount + " 页  当前第 " + currentPageNum + " 页";
                displayPagesButtons();
                if (currentPageNum < 1) currentPageNum = 1;
                GoToPageNum(currentPageNum);
            }
            get
            {
                return pageCount;
            }
        }
        public int PageIndex{
            get
            {
                return currentPageNum;
            }
            set
            {
                GoToPageNum(value);
                //通知页码更换
                if (PageNumChanged != null) { PageNumChanged(currentPageNum); }
                pageCountLabel.Content = "共 " + pageCount + " 页  当前第 " + currentPageNum + " 页";
            }
        }
        public delegate void PageNumChangedDelegate(int pageNum);

        public PageNumChangedDelegate PageNumChanged{ set; get; }

        public PageControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 根据 pageCount 初始化控件.
        /// </summary>
        private void GoToPageNum(int pageNum)
        {
            if (pageNum < 1 || pageNum > pageCount) return;
            currentPageNum = pageNum;
            if (currentPageNum < displayFirstPageNum)
            {
                displayFirstPageNum = currentPageNum;
            }
            else if (currentPageNum > displayLastPageNum)
            {
                displayFirstPageNum = currentPageNum;
                if ((displayFirstPageNum + 4) > pageCount) displayFirstPageNum = pageCount - 4;
            }
            if (displayFirstPageNum < 1) displayFirstPageNum = 1;
            //go to pageNum
            displayPagesButtons();
        }
        /// <summary>
        /// 根据pageCount  currentPage显示buttons
        /// </summary>
        private void displayPagesButtons()
        {
            ContentStackPanel.Children.Clear();
            if (pageCount < 1) return;

            Button preNumBtn = generatePageNumButton(PageNumButtonType.GoToPrePage);
            if (currentPageNum == 1) preNumBtn.IsEnabled = false;
            ContentStackPanel.Children.Add(preNumBtn);
            int num = displayFirstPageNum;
            if (displayFirstPageNum > 1)
            {
                ContentStackPanel.Children.Add(generatePageNumButton(PageNumButtonType.PrePages));
            }
            else
            {
                ContentStackPanel.Children.Add(generatePageNumButton(num++, PageNumButtonType.PageNum));
              
            }
            for (int i = 0; i < 4; i++)
                if (num <= pageCount)
                {
                    ContentStackPanel.Children.Add(generatePageNumButton(num++, PageNumButtonType.PageNum));
                 
                }
                else break;
            if (pageCount == num)
            {
                ContentStackPanel.Children.Add(generatePageNumButton(num++, PageNumButtonType.PageNum));
            }
            else if(pageCount>num)
            {
                ContentStackPanel.Children.Add(generatePageNumButton(PageNumButtonType.NextPages));
            }
            displayLastPageNum = num - 1;
            Button nextNumBtn = generatePageNumButton(PageNumButtonType.GoToNextPage);
            if (currentPageNum == pageCount) nextNumBtn.IsEnabled = false;
            ContentStackPanel.Children.Add(nextNumBtn);

            //设置选中样式
            foreach (Button btn in ContentStackPanel.Children)
            {
                if (btn.Content.ToString() == PageIndex.ToString())
                {
                    btn.Style = (Style)FindResource("Wyl_ShhtsBtBlue");
                }
                else
                {
                    btn.Style = (Style) FindResource("Wyl_ShhtsBtGray");
                }
            }
        }

        private Button generatePageNumButton( PageNumButtonType btnType)
        {
            return generatePageNumButton(0, btnType);
        }
        private Button generatePageNumButton(int pageNum,PageNumButtonType btnType)
        {
            Button btn = new Button();
           // <Button x:Name="PageNumButtonB" Content="2" Width="30" Height="30" 
            //VerticalAlignment="Center"  Margin="1" Click="PageNumButton_Click"></Button>
            btn.Width = 26;
            btn.Height = 30;
            btn.Margin = new Thickness(1);
            btn.Click += this.PageNumButton_Click;
            btn.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            btn.Tag = btnType;
            if (btnType == PageNumButtonType.PageNum)
            {
                btn.Content = "" + pageNum;
            }
            else if (btnType == PageNumButtonType.GoToPrePage)
            {//第一个按钮 <<
                btn.Content = "《";
            }
            else if (btnType == PageNumButtonType.GoToNextPage)
            {//最后一个按钮>>
                btn.Content = "》";
            }
            else if (btnType == PageNumButtonType.PrePages)
            {//向前跳转页码 ...
                btn.Content = "...";
            }
            else if (btnType == PageNumButtonType.NextPages)
            {//向后跳转页码 ...
                btn.Content = "...";
            }
            return btn;
        }
        private void PageNumButton_Click(object sender, RoutedEventArgs e)
        {
            
            Button btn = (Button)sender;
            string btnContent = btn.Content.ToString();
            PageNumButtonType btnType = (PageNumButtonType) btn.Tag;
            if (btnType==PageNumButtonType.PageNum)
            {
                PageIndex = Int32.Parse(btn.Content.ToString());
            }
            else if (btnType == PageNumButtonType.GoToPrePage)
            {//第一个按钮 <<
                PageIndex = currentPageNum - 1;
            }
            else if (btnType == PageNumButtonType.GoToNextPage)
            {//最后一个按钮>>
                PageIndex = currentPageNum + 1;
            }
            else if (btnType == PageNumButtonType.PrePages)
            {//向前跳转页码 ...
                displayFirstPageNum -= 4;
                if (displayFirstPageNum < 1) displayFirstPageNum = 1;
                displayPagesButtons();
            }
            else if (btnType == PageNumButtonType.NextPages)
            {//向后跳转页码 ...
                displayFirstPageNum += 4;
                if ((displayFirstPageNum + 4) > pageCount) displayFirstPageNum = pageCount - 4;
                displayPagesButtons();
            }
           

        }


        #region 限制输入数字
        private void textBox1_Pasting(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(String)))
            {
                String text = (String)e.DataObject.GetData(typeof(String));
                if (!isNumberic(text))
                { e.CancelCommand(); }
            }
            else { e.CancelCommand(); }
        }
        private void textBox1_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
                e.Handled = true;
        }
        private void textBox1_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!isNumberic(e.Text))
            {
                e.Handled = true;
            }
            else
                e.Handled = false;
        }
        //isDigit是否是数字
        public static bool isNumberic(string _string)
        {
            if (string.IsNullOrEmpty(_string))
                return false;
            foreach (char c in _string)
            {
                if (!char.IsDigit(c))
                    //if(c<'0' c="">'9')//最好的方法,在下面测试数据中再加一个0，然后这种方法效率会搞10毫秒左右
                    return false;
            }
            return true;
        }
        #endregion

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (string.IsNullOrEmpty(textBox1.Text)) return;
                PageIndex = Int32.Parse(textBox1.Text);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text)) return;
            PageIndex = Int32.Parse(textBox1.Text);
        }
    }

    public enum PageNumButtonType
    {
        /// <summary>
        /// 向前翻一页  <<
        /// </summary>
        GoToPrePage,
        /// <summary>
        /// 显示前面页码 ...
        /// </summary>
        PrePages,
        /// <summary>
        /// 页码按钮
        /// </summary>
        PageNum,
        /// <summary>
        /// 显示后面的页码 ...
        /// </summary>
        NextPages,
        /// <summary>
        /// 向后翻一页
        /// </summary>
        GoToNextPage

    }

    public  class PageNumOverflowException : Exception
    {
        public PageNumOverflowException():base("页码范围超出")
        {
            
        }


    }
}
