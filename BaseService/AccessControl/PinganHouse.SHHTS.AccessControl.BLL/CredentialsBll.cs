﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using PinganHouse.SHHTS.AccessControl.Common;
using PinganHouse.SHHTS.AccessControl.Common.Enum;
using PinganHouse.SHHTS.AccessControl.Entities;
using PinganHouse.SHHTS.AccessControl.Utils.Log;

namespace PinganHouse.SHHTS.AccessControl.BLL
{
    public class CredentialsBll
    {
        private const string ClassName = "CredentialsBll";


        /// <summary>
        /// 输入和输出是否是同一个域(有效性验证)
        /// </summary>
        /// <param name="inputApplications"></param>
        /// <param name="sysNo">证书系统编号</param>
        /// <returns></returns>
        public static bool Validation(long inputApplications, long sysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var credentials = Credentials.GetObject(sysNo);

                if (credentials == null) return false;

                return credentials.Applications == inputApplications;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "Validation",
                                DateTime.Now.Ticks - ts, e));

                throw;
            }
        }

        /// <summary>
        /// 获取证书列表
        /// </summary>
        /// <param name="pageSize">页显示数</param>
        /// <param name="pageIndex">显示页数</param>
        /// <param name="filter">过滤条件</param>
        /// <param name="applications">应用程序编码</param>
        /// <param name="count">总数</param>
        /// <returns>证书信息列表</returns>
        public static DataTable GetCredentialsTable(int pageSize, int pageIndex, string filter, string applications,out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var dataTable = Credentials.GetCredentialsTable(pageSize, pageIndex, filter, applications, out count);

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetResourceTable",
                                DateTime.Now.Ticks - ts, e));

                throw;
            }
        }

        /// <summary>
        /// 获取证书列表
        /// </summary>
        /// <param name="pageSize">页显示数</param>
        /// <param name="pageIndex">显示页数</param>
        /// <param name="resource">资源</param>
        /// <param name="applications">应用程序</param>
        /// <param name="count">总数</param>
        /// <param name="filter">过滤条件</param>
        /// <returns>证书信息列表</returns>
        public static DataTable GetCredentialsTableListOutResource(int pageSize, int pageIndex, string resource,string filter, string applications, out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var dataTable = Credentials.GetCredentialsTableListOutResource(pageSize, pageIndex, resource,filter, applications, out count);

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsTableListOutResource",
                                DateTime.Now.Ticks - ts, e));

                throw;
            }
        }

        /// <summary>
        /// 获取证书列表
        /// </summary>
        /// <param name="pageSize">页显示数</param>
        /// <param name="pageIndex">显示页数</param>
        /// <param name="resource">资源</param>
        /// <param name="filter">过滤</param>
        /// <param name="applications">应用程序域</param>
        /// <param name="count">总数</param>
        /// <returns>证书信息列表</returns>
        public static DataTable GetCredentialsTableListOutPrincipalPart(int pageSize, int pageIndex, string resource, string filter, string applications, out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var dataTable = Credentials.GetCredentialsTableListOutPrincipalPart(pageSize, pageIndex, resource,filter,applications,out count);

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsTableListOutResource",
                                DateTime.Now.Ticks - ts, e));

                throw;
            }
        }

        /// <summary>
        /// 查询主体持有的所有证书
        /// </summary>
        /// <param name="principalPartNo">主体编号</param>
        /// <returns></returns>
        public static List<Credentials> GetCredentialsListByPrincipalPartNo(string principalPartNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var principalPart = PrincipalPart.GetPrincipalPartByNo(principalPartNo);

                if (principalPart == null)
                    throw new Exception("object PrincipalPart is null");

                var credentialsTableInPrincipalPart = Credentials.GetCredentialsTableListInPrincipalPart(principalPartNo);

                if(credentialsTableInPrincipalPart==null || credentialsTableInPrincipalPart.Rows.Count==0)
                    throw new Exception("could not find any credentials in principalPart ");

                var credentialsSysNoList = new List<long>();

                foreach (DataRow dr in credentialsTableInPrincipalPart.Rows)
                {
                    long credentialsSysNo;
                    long.TryParse(Convert.ToString(dr["CredentialsSysNo"]), out credentialsSysNo);
                    credentialsSysNoList.Add(credentialsSysNo);
                }

                //获取主体拥有证书集合
                var credentialsList = Credentials.GetCredentialsListByCredentialsSysNoList(credentialsSysNoList);

                //返回证书集合
                return credentialsList;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsListByPrincipalPartNo",
                                DateTime.Now.Ticks - ts, e));

                throw;
            }
        }

        /// <summary>
        /// 查询资源所绑定的证书集合
        /// </summary>
        /// <param name="resourceNo">主体编号</param>
        /// <returns></returns>
        public static List<Credentials> GetCredentialsListByResourceNo(string resourceNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var resource = Resource.GetObjectByResourceNo(resourceNo);

                if (resource == null)
                    throw new Exception("object Resource is null");

                var credentialsTableInResource = Credentials.GetCredentialsTableListInResource(resourceNo);

                if (credentialsTableInResource == null || credentialsTableInResource.Rows.Count == 0)
                    throw new Exception("could not find any credentials in Resource ");

                var credentialsSysNoList = new List<long>();

                //访问规则
                var limitsrules = new Dictionary<long, LimitsRules>();

                foreach (DataRow dr in credentialsTableInResource.Rows)
                {
                    long credentialsSysNo;
                    long.TryParse(Convert.ToString(dr["CredentialsSysNo"]), out credentialsSysNo);

                    //如果证书编号不存在则继续下一个
                    if (credentialsSysNo == 0) continue;
                    credentialsSysNoList.Add(credentialsSysNo);

                    //判断Key是否存在
                    if (limitsrules.ContainsKey(credentialsSysNo)) continue;
                    //添加访问规则
                    limitsrules.Add(credentialsSysNo, (LimitsRules)dr["LimitsRules"]);
                }

                //获取主体拥有证书集合
                var credentialsList = Credentials.GetCredentialsListByCredentialsSysNoList(credentialsSysNoList);

                if (credentialsList == null || credentialsList.Count == 0) return credentialsList;

                foreach (var credentialse in credentialsList)
                {
                    credentialse.LimitsRules = limitsrules[credentialse.SysNo];
                }

                //返回证书集合
                return credentialsList;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsListByPrincipalPartNo",
                                DateTime.Now.Ticks - ts, e));

                throw;
            }
        }

        /// <summary>
        /// 获取主体和证书共同拥有的证书集合
        /// </summary>
        /// <param name="resourceNo">主体集合</param>
        /// <param name="principalPartNos">主体集合</param>
        /// <returns></returns>
        public static List<Credentials> GetCredentialsListByResourceNoAndPrincipalPartNo(string resourceNo,
            params string[] principalPartNos)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                //查询资源
                var resource = Resource.GetObjectByResourceNo(resourceNo);

                if (resource == null)
                    throw new Exception("object Resource is null");

                var principalPartSysNoList = new List<long>();

                //查询主体
                foreach (var principalPartNo in principalPartNos)
                {
                    var principalPart = PrincipalPart.GetPrincipalPartByNo(principalPartNo);
                    if (principalPart == null) continue;
                    principalPartSysNoList.Add(principalPart.SysNo);
                }

                //查询证书集合
                var credentialsTable = Credentials.GetCredentialsTableListInResourceAndPrincipalPart(resource.SysNo, principalPartSysNoList);

                if (credentialsTable == null || credentialsTable.Rows.Count == 0) return null;


                var credentialsList = new List<Credentials>();
                foreach (DataRow dr in credentialsTable.Rows)
                {
                    var credentials = new Credentials
                    {
                        SysNo = (long)dr["CredentialsSysNo"],
                        CredentialsNo = Convert.ToString(dr["CredentialsNo"]),
                        CredentialsName = Convert.ToString(dr["CredentialsName"]),
                        LimitsRules = (LimitsRules) dr["LimitsRules"]
                    };
                    credentialsList.Add(credentials);
                }

                return credentialsList;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsListByPrincipalPartNo",
                                DateTime.Now.Ticks - ts, e));

                throw;
            }
        }

        /// <summary>
        /// 验证主体持有的证书是否能够合法的访问资源
        /// </summary>
        /// <param name="resourceNo"></param>
        /// <param name="principalPartNos"></param>
        /// <returns></returns>
        public bool VerifyCredentialsVisitToResource(string resourceNo,
            params string[] principalPartNos)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var credentialsList = GetCredentialsListByResourceNoAndPrincipalPartNo(resourceNo, principalPartNos);
                if (credentialsList == null || credentialsList.Count == 0)
                    throw new Exception("check fails to find a valid credentials ");
                //存在拒绝
                if (credentialsList.Any(credentialse => credentialse.LimitsRules == LimitsRules.Refuse))
                    return false;
                //未存在拒绝且存在允许
                if (credentialsList.Any(credentialse => credentialse.LimitsRules == LimitsRules.Allow))
                    return true;
                //全部默认则拒绝
                return false;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "VerifyCredentialsVisitToResource",
                                DateTime.Now.Ticks - ts, e));

                throw;
            }
        }
    }
}
