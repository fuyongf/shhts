﻿using System;
using System.Data;

using PinganHouse.SHHTS.AccessControl.Common;
using PinganHouse.SHHTS.AccessControl.Entities;
using PinganHouse.SHHTS.AccessControl.Utils.Log;

namespace PinganHouse.SHHTS.AccessControl.BLL
{
    public class UserBll
    {
        private const string ClassName = "UserBll";

        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <param name="pageSize">页显示数</param>
        /// <param name="pageIndex">显示页数</param>
        /// <param name="count">总数</param>
        /// <param name="filter">筛选条件</param>
        /// <returns>用户信息列表</returns>
        public static DataTable GetUserTable(int pageSize, int pageIndex,string filter,out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var dataTable = User.GetUserTable(pageSize, pageIndex,filter, out count);

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetUserTable",
                                DateTime.Now.Ticks - ts, e));

                throw;
            }
        }


        /// <summary>
        /// 获取指定应用
        /// </summary>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="applications"></param>
        /// <param name="filter"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static DataTable GetUserListOutApplications(int page, int rows, string applications, string filter, out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var dataTable = User.GetUserTableListOutApplications(page, rows, applications, filter, out count);

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetUserTable",
                                DateTime.Now.Ticks - ts, e));

                throw;
            }
        }

        /// <summary>
        /// 获取指定应用
        /// </summary>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="applications"></param>
        /// <param name="filter"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static DataTable GetUserListInApplications(int page, int rows, string applications, string filter, out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var dataTable = User.GetUserTableListInApplications(page, rows, applications, filter, out count);

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetUserTable",
                                DateTime.Now.Ticks - ts, e));

                throw;
            }
        }
    }
}
