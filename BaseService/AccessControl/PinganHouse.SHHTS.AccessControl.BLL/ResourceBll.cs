﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

using PinganHouse.SHHTS.AccessControl.Common;
using PinganHouse.SHHTS.AccessControl.Entities;
using PinganHouse.SHHTS.AccessControl.Utils.Log;

namespace PinganHouse.SHHTS.AccessControl.BLL
{
    public class ResourceBll
    {
        private const string ClassName = "ResourceBll";

        /// <summary>
        /// 输入和输出是否是同一个域(有效性验证)
        /// </summary>
        /// <param name="inputApplications"></param>
        /// <param name="sysNo">证书系统编号</param>
        /// <returns></returns>
        public static bool Validation(long inputApplications, long sysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var resource = Resource.GetObject(sysNo);

                if (resource == null) return false;

                return resource.Applications == inputApplications;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "Validation",
                                DateTime.Now.Ticks - ts, e));

                throw;
            }
        }

        /// <summary>
        /// 获取资源列表
        /// </summary>
        /// <param name="pageSize">页显示数</param>
        /// <param name="pageIndex">显示页数</param>
        /// <param name="filter">过滤条件</param>
        /// <param name="applications">应用程序域</param>
        /// <param name="count">总数</param>
        /// <returns>资源信息列表</returns>
        public static DataTable GetResourceTable(int pageSize, int pageIndex,string filter,string applications, out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var dataTable = Resource.GetResourceTable(pageSize, pageIndex, filter,applications, out count);

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetResourceTable",
                                DateTime.Now.Ticks - ts, e));

                throw;
            }
        }

        /// <summary>
        /// 获取该资源绑定证书集合
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">显示行</param>
        /// <param name="resource">资源</param>
        /// <param name="count">总记录数</param>
        /// <returns></returns>
        public static DataTable GetCredentialsTableListInResource(int page, int rows, string resource, out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var dataTable = Credentials.GetCredentialsTableListInResource(page, rows, resource, out count);

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsListInResource",
                                DateTime.Now.Ticks - ts, e));

                throw;
            }
        }

        /// <summary>
        /// 获取主体集合所有资源及相关证书规则
        /// </summary>
        /// <param name="principalNos"></param>
        /// <param name="appNo"></param>
        /// <returns></returns>
        public static Dictionary<string, DataRowCollection> GetResourcesByPrincipalNos(List<string> principalNos, string appNo)
        {
            var data = new Dictionary<string, DataRowCollection>();
            if (principalNos == null || principalNos.Count == 0 || string.IsNullOrWhiteSpace(appNo))
                return data;
            var principals = PrincipalPartBll.GetPrincipalPartsByNos(principalNos, appNo);
            if (principals == null || principals.Count==0)
                return data;
            Parallel.ForEach(principals, principal =>
            {
                try
                {
                    List<long> credentials = PrincipalPartBll.GetCredentialsByPrincipalSysNo(principal.SysNo) as List<long>;
                    if (credentials != null && credentials.Count > 0)
                    {
                        var result = Resource.GetResoureRulesByCredentialsSysNo(credentials);
                        if (result != null && result.Count > 0)
                        {
                            data.Add(principal.PrincipalPartNo, result);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                }
            });
            return data;
        }
    }
}
