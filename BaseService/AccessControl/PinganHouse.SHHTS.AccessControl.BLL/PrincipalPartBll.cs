﻿using System;
using System.Data;
using System.Collections.Generic;

using PinganHouse.SHHTS.AccessControl.Common;
using PinganHouse.SHHTS.AccessControl.Entities;
using PinganHouse.SHHTS.AccessControl.Utils.Log;

namespace PinganHouse.SHHTS.AccessControl.BLL
{
    public class PrincipalPartBll
    {

        private const string ClassName = "ResourceBll";

        /// <summary>
        /// 输入和输出是否是同一个域(有效性验证)
        /// </summary>
        /// <param name="inputApplications"></param>
        /// <param name="sysNo">证书系统编号</param>
        /// <returns></returns>
        public static bool Validation(long inputApplications, long sysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var principalPart = PrincipalPart.GetObject(sysNo);

                if (principalPart == null) return false;

                return principalPart.Applications == inputApplications;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "Validation",
                                DateTime.Now.Ticks - ts, e));

                throw;
            }
        }

        /// <summary>
        /// 获取该资源绑定证书集合
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">显示行</param>
        /// <param name="principalPart">主体</param>
        /// <param name="count">总记录数</param>
        /// <returns></returns>
        public static DataTable GetCredentialsListInPrincipalPart(int page, int rows, string principalPart, out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var dataTable = Credentials.GetCredentialsTableListInPrincipalPart(page, rows, principalPart, out count);

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsListInPrincipalPart",
                                DateTime.Now.Ticks - ts, e));

                throw;
            }
        }

        /// <summary>
        /// 获取主体列表
        /// </summary>
        /// <param name="principalPartNos"></param>
        /// <param name="appNo"></param>
        /// <returns></returns>
        public static IList<PrincipalPart> GetPrincipalPartsByNos(List<string> principalPartNos, string appNo)
        {
            if (principalPartNos == null || principalPartNos.Count == 0 || string.IsNullOrWhiteSpace(appNo))
                return null;
            var result = Applications.GetApplicationSysNo(appNo);
            long applicationSysNo = 0;
            var change = long.TryParse(result, out applicationSysNo);
            if (!change)
                return null;
            
            return PrincipalPart.GetPrincipalPartsByNos(principalPartNos, applicationSysNo);
        }


        public static IList<long> GetCredentialsByPrincipalSysNo(long principalSysNo)
        {
            if (principalSysNo <= 0)
                throw new ArgumentNullException("principalSysNo", "参数值不合法");
            return PrincipalPart.GetCredentialsByPrincipalSysNo(principalSysNo);
        }
    }
}
