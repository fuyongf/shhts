﻿using System;
using System.Collections;

using PinganHouse.SHHTS.AccessControl.ServiceContracts.DTO;

namespace PinganHouse.SHHTS.AccessControl.Proxy
{
    /// <summary>
    ///     授权属性
    /// </summary>
    [Serializable, AttributeUsage(AttributeTargets.All, AllowMultiple = true, Inherited = true)]
    public class PermissionAttribute : Attribute , IResourceInfo
    {
        #region ctor

        /// <summary>
        ///     构建一个权限属性
        /// </summary>
        /// <param name="permissionCode">权限码</param>
        public PermissionAttribute(string resourceNo)
        {
            ResourceNo = resourceNo;
        }

        public PermissionAttribute()
        {
        }

        public PermissionAttribute(string resourceNo, string appNo)
            : this(resourceNo)
        {
            AppNo = appNo;
        }


        /// <summary>
        ///     构建一个权限属性
        /// </summary>
        /// <param name="permissionCode">权限码</param>
        /// <param name="attach">附属信息</param>
        public PermissionAttribute(string resourceNo, Hashtable attach)
        {
            ResourceNo = resourceNo;
            Attach = attach;
        }

        #endregion

        #region property

        /// <summary>
        ///     权限码
        /// </summary>
        public string ResourceNo { get; set; }

        /// <summary>
        ///     附属信息
        /// </summary>
        public IDictionary Attach { get; set; }

        /// <summary>
        /// 业务系统编号
        /// </summary>
        public string AppNo { get; set; }

        #endregion
    }
}