﻿using System.Security.Permissions;

namespace PinganHouse.SHHTS.AccessControl.Proxy
{
    /// <summary>
    ///     拦截动作
    /// </summary>
    public enum InterceptorAction
    {
        /// <summary>
        ///     要求拦截认证
        /// </summary>
        Demand = SecurityAction.Demand
    }
}