﻿using System;
using System.Collections;
using System.Security;
using System.Security.Permissions;

using PinganHouse.SHHTS.AccessControl.ServiceContracts.DTO;

namespace PinganHouse.SHHTS.AccessControl.Proxy
{
    /// <summary>
    ///     安全认证属性
    /// </summary>
    [Serializable,
     AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class SecurityAuthorizeAttribute : InterceptorAttribute, IPermission, IResourceInfo
    {
        /// <summary>
        ///     构建一个安全认证属性
        /// </summary>
        /// <param name="action">安全操作</param>
        public SecurityAuthorizeAttribute(SecurityAction action= SecurityAction.Demand)
            : base(action)
        {
        }

        /// <summary>
        ///     构建一个安全认证属性
        /// </summary>
        /// <param name="action">拦截动作</param>
        public SecurityAuthorizeAttribute(InterceptorAction action)
            : base(action)
        {
        }

        /// <summary>
        ///     权限码
        /// </summary>
        public string ResourceNo { get; set; }

        /// <summary>
        /// 业务系统编号
        /// </summary>
        public string AppNo { get;set;}

        public IDictionary Attach { get; set; }

        protected override void Demand()
        {
            PermissionProxyService.GetInstance().Authorize(this, true, true);
        }

        #region IPermission Members

        IPermission IPermission.Copy()
        {
            return (IPermission)MemberwiseClone();
        }

        IPermission IPermission.Intersect(IPermission target)
        {
            if (target != null && target.GetType() == GetType())
            {
                return (IPermission)((SecurityAuthorizeAttribute)target).MemberwiseClone();
            }
            return null;
        }

        bool IPermission.IsSubsetOf(IPermission target)
        {
            return false;
        }

        IPermission IPermission.Union(IPermission target)
        {
            if (target != null && target.GetType() == GetType())
            {
                return (IPermission)((SecurityAuthorizeAttribute)target).MemberwiseClone();
            }
            return null;
        }

        #endregion

        #region ISecurityEncodable Members

        void ISecurityEncodable.FromXml(SecurityElement e)
        {
            throw new NotImplementedException();
        }

        SecurityElement ISecurityEncodable.ToXml()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}