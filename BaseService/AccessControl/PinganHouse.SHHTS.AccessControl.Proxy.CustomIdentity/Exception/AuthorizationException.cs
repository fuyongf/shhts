﻿using System;

namespace PinganHouse.SHHTS.AccessControl.Proxy
{
    /// <summary>
    /// 用户授权认证失败时的异常信息
    /// </summary>
    public class AuthorizationException : UnauthorizedAccessException
    {
        public AuthorizationException()
        {
        }

        public AuthorizationException(string message) : base(message)
        {
        }
    }
}