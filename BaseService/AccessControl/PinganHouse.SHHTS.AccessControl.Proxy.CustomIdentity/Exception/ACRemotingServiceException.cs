﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.AccessControl.Proxy
{
    public class ACRemotingServiceException : SystemException
    {
        public ACRemotingServiceException()
        {
        }

        public ACRemotingServiceException(string message):base(message)
        {
        }
    }
}
