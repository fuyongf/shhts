﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace PinganHouse.SHHTS.AccessControl.Proxy.Utils
{
    public class CacheHelper<T>
    {
        static Dictionary<string, CacheObject<T>> CacheContainer = new Dictionary<string, CacheObject<T>>();

        static CacheHelper<T> _instance = new CacheHelper<T>();

        static object _SyncLock = new object();

        private const double _interval = 1000;

        private Timer _timer = null; 

        private CacheHelper() 
        {
            _timer = new Timer(_interval);
            _timer.Enabled =true;
            _timer.AutoReset = true;
            _timer.Elapsed += new ElapsedEventHandler(RefreshCache);
            _timer.Start();
        }

        public static CacheHelper<T> Instance 
        {
            get 
            {
                return _instance;
            }
        }


        #region 基础操作
        /// <summary>
        /// 设置缓存对象
        /// </summary>
        /// <param name="key"></param>
        /// <param name="obj"></param>
        /// <param name="expirs"></param>
        public void Set(string key, T obj, TimeSpan expirs)
        {
            CacheObject<T> cacheObj = new CacheObject<T>
            {
                Key = key,
                Value = obj,
                Expirs = DateTime.Now.Add(expirs)
            };
            lock (_SyncLock)
            {
                if (CacheContainer.ContainsKey(key))
                {
                    CacheContainer[key] = cacheObj;
                }
                else
                {
                    CacheContainer.Add(key, cacheObj);
                }
            }
        }

        /// <summary>
        /// 从缓存中获取对象
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public T Get(string key)
        {
            if (!CacheContainer.ContainsKey(key))
                return default(T);
            CacheObject<T> cache = CacheContainer[key];
            if (cache == null)
                return default(T);
            if (cache.Expirs < DateTime.Now)
            {
                lock (_SyncLock)
                {
                    CacheContainer.Remove(key);
                }
                return default(T);
            }
            return cache.Value;
        }

        /// <summary>
        /// 清空缓存
        /// </summary>
        public void Clear() 
        {
            if (CacheContainer.Count == 0)
                return;
            try
            {
                lock (_SyncLock) 
                {
                    CacheContainer.Clear();
                }
            }
            catch { }
        }

        #endregion


        #region 缓存策略
        private void RefreshCache(object sender, EventArgs e) 
        {
            if (CacheContainer.Count == 0)
                return;
            try
            {
                foreach (var item in CacheContainer)
                {
                    lock (_SyncLock)
                    {
                        if (item.Value.Expirs <= DateTime.Now)
                        {
                            CacheContainer.Remove(item.Key);
                        }
                    }
                }
            }
            catch { }
        }
        #endregion
    }


    class CacheObject<T> 
    {
        public string Key { get; set; }
        public T Value { get; set; }

        public DateTime Expirs { get; set; }
    }
}
