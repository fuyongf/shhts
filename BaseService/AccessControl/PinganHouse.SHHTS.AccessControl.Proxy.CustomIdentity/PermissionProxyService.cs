﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;

using HTB.DevFx;

using PinganHouse.SHHTS.AccessControl.ServiceContracts.DTO;
using PinganHouse.SHHTS.AccessControl.ServiceContracts;
using PinganHouse.SHHTS.AccessControl.Proxy.Utils;

namespace PinganHouse.SHHTS.AccessControl.Proxy
{
    /// <summary>
    /// AC服务端出错委托
    /// </summary>
    /// <param name="?"></param>
    /// <returns></returns>
    public delegate bool ACRemotingServiceErrorEventHandler();

    public class PermissionProxyService
    {
        static IUserIdentity UserIdentity;

        readonly static PermissionProxyService permissionProxyService = new PermissionProxyService();

        protected IPermissionService PermissionService {
            get { return ObjectService.GetObject<IPermissionService>(); }
        }

        private PermissionProxyService()
        {
        }

        public static PermissionProxyService GetInstance()
        {
            return permissionProxyService;
        }

        /// <summary>
        /// AC服务端出错事件
        /// </summary>
        public event ACRemotingServiceErrorEventHandler OnACRemotingServiceErrorOccurring;

        /// <summary>
        /// 根据角色编号获取此角色下所有资源信息
        /// </summary>
        /// <param name="principalNos">资源编号</param>
        /// <returns></returns>
        private IList<ResourceInfo> GetResourcesByPrincipalNos(IList<string> principalNos, string appNo)
        {
            if (principalNos == null || principalNos.Count == 0 || string.IsNullOrEmpty(appNo))
                return null;
            List<ResourceInfo> resources = new List<ResourceInfo>();
            List<string> queryStrings = new List<string>();
            foreach (string principalNo in principalNos)
            {
                var resCache = CacheHelper<IList<ResourceInfo>>.Instance.Get(principalNo);
                if (resCache == null)
                {
                    queryStrings.Add(principalNo);
                    continue;
                }
                resources.AddRange(resCache);
            }
            if (queryStrings.Count != 0)
            {
                this.GetResourcesByPrincipalNosInternal(queryStrings, appNo, ref resources);
            }
            return resources;
        }


        private void GetResourcesByPrincipalNosInternal(List<string> principalNos, string appNo, ref List<ResourceInfo> resources)
        {
            try
            {
                int expireTime = 0;
                var result = PermissionService.GetResourcesByPrincipalNos(principalNos, appNo, ref expireTime);
                if (result != null && result.Count > 0)
                {
                    foreach (var r in result)
                    {
                        resources.AddRange(r.Value);
                        CacheHelper<IList<ResourceInfo>>.Instance.Set(r.Key, r.Value, new TimeSpan(0, expireTime, 0));
                    }
                }
            }
            catch (EndpointNotFoundException ex)
            {
                throw new ACRemotingServiceException("网络有异常，请稍后重试!");
            }
            catch (Exception ex)
            {
                throw new ACRemotingServiceException("权限系统内部错误，请及时联系管理员!");
            }
        }

        /// <summary>
        /// 如AC服务端出现问题，比如服务不可用，客户重载决定是否放开权限
        /// 默认为不放开
        /// </summary>
        /// <returns></returns>
        protected virtual bool SetSwitchOnACRemotingServiceErrorOccurring()
        {
            bool permissionSwitch = false;
            //如AC服务端出现问题，比如服务不可用，通过事件通知客户，便客户决定是否放开权限
            var handler = this.OnACRemotingServiceErrorOccurring;
            if (handler != null)
                permissionSwitch = handler();
            return permissionSwitch;
        }

        /// <summary>
        /// 获取权限开关
        /// </summary>
        /// <returns></returns>
        private bool GetPermissionSwitsh(string appNo)
        {
            if (string.IsNullOrEmpty(appNo))
                return false;
            string key = string.Format("Switch{0}", appNo);
            bool permissionSwitch = false;
            int expireTime = 0;
            try 
            {
                var pSwitch = CacheHelper<bool?>.Instance.Get(key);
                if (pSwitch == null || !pSwitch.HasValue)
                {
                    pSwitch = PermissionService.GetPermissionSwitsh(appNo, ref expireTime);
                    CacheHelper<bool?>.Instance.Set(key, pSwitch, new TimeSpan(0, expireTime, 0));
                }              
                permissionSwitch = pSwitch.Value;
            }
            catch (Exception ex)
            {
                //如AC服务端出现问题，比如服务不可用，通过事件通知客户，便客户决定是否放开权限
                permissionSwitch = this.SetSwitchOnACRemotingServiceErrorOccurring();
            }
            return permissionSwitch;
        }
        #region 客户端核心验证

        /// <summary>
        ///     授权认证
        /// </summary>
        /// <param name="permissionCode">权限编码</param>
        /// <param name="appNo">应用系统业务编码</param>
        /// <param name="principalNos">主体业务编号集合</param>
        /// <param name="attach">是否从调用堆栈获取权限</param>
        /// <param name="includeStack">认证失败时是否抛出异常</param>
        /// <param name="throwOnFailed"></param>
        /// <returns>是否认证成功</returns>
        public bool Authorize(string permissionCode = null, string appNo = null, IList<string> principalNos = null, Hashtable attach = null, bool includeStack = true,
            bool throwOnFailed = false)
        {
            return Authorize(new ResourceInfo { ResourceNo = permissionCode, Attach = attach, AppNo=appNo }, includeStack,
                throwOnFailed, principalNos);
        }

        /// <summary>
        ///     授权认证
        /// </summary>
        /// <param name="permission">权限对象</param>
        /// <param name="includeStack">是否包含堆栈中的权限对象信息</param>
        /// <param name="throwOnFailed">认证失败时是否抛出异常</param>
        /// <returns>是否认证成功</returns>
        public bool Authorize(IResourceInfo permission, bool includeStack = true, bool throwOnFailed = false, IList<string> principalNos = null)
        {
            List<IResourceInfo> list = includeStack ? GetPermissionsFromStack() : new List<IResourceInfo>();
            if (permission != null && !string.IsNullOrEmpty(permission.ResourceNo))
            {
                list.Insert(0, permission);
            }
            return Authorize(throwOnFailed, principalNos, list.ToArray());
        }

        /// <summary>
        ///     授权认证
        /// </summary>
        /// <param name="throwOnFailed">验证失败时是否抛出异常</param>
        /// <param name="permissions">一组资源对象</param>
        /// <returns>是否通过认证</returns>
        public bool Authorize(bool throwOnFailed, IList<string> principalNos, params IResourceInfo[] resources)
        {
            //检验当前用户是否已通过了认证
            if (UserIdentity == null || !UserIdentity.IsAuthenticated)
            {
                if (throwOnFailed)
                {
                    ThrowException(new AuthenticationException("当前未能通过身份认证！"));
                }
                return false;
            }

            ResourceInfo resource = null;

            //从给定的一组资源对象中查找到第一个设置了资源权限的对象
            if (resources != null && resources.Length > 0)
            {
                foreach (IResourceInfo resItem in resources)
                {
                    if (!string.IsNullOrEmpty(resItem.ResourceNo))
                    {
                        resource = new ResourceInfo
                        {
                            ResourceNo = resItem.ResourceNo,
                            Attach = resItem.Attach,
                            AppNo = resItem.AppNo
                        };
                        break;
                    }
                }
            }

            //未设任何权限限制的则表示通过认证
            //设了权限限制但是权限码高为空的，表示通过认证
            if (resource == null || string.IsNullOrEmpty(resource.ResourceNo))
            {
                return true;
            }
            //如未埋入appno，则取全局
            if (string.IsNullOrEmpty(resource.AppNo))
                resource.AppNo = UserIdentity.AppNo;
            //权限开关
            bool switsh = this.GetPermissionSwitsh(resource.AppNo);
            if (!switsh)
                return true;
            if (principalNos == null)
            {
                principalNos = UserIdentity.PrincipalNos;
            }
            //获取主体所持有的资源规则
            IList<ResourceInfo> userPermissionList = GetResourcesByPrincipalNos(principalNos, resource.AppNo);

            if (userPermissionList == null || userPermissionList.Count == 0)
            {
                if (throwOnFailed)
                {
                    ThrowException(new AuthorizationException(string.Format("当前用户不具有此操作的权限！ApppNo:{0}, ResourceNo:{1},PNO:{2}", resource.AppNo,resource.ResourceNo, principalNos[0])));
                }
                return false;
            }

            var userRes = userPermissionList.Where(m => m != null && m.ResourceNo == resource.ResourceNo);

            bool result = true;

            if (userRes == null || userRes.Count() == 0 || userRes.Any(m => m.LimitsRules == -1))
                result = false;
            else
                result = userRes.Any(m => m.Authorized);

            if (!result && throwOnFailed)
            {
                ThrowException(new AuthorizationException(string.Format("当前用户不具有此操作的权限！ApppNo:{0}, ResourceNo:{1},PNO:{2}", resource.AppNo,resource.ResourceNo, principalNos[0])));
            }
            return result;
        }

        private void ThrowException(Exception ex) 
        {
            throw ex;
        }

        /// <summary>
        ///     从堆栈中获取元属性
        /// </summary>
        /// <returns></returns>
        private static List<IResourceInfo> GetPermissionsFromStack()
        {
            var list = new List<IResourceInfo>();
            PermissionAttribute[] ps = Utils.PermissionHelp.GetAttributeFromRuntimeStack<PermissionAttribute>(true);
            if (ps != null && ps.Length > 0)
            {
                list.AddRange(ps);
            }
            return list;
        }

        #endregion

        #region 用户相关

        /// <summary>
        /// 设置用户登陆信息引用
        /// </summary>
        /// <param name="userIdentity"></param>
        public void SetUserIdentity(IUserIdentity userIdentity, bool flushCache = true)
        {
            UserIdentity = userIdentity;
            if (flushCache)
                CacheHelper<IList<ResourceInfo>>.Instance.Clear();
        }
        #endregion
    }
}
