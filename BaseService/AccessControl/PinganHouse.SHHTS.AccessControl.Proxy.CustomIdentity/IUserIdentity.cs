﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.AccessControl.Proxy
{
    public interface IUserIdentity
    {
        /// <summary>
        /// 应用编号 由AC系统配置分配
        /// </summary>
        string AppNo { get; set; }

        /// <summary>
        /// 主体业务编号
        /// </summary>
        IList<string> PrincipalNos { get; set; }

        /// <summary>
        /// 是否已认证
        /// </summary>
        bool IsAuthenticated { get; set; }

        /// <summary>
        /// 认证时间
        /// </summary>
        DateTime AuthenticatedTime { get; set; }
    }
}
