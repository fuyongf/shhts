﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PinganHouse.SHHTS.AccessControl.ServiceContracts.DTO
{
    [Serializable]
    public class UserPermissionInfo
    {
        /// <summary>
        /// 员工编号
        /// </summary>
        public string UserNo { get; set; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 用户邮箱
        /// </summary>
        public string EmaillAddr { get; set; }

        /// <summary>
        /// 用户域帐号
        /// </summary>
        public string DomainAccount { get; set; }

        /// <summary>
        /// 已配置的资源
        /// </summary>
        public IList<ResourceInfo> Resources = new List<ResourceInfo>();
    }
}
