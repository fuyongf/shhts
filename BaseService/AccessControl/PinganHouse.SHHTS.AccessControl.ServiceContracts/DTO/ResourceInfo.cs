﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace PinganHouse.SHHTS.AccessControl.ServiceContracts.DTO
{
    public interface IResourceInfo 
    {
        /// <summary>
        /// 资源编号
        /// </summary>
        string ResourceNo { get; }
        /// <summary>
        /// 业务系统编号
        /// </summary>
        string AppNo { get; set; }
        /// <summary>
        /// 附属信息
        /// </summary>
        IDictionary Attach { get; }
    }

    [Serializable]
    public class ResourceInfo : IResourceInfo
    {
        /// <summary>
        /// 资源编号
        /// </summary>
        public string ResourceNo { get; set; }
        /// <summary>
        /// 业务系统编号
        /// </summary>
        public string AppNo { get; set; }

        /// <summary>
        /// 是否已授权
        /// </summary>
        public bool Authorized {
            get { return this.LimitsRules == 1; } 
        }

        /// <summary>
        /// 规则
        /// </summary>
        public int LimitsRules
        {
            get;
            set;
        }

        /// <summary>
        ///     附属信息
        /// </summary>
        public IDictionary Attach { get; set; }
    }
}
