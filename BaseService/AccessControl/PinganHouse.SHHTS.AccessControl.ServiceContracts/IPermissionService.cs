﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;

using PinganHouse.SHHTS.AccessControl.ServiceContracts.DTO;

namespace PinganHouse.SHHTS.AccessControl.ServiceContracts
{
    [ServiceContract]
    public interface IPermissionService
    {

        /// <summary>
        /// 根据主体业务编号获取此主体下所有资源信息
        /// </summary>
        /// <param name="principalNo">主体编号</param>
        /// <param name="appNo">应用系统编号</param>
        /// <returns></returns>
        [OperationContract]
        IList<ResourceInfo> GetResourcesByPrincipalNo(string principalNo, string appNo);

        /// <summary>
        /// 根据主体业务编号获取此主体下所有资源信息
        /// </summary>
        /// <param name="principalNo">主体编号</param>
        /// <param name="appNo">应用系统编号</param>
        /// <param name="cacheMinutes">通知客户端缓存时间（分钟）</param>
        /// <returns></returns>
        [OperationContract(Name = "GetResourcesWithCachTimeByPrincipalNo")]
        IList<ResourceInfo> GetResourcesByPrincipalNo(string principalNo, string appNo, ref int cacheMinutes);

        /// <summary>
        /// 获取应用程序系统权限开关
        /// </summary>
        /// <param name="appNo">应用系统编号</param>
        /// <param name="cacheMinutes">通知客户端缓存时间（分钟）</param>
        /// <returns></returns>
        [OperationContract]
        bool GetPermissionSwitsh(string appNo, ref int cacheMinutes);

        /// <summary>
        /// 检验
        /// </summary>
        /// <param name="principalNo">主体编号</param>
        /// <param name="appNo">应用系统编号</param>
        /// <param name="resourceNo">资源业务编号</param>
        /// <returns></returns>
        [OperationContract]
        bool ValidateAuthorization(List<string> principalNo, string resourceNo, string appNo);

        /// <summary>
        /// 获取主体所持有的证书
        /// </summary>
        /// <param name="principalNos">主体业务编号列表</param>
        /// <param name="appNo">应用系统编号</param>
        /// <returns></returns>
        [OperationContract]
        Dictionary<string, IList<ResourceInfo>> GetResourcesByPrincipalNos(List<string> principalNos, string appNo);

        /// <summary>
        /// 获取主体所持有的证书
        /// </summary>
        /// <param name="principalNos">主体业务编号列表</param>
        /// <param name="appNo">应用系统编号</param>
        ///  <param name="cacheMinutes">通知客户端缓存时间（分钟）</param>
        /// <returns></returns>
        [OperationContract(Name = "GetResourcesWithCacheTimeByPrincipalNos")]
        Dictionary<string, IList<ResourceInfo>> GetResourcesByPrincipalNos(List<string> principalNos, string appNo, ref int cacheMinutes);
    }
}
