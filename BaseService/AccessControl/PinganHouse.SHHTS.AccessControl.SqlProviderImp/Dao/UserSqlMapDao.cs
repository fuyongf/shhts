﻿using System;
using System.Collections.Generic;
using System.Data;

using PinganHouse.SHHTS.AccessControl.Common;
using PinganHouse.SHHTS.AccessControl.Entities;
using PinganHouse.SHHTS.AccessControl.Entities.IDao;
using PinganHouse.SHHTS.AccessControl.Utils.Log;

namespace PinganHouse.SHHTS.AccessControl.SqlProviderImp.Dao
{
    public class UserSqlMapDao : BaseSqlMapDao<User>, IUserDao
    {
        private const string ClassName = "UserSqlMapDao";

        /// <summary>
        /// 添加用户
        /// </summary>
        /// <param name="user">用户对象实体类</param>
        /// <returns></returns>
        public long AddSelf(User user)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var check = IsExists(user.UserName,user.Name);
                if (check)
                {
                    throw new Exception("The same entity exists");
                }
                var checkUserNo = IsExistsByUserNo(user.UserNo);
                if (checkUserNo)
                {
                    throw new Exception("The UserNo already exists");
                }
                var checkDomainName = IsExistsByDomainName(user.DomainName);
                if (checkDomainName)
                {
                    throw new Exception("The DomainName already exists");
                }
                var checkEmailAddr = IsExistsByEmailAddr(user.EmailAddr);
                if (checkEmailAddr)
                {
                    throw new Exception("The EmailAddr already exists");
                }

                ExecuteInsert("User.Insert", user);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "AddSelf",
                                DateTime.Now.Ticks - ts,
                                "[User SysNo:" + user.SysNo + "]"));

                return user.SysNo;

            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "AddSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 修改用户
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool UpdateSelf(User user)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var check = IsCheckByUserNoOrDomainNameOrEmailAddr(user.UserNo, user.DomainName, user.EmailAddr);
                if (check)
                {
                    throw new Exception("The same entity exists");
                }

                var oldUser = GetObject(user.SysNo);

                if (string.IsNullOrEmpty(user.Password))
                    user.Password = oldUser.Password;

                var obj = ExecuteUpdate("User.Update", user);


                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "UpdateSelf",
                                DateTime.Now.Ticks - ts,
                                "[User SysNo:" + user.SysNo + "]"));

                return obj > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "UpdateSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        public bool DeleteSelf(long userSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj = ExecuteDelete("User.Delete", userSysNo);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "DeleteSelf",
                                DateTime.Now.Ticks - ts,
                                "[User SysNo:" + userSysNo + "]"));

                return obj > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "DeleteSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 检查是否已经存在
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool IsExists(string userName, string name)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var dic = new Dictionary<string, object>
                {
                    {"UserName", userName},
                    {"Name", name}
                };
                var obj = ExecuteQueryForAnyObject("User.IsExists", dic);

                int cnt;

                int.TryParse(Convert.ToString(obj), out cnt);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "IsExists",
                                DateTime.Now.Ticks - ts,
                                "[User UserName:" + userName + "]" +
                                "[User Name:" + name + "]"));

                return cnt > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "IsExists",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 检查用户编号是否已经存在
        /// </summary>
        /// <param name="userNo">用户编号</param>
        /// <returns></returns>
        public bool IsExistsByUserNo(string userNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj =ExecuteQueryForAnyObject("User.IsExistsByUserNo", userNo);

                int cnt;

                int.TryParse(Convert.ToString(obj), out cnt);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "IsExistsByUserNo",
                                DateTime.Now.Ticks - ts,
                                "[User UserNo:" + userNo + "]"));

                return cnt > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "IsExistsByUserNo",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 检查域账号是否已经存在
        /// </summary>
        /// <param name="domainName">域账号</param>
        /// <returns></returns>
        public bool IsExistsByDomainName(string domainName)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj = ExecuteQueryForAnyObject("User.IsExistsByDomainName", domainName);

                int cnt;

                int.TryParse(Convert.ToString(obj), out cnt);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "IsExistsByDomainName",
                                DateTime.Now.Ticks - ts,
                                "[User DomainName:" + domainName + "]"));

                return cnt > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "IsExistsByDomainName",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 检查域帐户（邮箱地址）是否已经存在
        /// </summary>
        /// <param name="emailAddr">域帐户（邮箱地址）</param>
        /// <returns></returns>
        public bool IsExistsByEmailAddr(string emailAddr)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj = ExecuteQueryForAnyObject("User.IsExistsByEmailAddr", emailAddr);

                int cnt;

                int.TryParse(Convert.ToString(obj), out cnt);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "IsExistsByEmailAddr",
                                DateTime.Now.Ticks - ts,
                                "[User EmailAddr:" + emailAddr + "]"));

                return cnt > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "IsExistsByEmailAddr",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }


        /// <summary>
        /// 检查用户编号,域账号,域帐户（邮箱地址）是否已经存在
        /// </summary>
        /// <param name="userNo">用户编号</param>
        /// <param name="domainName">域账号</param>
        /// <param name="emailAddr">域帐户（邮箱地址）</param>
        /// <returns></returns>
        public bool IsCheckByUserNoOrDomainNameOrEmailAddr(string userNo, string domainName, string emailAddr)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var dic = new Dictionary<string, object>
                {
                    {"UserNo", userNo},
                    {"DomainName", domainName},
                    {"EmailAddr", emailAddr}
                };

                var obj = ExecuteQueryForAnyObject("User.IsCheckByUserNoOrDomainNameOrEmailAddr", dic);

                int cnt;

                int.TryParse(Convert.ToString(obj), out cnt);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "IsCheckByUserNoOrDomainNameOrEmailAddr",
                                DateTime.Now.Ticks - ts,
                                "[User UserNo:" + userNo + "]" +
                                "[User DomainName:" + domainName + "]" +
                                "[User EmailAddr:" + emailAddr + "]"));

                return cnt > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "IsCheckByUserNoOrDomainNameOrEmailAddr",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 获取用户对象
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        public User GetObject(long userSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj = ExecuteQueryForObject("User.GetObject", userSysNo);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetObject",
                                DateTime.Now.Ticks - ts,
                                "[User SysNo:" + userSysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetObject",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 根据工号查询
        /// </summary>
        /// <param name="userNo">工号</param>
        /// <returns></returns>
        public User GetUserByUserNo(string userNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj = ExecuteQueryForObject("User.GetUserByUserNo", userNo);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetUserByUserNo",
                                DateTime.Now.Ticks - ts,
                                "[User Empid:" + userNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetUserByUserNo",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 根据域账号查询
        /// </summary>
        /// <param name="domainName">域账号</param>
        /// <returns></returns>
        public User GetUserByDomainName(string domainName)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj = ExecuteQueryForObject("User.GetUserByDomainName", domainName);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetUserByDomainName",
                                DateTime.Now.Ticks - ts,
                                "[User DomainName:" + domainName + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetUserByDomainName",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 根据域帐户（邮箱地址）查询
        /// </summary>
        /// <param name="emailAddr">域帐户（邮箱地址）</param>
        /// <returns></returns>
        public User GetUserByEmailAddr(string emailAddr)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj = ExecuteQueryForObject("User.GetUserByEmailAddr", emailAddr);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetUserByEmailAddr",
                                DateTime.Now.Ticks - ts,
                                "[User EmailAddr:" + emailAddr + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetUserByEmailAddr",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <param name="pageSize">页显示数</param>
        /// <param name="pageIndex">显示页数</param>
        /// <param name="count">总数</param>
        /// <param name="filter">筛选列表</param>
        /// <returns>用户信息列表</returns>
        public DataTable GetUserTable(int pageSize, int pageIndex, string filter,out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var dic = new Dictionary<string, object>
                {
                    {"PageSize", pageSize},
                    {"PageIndex", pageIndex},
                    {"Filter", filter}
                };

                var dataSet = ExecuteQueryWithDataSet("User.GetUserTable", dic);

                var dataTable = dataSet.Tables[0];
                int.TryParse(Convert.ToString(dataSet.Tables[1].Rows[0][0]),out count);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                    ClassName,
                                    "GetUserTable",
                                    DateTime.Now.Ticks - ts,
                                    "[User PageIndex:" + pageIndex + "]" +
                                    "[User PageSize:" +  pageSize + "]"));

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                            ClassName,
                            "GetUserTable",
                            DateTime.Now.Ticks - ts, e));
                throw;
            }
        }


        /// <summary>
        /// 查询未被添加到某指定应用的管理人员
        /// </summary>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="applications">应用程序域</param>
        /// <param name="filter"></param>
        /// <param name="count"></param>
        /// <returns>人员列表</returns>
        public DataTable GetUserTableListOutApplications(int page, int rows, string applications, string filter, out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var dic = new Dictionary<string, object>
                {
                    {"PageSize", page},
                    {"PageIndex", rows},
                    {"Filter", filter},
                    {"ApplicationsSysNo", applications}
                };

                //获取关系表集合
                var dataSet = ExecuteQueryWithDataSet("User.GetUserTableListOutApplications", dic);

                var dataTable = dataSet.Tables[0];

                int.TryParse(Convert.ToString(dataSet.Tables[1].Rows[0][0]), out count);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetUserTableListOutApplications",
                                DateTime.Now.Ticks - ts,
                                "[Applications sysNo:" + applications + "]"));

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetUserTableListOutApplications",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }


        /// <summary>
        /// 查询未被添加到某指定应用的管理人员
        /// </summary>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="applications">应用程序域</param>
        /// <param name="filter"></param>
        /// <param name="count"></param>
        /// <returns>人员列表</returns>
        public DataTable GetUserTableListInApplications(int page, int rows, string applications, string filter, out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var dic = new Dictionary<string, object>
                {
                    {"PageSize", page},
                    {"PageIndex", rows},
                    {"Filter", filter},
                    {"ApplicationsSysNo", applications}
                };

                //获取关系表集合
                var dataSet = ExecuteQueryWithDataSet("User.GetUserTableListInApplications", dic);

                var dataTable = dataSet.Tables[0];

                int.TryParse(Convert.ToString(dataSet.Tables[1].Rows[0][0]), out count);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetUserTableListInApplications",
                                DateTime.Now.Ticks - ts,
                                "[Applications sysNo:" + applications + "]"));

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetUserTableListInApplications",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 添加人员到应用
        /// </summary>
        /// <param name="applicationsEntity"></param>
        /// <param name="userListEntity"></param>
        /// <returns></returns>
        public bool AddUserListToApplications(Applications applicationsEntity, List<User> userListEntity)
        {
            var ts = DateTime.Now.Ticks;
            var sqlMap = GetLocalSqlMap();
            try
            {
                if (userListEntity == null)
                {
                    throw new Exception("user can not be empty or Null ");
                }
                sqlMap.BeginTransaction();

                foreach (var user in userListEntity)
                {
                    var dic = new Dictionary<string, object>
                    {
                        {"Applications", applicationsEntity},
                        {"UserSysNo", user.SysNo}
                    };
                    //检查是否已经存在
                    var cnt = sqlMap.QueryForObject<int>("User.IsExistsUserInApplications", dic);
                    //判断是否已经插入
                    if (cnt > 0) continue;
                    //插入关联
                    sqlMap.Insert("User.AddUserToApplications", dic);
                }

                sqlMap.CommitTransaction();

                return true;
            }
            catch (Exception e)
            {

                sqlMap.RollBackTransaction();

                Log.Error(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "AddUserListToApplications",
                    DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 移除管理人员
        /// </summary>
        /// <param name="relationSysNo">关系系统编号</param>
        /// <returns></returns>
        public bool RemoveUserInApplications(int relationSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj = ExecuteUpdate("User.RemoveUserInApplications", relationSysNo);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "RemoveUserInApplications",
                    DateTime.Now.Ticks - ts,
                    "[Relation SysNo:" + relationSysNo + "]"));

                return obj > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "RemoveUserInApplications",
                    DateTime.Now.Ticks - ts, e));
                throw;
            }
        }
    }
}
