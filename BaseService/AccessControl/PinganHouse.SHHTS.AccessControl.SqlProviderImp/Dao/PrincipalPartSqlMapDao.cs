﻿using System;
using System.Collections.Generic;
using System.Data;

using PinganHouse.SHHTS.AccessControl.Common;
using PinganHouse.SHHTS.AccessControl.Entities;
using PinganHouse.SHHTS.AccessControl.Entities.IDao;
using PinganHouse.SHHTS.AccessControl.Utils;
using PinganHouse.SHHTS.AccessControl.Utils.Log;

namespace PinganHouse.SHHTS.AccessControl.SqlProviderImp.Dao
{
    public class PrincipalPartSqlMapDao : BaseSqlMapDao<PrincipalPart>, IPrincipalPartDao
    {
        private const string ClassName = "PrincipalPartSqlMapDao";

        /// <summary>
        /// 添加主体
        /// </summary>
        /// <param name="principalPart">主体对象</param>
        /// <returns></returns>
        public long AddSelf(PrincipalPart principalPart)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var check = IsExists(principalPart.PrincipalPartName,Convert.ToString(principalPart.Applications));
                if (check)
                {
                    throw new Exception("The same entity exists");
                }

                ExecuteInsert("PrincipalPart.Insert", principalPart);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "AddSelf",
                                DateTime.Now.Ticks - ts,
                                "[PrincipalPart SysNo:" + principalPart.SysNo + "]"));

                return principalPart.SysNo;

            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "AddSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 检查同个应用程序是否存在同一个
        /// </summary>
        /// <param name="principalPartName">主体名称</param>
        /// <param name="applications">应用程序名称</param>
        /// <returns></returns>
        private bool IsExists(string principalPartName, string applications)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var dic = new Dictionary<string, object>
                {
                    {"PrincipalPartName", principalPartName},
                    {"Applications", applications}
                };
                var obj = ExecuteQueryForAnyObject("PrincipalPart.IsExists", dic);

                int cnt;

                int.TryParse(Convert.ToString(obj), out cnt);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "IsExists",
                                DateTime.Now.Ticks - ts,
                                "[PrincipalPart Name:" + principalPartName + "]" +
                                "[PrincipalPart Applications:" + applications + "]"));

                return cnt > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "IsExists",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 修改主体对象
        /// </summary>
        /// <param name="principalPart"></param>
        /// <returns></returns>
        public bool UpdateSelf(PrincipalPart principalPart)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var check = IsExistsExcludeSelf(principalPart.PrincipalPartName, Convert.ToString(principalPart.Applications), principalPart.SysNo);
                if (check)
                {
                    throw new Exception("Modify data without legal");
                }


                var obj = ExecuteUpdate("PrincipalPart.Update", principalPart);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "UpdateSelf",
                                DateTime.Now.Ticks - ts,
                                "[PrincipalPart SysNo:" + principalPart.SysNo + "]"));

                return obj > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "UpdateSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 检查同一个应用程序修改主体是否存在同名
        /// </summary>
        /// <param name="principalPartName">主体名称</param>
        /// <param name="applications">应用程序</param>
        /// <param name="sysNo">主体系统编号</param>
        /// <returns></returns>
        private bool IsExistsExcludeSelf(string principalPartName, string applications, long sysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var dic = new Dictionary<string, object>
                {
                    {"PrincipalPartName", principalPartName},
                    {"Applications", applications},
                    {"SysNo", sysNo}
                };
                var obj = ExecuteQueryForAnyObject("PrincipalPart.IsExistsExcludeSelf", dic);

                int cnt;

                int.TryParse(Convert.ToString(obj), out cnt);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "IsExistsExcludeSelf",
                                DateTime.Now.Ticks - ts,
                                "[PrincipalPart SysNo:" + sysNo + "]" +
                                "[PrincipalPart Name:" + principalPartName + "]" +
                                "[Applications :" + applications + "]"));

                return cnt > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "IsExistsExcludeSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 删除主体
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        public bool DeleteSelf(long sysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj = ExecuteDelete("PrincipalPart.Delete", sysNo);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "DeleteSelf",
                                DateTime.Now.Ticks - ts,
                                "[PrincipalPart SysNo:" + sysNo + "]"));

                return obj > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "DeleteSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 获取主体
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        public PrincipalPart GetObject(long sysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj = ExecuteQueryForObject("PrincipalPart.GetObject", sysNo);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetObject",
                                DateTime.Now.Ticks - ts,
                                "[PrincipalPart SysNo:" + sysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetObject",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 获取主体
        /// </summary>
        /// <param name="principalPartNo">主体业务编</param>
        /// <returns></returns>
        public PrincipalPart GetPrincipalPartByNo(string principalPartNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj = ExecuteQueryForObject("PrincipalPart.GetPrincipalPartByNo", principalPartNo);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetPrincipalPartByNo",
                                DateTime.Now.Ticks - ts,
                                "[PrincipalPartNo:" + principalPartNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetPrincipalPartByNo",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        public IList<PrincipalPart> GetPrincipalPartsByNos(List<string> principalPartNos, long applicationSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var dic = new Dictionary<string, object>
                {
                    {"principalPartNos",principalPartNos},
                    {"applicationSysNo",applicationSysNo}
                };
                return ExecuteQueryForList<PrincipalPart>("PrincipalPart.GetPrincipalPartsByNos", dic);
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }

        /// <summary>
        /// 获取主体列表
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="filter"></param>
        /// <param name="applications">应用程序域</param>
        /// <param name="count"></param>
        /// <returns></returns>
        public DataTable GetPrincipalPartTable(int pageSize, int pageIndex, string filter,string applications, out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var dic = new Dictionary<string, object>
                {
                    {"PageSize", pageSize},
                    {"Filter", filter},
                    {"Applications", applications},
                    {"PageIndex", pageIndex}
                };

                var dataset = ExecuteQueryWithDataSet("PrincipalPart.GetPrincipalPartTable", dic);
                //赋值--总记录数
                int.TryParse(Convert.ToString(dataset.Tables[1].Rows[0][0]), out count);
                //数据
                var dataTable = dataset.Tables[0];

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetPrincipalPartTable",
                                DateTime.Now.Ticks - ts,
                                "[PrincipalPart PageIndex:" + pageIndex + "]" +
                                "[PrincipalPart PageSize:" + pageSize + "]"));

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetPrincipalPartTable",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 给主体添加持有证书
        /// </summary>
        /// <param name="principalPart">主体对象</param>
        /// <param name="credentialsList">证书集合体</param>
        /// <returns></returns>
        public bool AddCredentialsToPrincipalPart(PrincipalPart principalPart, List<Credentials> credentialsList)
        {
            var ts = DateTime.Now.Ticks;
            var sqlMap = GetLocalSqlMap();
            try
            {
                if (credentialsList == null)
                {
                    throw new Exception("Parameters can not be empty or Null ");
                }
                sqlMap.BeginTransaction();

                foreach (var credentials in credentialsList)
                {
                    var dic = new Dictionary<string, object>
                    {
                        {"PrincipalPart", principalPart},
                        {"CredentialsSysNo", credentials.SysNo}
                    };
                    //检查是否已经存在
                    var cnt = sqlMap.QueryForObject<int>("PrincipalPartRelation.IsExistsCredentialsInPrincipalPart", dic);
                    if (cnt > 0) continue;
                    dic.Add("SysNo", Sequence.Get());
                    //插入关联
                    sqlMap.Insert("PrincipalPartRelation.AddCredentialsToPrincipalPart", dic);
                }

                sqlMap.CommitTransaction();

                return true;
            }
            catch (Exception e)
            {

                sqlMap.RollBackTransaction();

                Log.Error(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "AddCredentialsToPrincipalPart",
                    DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 移除主体持有证书
        /// </summary>
        /// <param name="relationSysNo"></param>
        /// <returns></returns>
        public bool ReMoveCredentialsInPrincipalPart(long relationSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj = ExecuteUpdate("PrincipalPartRelation.ReMoveCredentialsInPrincipalPart", relationSysNo);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "ReMoveCredentialsInPrincipalPart",
                    DateTime.Now.Ticks - ts,
                    "[Relation SysNo:" + relationSysNo + "]"));

                return obj > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "ReMoveCredentialsInPrincipalPart",
                    DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        public IList<long> GetCredentialsByPrincipalSysNo(long principalSysNo)
        {
            return ExecuteQueryForList<long>("PrincipalPartRelation.GetCredentialsByPrincipalSysNo", principalSysNo);
        }
    }
}
