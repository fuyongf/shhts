﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;

using PinganHouse.SHHTS.AccessControl.Common;
using PinganHouse.SHHTS.AccessControl.Entities;
using PinganHouse.SHHTS.AccessControl.Entities.IDao;
using PinganHouse.SHHTS.AccessControl.Utils;
using PinganHouse.SHHTS.AccessControl.Utils.Log;

namespace PinganHouse.SHHTS.AccessControl.SqlProviderImp.Dao
{
    public class ResourceSqlMapDao : BaseSqlMapDao<Resource>, IResourceDao
    {
        private const string ClassName = "ResourceSqlMapDao";

        /// <summary>
        /// 添加资源
        /// </summary>
        /// <param name="resource">资源对象实体类</param>
        /// <returns></returns>
        public long AddSelf(Resource resource)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var check = IsExists(resource.ResourceName,Convert.ToString(resource.Applications));
                if (check)
                {
                    throw new Exception("The same entity exists");
                }

                ExecuteInsert("Resource.Insert", resource);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "AddSelf",
                    DateTime.Now.Ticks - ts,
                    "[Resource SysNo:" + resource.SysNo + "]"));

                return resource.SysNo;

            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "AddSelf",
                    DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 修改资源
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        public bool UpdateSelf(Resource resource)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var check = IsExistsExcludeSelf(resource.ResourceName,Convert.ToString(resource.Applications), resource.SysNo);

                if (check)
                {
                    throw new Exception("Modify data without legal");
                }

                var obj = ExecuteUpdate("Resource.Update", resource);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "UpdateSelf",
                    DateTime.Now.Ticks - ts,
                    "[Resource SysNo:" + resource.SysNo + "]"));

                return obj > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "UpdateSelf",
                    DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 删除资源
        /// </summary>
        /// <param name="resourceSysNo"></param>
        /// <returns></returns>
        public bool DeleteSelf(long resourceSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj = ExecuteDelete("Resource.Delete", resourceSysNo);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "DeleteSelf",
                    DateTime.Now.Ticks - ts,
                    "[Resource SysNo:" + resourceSysNo + "]"));

                return obj > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "DeleteSelf",
                    DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 检查是否已经存在
        /// </summary>
        /// <param name="resourceName">资源名称</param>
        /// <param name="resourceType">资源类型</param>
        /// <returns></returns>
        public bool IsExists(string resourceName, string resourceType)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var dic = new Dictionary<string, object>
                {
                    {"ResourceName", resourceName},
                    {"ResourceType", resourceType}
                };
                var obj = ExecuteQueryForAnyObject("Resource.IsExists", dic);
                int cnt;
                int.TryParse(Convert.ToString(obj), out cnt);
                Log.Dao(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "IsExists",
                    DateTime.Now.Ticks - ts,
                    "[Resource Name:" + resourceName + "]" +
                    "[Resource Type:" + resourceType + "]"));
                return cnt > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "IsExists",
                    DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 检查是否已经存在(排除自身)
        /// </summary>
        /// <param name="resourceName">资源编号</param>
        /// <param name="resourceType">资源类型</param>
        /// <param name="sysNo">系统编号</param>
        /// <returns></returns>
        public bool IsExistsExcludeSelf(string resourceName, string resourceType, long sysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var dic = new Dictionary<string, object>
                {
                    {"ResourceName", resourceName},
                    {"ResourceType", resourceType},
                    {"SysNo", sysNo}
                };
                var obj = ExecuteQueryForAnyObject("Resource.IsExistsExcludeSelf", dic);
                int cnt;
                int.TryParse(Convert.ToString(obj), out cnt);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "IsExistsExcludeSelf",
                    DateTime.Now.Ticks - ts,
                    "[Resource SysNo:" + sysNo + "]" +
                    "[Resource Name:" + resourceName + "]" +
                    "[Resource Type:" + resourceType + "]"));
                return cnt > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "IsExistsExcludeSelf",
                    DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 获取资源对象
        /// </summary>
        /// <param name="resourceSysNo">资源系统编号</param>
        /// <returns></returns>
        public Resource GetObject(long resourceSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj = ExecuteQueryForObject("Resource.GetObject", resourceSysNo);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "GetObject",
                    DateTime.Now.Ticks - ts,
                    "[Resource SysNo:" + resourceSysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "GetObject",
                    DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 获取资源对象
        /// </summary>
        /// <param name="resourceNo"></param>
        /// <returns></returns>
        public Resource GetObjectByResourceNo(string resourceNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj = ExecuteQueryForObject("Resource.GetObjectByResourceNo", resourceNo);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "GetObjectByResourceNo",
                    DateTime.Now.Ticks - ts,
                    "[Resource No:" + resourceNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "GetObjectByResourceNo",
                    DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 获取资源列表
        /// </summary>
        /// <param name="pageSize">页显示数</param>
        /// <param name="pageIndex">显示页数</param>
        /// <param name="filter">过滤条件</param>
        /// <param name="applications">应用程序域</param>
        /// <param name="count">总数</param>
        /// <returns>资源信息列表</returns>
        public DataTable GetResourceTable(int pageSize, int pageIndex, string filter, string applications, out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var dic = new Dictionary<string, object>
                {
                    {"PageSize", pageSize},
                    {"PageIndex", pageIndex},
                    {"Applications",applications},
                    {"Filter", filter}
                };

                var dataset = ExecuteQueryWithDataSet("Resource.GetResourceTable", dic);

                var dataTable = dataset.Tables[0];

                int.TryParse(Convert.ToString(dataset.Tables[1].Rows[0][0]), out count);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "GetResourceTable",
                    DateTime.Now.Ticks - ts,
                    "[Resource PageIndex:" + pageIndex + "]" +
                    "[Resource PageSize:" + pageSize + "]" +
                    "[Resource Filter:" + filter + "]"));

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "GetObject",
                    DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 获取所属某个证书的资源列表
        /// </summary>
        /// <param name="credentialsSysNo">证书系统编号</param>
        /// <param name="page"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public Hashtable GetResourceListTableByCredentialsSysNo(long credentialsSysNo, int page, int pageIndex)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var hashtable = new Hashtable
                {
                    {"Count", 0},
                    {"Data", null}
                };

                var dic = new Dictionary<string, object>
                {
                    {"CredentialsSysNo", credentialsSysNo},
                    {"Page", page},
                    {"PageIndex", pageIndex}
                };

                var dataset = ExecuteQueryWithDataSet("ResourceRelation.GetResourceListTableByCredentialsSysNo", dic);
                hashtable["Count"] = dataset.Tables[0].Rows[0][0];
                hashtable["Data"] = dataset.Tables[1];

                Log.Dao(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "GetListResourceTableByCredentialsSysNo",
                    DateTime.Now.Ticks - ts,
                    "[Resource PageIndex:" + pageIndex + "]" +
                    "[Resource Page:" + page + "]"));

                return hashtable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "GetObject",
                    DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 给资源添加证书
        /// </summary>
        /// <param name="resource">资源对象</param>
        /// <param name="credentialsList">证书编号集合</param>
        /// <returns></returns>
        public bool AddCredentialsToResource(Resource resource, List<Credentials> credentialsList)
        {
            var ts = DateTime.Now.Ticks;
            var sqlMap = GetLocalSqlMap();
            try
            {
                if (credentialsList == null)
                {
                    throw new Exception("Parameters can not be empty or Null ");
                }
                sqlMap.BeginTransaction();

                foreach (var credentials in credentialsList)
                {
                    var dic = new Dictionary<string, object>
                    {
                        {"Resource", resource},
                        {"CredentialsSysNo", credentials.SysNo}
                    };
                    //检查是否已经存在
                    var cnt = sqlMap.QueryForObject<int>("ResourceRelation.IsExistsCredentialsInResource", dic);
                    if (cnt > 0) continue;
                    dic.Add("SysNo", Sequence.Get());
                    //插入关联
                    sqlMap.Insert("ResourceRelation.AddCredentialsToResource", dic);
                }

                sqlMap.CommitTransaction();

                return true;
            }
            catch (Exception e)
            {

                sqlMap.RollBackTransaction();

                Log.Error(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "AddCredentialsToResource",
                    DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 移除资源上的证书
        /// </summary>
        /// <param name="relationSysNo">证书资源绑定关系表sysNo</param>
        /// <returns></returns>
        public bool ReMoveCredentialsInResource(long relationSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj = ExecuteUpdate("ResourceRelation.ReMoveCredentialsInResource", relationSysNo);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "ReMoveCredentialsInResource",
                    DateTime.Now.Ticks - ts,
                    "[Relation SysNo:" + relationSysNo + "]"));

                return obj > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "ReMoveCredentialsInResource",
                    DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 为证书设置访问资源的访问规则
        /// </summary>
        /// <param name="dicLimitsRules">对应关系表SysNo,以及访问规则</param>
        /// <returns>T:全部设置成功 F:设置发生错误</returns>
        public bool SetCredentialsRulesListToResource(Dictionary<long, Common.Enum.LimitsRules> dicLimitsRules)
        {
            var ts = DateTime.Now.Ticks;
            var sqlMap = GetLocalSqlMap();
            try
            {
                sqlMap.BeginTransaction();

                foreach (var item in dicLimitsRules)
                {
                    var dic = new Dictionary<string, object>
                    {
                        {"RelationSysNo", item.Key},
                        {"LimitsRules", item.Value}
                    };
                    sqlMap.Update("ResourceRelation.SetLimitsRules", dic);
                }

                sqlMap.CommitTransaction();

                return true;
            }
            catch (Exception e)
            {
                sqlMap.RollBackTransaction();

                Log.Error(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "SetCredentialsRulesListToResource",
                    DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 获取所属某个证书的所有资源列表
        /// </summary>
        /// <param name="credentialsSysNo">证书系统编号</param>
        /// <returns></returns>
        public DataTable GetResourceListByCredentialsSysNo(long credentialsSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                DataTable res =
                    ExecuteQueryWithDataSet("ResourceRelation.GetResourceListByCredentialsSysNo", credentialsSysNo).Tables[0];

                Log.Dao(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "GetResourceListByCredentialsSysNo",
                    DateTime.Now.Ticks - ts,
                    "[Credentials SysNo:" + credentialsSysNo + "]"));

                return res;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                    ClassName,
                    "GetResourceListByCredentialsSysNo",
                    DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        public DataRowCollection GetResoureRulesByCredentialsSysNo(List<long> credentialsSysNo)
        {
            var result = ExecuteQueryWithDataSet("ResourceRelation.GetResoureRulesByCredentialsSysNo", credentialsSysNo);
            if (result != null && result.Tables.Count > 0)
                return result.Tables[0].Rows;
            return null;
        }
    }
}
