﻿using System;
using System.Collections.Generic;
using System.Data;

using PinganHouse.SHHTS.AccessControl.Common;
using PinganHouse.SHHTS.AccessControl.Entities;
using PinganHouse.SHHTS.AccessControl.Entities.IDao;
using PinganHouse.SHHTS.AccessControl.Utils;
using PinganHouse.SHHTS.AccessControl.Utils.Log;

namespace PinganHouse.SHHTS.AccessControl.SqlProviderImp.Dao
{
    public class CredentialsSqlMapDao : BaseSqlMapDao<Credentials>, ICredentialsDao
    {
        private const string ClassName = "CredentialsSqlMapDao";

        /// <summary>
        /// 添加证书
        /// </summary>
        /// <param name="credentials"></param>
        /// <returns></returns>
        public long AddSelf(Credentials credentials)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var check = IsExists(credentials.CredentialsName,Convert.ToString(credentials.Applications));
                if (check)
                {
                    throw new Exception("The same entity exists");
                }

                ExecuteInsert("Credentials.Insert", credentials);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "AddSelf",
                                DateTime.Now.Ticks - ts,
                                "[Credentials SysNo:" + credentials.SysNo + "]"));

                return credentials.SysNo;

            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "AddSelf",
                                DateTime.Now.Ticks - ts, e));                
                throw;
            }
        }

        /// <summary>
        /// 修改证书
        /// </summary>
        /// <param name="credentials"></param>
        /// <returns></returns>
        public bool UpdateSelf(Credentials credentials)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var check = IsExistsExcludeSelf(credentials.CredentialsName,Convert.ToString(credentials.Applications), credentials.SysNo);
                if (check)
                {
                    throw new Exception("Modify data without legal");
                }


                var obj = ExecuteUpdate("Credentials.Update", credentials);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "UpdateSelf",
                                DateTime.Now.Ticks - ts,
                                "[Credentials SysNo:" + credentials.SysNo + "]"));

                return obj > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "UpdateSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 删除证书
        /// </summary>
        /// <param name="credentialsNo"></param>
        /// <returns></returns>
        public bool DeleteSelf(long credentialsNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj = ExecuteDelete("Credentials.Delete", credentialsNo);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "DeleteSelf",
                                DateTime.Now.Ticks - ts,
                                "[Credentials SysNo:" + credentialsNo + "]"));

                return obj > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "DeleteSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 检查证书是否存在
        /// </summary>
        /// <param name="name"></param>
        /// <param name="applications"></param>
        /// <returns></returns>
        public bool IsExists(string name, string applications)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var dic = new Dictionary<string, object>
                {
                    {"CredentialsName", name},
                    {"CredentialsType", applications}
                };
                var obj = ExecuteQueryForAnyObject("Credentials.IsExists", dic);

                int cnt;

                int.TryParse(Convert.ToString(obj), out cnt);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "IsExists",
                                DateTime.Now.Ticks - ts,
                                "[Resource Name:" + name + "]" +
                                "[Resource Type:" + applications + "]"));

                return cnt > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "IsExists",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 检查证书是否存在
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <param name="sysNo">系统编号</param>
        /// <returns></returns>
        public bool IsExistsExcludeSelf(string name, string type, long sysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var dic = new Dictionary<string, object>
                {
                    {"CredentialsName", name},
                    {"CredentialsType", type},
                    {"SysNo", sysNo}
                };
                var obj = ExecuteQueryForAnyObject("Credentials.IsExistsExcludeSelf", dic);

                int cnt;

                int.TryParse(Convert.ToString(obj), out cnt);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "IsExistsExcludeSelf",
                                DateTime.Now.Ticks - ts,
                                "[Resource SysNo:" + sysNo + "]" +
                                "[Resource Name:" + name + "]" +
                                "[Resource Type:" + type + "]"));

                return cnt > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "IsExistsExcludeSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 删除前的检查
        /// </summary>
        /// <param name="credentialsSysNo"></param>
        /// <returns></returns>
        public bool CheckBeforeDel(long credentialsSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var obj = ExecuteQueryForAnyObject("Credentials.CheckBeforeDel", credentialsSysNo);

                int cnt;

                int.TryParse(Convert.ToString(obj), out cnt);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "CheckBeforeDel",
                                DateTime.Now.Ticks - ts,
                                "[Credentials SysNo:" + credentialsSysNo + "]"));

                return cnt > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "CheckBeforeDel",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 获取证书
        /// </summary>
        /// <param name="credentialsSysNo"></param>
        /// <returns></returns>
        public Credentials GetObject(long credentialsSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj = ExecuteQueryForObject("Credentials.GetObject", credentialsSysNo);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetObject",
                                DateTime.Now.Ticks - ts,
                                "[Credentials SysNo:" + credentialsSysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetObject",
                                DateTime.Now.Ticks - ts, e));                
                throw;
            }
        }

        /// <summary>
        /// 根据证书编号获取证书
        /// </summary>
        /// <param name="credentialsNo"></param>
        /// <returns></returns>
        public Credentials GetCredentialsByNo(string credentialsNo) 
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj = ExecuteQueryForObject("Credentials.GetCredentialsByNo", credentialsNo);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsByNo",
                                DateTime.Now.Ticks - ts,
                                "[credentialsNo:" + credentialsNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsByNo",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 获取证书列表
        /// </summary>
        /// <param name="pageSize">页显示数</param>
        /// <param name="pageIndex">显示页数</param>
        /// <param name="filter">过滤条件</param>
        /// <param name="applications">应用程序域</param>
        /// <param name="count">总数</param>
        /// <returns>证书信息列表</returns>
        public DataTable GetCredentialsTable(int pageSize, int pageIndex, string filter,string applications, out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var dic = new Dictionary<string, object>
                {
                    {"PageSize", pageSize},
                    {"Filter", filter},
                    {"Applications", applications},
                    {"PageIndex", pageIndex}
                };

                var dataset = ExecuteQueryWithDataSet("Credentials.GetCredentialsTable", dic);
                //赋值--总记录数
                int.TryParse(Convert.ToString(dataset.Tables[1].Rows[0][0]),out count);
                //数据
                var dataTable = dataset.Tables[0];

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsTable",
                                DateTime.Now.Ticks - ts,
                                "[Credentials PageIndex:" + pageIndex + "]" +
                                "[Credentials PageSize:" + pageSize + "]"));

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsTable",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 查询所属某个资源的证书列表
        /// </summary>
        /// <param name="credentialsSysNoList">证书编号集合</param>
        /// <returns>返回DataTable</returns>
        public List<Credentials> GetCredentialsListByCredentialsSysNoList(List<long> credentialsSysNoList)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                //获取关系表集合
                var credentialsList = ExecuteQueryForList("Credentials.GetCredentialsListBySysNoList", credentialsSysNoList.ToArray()) as List<Credentials>;

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsListByCredentialsSysNoList",
                                DateTime.Now.Ticks - ts,
                                "[CredentialsSysNoList Count:" + credentialsSysNoList.Count + "]"));

                return credentialsList;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsListByCredentialsSysNoList",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 查询所属某个资源的证书列表
        /// </summary>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="resource">资源编号</param>
        /// <param name="count"></param>
        /// <returns>返回DataTable</returns>
        public DataTable GetCredentialsTableListInResource(int page, int rows, string resource, out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                //获取关系表集合
                var dataSet = ExecuteQueryWithDataSet("ResourceRelation.GetCredentialsTableListInResource", resource);

                var dataTable = dataSet.Tables[0];

                int.TryParse(Convert.ToString(dataSet.Tables[1].Rows[0][0]),out count);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsTableListInResource",
                                DateTime.Now.Ticks - ts,
                                "[Resource No:" + resource + "]"));

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsTableListInResource",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 查询资源所有绑定证书集合
        /// </summary>
        /// <returns></returns>
        public DataTable GetCredentialsTableListInResource(string resourceNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                //获取关系表集合
                var dataSet = ExecuteQueryWithDataSet("ResourceRelation.GetAllCredentialsTableListInResource", resourceNo);

                var dataTable = dataSet.Tables[0];

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsTableListInResource",
                                DateTime.Now.Ticks - ts,
                                "[ select all credentials in resource"));

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsTableListInResource",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 查询所属某个主体持有的证书列表
        /// </summary>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="principalPart">主体编号</param>
        /// <param name="count"></param>
        /// <returns>返回DataTable</returns>
        public DataTable GetCredentialsTableListInPrincipalPart(int page, int rows, string principalPart, out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                //获取关系表集合
                var dataSet = ExecuteQueryWithDataSet("PrincipalPartRelation.GetCredentialsTableListInPrincipalPart", principalPart);

                var dataTable = dataSet.Tables[0];

                int.TryParse(Convert.ToString(dataSet.Tables[1].Rows[0][0]), out count);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsListInPrincipalPart",
                                DateTime.Now.Ticks - ts,
                                "[Resource No:" + principalPart + "]"));

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsListInPrincipalPart",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 查询主体持有的所有证书(仅返回证书系统编号集合)
        /// </summary>
        /// <returns></returns>
        public DataTable GetCredentialsTableListInPrincipalPart(string principalPartNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                //获取关系表集合
                var dataSet = ExecuteQueryWithDataSet("PrincipalPartRelation.GetAllCredentialsTableListInPrincipalPart", principalPartNo);

                var dataTable = dataSet.Tables[0];

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsListInPrincipalPart",
                                DateTime.Now.Ticks - ts,
                                "[select all credentialsList in principalPart]"));

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsListInPrincipalPart",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 查询未绑定某个资源的证书列表
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="resource">资源编号</param>
        /// <param name="applications">应用域</param>
        /// <param name="count"></param>
        /// <param name="filter">过滤</param>
        /// <returns>返回DataTable</returns>
        public DataTable GetCredentialsTableListOutResource(int pageSize, int pageIndex, string resource, string filter, string applications, out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var dic = new Dictionary<string, object>
                {
                    {"PageSize", pageSize},
                    {"PageIndex", pageIndex},
                    {"Filter", filter},
                    {"Applications", applications},
                    {"ResourceSysNo", resource}
                };

                //获取关系表集合
                var dataSet = ExecuteQueryWithDataSet("ResourceRelation.GetCredentialsTableListOutResource", dic);

                var dataTable = dataSet.Tables[0];

                int.TryParse(Convert.ToString(dataSet.Tables[1].Rows[0][0]), out count);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsTableListOutResource",
                                DateTime.Now.Ticks - ts,
                                "[Resource No:" + resource + "]"));

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsTableListOutResource",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 查询未绑定某个主体的证书列表
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="principalPart">资源编号</param>
        /// <param name="applications">应用域</param>
        /// <param name="count"></param>
        /// <param name="filter">过滤</param>
        /// <returns>返回DataTable</returns>
        public DataTable GetCredentialsTableListOutPrincipalPart(int pageSize, int pageIndex, string principalPart,string filter, string applications, out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var dic = new Dictionary<string, object>
                {
                    {"PageSize", pageSize},
                    {"PageIndex", pageIndex},
                    {"Filter", filter},
                    {"Applications", applications},
                    {"PrincipalPartSysNo", principalPart}
                };

                //获取关系表集合
                var dataSet = ExecuteQueryWithDataSet("PrincipalPartRelation.GetCredentialsTableListOutPrincipalPart", dic);

                var dataTable = dataSet.Tables[0];

                int.TryParse(Convert.ToString(dataSet.Tables[1].Rows[0][0]), out count);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsTableListOutPrincipalPart",
                                DateTime.Now.Ticks - ts,
                                "[PrincipalPart sysNo:" + principalPart + "]"));

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsTableListOutPrincipalPart",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 获取资源和主体同时用户的证书集合
        /// </summary>
        /// <param name="resourceSysNo">资源系统编号</param>
        /// <param name="principalPartSysNoList">主体系统编号</param>
        /// <returns>证书集合</returns>
        public DataTable GetCredentialsTableListInResourceAndPrincipalPart(long resourceSysNo, List<long> principalPartSysNoList)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var principalPartSysNoString = string.Join(",", principalPartSysNoList.ConvertAll(Convert.ToString).ToArray());
                var dic = new Dictionary<string, object>
                {
                    {"ResourceSysNo", resourceSysNo},
                    {"PrincipalPartSysNoList", principalPartSysNoString}
                };

                //获取关系表集合
                var dataSet = ExecuteQueryWithDataSet("ResourceRelation.GetCredentialsTableListInResourceAndPrincipalPart", dic);

                var dataTable = dataSet.Tables[0];


                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsTableListInResourceAndPrincipalPart",
                                DateTime.Now.Ticks - ts,
                                "[Resource sysNo:" + resourceSysNo + "]"));

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsTableListInResourceAndPrincipalPart",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }
    }
}
