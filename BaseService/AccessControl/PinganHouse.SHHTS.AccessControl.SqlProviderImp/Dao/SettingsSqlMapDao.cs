﻿using System;
using System.Collections.Generic;

using IBatisNet.DataMapper.TypeHandlers;

using PinganHouse.SHHTS.AccessControl.Entities.IDao;
using PinganHouse.SHHTS.AccessControl.Entities;
using PinganHouse.SHHTS.AccessControl.Common;
using PinganHouse.SHHTS.AccessControl.Utils;
using PinganHouse.SHHTS.AccessControl.Utils.Log;

namespace PinganHouse.SHHTS.AccessControl.SqlProviderImp.Dao
{
    public class SettingsSqlMapDao : EntityBaseMapDao<Settings>, ISettingsDao
    {
        private const string ClassName = "SettingsSqlMapDao";

        public IList<Settings> GetKeyValueList()
        {
            long ts = DateTime.Now.Ticks;
            try
            {
                var lst = ExecuteQueryForList("Settings.GetAll", null);
                Log.Dao(string.Format(Configs.ServerLogFormat, ClassName, "GetKeyValueList", DateTime.Now.Ticks - ts, "lst.Count=" + lst.Count));
                return lst;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat, ClassName, "GetKeyValueList", DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        public int DeleteAll(long[] sysNoArr)
        {
            if (sysNoArr == null || sysNoArr.Length < 1)
                throw new ArgumentNullException("sysNoArr", "删除的主键数组不能为null");
            long ts = DateTime.Now.Ticks;
            try
            {
                var count = ExecuteDelete("Settings.DeleteArr", sysNoArr);
                Log.Dao(string.Format(Configs.ServerLogFormat, ClassName, "DeleteAll",
                               DateTime.Now.Ticks - ts, sysNoArr));
                return count;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat, ClassName, "DeleteAll", DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        public Settings GetObjectByKeyAndParent(string key, Settings parent)
        {
            long ts = DateTime.Now.Ticks;
            try
            {
                var dictionary = new Dictionary<string, object>
                                     {
                                         {"key", key}, 
                                         {"parentSysNo", parent.SysNo}
                                     };

                var obj = ExecuteQueryForObject("Settings.GetObjectByKeyAndParent", dictionary);
                Log.Dao(string.Format(Configs.ServerLogFormat, ClassName, "GetObjectByKeyAndParent", DateTime.Now.Ticks - ts, "key"));
                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat, ClassName, "GetObjectByKeyAndParent", DateTime.Now.Ticks - ts, e));
                throw;
            }
        }


        public List<Settings> SearchSettingsSonGroup(long parentID)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("ParentID", parentID);
                List<Settings> rtn = ExecuteQueryForList<Settings>("Settings.SearchSettingsSonGroup", parameters) as List<Settings>;
                Log.Dao(string.Format(Configs.ServerLogFormat, ClassName, "SearchSettingsSonGroup",
                                      DateTime.Now.Ticks - ts, rtn));
                return rtn;
            }
            catch (Exception ex)
            {
                Log.Error(string.Format(Configs.ServerLogFormat, ClassName, "SearchSettingsSonGroup",
                                      DateTime.Now.Ticks - ts, ex.ToString()));
                throw;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sysNoList"></param>
        /// <returns></returns>
        public int DeleteMultiSettings(List<long> sysNoList)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                int rtn = ExecuteDelete("Settings.DeleteMultiSettingsFamily", sysNoList);
                Log.Dao(string.Format(Configs.ServerLogFormat, ClassName, "DeleteMultiSettings",
                                      DateTime.Now.Ticks - ts, rtn));
                return rtn;
            }
            catch (Exception ex)
            {
                Log.Error(string.Format(Configs.ServerLogFormat, ClassName, "DeleteMultiSettings",
                                      DateTime.Now.Ticks - ts, ex.ToString()));
                throw;
            }
        }

        public int UpdateSettings(List<Settings> settingsList, List<long> delSysNoList)
        {
            var ts = DateTime.Now.Ticks;
            var sqlMapper = GetLocalSqlMap();
            try
            {
                if ((settingsList == null || settingsList.Count == 0)&& 
                    (delSysNoList==null||delSysNoList.Count == 0))
                {
                    throw new Exception("参数不正确");
                }
                if (settingsList != null && settingsList.Count > 0)
                {
                    long[] sysNoList = Sequence.Get(settingsList.Count);
                    for (int index = 0; index < settingsList.Count; index++)
                    {
                        settingsList[index].SysNo = sysNoList[index];
                    }
                }
                sqlMapper.BeginTransaction();
                if (delSysNoList != null && delSysNoList.Count > 0)
                {
                    int rtnDelete = sqlMapper.Delete("Settings.DeleteMultiSettingsSon", delSysNoList);
                    if (rtnDelete <= 0)
                    {
                        throw new Exception(
                            string.Format("删除失败{0},{1}", "DeleteMultiSettingsSon",delSysNoList.Count));
                    }
                }
                if (settingsList != null && settingsList.Count > 0)
                {
                    try
                    {
                        sqlMapper.Insert("Settings.InsertMulti", settingsList);
                    }
                    catch (Exception)
                    {
                        throw new Exception(
                             string.Format("写入失败{0},{1}", "InsertMulti", settingsList.Count));
                    }
                }
                sqlMapper.CommitTransaction();
                Log.Dao(string.Format(Configs.ServerLogFormat, ClassName, "UpdateSettings",
                                      DateTime.Now.Ticks - ts, settingsList.Count));
                return 1;
            }
            catch (Exception ex)
            {
                sqlMapper.RollBackTransaction();
                Log.Error(string.Format(Configs.ServerLogFormat, ClassName, "UpdateSettings",
                                      DateTime.Now.Ticks - ts, ex.ToString()));
                throw;
            }
        }

        public int InsertSettings(Settings settingsDto, List<Settings> settingsDtos)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                if (settingsDto == null || settingsDtos == null || settingsDtos.Count == 0)
                {
                    throw new Exception("参数不正确");
                }
                long[] sysNoList = Sequence.Get(settingsDtos.Count + 1);
                settingsDto.SysNo = sysNoList[settingsDtos.Count];
                for (int index = 0; index < settingsDtos.Count; index++)
                {
                    var st = settingsDtos[index];
                    st.ParentSysNo = settingsDto.SysNo;
                    st.SysNo = sysNoList[index];
                }
                settingsDtos.Add(settingsDto);
                ExecuteInsert("Settings.InsertMulti", settingsDtos);
                Log.Dao(string.Format(Configs.ServerLogFormat, ClassName, "InsertSettings",
                                      DateTime.Now.Ticks - ts, settingsDtos.Count));
                return 1;
            }
            catch (Exception ex)
            {
                Log.Error(string.Format(Configs.ServerLogFormat, ClassName, "InsertSettings",
                                      DateTime.Now.Ticks - ts, ex.ToString()));
                throw;
            }
        }

        public Settings GetRootObject(string rootKey)
        {
            long ts = DateTime.Now.Ticks;
            try
            {
                var obj = ExecuteQueryForObject("Settings.GetRootObject", rootKey);
                Log.Dao(string.Format(Configs.ServerLogFormat, ClassName, "GetRootObject", DateTime.Now.Ticks - ts, "key"));
                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat, ClassName, "GetRootObject", DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        //查询全国地址
        public List<Settings> GetAllNationalAddressList(long parentSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var filingWhereText = ExecuteQueryForList("Settings.GetAllNationalAddress", parentSysNo) as List<Settings>;

                Log.Dao(string.Format(Configs.ServerLogFormat,
                        ClassName,
                        "GetAllNationalAddressList",
                        DateTime.Now.Ticks - ts,
                        string.Format("[parentSysNo:{0};", parentSysNo)));

                return filingWhereText;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                        ClassName,
                        "GetAllNationalAddressList",
                        DateTime.Now.Ticks - ts, e));
                throw;
            }
        }


        public IList<Settings> GetAllNationalAddressList()
        {
            long ts = DateTime.Now.Ticks;
            try
            {
                var lst = ExecuteQueryForList("Settings.GetAllNationalAddressList", null);
                Log.Dao(string.Format(Configs.ServerLogFormat, ClassName, "GetAllNationalAddressList", DateTime.Now.Ticks - ts,
                    "lst.Count=" + lst.Count));
                return lst;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat, ClassName, "GetAllNationalAddressList", DateTime.Now.Ticks - ts,e));
                throw;
            }
        }
    }

    public class NullTypeHandler : ITypeHandlerCallback
    {
        public void SetParameter(IParameterSetter setter, object parameter)
        {
            setter.Value = parameter == null
                ? 0
                : ((Settings)parameter).SysNo;
        }

        public object GetResult(IResultGetter getter)
        {
            // 用于将从数据库读取的值转换成.NET中的值
            //decimal v1 = Convert.ToDecimal(getter.Value);
            //double v2 = (double)v1;
            //return v2;

            if (Convert.ToInt32(getter.Value) == 0)
            {
                return null;
            }
            return getter;
        }

        public object ValueOf(string s)
        {
            // 这个函数用于将nullValue值翻译成要比较的null值
            // 如果没有，则推荐返回字符串s
            return s;
        }

        public object NullValue { get; private set; }
    }
}
