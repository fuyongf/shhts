﻿using System;

using PinganHouse.SHHTS.AccessControl.Common;
using PinganHouse.SHHTS.AccessControl.Entities;
using PinganHouse.SHHTS.AccessControl.Entities.IDao;
using PinganHouse.SHHTS.AccessControl.Utils.Log;

namespace PinganHouse.SHHTS.AccessControl.SqlProviderImp.Dao
{
    public class LogSqlMapDao : BaseSqlMapDao<AcLog>, IAcLogDao
    {
        private const string ClassName = "LogSqlMapDao";
        /// <summary>
        /// 插入日志
        /// </summary>
        /// <param name="log">日志</param>
        /// <returns></returns>
        public bool Insert(AcLog log)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                ExecuteInsert("AccessLogs.Insert", log);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "AddSelf",
                                DateTime.Now.Ticks - ts,
                                "[Applications SysNo:" + log.SysNo + "]"));

                return true;

            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "AddSelf",
                                DateTime.Now.Ticks - ts, e));
                return false;
            }
        }
    }
}
