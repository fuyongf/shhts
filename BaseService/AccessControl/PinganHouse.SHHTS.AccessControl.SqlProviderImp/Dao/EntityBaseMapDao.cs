﻿using System;
using System.Collections.Generic;

using PinganHouse.SHHTS.AccessControl.Entities.IDao;

namespace PinganHouse.SHHTS.AccessControl.SqlProviderImp.Dao
{
    public class EntityBaseMapDao<T> : BaseSqlMapDao<T>, IBaseDao<T> where T : class
    {
        private readonly string _className = typeof(T).Name;

        /// <summary>
        /// 根据给定的对象进行插入
        /// </summary>
        /// <returns>插入的对象所返回的主键</returns>
        public virtual object Insert(T t)
        {
            if (t == null)
                throw new ArgumentNullException("t", "插入的对象不能为null");

            return ExecuteInsert(_className + ".Insert", t);
        }

        /// <summary>
        /// 根据给定的对象进行更新
        /// </summary>
        /// <param name="t">对象</param>
        /// <returns>更新所影响的行数</returns>
        public virtual int Update(T t)
        {
            if (t == null)
                throw new ArgumentNullException("t", "更新的对象不能为Null");

            return ExecuteUpdate(_className + ".UpdateObject", t);
        }

        /// <summary>
        /// 根据给定的主键唯一值进行物理删除
        /// </summary>
        /// <param name="o">主键/唯一值</param>
        /// <returns>该操作影响的行数</returns>
        public virtual int Delete(object o)
        {
            if (o == null)
                throw new ArgumentNullException("o", "删除的主键对象不能为Null");

            return ExecuteDelete(_className + ".DeleteObject", o);
        }

        /// <summary>
        /// 根据给定的主键唯一值查询对象
        /// </summary>
        /// <param name="o">主键/唯一值</param>
        /// <returns>查询到的对象</returns>
        public virtual T GetObject(object o)
        {
            if (o == null)
                throw new ArgumentNullException("o", "获取的主键唯一标识不能为Null");

            return ExecuteQueryForObject(_className + ".GetObject", o);
        }

        /// <summary>
        /// 列出所有数据
        /// </summary>
        /// <returns></returns>
        public virtual IList<T> GetAll()
        {
            return ExecuteQueryForList(_className + ".GetAll", null);
        }

        /// <summary>
        /// 分页获取
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public virtual IList<T> GetAll(long pageIndex, long pageSize)
        {
            var parameters = new Dictionary<string, object>
                                 {
                                     {"PageIndex", pageIndex},
                                     {"PageSize", pageSize}
                                 };
            return ExecuteQueryForList(_className + ".GetByDictionary", parameters);
        }
    }
}
