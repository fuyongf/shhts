﻿using System;
using System.Collections.Generic;

using PinganHouse.SHHTS.AccessControl.Common;
using PinganHouse.SHHTS.AccessControl.Entities;
using PinganHouse.SHHTS.AccessControl.Entities.IDao;
using PinganHouse.SHHTS.AccessControl.Utils;
using PinganHouse.SHHTS.AccessControl.Utils.Log;

namespace PinganHouse.SHHTS.AccessControl.SqlProviderImp.Dao
{
    public class ApplicationsSqlMapDao : BaseSqlMapDao<Applications>, IApplicationsDao
    {
        private const string ClassName = "ApplicationsSqlMapDao";

        /// <summary>
        /// 获取应用程序列表
        /// </summary>
        /// <returns></returns>
        public List<Applications> GetApplicationsList(long userSysNo,bool isAdmin)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                List<Applications> applicationsList;

                if (isAdmin)
                {
                    applicationsList = ExecuteQueryForList("Applications.GetAllApplicationsList", null) as List<Applications>;
                }
                else
                {
                    applicationsList = ExecuteQueryForList("Applications.GetApplicationsList", userSysNo) as List<Applications>;
                }

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetApplicationsList",
                                DateTime.Now.Ticks - ts,
                                "Method GetApplicationsList No Paras"));

                return applicationsList;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsTable",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 获取应用成
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        public Applications GetObject(long sysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var applicationsList = ExecuteQueryForObject("Applications.GetObject", sysNo);


                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetObject",
                                DateTime.Now.Ticks - ts,
                                "[Applications SysNo :" + sysNo + "]"));

                return applicationsList;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetObject",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 修改应用
        /// </summary>
        /// <param name="applications"></param>
        /// <returns></returns>
        public bool UpdateSelf(Applications applications)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var check = IsExistsExcludeSelf(applications.ApplicationsName, applications.SysNo);
                if (check)
                {
                    throw new Exception("Modify data without legal");
                }


                var obj = ExecuteUpdate("Applications.Update", applications);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "UpdateSelf",
                                DateTime.Now.Ticks - ts,
                                "[Applications SysNo:" + applications.SysNo + "]"));

                return obj > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "UpdateSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 新增应用
        /// </summary>
        /// <param name="applications"></param>
        /// <returns></returns>
        public long AddSelf(Applications applications)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var check = IsExists(applications.ApplicationsName);
                if (check)
                {
                    throw new Exception("The same entity exists");
                }

                ExecuteInsert("Applications.Insert", applications);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "AddSelf",
                                DateTime.Now.Ticks - ts,
                                "[Applications SysNo:" + applications.SysNo + "]"));

                return applications.SysNo;

            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "AddSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 检查应用是否存在
        /// </summary>
        /// <param name="applications"></param>
        /// <returns></returns>
        public bool IsExists(string applications)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj = ExecuteQueryForAnyObject("Applications.IsExists", applications);

                int cnt;

                int.TryParse(Convert.ToString(obj), out cnt);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "IsExists",
                                DateTime.Now.Ticks - ts,
                                "[Applications Name:" + applications + "]"));

                return cnt > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "IsExists",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 检查证书是否存在
        /// </summary>
        /// <param name="applications">应用程序名称</param>
        /// <param name="sysNo">系统编号</param>
        /// <returns></returns>
        public bool IsExistsExcludeSelf(string applications, long sysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var dic = new Dictionary<string, object>
                {
                    {"ApplicationsName", applications},
                    {"SysNo", sysNo}
                };
                var obj = ExecuteQueryForAnyObject("Applications.IsExistsExcludeSelf", dic);

                int cnt;

                int.TryParse(Convert.ToString(obj), out cnt);

                Log.Dao(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "IsExistsExcludeSelf",
                                DateTime.Now.Ticks - ts,
                                "[Applications SysNo:" + sysNo + "]" +
                                "[Applications Name:" + applications + "]"));

                return cnt > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "IsExistsExcludeSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }


        public string GetApplicationSysNo(string appNo)
        {
            var result = ExecuteQueryForAnyObject("Applications.GetApplicationSysNo", appNo);
            if (result != null)
                return result.ToString();
            return null;
        }
    }
}
