﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Configuration;
using System.Threading.Tasks;

using PinganHouse.SHHTS.AccessControl.Common.Enum;
using PinganHouse.SHHTS.AccessControl.Entities;
using PinganHouse.SHHTS.AccessControl.BLL;

using PinganHouse.SHHTS.AccessControl.ServiceContracts;
using PinganHouse.SHHTS.AccessControl.ServiceContracts.DTO;
using PinganHouse.SHHTS.AccessControl.Utils.Log;


namespace PinganHouse.SHHTS.AccessControl.ServiceImpl
{
    public class PermissionService : IPermissionService
    {
        /// <summary>
        /// 验证指定角色是否拥有指定资源访问权限
        /// </summary>
        /// <param name="roleNo">角色(证书)编号</param>
        /// <param name="resourceNo">资源编号</param>
        /// <returns></returns>
        public bool ValidateAuthorization(List<string> principalNos, string resourceNo, string appNo)
        {
            var rData = this.GetResourcesByPrincipalNos(principalNos, appNo);
            if (resourceNo == null || rData==null|| rData.Count == 0)
                return false;
            var resources = new List<ResourceInfo>();
            Parallel.ForEach(rData.Values, rl => {
                var rs = rl.Where(r => r.ResourceNo == resourceNo);
                resources.AddRange(rs);
            });
            //拒绝优先，存在拒绝规则则拒绝
            if (resources.Any(r => r.LimitsRules == -1))
                return false;
            //存在允许则允许，否则全默认即拒绝
            return resources.Any(r => r.Authorized);
        }

        /// <summary>
        /// 根据角色编号获取此角色下所有资源信息
        /// </summary>
        /// <param name="roleNo">角色(证书)编号</param>
        /// <returns></returns>
        public IList<ResourceInfo> GetResourcesByPrincipalNo(string principalNo, string appNo)
        {
            if (string.IsNullOrEmpty(principalNo) || string.IsNullOrWhiteSpace(appNo))
                return null;
            var ps = new List<string>();
            ps.Add(principalNo);
            var result = this.GetResourcesByPrincipalNos(ps, appNo);
            if(result!=null&& result.Count>0)
                return result[principalNo];
            return null;
        }


        /// <summary>
        /// 根据角色编号获取此角色下所有资源信息
        /// </summary>
        /// <param name="roleNo">角色(证书)编号</param>
        /// <returns></returns>
        public IList<ResourceInfo> GetResourcesByPrincipalNo(string principalNo, string appNo, ref int cacheMinutes)
        {
            var ps = new List<string>();
            ps.Add(principalNo);
            var result = this.GetResourcesByPrincipalNos(ps, appNo, ref cacheMinutes);
            if (result != null && result.Count > 0)
                return result[principalNo];
            return null;
        }

        /// <summary>
        /// 获取权限系统总开关
        /// </summary>
        /// <returns></returns>
        public bool GetPermissionSwitsh(string appNo, ref int cacheMinutes)
        {
            bool permissionSwitsh = false;
            if (string.IsNullOrWhiteSpace(appNo))
                return permissionSwitsh;
            try
            {
                cacheMinutes = 30;
                var result = Settings.GetMultiKeyValue("system", "permissioSwitch");          
                if(result!=null&& result.ContainsKey(appNo))
                    permissionSwitsh = result[appNo]=="1";
                var cacheTime = Settings.GetMultiKeyValue("system", "permissioSwitchCache");
                if (cacheTime != null && cacheTime.ContainsKey(appNo))
                    int.TryParse(cacheTime[appNo], out cacheMinutes);
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("AppNo:{0},ErrorMes:{1}{2}StackTrace:{3}", appNo, ex.Message, Environment.NewLine, ex.StackTrace));
            }
            return permissionSwitsh;
        }

        /// <summary>
        /// 获取主体资源规则
        /// </summary>
        /// <param name="principalNos"></param>
        /// <returns></returns>
        public Dictionary<string, IList<ResourceInfo>> GetResourcesByPrincipalNos(List<string> principalNos, string appNo)
        {
            try
            {
                if (principalNos == null || principalNos.Count == 0 || string.IsNullOrWhiteSpace(appNo))
                    return null;
                var result = ResourceBll.GetResourcesByPrincipalNos(principalNos, appNo);
                if (result == null || result.Count == 0)
                    return null;
                Dictionary<string, IList<ResourceInfo>> rData = new Dictionary<string, IList<ResourceInfo>>();
                Parallel.ForEach(result, r => {
                    List<ResourceInfo> rl = new List<ResourceInfo>();
                    try
                    {
                        foreach (DataRow resource in r.Value)
                        {
                            int rule = 0;
                            //无法转换
                            //resource["LimitsRules"] as string;
                            int.TryParse(resource["LimitsRules"].ToString(), out rule);
                            var resourceInfo = new ResourceInfo
                            {
                                ResourceNo = resource["ResourceNo"].ToString(),
                                LimitsRules = rule,
                            };
                            rl.Add(resourceInfo);
                        }
                        if (rl.Count > 0)
                            rData.Add(r.Key, rl);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex);
                    }
                });
                return rData;
            }
            catch (Exception ex)
            { 
                Log.Error(ex);
                throw;
            }
        }


        /// <summary>
        /// 获取主体资源规则
        /// </summary>
        /// <param name="principalNos"></param>
        /// <returns></returns>
        public Dictionary<string, IList<ResourceInfo>> GetResourcesByPrincipalNos(List<string> principalNos, string appNo, ref int cacheMinutes)
        {
            cacheMinutes = 30;
            if (principalNos == null || principalNos.Count == 0 || string.IsNullOrWhiteSpace(appNo))
                return null;
            var result = Settings.GetMultiKeyValue("system", "resourceCache");
            if (result != null && result.ContainsKey(appNo))
                int.TryParse(result[appNo], out cacheMinutes);                
            return this.GetResourcesByPrincipalNos(principalNos, appNo);
        }
    }
}
