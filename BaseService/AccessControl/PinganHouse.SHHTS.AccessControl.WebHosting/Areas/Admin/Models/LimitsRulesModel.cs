﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PinganHouse.SHHTS.AccessControl.Common.Enum;

namespace PinganHouse.SHHTS.AccessControl.WebHosting.Areas.Admin.Models
{
    public class LimitsRulesModel
    {
        /// <summary>
        /// 关系表系统编号
        /// </summary>
        public long SysNo { get; set; }

        /// <summary>
        /// 访问规则
        /// </summary>
        public LimitsRules LimitsRules { get; set; }
    }
}