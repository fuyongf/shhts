﻿using System.Collections.Generic;

namespace PinganHouse.SHHTS.AccessControl.WebHosting.Areas.Admin.Models
{
    public class Menus
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public Menus()
        {
            menus = new List<Menus>();
        }

        /// <summary>
        /// 菜单ID
        /// </summary>
        public string menuid { get; set; }

        /// <summary>
        /// Menu的应用域
        /// </summary>
        public string appid { get; set; }

        /// <summary>
        /// 菜单显示图标
        /// </summary>
        public string icon { get; set; }

        /// <summary>
        /// 菜单名称
        /// </summary>
        public string menuname { get; set; }

        /// <summary>
        /// 菜单连接
        /// </summary>
        public string url { get; set; }

        /// <summary>
        /// 子菜单
        /// </summary>
        public List<Menus> menus { get; set; }
    }
}