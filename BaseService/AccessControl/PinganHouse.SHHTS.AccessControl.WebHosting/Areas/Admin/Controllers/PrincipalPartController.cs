﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Mvc;

using Newtonsoft.Json;

using PinganHouse.SHHTS.AccessControl.BLL;
using PinganHouse.SHHTS.AccessControl.Common;
using PinganHouse.SHHTS.AccessControl.Entities;
using PinganHouse.SHHTS.AccessControl.Utils.Log;

namespace PinganHouse.SHHTS.AccessControl.WebHosting.Areas.Admin.Controllers
{
    public class PrincipalPartController : Controller
    {
        //
        // GET: /PrincipalPart/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Edit()
        {
            return View();
        }

        /// <summary>
        /// 主体+
        /// </summary>
        /// <returns></returns>
        public ActionResult RelationIndex()
        {
            return View();
        }

        /// <summary>
        /// 获取主体
        /// </summary>
        /// <returns></returns>
        public ActionResult GetPrincipalPart()
        {
            try
            {
                var sysNoStr = Request["sysNo"];
                long sysNo;
                long.TryParse(sysNoStr, out sysNo);
                var principalPart = PrincipalPart.GetObject(sysNo) ?? new PrincipalPart();
                //序列化返回数据
                return Json(principalPart);
            }
            catch (Exception e)
            {
                Log.Error(e);
                //序列化返回数据
                return Json(new PrincipalPart());
            }
        }

        /// <summary>
        /// 获取主体列表
        /// </summary>
        /// <returns></returns>
        public ActionResult GetPrincipalPartList()
        {
            try
            {
                var rows = Convert.ToInt32(Request["rows"]);
                var page = Convert.ToInt32(Request["page"]);
                var filter = Convert.ToString(Request["filter"]);

                //应用程序域
                var applications = HttpHelper.CreateInstance().GetApplications(HttpContext);

                //总记录数
                int count;
                //结果集
                var dataTable = PrincipalPart.GetPrincipalPartTable(page, rows, filter,applications, out count);
                //序列化数据
                var jsonData = JsonHelper.FormatJsonForEasyuiDataGrid(count, dataTable);
                //返回
                return Content(jsonData, "text/html;charset=UTF-8");

            }
            catch (Exception e)
            {
                Log.Error(e);
                return Content(JsonHelper.FormatJsonForEasyuiDataGrid(0, new DataTable()), "text/html;charset=UTF-8");
            }
        }

        /// <summary>
        /// 删除主体
        /// </summary>
        /// <returns></returns>
        public ActionResult DelPrincipalPart()
        {
            try
            {
                var sysNoStr = Request["sysNo"];
                long sysNo;
                long.TryParse(sysNoStr, out sysNo);
                //操作结果
                var result = PrincipalPart.DeleteSelf(sysNo);
                if (result)
                {
                    //应用程序域
                    var applications = HttpHelper.CreateInstance().GetApplications(HttpContext);
                    //获取登录信息
                    long empSysNo;
                    var userSysNo = HttpHelper.CreateInstance().GetUser(HttpContext, "SysNo");
                    long.TryParse(userSysNo, out empSysNo);
                    var userName = HttpUtility.UrlDecode(HttpHelper.CreateInstance().GetUser(HttpContext, "Name"));

                    //记录删除资源日志
                    AcLog.Insert(applications, "Del PrincipalPart", "PrincipalPart", Convert.ToString(sysNo), empSysNo, userName, null);
                }
                return Json(result);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Json(false);
            }
        }

        /// <summary>
        /// 保存主体
        /// </summary>
        /// <returns></returns>
        public ActionResult SavePrincipalPart()
        {
            try
            {
                //Json 对象字符串
                var principalPartStr = Request["principalPart"];
                //Json对象
                var principalPart = (PrincipalPart)JsonConvert.DeserializeObject(principalPartStr, typeof(PrincipalPart));
                //应用程序域
                var applicationsCookie = HttpHelper.CreateInstance().GetApplications(HttpContext);
                long applications;
                long.TryParse(applicationsCookie, out applications);
                principalPart.Applications = applications;
                //获取登录信息
                long empSysNo;
                var sysNo = HttpHelper.CreateInstance().GetUser(HttpContext, "SysNo");
                long.TryParse(sysNo, out empSysNo);
                var name = HttpUtility.UrlDecode(HttpHelper.CreateInstance().GetUser(HttpContext, "Name"));
                //操作结果
                bool result;
                if (principalPart.SysNo > 0)
                {
                    var oldPrincipalPart = PrincipalPart.GetObject(principalPart.SysNo);
                    principalPart.ModifyUserName = name;
                    principalPart.ModifyUserSysNo = empSysNo;
                    result = principalPart.UpdateSelf();
                    if (result)
                    {
                        //记录修改资源码日志
                        AcLog.Insert(applicationsCookie, "Update PrincipalPart", "PrincipalPart", JsonConvert.SerializeObject(principalPart), empSysNo, name, JsonConvert.SerializeObject(oldPrincipalPart));
                    }
                }
                else
                {
                    principalPart.CreateUserName = name;
                    principalPart.CreateUserSysNo = empSysNo;
                    result = principalPart.AddSelf() > 0;
                    if (result)
                    {
                        //记录新增资源码日志
                        AcLog.Insert(applicationsCookie, "Add PrincipalPart", "PrincipalPart", JsonConvert.SerializeObject(principalPart), empSysNo, name, null);
                    }
                }
                return Json(result);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Json(false);
            }
        }

        /// <summary>
        /// 移除证书
        /// </summary>
        /// <returns></returns>
        public ActionResult RemoveCredentialsInPrincipalPart()
        {
            try
            {
                var sysNo = Convert.ToInt32(Request["sysNo"]);
                var result = PrincipalPart.ReMoveCredentialsInPrincipalPart(sysNo);
                if (result)
                {
                    //应用程序域
                    var applications = HttpHelper.CreateInstance().GetApplications(HttpContext);
                    //获取登录信息
                    long empSysNo;
                    var userSysNo = HttpHelper.CreateInstance().GetUser(HttpContext, "SysNo");
                    long.TryParse(userSysNo, out empSysNo);
                    var userName = HttpUtility.UrlDecode(HttpHelper.CreateInstance().GetUser(HttpContext, "Name"));

                    //移除证书
                    AcLog.Insert(applications, "RemoveCredentialsInPrincipalPart", "PrincipalPartRelation", Convert.ToString(sysNo), empSysNo, userName, null);
                }
                return Json(result);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Json(false);
            }
        }

        /// <summary>
        /// 给资源添加访问证书
        /// </summary>
        /// <returns></returns>
        public ActionResult AddCredentialsListToPrincipalPart()
        {
            try
            {
                //Json 对象字符串
                var principalPart = Request["principalPart"];
                var credentialsList = Request["credentialsList"];
                //Json对象
                var principalPartEntity = (PrincipalPart)JsonConvert.DeserializeObject(principalPart, typeof(PrincipalPart));
                //Json对象
                var credentialsListEntity = (List<Credentials>)JsonConvert.DeserializeObject(credentialsList, typeof(List<Credentials>));
                var result = PrincipalPart.AddCredentialsToPrincipalPart(principalPartEntity, credentialsListEntity);
                return Json(result);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Json(false);
            }
        }

        /// <summary>
        /// 获取资源证书列表
        /// </summary>
        /// <returns></returns>
        public ActionResult GetCredentialsListInPrincipalPart()
        {
            try
            {
                var rows = Convert.ToInt32(Request["rows"]);
                var page = Convert.ToInt32(Request["page"]);
                var principalPart = Request["principalPart"];
                //总记录数
                int count;
                //结果集
                var dataTable = PrincipalPartBll.GetCredentialsListInPrincipalPart(page, rows, principalPart, out count);
                //序列化数据
                var jsonData = JsonHelper.FormatJsonForEasyuiDataGrid(count, dataTable);
                //返回
                return Content(jsonData, "text/html;charset=UTF-8");

            }
            catch (Exception e)
            {
                Log.Error(e);
                return Content(JsonHelper.FormatJsonForEasyuiDataGrid(0, new DataTable()), "text/html;charset=UTF-8");
            }
        }
    }
}
