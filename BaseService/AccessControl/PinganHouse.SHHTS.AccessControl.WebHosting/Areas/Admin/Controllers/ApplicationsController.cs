﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

using Newtonsoft.Json;

using PinganHouse.SHHTS.AccessControl.Common;
using PinganHouse.SHHTS.AccessControl.Entities;
using PinganHouse.SHHTS.AccessControl.Utils.Log;

namespace PinganHouse.SHHTS.AccessControl.WebHosting.Areas.Admin.Controllers
{
    public class ApplicationsController : Controller
    {
        //
        // GET: /Hello/

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Edit()
        {
            return View();
        }


        public ActionResult ApplicationsRelation()
        {
            return View();
        }

        /// <summary>
        /// 获取应用
        /// </summary>
        /// <returns></returns>
        public ActionResult GetApplications()
        {
            try
            {
                var sysNoStr = Request["sysNo"];
                long sysNo;
                long.TryParse(sysNoStr, out sysNo);
                var resource = Applications.GetObject(sysNo) ?? new Applications();
                //序列化返回数据
                return Json(resource);

            }
            catch (Exception e)
            {
                Log.Error(e);
                //序列化返回数据
                return Json(new Applications());

            }
        }

        /// <summary>
        /// 获取应用程序集合
        /// </summary>
        /// <returns></returns>
        public ActionResult GetApplicationsList()
        {
            try
            {
                long userSysNo;
                long.TryParse(HttpHelper.CreateInstance().GetUser(HttpContext, "SysNo"), out userSysNo);

                bool isAdmin;
                bool.TryParse(HttpHelper.CreateInstance().GetUser(HttpContext, "IsAdmin"), out isAdmin);

                var list = Applications.GetApplicationsList(userSysNo, isAdmin);
                //序列化数据
                var jsonData = JsonHelper.FormatJsonForEasyuiDataGrid(list.Count, list);
                //返回
                return Content(jsonData, "text/html;charset=UTF-8");
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Content(JsonHelper.FormatJsonForEasyuiDataGrid(0,new List<Applications>()), "text/html;charset=UTF-8");
            }
        }

        /// <summary>
        /// 保存应用程序
        /// </summary>
        /// <returns></returns>
        public ActionResult SaveApplications()
        {
            try
            {
                //Json 对象字符串
                var applicationsStr = Request["applications"];

                //Json对象
                var applications = (Applications)JsonConvert.DeserializeObject(applicationsStr, typeof(Applications));

                //操作结果
                bool result;

                if (applications.SysNo > 0)
                {
                    result = applications.UpdateSelf();

                }
                else
                {
                    result = applications.AddSelf() > 0;
                }
                return Json(result);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Json(false);
            }
        }
    }
}
