﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using Newtonsoft.Json;

using PinganHouse.SHHTS.AccessControl.Common;
using PinganHouse.SHHTS.AccessControl.Entities;
using PinganHouse.SHHTS.AccessControl.Utils.Log;

namespace PinganHouse.SHHTS.AccessControl.WebHosting.Areas.Admin.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/
        /// <summary>
        /// 登录页面
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 登陆检查
        /// </summary>
        /// <returns></returns>
        public ActionResult Login()
        {
            try
            {
                var domainName = Request["domainName"];
                var password = Request["password"];

                bool rememberme;
                bool.TryParse(Request["rememberme"], out rememberme);
                var cookie = new HttpCookie("LOGIN")
                {
                    Domain = FormsAuthentication.CookieDomain
                };
                if (rememberme)
                {

                    cookie.Expires = DateTime.Now.AddYears(50);
                    cookie.Values.Add("DomainName", domainName);
                    cookie.Values.Add("Checked", "checked");
                    HttpContext.Response.AppendCookie(cookie);
                }
                else
                {
                    cookie.Expires = DateTime.Now.AddHours(-1);
                    cookie.Values.Add("DomainName", string.Empty);
                    cookie.Values.Add("Checked", string.Empty);
                    HttpContext.Response.AppendCookie(cookie);
                }

                //操作结果
                var user = Entities.User.GetUserByDomainName(domainName);
                if (user == null) return Json(false);
                if (!user.Password.Equals(password)) return Json(false);
                var userData = string.Format("{0};{1};{2};{3};{4}", user.SysNo, user.Name, user.UserName, user.Applications,Convert.ToString(user.IsAdmin).Trim(','));
                var ticket = new FormsAuthenticationTicket(DateTime.Now.Minute,Convert.ToString(user.SysNo), DateTime.Now, DateTime.Now.AddMinutes(FormsAuthentication.Timeout.TotalMinutes), false, userData, "/");
                var value=FormsAuthentication.Encrypt(ticket);
                var cookieApplications = new HttpCookie("ACUser")
                {
                    Domain = FormsAuthentication.CookieDomain,
                    Value = value
                };
                HttpContext.Response.AppendCookie(cookieApplications);
                //记录登录日志
                AcLog.Insert("LOGIN", "LOGIN", "USER", user.Name, user.SysNo, user.Name, null);
                return Json(true);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Json(false);
            }
        }

        /// <summary>
        /// 退出登录
        /// </summary>
        public void LoginOut()
        {
            var limit = Request.Cookies.Count;
            for (var i = 0; i < limit; i++)
            {
                var cookie = Request.Cookies[i];
                if (cookie == null) continue;
                var cookieName = cookie.Name;
                if (cookieName.Equals("LOGIN")) continue;
                var oldCookie = new HttpCookie(cookieName)
                {
                    Expires = DateTime.Now.AddDays(-1)
                };
                Response.Cookies.Add(oldCookie);
            }
            Response.Redirect("/Admin/Login/Index");
        }

        [HttpPost]
        public ActionResult UpdatePassWord()
        {
            try
            {
                var passWord = Request["passWord"];
                var sysNo = HttpHelper.CreateInstance().GetUser(HttpContext, "SysNo");
                if (string.IsNullOrEmpty(sysNo))
                {
                    LoginOut();
                }
                long userSysNo;
                long.TryParse(sysNo, out userSysNo);

                var user = Entities.User.GetObject(userSysNo);
                var result = false; 
                if (user != null)
                {
                    var oldPassWord = user.Password;
                    user.Password = passWord;
                    user.ConfirmPassword = passWord;
                    result = user.UpdateSelf();
                    if (result)
                    {
                        //记录修改密码日志
                        AcLog.Insert(Convert.ToString(user.Applications), "UPDATE PASSWORD", "USER", user.Password, user.SysNo, user.Name, oldPassWord);
                    }
                }
                else
                {
                    LoginOut();
                }

                return Json(result);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Json(false);
            }

        }
    }
}
