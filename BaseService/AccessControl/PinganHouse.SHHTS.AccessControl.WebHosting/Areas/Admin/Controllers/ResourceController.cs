﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Newtonsoft.Json;

using PinganHouse.SHHTS.AccessControl.BLL;
using PinganHouse.SHHTS.AccessControl.Common;
using PinganHouse.SHHTS.AccessControl.Entities;
using PinganHouse.SHHTS.AccessControl.Utils.Log;

using PinganHouse.SHHTS.AccessControl.WebHosting.Areas.Admin.Models;

namespace PinganHouse.SHHTS.AccessControl.WebHosting.Areas.Admin.Controllers
{
    public class ResourceController : Controller
    {
        //
        // GET: /Resource/

        /// <summary>
        /// 资源列表
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Edit()
        {
            return View();
        }

        /// <summary>
        /// 资源证书+规则 列表
        /// </summary>
        /// <returns></returns>
        public ActionResult RelationIndex()
        {
            return View();
        }

        /// <summary>
        /// 获取资源
        /// </summary>
        /// <returns></returns>
        public ActionResult GetResource()
        {
            try
            {
                var sysNoStr = Request["sysNo"];
                long sysNo;
                long.TryParse(sysNoStr, out sysNo);
                //获取资源
                var resource = Resource.GetObject(sysNo) ?? new Resource();
                //序列化返回数据
                return Json(resource);

            }
            catch (Exception e)
            {
                Log.Error(e);
                //序列化返回数据
                return Json(new Resource());

            }
        }

        /// <summary>
        /// 获取资源列表
        /// </summary>
        /// <returns></returns>
        public ActionResult GetResourceList()
        {
            try
            {
                var rows = Convert.ToInt32(Request["rows"]);
                var page = Convert.ToInt32(Request["page"]);
                var filter = Convert.ToString(Request["filter"]);

                //应用程序域
                var applications = HttpHelper.CreateInstance().GetApplications(HttpContext);
                //总记录数
                int count;
                //结果集
                var dataTable = ResourceBll.GetResourceTable(page, rows, filter, applications, out count);
                //序列化数据
                var jsonData = JsonHelper.FormatJsonForEasyuiDataGrid(count, dataTable);
                //返回
                return Content(jsonData, "text/html;charset=UTF-8");

            }
            catch (Exception e)
            {
                Log.Error(e);
                return Content(JsonHelper.FormatJsonForEasyuiDataGrid(0, new DataTable()), "text/html;charset=UTF-8");
            }
        }

        /// <summary>
        /// 获取资源证书列表
        /// </summary>
        /// <returns></returns>
        public ActionResult GetCredentialsListInResource()
        {
            try
            {
                var rows = Convert.ToInt32(Request["rows"]);
                var page = Convert.ToInt32(Request["page"]);
                var resource = Request["resource"];
                //总记录数
                int count;
                //结果集
                var dataTable = ResourceBll.GetCredentialsTableListInResource(page, rows, resource, out count);
                //序列化数据
                var jsonData = JsonHelper.FormatJsonForEasyuiDataGrid(count, dataTable);
                //返回
                return Content(jsonData, "text/html;charset=UTF-8");

            }
            catch (Exception e)
            {
                Log.Error(e);
                return Content(JsonHelper.FormatJsonForEasyuiDataGrid(0, new DataTable()), "text/html;charset=UTF-8");
            }
        }

        /// <summary>
        /// 保存资源
        /// </summary>
        /// <returns></returns>
        public ActionResult SaveResource()
        {
            try
            {
                //Json 对象字符串
                var resourceStr = Request["resource"];                
                //Json对象
                var resource = (Resource)JsonConvert.DeserializeObject(resourceStr, typeof(Resource));
                //应用程序域
                var applicationsCookie = HttpHelper.CreateInstance().GetApplications(HttpContext);
                long applications;
                long.TryParse(applicationsCookie, out applications);
                resource.Applications = applications;
                resource.GroupName = resource.GroupName.ToUpper();

                //获取登录信息
                long empSysNo;
                var sysNo = HttpHelper.CreateInstance().GetUser(HttpContext, "SysNo");
                long.TryParse(sysNo, out empSysNo);
                var name = HttpUtility.UrlDecode(HttpHelper.CreateInstance().GetUser(HttpContext, "Name"));

                //操作结果
                bool result;

                if (resource.SysNo > 0)
                {
                    var oldResource = Resource.GetObject(resource.SysNo);
                    resource.ModifyUserName = name;
                    resource.ModifyUserSysNo = empSysNo;
                    result = resource.UpdateSelf();
                    if (result)
                    {
                        //记录修改资源码日志
                        AcLog.Insert(applicationsCookie, "Update Resource", "Resource", JsonConvert.SerializeObject(resource), empSysNo, name, JsonConvert.SerializeObject(oldResource));
                    }
                }
                else
                {
                    resource.CreateUserName = name;
                    resource.CreateUserSysNo = empSysNo;
                    result = resource.AddSelf() > 0;
                    if (result)
                    {
                        //记录新增资源码日志
                        AcLog.Insert(applicationsCookie, "Add Resource", "Resource", JsonConvert.SerializeObject(resource), empSysNo, name, null);
                    }
                }
                return Json(result);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Json(false);
            }
        }

        /// <summary>
        /// 删除资源
        /// </summary>
        /// <returns></returns>
        public ActionResult DelResource()
        {
            try
            {
                var sysNoStr = Request["sysNo"];
                long sysNo;
                long.TryParse(sysNoStr, out sysNo);
                //操作结果
                var result = Resource.DeleteSelf(sysNo);
                if (result)
                {
                    //应用程序域
                    var applications = HttpHelper.CreateInstance().GetApplications(HttpContext);
                    //获取登录信息
                    long empSysNo;
                    var userSysNo = HttpHelper.CreateInstance().GetUser(HttpContext, "SysNo");
                    long.TryParse(userSysNo, out empSysNo);
                    var userName = HttpUtility.UrlDecode(HttpHelper.CreateInstance().GetUser(HttpContext, "Name"));

                    //记录删除资源日志
                    AcLog.Insert(applications, "Del Resource", "Resource", Convert.ToString(sysNo), empSysNo, userName, null);
                }
                return Json(result);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Json(false);
            }
        }

        /// <summary>
        /// 给资源添加访问证书
        /// </summary>
        /// <returns></returns>
        public ActionResult AddCredentialsListToResource()
        {
            try
            {
                //Json 对象字符串
                var resource = Request["resource"];
                var credentialsList = Request["credentialsList"];
                //Json对象
                var resourceEntity = (Resource)JsonConvert.DeserializeObject(resource, typeof(Resource));
                //Json对象
                var credentialsListEntity = (List<Credentials>)JsonConvert.DeserializeObject(credentialsList, typeof(List<Credentials>));
                //获取登录信息
                long empSysNo;
                var sysNo = HttpHelper.CreateInstance().GetUser(HttpContext, "SysNo");
                long.TryParse(sysNo, out empSysNo);
                var name = HttpUtility.UrlDecode(HttpHelper.CreateInstance().GetUser(HttpContext, "Name"));
                resourceEntity.CreateUserName = name;
                resourceEntity.CreateUserSysNo = empSysNo;
                var result = Resource.AddCredentialsToResource(resourceEntity, credentialsListEntity);
                return Json(result);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Json(false);
            }
        }

        /// <summary>
        /// 添加访问规则
        /// </summary>
        /// <returns></returns>
        public ActionResult AddCredentialsRulesListToResource()
        {
            try
            {
                var rulesList = Request["rulesList"];
                //Json对象 访问规则
                var rulesListModel = (List<LimitsRulesModel>)JsonConvert.DeserializeObject(rulesList, typeof(List<LimitsRulesModel>));
                //添加修改的规则
                var dic = rulesListModel.ToDictionary(rules => rules.SysNo, rules => rules.LimitsRules);
                var result = Resource.SetCredentialsRulesListToResource(dic);
                return Json(result);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Json(false);
            }
        }

        /// <summary>
        /// 移除证书
        /// </summary>
        /// <returns></returns>
        public ActionResult RemoveCredentialsInResource()
        {
            try
            {
                var sysNo = Convert.ToInt32(Request["sysNo"]);
                var result = Resource.ReMoveCredentialsInResource(sysNo);
                if (result)
                {
                    //应用程序域
                    var applications = HttpHelper.CreateInstance().GetApplications(HttpContext);
                    //获取登录信息
                    long empSysNo;
                    var userSysNo = HttpHelper.CreateInstance().GetUser(HttpContext, "SysNo");
                    long.TryParse(userSysNo, out empSysNo);
                    var userName = HttpUtility.UrlDecode(HttpHelper.CreateInstance().GetUser(HttpContext, "Name"));

                    //移除证书
                    AcLog.Insert(applications, "RemoveCredentialsInResource", "ResourceRelation", Convert.ToString(sysNo), empSysNo, userName, null);
                }
                return Json(result);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Json(false);
            }
        }
    }
}
