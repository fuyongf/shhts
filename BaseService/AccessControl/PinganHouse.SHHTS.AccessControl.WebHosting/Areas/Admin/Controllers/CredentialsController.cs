﻿using System;
using System.Data;
using System.Web;
using System.Web.Mvc;

using Newtonsoft.Json;

using PinganHouse.SHHTS.AccessControl.BLL;
using PinganHouse.SHHTS.AccessControl.Common;
using PinganHouse.SHHTS.AccessControl.Entities;
using PinganHouse.SHHTS.AccessControl.Utils.Log;

namespace PinganHouse.SHHTS.AccessControl.WebHosting.Areas.Admin.Controllers
{
    public class CredentialsController : Controller
    {
        //
        // GET: /Credentials/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Edit()
        {
            return View();
        }

        /// <summary>
        /// 证书列表(未绑定指定资源的证书)
        /// </summary>
        /// <returns></returns>
        public ActionResult CredentialsIndex()
        {
            var resource = Request["resource"];
            int resourceNo;
            int.TryParse(resource, out resourceNo);
            return View(resourceNo);
        }

        /// <summary>
        /// 证书列表(未绑定指定资源的证书)
        /// </summary>
        /// <returns></returns>
        public ActionResult CredentialsOutPrincipalPart()
        {
            var principalPart = Request["principalPart"];
            int principalPartSysNo;
            int.TryParse(principalPart, out principalPartSysNo);
            return View(principalPartSysNo);
        }

        /// <summary>
        /// 获取证书
        /// </summary>
        /// <returns></returns>
        public ActionResult GetCredentials()
        {
            try
            {
                var sysNoStr = Request["sysNo"];
                long sysNo;
                long.TryParse(sysNoStr, out sysNo);
                var credentials = Credentials.GetObject(sysNo) ?? new Credentials();

                //序列化返回数据
                return Json(credentials);

            }
            catch (Exception e)
            {
                Log.Error(e);
                //序列化返回数据
                return Json(new Credentials());

            }
        }

        /// <summary>
        /// 获取证书列表
        /// </summary>
        /// <returns></returns>
        public ActionResult GetCredentialsList()
        {
            try
            {
                var rows = Convert.ToInt32(Request["rows"]);
                var page = Convert.ToInt32(Request["page"]);
                var filter = Convert.ToString(Request["filter"]);

                //应用程序域
                var applications = HttpHelper.CreateInstance().GetApplications(HttpContext);

                //总记录数
                int count;
                //结果集
                var dataTable = CredentialsBll.GetCredentialsTable(page, rows, filter,applications,out count);
                //序列化数据
                var jsonData = JsonHelper.FormatJsonForEasyuiDataGrid(count, dataTable);
                //返回
                return Content(jsonData, "text/html;charset=UTF-8");
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Content(JsonHelper.FormatJsonForEasyuiDataGrid(0, new DataTable()), "text/html;charset=UTF-8");
            }
        }

        /// <summary>
        /// 获取证书列表
        /// </summary>
        /// <returns></returns>
        public ActionResult GetCredentialsListOutResource()
        {
            try
            {
                var rows = Convert.ToInt32(Request["rows"]);
                var page = Convert.ToInt32(Request["page"]);
                var resource =Request["resource"];
                var filter = Convert.ToString(Request["page"]);
                //应用程序域
                var applications = HttpHelper.CreateInstance().GetApplications(HttpContext);

                //总记录数
                int count;
                //结果集
                var dataTable = CredentialsBll.GetCredentialsTableListOutResource(page,rows, resource,filter,applications, out count);
                //序列化数据
                var jsonData = JsonHelper.FormatJsonForEasyuiDataGrid(count, dataTable);
                //返回
                return Content(jsonData, "text/html;charset=UTF-8");
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Content(string.Empty, "text/html;charset=UTF-8");
            }            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult GetCredentialsListOutPrincipalPart()
        {
            try
            {
                var rows = Convert.ToInt32(Request["rows"]);
                var page = Convert.ToInt32(Request["page"]);
                var principalPart = Request["principalPart"];
                var filter = Convert.ToString(Request["page"]);
                //应用程序域
                var applications = HttpHelper.CreateInstance().GetApplications(HttpContext);
                //总记录数
                int count;
                //结果集
                var dataTable = CredentialsBll.GetCredentialsTableListOutPrincipalPart(page, rows, principalPart,filter,applications, out count);
                //序列化数据
                var jsonData = JsonHelper.FormatJsonForEasyuiDataGrid(count, dataTable);
                //返回
                return Content(jsonData, "text/html;charset=UTF-8");
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Content(string.Empty, "text/html;charset=UTF-8");
            }
        }

        /// <summary>
        /// 保存证书
        /// </summary>
        /// <returns></returns>
        public ActionResult SaveCredentials()
        {
            try
            {
                //Json 对象字符串
                var credentialsStr = Request["credentials"];

                //Json对象
                var credentials = (Credentials)JsonConvert.DeserializeObject(credentialsStr, typeof(Credentials));

                //应用程序域
                var applicationsCookie = HttpHelper.CreateInstance().GetApplications(HttpContext);
                long applications;
                long.TryParse(applicationsCookie, out applications);
                credentials.Applications = applications;

                //获取登录信息
                long empSysNo;
                var sysNo = HttpHelper.CreateInstance().GetUser(HttpContext, "SysNo");
                long.TryParse(sysNo,out empSysNo);
                var name = HttpUtility.UrlDecode(HttpHelper.CreateInstance().GetUser(HttpContext, "Name"));

                //操作结果
                bool result;

                if (credentials.SysNo > 0)
                {
                    var oldCredentials = Credentials.GetObject(credentials.SysNo);
                    credentials.ModifyUserName = name;
                    credentials.ModifyUserSysNo = empSysNo;
                    result = credentials.UpdateSelf();
                    if (result)
                    {
                        //记录修改资源码日志
                        AcLog.Insert(applicationsCookie, "Update Resource", "Resource", JsonConvert.SerializeObject(credentials), empSysNo, name, JsonConvert.SerializeObject(oldCredentials));
                    }

                }
                else
                {
                    credentials.CreateUserName = name;
                    credentials.CreateUserSysNo = empSysNo;
                    result = credentials.AddSelf() > 0;
                    if (result)
                    {
                        //记录新增资源码日志
                        AcLog.Insert(applicationsCookie, "Add Credentials", "Credentials", JsonConvert.SerializeObject(credentials), empSysNo, name, null);
                    }
                }
                return Json(result);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Json(false);
            }
        }


        /// <summary>
        /// 保存证书
        /// </summary>
        /// <returns></returns>
        public ActionResult DelCredentials()
        {
            try
            {
                var sysNoStr = Request["sysNo"];
                long sysNo;
                long.TryParse(sysNoStr, out sysNo);

                //删除前检查
                var checkResult = Credentials.CheckBeforeDel(sysNo);

                //如果正如被资源绑定
                if (checkResult)
                {
                    return Json(false);
                }

                //操作结果
                var result = Credentials.DeleteSelf(sysNo);
                if (result)
                {
                    //应用程序域
                    var applications = HttpHelper.CreateInstance().GetApplications(HttpContext);
                    //获取登录信息
                    long empSysNo;
                    var userSysNo = HttpHelper.CreateInstance().GetUser(HttpContext, "SysNo");
                    long.TryParse(userSysNo, out empSysNo);
                    var userName = HttpUtility.UrlDecode(HttpHelper.CreateInstance().GetUser(HttpContext, "Name"));

                    //记录删除证书日志
                    AcLog.Insert(applications, "Del Credentials", "Credentials", Convert.ToString(sysNo), empSysNo, userName, null);
                }
                return Json(result);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Json(false);
            }
        }

    }
}
