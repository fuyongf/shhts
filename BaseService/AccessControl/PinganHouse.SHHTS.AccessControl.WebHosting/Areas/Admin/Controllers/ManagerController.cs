﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using Newtonsoft.Json;

using PinganHouse.SHHTS.AccessControl.Common;
using PinganHouse.SHHTS.AccessControl.Entities;
using PinganHouse.SHHTS.AccessControl.Utils.Log;

using PinganHouse.SHHTS.AccessControl.WebHosting.Areas.Admin.Models;

namespace PinganHouse.SHHTS.AccessControl.WebHosting.Areas.Admin.Controllers
{
    public class ManagerController : Controller
    {
        //
        // GET: /Manager/

        public ActionResult Index()
        {
            //获取登录信息
            ViewBag.UserName = HttpHelper.CreateInstance().GetUser(HttpContext, "UserName");
            ViewBag.Name = HttpUtility.UrlDecode(HttpHelper.CreateInstance().GetUser(HttpContext, "Name"));

            if (string.IsNullOrEmpty(ViewBag.UserName))
            {
                Response.Redirect("/Admin/Login/Index");
            }
            //获取系统按钮
            var menuList = GetApplicationsMenu();
            
            //反序
            ViewBag.Menus = JsonConvert.SerializeObject(menuList);

            return View();
        }

        /// <summary>
        /// 页面捕获跳转
        /// </summary>
        /// <returns></returns>
        public void ManagerMenu()
        {
            var menuIds = Request["Menuid"].Split('-');

            //获取应用程序ID
            var applications = menuIds[0];

            InstallApplicationsCookie(applications);

            //获取跳转页面编号
            var menuId = menuIds[1];

            switch (menuId)
            {
                case "1":
                    Response.Redirect("/Admin/Resource/Index");
                    break;
                case "2":
                    Response.Redirect("/Admin/Credentials/Index");
                    break;
                case "3":
                    Response.Redirect("/Admin/Resource/RelationIndex");
                    break;
                case "4":
                    Response.Redirect("/Admin/PrincipalPart/Index");
                    break;
                case "5":
                    Response.Redirect("/Admin/PrincipalPart/RelationIndex");
                    break;
            }
        }

        /// <summary>
        /// 设置程序选择Cook
        /// </summary>
        /// <param name="applications"></param>
        private void InstallApplicationsCookie(string applications)
        {
            var userData = string.Format("{0}", applications);
            var ticket = new FormsAuthenticationTicket(DateTime.Now.Minute, applications, DateTime.Now, DateTime.Now.AddMinutes(FormsAuthentication.Timeout.TotalMinutes), false, userData, "/");
            var value = FormsAuthentication.Encrypt(ticket);
            var cookieApplications = new HttpCookie("Applications")
            {
                Domain = FormsAuthentication.CookieDomain,
                Value =value
            };
            HttpContext.Response.AppendCookie(cookieApplications);
        }

        /// <summary>
        /// 获取应用程序并生成按钮
        /// </summary>
        /// <returns></returns>
        public List<Menus> GetApplicationsMenu()
        {
            var menusList = new List<Menus>();
            try
            {
                long userSysNo;
                long.TryParse(HttpHelper.CreateInstance().GetUser(HttpContext, "SysNo"), out userSysNo);

                bool isAdmin;
                bool.TryParse(HttpHelper.CreateInstance().GetUser(HttpContext, "IsAdmin"), out isAdmin);
                var applicationsList = Applications.GetApplicationsList(userSysNo, isAdmin);

                foreach (var applicationse in applicationsList)
                {
                    var menu = new Menus
                    {
                        menuname = applicationse.ApplicationsName,
                        icon = applicationse.Icon,
                        appid=Convert.ToString(applicationse.SysNo),
                        menuid = Convert.ToString(applicationse.SysNo)
                    };
                    //*********************添加子菜单
                    AddMenusChildMenu(menu, menu.menuid);

                    menusList.Add(menu);
                }

                //超级管理员则添加系统管理菜单
                if (isAdmin)
                {
                    AddOtherMenus(menusList);
                }
                return menusList;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return menusList;
            }
        }

        /// <summary>
        /// 添加其他管理
        /// </summary>
        /// <param name="menusList"></param>
        private static void AddOtherMenus(List<Menus> menusList)
        {
            var menuOther = new Menus
            {
                menuname = "系统管理",
                icon = "icon-brick",
                menuid = Convert.ToString(9999)
            };

            //人员列表
            var menuUser = new Menus
            {
                menuid = "9999-1",
                menuname = "人员列表",
                icon = "icon-group",
                url = "/Admin/User/Index"
            };
            menuOther.menus.Add(menuUser);

            //应用管理
            var menuApp = new Menus
            {
                menuid = "9999-2",
                menuname = "应用管理",
                icon = "icon-link",
                url = "/Admin/Applications/Index"
            };
            menuOther.menus.Add(menuApp);

            //应用管理
            var menuAppRelationIndex = new Menus
            {
                menuid = "9999-3",
                menuname = "应用管理人员配置",
                icon = "icon-set",
                url = "/Admin/Applications/ApplicationsRelation"
            };
            menuOther.menus.Add(menuAppRelationIndex);

            menusList.Add(menuOther);
        }

        /// <summary>
        /// 给应用程序添加子菜单
        /// </summary>
        /// <param name="fatherMenu"></param>
        /// <param name="appid">应用域</param>
        private static void AddMenusChildMenu(Menus fatherMenu, string appid)
        {
            //资源
            var menuResource = new Menus
            {
                menuid = fatherMenu.menuid + "-1",
                menuname = "资源列表",
                icon = "icon-sitemap_color ",
                appid=appid,
                url = "/Admin/Manager/ManagerMenu?Menuid=" + fatherMenu.menuid + "-1"
            };
            fatherMenu.menus.Add(menuResource);


            //证书
            var menuCredentials = new Menus
            {
                menuid = fatherMenu.menuid + "-2",
                menuname = "证书列表",
                icon = "icon-textfield_key",
                appid = appid,
                url = "/Admin/Manager/ManagerMenu?Menuid=" + fatherMenu.menuid + "-2"
            };
            fatherMenu.menus.Add(menuCredentials);

            //资源/证书配置
            var menuResourceRelation = new Menus
            {
                menuid = fatherMenu.menuid + "-3",
                menuname = "资源/证书配置",
                icon = "icon-set",
                appid = appid,
                url = "/Admin/Manager/ManagerMenu?Menuid=" + fatherMenu.menuid + "-3"
            };
            fatherMenu.menus.Add(menuResourceRelation);

            //主体
            var menuPrincipalPart = new Menus
            {
                menuid = fatherMenu.menuid + "-4",
                menuname = "主体列表",
                icon = "icon-layers",
                appid = appid,
                url = "/Admin/Manager/ManagerMenu?Menuid=" + fatherMenu.menuid + "-4"
            };
            fatherMenu.menus.Add(menuPrincipalPart);


            //主体/证书配置
            var menuPrincipalPartRelation = new Menus
            {
                menuid = fatherMenu.menuid + "-5",
                menuname = "主体/证书配置",
                icon = "icon-set2",
                appid = appid,
                url = "/Admin/Manager/ManagerMenu?Menuid=" + fatherMenu.menuid + "-5"
            };
            fatherMenu.menus.Add(menuPrincipalPartRelation);
        }

    }
}
