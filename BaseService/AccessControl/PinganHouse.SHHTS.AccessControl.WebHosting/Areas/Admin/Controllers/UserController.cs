﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

using Newtonsoft.Json;

using PinganHouse.SHHTS.AccessControl.BLL;
using PinganHouse.SHHTS.AccessControl.Common;
using PinganHouse.SHHTS.AccessControl.Entities;
using PinganHouse.SHHTS.AccessControl.Utils.Log;

namespace PinganHouse.SHHTS.AccessControl.WebHosting.Areas.Admin.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 系统用户编辑页面
        /// </summary>
        /// <returns></returns>
        public ActionResult Edit()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult UserIndex()
        {
            var applications = Request["applications"];
            int applicationsSysNo;
            int.TryParse(applications, out applicationsSysNo);
            return View(applicationsSysNo);
        }

        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <returns></returns>
        public ActionResult GetUserList()
        {

            try
            {
                var rows = Convert.ToInt32(Request["rows"]);
                var page = Convert.ToInt32(Request["page"]);
                var filter = Request["filter"];
                //总记录数
                int count;
                //结果集
                var dataTable = UserBll.GetUserTable(page, rows,filter,out count);
                //序列化数据
                var jsonData=JsonHelper.FormatJsonForEasyuiDataGrid(count, dataTable);
                //返回
                return Content(jsonData, "text/html;charset=UTF-8");
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Content(string.Empty, "text/html;charset=UTF-8");
            }
        }

        /// <summary>
        /// 获取应用的管理人员
        /// </summary>
        /// <returns></returns>
        public ActionResult GetUserListInApplications()
        {
            try
            {
                var rows = Convert.ToInt32(Request["rows"]);
                var page = Convert.ToInt32(Request["page"]);
                var applications = Request["applications"];
                var filter = Convert.ToString(Request["page"]);
                //总记录数
                int count;
                //结果集
                var dataTable = UserBll.GetUserListInApplications(page, rows, applications, filter, out count);
                //序列化数据
                var jsonData = JsonHelper.FormatJsonForEasyuiDataGrid(count, dataTable);
                //返回
                return Content(jsonData, "text/html;charset=UTF-8");
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Content(string.Empty, "text/html;charset=UTF-8");
            }
        }

        /// <summary>
        /// 查询未配置的管理人员
        /// </summary>
        /// <returns></returns>
        public ActionResult GetUserListOutApplications()
        {
            try
            {
                var rows = Convert.ToInt32(Request["rows"]);
                var page = Convert.ToInt32(Request["page"]);
                var applications = Request["applications"];
                var filter = Convert.ToString(Request["page"]);
                //总记录数
                int count;
                //结果集
                var dataTable = UserBll.GetUserListOutApplications(page, rows, applications, filter, out count);
                //序列化数据
                var jsonData = JsonHelper.FormatJsonForEasyuiDataGrid(count, dataTable);
                //返回
                return Content(jsonData, "text/html;charset=UTF-8");
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Content(string.Empty, "text/html;charset=UTF-8");
            }
        }

        /// <summary>
        /// 获取系统用户
        /// </summary>
        /// <returns></returns>
        public ActionResult GetUser()
        {
            try
            {
                var sysNoStr = Request["sysNo"];
                long sysNo;
                long.TryParse(sysNoStr, out sysNo);
                var user = Entities.User.GetObject(sysNo);
                //序列化返回数据
                return Json(user);
            }
            catch (Exception e)
            {
                Log.Error(e);
                //序列化返回数据
                return Json(new User());

            }
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <returns></returns>
        public ActionResult DelUser()
        {
            try
            {
                var sysNoStr = Request["sysNo"];
                long sysNo;
                long.TryParse(sysNoStr, out sysNo);
                //操作结果
                var result = Entities.User.DeleteSelf(sysNo);
                return Json(result);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Json(false);
            }
        }

        /// <summary>
        /// 保存主体
        /// </summary>
        /// <returns></returns>
        public ActionResult SaveUser()
        {
            try
            {
                //Json 对象字符串
                var userStr = Request["user"];

                //Json对象
                var user = (User)JsonConvert.DeserializeObject(userStr, typeof(User));
                var applications = HttpHelper.CreateInstance().GetUser(HttpContext, "Applications");
                long app;
                long.TryParse(applications, out app);
                user.Applications = app;
                //操作结果
                bool result;
                if (user.SysNo > 0)
                {
                    result = user.UpdateSelf();
                }
                else
                {
                    result = user.AddSelf() > 0;
                }
                return Json(result);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Json(false);
            }
        }

        /// <summary>
        /// 添加人员到应用程序
        /// </summary>
        /// <returns></returns>
        public ActionResult AddUserListToApplications()
        {
            try
            {
                //Json 对象字符串
                var applications = Request["applications"];
                var userList = Request["userList"];

                //Json对象
                var applicationsEntity = (Applications)JsonConvert.DeserializeObject(applications, typeof(Applications));

                //Json对象
                var userListEntity = (List<User>)JsonConvert.DeserializeObject(userList, typeof(List<User>));

                //获取登录信息
                long empSysNo;
                var sysNo = HttpHelper.CreateInstance().GetUser(HttpContext, "SysNo");
                long.TryParse(sysNo, out empSysNo);
                var name = HttpUtility.UrlDecode(HttpHelper.CreateInstance().GetUser(HttpContext, "Name"));
                applicationsEntity.CreateUserName = name;
                applicationsEntity.CreateUserSysNo = empSysNo;

                var result = Entities.User.AddUserListToApplications(applicationsEntity, userListEntity);

                return Json(result);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Json(false);
            }
        }

        /// <summary>
        /// 移除管理人员
        /// </summary>
        /// <returns></returns>
        public ActionResult RemoveUserInApplications()
        {
            try
            {
                var sysNo = Convert.ToInt32(Request["sysNo"]);
                var result = Entities.User.RemoveUserInApplications(sysNo);
                return Json(result);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Json(false);
            }
        }

    }
}
