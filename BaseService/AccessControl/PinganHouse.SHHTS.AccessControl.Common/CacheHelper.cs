﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.AccessControl.Common
{
    /// <summary>
    /// 本地缓存
    /// </summary>
    public class CacheHelper<T>
    {
        static Dictionary<string, CacheObject<T>> CacheContainer = new Dictionary<string, CacheObject<T>>();

        static CacheHelper<T> _instance = new CacheHelper<T>();

        static object _SyncLock = new object();

        private CacheHelper()
        {
        }

        public static CacheHelper<T> Instance
        {
            get
            {
                return _instance;
            }
        }


        #region 基础操作
        /// <summary>
        /// 设置缓存对象
        /// </summary>
        /// <param name="key"></param>
        /// <param name="obj"></param>
        /// <param name="expirs"></param>
        public void Set(string key, T obj, TimeSpan expirs)
        {
            CacheObject<T> cacheObj = new CacheObject<T>
            {
                Key = key,
                Value = obj,
                Expirs = DateTime.Now.Add(expirs)
            };
            lock (_SyncLock)
            {
                if (CacheContainer.ContainsKey(key))
                {
                    CacheContainer[key] = cacheObj;
                }
                else
                {
                    CacheContainer.Add(key, cacheObj);
                }
            }
        }

        /// <summary>
        /// 设置缓存对象
        /// </summary>
        /// <param name="key"></param>
        /// <param name="obj"></param>
        /// <param name="expirs"></param>
        public void Set(string key, T obj, DateTime expireTime)
        {
            CacheObject<T> cacheObj = new CacheObject<T>
            {
                Key = key,
                Value = obj,
                Expirs = expireTime
            };
            lock (_SyncLock)
            {
                if (CacheContainer.ContainsKey(key))
                {
                    CacheContainer[key] = cacheObj;
                }
                else
                {
                    CacheContainer.Add(key, cacheObj);
                }
            }
        }

        /// <summary>
        /// 从缓存中获取对象
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public T Get(string key)
        {
            if (!CacheContainer.ContainsKey(key))
                return default(T);
            CacheObject<T> cache = CacheContainer[key];
            if (cache == null)
                return default(T);
            if (cache.Expirs < DateTime.Now)
            {
                lock (_SyncLock)
                {
                    CacheContainer.Remove(key);
                }
                return default(T);
            }
            return cache.Value;
        }

        #endregion

    }
    class CacheObject<T>
    {
        public string Key { get; set; }
        public T Value { get; set; }

        public DateTime Expirs { get; set; }
    }
}
