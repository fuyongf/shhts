﻿namespace PinganHouse.SHHTS.AccessControl.Common
{
    public class Configs
    {
        /// <summary>
        /// ｛0｝　类名　｛1｝　方法名　｛2｝　执行时间　｛3｝关键参数值和名 或者消息
        /// </summary>
        public readonly static string ServerLogFormat = "Server\t{0}\t{1}\t{2}\t{3}";
    }
}
