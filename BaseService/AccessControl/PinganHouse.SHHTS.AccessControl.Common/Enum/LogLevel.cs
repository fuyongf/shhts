﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.AccessControl.Common.Enum
{
    public enum LogLevel
    {
        /// <summary>
        /// 低级
        /// </summary>
        Low =0,

        /// <summary>
        /// 中级
        /// </summary>
        In =1,

        /// <summary>
        /// 高级
        /// </summary>
        High=2,
    }
}
