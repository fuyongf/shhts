﻿namespace PinganHouse.SHHTS.AccessControl.Common.Enum
{
    /// <summary>
    /// 权限的限制规则
    /// </summary>
    public enum LimitsRules
    {   

        /// <summary>
        /// 允许
        /// </summary>
        Allow=1,
        /// <summary>
        /// 拒绝
        /// </summary>
        Refuse=-1,
        /// <summary>
        /// 默认
        /// </summary>
        Default = 0
    }
}
