﻿using System.Web;
using System.Web.Security;

namespace PinganHouse.SHHTS.AccessControl.Common
{
    public class HttpHelper
    {
        private volatile static HttpHelper _instance;
        private static readonly object LockHelper = new object();
        private HttpHelper() { }
        public static HttpHelper CreateInstance()
        {
            if (_instance != null) return _instance;
            lock(LockHelper)
            {
                if(_instance == null)
                    _instance = new HttpHelper();
            }
            return _instance;
        }

        /// <summary>
        /// 获取应用程序域
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        public string GetApplications(HttpContextBase httpContext)
        {
            var applications = string.Empty;
            var applicationsCookie = httpContext.Request.Cookies["Applications"];
            if (applicationsCookie == null) return applications;
            var applicationsCookieValues = FormsAuthentication.Decrypt(applicationsCookie.Value);
            if (applicationsCookieValues == null) return applications;
            applications = applicationsCookieValues.UserData;
            return applications;
        }

        /// <summary>
        /// 获取登录信息
        /// </summary>
        /// <param name="httpContext"></param>
        /// <param name="key">获取的节点</param>
        /// <returns></returns>
        public string GetUser(HttpContextBase httpContext,string key)
        {
            var value = string.Empty;
            var userCookie = httpContext.Request.Cookies["ACUser"];
            if (userCookie == null) return value;
            var userCookieValues = FormsAuthentication.Decrypt(userCookie.Value);
            if (userCookieValues == null) return value;
            var values = userCookieValues.UserData.Split(';');
            switch (key)
            {
                case "SysNo":
                    value = values[0];
                    break;
                case "Name":
                    value = values[1];
                    break;
                case "UserName":
                    value = values[2];
                    break;
                case "Applications":
                    value = values[3];
                    break;
                case "IsAdmin":
                    value = values[4];
                    break;
            }
            return value;
        }
    }
}
