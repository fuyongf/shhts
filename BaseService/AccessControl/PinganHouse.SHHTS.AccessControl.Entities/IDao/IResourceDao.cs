﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using PinganHouse.SHHTS.AccessControl.Common.Enum;

namespace PinganHouse.SHHTS.AccessControl.Entities.IDao
{
    public interface IResourceDao
    {
        /// <summary>
        /// 添加资源
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        long AddSelf(Resource resource);

        /// <summary>
        /// 修改资源
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        bool UpdateSelf(Resource resource);

        /// <summary>
        /// 删除资源
        /// </summary>
        /// <param name="resourceSysNo"></param>
        /// <returns></returns>
        bool DeleteSelf(long resourceSysNo);

        /// <summary>
        /// 检查资源是否已经存在
        /// </summary>
        /// <param name="resourceName">资源名称</param>
        /// <param name="resourceType">资源类型</param>
        /// <returns></returns>
        bool IsExists(string resourceName, string resourceType);


        /// <summary>
        /// 检查资源是否已经存在(排除自身)
        /// </summary>
        /// <param name="resourceName">资源名称</param>
        /// <param name="applications">资源类型</param>
        /// <param name="sysNo">系统编号</param>
        /// <returns></returns>
        bool IsExistsExcludeSelf(string resourceName, string applications, long sysNo);

        /// <summary>
        /// 为资源添加相应证书
        /// </summary>
        /// <param name="resource"></param>
        /// <param name="credentialsList"></param>
        /// <returns></returns>
        bool AddCredentialsToResource(Resource resource, List<Credentials> credentialsList);

        /// <summary>
        /// 移除资源上的证书
        /// </summary>
        /// <param name="relationSysNo">证书资源绑定关系表sysNo</param>
        /// <returns></returns>
        bool ReMoveCredentialsInResource(long relationSysNo);

        /// <summary>
        /// 获取资源
        /// </summary>
        /// <param name="resourceSysNo">资源系统编号</param>
        /// <returns></returns>
        Resource GetObject(long resourceSysNo);

        /// <summary>
        /// 获取资源
        /// </summary>
        /// <param name="resourceNo">资源业务编号</param>
        /// <returns></returns>
        Resource GetObjectByResourceNo(string resourceNo);

        /// <summary>
        /// 获取资源列表
        /// </summary>
        /// <param name="pageSize">页显示数</param>
        /// <param name="pageIndex">显示页数</param>
        /// <param name="filter">过滤条件</param>
        /// <param name="applications">应用程序域</param>
        /// <param name="count">总数</param>
        /// <returns>资源信息列表</returns>
        DataTable GetResourceTable(int pageSize, int pageIndex, string filter,string applications, out int count);

        /// <summary>
        /// 根据证书编获取资源列表
        /// </summary>
        /// <param name="credentialsSysNo"></param>
        /// <param name="page"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        Hashtable GetResourceListTableByCredentialsSysNo(long credentialsSysNo, int page, int pageIndex);

        /// <summary>
        /// 为证书设置访问资源的访问规则
        /// </summary>
        /// <param name="dic">对应关系表SysNo,以及访问规则</param>
        /// <returns>T:全部设置成功 F:设置发生错误</returns>
        bool SetCredentialsRulesListToResource(Dictionary<long, LimitsRules> dic);

        /// <summary>
        /// 获取所属某个证书的所有资源列表
        /// </summary>
        /// <param name="credentialsSysNo">证书系统编号</param>
        /// <returns></returns>
        DataTable GetResourceListByCredentialsSysNo(long credentialsSysNo);

        /// <summary>
        /// 获取资源证书规则
        /// </summary>
        /// <param name="credentialsSysNo"></param>
        /// <returns></returns>
        DataRowCollection GetResoureRulesByCredentialsSysNo(List<long> credentialsSysNo);
    }
}
