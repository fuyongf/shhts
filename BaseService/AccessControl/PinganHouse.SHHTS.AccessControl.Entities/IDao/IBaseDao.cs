﻿using System.Collections.Generic;

namespace PinganHouse.SHHTS.AccessControl.Entities.IDao
{
    public interface IBaseDao<T>    where T : class
    {
        /// <summary>
        /// 根据给定的对象进行插入
        /// </summary>
        /// <returns>插入的对象所返回的主键</returns>
        object Insert(T t);

        /// <summary>
        /// 根据给定的对象进行更新
        /// </summary>
        /// <param name="t">对象</param>
        /// <returns>更新所影响的行数</returns>
        int Update(T t);

        /// <summary>
        /// 根据给定的主键唯一值进行物理删除
        /// </summary>
        /// <param name="o">主键/唯一值</param>
        /// <returns>该操作影响的行数</returns>
        int Delete(object o);

        /// <summary>
        /// 根据给定的主键唯一值查询对象
        /// </summary>
        /// <param name="o">主键/唯一值</param>
        /// <returns>查询到的对象</returns>
        T GetObject(object o);

        /// <summary>
        /// 列出所有数据
        /// </summary>
        /// <returns></returns>
        IList<T> GetAll();

        /// <summary>
        /// 分页获取所有数据
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        IList<T> GetAll(long pageIndex, long pageSize);
    }
}
