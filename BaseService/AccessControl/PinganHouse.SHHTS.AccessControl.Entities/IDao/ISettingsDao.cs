﻿using System.Collections.Generic;

namespace PinganHouse.SHHTS.AccessControl.Entities.IDao
{
    public interface ISettingsDao : IBaseDao<Settings>
    {
        /// <summary>
        /// 获取所有记录
        /// </summary>
        /// <returns></returns>
        IList<Settings> GetKeyValueList();

        int DeleteAll(long[] sysNoArr);
        
        Settings GetObjectByKeyAndParent(string key,Settings parent);

        Settings GetRootObject(string rootKey);
        
        List<Settings> SearchSettingsSonGroup(long parentID);

        int DeleteMultiSettings(List<long> sysNoList);

        int InsertSettings(Settings settingsDto, List<Settings> settingsDtos);

        int UpdateSettings(List<Settings> settingsList, List<long> delSysNoList);

        /// <summary>
        /// 返回所有Settings集合
        /// </summary>
        /// <returns>所有Settings集合</returns>
        List<Settings> GetAllNationalAddressList(long parentSysNo);

        /// <summary>
        /// 返回所有城市集合
        /// </summary>
        /// <returns>所有Settings集合</returns>
        IList<Settings> GetAllNationalAddressList();
    }
}
