﻿using System.Collections.Generic;

namespace PinganHouse.SHHTS.AccessControl.Entities.IDao
{
    public interface IApplicationsDao
    {
        /// <summary>
        /// 获取全部应用程序
        /// </summary>
        /// <returns></returns>
        List<Applications> GetApplicationsList(long applications,bool isAdmin);

        /// <summary>
        /// 获取应用
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        Applications GetObject(long sysNo);

        /// <summary>
        /// 修改应用程序
        /// </summary>
        /// <param name="applications"></param>
        /// <returns></returns>
        bool UpdateSelf(Applications applications);

        /// <summary>
        /// 新增应用程序
        /// </summary>
        /// <param name="applications"></param>
        /// <returns></returns>
        long AddSelf(Applications applications);

        /// <summary>
        /// 获取应用系统系统编号
        /// </summary>
        /// <param name="appNo"></param>
        /// <returns></returns>
        string GetApplicationSysNo(string appNo);
    }
}
