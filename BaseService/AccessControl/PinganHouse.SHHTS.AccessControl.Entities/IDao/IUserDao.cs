﻿using System.Data;
namespace PinganHouse.SHHTS.AccessControl.Entities.IDao
{
    public interface IUserDao
    {
        /// <summary>
        /// 添加用户
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        long AddSelf(User user);


        /// <summary>
        /// 修改用户
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        bool UpdateSelf(User user);


        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        bool DeleteSelf(long userSysNo);


        /// <summary>
        /// 检查用户是否已经存在
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        bool IsExists(string userName, string name);

        /// <summary>
        /// 检查用户编号是否已经存在
        /// </summary>
        /// <param name="userNo">用户编号</param>
        /// <returns></returns>
        bool IsExistsByUserNo(string userNo);

        /// <summary>
        /// 检查域账号是否已经存在
        /// </summary>
        /// <param name="domainName">域账号</param>
        /// <returns></returns>
        bool IsExistsByDomainName(string domainName);

        /// <summary>
        /// 检查域帐户（邮箱地址）是否已经存在
        /// </summary>
        /// <param name="emailAddr">域帐户（邮箱地址）</param>
        /// <returns></returns>
        bool IsExistsByEmailAddr(string emailAddr);

        /// <summary>
        /// 检查用户编号,域账号,域帐户（邮箱地址）是否已经存在
        /// </summary>
        /// <param name="userNo">用户编号</param>
        /// <param name="domainName">域账号</param>
        /// <param name="emailAddr">域帐户（邮箱地址）</param>
        /// <returns></returns>
        bool IsCheckByUserNoOrDomainNameOrEmailAddr(string userNo, string domainName, string emailAddr);

        /// <summary>
        /// 获取用户
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        User GetObject(long userSysNo);


        /// <summary>
        /// 根据工号查询
        /// </summary>
        /// <param name="userNo">工号</param>
        /// <returns></returns>
        User GetUserByUserNo(string userNo);

        /// <summary>
        /// 根据域账号查询
        /// </summary>
        /// <param name="domainName">域账号</param>
        /// <returns></returns>
        User GetUserByDomainName(string domainName);

        /// <summary>
        /// 根据域帐户（邮箱地址）查询
        /// </summary>
        /// <param name="emailAddr">域帐户（邮箱地址）</param>
        /// <returns></returns>
        User GetUserByEmailAddr(string emailAddr);

        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <param name="pageSize">页显示数</param>
        /// <param name="pageIndex">显示页数</param>
        /// <param name="count">总数</param>
        /// <param name="filter">筛选条件</param>
        /// <returns>用户信息列表</returns>
        DataTable GetUserTable(int pageSize, int pageIndex, string filter, out int count);

        /// <summary>
        /// 查询未被添加到某指定应用的管理人员
        /// </summary>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="applications">应用程序域</param>
        /// <param name="filter"></param>
        /// <param name="count"></param>
        /// <returns>人员列表</returns>
        DataTable GetUserTableListOutApplications(int page, int rows, string applications, string filter, out int count);

        /// <summary>
        /// 查询添加到某指定应用的管理人员
        /// </summary>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="applications">应用程序域</param>
        /// <param name="filter"></param>
        /// <param name="count"></param>
        /// <returns>人员列表</returns>
        DataTable GetUserTableListInApplications(int page, int rows, string applications, string filter, out int count);


        /// <summary>
        /// 添加人员到应用
        /// </summary>
        /// <param name="applicationsEntity"></param>
        /// <param name="userListEntity"></param>
        /// <returns></returns>
        bool AddUserListToApplications(Applications applicationsEntity, System.Collections.Generic.List<User> userListEntity);


        /// <summary>
        /// 移除管理人员
        /// </summary>
        /// <param name="relationSysNo"></param>
        /// <returns></returns>
        bool RemoveUserInApplications(int relationSysNo);
    }
}
