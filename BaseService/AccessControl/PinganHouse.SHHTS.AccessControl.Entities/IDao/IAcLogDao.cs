﻿namespace PinganHouse.SHHTS.AccessControl.Entities.IDao
{
    public interface IAcLogDao
    {
        /// <summary>
        /// 日志
        /// </summary>
        /// <param name="log"></param>
        /// <returns></returns>
        bool Insert(AcLog log);
    }
}
