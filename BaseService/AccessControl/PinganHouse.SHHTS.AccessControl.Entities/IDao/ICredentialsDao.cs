﻿using System.Collections.Generic;
using System.Data;

namespace PinganHouse.SHHTS.AccessControl.Entities.IDao
{
    public interface ICredentialsDao
    {
        /// <summary>
        /// 添加证书
        /// </summary>
        /// <param name="credentials"></param>
        /// <returns></returns>
        long AddSelf(Credentials credentials);

        /// <summary>
        /// 修改证书
        /// </summary>
        /// <param name="credentials"></param>
        /// <returns></returns>
        bool UpdateSelf(Credentials credentials);

        /// <summary>
        /// 删除证书
        /// </summary>
        /// <param name="resourceSysNo"></param>
        /// <returns></returns>
        bool DeleteSelf(long resourceSysNo);

        /// <summary>
        /// 检查证书是否已经存在
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        bool IsExists(string name, string type);

        /// <summary>
        /// 检查证书是否已经存在(排除自身)
        /// </summary>
        /// <param name="name">证书名称</param>
        /// <param name="type">证书类型</param>
        /// <param name="sysNo">系统编号</param>
        /// <returns></returns>
        bool IsExistsExcludeSelf(string name, string type, long sysNo);

        /// <summary>
        /// 删除证书前的检查
        /// </summary>
        /// <param name="credentialsSysNo"></param>
        /// <returns></returns>
        bool CheckBeforeDel(long credentialsSysNo);

        /// <summary>
        /// 获取证书
        /// </summary>
        /// <param name="resourceSysNo"></param>
        /// <returns></returns>
        Credentials GetObject(long resourceSysNo);

        /// <summary>
        /// 根据证书编号获取证书
        /// </summary>
        /// <param name="credentialsNo"></param>
        /// <returns></returns>
        Credentials GetCredentialsByNo(string credentialsNo);

        /// <summary>
        /// 获取证书列表
        /// </summary>
        /// <param name="pageSize">页显示数</param>
        /// <param name="pageIndex">显示页数</param>
        /// <param name="filter">过滤条件</param>
        /// <param name="applications">应用程序编号</param>
        /// <param name="count">总数</param>
        /// <returns>证书信息列表</returns>
        DataTable GetCredentialsTable(int pageSize, int pageIndex, string filter,string applications,out int count);

        /// <summary>
        /// 查询所属某个资源的证书列表
        /// </summary>
        /// <param name="credentialsSysNoList">证书系统编号集合</param>
        /// <returns>返回DataTable</returns>
        List<Credentials> GetCredentialsListByCredentialsSysNoList(List<long> credentialsSysNoList);

        /// <summary>
        /// 查询所属某个资源的证书列表
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="resource">资源系统编号</param>
        /// <param name="page"></param>
        /// <param name="count"></param>
        /// <returns>返回DataTable</returns>
        DataTable GetCredentialsTableListInResource(int page, int rows, string resource, out int count);

        /// <summary>
        /// 查询资源所有的绑定证书(仅编号)
        /// </summary>
        /// <returns></returns>
        DataTable GetCredentialsTableListInResource(string resourceNo);

        /// <summary>
        /// 查询所属某个资源的证书列表
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="principalPart">主体系统编号</param>
        /// <param name="page"></param>
        /// <param name="count"></param>
        /// <returns>返回DataTable</returns>
        DataTable GetCredentialsTableListInPrincipalPart(int page, int rows, string principalPart, out int count);

        /// <summary>
        /// 获取主体持有的所有证书集合(仅返回SysNo)
        /// </summary>
        /// <returns></returns>
        DataTable GetCredentialsTableListInPrincipalPart(string principalPartNo);

        /// <summary>
        /// 查询未绑定某个资源的证书列表
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="resource">资源编号</param>
        /// <param name="pageIndex"></param>
        /// <param name="applications">应用程序</param>
        /// <param name="count"></param>
        /// <param name="filter">过滤</param>
        /// <returns>返回DataTable</returns>
        DataTable GetCredentialsTableListOutResource(int pageSize, int pageIndex, string resource, string filter, string applications, out int count);

        /// <summary>
        /// 查询所属某个资源的证书列表
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="principalPart">主体编号</param>
        /// <param name="pageIndex"></param>
        /// <param name="applications">应用域</param>
        /// <param name="count"></param>
        /// <param name="filter">过滤</param>
        /// <returns>返回DataTable</returns>
        DataTable GetCredentialsTableListOutPrincipalPart(int pageSize, int pageIndex, string principalPart, string filter, string applications, out int count);

        /// <summary>
        /// 获取资源和主体同时用户的证书集合
        /// </summary>
        /// <param name="resourceSysNo">资源系统编号</param>
        /// <param name="principalPartSysNoList">主体系统编号</param>
        /// <returns>证书集合</returns>
        DataTable GetCredentialsTableListInResourceAndPrincipalPart(long resourceSysNo, List<long> principalPartSysNoList);
    }
}
