﻿using System.Collections.Generic;
using System.Data;

namespace PinganHouse.SHHTS.AccessControl.Entities.IDao
{
    public interface IPrincipalPartDao
    {
        /// <summary>
        /// 添加主体
        /// </summary>
        /// <param name="principalPart">主体对象</param>
        /// <returns></returns>
        long AddSelf(PrincipalPart principalPart);

        /// <summary>
        /// 修改主体
        /// </summary>
        /// <param name="principalPart"></param>
        /// <returns></returns>
        bool UpdateSelf(PrincipalPart principalPart);

        /// <summary>
        /// 删除主体
        /// </summary>
        /// <param name="sysNo">主体系统编号</param>
        /// <returns></returns>
        bool DeleteSelf(long sysNo);


        /// <summary>
        /// 获取主体
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        PrincipalPart GetObject(long sysNo);

        /// <summary>
        /// 根据主体编号获取主体
        /// </summary>
        /// <param name="principalPartNo"></param>
        /// <returns></returns>
        PrincipalPart GetPrincipalPartByNo(string principalPartNo);

        /// <summary>
        /// 根据主体编号集合获取列表
        /// </summary>
        /// <param name="principalPartNos"></param>
        /// <param name="appNo"></param>
        /// <returns></returns>
        IList<PrincipalPart> GetPrincipalPartsByNos(List<string> principalPartNos, long applicationSysNo);

        /// <summary>
        /// 获取主体列表
        /// </summary>
        /// <param name="pageSize">页显示数</param>
        /// <param name="pageIndex">显示页数</param>
        /// <param name="filter">过滤条件</param>
        /// <param name="applications">应用域</param>
        /// <param name="count">总数</param>
        /// <returns>资源信息列表</returns>
        DataTable GetPrincipalPartTable(int pageSize, int pageIndex, string filter,string applications,out int count);

        /// <summary>
        /// 为主体添加相应证书
        /// </summary>
        /// <param name="principalPart">主体</param>
        /// <param name="credentialsList">证书集合</param>
        /// <returns></returns>
        bool AddCredentialsToPrincipalPart(PrincipalPart principalPart, List<Credentials> credentialsList);

        /// <summary>
        /// 移除主体持有的上的证书
        /// </summary>
        /// <param name="relationSysNo">证书主体绑定关系表sysNo</param>
        /// <returns></returns>
        bool ReMoveCredentialsInPrincipalPart(long relationSysNo);

        /// <summary>
        /// 获取主体所持有的证书编号
        /// </summary>
        /// <param name="principalSysNo"></param>
        /// <returns></returns>
        IList<long> GetCredentialsByPrincipalSysNo(long principalSysNo);
    }
}
