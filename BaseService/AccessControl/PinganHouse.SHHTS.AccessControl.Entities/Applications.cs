﻿using System;
using System.Collections.Generic;

using PinganHouse.SHHTS.AccessControl.Common;
using PinganHouse.SHHTS.AccessControl.Entities.IDao;
using PinganHouse.SHHTS.AccessControl.Utils;
using PinganHouse.SHHTS.AccessControl.Utils.Log;

namespace PinganHouse.SHHTS.AccessControl.Entities
{
    /// <summary>
    /// 应用程序类
    /// </summary>
    public class Applications:BaseDbEntity
    {

        private const string ClassName = "Applications";

        public static readonly IApplicationsDao ApplicationsDao = DaoProviderService.Instance.Create<IApplicationsDao>();

        /// <summary>
        /// 应用程序编号
        /// </summary>
        public string ApplicationsNo { get; set; }

        /// <summary>
        /// 应用程序标志
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// 应用程序描述
        /// </summary>
        public string ApplicationsDescribe { get; set; }

        /// <summary>
        /// 应用程序名称
        /// </summary>
        public string ApplicationsName { get; set; }



        /// <summary>
        /// 获取证书列表
        /// </summary>
        /// <returns>获取应用程序列表</returns>
        public static List<Applications> GetApplicationsList(long userSysNo,bool isAdmin=false)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var dataTable = ApplicationsDao.GetApplicationsList(userSysNo, isAdmin);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetApplicationsList",
                                   DateTime.Now.Ticks - ts,
                                   "Method GetApplicationsList No Paras"));

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetApplicationsList",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 获取应用
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        public static Applications GetObject(long sysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var obj = ApplicationsDao.GetObject(sysNo);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetObject",
                                   DateTime.Now.Ticks - ts,
                                   "[GetObject:" + sysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetObject",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        public static string GetApplicationSysNo(string appNo)
        {
            return ApplicationsDao.GetApplicationSysNo(appNo);
        }

        /// <summary>
        /// 修改应用
        /// </summary>
        /// <returns></returns>
        public bool UpdateSelf()
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                if (string.IsNullOrEmpty(ApplicationsNo))
                {
                    throw new Exception("ApplicationsNo can not Empty.");
                }
                if (string.IsNullOrEmpty(ApplicationsName))
                {
                    throw new Exception("ApplicationsName can not Empty.");
                }

                var obj = ApplicationsDao.UpdateSelf(this);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "UpdateSelf",
                                   DateTime.Now.Ticks - ts,
                                   "[UpdateSelf:" + SysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "UpdateSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 添加应用
        /// </summary>
        /// <returns></returns>
        public long AddSelf()
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                if (string.IsNullOrEmpty(ApplicationsNo))
                {
                    throw new Exception("ApplicationsNo can not Empty.");
                }
                if (string.IsNullOrEmpty(ApplicationsName))
                {
                    throw new Exception("ApplicationsName can not Empty.");
                }

                SysNo = Sequence.Get();

                var obj = ApplicationsDao.AddSelf(this);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "AddSelf",
                                   DateTime.Now.Ticks - ts,
                                   "[AddSelf:" + SysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "AddSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }
    }
}
