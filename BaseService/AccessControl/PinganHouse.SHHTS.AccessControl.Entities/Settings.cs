﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using PinganHouse.SHHTS.AccessControl.Entities.IDao;
using PinganHouse.SHHTS.AccessControl.Common;
using PinganHouse.SHHTS.AccessControl.Utils;
using PinganHouse.SHHTS.AccessControl.Utils.Log;

namespace PinganHouse.SHHTS.AccessControl.Entities
{
    /// <summary>
    /// 键值对数据对象
    /// </summary>
    public class Settings : BaseDbEntity, IEquatable<Settings>
    {
        private static readonly ISettingsDao SettingsDao = DaoProviderService.Instance.Create<ISettingsDao>();

        private const string ClassName = "SettingsManager";


        #region Property
        private string _key = string.Empty;
        private string _value = string.Empty;

        /// <summary>
        /// 父节点
        /// </summary>
        public Settings Parent { get; set; }

        /// <summary>
        /// 父节点标识列
        /// </summary>
        public long ParentSysNo { get; set; }

        /// <summary>
        /// 键
        /// </summary>
        public string Key
        {
            get { return _key; }
            set
            {
                if (null != value)
                    _key = value;
            }
        }

        /// <summary>
        /// 值
        /// </summary>
        public string Value
        {
            get { return _value; }
            set
            {
                if (null != value)
                    _value = value;
            }
        }



        #endregion

        public Settings(){}

        public Settings(long parentSysNo,string key,string value)
            :this()
        {

        }

        public Settings(Settings parent, string key, string value)
            :this(parent.SysNo,key,value)
        {

        }

        /// <summary>
        /// 是否有子节点
        /// </summary>
        public bool HasSubKeyValueString
        {
            get
            {
                var subKey = new List<Settings>(GetSubKeyValueString());
                return subKey.Count > 0;
            }
        }

        /// <summary>
        /// 获取根节点SysNo
        /// </summary>
        public long RootSysNo
        {
            get { return GetParentSysNo(this); }
        }

        /// <summary>
        /// 递归查询根节点SysNo
        /// </summary>
        /// <param name="settings">当前Settings</param>
        /// <returns>根节点SysNo</returns>
        private static long GetParentSysNo(Settings settings)
        {
            if (settings == null)
                return -1;

            return settings.Parent != null
                ? GetParentSysNo(settings.Parent)
                : settings.SysNo;
        }

        /// <summary>
        /// 取其下面的所有子节点
        /// </summary>
        /// <returns>其下面的所有子节点</returns>
        public IEnumerable<Settings> GetSubKeyValueString()
        {
            var keyValueStringList = GetAllSettingsList();

            return keyValueStringList.FindAll(c =>
                c.Parent != null &&
                c.Parent.SysNo == SysNo);
        }


        /// <summary>
        /// 返回所有子节点SysNo
        /// </summary>
        /// <returns>所有子栏目ID</returns>
        public List<long> GetAllSubKeyValueStringSysNoList()
        {
            var subKeyValueStringList = new List<long>();

            GetAllSubKeyValueString(ref subKeyValueStringList, this);

            return subKeyValueStringList;
        }


        private static void GetAllSubKeyValueString(
            ref List<long> subKeyValueSysNoList,
            Settings settings)
        {
            if (settings == null)
                return;

            subKeyValueSysNoList.Add(settings.SysNo);

            foreach (var subKeyValueString in settings.GetSubKeyValueString())
            {
                GetAllSubKeyValueString(ref subKeyValueSysNoList, subKeyValueString);
            }
        }


        public bool HasDuplicate()
        {
            Settings settings = Get(SysNo);
            return settings != null;
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <returns>影响的行数</returns>
        public int Insert()
        {
            if (HasDuplicate())
            {
                return 0;
            }
            try
            {
                SettingsDao.Insert(this);
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <returns>影响的行数</returns>
        public int Update()
        {
            return HasSubKeyValueString
                ? 0
                : SettingsDao.Update(this);
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="flag">是否移动其子类的标识</param>
        /// <returns></returns>
        public int Update(bool flag)
        {
            if (flag)
            {
                return HasDuplicate()
                    ? 0
                    : SettingsDao.Update(this);
            }
            return Update();
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <returns>影响的行数</returns>
        public int Delete()
        {
            return HasSubKeyValueString
                ? 0
                : SettingsDao.Delete(this);
        }

        public int Delete(bool flag)
        {
            if (!flag)
            {
                return Delete();
            }
            var subKeyValueStringList = new List<long>();
            GetAllSubKeyValueString(ref subKeyValueStringList, this);
            long[] sysNoList = subKeyValueStringList.ToArray();
            return SettingsDao.DeleteAll(sysNoList);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sysNo"></param>
        /// <param name="flag">是否删除其子节点的标识</param>
        /// <returns></returns>
        public static int Delete(long sysNo, bool flag)
        {
            var settings = Get(sysNo);
            return settings == null ? 0 : settings.Delete(flag);
        }

        private static int DeleteAll(long[] sysNoList)
        {
            return SettingsDao.DeleteAll(sysNoList);
        }

        /// <summary>
        /// 根据业务主键获取实体对象
        /// </summary>
        /// <param name="sysNo">业务主键</param>
        /// <returns></returns>
        public static Settings Get(long sysNo)
        {
            return SettingsDao.GetObject(sysNo);
        }


        /// <summary>
        /// 返回所有Settings集合
        /// </summary>
        /// <returns>所有Settings集合</returns>
        public static List<Settings> GetAllSettingsList()
        {
            var settingsesCache = new List<Settings>();
            var settingsTableList = (List<Settings>)SettingsDao.GetKeyValueList();
            var rootSettings = settingsTableList.FindAll(s => s.ParentSysNo == 0);
            settingsesCache.AddRange(rootSettings);
            foreach (var settings in rootSettings)
            {
                CreateParentSubNode(ref settingsesCache, settingsTableList, settings.SysNo);
            }
            return settingsesCache;
        }

        /// <summary>
        /// 建立父节点引用关系(递归体)
        /// </summary>
        private static void CreateParentSubNode(ref List<Settings> mainSettingses, List<Settings> settingsesTable, long parentSysNo)
        {
            List<Settings> subSettingses = settingsesTable.FindAll(s => s.ParentSysNo == parentSysNo);

            foreach (Settings subSettingse in subSettingses)
            {
                subSettingse.Parent = mainSettingses.FirstOrDefault(s => s.SysNo == parentSysNo);
                mainSettingses.Add(subSettingse);
                CreateParentSubNode(ref mainSettingses, settingsesTable, subSettingse.SysNo);
            }
        }


        /// <summary>
        /// 根据节点路线，返回最终子节点value值
        /// </summary>
        /// <returns>子节点Value</returns>
        public static string GetValueByNodeRoutes(params string[] keys)
        {
            Settings parent = null;
            var allsettings = GetAllSettingsList();
            for (var i = 0; i < keys.Length; i++)
            {
                var settings = allsettings.Find(
                    s => s.Key == keys[i] &&
                         s.Parent == parent);

                if (settings == null)
                {
                    if (parent == null)
                        throw new ArgumentOutOfRangeException("不存在 " + (string.IsNullOrEmpty(keys[i]) ? "\"\"" : keys[i]) + " 节点！");

                    throw new ArgumentOutOfRangeException("keys", parent.Key + " 下不存在 " + (string.IsNullOrEmpty(keys[i]) ? "\"\"" : keys[i]) + " 子节点！");
                }

                if (i == keys.Length - 1)
                    return settings.Value;

                parent = settings;
            }

            return null;
        }


        /// <summary>
        /// 根据节点路线，返回最终子节点value值
        /// </summary>
        /// <returns>子节点Value</returns>
        public static Settings GetSettingByNodeRoutes(params string[] keys)
        {
            Settings parent = null;
            var allsettings = GetAllSettingsList();
            for (var i = 0; i < keys.Length; i++)
            {
                var settings = allsettings.Find(
                    s => s.Key == keys[i] &&
                         s.Parent == parent);

                if (settings == null)
                {
                    if (parent == null)
                        throw new ArgumentOutOfRangeException("不存在 " + (string.IsNullOrEmpty(keys[i]) ? "\"\"" : keys[i]) + " 节点！");

                    throw new ArgumentOutOfRangeException("keys", parent.Key + " 下不存在 " + (string.IsNullOrEmpty(keys[i]) ? "\"\"" : keys[i]) + " 子节点！");
                }

                if (i == keys.Length - 1)
                    return settings;

                parent = settings;
            }

            return null;
        }

        /// <summary>
        /// 检索当前节点下所有子节点
        /// </summary>
        /// <param name="sysno">当前节点标识</param>
        /// <returns></returns>
        public static List<Settings> SearchSettingsSonGroup(long sysno)
        {
            return SettingsDao.SearchSettingsSonGroup(sysno);
        }

        /// <summary>
        /// 删除多个Settings节点
        /// </summary>
        /// <param name="sysNoList"></param>
        /// <returns></returns>
        public static int DeleteMultiSettings(List<long> sysNoList)
        {
            long ts = DateTime.Now.Ticks;
            if (sysNoList.Count == 0)
            {
                throw new Exception("删除数量为0");
            }
            return SettingsDao.DeleteMultiSettings(sysNoList);

        }

        public static int InsertSettings(Settings settingsDto, List<Settings> settingsDtos)
        {
            long ts = DateTime.Now.Ticks;
            try
            {
                int rtn = SettingsDao.InsertSettings(settingsDto, settingsDtos);
                Log.Bll(string.Format(Configs.ServerLogFormat, ClassName, "InsertSettings",
                                      DateTime.Now.Ticks - ts, settingsDto.Key));
                return rtn;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat, ClassName, "InsertSettings",
                                        DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        public static int UpdateSettings(List<Settings> settingsList, List<long> delSysNoList)
        {
            long ts = DateTime.Now.Ticks;
            try
            {
                int rtn = SettingsDao.UpdateSettings(settingsList, delSysNoList);
                Log.Bll(string.Format(Configs.ServerLogFormat, ClassName, "UpdateSettings",
                                      DateTime.Now.Ticks - ts, settingsList.Count));
                return rtn;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat, ClassName, "UpdateSettings",
                                        DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 根据节点路径返回最终节点下一级所有Key,Value字典集合
        /// </summary>
        /// <param name="keys">节点路径</param>
        /// <returns></returns>
        public static Dictionary<string, string> GetMultiKeyValue(params string[] keys)
        {
            if (keys == null ||
                keys.Length < 1)
                throw new ArgumentNullException("keys", "获取SettingS时，节点路径不能为空！");

            var allsettings = GetAllSettingsList();
            return GetMultiKeyValue(allsettings, keys);

        }

        /// <summary>
        /// 根据节点路径返回最终节点下一级所有Key,Value字典集合
        /// </summary>
        /// <param name="allsettings">配置集合</param>
        /// <param name="keys">节点路径</param>
        /// <returns></returns>
        public static Dictionary<string, string> GetMultiKeyValue(List<Settings> allsettings, params string[] keys)
        {            
            if (allsettings == null)
            {
                throw new SystemException("获取SettingS缓存异常！！请联系管理员！");
            }
            Settings parent = null;

            foreach (string t in keys)
            {
                var settings = allsettings.Find(
                    s => s != null &&
                         s.Key == t &&
                         s.Parent == parent);

                if (settings == null)
                {
                    if (parent == null)
                        throw new ArgumentOutOfRangeException("不存在 " + (string.IsNullOrEmpty(t) ? "\"\"" : t) + " 节点！");

                    throw new ArgumentOutOfRangeException("keys", parent.Key + " 下不存在 " + (string.IsNullOrEmpty(t) ? "\"\"" : t) + " 子节点！");
                }
                parent = settings;
            }

            var settingses = allsettings.FindAll(s => s.Parent == parent && s.Parent != null);

            return settingses.Count > 0
                ? settingses.ToDictionary(s => s.Key, s => s.Value)
                : new Dictionary<string, string>();
        }


        /// <summary>
        /// 根据节点路径返回最终节点下一级所有Key,Value字典集合
        /// </summary>
        /// <param name="allsettings">配置集合</param>
        /// <param name="keys">节点路径</param>
        /// <returns></returns>
        public static List<Settings> GetMultiSettings(List<Settings> allsettings, params string[] keys)
        {
            if (allsettings == null)
            {
                throw new SystemException("获取SettingS缓存异常！！请联系管理员！");
            }
            Settings parent = null;

            foreach (string t in keys)
            {
                var settings = allsettings.Find(
                    s => s != null &&
                         s.Key == t &&
                         s.Parent == parent);

                if (settings == null)
                {
                    if (parent == null)
                        throw new ArgumentOutOfRangeException("不存在 " + (string.IsNullOrEmpty(t) ? "\"\"" : t) + " 节点！");

                    throw new ArgumentOutOfRangeException("keys", parent.Key + " 下不存在 " + (string.IsNullOrEmpty(t) ? "\"\"" : t) + " 子节点！");
                }
                parent = settings;
            }

            var settingses = allsettings.FindAll(s => s.Parent == parent && s.Parent != null);

            return settingses;
        }


        public bool Equals(Settings other)
        {
            if (other == null)
                return false;

            return SysNo == other.SysNo &&
                   Parent.SysNo == other.Parent.SysNo &&
                   Key == other.Key;
        }

        /// <summary>
        /// 根据节点路径返回最终节点下一级所有Key,Value字典集合
        /// </summary>
        /// <param name="parentSysNo">父节点</param>
        /// <returns></returns>
        public static List<Settings> GetAllNationalAddressList(long parentSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var listCustomer = SettingsDao.GetAllNationalAddressList(parentSysNo);
                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetAllNationalAddressList",
                                   DateTime.Now.Ticks - ts,
                                   "parentSysNo" + parentSysNo));
                return listCustomer;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                  ClassName,
                                  "GetAllNationalAddressList",
                                  DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 返回所有Settings集合
        /// </summary>
        /// <returns>所有Settings集合</returns>
        public static List<Settings> GetAllNationalAddressList()
        {
            var settingsesCache = new List<Settings>();
            var settingsTableList = (List<Settings>)SettingsDao.GetAllNationalAddressList();
            var rootSettings = settingsTableList.FindAll(s => s.ParentSysNo == 0);
            settingsesCache.AddRange(rootSettings);
            foreach (var settings in rootSettings)
            {
                CreateParentSubNode(ref settingsesCache, settingsTableList, settings.SysNo);
            }
            return settingsesCache;
        }
    }
}
