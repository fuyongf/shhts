﻿using PinganHouse.SHHTS.AccessControl.Common.Enum;
using PinganHouse.SHHTS.AccessControl.Entities.IDao;

namespace PinganHouse.SHHTS.AccessControl.Entities
{
    public class AcLog:BaseDbEntity
    {

        public static readonly IAcLogDao LogDao = DaoProviderService.Instance.Create<IAcLogDao>();

        /// <summary>
        /// 应用程序
        /// </summary>
        public string Applications { get; set; }

        /// <summary>
        /// 工作范围
        /// </summary>
        public string OperatingRange { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 影响的表名称
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// 旧的值
        /// </summary>
        public string OldValue { get; set; }

        /// <summary>
        /// 新的值
        /// </summary>
        public string NewValue { get; set; }

        /// <summary>
        /// 级别
        /// </summary>
        public LogLevel Important { get; set; }


        /// <summary>
        /// 插入
        /// </summary>
        /// <returns></returns>
        public bool Insert()
        {
            return LogDao.Insert(this);
        }

        /// <summary>
        /// 插入操作日志
        /// </summary>
        /// <param name="applications"></param>
        /// <param name="description"></param>
        /// <param name="tableName"></param>
        /// <param name="newValue"></param>
        /// <param name="createSysNo"></param>
        /// <param name="createName"></param>
        /// <param name="oldValue"></param>
        /// <param name="important"></param>
        /// <returns></returns>
        public static bool Insert(string applications, string description, string tableName, string newValue,long createSysNo,string createName,string oldValue,LogLevel important = LogLevel.Low)
        {
            var acLog = new AcLog
            {
                Applications = applications,
                Description = description,
                TableName = tableName,
                NewValue = newValue,
                OldValue = oldValue,
                CreateUserSysNo= createSysNo,
                CreateUserName= createName,
                Important = important
            };
            return LogDao.Insert(acLog);
        }
    }
}
