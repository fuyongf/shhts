﻿using System;
using System.Collections.Generic;
using System.Data;

using PinganHouse.SHHTS.AccessControl.Common;
using PinganHouse.SHHTS.AccessControl.Common.Enum;
using PinganHouse.SHHTS.AccessControl.Entities.IDao;
using PinganHouse.SHHTS.AccessControl.Utils;
using PinganHouse.SHHTS.AccessControl.Utils.Log;

namespace PinganHouse.SHHTS.AccessControl.Entities
{
    /// <summary>
    /// 证书[系统角色]
    /// </summary>
    public class Credentials : BaseDbEntity
    {

        private const string ClassName = "Credentials";

        public static readonly ICredentialsDao CredentialsDao = DaoProviderService.Instance.Create<ICredentialsDao>();

        /// <summary>
        /// 默认构造函数
        /// </summary>
        public Credentials(){}

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="credentialsName">证书名称</param>
        /// <param name="applications">证书类型(隶属应用域)</param>
        public Credentials(string credentialsName, long applications)
        {
            CredentialsName = credentialsName;
            Applications = applications;
        }

        /// <summary>
        /// 证书编号
        /// </summary>
        public string CredentialsNo { get; set; }

        /// <summary>
        /// 证书名称
        /// </summary>
        public string CredentialsName { get; set; }

        /// <summary>
        /// 证书隶属应用域
        /// </summary>
        public long Applications { get; set; }

        /// <summary>
        /// 证书描述
        /// </summary>
        public string CredentialsDescribe { get; set; }

        /// <summary>
        /// 默认标识
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// 证书访问规则(冗余属性)
        /// </summary>
        public LimitsRules LimitsRules { get; set; }

        /// <summary>
        /// 添加证书
        /// </summary>
        /// <returns>返回证书的系统唯一编号</returns>
        public long AddSelf()
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                if (string.IsNullOrEmpty(CredentialsName))
                {
                    throw new Exception("CredentialsName can not Empty.");
                }

                SysNo = Sequence.Get();

                var obj = CredentialsDao.AddSelf(this);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "AddSelf",
                                   DateTime.Now.Ticks - ts,
                                   "[AddSelf:" + SysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "AddSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 修改证书
        /// </summary>
        /// <returns>返回值：ture 表示修改成功</returns>
        public bool UpdateSelf()
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                if (string.IsNullOrEmpty(CredentialsName))
                {
                    throw new Exception("CredentialsName can not Empty.");
                }

                var obj = CredentialsDao.UpdateSelf(this);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "UpdateSelf",
                                   DateTime.Now.Ticks - ts,
                                   "[UpdateSelf:" + SysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "UpdateSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 删除证书
        /// </summary>
        /// <param name="credentialsSysNo">证书系统编码</param>
        /// <returns>返回值：ture 表示删除成功</returns>
        public static bool DeleteSelf(long credentialsSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var obj = CredentialsDao.DeleteSelf(credentialsSysNo);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "DeleteSelf",
                                   DateTime.Now.Ticks - ts,
                                   "[DeleteSelf:" + credentialsSysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "DeleteSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 检查证书是否已经存在
        /// </summary>
        /// <param name="name">名称</param>
        /// <param name="type">类型</param>
        /// <returns>true 表示存在该证书</returns>
        public static bool IsExists(string name, string type)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var obj = CredentialsDao.IsExists(name, type);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "IsExists",
                                   DateTime.Now.Ticks - ts,
                                   "[Name:" + name + "]" +
                                   "[Type:" + type + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "IsExists",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 检查证书是否已经存在(排除自身)
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        public static bool IsExistsExcludeSelf(string name, string type, long sysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var obj = CredentialsDao.IsExistsExcludeSelf(name, type, sysNo);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "IsExists",
                                   DateTime.Now.Ticks - ts,
                                   "[Name:" + name + "]" +
                                   "[Type:" + type + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "IsExistsExcludeSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 获取证书
        /// </summary>
        /// <param name="credentialsSysNo">证书系统编码</param>
        /// <returns>返回值：证书对象 object </returns>
        public static Credentials GetObject(long credentialsSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var obj = CredentialsDao.GetObject(credentialsSysNo);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetObject",
                                   DateTime.Now.Ticks - ts,
                                   "[GetObject:" + credentialsSysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetObject",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 根据证书编号查询
        /// </summary>
        /// <param name="credentialsNo"></param>
        /// <returns></returns>
        public static Credentials GetCredentialsByNo(string credentialsNo) 
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var obj = CredentialsDao.GetCredentialsByNo(credentialsNo);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetCredentialsByNo",
                                   DateTime.Now.Ticks - ts,
                                   "[GetCredentialsByNo:" + credentialsNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsByNo",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 获取证书列表
        /// </summary>
        /// <param name="pageSize">页显示数</param>
        /// <param name="pageIndex">显示页数</param>
        /// <param name="filter">过滤条件</param>
        /// <param name="applications">应用域</param>
        /// <param name="count">总数</param>
        /// <returns>证书信息列表</returns>
        public static DataTable GetCredentialsTable(int pageSize, int pageIndex, string filter,string applications,out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var dataTable = CredentialsDao.GetCredentialsTable(pageSize, pageIndex, filter,applications, out count);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetCredentialsTable",
                                   DateTime.Now.Ticks - ts,
                                   "[PageSize:" + pageSize + "]" +
                                   "[PageIndex:" + pageIndex + "]"));

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsTable",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 查询所属某个资源的证书列表
        /// </summary>
        /// <param name="credentialsSysNoList">证书系统编号集合</param>
        /// <returns>返回DataTable</returns>
        public static List<Credentials> GetCredentialsListByCredentialsSysNoList(List<long> credentialsSysNoList)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var credentialsList = CredentialsDao.GetCredentialsListByCredentialsSysNoList(credentialsSysNoList);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetCredentialsListByCredentialsSysNoList",
                                   DateTime.Now.Ticks - ts,
                                   "[CredentialsSysNoList:" + credentialsSysNoList.Count + "]"));

                return credentialsList;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsListByCredentialsSysNoList",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 查询所属某个资源的证书列表
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="resource">资源编号</param>
        /// <param name="page"></param>
        /// <param name="count"></param>
        /// <returns>返回DataTable</returns>
        public static DataTable GetCredentialsTableListInResource(int page, int rows, string resource, out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var credentialsList = CredentialsDao.GetCredentialsTableListInResource(page, rows, resource, out count);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetCredentialsTableListInResource",
                                   DateTime.Now.Ticks - ts,
                                   "[Resource SysNo:" + resource + "]"));

                return credentialsList;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsTableListInResource",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 返回资源所绑定的所有证书(仅返回证书)
        /// </summary>
        /// <returns></returns>
        public static DataTable GetCredentialsTableListInResource(string resourceNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var credentialsList = CredentialsDao.GetCredentialsTableListInResource(resourceNo);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetCredentialsTableListInResource",
                                   DateTime.Now.Ticks - ts,
                                   "[select all credentialsList in resource]"));

                return credentialsList;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsTableListInResource",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 查询所属某个主体持有的证书列表
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="principalPart">主体系统编号</param>
        /// <param name="page"></param>
        /// <param name="count"></param>
        /// <returns>返回DataTable</returns>
        public static DataTable GetCredentialsTableListInPrincipalPart(int page, int rows, string principalPart, out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var credentialsList = CredentialsDao.GetCredentialsTableListInPrincipalPart(page, rows, principalPart, out count);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetCredentialsTableListInPrincipalPart",
                                   DateTime.Now.Ticks - ts,
                                   "[PrincipalPart SysNo:" + principalPart + "]"));

                return credentialsList;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsTableListInPrincipalPart",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 查询所属某个主体持有的证书列表
        /// </summary>
        /// <returns>返回DataTable</returns>
        public static DataTable GetCredentialsTableListInPrincipalPart(string principalPartNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var credentialsList = CredentialsDao.GetCredentialsTableListInPrincipalPart(principalPartNo);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetCredentialsTableListInPrincipalPart",
                                   DateTime.Now.Ticks - ts,
                                   "[select all credentialsList in principalPart]"));

                return credentialsList;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsTableListInPrincipalPart",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }
        
        /// <summary>
        /// 获取指定资源未绑定的证书
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="resource"></param>
        /// <param name="applications">应用域</param>
        /// <param name="count"></param>
        /// <param name="filter">过滤</param>
        /// <returns>返回证书列表</returns>
        public static DataTable GetCredentialsTableListOutResource(int pageSize, int pageIndex, string resource, string filter, string applications, out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var credentialsList = CredentialsDao.GetCredentialsTableListOutResource(pageSize, pageIndex, resource,filter, applications,out count);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetCredentialsTableListOutResource",
                                   DateTime.Now.Ticks - ts,
                                   "[Resource SysNo:" + resource + "]"));

                return credentialsList;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsTableListOutResource",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 获取指定资源未主体的证书
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="principalPart"></param>
        /// <param name="applications"></param>
        /// <param name="count"></param>
        /// <param name="filter"></param>
        /// <returns>返回证书列表</returns>
        public static DataTable GetCredentialsTableListOutPrincipalPart(int pageSize, int pageIndex, string principalPart, string filter, string applications, out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var credentialsList = CredentialsDao.GetCredentialsTableListOutPrincipalPart(pageSize, pageIndex, principalPart,filter, applications, out count);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetCredentialsTableListOutPrincipalPart",
                                   DateTime.Now.Ticks - ts,
                                   "[PrincipalPart SysNo:" + principalPart + "]"));

                return credentialsList;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsTableListOutPrincipalPart",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }


        /// <summary>
        /// 获取资源和主体同时用户的证书集合
        /// </summary>
        /// <param name="resourceSysNo">资源系统编号</param>
        /// <param name="principalPartSysNoList">主体系统编号</param>
        /// <returns>证书集合</returns>
        public static DataTable GetCredentialsTableListInResourceAndPrincipalPart(long resourceSysNo, List<long> principalPartSysNoList)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var credentialsTable = CredentialsDao.GetCredentialsTableListInResourceAndPrincipalPart(resourceSysNo, principalPartSysNoList);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetCredentialsTableListInResourceAndPrincipalPart",
                                   DateTime.Now.Ticks - ts,
                                   "[Resource SysNo:" + resourceSysNo + "]"));

                return credentialsTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetCredentialsTableListInResourceAndPrincipalPart",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 删除前检查
        /// </summary>
        /// <param name="credentialsSysNo"></param>
        /// <returns></returns>
        public static bool CheckBeforeDel(long credentialsSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var credentialsTable = CredentialsDao.CheckBeforeDel(credentialsSysNo);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "CheckBeforeDel",
                                   DateTime.Now.Ticks - ts,
                                   "[Credentials SysNo:" + credentialsSysNo + "]"));

                return credentialsTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "CheckBeforeDel",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }
    }
}
