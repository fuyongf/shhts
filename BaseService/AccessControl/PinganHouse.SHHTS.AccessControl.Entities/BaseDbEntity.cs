﻿using System;

namespace PinganHouse.SHHTS.AccessControl.Entities
{
    /// <summary>
    /// 数据基类
    /// </summary>
    public class BaseDbEntity
    {
        private string _createUserName;
        private string _modifyUserName;

        /// <summary>
        /// 主标识
        /// </summary>
        public long SysNo { get; set; }

        /// <summary>
        /// 创建者标识
        /// </summary>
        public long CreateUserSysNo { get; set; }

        /// <summary>
        /// 创建者姓名
        /// </summary>
        public string CreateUserName
        {
            get { return _createUserName; }
            set
            {
                if (null != value)
                    _createUserName = value;
            }
        }

        /// <summary>
        /// 记录创建时间
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// 更新人标识
        /// </summary>
        public long ModifyUserSysNo { get; set; }

        /// <summary>
        /// 更新人名称
        /// </summary>
        public string ModifyUserName
        {
            get { return _modifyUserName; }
            set
            {
                if (null != value)
                    _modifyUserName = value;
            }
        }

        /// <summary>
        /// 记录更新时间
        /// </summary>
        public DateTime ModifyDate { get; set; }

        /// <summary>
        /// 指示是否为新创建的对象
        /// </summary>
        public bool IsNewObject { get; protected set; }

    }
}
