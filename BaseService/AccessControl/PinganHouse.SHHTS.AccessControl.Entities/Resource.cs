﻿using System;
using System.Collections.Generic;
using System.Data;

using PinganHouse.SHHTS.AccessControl.Common;
using PinganHouse.SHHTS.AccessControl.Common.Enum;
using PinganHouse.SHHTS.AccessControl.Entities.IDao;
using PinganHouse.SHHTS.AccessControl.Utils.Log;
using PinganHouse.SHHTS.AccessControl.Utils;

namespace PinganHouse.SHHTS.AccessControl.Entities
{
    /// <summary>
    /// 资源
    /// </summary>
    public class Resource : BaseDbEntity
    {
        private const string ClassName = "Resource";

        public static readonly IResourceDao ResourceDao = DaoProviderService.Instance.Create<IResourceDao>();

        /// <summary>
        /// 资源编号
        /// </summary>
        public long ResourceNo { get; set; }

        /// <summary>
        /// 资源组别
        /// </summary>
        public string GroupName { get; set; }

        /// <summary>
        /// 资源名称
        /// </summary>
        public string ResourceName { get; set; }

        /// <summary>
        /// 父资源名
        /// </summary>
        public string ParentNo { get; set; }

        /// <summary>
        /// 资源类别 隶属应用程序
        /// </summary>
        public long Applications { get; set; }

        /// <summary>
        /// 资源描述
        /// </summary>
        public string ResourcDescribe { get; set; }

        /// <summary>
        /// 添加资源
        /// </summary>
        /// <returns>返回资源的系统唯一编号</returns>
        public long AddSelf()
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                if (Applications==0)
                {
                    throw new Exception("Applications can not Empty.");
                }

                SysNo =Sequence.Get();

                var obj = ResourceDao.AddSelf(this);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "AddSelf",
                                   DateTime.Now.Ticks - ts,
                                   "[AddSelf:" + SysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "AddSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 修改资源
        /// </summary>
        /// <returns>返回值：ture 表示修改成功</returns>
        public bool UpdateSelf()
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                if (Applications==0)
                {
                    throw new Exception("Applications can not Empty.");
                }

                var obj = ResourceDao.UpdateSelf(this);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "UpdateSelf",
                                   DateTime.Now.Ticks - ts,
                                   "[UpdateSelf:" + SysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "UpdateSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 删除资源
        /// </summary>
        /// <param name="resourceSysNo">资源系统编码</param>
        /// <returns>返回值：ture 表示删除成功</returns>
        public static bool DeleteSelf(long resourceSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var obj = ResourceDao.DeleteSelf(resourceSysNo);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "DeleteSelf",
                                   DateTime.Now.Ticks - ts,
                                   "[DeleteSelf:" + resourceSysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "DeleteSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 获取资源
        /// </summary>
        /// <param name="resourceSysNo">资源系统编码</param>
        /// <returns>返回值：资源对象 object </returns>
        public static Resource GetObject(long resourceSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var obj = ResourceDao.GetObject(resourceSysNo);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetObject",
                                   DateTime.Now.Ticks - ts,
                                   "[Resource SysNo:" + resourceSysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetObject",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 获取资源
        /// </summary>
        /// <param name="resourceNo">资源系统编码</param>
        /// <returns>返回值：资源对象 object </returns>
        public static Resource GetObjectByResourceNo(string resourceNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var obj = ResourceDao.GetObjectByResourceNo(resourceNo);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetObjectByresourceNo",
                                   DateTime.Now.Ticks - ts,
                                   "[Resource No:" + resourceNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetObject",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }


        /// <summary>
        /// 检查资源是否已经存在
        /// </summary>
        /// <param name="resourceName">资源编号</param>
        /// <param name="resourceType">资源类型</param>
        /// <returns>true 表示存在该资源</returns>
        public static bool IsExists(string resourceName, string resourceType)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var obj = ResourceDao.IsExists(resourceName, resourceType);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "IsExists",
                                   DateTime.Now.Ticks - ts,
                                   "[Resource Name:" + resourceName + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "IsExists",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 检查资源是否已经存在但排除自身
        /// </summary>
        /// <param name="resourceName">资源名称</param>
        /// <param name="resourceType">资源类型</param>
        /// <param name="sysNo">系统编号</param>
        /// <returns>true 表示存在该资源</returns>
        public static bool IsExistsExcludeSelf(string resourceName, string resourceType,long sysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var obj = ResourceDao.IsExistsExcludeSelf(resourceName, resourceType, sysNo);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "IsExistsExcludeSelf",
                                   DateTime.Now.Ticks - ts,
                                   "[Resource Name:" + resourceName + "]" +
                                   "[Resource SysNo:" + sysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "IsExistsExcludeSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 为资源添加证书操作
        /// </summary>
        /// <param name="resource">资源对象</param>
        /// <param name="credentialsList">证书对象集合</param>
        /// <returns></returns>
        public static bool AddCredentialsToResource(Resource resource, List<Credentials> credentialsList)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj = ResourceDao.AddCredentialsToResource(resource, credentialsList);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "AddCredentialsToResource",
                                   DateTime.Now.Ticks - ts,
                                   "[Resource SysNo:" + resource.SysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "AddCredentialsToResource",
                                DateTime.Now.Ticks - ts, e));                
                throw;
            }
        }

        /// <summary>
        /// 移除资源上的证书
        /// </summary>
        /// <param name="relationSysNo">证书资源绑定关系表sysNo</param>
        /// <returns></returns>
        public static bool ReMoveCredentialsInResource(long relationSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj = ResourceDao.ReMoveCredentialsInResource(relationSysNo);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "ReMoveCredentialsInResource",
                                   DateTime.Now.Ticks - ts,
                                   "[Relation SysNo:" + relationSysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "ReMoveCredentialsInResource",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 获取所属某个证书的所有资源列表
        /// </summary>
        /// <param name="creSysNo"></param>
        /// <returns></returns>
        public static DataTable GetResourceListByCredentialsSysNo(long creSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var res = ResourceDao.GetResourceListByCredentialsSysNo(creSysNo);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetResourceListByCredentialsSysNo",
                                   DateTime.Now.Ticks - ts,
                                   "[Credentials SysNo:" + creSysNo + "]"));

                return res;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetResourceListByCredentialsSysNo",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 获取资源列表
        /// </summary>
        /// <param name="pageSize">页显示数</param>
        /// <param name="pageIndex">显示页数</param>
        /// <param name="filter">过滤条件</param>
        /// <param name="applications">应用程序域</param>
        /// <param name="count">总数</param>
        /// <returns>资源信息列表</returns>
        public static DataTable GetResourceTable(int pageSize, int pageIndex, string filter,string applications, out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var dataTable = ResourceDao.GetResourceTable(pageSize, pageIndex, filter,applications, out count);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetResourceTable",
                                   DateTime.Now.Ticks - ts,
                                   "[Resource PageSize:" + pageSize + "]" +
                                   "[Resource PageIndex:" + pageIndex + "]"));

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetResourceTable",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 为证书设置访问资源的访问规则
        /// </summary>
        /// <param name="dic">对应关系表SysNo,以及访问规则</param>
        /// <returns>T:全部设置成功 F:设置发生错误</returns>
        public static bool SetCredentialsRulesListToResource(Dictionary<long, LimitsRules> dic)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var result = ResourceDao.SetCredentialsRulesListToResource(dic);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "SetCredentialsRulesListToResource",
                                   DateTime.Now.Ticks - ts,
                                   "[Credentials PageIndex:" + dic.Count + "]"));

                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "SetCredentialsRulesListToResource",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        public static DataRowCollection GetResoureRulesByCredentialsSysNo(List<long> credentialsSysNo)
        {
            return ResourceDao.GetResoureRulesByCredentialsSysNo(credentialsSysNo);
        }
    }
}
