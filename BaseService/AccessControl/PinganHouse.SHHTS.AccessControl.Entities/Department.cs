﻿namespace PinganHouse.SHHTS.AccessControl.Entities
{
    public class Department : BaseDbEntity
    {
        private const string ClassName = "Department";

        /// <summary>
        /// 部门编号
        /// </summary>
        public string DepartmentNo { get; set; }

        /// <summary>
        /// 部门名称
        /// </summary>
        public string DepartmentName { get; set; }

    }
}
