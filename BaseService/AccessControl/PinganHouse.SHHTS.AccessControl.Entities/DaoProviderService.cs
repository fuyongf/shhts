﻿using System;
using HTB.DevFx;

namespace PinganHouse.SHHTS.AccessControl.Entities
{
    public sealed class DaoProviderService
    {
        public readonly static DaoProviderService Instance;


        static DaoProviderService()
        {
            Instance = new DaoProviderService();
        }

        public T Create<T>()
            where T : class
        {
            return ObjectService.CreateObject<T>();
        }
    }
}
