﻿using System;
using System.Data;

using PinganHouse.SHHTS.AccessControl.Common;
using PinganHouse.SHHTS.AccessControl.Entities.IDao;
using PinganHouse.SHHTS.AccessControl.Utils;
using PinganHouse.SHHTS.AccessControl.Utils.Log;

namespace PinganHouse.SHHTS.AccessControl.Entities
{
    /// <summary>
    /// 用户
    /// </summary>
    public class User : BaseDbEntity
    {
        private const string ClassName = "User";

        public static readonly IUserDao UserDao = DaoProviderService.Instance.Create<IUserDao>();

        /// <summary>
        /// 隶属应用程序
        /// </summary>
        public long Applications { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 用户编号
        /// </summary>
        public string UserNo { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 密码确认
        /// </summary>
        public string ConfirmPassword { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// 域账号
        /// </summary>
        public string DomainName { get; set; }

        /// <summary>
        /// 域帐户（邮箱地址）
        /// </summary>
        public string EmailAddr { get; set; }

        /// <summary>
        /// 部门编号
        /// </summary>
        public string Department { get; set; }

        /// <summary>
        /// 职位编号
        /// </summary>
        public string Post { get; set; }


        /// <summary>
        /// 是否禁用
        /// </summary>
        public bool IsEnable { get; set; }


        /// <summary>
        /// 是否超级管理员
        /// </summary>
        public bool IsAdmin { get; set; }

        /// <summary>
        /// 添加用户
        /// </summary>
        /// <returns>返回用户的系统唯一编号</returns>
        public long AddSelf()
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                SysNo = Sequence.Get();

                var obj = UserDao.AddSelf(this);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "AddSelf",
                                   DateTime.Now.Ticks - ts,
                                   "[AddSelf:" + SysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "AddSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 修改用户
        /// </summary>
        /// <returns>返回值：ture 表示修改成功</returns>
        public bool UpdateSelf()
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var obj = UserDao.UpdateSelf(this);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "UpdateSelf",
                                   DateTime.Now.Ticks - ts,
                                   "[UpdateSelf:" + SysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "UpdateSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="userSysNo">资源系统编码</param>
        /// <returns>返回值：ture 表示删除成功</returns>
        public static bool DeleteSelf(long userSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var obj = UserDao.DeleteSelf(userSysNo);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "DeleteSelf",
                                   DateTime.Now.Ticks - ts,
                                   "[DeleteSelf:" + userSysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "DeleteSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 获取用户
        /// </summary>
        /// <param name="userSysNo">用户系统编码</param>
        /// <returns>返回值：用户对象 object </returns>
        public static User GetObject(long userSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var obj = UserDao.GetObject(userSysNo);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetObject",
                                   DateTime.Now.Ticks - ts,
                                   "[GetObject:" + userSysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetObject",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 根据工号查询
        /// </summary>
        /// <param name="userNo">工号</param>
        /// <returns></returns>
        public static User GetUserByUserNo(string userNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var obj = UserDao.GetUserByUserNo(userNo);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetUserByUserNo",
                                   DateTime.Now.Ticks - ts,
                                   "[GetUserByUserNo:" + userNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetUserByUserNo",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 根据域账号查询
        /// </summary>
        /// <param name="domainName">域账号</param>
        /// <returns></returns>
        public static User GetUserByDomainName(string domainName)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var obj = UserDao.GetUserByDomainName(domainName);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetUserByDomainName",
                                   DateTime.Now.Ticks - ts,
                                   "[GetUserByDomainName:" + domainName + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetUserByDomainName",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 根据域帐户（邮箱地址）查询
        /// </summary>
        /// <param name="emailAddr">域帐户（邮箱地址）</param>
        /// <returns></returns>
        public static User GetUserByEmailAddr(string emailAddr)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var obj = UserDao.GetUserByEmailAddr(emailAddr);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetUserByEmailAddr",
                                   DateTime.Now.Ticks - ts,
                                   "[GetUserByEmailAddr:" + emailAddr + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetUserByEmailAddr",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <param name="pageSize">页显示数</param>
        /// <param name="pageIndex">显示页数</param>
        /// <param name="count">总数</param>
        /// <param name="filter">查询条件</param>
        /// <returns>用户信息列表</returns>
        public static DataTable GetUserTable(int pageSize, int pageIndex, string filter, out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var dataTable = UserDao.GetUserTable(pageSize, pageIndex, filter,out count);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetUserTable",
                                   DateTime.Now.Ticks - ts,
                                    "[pageIndex:" + pageIndex + "]"));

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetUserTable",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }


        /// <summary>
        /// 查询为被某应用添加的系统员
        /// </summary>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="applications">应用域系统编号</param>
        /// <param name="filter"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static DataTable GetUserTableListOutApplications(int page, int rows, string applications, string filter, out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var userList = UserDao.GetUserTableListOutApplications(page, rows, applications, filter, out count);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetUserTableListOutApplications",
                                   DateTime.Now.Ticks - ts,
                                   "[Applications SysNo:" + applications + "]"));

                return userList;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetUserTableListOutApplications",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 查询为被某应用添加的系统员
        /// </summary>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="applications">应用域系统编号</param>
        /// <param name="filter"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static DataTable GetUserTableListInApplications(int page, int rows, string applications, string filter, out int count)
        {
            var ts = DateTime.Now.Ticks;
            count = 0;
            try
            {
                if (string.IsNullOrWhiteSpace(applications))
                    return null;
                long appSysNo = 0;
                if (!long.TryParse(applications,out appSysNo))
                    return null;
                var userList = UserDao.GetUserTableListInApplications(page, rows, applications, filter, out count);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetUserTableListInApplications",
                                   DateTime.Now.Ticks - ts,
                                   "[Applications SysNo:" + applications + "]"));

                return userList;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetUserTableListInApplications",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 添加人员到应用
        /// </summary>
        /// <param name="applicationsEntity"></param>
        /// <param name="userListEntity"></param>
        /// <returns></returns>
        public static bool AddUserListToApplications(Applications applicationsEntity, System.Collections.Generic.List<User> userListEntity)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj = UserDao.AddUserListToApplications(applicationsEntity, userListEntity);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "AddUserListToApplications",
                                   DateTime.Now.Ticks - ts,
                                   "[Applications SysNo:" + applicationsEntity.SysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "AddUserListToApplications",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 移除管理人员
        /// </summary>
        /// <param name="relationSysNo"></param>
        /// <returns></returns>
        public static bool RemoveUserInApplications(int relationSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj = UserDao.RemoveUserInApplications(relationSysNo);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "ReMoveCredentialsInResource",
                                   DateTime.Now.Ticks - ts,
                                   "[Relation SysNo:" + relationSysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "ReMoveCredentialsInResource",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }
    }
}
