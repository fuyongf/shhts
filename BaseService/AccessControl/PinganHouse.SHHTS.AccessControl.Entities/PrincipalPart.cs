﻿using System;
using System.Collections.Generic;
using System.Data;

using PinganHouse.SHHTS.AccessControl.Common;
using PinganHouse.SHHTS.AccessControl.Entities.IDao;
using PinganHouse.SHHTS.AccessControl.Utils;
using PinganHouse.SHHTS.AccessControl.Utils.Log;


namespace PinganHouse.SHHTS.AccessControl.Entities
{
    public class PrincipalPart : BaseDbEntity
    {
        private const string ClassName = "PrincipalPart";

        public static readonly IPrincipalPartDao PrincipalPartDao = DaoProviderService.Instance.Create<IPrincipalPartDao>();

        /// <summary>
        /// 主体编号
        /// </summary>
        public string PrincipalPartNo { get; set; }

        /// <summary>
        /// 主体名称
        /// </summary>
        public string PrincipalPartName { get; set; }

        /// <summary>
        /// 应用程序
        /// </summary>
        public long Applications { get; set; }

        /// <summary>
        /// 主体组
        /// </summary>
        public string PrincipalPartGroup { get; set; }

        /// <summary>
        /// 主体描述
        /// </summary>
        public string PrincipalPartDescribe { get; set; }


        /// <summary>
        /// 添加主体
        /// </summary>
        /// <returns>返回系统主体的系统唯一编号</returns>
        public long AddSelf()
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                if (string.IsNullOrEmpty(PrincipalPartNo))
                {
                    throw new Exception("PrincipalPartNo can not Empty.");
                }
                if (string.IsNullOrEmpty(PrincipalPartName))
                {
                    throw new Exception("PrincipalPartName can not Empty.");
                }

                SysNo = Sequence.Get();

                var obj = PrincipalPartDao.AddSelf(this);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "AddSelf",
                                   DateTime.Now.Ticks - ts,
                                   "[AddSelf:" + SysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "AddSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 修改主体
        /// </summary>
        /// <returns>返回值：ture 表示修改成功</returns>
        public bool UpdateSelf()
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                if (string.IsNullOrEmpty(PrincipalPartNo))
                {
                    throw new Exception("PrincipalPartNo can not Empty.");
                }
                if (string.IsNullOrEmpty(PrincipalPartName))
                {
                    throw new Exception("PrincipalPartName can not Empty.");
                }

                var obj = PrincipalPartDao.UpdateSelf(this);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "UpdateSelf",
                                   DateTime.Now.Ticks - ts,
                                   "[UpdateSelf:" + SysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "UpdateSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }


        /// <summary>
        /// 删除主体
        /// </summary>
        /// <param name="sysNo">主体系统编码</param>
        /// <returns>返回值：ture 表示删除成功</returns>
        public static bool DeleteSelf(long sysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var obj = PrincipalPartDao.DeleteSelf(sysNo);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "DeleteSelf",
                                   DateTime.Now.Ticks - ts,
                                   "[DeleteSelf:" + sysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "DeleteSelf",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 获取主体
        /// </summary>
        /// <param name="sysNo">主体系统编码</param>
        /// <returns>返回值：主体对象 object </returns>
        public static PrincipalPart GetObject(long sysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var obj = PrincipalPartDao.GetObject(sysNo);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetObject",
                                   DateTime.Now.Ticks - ts,
                                   "[PrincipalPart SysNo:" + sysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetObject",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 根据主体编号查询
        /// </summary>
        /// <param name="principalPartNo"></param>
        /// <returns></returns>
        public static PrincipalPart GetPrincipalPartByNo(string principalPartNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var obj = PrincipalPartDao.GetPrincipalPartByNo(principalPartNo);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetPrincipalPartByNo",
                                   DateTime.Now.Ticks - ts,
                                   "[PrincipalPart No:" + principalPartNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetPrincipalPartByNo",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 根据主体业务编号获取主体列表
        /// </summary>
        /// <param name="principalPartNos"></param>
        /// <returns></returns>
        public static IList<PrincipalPart> GetPrincipalPartsByNos(List<string> principalPartNos, long applicationSysNo)
        {
            try
            {
                return PrincipalPartDao.GetPrincipalPartsByNos(principalPartNos, applicationSysNo);
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }


        /// <summary>
        /// 获取主体列表
        /// </summary>
        /// <param name="pageSize">页显示数</param>
        /// <param name="pageIndex">显示页数</param>
        /// <param name="filter">过滤条件</param>
        /// <param name="applications">应用程序域</param>
        /// <param name="count">总数</param>
        /// <returns>资源信息列表</returns>
        public static DataTable GetPrincipalPartTable(int pageSize, int pageIndex, string filter,string applications, out int count)
        {
            var ts = DateTime.Now.Ticks;
            try
            {

                var dataTable = PrincipalPartDao.GetPrincipalPartTable(pageSize, pageIndex, filter,applications,out count);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "GetPrincipalPartTable",
                                   DateTime.Now.Ticks - ts,
                                   "[PrincipalPart PageSize:" + pageSize + "]" +
                                   "[PrincipalPart PageIndex:" + pageIndex + "]"));

                return dataTable;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "GetPrincipalPartTable",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 为主体添加相应证书
        /// </summary>
        /// <param name="principalPart">主体</param>
        /// <param name="credentialsList">证书集合</param>
        /// <returns></returns>
        public static bool AddCredentialsToPrincipalPart(PrincipalPart principalPart, List<Credentials> credentialsList)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj = PrincipalPartDao.AddCredentialsToPrincipalPart(principalPart, credentialsList);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "AddCredentialsToPrincipalPart",
                                   DateTime.Now.Ticks - ts,
                                   "[PrincipalPart SysNo:" + principalPart.SysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "AddCredentialsToPrincipalPart",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        /// <summary>
        /// 移除主体持有的上的证书
        /// </summary>
        /// <param name="relationSysNo">证书主体绑定关系表sysNo</param>
        /// <returns></returns>
        public static bool ReMoveCredentialsInPrincipalPart(long relationSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var obj = PrincipalPartDao.ReMoveCredentialsInPrincipalPart(relationSysNo);

                Log.Entity(string.Format(Configs.ServerLogFormat,
                                   ClassName,
                                   "ReMoveCredentialsInPrincipalPart",
                                   DateTime.Now.Ticks - ts,
                                   "[Relation SysNo:" + relationSysNo + "]"));

                return obj;
            }
            catch (Exception e)
            {
                Log.Error(string.Format(Configs.ServerLogFormat,
                                ClassName,
                                "ReMoveCredentialsInPrincipalPart",
                                DateTime.Now.Ticks - ts, e));
                throw;
            }
        }

        public static IList<long> GetCredentialsByPrincipalSysNo(long principalSysNo)
        {
            return PrincipalPartDao.GetCredentialsByPrincipalSysNo(principalSysNo);
        }
    }
}
