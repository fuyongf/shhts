﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using HTB.DevFx;

namespace PinganHouse.SHHTS.AccessControl.Proxy.Utils
{
    internal static class LogHandler
    {
        private static ILogService m_Logger = null;

        static LogHandler() 
        {
            try
            {
                m_Logger = ObjectService.GetObject<ILogService>();
            }
            catch { }
        }

        public static ILogService Logger 
        {
            get 
            {
                return m_Logger;
            }
        }

        public static void Write(string message) 
        {
            if (Logger != null)
            {
                Logger.Write(message);
            }
        }
    }
}
