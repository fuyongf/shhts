﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.AccessControl.Proxy.Utils
{
    public static class PermissionHelp
    {
        public static T[] GetAttributeFromRuntimeStack<T>(bool includeAll) where T : Attribute
        {
            List<T> list = new List<T>();
            StackTrace trace = new StackTrace(false);
            for (int i = 0; i < trace.FrameCount; i++)
            {
                T[] customAttributes = Attribute.GetCustomAttributes(trace.GetFrame(i).GetMethod(), typeof(T)) as T[];
                if ((customAttributes != null) && (customAttributes.Length > 0))
                {
                    list.AddRange(customAttributes);
                    if (!includeAll)
                    {
                        break;
                    }
                }
            }
            return list.ToArray();
        }
    }
}
