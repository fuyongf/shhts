﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.AccessControl.Proxy.Utils
{
    public interface ILogService
    {
        /// <summary>
        /// 记录日志
        /// </summary>
        /// <param name="message"></param>
        void Write(string message);
    }
}
