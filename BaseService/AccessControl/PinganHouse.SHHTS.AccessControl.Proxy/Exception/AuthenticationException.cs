﻿using System;

namespace PinganHouse.SHHTS.AccessControl.Proxy
{
    /// <summary>
    ///     用户身份认证失败时的异常信息
    /// </summary>
    public class AuthenticationException : UnauthorizedAccessException
    {
        public AuthenticationException()
        {
        }

        public AuthenticationException(string message) : base(message)
        {
        }
    }
}