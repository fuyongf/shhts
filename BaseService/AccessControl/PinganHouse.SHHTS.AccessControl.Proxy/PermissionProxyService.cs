﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Configuration;

using HTB.DevFx;

using PinganHouse.SHHTS.AccessControl.ServiceContracts.DTO;
using PinganHouse.SHHTS.AccessControl.ServiceContracts;
using PinganHouse.SHHTS.AccessControl.Proxy.Utils;

namespace PinganHouse.SHHTS.AccessControl.Proxy
{
    public class PermissionProxyService
    {
        readonly static bool permissionSwitsh = false;
        readonly static string acAppNo = string.Empty;
        readonly static PermissionProxyService permissionProxyService = new PermissionProxyService();
        protected IPermissionService PermissionService {
            get { return ObjectService.GetObject<IPermissionService>(); }
        }

        static PermissionProxyService()
        {
            bool.TryParse(ConfigurationManager.AppSettings["permissionSwitsh"], out permissionSwitsh);
            acAppNo = ConfigurationManager.AppSettings["ACAppNo"];
        }

        private PermissionProxyService()
        {

        }

        public static PermissionProxyService GetInstance()
        {
            return permissionProxyService;
        }     

        /// <summary>
        /// 根据角色编号获取此角色下所有资源信息
        /// </summary>
        /// <param name="roleNo">角色(证书)编号</param>
        /// <returns></returns>
        private IList<ResourceInfo> GetResourcesByPrincipalNos(IList<string> principalNos, string appNo)
        {
            if (principalNos == null || principalNos.Count == 0 || string.IsNullOrEmpty(appNo))
                return null;

            List<ResourceInfo> resources = new List<ResourceInfo>();
            List<string> queryStrings = new List<string>(); 
            foreach (string principalNo in principalNos)
            {
                var resCache = HttpRuntime.Cache[principalNo] as IList<ResourceInfo>;
                if (resCache == null)
                {
                    queryStrings.Add(principalNo);
                    continue;
                }
                resources.AddRange(resCache);
            }
            if (queryStrings.Count != 0)
            {
                this.GetResourcesByPrincipalNosInternal(queryStrings, appNo, ref resources);
            }
            return resources;
        }


        private void GetResourcesByPrincipalNosInternal(List<string> principalNos, string appNo, ref List<ResourceInfo> resources)
        {
            try
            {
                int cacheMinute = 0;
                var result = PermissionService.GetResourcesByPrincipalNos(principalNos, appNo, ref cacheMinute);
                if (result != null && result.Count > 0)
                {
                    foreach (var r in result)
                    {
                        resources.AddRange(r.Value);
                        HttpRuntime.Cache.Insert(r.Key, r.Value, null, DateTime.Now.AddMinutes(cacheMinute), TimeSpan.Zero);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.ToString());
            }
        }

        #region 客户端核心验证
        /// <summary>
        ///     授权认证
        /// </summary>
        /// <param name="permissionCode">权限编码</param>
        /// <param name="attach">是否从调用堆栈获取权限</param>
        /// <param name="includeStack">认证失败时是否抛出异常</param>
        /// <param name="throwOnFailed"></param>
        /// <param name="isAuthenticated">是否已验证</param>
        /// <returns>是否认证成功</returns>
        public bool Authorize(string permissionCode = null, string appNo = null, IList<string> principalNos = null, Hashtable attach = null, bool includeStack = true,
            bool throwOnFailed = false, bool isAuthenticated=true)
        {
            return Authorize(new ResourceInfo { ResourceNo = permissionCode, AppNo=appNo, Attach = attach }, includeStack,
                throwOnFailed, principalNos, isAuthenticated);
        }

        /// <summary>
        ///     授权认证
        /// </summary>
        /// <param name="permission">权限对象</param>
        /// <param name="includeStack">是否包含堆栈中的权限对象信息</param>
        /// <param name="throwOnFailed">认证失败时是否抛出异常</param>
        /// <param name="isAuthenticated">是否已验证</param>
        /// <returns>是否认证成功</returns>
        public bool Authorize(IResourceInfo permission, bool includeStack = true, bool throwOnFailed = false, IList<string> principalNos = null, bool isAuthenticated=true)
        {
            List<IResourceInfo> list = includeStack ? GetPermissionsFromStack() : new List<IResourceInfo>();
            if (permission != null && !string.IsNullOrEmpty(permission.ResourceNo))
            {
                list.Insert(0, permission);
            }
            return Authorize(throwOnFailed, principalNos, isAuthenticated, list.ToArray());
        }

        /// <summary>
        ///     授权认证
        /// </summary>
        /// <param name="throwOnFailed">验证失败时是否抛出异常</param>
        /// <param name="permissions">一组资源对象</param>
        /// <returns>是否通过认证</returns>
        public bool Authorize(bool throwOnFailed, IList<string> principalNos,bool isAuthenticated=true, params IResourceInfo[] resources)
        {
            //检测开关 当前客户端config配置
            if (permissionSwitsh)
                return permissionSwitsh;
            //是否需要校验当前用户
            if (isAuthenticated)
            {
                //检验当前用户是否已通过了认证
                IIdentity identity = Thread.CurrentPrincipal.Identity;
                if (!identity.IsAuthenticated)
                {
                    if (throwOnFailed)
                    {
                        ThrowException(new AuthenticationException("当前未能通过身份认证！"));
                    }
                    return false;
                }
            }

            ResourceInfo resource = null;

            //从给定的一组资源对象中查找到第一个设置了资源权限的对象
            if (resources != null && resources.Length > 0)
            {
                foreach (IResourceInfo resItem in resources)
                {
                    if (!string.IsNullOrEmpty(resItem.ResourceNo))
                    {
                        resource = new ResourceInfo
                        {
                            ResourceNo = resItem.ResourceNo,
                            Attach = resItem.Attach,
                            AppNo = resItem.AppNo,
                        };
                        break;
                    }
                }
            }

            //未设任何权限限制的则表示通过认证
            //设了权限限制但是权限码高为空的，表示通过认证
            if (resource == null||string.IsNullOrEmpty(resource.ResourceNo))
            {
                return true;
            }
            if (string.IsNullOrEmpty(resource.AppNo))
                resource.AppNo = acAppNo;
            if (principalNos == null)
            {
                principalNos = GetCurrentPrincipal();
            }
            
            //获取主体所持有的资源规则
            IList<ResourceInfo> userPermissionList = GetResourcesByPrincipalNos(principalNos, resource.AppNo);
            if (userPermissionList == null || userPermissionList.Count == 0)
            {
                if (throwOnFailed)
                {
                    ThrowException(new AuthorizationException("当前用户不具有此操作的权限！"));
                }
                return false;
            }

            var userRes = userPermissionList.Where(m => m != null && m.ResourceNo == resource.ResourceNo);

            bool result = true;

            if (userRes == null || userRes.Count() == 0 || userRes.Any(m => m.LimitsRules == -1))
                result = false;
            else
                result = userRes.Any(m=>m.Authorized);

            if (!result && throwOnFailed)
            {
                ThrowException(new AuthorizationException("当前用户不具有此操作的权限！"));
            }
            return result;
        }

        private void ThrowException(Exception ex) 
        {
            //if (HttpContext.Current != null)
            //{
            //    HttpContext.Current.Response.Write(ex.Message);
            //    HttpContext.Current.Response.End();
            //}
            //else 
            //{
                throw ex;
            //}
        }

        /// <summary>
        ///     从堆栈中获取元属性,获取标签配置的资源编号
        /// </summary>
        /// <returns></returns>
        private static List<IResourceInfo> GetPermissionsFromStack()
        {
            var list = new List<IResourceInfo>();
            PermissionAttribute[] ps = Utils.PermissionHelp.GetAttributeFromRuntimeStack<PermissionAttribute>(true);
            if (ps != null && ps.Length > 0)
            {
                list.AddRange(ps);
            }
            return list;
        }

        #endregion

        #region 用户相关
        /// <summary>
        /// 获取当前用户
        /// </summary>
        /// <returns></returns>
        protected virtual List<string> GetCurrentPrincipal()
        {
            if (HttpContext.Current != null && HttpContext.Current.User != null)
            {
                try
                {
                    HttpCookie cookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
                    if (cookie == null)
                        return null;
                    string userData = FormsAuthentication.Decrypt(cookie.Value).UserData;
                    if (string.IsNullOrEmpty(userData))
                        return null;
                    string[] datas = userData.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    if (datas.Length < 3)
                        return null;

                    string[] roles = datas[2].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    if (roles == null || roles.Length == 0)
                        return null;
                    return roles.ToList();
                    //return HttpContext.Current.User.Identity.Name;
                }
                catch
                {
                    return null;
                }
            }
            return null;
        }
        #endregion
    }
}
