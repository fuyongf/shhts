﻿using Fabao.Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Fabao.UnitTest
{

    [TestFixture]
    public class TaskTest : TestSetup
    {

        [Test]
        public void Create()
        {
            var proxy = ServiceHub.Resolve<ITaskService>();

            var r = proxy.Create(13525, TaskKind.Trade, "测试abc");

            Assert.IsTrue(r.Success);
            Console.WriteLine(r.Message);
        }

        [Test]
        public void Get()
        {
            var proxy = ServiceHub.Resolve<ITaskService>();

            var r = proxy.Create(13525, TaskKind.Trade, "测试abc");

            r=proxy.GetTask((string)r.Data["TaskId"]);

            Assert.NotNull(r.Data["Task"]);
        }

        [Test]
        public void Accept()
        {
            var proxy = ServiceHub.Resolve<ITaskService>();

            var r = proxy.Create(13525, TaskKind.Trade, "测试abc");

            r = proxy.Accept((string)r.Data["TaskId"], 13523);

            Assert.IsTrue(r.Success);
        }

        [Test]
        public void Submit()
        {
            var proxy = ServiceHub.Resolve<ITaskService>();

            var r = proxy.Create(13525, TaskKind.Trade, "测试abc");
            var taskId=(string)r.Data["TaskId"];
            r = proxy.Accept(taskId, 13523);

            r = proxy.Submit(taskId, 13523);

            Assert.IsTrue(r.Success);
        }

        [Test]
        public void Verify()
        {
            var proxy = ServiceHub.Resolve<ITaskService>();

            var r = proxy.Create(13525, TaskKind.Trade, "测试abc");
            var taskId = (string)r.Data["TaskId"];
            r = proxy.Accept(taskId, 13523);

            r = proxy.Submit(taskId, 13523);

            r = proxy.Verify(taskId, 13525);

            Assert.IsTrue(r.Success);
        }

        [Test]
        public void Cancel()
        {
            var proxy = ServiceHub.Resolve<ITaskService>();

            var r = proxy.Create(13525, TaskKind.Trade, "测试abc");
            var taskId = (string)r.Data["TaskId"];
            r = proxy.Cancel(taskId, 13525);
            Assert.IsTrue(r.Success);
        }

        [Test]
        public void Terminate()
        {
            var proxy = ServiceHub.Resolve<ITaskService>();

            var r = proxy.Create(13525, TaskKind.Trade, "测试abc");
            var taskId = (string)r.Data["TaskId"];
            r = proxy.Accept(taskId, 13523);

            r = proxy.Submit(taskId, 13523);

            r = proxy.Terminate(taskId, 13525,"完全不对");

            Assert.IsTrue(r.Success);
        }
    }
}
