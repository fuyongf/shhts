﻿using Radial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;

namespace Fabao.UnitTest
{
    static class ServiceHub
    {
        public static TService Resolve<TService>()
        {
            return Components.Container.Resolve<TService>();
        }
    }
}
