﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using System.ServiceModel;
using Radial;
using NUnit.Framework;

namespace Fabao.UnitTest
{
    [SetUpFixture]
    public class TestSetup
    {
        [SetUp]
        public void SetUp()
        {
            Components.Container.RegisterInstance<ITaskService>(CreateChannel<ITaskService>(), new ContainerControlledLifetimeManager());

        }

        public TService CreateChannel<TService>()
        {
            return new ChannelFactory<TService>(typeof(TService).FullName).CreateChannel();
        }
    }
}
