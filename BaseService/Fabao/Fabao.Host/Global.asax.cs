﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace Fabao.Host
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            Radial.Boot.Bootstrapper.RegisterTask(new Startup());
            Radial.Boot.Bootstrapper.Initialize();
            Radial.Boot.Bootstrapper.Start();
        }
    }
}