﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Practices.Unity;
using Fabao.Impl.Infras.Persist.Core;
using Radial;

namespace Fabao.Host
{
    public class Startup:Radial.Boot.IBootTask
    {
        public void Initialize()
        {
            Components.Container.RegisterType<IEngineInitializer, EmbedEngineInitializer>();
        }

        public void Start()
        {
        }

        public void Stop()
        {
        }
    }
}