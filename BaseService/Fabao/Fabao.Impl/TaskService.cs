﻿using Fabao.Impl.Infras.Persist;
using Fabao.Impl.Infras.Persist.Core;
using Fabao.Model;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy;
using Radial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Fabao.Impl
{
    /// <summary>
    /// TaskService
    /// </summary>
    public class TaskService : ITaskService
    {
        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <param name="sysNo">The system no.</param>
        /// <returns></returns>
        Tuple<User, UserType> GetUser(long sysNo)
        {
            UserType userType = UserType.Unknown;
            User u = null;

            if (u == null)
            {
                u = CustomerServiceProxy.GetCustomer(sysNo);
                if (u != null)
                    userType = UserType.Customer;
            }

            if (u == null)
            {
                u = PinganStaffServiceProxy.GetPinganStaff(sysNo);
                if (u != null)
                    userType = UserType.PinganStaff;
            }

            if (u == null)
            {
                u = ThirdpartyUserServiceProxy.GetThirdpartyUser(sysNo);
                if (u != null)
                    userType = UserType.ThirdpartyUser;
            }

            if (u == null)
            {
                u = AgentServiceProxy.GetAgentSatff(sysNo);
                if (u != null)
                    userType = UserType.AgentStaff;
            }

            return new Tuple<User, UserType>(u, userType);
        }

        /// <summary>
        /// 创建一个任务.
        /// </summary>
        /// <param name="publisherId">发布人系统标识.</param>
        /// <param name="kind">任务类型</param>
        /// <param name="description">任务描述.</param>
        /// <![CDATA[
        /// Key:TaskId Value:string
        /// ]]>
        /// </returns>
        public OpResult Create(long publisherId, TaskKind kind, string description)
        {
            OpResult result = new OpResult();

            try
            {
                Checker.Parameter(!string.IsNullOrWhiteSpace(description), "任务描述不能为空");

                var uots = GetUser(publisherId);

                Checker.Requires(uots.Item1 != null, "未找到指定的用户: {0}", publisherId);


                using (var session = MyBatisEngine.First.BeginTransaction())
                {
                    PublisherDal pudal = new PublisherDal(session.SqlMapper);

                    Publisher pu = pudal.Find(publisherId.ToString());

                    if (pu == null)
                    {
                        pu = new Publisher
                        {
                            Type = uots.Item2,
                            Id = publisherId.ToString()
                        };
                        pudal.Insert(pu);
                    }

                    TaskObject task = new TaskObject
                    {
                        Id = TimingSeq.Next(),
                        PublisherId = pu.Id,
                        PublishTime = DateTime.Now,
                        Kind = kind,
                        Description = description.Trim()
                    };

                    TaskObjectDal taDal = new TaskObjectDal(session.SqlMapper);

                    taDal.Insert(task);

                    session.Complete();

                    result.Data["TaskId"] = task.Id;
                }

                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;

            }
            return result;
        }


        /// <summary>
        /// Gets the tasks.
        /// </summary>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <returns>
        /// <![CDATA[
        /// Key:Tasks Value:IList<TaskObject>
        /// Key:ObjectTotal Value:int
        /// ]]>
        /// </returns>
        public OpResult GetTasks(int? pageSize, int? pageIndex)
        {
            OpResult result = new OpResult();

            return result;
        }

        /// <summary>
        /// Gets the task.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// <![CDATA[
        /// Key:Task Value:TaskObject
        /// ]]>
        /// </returns>
        public OpResult GetTask(string id)
        {
            OpResult result = new OpResult();

            try
            {
                Checker.Parameter(!string.IsNullOrWhiteSpace(id), "任务标识不能为空");

                TaskObjectDal taDal = new TaskObjectDal(MyBatisEngine.First);

                result.Data["Task"]=taDal.Find(id);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;

            }


            return result;
        }


        /// <summary>
        /// 接受任务
        /// </summary>
        /// <param name="taskId">任务标识.</param>
        /// <param name="executorId">执行人标识.</param>
        /// <returns></returns>
        public OpResult Accept(string taskId, long executorId)
        {
            OpResult result = new OpResult();

            try
            {
                Checker.Parameter(!string.IsNullOrWhiteSpace(taskId), "任务标识不能为空");

                var uots = GetUser(executorId);

                Checker.Requires(uots.Item1 != null, "未找到指定的用户: {0}", executorId);

                using (var session = MyBatisEngine.First.BeginTransaction())
                {
                    TaskObjectDal taDal = new TaskObjectDal(session.SqlMapper);
                    var task = taDal.Find(taskId);

                    Checker.Requires(task != null, "找不到指定的任务: {0}", taskId);

                    Checker.Requires(task.PublisherId != executorId.ToString(), "发布人不能承接自己发布的任务: {0}", taskId);

                    ExecutorDal exDal = new ExecutorDal(session.SqlMapper);

                    var exeUser = exDal.Find(executorId.ToString());

                    if (exeUser == null)
                    {
                        exeUser = new Executor
                        {
                            Id = executorId.ToString(),
                            Type = uots.Item2
                        };
                        exDal.Insert(exeUser);
                    }

                    task.ExecutorId = exeUser.Id;
                    task.AcceptTime = DateTime.Now;
                    task.State = TaskState.Executing;

                    taDal.Update(task);

                    session.Complete();
                }
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }


            return result;
        }


        /// <summary>
        /// 提交任务，等待验收
        /// </summary>
        /// <param name="taskId">任务标识.</param>
        /// <param name="executorId">执行人标识.</param>
        /// <returns></returns>
        public OpResult Submit(string taskId, long executorId)
        {
            OpResult result = new OpResult();

            try
            {
                Checker.Parameter(!string.IsNullOrWhiteSpace(taskId), "任务标识不能为空");

                using (var session = MyBatisEngine.First.BeginTransaction())
                {
                    TaskObjectDal taDal = new TaskObjectDal(session.SqlMapper);
                    var task = taDal.Find(taskId);

                    Checker.Requires(task != null, "找不到指定的任务: {0}", taskId);
                    Checker.Requires(task.State == TaskState.Executing, "任务状态不正确: {0}", taskId);

                    ExecutorDal exDal = new ExecutorDal(session.SqlMapper);

                    var exeUser = exDal.Find(executorId.ToString());

                    Checker.Requires(exeUser != null, "执行人未录入系统: {0}", executorId);
                    Checker.Requires(task.ExecutorId == exeUser.Id, "执行人与任务无关: {0}", executorId);

                    task.State = TaskState.PendingVerify;
                    task.SubmitTime = DateTime.Now;

                    taDal.Update(task);

                    session.Complete();
                }

                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            return result;
        }


        /// <summary>
        /// 验收任务
        /// </summary>
        /// <param name="taskId">任务标识.</param>
        /// <param name="publisherId">发布人标识.</param>
        /// <returns></returns>
        public OpResult Verify(string taskId, long publisherId)
        {
            OpResult result = new OpResult();

            try
            {
                Checker.Parameter(!string.IsNullOrWhiteSpace(taskId), "任务标识不能为空");

                using (var session = MyBatisEngine.First.BeginTransaction())
                {
                    TaskObjectDal taDal = new TaskObjectDal(session.SqlMapper);
                    var task = taDal.Find(taskId);

                    Checker.Requires(task != null, "找不到指定的任务: {0}", taskId);
                    Checker.Requires(task.State == TaskState.PendingVerify, "任务状态不正确: {0}", taskId);

                    PublisherDal puDal = new PublisherDal(session.SqlMapper);

                    var pubUser = puDal.Find(publisherId.ToString());

                    Checker.Requires(pubUser != null, "发布人未录入系统: {0}", publisherId);
                    Checker.Requires(task.PublisherId == pubUser.Id, "发布人与任务无关: {0}", publisherId);

                    task.State = TaskState.VerifyOk;
                    task.VerifyTime = DateTime.Now;

                    taDal.Update(task);

                    session.Complete();
                }
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }


            return result;
        }


        /// <summary>
        /// 取消任务
        /// </summary>
        /// <param name="taskId">任务标识.</param>
        /// <param name="publisherId">发布人标识.</param>
        /// <returns></returns>
        public OpResult Cancel(string taskId, long publisherId)
        {
            OpResult result = new OpResult();

            try
            {
                Checker.Parameter(!string.IsNullOrWhiteSpace(taskId), "任务标识不能为空");

                using (var session = MyBatisEngine.First.BeginTransaction())
                {
                    session.BeginTransaction();

                    TaskObjectDal taDal = new TaskObjectDal(session.SqlMapper);
                    var task = taDal.Find(taskId);

                    Checker.Requires(task != null, "找不到指定的任务: {0}", taskId);
                    Checker.Requires(task.State == TaskState.Initial, "任务状态不正确: {0}", taskId);

                    PublisherDal puDal = new PublisherDal(session.SqlMapper);

                    var pubUser = puDal.Find(publisherId.ToString());

                    Checker.Requires(pubUser != null, "发布人未录入系统: {0}", publisherId);

                    Checker.Requires(task.PublisherId == pubUser.Id, "发布人与任务无关: {0}", publisherId);

                    task.State = TaskState.Cancel;
                    task.CancelTime = DateTime.Now;

                    taDal.Update(task);

                    session.CommitTransaction();
                }
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }


            return result;
        }


        /// <summary>
        /// 终止任务
        /// </summary>
        /// <param name="taskId">任务标识.</param>
        /// <param name="publisherId">发布人标识.</param>
        /// <param name="note">终止任务原因</param>
        /// <returns></returns>
        public OpResult Terminate(string taskId, long publisherId, string note)
        {
            OpResult result = new OpResult();

            try
            {
                Checker.Parameter(!string.IsNullOrWhiteSpace(taskId), "任务标识不能为空");
                Checker.Parameter(!string.IsNullOrWhiteSpace(note), "终止任务原因不能为空");

                using (var session = MyBatisEngine.First.BeginTransaction())
                {
                    TaskObjectDal taDal = new TaskObjectDal(session.SqlMapper);
                    var task = taDal.Find(taskId);

                    Checker.Requires(task != null, "找不到指定的任务: {0}", taskId);
                    Checker.Requires(task.State >= TaskState.Executing, "任务状态不正确: {0}", taskId);

                    PublisherDal puDal = new PublisherDal(session.SqlMapper);

                    var pubUser = puDal.Find(publisherId.ToString());

                    Checker.Requires(pubUser != null, "发布人未录入系统: {0}", publisherId);

                    Checker.Requires(task.PublisherId == pubUser.Id, "发布人与任务无关: {0}", publisherId);

                    task.State = TaskState.Terminate;
                    task.TerminateTime = DateTime.Now;
                    task.TerminateNote = note;

                    taDal.Update(task);

                    session.Complete();
                }
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }


            return result;
        }
    }
}
