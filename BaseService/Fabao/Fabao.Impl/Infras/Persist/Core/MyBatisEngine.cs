﻿using Radial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using IBatisNet.DataMapper;

namespace Fabao.Impl.Infras.Persist.Core
{

    /// <summary>
    /// MyBatisEngine
    /// </summary>
    public class MyBatisEngine
    {
        static ISet<MapperWrapper> S_MapperWrapperSet;

        const string CanNotFindInstanceExceptionMessage = "can not find the specified ISqlMapper instance";
        const string CanNotFindInstanceExceptionMessageWithAlias = "can not find the specified ISqlMapper instance, alias: {0}";

        /// <summary>
        /// Initializes the <see cref="MyBatisEngine"/> class.
        /// </summary>
        static MyBatisEngine()
        {
            if (S_MapperWrapperSet == null)
            {
                Logger.Debug("begin initialize engine");

                IEngineInitializer initializer = null;
                if (Components.Container.IsRegistered<IEngineInitializer>())
                {
                    initializer = Components.Container.Resolve<IEngineInitializer>();
                }
                else
                {
                    //use default when not set.
                    Logger.Debug("can not found any engine initializer, using DefaultEngineInitializer instead");
                    initializer = new EmbedEngineInitializer();
                }

                S_MapperWrapperSet = initializer.Execute();

                Checker.Requires(S_MapperWrapperSet != null && S_MapperWrapperSet.Count > 0, "failed to initialize: MapperWrapper set was null or empty");

                Logger.Debug("end initialize engine");
            }
        }

        /// <summary>
        /// Gets the logger.
        /// </summary>
        private static Logger Logger
        {
            get
            {
                return Logger.GetInstance("MyBatisEngine");
            }
        }


        /// <summary>
        /// Gets the first IBatisNet.DataMapper.ISqlMapper instance.
        /// </summary>
        public static ISqlMapper First
        {
            get
            {
                MapperWrapper wrapper = GetMapperWrapper().FirstOrDefault();

                return wrapper.Mapper;
            }
        }


        /// <summary>
        /// Gets the MapperWrapper object with the specified storage alias.
        /// </summary>
        /// <param name="storageAlias">The storage alias (case insensitive).</param>
        /// <returns>The MapperWrapper object.</returns>
        public static MapperWrapper GeMapperWrapper(string storageAlias)
        {
            return GetMapperWrapper(storageAlias).FirstOrDefault();
        }

        /// <summary>
        /// Gets the MapperWrapper object with the specified storage aliases.
        /// </summary>
        /// <param name="storageAliases">The storage alias array (case insensitive).</param>
        /// <returns>The MapperWrapper object array.</returns>
        public static MapperWrapper[] GetMapperWrapper(params string[] storageAliases)
        {
            IList<MapperWrapper> list = new List<MapperWrapper>();

            if (storageAliases != null && storageAliases.Length > 0)
            {
                foreach (string alias in storageAliases)
                {
                    string normalizedAlias = alias.ToLower().Trim();
                    MapperWrapper wrapper = S_MapperWrapperSet.SingleOrDefault(o => o.Alias == normalizedAlias);

                    Checker.Requires(wrapper != null, CanNotFindInstanceExceptionMessageWithAlias, normalizedAlias);

                    list.Add(wrapper);
                }

                return list.ToArray();
            }

            return S_MapperWrapperSet.ToArray();

        }


        /// <summary>
        /// Gets the <see cref="IBatisNet.DataMapper.ISqlMapper"/> instance with the specified storage alias.
        /// </summary>
        /// <param name="storageAlias">The storage alias (case insensitive).</param>
        /// <returns>The IBatisNet.DataMapper.ISqlMapper instance</returns>
        public static ISqlMapper GetMapperInstance(string storageAlias)
        {
            return GeMapperWrapper(storageAlias).Mapper;
        }

        /// <summary>
        /// Gets all storage aliases.
        /// </summary>
        /// <returns>
        /// The storage aliases array.
        /// </returns>
        public static string[] GetStorageAliases()
        {
            IList<string> aliases = new List<string>(S_MapperWrapperSet.Count);

            foreach (MapperWrapper wrapper in S_MapperWrapperSet)
                aliases.Add(wrapper.Alias);

            return aliases.ToArray();

        }
    }
}
