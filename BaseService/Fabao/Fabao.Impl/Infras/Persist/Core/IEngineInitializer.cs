﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabao.Impl.Infras.Persist.Core
{

    /// <summary>
    /// Mapper engine initializer
    /// </summary>
    public interface IEngineInitializer
    {
        /// <summary>
        /// Execute engine initialization.
        /// </summary>
        /// <returns>
        /// The mapper wrapper set.
        /// </returns>
        ISet<MapperWrapper> Execute();
    }
}
