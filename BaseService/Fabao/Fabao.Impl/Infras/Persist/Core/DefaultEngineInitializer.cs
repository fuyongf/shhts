﻿using IBatisNet.DataMapper;
using IBatisNet.DataMapper.Configuration;
using Radial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabao.Impl.Infras.Persist.Core
{

    /// <summary>
    /// DefaultEngineInitializer
    /// </summary>
    public class DefaultEngineInitializer : IEngineInitializer
    {
        /// <summary>
        /// Execute engine initialization.
        /// </summary>
        /// <returns>
        /// The mapper wrapper set.
        /// </returns>
        public virtual ISet<MapperWrapper> Execute()
        {
            ISet<MapperWrapper> wrappers = new HashSet<MapperWrapper>();

            wrappers.Add(new MapperWrapper("default", Mapper.Instance()));

            return wrappers;
        }
    }
}
