﻿using IBatisNet.Common.Utilities;
using IBatisNet.DataMapper;
using IBatisNet.DataMapper.Configuration;
using Radial;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Fabao.Impl.Infras.Persist.Core
{
    /// <summary>
    /// Abstract class of IGeneralDal
    /// </summary>
    /// <typeparam name="TObject">The type of the object.</typeparam>
    /// <typeparam name="TKey">The type of the key.</typeparam>
    public abstract class GeneralDal<TObject, TKey> : IGeneralDal<TObject, TKey> where TObject : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GeneralDal{TObject, TKey}"/> class.
        /// </summary>
        /// <param name="mapper">The mapper.</param>
        public GeneralDal(ISqlMapper mapper)
        {
            Checker.Parameter(mapper != null, "mapper instance can not be null");
            Mapper = mapper;
        }

        /// <summary>
        /// Gets the mapper.
        /// </summary>
        /// <value>
        /// The mapper.
        /// </value>
        protected ISqlMapper Mapper
        {
            get;
            private set;
        }

        /// <summary>
        /// Builds the name of the statement.
        /// </summary>
        /// <param name="actionName">Name of the action.</param>
        /// <returns></returns>
        protected string BuildStatementName(string actionName)
        {
            return string.Concat(typeof(TObject).Name, ".", actionName);
        }

        /// <summary>
        /// Inserts the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        public void Insert(TObject obj)
        {
            if (obj == null)
                return;
            Mapper.Insert(BuildStatementName("Insert"), obj);
        }

        /// <summary>
        /// Updates the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        public void Update(TObject obj)
        {
            if (obj == null)
                return;

            Mapper.Update(BuildStatementName("Update"), obj);
        }

        /// <summary>
        /// Deletes the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        public void Delete(TKey key)
        {
            if (key == null)
                return;

            Mapper.Delete(BuildStatementName("Delete"), key);
        }


        /// <summary>
        /// Finds the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public TObject Find(TKey key)
        {
            if (key == null)
                return default(TObject);

           return Mapper.QueryForObject<TObject>(BuildStatementName("Find"), key);
        }
    }
}
