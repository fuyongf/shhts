﻿using IBatisNet.DataMapper;
using IBatisNet.DataMapper.Configuration;
using Radial;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Fabao.Impl.Infras.Persist.Core
{

    /// <summary>
    /// Embedded config engine initializer
    /// </summary>
    public class EmbedEngineInitializer : IEngineInitializer
    {
        /// <summary>
        /// Execute engine initialization.
        /// </summary>
        /// <returns>
        /// The mapper wrapper set.
        /// </returns>
        public virtual ISet<MapperWrapper> Execute()
        {
            ISet<MapperWrapper> wrappers = new HashSet<MapperWrapper>();

            DomSqlMapBuilder builder = new DomSqlMapBuilder();

            //设置根目录
            var properties = new NameValueCollection { { "root", StaticVariables.BaseDirectory } };

            builder.Properties = properties;

            Assembly configAssembly = null;

            //must  be full name,eg "abc.SqlMap.config, abc"
            var configNameParts = ConfigurationManager.AppSettings["MyBatis.EmbedEngine.ConfigName"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var fileName = configNameParts.Length > 0 ? configNameParts[0].Trim() : null;

            Checker.Requires(!string.IsNullOrWhiteSpace(fileName), "Embedded the config file name cannot be empty");

            var assemblyName = configNameParts.Length > 1 ? configNameParts[1].Trim() : null;

            if (!string.IsNullOrWhiteSpace(assemblyName))
                configAssembly = Assembly.Load(assemblyName);

            if (configAssembly == null)
                configAssembly = Assembly.GetExecutingAssembly();

            Stream configStream = configAssembly.GetManifestResourceStream(fileName);

            //no file watcher
            wrappers.Add(new MapperWrapper("default", builder.Configure(configStream)));

            return wrappers;
        }
    }
}
