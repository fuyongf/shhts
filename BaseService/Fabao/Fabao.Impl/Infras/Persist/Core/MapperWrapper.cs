﻿using IBatisNet.DataMapper;
using Radial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabao.Impl.Infras.Persist.Core
{
    /// <summary>
    /// MapperWrapper
    /// </summary>
    public sealed class MapperWrapper : IEquatable<MapperWrapper>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MapperWrapper"/> class.
        /// </summary>
        /// <param name="alias">The storage alias array (case insensitive).</param>
        /// <param name="mapper">The mapper.</param>
        public MapperWrapper(string alias, ISqlMapper mapper)
        {
            Checker.Parameter(!string.IsNullOrWhiteSpace(alias), "storage alias can not be empty or null");
            Checker.Parameter(mapper != null, "mapper instance can not be null");

            Alias = alias.Trim().ToLower();
            Mapper = mapper;
        }


        /// <summary>
        /// Gets the storage alias.
        /// </summary>
        /// <value>
        /// The storage alias.
        /// </value>
        public string Alias
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the mapper.
        /// </summary>
        /// <value>
        /// The mapper.
        /// </value>
        public ISqlMapper Mapper
        {
            get;
            private set;
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj))
                return true;

            if (obj == null)
                return false;

            return this.GetHashCode() == obj.GetHashCode();
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return Alias.GetHashCode();
        }

        /// <summary>
        /// Equalses the specified other.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns></returns>
        public bool Equals(MapperWrapper other)
        {
            return Equals(other);
        }
    }
}
