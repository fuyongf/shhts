﻿
using Fabao.Impl.Infras.Persist;
using Fabao.Impl.Infras.Persist.Core;
using Fabao.Model;
using IBatisNet.DataMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabao.Impl.Infras.Persist
{
    /// <summary>
    /// TaskObjectDal
    /// </summary>
    public class TaskObjectDal : GeneralDal<TaskObject, string>
    {
        public TaskObjectDal(ISqlMapper mapper)
            : base(mapper) { }
    }
}
