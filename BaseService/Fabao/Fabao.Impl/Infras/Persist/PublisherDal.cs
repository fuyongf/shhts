﻿
using Fabao.Impl.Infras.Persist;
using Fabao.Impl.Infras.Persist.Core;
using Fabao.Model;
using IBatisNet.DataMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabao.Impl.Infras.Persist
{
    /// <summary>
    /// PublisherDal
    /// </summary>
    public class PublisherDal : GeneralDal<Publisher, string>
    {
        public PublisherDal(ISqlMapper mapper)
            : base(mapper) { }
    }
}
