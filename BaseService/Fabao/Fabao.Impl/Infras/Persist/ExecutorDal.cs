﻿
using Fabao.Impl.Infras.Persist;
using Fabao.Impl.Infras.Persist.Core;
using Fabao.Model;
using IBatisNet.DataMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabao.Impl.Infras.Persist
{
    /// <summary>
    /// ExecutorDal
    /// </summary>
    public class ExecutorDal : GeneralDal<Executor, string>
    {
        public ExecutorDal(ISqlMapper mapper)
            : base(mapper) { }
    }
}
