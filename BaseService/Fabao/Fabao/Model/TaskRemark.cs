﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabao.Model
{
    public class TaskRemark : ModelBase
    {
        /// <summary>
        /// 获取或设置任务标识.
        /// </summary>
        public string TaskId { get; set; }
        /// <summary>
        /// 获取或设置评价人标识.
        /// </summary>
        public string FromUserId { get; set; }
        /// <summary>
        /// 获取或设置被评价人标识.
        /// </summary>
        public string ToUserId { get; set; }
        /// <summary>
        /// 获取或设置评价星级.
        /// </summary>
        public decimal Rating { get; set; }
        /// <summary>
        /// 获取或设置评价内容.
        /// </summary>
        public string Content { get; set; }
    }
}
