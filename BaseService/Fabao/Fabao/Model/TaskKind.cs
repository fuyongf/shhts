﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fabao.Model
{
    /// <summary>
    /// 任务类型
    /// </summary>
    public enum TaskKind
    {
        /// <summary>
        /// 交易
        /// </summary>
        Trade,
        /// <summary>
        /// 贷款
        /// </summary>
        Credit,
    }
}
