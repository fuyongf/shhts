﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabao.Model
{
    /// <summary>
    /// 任务发布人
    /// </summary>
    public class Publisher : ModelBase
    {
        /// <summary>
        /// 获取或设置用户类型.
        /// </summary>
        public UserType Type { get; set; }
        /// <summary>
        /// 获取或设置评分.
        /// </summary>
        public decimal Grade { get; set; }
    }
}
