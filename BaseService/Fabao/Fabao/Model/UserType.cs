﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabao.Model
{
    /// <summary>
    /// UserType
    /// </summary>
    public enum UserType
    {        
        /// <summary>
        /// 未知
        /// </summary>
        Unknown=-1,
        /// <summary>
        /// 上下家客户
        /// </summary>
        Customer = 0,

        /// <summary>
        /// 中介(经纪人)
        /// </summary>
        AgentStaff = 1,
        /// <summary>
        /// 平安员工
        /// </summary>
        PinganStaff = 2,
        /// <summary>
        /// 第三方公司员工
        /// </summary>
        ThirdpartyUser = 3
    }
}
