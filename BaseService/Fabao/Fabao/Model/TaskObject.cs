﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fabao.Model
{
    /// <summary>
    /// 任务对象
    /// </summary>
    public class TaskObject : ModelBase
    {
        /// <summary>
        /// 获取或设置任务类型
        /// </summary>
        public TaskKind Kind { get; set; }
        /// <summary>
        /// 获取或设置任务描述
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 获取或设置任务状态.
        /// </summary>
        public TaskState State { get; set; }
        /// <summary>
        /// 获取或设置发布人Id.
        /// </summary>
        public string PublisherId { get; set; }
        /// <summary>
        /// 获取或设置发布时间.
        /// </summary>
        public DateTime PublishTime { get; set; }
        /// <summary>
        /// 获取或设置接单人Id.
        /// </summary>
        public string ExecutorId { get; set; }
        /// <summary>
        /// 获取或设置接单时间.
        /// </summary>
        public DateTime? AcceptTime { get; set; }
        /// <summary>
        /// 获取或设置提交审核时间
        /// </summary>
        public DateTime? SubmitTime { get; set; }
        /// <summary>
        /// 获取或设置验收时间.
        /// </summary>
        public DateTime? VerifyTime { get; set; }
        /// <summary>
        /// 获取或设置取消时间.
        /// </summary>
        public DateTime? CancelTime { get; set; }
        /// <summary>
        /// 获取或设置终止时间.
        /// </summary>
        public DateTime? TerminateTime { get; set; }
        /// <summary>
        /// 获取或设置终止原因.
        /// </summary>
        public string TerminateNote { get; set; }
    }
}
