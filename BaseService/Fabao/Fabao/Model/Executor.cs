﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabao.Model
{
    /// <summary>
    /// 任务执行人
    /// </summary>
    public class Executor : ModelBase
    {
        /// <summary>
        /// 获取或设置用户类型.
        /// </summary>
        public UserType Type { get; set; }
        /// <summary>
        /// 获取或设置评分.
        /// </summary>
        public decimal Grade { get; set; }
    }
}
