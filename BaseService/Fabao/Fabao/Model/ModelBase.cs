﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabao.Model
{
    /// <summary>
    /// 模型基类
    /// </summary>
    public abstract class ModelBase
    {
        /// <summary>
        /// 获取或设置标识.
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 获取或设置是否已删除
        /// </summary>
        public bool IsDelete { get; set; }
    }
}
