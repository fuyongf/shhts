﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabao.Model
{
    /// <summary>
    /// 任务状态
    /// </summary>
    public enum TaskState
    {
        /// <summary>
        /// 初始状态
        /// </summary>
        Initial=0,
        /// <summary>
        /// 正在执行中
        /// </summary>
        Executing=1,        
        /// <summary>
        /// 等待验收
        /// </summary>
        PendingVerify= 2,
        /// <summary>
        /// 验收通过
        /// </summary>
        VerifyOk = 3,
        /// <summary>
        /// 撤销
        /// </summary>
        Cancel=-20,
        /// <summary>
        /// 终止
        /// </summary>
        Terminate=-30
    }
}
