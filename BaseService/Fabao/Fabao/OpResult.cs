﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fabao
{
    /// <summary>
    /// 表示操作结果
    /// </summary>
    public class OpResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OpResult"/> class.
        /// </summary>
        public OpResult()
        {
            Data = new Dictionary<string, object>();
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="OpResult"/> is success.
        /// </summary>
        /// <value>
        ///   <c>true</c> if success; otherwise, <c>false</c>.
        /// </value>
        public bool Success { get; set; }
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        public IDictionary<string, object> Data { get; set; } 
    }
}
