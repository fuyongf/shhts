﻿using Fabao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Fabao
{
    /// <summary>
    /// 任务服务
    /// </summary>
    [ServiceContract]
    public interface ITaskService
    {
        /// <summary>
        /// 创建一个任务.
        /// </summary>
        /// <param name="publisherId">发布人系统标识.</param>
        /// <param name="kind">任务类型</param>
        /// <param name="description">任务描述.</param>
        /// <![CDATA[
        /// Key:TaskId Value:string
        /// ]]>
        /// </returns>
        [OperationContract]
        OpResult Create(long publisherId, TaskKind kind, string description);

        /// <summary>
        /// Gets the tasks.
        /// </summary>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <returns>
        /// <![CDATA[
        /// Key:Tasks Value:IList<TaskObject>
        /// Key:ObjectTotal Value:int
        /// ]]>
        /// </returns>
        [OperationContract]
        OpResult GetTasks(int? pageSize, int? pageIndex);

        /// <summary>
        /// 获取任务.
        /// </summary>
        /// <param name="id">任务标识.</param>
        /// <returns>
        /// <![CDATA[
        /// Key:Task Value:TaskObject
        /// ]]>
        /// </returns>
        [OperationContract]
        [ServiceKnownType(typeof(TaskObject))]
        OpResult GetTask(string id);

        /// <summary>
        /// 接受任务
        /// </summary>
        /// <param name="taskId">任务标识.</param>
        /// <param name="executorId">执行人标识.</param>
        /// <returns></returns>
        [OperationContract]
        OpResult Accept(string taskId, long executorId);
        /// <summary>
        /// 提交任务，等待验收
        /// </summary>
        /// <param name="taskId">任务标识.</param>
        /// <param name="executorId">执行人标识.</param>
        /// <returns></returns>
        [OperationContract]
        OpResult Submit(string taskId, long executorId);
        /// <summary>
        /// 验收任务
        /// </summary>
        /// <param name="taskId">任务标识.</param>
        /// <param name="publisherId">发布人标识.</param>
        /// <returns></returns>
        [OperationContract]
        OpResult Verify(string taskId, long publisherId);
        /// <summary>
        /// 取消任务
        /// </summary>
        /// <param name="taskId">任务标识.</param>
        /// <param name="publisherId">发布人标识.</param>
        /// <returns></returns>
        [OperationContract]
        OpResult Cancel(string taskId, long publisherId);
        /// <summary>
        /// 终止任务
        /// </summary>
        /// <param name="taskId">任务标识.</param>
        /// <param name="publisherId">发布人标识.</param>
        /// <param name="note">终止任务原因.</param>
        /// <returns></returns>
        [OperationContract]
        OpResult Terminate(string taskId, long publisherId,string note);
    }
}
