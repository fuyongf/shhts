﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PinganHouse.SHHTS.RemoteServiceProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.Testing.Client
{
    [TestClass]
    public class CaptchaProxyServiceTest
    {
        [TestMethod]
        public void RequestCaptcha()
        {
            var captcha = CaptchaProxyService.GetInstanse().RequetCaptcha("123456", Enumerations.BusinessCacheType.BindIdentity);
            Assert.IsNotNull(captcha);
            var result = CaptchaProxyService.GetInstanse().ValidateCaptcha(captcha.Key, "123213");
            Assert.IsFalse(result.Success);
            result = CaptchaProxyService.GetInstanse().ValidateCaptcha(captcha.Key, captcha.Value);
            Assert.IsTrue(result.Success);
            var img = captcha.ToBitmap();
        }
    }
}
