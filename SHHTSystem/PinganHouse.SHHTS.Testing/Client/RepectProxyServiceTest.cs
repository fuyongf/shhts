﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Testing.Client
{
    [TestClass]
    public class RepectProxyServiceTest
    {
        [TestMethod]
        public void GetReceipt()
        {
            var result = ReceiptProxyService.GetInstanse().PreviewReceipt("2015040205076296");
            Assert.IsNotNull(result);
        }

        //int totalCount = 0;
        [TestMethod]
        public void Get()
        {
            var reviewRule = ReviewProxyService.GetInstanse().ReviewRule(Enumerations.ReviewCategory.Receipt, "1", 1);
          
            //var result = ReceiptProxyService.GetInstanse().GetPaginatedList(1, 10, ref totalCount, ReceptionCenter.PuDong, startDate: DateTime.Now.Date.AddDays(-20));
            //Assert.IsNotNull(result);
        }
    }
}
