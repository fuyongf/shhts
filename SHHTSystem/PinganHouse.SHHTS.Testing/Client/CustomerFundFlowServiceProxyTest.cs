﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Testing.Client
{
    [TestClass]
    public class CustomerFundFlowServiceProxyTest
    {
        [TestMethod]
        public void Add()
        {
            var result = CustomerFundFlowServiceProxy.Create(new CustomerFundFlow
            {
                CaseId = "32039001008576",
                CustomerType = CustomerType.Seller,
                ConditionType = "条件",
                ConditionName = "签约后",
                ConditionValue = "三个工作日",
                HouseFund = 1,
                Taxes = 1,
                Brokerage = 1,
                DecorateCompensation = 1,
                OtherCharges = 1,
                Amount = 5,
                AccountBalance = 5,
                CreateUserSysNo = 0
            });
            Assert.AreEqual(result.Success, true);
        }

        [TestMethod]
        public void Get()
        {
            var result = CustomerFundFlowServiceProxy.GetFlows("32039001008576", CustomerType.Seller);
            Assert.IsNotNull(result);
        }
    }
}
