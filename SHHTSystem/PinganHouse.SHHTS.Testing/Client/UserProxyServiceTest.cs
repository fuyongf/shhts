﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Testing.Client
{
    [TestClass]
    public class UserProxyServiceTest
    {
        [TestMethod]
        public void AddSeller()
        {
            //var result = CustomerServiceProxy.CreateSeller(new SellerModel
            //{
            //    IdentityNo = "301000000000000000",
            //    Address = "测试3",
            //    Birthday = DateTime.Now,
            //    EffectiveDate = DateTime.Now,
            //    ExpiryDate = DateTime.Now,
            //    Gender = Gender.Male,
            //    Mobile = "11111111111",
            //    Name = "测试2",
            //    IsTrusted = false,
            //    Nation = Nation.白族,
            //    VisaAgency = "测试1"

            //}, 0);
            //Assert.IsNotNull(result);
        }

        [TestMethod]
        public void AddBuyer()
        {
            //var result = CustomerServiceProxy.CreateBuyer(new BuyerModel
            //{
            //    IdentityNo = "222222222222222222",
            //    Address = "浦电路1",
            //    Birthday = DateTime.Now,
            //    Gender = Gender.Female,
            //    EffectiveDate = DateTime.Now,
            //    ExpiryDate = DateTime.Now,
            //    Mobile = "13999999999",
            //    Name = "测试4",
            //    Nation = Nation.阿昌族,
            //    PostAddress = "浦电路2",
            //    IsTrusted = false,
            //    VisaAgency = "上海1",
            //    TelPhone = "133333333",
            //    CreditType = CreditType.Combination
            //}, 0);
            //Assert.AreEqual(result.Success, true);
        }

        [TestMethod]
        public void AddAgentStaff()
        {
            var result = AgentServiceProxy.CreateAgentStaff(new AgentStaffModel
            {
                Mobile = "12333333333",
                Name = "经纪人-1",
                AgentCompanySysNo = 10131
            }, 0);
            Assert.AreEqual(result.Success, true);
        }

        [TestMethod]
        public void AddThirdpartyUser()
        {
            var result = ThirdpartyUserServiceProxy.CreateThirdpartyUser(new ThirdpartyUserModel
            {
                Name = "第三方-4",
                TpName = "工行",
                LoginName = "login4",
                Password = "123456"
            }, 0);

            Assert.AreEqual(result.Success, true);
        }

        [TestMethod]
        public void PerfectBuyer()
        {
            //var result = CustomerServiceProxy.PerfectBuyer("32039001008576", new BuyerModel
            //{
            //    CustomerNature = CustomerNature.Personal,
            //    Recipients = "test",
            //    RecipientsMobile = "12222222222",
            //    PostAddress = "上海浦东新区",
            //    PostCode = "123456",
            //    IsCredit = true,
            //    CreditType = CreditType.Combination,
            //    SelfCredit = false,
            //}, null, null, 0);
            //Assert.AreEqual(result.Success, true);
        }

        [TestMethod]
        public void Get()
        {
            var customer = CustomerServiceProxy.GetCustomer(10189);
            //Assert.IsNotNull(customer);

            var agentStaff = AgentServiceProxy.GetAgentSatff(10222);
            agentStaff = AgentServiceProxy.GetAgentSatff("A150229140529258");
            Assert.IsNotNull(agentStaff);

            var thirdpartyUser = ThirdpartyUserServiceProxy.GetThirdpartyUser(10224);
            Assert.IsNotNull(thirdpartyUser);
        }

        [TestMethod]
        public void Pager()
        {
            int totalCount = 0;
            var result = UserServiceProxy.GetUsers(1, 10, out totalCount);

            var r = AgentServiceProxy.GetAgentStaffs(1, 10, out totalCount, null, null);

            var r1 = PinganStaffServiceProxy.GetPinganStaffs(1, 10, out totalCount);

            var r2 = ThirdpartyUserServiceProxy.GetThirdpartyUsers(1, 10, out totalCount);

            var r3 = CustomerServiceProxy.GetCustomers(1, 10, out totalCount);
        }

        [TestMethod]
        public void Login()
        {
            var result = UserServiceProxy.Login("login1", "123456");
            Assert.AreEqual(result.Success, true);
        }

        [TestMethod]
        public void GetCustomerByCase()
        {
            var customers = CustomerServiceProxy.GetCustomerByCase("06202537410653");
            Assert.IsNotNull(customers);
        }

        [TestMethod]
        public void GetCustomerDetailsByIdentityNo()
        {
            var customers = CustomerServiceProxy.GetCustomerDetailsByIdentityNo("544454");
            Assert.IsNotNull(customers);
        }
        
    }
}
