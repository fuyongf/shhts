﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using System.Security.Cryptography;

namespace PinganHouse.SHHTS.Testing.Client
{
    [TestClass]
    public class ConfigProxyServiceTest
    {
        [TestMethod]
        public void RegionInfo()
        {
            var j = "{\"iOrderAmount\":\"10000\",\"iPayAmount\":\"2000\",\"iPayType\":\"1\",\"iRequestTime\":\"1426759788\",\"sBank\":\"ICBC\",\"sBankSerialNum\":\"1234567890\",\"sBusinessID\":\"2015031906356671\",\"sCaseID\":\"06199853056055\",\"sOrderID\":\"2015031906356583\",\"sPayee\":\"Test\",\"sPosID\":\"1001\",\"sPropertyAddr\":\"杨树浦路1001弄7号701\",\"sStoreID\":\"浦东\",\"sSwipecardTime\":\"20150319\",\"sTradeDate\":\"20150319\"}";

            var m = GetMD5("7987ea40976f29c71526701ce748402e" + "_ping_an_fang_" + j,"gb2312");

            string JsonStr = "{\"iOrderAmount\":\"10000\",\"iPayAmount\":\"2000\",\"iPayType\":\"1\",\"iRequestTime\":\"1426752135\",\"sBank\":\"ICBC\",\"sBankSerialNum\":\"1234567890\",\"sBusinessID\":\"2015031906356813\",\"sCaseID\":\"06199853056055\",\"sOrderID\":\"2015031906356584\",\"sPayee\":\"Test\",\"sPosID\":\"1001\",\"sPropertyAddr\":\"\u6768\u6811\u6d66\u8def1001\u5f047\u53f7701\",\"sStoreID\":\"\u6d66\u4e1c\",\"sSwipecardTime\":\"20150319\",\"sTradeDate\":\"20150319\"}";
            string json1 = "{\"iOrderAmount\":\"10000\",\"iPayAmount\":\"2000\",\"iPayType\":\"1\",\"iRequestTime\":\"1426752135\",\"sBank\":\"ICBC\",\"sBankSerialNum\":\"1234567890\",\"sBusinessID\":\"2015031906356813\",\"sCaseID\":\"06199853056055\",\"sOrderID\":\"2015031906356584\",\"sPayee\":\"Test\",\"sPosID\":\"1001\",\"sPropertyAddr\":\"杨树浦路1001弄7号701\",\"sStoreID\":\"浦东\",\"sSwipecardTime\":\"20150319\",\"sTradeDate\":\"20150319\"}";

            var m1 = PinganHouse.SHHTS.Utils.EncryptHelper.GetMD5(JsonStr);
            var m2 = PinganHouse.SHHTS.Utils.EncryptHelper.GetMD5(json1);

            string json = "{\"data\":[],\"code\":\"4\",\"memo\":\"不含时间戳或请求超过规定时间\"}";
            var obj = PinganHouse.SHHTS.Utils.JsonSerializer.DeserializeToObject<Dictionary<string, object>>(json);

            var result = ConfigServiceProxy.GetRegionInfo();
            var cities = ConfigServiceProxy.GetRegionInfo("/北京市");

            var md5 = GetMD5("/System/RegionConfig/北京市", "gb2312");

        }


        [TestMethod]
        public void GetBanks()
        {
            var result = ConfigServiceProxy.GetBankDic();
        }
        

        public string GetMD5(string s, string charset = "utf-8")
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] t = md5.ComputeHash(Encoding.GetEncoding(charset).GetBytes(s));
            StringBuilder sb = new StringBuilder(32);
            for (int i = 0; i < t.Length; i++)
            {
                sb.Append(t[i].ToString("x").PadLeft(2, '0'));
            }
            return sb.ToString();
        }

        [TestMethod]
        public void GetIdentifyBankCfgs()
        {
            var resutl = ConfigServiceProxy.GetIdentifyBankCfgs();
        }
    }
}
