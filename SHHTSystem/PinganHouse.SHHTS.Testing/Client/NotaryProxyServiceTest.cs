﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects.Filters;
using PinganHouse.SHHTS.RemoteServiceProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.Testing.Client
{
    [TestClass]
    public class NotaryProxyServiceTest
    {
        [TestMethod]
        public void Search()
        {
            NotaryFilter nf = new NotaryFilter { Keywords = "陈" };
            NotaryProxyService.GetInstanse().Search(nf);
        }

        [TestMethod]
        public void Inser_Update()
        {

            var d = new DataTransferObjects.NotaryDto
            {
                Name = Guid.NewGuid().ToString("n").Substring(0, 8),
                Mobile = "13800138000",
                OfficeName = "234345",
                CreateUserSysNo = 28291
            };

            NotaryProxyService.GetInstanse().Insert(d.Name, d.Mobile, d.OfficeName, d.CreateUserSysNo.Value);

            var obj = NotaryProxyService.GetInstanse().Get(d.Name, "13800138000");

            obj.Name = Guid.NewGuid().ToString("n").Substring(0, 8);
            obj.ModifyUserSysNo = 28291;

            NotaryProxyService.GetInstanse().Update(obj.SysNo, obj.Name, obj.Mobile, obj.OfficeName, obj.ModifyUserSysNo.Value);
        }
    }
}
