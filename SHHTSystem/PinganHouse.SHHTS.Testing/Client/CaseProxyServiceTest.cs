﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Testing.Client
{
    [TestClass]
    public class CaseProxyServiceTest
    {
        [TestMethod]
        public void GetCaseByCaseId()
        {
            var result = CaseProxyService.GetInstanse().GetCaseByCaseId("635591558532800677");
            Assert.IsNotNull(result);
        }

        //int totalCount = 0;
        //[TestMethod]
        //public void GetCaseList()
        //{

        //     var result = CaseProxyService.GetInstanse().GetPaginatedList(1, 10, out totalCount, ReceptionCenter.PuDong, CaseStatus.HD1 | CaseStatus.HD2 | CaseStatus.HD3 | CaseStatus.HD4 | CaseStatus.HD5, null,topStatus:CaseStatus.HD1, operatorName:"第三");
        //    Assert.IsTrue(result.Count != 0);
        //}

        //private static CaseStatus auditTaxStatus = CaseStatus.ZB1 |
        //                           CaseStatus.JY1 |
        //                           CaseStatus.JY2 |
        //                           CaseStatus.JY3 |
        //                           CaseStatus.JY4 |
        //                           CaseStatus.JY6 |
        //                           CaseStatus.JY7 |
        //                           CaseStatus.JY8 |
        //                           CaseStatus.JY9 |
        //                           CaseStatus.JY10 |
        //                           CaseStatus.JY11 |
        //                           CaseStatus.JY12 |
        //                           CaseStatus.JY11A1 |
        //                           CaseStatus.ZB6 |
        //                           CaseStatus.ZB7;
        ////买方贷款
        //private static CaseStatus buyerStatus = CaseStatus.DK1 | CaseStatus.DK2 | CaseStatus.DK3 | CaseStatus.DK4 | CaseStatus.ZB11
        //    | CaseStatus.DK5 | CaseStatus.DK6 | CaseStatus.DK7 | CaseStatus.DK8 | CaseStatus.DK9 | CaseStatus.DK10
        //    | CaseStatus.JY5 | CaseStatus.ZB2 | CaseStatus.ZB7;

        ////卖方还贷
        //private static CaseStatus sellerStatus = CaseStatus.ZB3 | CaseStatus.HD1 | CaseStatus.HD2 | CaseStatus.HD3
        //    | CaseStatus.HD4 | CaseStatus.HD5 | CaseStatus.HD6 | CaseStatus.ZB12;

        //CaseStatus allStatus = CaseStatus.YJ1
        //           | CaseStatus.ZB2 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.HA3 | CaseStatus.HA4
        //           | CaseStatus.HA5 | CaseStatus.HA6 | CaseStatus.ZB1 | CaseStatus.JY1 | CaseStatus.JY2
        //           | CaseStatus.JY3 | CaseStatus.JY4 | CaseStatus.JY5 | CaseStatus.JY6 | CaseStatus.JY7
        //           | CaseStatus.JY8 | CaseStatus.JY9 | CaseStatus.JY10 | CaseStatus.JY11 | CaseStatus.JY12
        //           | CaseStatus.ZB3 | CaseStatus.HD1 | CaseStatus.HD2 | CaseStatus.HD3 | CaseStatus.HD4
        //           | CaseStatus.HD5 | CaseStatus.HD6 | CaseStatus.DK1 | CaseStatus.DK2 | CaseStatus.DK3
        //           | CaseStatus.DK4 | CaseStatus.DK5 | CaseStatus.DK6 | CaseStatus.DK7 | CaseStatus.DK8
        //           | CaseStatus.DK9 | CaseStatus.DK10 | CaseStatus.ZB4 | CaseStatus.ZB5 | CaseStatus.ZB6
        //           | CaseStatus.ZB7 | CaseStatus.ZB8 | CaseStatus.ZB9 | CaseStatus.ZB10 | CaseStatus.ZB11 | CaseStatus.ZB12
        //           | CaseStatus.ZB13 | CaseStatus.JY11A1;
        [TestMethod]
        public void GetCaseListByAgent()
        {
       //     var allStatus = CaseStatus.YJ1
       //| CaseStatus.ZB2 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.HA3 | CaseStatus.HA4
       //| CaseStatus.HA5 | CaseStatus.HA6 | CaseStatus.ZB1 | CaseStatus.JY1 | CaseStatus.JY2
       //| CaseStatus.JY3 | CaseStatus.JY4 | CaseStatus.JY5 | CaseStatus.JY6 | CaseStatus.JY7
       //| CaseStatus.JY8 | CaseStatus.JY9 | CaseStatus.JY10 | CaseStatus.JY11 | CaseStatus.JY12
       //| CaseStatus.ZB3 | CaseStatus.HD1 | CaseStatus.HD2 | CaseStatus.HD3 | CaseStatus.HD4
       //| CaseStatus.HD5 | CaseStatus.HD6 | CaseStatus.DK1 | CaseStatus.DK2 | CaseStatus.DK3
       //| CaseStatus.DK4 | CaseStatus.DK5 | CaseStatus.DK6 | CaseStatus.DK7 | CaseStatus.DK8
       //| CaseStatus.DK9 | CaseStatus.DK10 | CaseStatus.ZB4 | CaseStatus.ZB5 | CaseStatus.ZB6
       //| CaseStatus.ZB7 | CaseStatus.ZB8 | CaseStatus.ZB9 | CaseStatus.ZB10 | CaseStatus.ZB11 | CaseStatus.ZB12
       //| CaseStatus.ZB13 | CaseStatus.JY11A1;


       //     var finishedStatus = CaseStatus.ZB13 | CaseStatus.ZB7 | CaseStatus.ZB2;
       //     var result = CaseProxyService.GetInstanse().GetPaginatedList(1, 1000, out totalCount, "C155542160305498", AgentType.AgentCompany, allStatus, new List<CaseStatus> { auditTaxStatus, buyerStatus, sellerStatus }, excludeCaseStatus: finishedStatus);
       //     Assert.IsTrue(result.Count != 0);
        }

        [TestMethod]
        public void GenerateCase()
        {
            var entity = new CaseDto
            {
                AgencySysNo = 10222,
                TenementAddress = "上海",

                ReceptionCenter = Enumerations.ReceptionCenter.PuDong,
                Remark = "Test",
                CreateUserSysNo = 10189,
                CompanySysNo = 10130,
                OperatorSysNo = 10226
            };
            entity.AddBuyer(10143, 10223, 10269, 10223);
            entity.AddSeller(10189, 10226);
            //for (int i = 0; i < 30; i++)
            //{
            //    entity.TenementContract = string.Format("宝{0}", DateTime.Now.Ticks);
            //    var result = ServiceDepository.CaseService.GenerateCase(entity, 52);
            //}
            entity.TenementContract = string.Format("宝{0}", DateTime.Now.Ticks);
            var result = CaseProxyService.GetInstanse().GenerateCase(entity);
            Assert.IsTrue(result.ResultNo==2);
            //Assert.IsTrue(result.Success);
        }

        [TestMethod]
        public void GetCases()
        {
            //int totalCount = 0;
            //var result = CaseProxyService.GetInstanse().GetPaginatedList(1, 10, out totalCount, ReceptionCenter.PuDong, CaseStatus.YJ1 | CaseStatus.HA1);
            //Assert.IsTrue(result.Count != 0);
        }

        [TestMethod]
      public void GetCaseAttachment()
      {
          var result = CaseProxyService.GetInstanse().GetCaseAttenment("06195474596202");
          Assert.IsNotNull(result);
      }


        [TestMethod]
        public void GetCaseAndProcess()
        {
            var dic = new Dictionary<CaseStatus, Tuple<string, DateTime>>();
            var result = CaseProxyService.GetInstanse().GetCaseAndProcess("06199181967382", CaseStatus.HA1 | CaseStatus.HA2, ref dic);
            Assert.IsTrue(result != null);
        }
        [TestMethod]
        public void Update()
        {
            var dic =new Dictionary<string,object>
            {
                {"1","ok"},
                {"34",3}
            };
            var result = CaseProxyService.GetInstanse().UpdateCaseStatus("06207570575614", CaseStatus.JY2, 10269);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetEvents()
        {
            int totalCount = 0;
            var result = CaseProxyService.GetInstanse().GetCaseEvents("06195474596202", 1, 2, out totalCount, CaseStatus.YJ1, CaseStatus.HA1);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetCaseEvents()
        {
            var result = CaseProxyService.GetInstanse().GetCaseEvents("06195474596202", CaseStatus.HA6, CaseStatus.HA4);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetCaseEvent()
        {
            int totalCount;
            var result = CaseProxyService.GetInstanse().GetCaseEvents("06195474596202",1, 10, out totalCount, CaseStatus.HA6);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ModifyNetlablePrice()
        {
            var result = CaseProxyService.GetInstanse().ModifyNetlabelPrice(16449, 1000, 0);

        }

        [TestMethod]
        public void GetBankAccount()
        {
            var result = CaseProxyService.GetInstanse().GetCustomerBankAccounts(16841);
        }

        [TestMethod]
        public void GetCustomer()
        {
            var result = CaseProxyService.GetInstanse().GetCaseCustomers("06202537410646");
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetCase1()
        {
            var result = CaseProxyService.GetInstanse().GetCaseWithCustomer("06202537410653", ReceptionCenter.PuDong);
        }

        [TestMethod]
        public void GetCaseByCaseIdAndStatus()
        {
            var result = CaseProxyService.GetInstanse().GetCaseByCaseIdAndCaseStatus("06218979083986", CaseStatus.DK1 | CaseStatus.DK2 | CaseStatus.DK3 | CaseStatus.DK4 | CaseStatus.ZB11
            | CaseStatus.DK5 | CaseStatus.DK6 | CaseStatus.DK7 | CaseStatus.DK8 | CaseStatus.DK9 | CaseStatus.DK10
            | CaseStatus.JY5 | CaseStatus.ZB2 | CaseStatus.ZB7);
            Assert.IsNotNull(result);
        }

    }
}
