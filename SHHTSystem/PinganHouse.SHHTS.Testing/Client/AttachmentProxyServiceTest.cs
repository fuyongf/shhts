﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Testing.Client
{
    [TestClass]
    public class AttachmentProxyServiceTest
    {
        [TestMethod]
        public void GetAttachments()
        {
            var result =AttachmentProxyService.GetInstanse().GetAttachments("createcaseId", 1, 10);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void SetAttachment()
        {
            var attachment = new AttachmentContent
            {
                AssociationNo = "10143",
                AttachmentType = Enumerations.AttachmentType.身份证,
                FileName = "beautiful",
                Remark = "beautiful",
            };

            string path = @"C:\Users\Public\Pictures\Sample Pictures\11.jpg";
            using (FileStream stream = File.OpenRead(path))
            {
                attachment.AttachmentContentData = stream;
                var result = AttachmentProxyService.GetInstanse().AddAttachment(attachment);
                Assert.IsNotNull(result);
                Assert.IsTrue(result.Success);
            }
 
        }
        [TestMethod]
        public void DownloadAttachment()
        {
            var result = AttachmentProxyService.GetInstanse().DownloadAttachment("55305640f855f55d38a8cdfd");
 
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetAttachs()
        {
            var result = AttachmentProxyService.GetInstanse().GetAttachments("12403", new List<AttachmentType> { AttachmentType.户口本 });
        }

        [TestMethod]
        public void DeleteAttachs()
        {
            var result = AttachmentProxyService.GetInstanse().DeleteAttachment("55544c2cf855f51b2847e1da",123456);
        }
    }
}
