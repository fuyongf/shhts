﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PinganHouse.SHHTS.DataTransferObjects.External;
using PinganHouse.SHHTS.RemoteServiceProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Testing.Client
{
    [TestClass]
    public class ParterProxyServiceTest
    {
        [TestMethod]
        public void AcceptMessage()
        {
            var message = new WXMessage { MsgType= Enumerations.WXMessageType.Image};
            var result = ParterProxyService.GetInstanse().AcceptMessage(message);
        }
    }
}
