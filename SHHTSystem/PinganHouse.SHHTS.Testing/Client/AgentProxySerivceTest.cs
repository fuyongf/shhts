﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Testing.Client
{
    [TestClass]
    public class AgentProxySerivceTest
    {
        [TestMethod]
        public void AddAgent()
        {
            var result = AgentServiceProxy.AgentJoint(new AgentCompanyModel
            {
                CompanyName = "德佑",
                Address = "不知道",
                ContactInfo = "20667246",
                Principal = "不认识"
            }, 0, AgentJointStatus.Jointing);
            Assert.AreEqual(result.Success, true);
        }

        [TestMethod]
        public void AgentJoint()
        {
            var result = AgentServiceProxy.AgentJoint(10131, 0);
            Assert.AreEqual(result.Success, true);
        }

        [TestMethod]
        public void Get()
        {
            var agent = AgentServiceProxy.GetAgentCompany(10131);
            Assert.IsNotNull(agent);
        }

        [TestMethod]
        public void Calcel()
        {
            var result = AgentServiceProxy.AgentCancel(10130, "测试", 0);
            Assert.AreEqual(result.Success, true);
        }

        [TestMethod]
        public void GetList()
        {
            int totalCount = 0;
            var result = AgentServiceProxy.GetAgentCompanyList(null, AgentJointStatus.Jointing, 1, 10, out totalCount);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void AddStaff()
        {
            //var result = AgentServiceProxy.CreateAgentStaff(new AgentStaffModel
            //{
            //    AgentCompanySysNo = 11603,
            //    Address = "测试地址",
            //    Birthday = DateTime.Now,
            //    CertificateType = CertificateType.IdCard,
            //    EffectiveDate = DateTime.Now,
            //    ExpiryDate = DateTime.Now,
            //    Gender = Gender.Female,
            //    IdentityNo = "310111198009222345",
            //    LoginName = "agent1",
            //    Mobile = "13333333333",
            //    Name = "二货",
            //    Nation = Nation.白族,
            //    Nationality = "中国",
            //    Password = "123456",
            //    VisaAgency = "测试"
            //}, 0);
            //Assert.AreEqual(result.Success, true);

            var result = AgentServiceProxy.CreateAgentStaff(new AgentStaffModel
            {
                AgentCompanySysNo = 11604,
                Address = "测试地址",
                Birthday = DateTime.Now,
                CertificateType = CertificateType.IdCard,
                EffectiveDate = DateTime.Now,
                ExpiryDate = DateTime.Now,
                Gender = Gender.Female,
                IdentityNo = "310111198009222346",
                LoginName = "agent2",
                Mobile = "13444444444",
                Name = "德佑经纪人",
                Nation = Nation.白族,
                Nationality = "中国",
                Password = "123456",
                VisaAgency = "测试"
            }, 0);
            Assert.AreEqual(result.Success, true);
        }
    }
}
