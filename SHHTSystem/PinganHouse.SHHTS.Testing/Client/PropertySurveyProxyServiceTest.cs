﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Testing.Client
{
    [TestClass]
    public class PropertySurveyProxyServiceTest
    {
        [TestMethod]
        public void AddPropertySurvey()
        {
            var dto =new PropertySurveyDto
            {
                TenementContract="-1231111",
                IsAttachment=true,
            };
            var result = PropertySurveyProxyService.GetInstanse().AddPropertySurvey(dto,null,null);
            Assert.IsNotNull(result);
        }

        int totalCount;
        [TestMethod]
        public void GetPaginatedList()
        {
            var result = PropertySurveyProxyService.GetInstanse().GetPaginatedList(1, 10, out totalCount);
            foreach (var v in result[0].Item2)
            {
                Assert.IsTrue(result[0].Item1.TenementContract == v.TenementContract);

            }
            Assert.IsNotNull(result);
        }

    }
}
