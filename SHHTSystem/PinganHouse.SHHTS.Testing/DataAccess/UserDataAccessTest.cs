﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Testing.DataAccess
{
    [TestClass]
    public class UserDataAccessTest
    {
        [TestMethod]
        public void UserInfo()
        {
            //bool result = ServiceDepository.UserDataAccess.Delete(0);
            //bool reuslt = ServiceDepository.CustomerDataAccess.Insert(new CustomerInfo
            //{
            //    SysNo = 1,
            //    CustomerType = CustomerType.Buyer,
            //    CreateDate = DateTime.Now,
            //    IdentityNo = "",
            //    UserId = "123"
            //});

            //bool reuslt1 = ServiceDepository.AgentStaffDataAccess.Insert(new AgentStaffInfo
            //{
            //    SysNo = 2,
            //    CreateDate = DateTime.Now,
            //    IdentityNo = "",
            //    UserId = "123",
            //    AgentCompanySysNo = 1
            //});

            //bool reuslt2 = ServiceDepository.PinganStaffDataAccess.Insert(new PinganStaffInfo
            //{
            //    SysNo = 3,
            //    CreateDate = DateTime.Now,
            //    IdentityNo = "",
            //    UserId = "123",
            //    UMCode = "test123"
            //});

            //bool reuslt3 = ServiceDepository.ThirdpartyUserDataAccess.Insert(new ThirdpartyUserInfo
            //{
            //    SysNo = 4,
            //    CreateDate = DateTime.Now,
            //    IdentityNo = "",
            //    UserId = "TP123",
            //    TpName = "test"
            //});
        }

        [TestMethod]
        public void GetUser()
        {
            //var user = ServiceDepository.UserDataAccess.Get(4);
            var user = ServiceDepository.UserDataAccess.Get(17737);

            var customer = ServiceDepository.CustomerDataAccess.Get(17737);
        }

        [TestMethod]
        public void Update()
        {
            var user = ServiceDepository.ThirdpartyUserDataAccess.Get(4);
            user.IdentityNo = "135679";
            bool result = ServiceDepository.ThirdpartyUserDataAccess.Update(user);
        }

        [TestMethod]
        public void GetCustomerByCase()
        {
            var result = ServiceDepository.CustomerDataAccess.GetCustomerByCase("06202537410653");
            Assert.IsNotNull(result);
        }


        [TestMethod]
        public void GetUserIdentity()
        {
            var result = ServiceDepository.UserIdentityDataAccess.Get("00805");
            result.EffectiveDate = DateTime.MinValue;
            result.ExpiryDate = DateTime.MaxValue;
            var b = ServiceDepository.UserIdentityDataAccess.Update(result);
        }

        [TestMethod]
        public void GetUserName()
        {
            var user = ServiceDepository.UserDataAccess.GetUserName("13588");
        }

        [TestMethod]
        public void GetByIdentityNo()
        {
            var user = ServiceDepository.UserDataAccess.GetByIdentityNo(CertificateType.IdCard, "341209198709293728");
            Assert.IsNotNull(user);
        }

    }
}
