﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Testing.DataAccess
{
    [TestClass]
    public class AgentDataAccessTest
    {
        [TestMethod]
        public void AddCompany()
        {
            bool result = ServiceDepository.AgentCompanyDataAccess.Insert(new AgentCompanyInfo
            {
                SysNo = 1,
                CreateDate = DateTime.Now,
                CompanyName = "test"
            });
        }

        [TestMethod]
        public void GetCompany()
        {
            //var company = ServiceDepository.AgentCompanyDataAccess.Get(1);
            bool exists = ServiceDepository.AgentCompanyDataAccess.Exists(2);
        }

        [TestMethod]
        public void GetPager()
        {
            int count = 0;
            var list = ServiceDepository.AgentCompanyDataAccess.GetPaginatedList("t", null, 1, 10, out count);
        }

        [TestMethod]
        public void GetAgentStaff()
        {
            int count = 0;
            var list = ServiceDepository.AgentStaffDataAccess.GetPaginatedListWithCompany("乳山路", null, null, 1, 100, out count);
        }
    }
}
