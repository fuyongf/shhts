﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Testing.DataAccess
{
    [TestClass]
    public class ReviewDataAccessTest
    {
        [TestMethod]
        public void CreateReview()
        { 
             ReviewInfo review = new ReviewInfo 
            { 
                ReviewCategory= Enumerations.ReviewCategory.Receipt,
                Reviewer="110,120",
                ReviewMode = "Timecard,Fingerprint",
            };
             var result = ServiceDepository.ReviewDataAccess.Insert(review);
             Assert.IsTrue(result);
        }


        [TestMethod]
        public void GetReview()
        {
            var result = ServiceDepository.ReviewDataAccess.Get(47455);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void CreateReviewDetail()
        {
            ReviewDetail reviewDetail = new ReviewDetail 
            { 
                Remark="ok",
                ReviewMode= Enumerations.ReviewMode.Timecard,
                ReviewSysNo = 47455,
                UserSysNo=110,
                IsValid=true,
                
            };
            var result = ServiceDepository.ReviewDetailDataAccess.Insert(reviewDetail);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void GetReviewDetail()
        {
            var result = ServiceDepository.ReviewDetailDataAccess.GetLastValidReviewDetail(47455);
            Assert.IsNotNull(result);
        }
    }
}
