﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Testing.DataAccess
{
    [TestClass]
    public class AttachmentDataAccessTest
    {
        [TestMethod]
        public void Insert()
        {
            var entity = new AttachmentInfo
            {
                AssociationNo = "12321321311",
                FileId = "122322qw332",
                FileName = "说的qw",
                AttachmentType = Enumerations.AttachmentType.身份证,
                Remark = "asds",
            };
            var result = ServiceDepository.AttachmentDataAccess.Insert(entity);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Get()
        {
            var res = ServiceDepository.AttachmentDataAccess.GetAttachments("12403", new List<AttachmentType> { AttachmentType.户口本 });
            var res1 = ServiceDepository.AttachmentDataAccess.GetAttachments("12403", AttachmentType.户口本);
        }
    }
}
