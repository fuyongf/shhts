﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Testing.DataAccess
{
    [TestClass]
    public class CaseDataAccessTest
    {
        [TestMethod]
        public void Insert()
        {
            var entity = new Case
            {
                SysNo = 12211211232,
                CaseId = DateTime.Now.Ticks.ToString(),
                AgencySysNo = 10189,//10143,
                TenementContract = "宝1212222222222",
                CaseStatus = CaseStatus.Unkown,
                OperatorSysNo = 10143,
                TenementAddress = "123",
                ReceptionCenter = ReceptionCenter.PuDong

            };
            entity.AddBuyer(10143, 10189);
            entity.AddSeller(10143, 10189);
            var result = ServiceDepository.CaseDataAccess.Insert(entity);
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void GetCaseByCaseId()
        {
            var dic = new Dictionary<string, int> {{"a",1},{"b",2},{"c",3},{"d",4} };
            var st = PinganHouse.SHHTS.Utils.JsonSerializer.SerializeToJson(dic);
            var result = ServiceDepository.CaseDataAccess.GetCaseByCaseId("635591557572344641");
            Assert.IsNotNull(result);
        }
        //int totalCount = 0;
        [TestMethod]
        public void GetPaginatedData()
        {
            //var data = new Dictionary<string, object> { { "agencyName", "测试中介" }, { "sellerName", "李婷婷"} };
            //CaseStatus status = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.HA3
            //   | CaseStatus.HA4 | CaseStatus.HA5 | CaseStatus.HA6 | CaseStatus.ZB2 | CaseStatus.ZB5;
            //var result = ServiceDepository.CaseDataAccess.GetPaginatedList(1, 10, out totalCount, ReceptionCenter.PuDong, status, data: data);
            //Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetPaginatedDatas()
        {
            //var allStatus = CaseStatus.YJ1
            //       | CaseStatus.ZB2 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.HA3 | CaseStatus.HA4
            //       | CaseStatus.HA5 | CaseStatus.HA6 | CaseStatus.ZB1 | CaseStatus.JY1 | CaseStatus.JY2
            //       | CaseStatus.JY3 | CaseStatus.JY4 | CaseStatus.JY5 | CaseStatus.JY6 | CaseStatus.JY7
            //       | CaseStatus.JY8 | CaseStatus.JY9 | CaseStatus.JY10 | CaseStatus.JY11 | CaseStatus.JY12
            //       | CaseStatus.ZB3 | CaseStatus.HD1 | CaseStatus.HD2 | CaseStatus.HD3 | CaseStatus.HD4
            //       | CaseStatus.HD5 | CaseStatus.HD6 | CaseStatus.DK1 | CaseStatus.DK2 | CaseStatus.DK3
            //       | CaseStatus.DK4 | CaseStatus.DK5 | CaseStatus.DK6 | CaseStatus.DK7 | CaseStatus.DK8
            //       | CaseStatus.DK9 | CaseStatus.DK10 | CaseStatus.ZB4 | CaseStatus.ZB5 | CaseStatus.ZB6
            //       | CaseStatus.ZB7 | CaseStatus.ZB8 | CaseStatus.ZB9 | CaseStatus.ZB10 | CaseStatus.ZB11 | CaseStatus.ZB12
            //       | CaseStatus.ZB13 | CaseStatus.JY11A1;


            //var finishedStatus = CaseStatus.ZB13 | CaseStatus.ZB7 | CaseStatus.ZB2;
            //var user = ServiceDepository.AgentStaffDataAccess.Get("C155542160305498");
            //var result = ServiceDepository.CaseDataAccess.GetPaginatedList(1, 10, out totalCount, user, AgentType.AgentCompany, allStatus, excludeCaseStatus: finishedStatus);
            //Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetPaginatedDataByBuyerName()
        {
            //var dic = new Dictionary<string, Object> { { "buyerName", "王晓伟" } };
            //var result = ServiceDepository.CaseDataAccess.GetPaginatedList(1, 10, out totalCount, ReceptionCenter.PuDong, PinganHouse.SHHTS.Enumerations.CaseStatus.ZB3 | PinganHouse.SHHTS.Enumerations.CaseStatus.HD1 | PinganHouse.SHHTS.Enumerations.CaseStatus.HD2 | PinganHouse.SHHTS.Enumerations.CaseStatus.ZB10 | PinganHouse.SHHTS.Enumerations.CaseStatus.ZB12,null);
            //Assert.IsNotNull(result);
        }

        //[TestMethod]
        //public void GetPaginatedDataByCaseId()
        //{
        //    var result = ServiceDepository.CaseDataAccess.GetPaginatedList(1, 10, out totalCount, ReceptionCenter.PuDong, CaseStatus.HA1|, 10224,);
        //    Assert.IsNotNull(result);
        //}

        //[TestMethod]
        //public void GetPaginatedDataByTenementAddress()
        //{
        //    var dic = new Dictionary<string, Object> { { "tenementAddress", "123" } };
        //    var result = ServiceDepository.CaseDataAccess.GetPaginatedList(1, 10, out totalCount, ReceptionCenter.PuDong, CaseStatus.HA1, data: dic);
        //    Assert.IsNotNull(result);
        //}

        //[TestMethod]
        //public void GetPaginatedDatas()
        //{
        //    var dic = new Dictionary<string, Object> { { "buyerName", "王晓伟" }, { "agencyName", "经纪人-1" }, };
        //    //startDate:DateTime.Now.AddDays(-1),endDate:DateTime.Now, data: dic,, caseStatus: CaseStatus.Cancel
        //    var result = ServiceDepository.CaseDataAccess.GetPaginatedList(1, 10, out totalCount, ReceptionCenter.PuDong, CaseStatus.ZB2 | CaseStatus.YJ1);
        //    Assert.IsNotNull(result);
        //}
        [TestMethod]
        public void GetCaseEventByCaseId()
        {
            var result = ServiceDepository.CaseEventDataAccess.GetNewByCaseId("06188813518716", CaseStatus.YJ1 | CaseStatus.HA1);//Get("06188813518716");//
        }

        [TestMethod]
        public void BankAccount()
        {
            var result = ServiceDepository.CustomerBankAccountDataAccess.GetByCase(17422);
        }
        [TestMethod]
        public void GetTrades()
        {
            var result = ServiceDepository.TradeDataAccess.GetTrades("06202537410653", AccountType.Buyer);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetCaseEvents()
        {
            var caseEvents = ServiceDepository.CaseEventDataAccess.GetCaseEvents(CaseStatus.JY8);
            ServiceDepository.CaseEventDataAccess.UpdateRemark(caseEvents.FirstOrDefault().SysNo, "已发送");
            Assert.IsNotNull(caseEvents);
        }
    }
}
