﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;

namespace PinganHouse.SHHTS.Testing.DataAccess
{
    [TestClass]
    public class OrderDataAccessTest
    {
        [TestMethod]
        public void GetByOrder()
        {
            var result = ServiceDepository.TradeDataAccess.GetByOrder("2015031906356545");
        }

        [TestMethod]
        public void GetOrder()
        {
            var result = ServiceDepository.OrderDataAccess.Get("2015032606505672");

        }

        [TestMethod]
        public void OrderList()
        {
            int totalCount = 0;
            var result = ServiceDepository.OrderDataAccess.GetPaginatedList(1, 100, out totalCount, ReceptionCenter.PuDong, null, null, null, null, null, null, null, null);
        }

        [TestMethod]
        public void TradeList()
        {
            int totalCount = 0;
            var result = ServiceDepository.TradeService.GetTrades(1, 1000, out totalCount, ReceptionCenter.PuDong, null, null, null, null, null, null, null);

        }

        [TestMethod]
        public void AccountList()
        {
            int totalCount = 0;
            var result = ServiceDepository.AccountDataAccess.GetPaginatedList(1, 1000, out totalCount, ReceptionCenter.PuDong, null, null, null, null, null);
        }

        [TestMethod]
        public void GetOrderByStatus()
        {
            var result = ServiceDepository.OrderDataAccess.GetOrdersByStatus(OrderType.Collection, OrderStatus.Initial);
        }

        [TestMethod]
        public void GetTrderByOrder()
        {
            var result = ServiceDepository.TradeDataAccess.GetByOrder("2015032610996046");
        }

        [TestMethod]
        public void GetRefundOrder()
        {
            int totalCount = 0;

            var r = TradeServiceProxy.GetOrders(1, 1000, out totalCount, ReceptionCenter.PuDong, null, null, null, null, null, null, null, null);
            //var result = ServiceDepository.TradeService.GetRefundOrders(1,1000,out totalCount, ReceptionCenter.PuDong,null,null,null,null);
            var res = TradeServiceProxy.GetRefundOrders(1, 1000, out totalCount, ReceptionCenter.PuDong, null, null, null, null);
        }

        [TestMethod]
        public void GetBillByOrder()
        {
            int totalCount = 0;
            //var result = ServiceDepository.TradeService.GetBillByOrder("2015040306389455",null, BillType.Expened);
            //var result = TradeServiceProxy.GetPosBillByOrder("2015040207297094", BillType.Expened);
            var result = ServiceDepository.OrderDataAccess.GetPaginatedList(1, 100, out totalCount, ReceptionCenter.PuDong, null, null, null, null, null, null, null, null);
            //var result = TradeServiceProxy.GetOrders(1, 100, out totalCount, ReceptionCenter.PuDong, null, null, null, null, null, null, null, null);
        }

        [TestMethod]
        public void GetBills()
        {
            var result = ServiceDepository.TradeDataAccess.GetSendBillTrades();
        }

        [TestMethod]
        public void GetTrade()
        {
            var trade = TradeServiceProxy.GetTradeInfo("2015040818810725");

            int totalCount = 0;
            var result = TradeServiceProxy.GetTrades(1, 1, out totalCount, ReceptionCenter.PuDong, null, null, "2015040818810725", null, null, null, null);
        }

        [TestMethod]
        public void Receipt()
        {
            int totalCount = 0;
            var re = ServiceDepository.TradeService.GetReceiptBySecurityCode("2130339d265b7c67f22810b20eff567d");
            var res = ServiceDepository.TradeService.GetReceipts(1, 100, out totalCount, ReceptionCenter.PuDong, null, null);

            var ret = ServiceDepository.TradeService.AbolishReceipt(23723, 10224);
        }

        [TestMethod]
        public void GetOrderByAccount()
        {
            int totalCount = 0;
            var result = ServiceDepository.OrderDataAccess.GetPaginatedListByAccount(1, 100, out totalCount, false, 42656, 42700, 42710, 42729, 42815, 42855, 42895, 42932, 42977, 43017, 43054, 43095, 43114, 43176, 43224, 43267, 43283, 43299, 43317, 43337, 43358, 43381, 43404, 43428, 43479, 43531, 43585, 43606, 43625, 43662, 43702, 43742, 43782, 43835, 43870, 43917, 43940, 43970, 43993, 44023, 44061, 44158, 44203, 44253, 44336, 44353, 44381, 44402, 44436, 44459, 44496, 44539, 44553, 44567, 44583, 44599, 44620, 44663, 44697, 44730, 44766, 44798, 44899, 45465, 45498, 45570, 45588, 45624, 45649, 45706, 45733, 45765, 45816, 45870, 45935, 45981, 46009, 46057, 46103, 46440, 46645, 46708, 46719, 46757, 46774, 46791, 46810, 46834);
            //var result1 = ServiceDepository.OrderDataAccess.GetOrderByAccount(42668, DateTime.Now.AddDays(-30), DateTime.Now);

            //var res = ServiceDepository.AccountService.GetAccountBill(AccountType.Agency, "13392", null, null);

        }
    }
}
