﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Testing.DataAccess
{
    [TestClass]
    public class ParterUserDataAccessTest
    {
        [TestMethod]
        public void Insert()
        {
            //DateTime.Now.Ticks.ToString()
            var entity = new ParterUserInfo { OpenID=DateTime.Now.Ticks.ToString(),UserSysNo=123455,NickName="2121"};
            var result = ServiceDepository.ParterUserInfoDataAccess.Insert(entity);
        }

        [TestMethod]
        public void Get()
        {
            Assert.IsTrue(true == true);
            var entity = ServiceDepository.ParterUserInfoDataAccess.Get("oWjycs45rEa3jMEVfe5eeiI3e4NA");
            var result = ServiceDepository.ParterUserInfoDataAccess.DeleteByOpenId("oWjycs45rEa3jMEVfe5eeiI3e4NA");
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void InsertMessage()
        {
            var dic = new Dictionary<string, object>();
            dic["url"] = "";
            dic["topcolor"] = "#FF0000";
            var data = new Dictionary<string, object> 
                { 
                    { "first", new Dictionary<string, object> { { "value", 1 }}},
                    { "keyword1", new Dictionary<string, object> { { "value", 2 }}},
                    { "keyword2", new Dictionary<string, object> { { "value", 3 } }}, 
                    { "keyword3", new Dictionary<string, object> { { "value", 4} }}, 
                    { "remark", new Dictionary<string, object> { { "value", "你好"} }}, 
                };
            dic["data"] = data;
            var message = new WXTemplateMessage { 
                MsgId="1234",
                Data = data,               
            };
            var result = ServiceDepository.WXTemplateMessageDataAccess.Insert(message);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void GetMessage()
        {
            var result = ServiceDepository.WXTemplateMessageDataAccess.GetMessageByMsgId("1234");
            result.Remark = "12";
            result.SendsCount = 1;
            result.Status= WXStatus.Success;
            var s = ServiceDepository.WXTemplateMessageDataAccess.Update(result);
            ServiceDepository.WXTemplateMessageDataAccess.UpdateByMsgId(result.MsgId, WXStatus.UserBlock);
            Assert.IsNotNull(result);
        }

        
    }
}
