﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Testing.Service
{
    [TestClass]
    public class AgentCompanyServiceTest
    {
        [TestMethod]
        public void AgentJoint()
        {
            AgentCompany agentCompany = new AgentCompany { 
                Address="上海",
               CompanyName="测试数据",
               ContactInfo="110",
               JointContractNo="11222111112",
               Principal="马问",

            };
            var result = ServiceDepository.AgentService.AgentJoint(agentCompany);
            Assert.IsTrue(result.Success);
        }
    }
}
