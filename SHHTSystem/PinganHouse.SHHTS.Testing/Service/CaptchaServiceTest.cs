﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PinganHouse.SHHTS.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.Testing.Service
{
    [TestClass]
    public class CaptchaServiceTest
    {
        [TestMethod]
        public void RequestCaptcha()
        {
            var s = "ASba";
            Assert.IsTrue(string.Compare(s, "asBA", true)==0);
            Assert.IsFalse(string.Compare(s, "asBA") == 0);
            var captcha = ServiceDepository.CaptchaService.RequetCaptcha("123456", Enumerations.BusinessCacheType.BindIdentity);
            Assert.IsNotNull(captcha);
            var result = ServiceDepository.CaptchaService.ValidateCaptcha(captcha.Key, "123213");
            Assert.IsFalse(result.Success);
            result = ServiceDepository.CaptchaService.ValidateCaptcha(captcha.Key, captcha.Value);
            Assert.IsTrue(result.Success);
        }

        [TestMethod]
        public void SendBindCaptcha()
        {
            var result = ServiceDepository.SmsService.SendBindCaptcha("18116121123");
            Assert.IsTrue(result.Success);
        }
    }
}
