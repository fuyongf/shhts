﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.Testing.Service
{
    [TestClass]
    public class NotaryServiceTest
    {
        [TestMethod]
        public void Search()
        {
            NotaryFilter nf = new NotaryFilter { Keywords = "陈" };
            ServiceDepository.NotaryService.Search(nf);
        }

        [TestMethod]
        public void Inser_Update()
        {

            var d = new DataTransferObjects.NotaryDto
            {
                Name = Guid.NewGuid().ToString("n").Substring(0, 8),
                Mobile = "13800138000",
                OfficeName = "234345",
                CreateUserSysNo = 28291
            };

            ServiceDepository.NotaryService.Insert(d.Name, d.Mobile, d.OfficeName, d.CreateUserSysNo.Value);

            var obj = ServiceDepository.NotaryService.Get(d.Name, "13800138000");

            obj.Name = Guid.NewGuid().ToString("n").Substring(0, 8);
            obj.ModifyUserSysNo = 28291;

            ServiceDepository.NotaryService.Update(obj.SysNo, obj.Name, obj.Mobile, obj.OfficeName, obj.ModifyUserSysNo.Value);
        }
    }
}
