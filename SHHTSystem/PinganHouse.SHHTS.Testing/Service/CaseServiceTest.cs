﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.DataTransferObjects.Filters;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.ServiceImpl;
using PinganHouse.SHHTS.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace PinganHouse.SHHTS.Testing.Service
{
    [TestClass]
    public class CaseServiceTest
    {
        [TestMethod]
        public void GenerateCase()
        {
            var entity = new CaseDto {
                AgencySysNo = 13588,
                TenementAddress="上海",
                SourceType= Enumerations.SourceType.ReceptionCenter,
                ReceptionCenter= Enumerations.ReceptionCenter.PuDong,
                Remark="Test",
                CreateUserSysNo = 28243,
                CompanySysNo = 13392,
                OperatorSysNo = 28243,
                TenementContract=string.Format("宝{0}", DateTime.Now.Ticks),
            };
            var customer = new Customer 
            { 
                Mobile = "18116121123",
                RealName = "Test",
                CertificateType = CertificateType.IdCard, 
                CustomerType= CustomerType.Buyer,
                IdentityNo="1234567897894561220",
            };
            var buyer = ServiceDepository.UserService.CreateCustomer(customer);
            customer.CustomerType = CustomerType.Seller;
            var seller = ServiceDepository.UserService.CreateCustomer(customer);
            entity.AddBuyer(long.Parse(buyer.OtherData["UserSysNo"].ToString()));
            entity.AddSeller(long.Parse(seller.OtherData["UserSysNo"].ToString()));
            var result = ServiceDepository.CaseService.GenerateCase(entity);
           
            Assert.IsTrue(result.Success);
        }

        private string GetCaseId()
        {
            var entity = new CaseDto
            {
                AgencySysNo = 28299,
                TenementAddress = "上海",
                SourceType = Enumerations.SourceType.ReceptionCenter,
                ReceptionCenter = Enumerations.ReceptionCenter.PuDong,
                Remark = "Test",
                CreateUserSysNo = 28243,
                CompanySysNo = 13392,
                OperatorSysNo = 28243,
                TenementContract = string.Format("宝{0}", DateTime.Now.Ticks),
            };
            var customer = new Customer
            {
                Mobile = "13636670628",
                RealName = "Test",
                CertificateType = CertificateType.IdCard,
                CustomerType = CustomerType.Buyer,
                IdentityNo = "1234567897894561220",
            };
            var buyer = ServiceDepository.UserService.CreateCustomer(customer);
            customer.CustomerType = CustomerType.Seller;
            var seller = ServiceDepository.UserService.CreateCustomer(customer);
            entity.AddBuyer(long.Parse(buyer.OtherData["UserSysNo"].ToString()));
            entity.AddSeller(long.Parse(seller.OtherData["UserSysNo"].ToString()));
            var result = ServiceDepository.CaseService.GenerateCase(entity);
            return result.ResultMessage;
        }

       

        [TestMethod]
        public void Updatajy1()
        {
            var caseId = GetCaseId();
            var result = ServiceDepository.CaseService.UpdateCaseStatus(caseId, CaseStatus.HA1, 10269);
            //result = ServiceDepository.CaseService.UpdateCaseStatus(caseId, CaseStatus.HA2, 10269);
            result = ServiceDepository.CaseService.UpdateCaseStatus(caseId, CaseStatus.ZB1, 10269, new Dictionary<string, object> { { "booktime", DateTime.Now } });
            //result = ServiceDepository.CaseService.UpdateCaseStatus(caseId, CaseStatus.JY1, 10269);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetCaseByCaseId()
        {
            var result = ServiceDepository.CaseService.GetCaseByCaseId("06195139051592");
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetCaseByCaseIdAndStatus()
        {
            var result = ServiceDepository.CaseService.GetCaseByCaseIdAndCaseStatus("06218979083986", CaseStatus.ZB3 | CaseStatus.HD1 | CaseStatus.HD2 | CaseStatus.HD3
            | CaseStatus.HD4 | CaseStatus.HD5 | CaseStatus.HD6 | CaseStatus.ZB12);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetCaseByCase()
        {
            var result = ServiceDepository.CaseService.GetCaseByCaseId("06207571230926", ReceptionCenter.BaoShan);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void DeleteCaseByCaseId()
        {
            var result = ServiceDepository.CaseService.DeleteCaseByCaseId("635591558532800677", "12");
            Assert.IsTrue(result.Success);
        }

        [TestMethod]
        public void GetCases()
        {
            int totalCount = 0;
            var dic =new Dictionary<string, object>();
            dic["agencyName"] = "经纪人";
            var result = ServiceDepository.CaseService.GetPaginatedList(1, 10, out totalCount, ReceptionCenter.PuDong, CaseStatus.HA2, data: dic);
            result = ServiceDepository.CaseService.GetPaginatedList(1, 10, out totalCount, ReceptionCenter.PuDong, CaseStatus.DK9);
            Assert.IsTrue(result.Count !=0);
        }

        int totalCount = 0;
        [TestMethod]
        public void GetCaseList()
        {

            var st = CaseStatus.DK1 | CaseStatus.DK2 | CaseStatus.DK3 | CaseStatus.DK4 | CaseStatus.ZB11
            | CaseStatus.DK5 | CaseStatus.DK6 | CaseStatus.DK7 | CaseStatus.DK8 | CaseStatus.DK9 | CaseStatus.JY5;
            var result = ServiceDepository.CaseService.GetPaginatedList(1, 10, out totalCount, ReceptionCenter.PuDong, st);
            Assert.IsTrue(result.Count != 0);
        }

        [TestMethod]
        public void GetPaginatedData()
        {               
            var dic = new Dictionary<string, Object> { { "operatorName", "第三" }};
            var result = ServiceDepository.CaseService.GetPaginatedList(1, 10, out totalCount, ReceptionCenter.PuDong, PinganHouse.SHHTS.Enumerations.CaseStatus.ZB3 | PinganHouse.SHHTS.Enumerations.CaseStatus.HD1 | PinganHouse.SHHTS.Enumerations.CaseStatus.ZB7 | PinganHouse.SHHTS.Enumerations.CaseStatus.ZB10 | PinganHouse.SHHTS.Enumerations.CaseStatus.ZB12, null, topStatus: CaseStatus.HD1, data: dic);
            Assert.IsNotNull(result);
        }

        private static CaseStatus auditTaxStatus = CaseStatus.ZB1 |
                                  CaseStatus.JY1 |
                                  CaseStatus.JY2 |
                                  CaseStatus.JY3 |
                                  CaseStatus.JY4 |
                                  CaseStatus.JY6 |
                                  CaseStatus.JY7 |
                                  CaseStatus.JY8 |
                                  CaseStatus.JY9 |
                                  CaseStatus.JY10 |
                                  CaseStatus.JY11 |
                                  CaseStatus.JY12 |
                                  CaseStatus.JY11A1 |
                                  CaseStatus.ZB6 |
                                  CaseStatus.ZB7;
        //买方贷款
        private static CaseStatus buyerStatus = CaseStatus.DK1 | CaseStatus.DK2 | CaseStatus.DK3 | CaseStatus.DK4 | CaseStatus.ZB11
            | CaseStatus.DK5 | CaseStatus.DK6 | CaseStatus.DK7 | CaseStatus.DK8 | CaseStatus.DK9 | CaseStatus.DK10
            | CaseStatus.JY5 | CaseStatus.ZB2 | CaseStatus.ZB7;

        //卖方还贷
        private static CaseStatus sellerStatus = CaseStatus.ZB3 | CaseStatus.HD1 | CaseStatus.HD2 | CaseStatus.HD3
            | CaseStatus.HD4 | CaseStatus.HD5 | CaseStatus.HD6 | CaseStatus.ZB12;
        CaseStatus allStatus = CaseStatus.YJ1
           | CaseStatus.ZB2 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.HA3 | CaseStatus.HA4
           | CaseStatus.HA5 | CaseStatus.HA6 | CaseStatus.ZB1 | CaseStatus.JY1 | CaseStatus.JY2
           | CaseStatus.JY3 | CaseStatus.JY4 | CaseStatus.JY5 | CaseStatus.JY6 | CaseStatus.JY7
           | CaseStatus.JY8 | CaseStatus.JY9 | CaseStatus.JY10 | CaseStatus.JY11 | CaseStatus.JY12
           | CaseStatus.ZB3 | CaseStatus.HD1 | CaseStatus.HD2 | CaseStatus.HD3 | CaseStatus.HD4
           | CaseStatus.HD5 | CaseStatus.HD6 | CaseStatus.DK1 | CaseStatus.DK2 | CaseStatus.DK3
           | CaseStatus.DK4 | CaseStatus.DK5 | CaseStatus.DK6 | CaseStatus.DK7 | CaseStatus.DK8
           | CaseStatus.DK9 | CaseStatus.DK10 | CaseStatus.ZB4 | CaseStatus.ZB5 | CaseStatus.ZB6
           | CaseStatus.ZB7 | CaseStatus.ZB8 | CaseStatus.ZB9 | CaseStatus.ZB10 | CaseStatus.ZB11 | CaseStatus.ZB12
           | CaseStatus.ZB13 | CaseStatus.JY11A1;
        [TestMethod]
        public void GetCaseListByAgent()
        {
            var data = new Dictionary<string, object>();
            data["MoblePhone"] = "13916972945";
            var result = ServiceDepository.CaseService.GetPaginatedList(1, 10, out totalCount, "C155542160305498", AgentType.AgentCompany, allStatus, new List<CaseStatus> { auditTaxStatus, buyerStatus, sellerStatus }, data: data);
            Assert.IsTrue(result.Count != 0);
        }


        [TestMethod]
        public void GetPaginatedData1()
        {
            CaseStatus status = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.HA3
               | CaseStatus.HA4 | CaseStatus.HA5 | CaseStatus.HA6 | CaseStatus.ZB2 | CaseStatus.ZB5;
            var dic = new Dictionary<string, object> {{"CreatorName", "第三"} };
            var result = ServiceDepository.CaseService.GetPaginatedList(1, 10, out totalCount, ReceptionCenter.PuDong, status, caseStatus: CaseStatus.YJ1, data:dic);
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void GenerateId()
        {
            for(int i=0;i<=10000;i++)
            {
                dic.Add(Generator.GenerateCaseId(52),1);
            }
            Assert.IsNotNull(dic);
        }
        Dictionary<string, int> dic = new Dictionary<string, int>(); 

        [TestMethod]
        public void GetCaseAttachment()
        {
            var result = ServiceDepository.CaseService.GetCaseAttenment("06199181967375");
            Assert.IsNotNull(result);
        }



        [TestMethod]
        public void UnBind()
         {
            var result = ServiceDepository.CaseService.UnBindUser("06192840048923", 10143, CaseUserType.Buyer, 10269);
            Assert.IsTrue(result.Success);
        }

        [TestMethod]
        public void Bind()
        {
            var result = ServiceDepository.CaseService.BindUsers("06192840048923",  CaseUserType.Buyer, 10270);
            Assert.IsTrue(result.Success);
        }
         [TestMethod]
        public void UpdateStatus()
        {
            var result = ServiceDepository.CaseService.UpdateCaseStatus("06226026172240", CaseStatus.JY8, 10270, new Dictionary<string, object> { { "booktime", DateTime.Now } });
            Assert.IsTrue(result.Success);
        }


        
        [TestMethod]
        public void UpdateCase()
        {
            var bus = new List<long> { 12936, 12937 };
            var sellers = new List<long> { 12948 };
            var result = ServiceDepository.CaseService.Update("06198510878732", bus, sellers, 13479, 10270,"上海市平安好房集团", "宝635619219827368181");
            Assert.IsTrue(result.Success);
        }

        [TestMethod]
        public void GetCustomerByCase()
        {
            var result = ServiceDepository.UserService.GetCustomerByCase("06202537410653");
            Assert.IsTrue(result!=null);
        }

        [TestMethod]
        public void GetCaseAndProcess()
        {
            var dic  = new Dictionary<CaseStatus, Tuple<string, DateTime>>();
            var result = ServiceDepository.CaseService.GetCaseAndProcess("06199181967382", CaseStatus.HA1 | CaseStatus.HA2, out dic);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public void GetCaseEvents()
        {
            var result = ServiceDepository.CaseService.GetCaseEvents("06195474596202", CaseStatus.HA6, CaseStatus.HA4);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void UpdateCaseEvents()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("booktime", DateTime.Now);
            dic.Add("remarks", "1");
            var result = ServiceDepository.CaseService.UpdateCaseEvent(61916, dic, 10224);
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void GetTenementContracts()
        {
            var result = ServiceDepository.CaseService.GetTenementContracts(QueryStringType.CaseId, "06202537410653", ReceptionCenter.BaoShan);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetCases1()
        {
            var result = ServiceDepository.CaseService.GetCases(1, 10, out totalCount, ReceptionCenter.PuDong, QueryStringType.CaseId, "11");
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetCaseCustomer() 
        {
            var result = ServiceDepository.CaseService.GetCaseCustomers("06202872954890");
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void InsertCaseTrail()
        {
            var dto = new CaseTrailDto { CaseId = "06202537410653", ProccessType = ProccessType.SeriousDelay, CreateUserSysNo = 10224, Remark="ok" };
            var result = ServiceDepository.CaseService.SetCaseTrail(dto);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetCaseTrailPaged()
        {
            var categorys = new List<CaseStatus>();
            categorys.Add(CaseStatus.DK1| CaseStatus.DK2| CaseStatus.DK3| CaseStatus.DK4| CaseStatus.DK5| CaseStatus.DK6);
            var result = ServiceDepository.CaseService.GetCaseTrailPaged(1, 10, out totalCount, ReceptionCenter.PuDong, CaseStatus.HA2, categorys);// ProccessType.SeriousDelay, DateTime.Now.AddDays(-10), "第"
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetIds()
        {
            int totalCount ;
            var result = ServiceDepository.CaseService.GetCaseIds(1, 10, out totalCount, ReceptionCenter.PuDong, "0620287295490");
        }

        [TestMethod]
        public void Get1() 
        {
            var result = ServiceDepository.CaseService.GetCases(1, 10, out totalCount, ReceptionCenter.PuDong,QueryStringType.TenementAddress, "上海市闸北区");
            
        }

        [TestMethod]
        public void GetBank()
        {
            var result = ServiceDepository.ConfigService.GetBankDic();
        }

        [TestMethod]
        public void ValidationMobile()
        {
            var result = Validation.Mobile("12233");
            Assert.IsFalse(result);
            result = Validation.Mobile("12233123456");
            Assert.IsFalse(result);
            result = Validation.Mobile("02233123456");
            Assert.IsFalse(result);
            result = Validation.Mobile("152233123456");
            Assert.IsFalse(result);
            result = Validation.Mobile("155233123456");
            Assert.IsFalse(result);
            result = Validation.Mobile("15523312345");
            Assert.IsTrue(result);
            result = Validation.Mobile("a5523312345");
            Assert.IsFalse(result);
            result = Validation.Mobile("18523312345");
            Assert.IsTrue(result);
            result = Validation.Mobile("18123312345");
            Assert.IsTrue(result);
        }



        [TestMethod]
        public void StatusFlow()
        {
            string caseId = "06240454003730";
            long muid = 28291;
            Dictionary<string, object> data = new Dictionary<string, object>();


            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "YJ1_HA1", muid);

            //data["_isTrialTax"] = true;
            //data["trialTaxOther"] = "abc";
            //data["_netlabelPrice"] = 15000M;
            ServiceDepository.CaseService.UpdateCaseStatus(caseId, "Any_ZB13", muid, data);

            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "HA2_HA3", muid);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "HA3_HA4", muid);

            //data.Clear();
            //data["_isPass"] = true;
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "HA4_HA5", muid, data);


            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "ZB10_ZB3", muid);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "ZB3_HD1", muid);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "HD1A1_HD1A1", muid);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "HD1_HD2", muid);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "HD2_HD3", muid);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "HD3_HD4", muid);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "HD4_HD5", muid);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "HD5_HD6", muid);


            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "ZB9_DK1", muid);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "DK1_DK2", muid);
            //data["something"] = "sdfd";
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "DK2A1_DK2A1", muid, data);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "DK2_DK3", muid);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "DK3_DK4", muid);
            //data.Clear();
            //data["_isPass"] = true;
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "DK4_DK5", muid, data);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "DK5_DK6", muid);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "DK6_JY5", muid);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "JY5_DK7", muid);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "DK7_DK8", muid);


            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "ZB1A1_ZB1A1", muid);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "ZB1_JY1", muid);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "JY1A1_ZB1", muid);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "ZB1_JY1", muid);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "JY1_JY2", muid);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "JY2_JY3", muid);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "JY3_JY4", muid);
            //data["_isLimit"] = true;
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "JY4_JY6", muid, data);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "JY9_JY4", muid);
            //data["_isLimit"] = false;
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "JY4_JY6", muid, data);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "JY6_JY8", muid);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "JY8_JY7", muid);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "JY7_JY10", muid);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "JY10_JY11", muid);
            //ServiceDepository.CaseService.UpdateCaseStatus(caseId, "JY11A1_JY12", muid);
        }

        [TestMethod]
        public void GetPaginatedDataNew()
        {
            var statuses = new CaseStatus[]{CaseStatus.YJ1, CaseStatus.HA1, CaseStatus.HA2, CaseStatus.HA3
            , CaseStatus.HA4, CaseStatus.HA5, CaseStatus.HA6, CaseStatus.ZB2, CaseStatus.ZB5, CaseStatus.ZB6
            , CaseStatus.ZB7, CaseStatus.ZB13};

            CaseFilter cf = new CaseFilter();
            cf.Statuses = statuses;
            cf.ReceptionCenters.Add(ReceptionCenter.PuDong);
            int totalCount;
            var list = ServiceDepository.CaseService.GetPaginatedList(cf, 2, 10, out totalCount);

        }
        
    }

}
