﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Testing.Service
{
    [TestClass]
    public class LoanOptimServiceTest
    {
        [TestMethod]
        public void Test1()
        {
            OInputs ips = new OInputs();
            ips.UserBasic1 = Gender.Male;
            ips.UserBasic2 = 15000M;
            ips.UserBasic3 = 200M;
            ips.UserBasic4 = 2000;
            ips.UserBasic5 = ResideType.Normal;
            ips.UserBasic6 = 26;
            ips.UserBasic7 = false;

            ips.UserRequire1 = 150M;
            ips.UserRequire2 = 4000M;
            ips.UserRequire3 = 30;
            ips.UserRequire4 = RepayType.ConstantPayment;

            ips.Gjj1 = 50000M;
            ips.Gjj2 = 1300M;
            ips.Gjj3 = 0M;
            ips.Gjj4 = 0M;
            ips.Gjj5 = 0M;
            ips.Gjj6 = 0M;
            ips.Gjj7 = true;
            ips.Gjj8 = false;
            ips.Gjj9 = false;
            ips.Gjj10 = false;
            ips.Gjj11 = true;
            ips.Gjj12 = false;

            ips.Xgxd1 = MaritalStatus.Single;
            ips.Xgxd2 = 0;
            ips.Xgxd3 = 0;
            ips.Xgxd4 = DomicileType.Local;

            //var models = ServiceDepository.LoanOptimizeService.GetPlans(ips);
        }
    }
}
