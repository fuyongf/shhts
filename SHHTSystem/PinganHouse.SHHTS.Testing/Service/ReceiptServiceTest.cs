﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Testing.Service
{
    [TestClass]
    public class ReceiptServiceTest
    {
        [TestMethod]
        public void GenerateReceipt()
        {
            ReceiptView dto = new ReceiptView();
            var result = ServiceDepository.TradeService.GenerateReceipt("201503200635672", 10143, out dto);
            Assert.IsNotNull(dto.ReceiptNo);
        }

        [TestMethod]
        public void PrivewReceipt()
        {
            var result = ServiceDepository.TradeService.PreviewReceipt("2015040206778950", "2015040294308555");
            Assert.IsNotNull(result.ReceiptNo);
        }


        [TestMethod]
        public void RePrint()
        {
            //var dto = ServiceDepository.TradeService.GetReceiptInfo(17572);
            ReceiptView dto = null;
            var result = ServiceDepository.TradeService.RePrintReceipt(17572, 10143, out dto, "test");
            Assert.IsTrue(result.Success);
        }

        //int totalCount = 0;
        [TestMethod]
        public void Get()
        {
            //var result = ServiceDepository.TradeService.GetReceipts(1, 10, out totalCount, ReceptionCenter.PuDong, startDate: DateTime.Now.Date.AddDays(-20));
            //Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetReceipts()
        {
            int totalCount = 0;
            ServiceDepository.TradeService.GetReceipts(1, 100, out totalCount, ReceptionCenter.PuDong, null, null);
        }

        [TestMethod]
        public void GetReceipt()
        {
            var result = ServiceDepository.TradeService.GetReceiptInfo(24148);
        }
    }
}
