﻿using HTB.DevFx;
using HTB.DevFx.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Core.External;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.DataTransferObjects.External;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Policy;
using System.Text;
using System.Web;

namespace PinganHouse.SHHTS.Testing.Service
{
    [TestClass]
    public class WeiXinServiceTest
    {
        [TestMethod]

        public void Refresh()
        {
            var timespan = TimeSpan.FromMinutes(5).Ticks;
            var date = DateTime.Now.AddTicks(timespan);
            object d = "2015/08/07 14:14";
            var t = DateTime.Parse(d.ToString()).ToString("MM月dd日HH：mm");
            
            var result = ServiceDepository.ParterChannelService.RefreshAccessToken(AgentType.AgentCompany);
            Assert.IsTrue(result.Success);
        }

        private  int GetMachineHash()
        {
            var hostName = Environment.MachineName; // use instead of Dns.HostName so it will work offline
            var i = hostName.GetHashCode();
            var s = Convert.ToString(i, 2);
            return 16777215 & i; // use first 3 bytes of hash
        }
        [TestMethod]

        public void GetQRCodeUrl()
        {
            var result = ServiceDepository.ParterChannelService.GetQrCodeUrl(61408);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void SendTemplate()
        {
            var dic = new Dictionary<string, object>();
            dic["url"] = "";
            dic["topcolor"] = "#FF0000";
            var data = new Dictionary<string, object> 
                { 
                    { "first", new Dictionary<string, object> { { "value", 1 }}},
                    { "keyword1", new Dictionary<string, object> { { "value", 2 }}},
                    { "keyword2", new Dictionary<string, object> { { "value", 3 } }}, 
                     { "keyword3", new Dictionary<string, object> { { "value", "4"} }}, 
                    { "remark", new Dictionary<string, object> { { "value", "你好"} }}, 
                };

            dic["data"] = data;
            var json = JsonSerializer.SerializeToJson(data);
            string js = "{{0}}";
            js = string.Format(js,"1");
            string st = "{\"first\":{\"value\":\"房屋已完成签约\"},\"keyword1\":{\"value\":\"{0}\"},\"keyword2\":{\"value\":\"签约\"},\"keyword3\":{\"value\":\"房屋核价\"},\"keyword4\":{\"value\":\"{1}\"},\"keyword5\":{\"value\":\"{2}\"},\"remark\":{\"value\":\"尊敬的客户，您好，已为您预约房屋核价时间，请您携带相关材料，至交易中心现场办理,{3} \r\n<a href=\"http://test.erpweixin.pinganhaofang.com/Home/Index?path=/System/Parter/WeiXin/Templates/CaseStatus/HA2/Buyer/material/\">查看详情   ></a>\"}}";
            var d = JsonSerializer.DeserializeToDictionary(st);
            //var result = ServiceDepository.ParterChannelService.SendTemplateMessage(10223, "CaseStatus", dic);
            //Assert.IsTrue(result.Success);
        }

        [TestMethod]
        public void CreateMenus()
        {
            var bt =new  List<Dictionary<string, object>>();

            var sbt1 = new List<Dictionary<string, object>>();
            var sbt11 = new Dictionary<string, object> { { "type", "click" }, { "key", 0 }, { "name", "交易进度" }, };
            //var sbt12 = new Dictionary<string, object> { { "type", "click" }, { "key", 1 }, { "name", "交易记录" }, };
            var sbt12 = new Dictionary<string, object> { { "type", "view" }, { "url", "http://test.erpweixin.pinganhaofang.com/Transaction/Index?isMine=0" }, { "name", "交易记录" }, };
            sbt1.Add(sbt11);
            sbt1.Add(sbt12);
            var bt1 = new Dictionary<string, object> { { "name", "我的" }, { "sub_button", sbt1 } };

          

            var bt2 = new Dictionary<string, object> { { "name", "我要" }, {"type","click"}, {"key","ok"}};
            //var sbt2 = new List<Dictionary<string, object>>();
           

            
            var sbt3 = new List<Dictionary<string, object>>();
            var sbt31 = new Dictionary<string, object> { { "type", "view" }, { "url", "http://test.erpweixin.pinganhaofang.com/LoanCalculation/Index" }, { "name", "房贷/税费计算器" }, };
            var sbt32 = new Dictionary<string, object> { { "type", "view" }, { "url", "http://test.erpweixin.pinganhaofang.com/LimitsPurchases/ShanghPeopleSearch" }, { "name", "购房资格查询" }, };
            //var sbt31 = new Dictionary<string, object> { { "type", "click" }, { "key", "ok" }, { "name", "房贷计算器" }, };
            //var sbt32 = new Dictionary<string, object> { { "type", "click" }, { "key", "ok" }, { "name", "限购查询" }, };
            var sbt33 = new Dictionary<string, object> { { "type", "click" }, { "key", 2 }, { "name", "联系我们" }, };
            sbt3.Add(sbt31);
            sbt3.Add(sbt32);
            sbt3.Add(sbt33);
            var bt3 = new Dictionary<string, object> { { "name", "好房助手" }, { "sub_button", sbt3 } };


            bt.Add(bt1);
            //bt.Add(bt2);
            bt.Add(bt3);
            var data = new Dictionary<string, object> { { "button", bt } };
            var json = JsonSerializer.SerializeToJson(data);
            var result = ServiceDepository.ParterChannelService.CreateMenus(bt);
            Assert.IsTrue(result.Success);
        }


        [TestMethod]
        public void CreateAgentCompanyMenus()
        {
            var bt = new List<Dictionary<string, object>>();


            var sbt1 = new List<Dictionary<string, object>>();
            var sbt11 = new Dictionary<string, object> { { "type", "view" }, { "url", "http://test.erpweixin.pinganhaofang.com/Case/CaseAgentCompanyIndex?route=enterprise" }, { "name", "交易" }, };
            var sbt12 = new Dictionary<string, object> { { "type", "view" }, { "url", "http://test.erpweixin.pinganhaofang.com/Bill/Index?route=enterprise" }, { "name", "账单" }, };
            sbt1.Add(sbt11);
            sbt1.Add(sbt12);
            var bt1 = new Dictionary<string, object> { { "name", "业务" }, { "sub_button", sbt1 } };



            var bt2 = new Dictionary<string, object> { { "name", "员工" }, { "type", "view" }, { "url", "http://test.erpweixin.pinganhaofang.com/Staff/List?route=enterprise" }, };


            var sbt3 = new List<Dictionary<string, object>>();
            //var sbt35 = new Dictionary<string, object> { { "type", "click" }, { "key", "ok" }, { "name", "优惠活动" }, };
            //var sbt31 = new Dictionary<string, object> { { "type", "click" }, { "key", "ok" }, { "name", "产调申请" }, };
            var sbt32 = new Dictionary<string, object> { { "type", "view" }, { "url", "http://test.erpweixin.pinganhaofang.com/LoanCalculation/Index?route=enterprise" }, { "name", "房贷/税费计算器" }, };
            var sbt33 = new Dictionary<string, object> { { "type", "view" }, { "url", "http://test.erpweixin.pinganhaofang.com/LimitsPurchases/ShanghPeopleSearch?route=enterprise" }, { "name", "购房资格查询" }, };
            var sbt34 = new Dictionary<string, object> { { "type", "click" }, { "key", 2 }, { "name", "联系我们" }, };
            //sbt3.Add(sbt35);
            //sbt3.Add(sbt31);
            sbt3.Add(sbt32);
            sbt3.Add(sbt33);
            sbt3.Add(sbt34);
       
            var bt3 = new Dictionary<string, object> { { "name", "好房助手" }, { "sub_button", sbt3 } };


            bt.Add(bt1);
            bt.Add(bt2);
            bt.Add(bt3);
            var result = ServiceDepository.ParterChannelService.CreateMenus(bt, AgentType.AgentCompany);
            Assert.IsTrue(result.Success);
        }


        [TestMethod]
        public void CreateQYMenus()
        {
            var bt = new List<Dictionary<string, object>>();

            var bt1 = new Dictionary<string, object> { { "type", "view" }, { "url", "http://test.erpweixin.pinganhaofang.com/Case/CaseStaffIndex?route=enterprise" }, { "name", "我的交易" }, };
          

            var bt2 = new Dictionary<string, object> { { "name", "我要办理" }, { "type", "click" }, { "key", "ok" } };


            var sbt3 = new List<Dictionary<string, object>>();
            //var sbt35 = new Dictionary<string, object> { { "type", "click" }, { "key", "ok" }, { "name", "优惠活动" }, };
            //var sbt31 = new Dictionary<string, object> { { "type", "click" }, { "key", "ok" }, { "name", "产调申请" }, };
            var sbt32 = new Dictionary<string, object> { { "type", "view" }, { "url", "http://test.erpweixin.pinganhaofang.com/LoanCalculation/Index?route=enterprise" }, { "name", "房贷/税费计算器" }, };
            var sbt33 = new Dictionary<string, object> { { "type", "view" }, { "url", "http://test.erpweixin.pinganhaofang.com/LimitsPurchases/ShanghPeopleSearch?route=enterprise" }, { "name", "购房资格查询" }, };
            var sbt34 = new Dictionary<string, object> { { "type", "click" }, { "key", 2 }, { "name", "联系我们" }, };
            //sbt3.Add(sbt35);
            //sbt3.Add(sbt31);
            sbt3.Add(sbt32);
            sbt3.Add(sbt33);
            sbt3.Add(sbt34);
          
            var bt3 = new Dictionary<string, object> { { "name", "好房助手" }, { "sub_button", sbt3 } };


            bt.Add(bt1);
            //bt.Add(bt2);
            bt.Add(bt3);
            var result = ServiceDepository.ParterChannelService.CreateMenus(bt, AgentType.Agency);
            Assert.IsTrue(result.Success);
        }


        [TestMethod]
        public void DeleteMenus()
        {
            var result = ServiceDepository.ParterChannelService.DeleteMenus();
            Assert.IsTrue(result.Success);
        }

        [TestMethod]
        public void GetKeys()
        {

            var result = ServiceDepository.ConfigService.GetWXKeys("");
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void ValidateAuth2CallResult()
        {
            var result = ServiceDepository.ParterChannelService.ValidateAuth2CallResult("cG56qBcwkA8xvZScLDii2n1cULa9GZZL4ViG2s2992Gim8a_K6SCMEFWLztTI1sUhv0ufCbvsfrLSvazq9H_CA", 4);
        }
        

        [TestMethod]
        public void GetCaseProcesses()
        {
            var partnerUsers = ServiceDepository.ParterUserInfoDataAccess.Get("oWjycs45rEa3jMEVfe5eeiI3e4NA");
            Assert.IsTrue(partnerUsers!=null);
        }

        [TestMethod]
        public void CreateDepartment()
        {
            int totalCount;
            var ds = ServiceDepository.AgentService.GetAgentCompanyList(null, null, 1, 1500, out totalCount);
            foreach (var entity in ds)
            {
                ServiceDepository.ParterChannelService.CreateDepartment(string.Format("{0}({1})", entity.CompanyName, entity.SysNo), (int)AgentType.AgentCompany, 9, (int)entity.SysNo);
            }
        }

        [TestMethod]
        public void UpdateDepartment()
        {
            var result = ServiceDepository.ParterChannelService.UpdateDepartment("2", "第三方", "1", 0);
            Assert.IsTrue(result.Success);
        }

        [TestMethod]
        public void DeleteDepartment()
        {
            int totalCount;
            var ds = ServiceDepository.AgentService.GetAgentCompanyList(null,null,1,1000, out totalCount);
            foreach (var d in ds)
            {
                ServiceDepository.ParterChannelService.DeleteDepartment(d.SysNo.ToString());
            }
            //var result = ServiceDepository.ParterChannelService.DeleteDepartment("2");
            //Assert.IsTrue(result.Success);
        }

        [TestMethod]
        public void DeleteUser()
        {
            var result = ServiceDepository.ParterChannelService.DeleteUser("C155542160305498");
            Assert.IsTrue(result.Success);
        }

        [TestMethod]
        public void DeleteAgentStaff()
        {
            var result = ServiceDepository.UserService.DeleteAgentStaff(13588, "A150229140529258");
            Assert.IsTrue(result.Success);
        }
        
        [TestMethod]
        public void UpdateUser()
        {
            var result = ServiceDepository.ParterChannelService.UpdateUser("fuyongf", departmentId:new string []{"1","5","4"});
            Assert.IsTrue(result.Success);
        }

        [TestMethod]
        public void CreateUser()
        {
            var dl = new List<string> { "2"};
            var result = ServiceDepository.ParterChannelService.CreateUser("fuyf", "fuyf", null, "1017582996@qq.com", null, null, dl);
            Assert.IsTrue(result.Success);
        }
        
        [TestMethod]
        public void GetDepartment()
        {
            var result = ServiceDepository.ParterChannelService.GetDepartments();
            Assert.IsTrue(result.Count!=0);
        }

        [TestMethod]
        public void GetUser()
        {
            var result = ServiceDepository.ParterChannelService.GetUser("100011");
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetUserByDepartment()
        {
            var result = ServiceDepository.ParterChannelService.GetUserByDepartment("5", true);
            Assert.IsNotNull(result);
        }


        [TestMethod]
        public void InviteUserSubscribe()
        {
            var result = ServiceDepository.ParterChannelService.InviteUserSubscribe("fuyf");
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void SendMessage()
        {
            var result = ServiceDepository.ParterChannelService.SendTextMessage(AgentType.Agency, "你好 <a href=\"tel:4008681111 \">4008681111</a>");
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void SendMessages()
        {
            var result = ServiceDepository.ParterChannelService.SendTextMessage(AgentType.AgentCompany, "你好 <a href=\"tel:4008681111 \">4008681111</a>", ReceiverType.ToDepartment, "13392");
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void SendCMessages()
        {
            var message = ServiceDepository.SettingService.GetSettingItemValueByPath("/System/Parter/WeiXin/ReplyMessages/Text/BindUserSuccessMessage");
            var text = string.Format(message,"\r");
            var result = ServiceDepository.ParterChannelService.SendCustomerTextMessage("o2xHasoy9jbdbVTZHmhHYSWGlqq4", text);
            Assert.IsNotNull(result);
        }

        [TestMethod]    
        public void UploadMaterial()
        {
            using (FileStream fs = new FileStream(@"C:\Users\Public\Pictures\Sample Pictures\12.png", System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                var result = ServiceDepository.ParterChannelService.UploadMaterial("Test", "12.png", fs);
            }
            
        }

        [TestMethod]
        public void SendImageMessage()
        {
            var url = "http://test.erpweixin.pinganhaofang.com:81/Staff/List?route=enterprise";
            var uri = new Uri(url);
            var ru = string.Format("{0}://{1}{2}", uri.Scheme, uri.Host, uri.PathAndQuery);
            var result = ServiceDepository.ParterChannelService.SendImageMessage(AgentType.Agency, "1aKh8TwPsCi_NXZlxUor1TfYUzmoFlWeYQtzY_Nu8bqYwlcpRea2P7kH3WUeaKg8j5T03hu-xARumyW_7qa7LsA");
        }


        [TestMethod]
        public void SendSmsMessage()
        {
            var result = ServiceDepository.SmsService.Send(SmsCategory.YY_HJ, "13916232461", new Dictionary<string, object> { { "Test", "test" }, { "datetime", DateTime.Now.ToString("MM月dd日HH：mm") }, { "number", "1234567890" }, { "phone", "400-820-1100转2" } });
            Assert.IsTrue(result.Success);
        }

        [TestMethod]
        public void GetRandom()
        {
            var result = HTB.DevFx.Utils.RandomHelper.GetRandomString(4, "0123456789");
            
        }


        [TestMethod]
        public void RelieveBind()
        {
            var result = ServiceDepository.ParterChannelService.RelieveBind(61408,1);
        }


        [TestMethod]
        public void BindUser()
        {
            var captcha = ServiceDepository.CaptchaService.RequetCaptcha("oWjycs020s2J_gArvMtjOPW0ZfNQ", BusinessCacheType.BindIdentity);
            var result = ServiceDepository.ParterChannelService.CheckUser("o2xHasoy9jbdbVTZHmhHYSWGlqq4", "819192819191919190", CertificateType.HKMacaoIdCard, captcha.Value, captcha.Key);
            var userId =  result.OtherData["UserId"].ToString();
            result = ServiceDepository.SmsService.SendBindCaptcha(result.OtherData["Mobile"].ToString());

            result = ServiceDepository.ParterChannelService.BindUser("o2xHasoy9jbdbVTZHmhHYSWGlqq4", userId, captcha.Value, result.OtherData["token"].ToString());
        }
    }
}
