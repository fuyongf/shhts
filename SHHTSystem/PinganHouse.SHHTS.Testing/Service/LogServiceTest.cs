﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.ServiceImpl;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Testing.Service
{
    [TestClass]
    public class LogServiceTest
    {
        [TestMethod]
        public void SetLogInfo()
        {
        }

        [TestMethod]
        public void GetLog()
        {
        }
        CacheStorages cacheService = new CacheStorages();
        [TestMethod]
        public void SetCache()
        {
            var t = new Test { Name="1", Age=23};
            cacheService.Upset<Test>("123",t);
            cacheService["1234", TimeSpan.FromHours(1)] = t;
        }

        [TestMethod]
        public void GetCache()
        {
            var r = cacheService.Get<Test>("123");
            var r1 = cacheService["1234"] as Test;
        }

        [TestMethod]
        public void RemoveCache()
        {
            var r = cacheService.Remove("123");
        }

        class Test 
        {
            public string Name { get; set; }

            public int Age { get; set; }
        }
    }
}
