﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Testing.Service
{
    [TestClass]
    public class PropertySurveyServiceTest
    {
       [TestMethod]
      public void AddPropertySurvey()
      {
          var dto = new PropertySurveyDto {

              TenementContract = DateTime.Now.Ticks.ToString(),
              ProposerSysNo = 13541,
              Remark="ok",
              SurveyDate=DateTime.Now,
              CreateUserSysNo = 13541,          
              IsAttachment=true,
              IsGuaranty=false,
              
          };
           for(int i=0;i<5;i++)
           {
               //dto.TenementContract = DateTime.Now.Ticks.ToString();
               var result = ServiceDepository.PropertySurveyService.AddPropertySurvey(dto, null, null);
           }
         
         // Assert.IsTrue(result.Success);
      }

       [TestMethod]
       public void GetPropertySurvey()
       {
           var result = ServiceDepository.PropertySurveyService.GetPropertySurvey("宝-20140123");
           Assert.IsNotNull(result);
       }

       int totalCount;
       [TestMethod]
       public void GetPaginatedList()
       {
           var result = ServiceDepository.PropertySurveyService.GetPaginatedList(1,10, out totalCount,startDate:DateTime.Now.AddDays(-5),endDate:DateTime.Now.AddDays(-5));
           foreach (var v in result[0].Item2)
           {
               Assert.IsTrue(result[0].Item1.TenementContract == v.TenementContract);
                   
           }
           Assert.IsNotNull(result);
       }

       [TestMethod]
       public void GetPaginatedList1()
       {
           var data = new Dictionary<string, object> { { "CaseId", "06199853056076" } };
           var result = ServiceDepository.PropertySurveyService.GetPaginatedList(1, 10, out totalCount,  data: data);
           foreach (var v in result[0].Item2)
           {
               Assert.IsTrue(result[0].Item1.TenementContract == v.TenementContract);

           }
           Assert.IsNotNull(result);
       }


       [TestMethod]
       public void GetPaginatedList2()
       {
           var result = ServiceDepository.PropertySurveyService.GetPaginatedList(1, 10, out totalCount, tenementAddress: "sh");
           Assert.IsNotNull(result);
       }
    }
}
