﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.DataTransferObjects;

namespace PinganHouse.SHHTS.Testing.Service
{
    [TestClass]
    public class UserServiceTest
    {
        [TestMethod]
        public void Login()
        {
            var result = ServiceDepository.UserService.Login("login1", "123456");
        }

        [TestMethod]
        public void CreateCustomer()
        {

        }

        [TestMethod]
        public void CreateTpUser()
        {
            var result = ServiceDepository.UserService.CreateThirdpartyUser(new ThirdpartyUser
            {
                CertificateType = CertificateType.IdCard,
                IdentityNo = "130825198808223713",
                RealName = "mamama",
                TpName = "234"
            }, "mawg01", "123456");
        }

        [TestMethod]
        public void GetTpUser()
        {
            var result = ServiceDepository.UserService.GetThirdpartyUser(27867);
        }

        [TestMethod]
        public void GetCustomerByCase()
        {
            var result = ServiceDepository.UserService.GetCustomerByCase("06202872954890");
            var result1 = ServiceDepository.CustomerDataAccess.GetWithAddress(17771);

            var result2 = ServiceDepository.UserService.GetUserIdentityNoByCustomerSysNos(new long[] { 17771, 17772 });
        }

        [TestMethod]
        public void LoginByFinger()
        {
            var result = ServiceDepository.UserService.LoginByFingerprint("RlBNGATYcXcABHuyo+CCynKs4YNY863mAZrzq2oEmnO17gZYVLgHg+mUrOUHeTW2DIIYtqThiBnWcXaFiLat6QaKFnUSCBh3sAgH6VdyfoT517cahPY4qu0IZlpuEofWemkChnacZwIEprxlcIrWnakOCNkepOYKuT9mCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
        }

        [TestMethod]
        public void GetAgentUser()
        {
            var result = ServiceDepository.UserService.GetAgentSatff("A150229140529258");
        }
        [TestMethod]
        public void DeleteAgentStaff()
        {
            var result = ServiceDepository.UserService.DeleteAgentStaff(49149, "A15101509021075");
        }

        [TestMethod]
        public void GetByStaffCardNo()
        {
            var result = ServiceDepository.UserService.GetByStaffCardNo("BCFA44CB");
        }

        [TestMethod]
        public void InsertUserExtend()
        {
            var entity = new UserExend { SysNo = 49149, IsSendSms=true};
            var result = ServiceDepository.UserExendDataAccess.Insert(entity);
        }

        [TestMethod]
        public void UpdateUserExtend()
        {
            var result = ServiceDepository.UserService.UpdateSendSmsFlag(49149, true, 49149);
        }


        [TestMethod]
        public void GetCustomerDetails()
        {
            var result = ServiceDepository.UserService.GetCustomerDetails("18521351913");
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetCustomerDetailsByIdentityNo()
        {
            var result = ServiceDepository.UserService.GetCustomerDetailsByIdentityNo("819192819191919190");
            Assert.IsNotNull(result);
        }
    }
}
