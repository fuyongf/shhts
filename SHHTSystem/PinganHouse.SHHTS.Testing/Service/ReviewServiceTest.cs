﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Testing.Service
{
    [TestClass]
    public class ReviewServiceTest
    {
        [TestMethod]
        public void CreateReview()
        {
            ReviewInfo review = new ReviewInfo 
            { 
                ReviewCategory= Enumerations.ReviewCategory.Receipt,
                Reviewer="110,120",
                ReviewMode = "Timecard,Fingerprint",
            };
            Assert.IsTrue(review.ReviewModes[0]== Enumerations.ReviewMode.Timecard);
            Assert.IsTrue(review.ReviewModes.FindIndex(0,r=>r==ReviewMode.Fingerprint)==1);
        }

        [TestMethod]
        public void ReviewRule()
        {
            var reviewDto = new ReviewDto();
            var result = ServiceDepository.ReviewService.ReviewRule(ReviewCategory.Receipt, "123455", 10143, out reviewDto);
            var sysno = reviewDto.SysNo;
            result = ServiceDepository.ReviewService.ValidateReviewRule(reviewDto.SysNo, 10224, ReviewMode.Timecard, out reviewDto);
            Assert.IsFalse(result.Success);
            Assert.IsNull(reviewDto);
            result = ServiceDepository.ReviewService.ValidateReviewRule(sysno, 10222, ReviewMode.Fingerprint, out reviewDto);
            Assert.IsFalse(result.Success);
            Assert.IsNull(reviewDto);
            result = ServiceDepository.ReviewService.ValidateReviewRule(sysno, 10222, ReviewMode.Timecard, out reviewDto);
            Assert.IsTrue(result.Success);
            Assert.IsNotNull(reviewDto);
            result = ServiceDepository.ReviewService.ValidateReviewRule(sysno, 10189, ReviewMode.Fingerprint, out reviewDto);
            Assert.IsFalse(result.Success);
            Assert.IsNull(reviewDto);
            result = ServiceDepository.ReviewService.ValidateReviewRule(sysno, 10222, ReviewMode.Fingerprint, out reviewDto);
            Assert.IsTrue(result.Success);
            Assert.IsNull(reviewDto);
        }


        [TestMethod]
        public void ValidateReviewRule()
        {
            var reviewDto = new ReviewDto();
            var result = ServiceDepository.ReviewService.ValidateReviewRule(47279, 10224, ReviewMode.Timecard, out reviewDto);
            Assert.IsFalse(result.Success);
            Assert.IsNull(reviewDto);
            result = ServiceDepository.ReviewService.ValidateReviewRule(47279, 10222, ReviewMode.Fingerprint, out reviewDto);
            Assert.IsFalse(result.Success);
            Assert.IsNull(reviewDto);
            result = ServiceDepository.ReviewService.ValidateReviewRule(47279, 10222, ReviewMode.Timecard, out reviewDto);
            Assert.IsTrue(result.Success);
            Assert.IsNotNull(reviewDto);
            result = ServiceDepository.ReviewService.ValidateReviewRule(47279, 10189, ReviewMode.Fingerprint, out reviewDto);
            Assert.IsFalse(result.Success);
            Assert.IsNull(reviewDto);
            result = ServiceDepository.ReviewService.ValidateReviewRule(47279, 10222, ReviewMode.Fingerprint, out reviewDto);
            Assert.IsTrue(result.Success);
            Assert.IsNull(reviewDto);
        }

    }
}
