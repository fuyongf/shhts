﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace PinganHouse.SHHTS.Testing.Service
{
    [TestClass]
    public class AttachmentServiceTest
    {
        [TestMethod]
        public void GetAttachments()
        {
            var result = ServiceDepository.AttachmentService.GetAttachments(new AttachmentQuery { AssociationNo = "19529", PageCount = 10, PageIndex = 1 });
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void SetAttachment()
        {
            var attachment = new AttachmentContent {
                AssociationNo = "10189",
                AttachmentType = Enumerations.AttachmentType.户口本,
                FileName = "beautiful",
                Remark = "beautiful",
            };
            string path = @"C:\Users\Public\Pictures\Sample Pictures\hope.jpg";
            using (FileStream stream = File.OpenRead(path))
            {
                attachment.AttachmentContentData = stream;
                var result = ServiceDepository.AttachmentService.AddAttachment(attachment);
                Assert.IsNotNull(result);
                Assert.IsTrue(result.Success);
            }
        }

        [TestMethod]
        public void GetAttachment()
        {
            var result = ServiceDepository.AttachmentService.DownloadAttachment(new AttachmentQuery { FileId = "552c797bf855f51df44b6c0d" });
            Assert.IsNotNull(result);
        }


        [TestMethod]
        public void DeleteAttachment()
        {
            var result = ServiceDepository.AttachmentService.DeleteAttachment(new DeleteAttachmentElement { FileId = "559c9d09f855f50220cde315", ModifyUserSysNo = 1010101 });
            Assert.IsTrue(result.Success);
        }
    }
}
