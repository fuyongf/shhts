﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.DataTransferObjects.External;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.Testing.Service
{
    [TestClass]
    public class PaymentServiceTest
    {
        [TestMethod]
        public void Collection()
        {
            //var result = ServiceDepository.TradeService.SubmitCollection(AccountType.Buyer, "06199853056034", 100, 20, OrderBizType.Collection, PaymentChannel.POS, RandomHelper.GetNumber(999999, 6), "1001", "6222010001109876543", "ICBC", "Test", "工商银行", DateTime.Now, "TEST", 0);
            //var result = ServiceDepository.TradeService.SubmitCollectionTrade("2015032503031076", PaymentChannel.POS, RandomHelper.GetNumber(99999999, 8), "1001", 70, "6222010001109876543", "ICBC", "Test", "工商银行", DateTime.Now, "TEST", 0);
            //Assert.IsTrue(result.Success);
        }

        [TestMethod]
        public void Cancel()
        {
            var result = ServiceDepository.TradeService.CancelCollectionTrade("2015032009015353", 0);
            Assert.IsTrue(result.Success);
        }

        [TestMethod]
        public void UpdatePropertyAddr()
        {
            var result = ServiceDepository.PartnerService.SendData("property", new Dictionary<string, object> 
            {
                {"sCaseID","06199853056055"},
                {"sPropertyAddr","杨树浦路1001弄7号702"}
            });
            Assert.IsTrue(result.Success);
        }

        [TestMethod]
        public void AddReceipt()
        {
            var result = ServiceDepository.PartnerService.SendData("receipt", new Dictionary<string, object> 
            {
                {"sBusinessID","2015031906356392"},
                {"sReceiptNum","100112233"}
            });
            Assert.IsTrue(result.Success);
        }

        [TestMethod]
        public void GetBanks()
        {
            var result = ServiceDepository.ExternalService.GetBanks();
            //var result = ServiceDepository.PartnerService.SendData<FsBankInfo>("bank", null);
        }

        [TestMethod]
        public void Payment()
        {
            var result = ServiceDepository.ExternalService.SubmitPayment(
                "06199853056034", "2015032312345678", "钦州北路93弄12号301",
                100, "ICBC", "6222021001013719502", "马文国", OrderBizType.HosingFund.GetDescription());
            Assert.IsTrue(result.Success);
        }

        [TestMethod]
        public void Payment1()
        {
            for (int i = 0; i < 10; i++)
            {
                var result = ServiceDepository.TradeService.SubmitEntrustPay(AccountType.Buyer, "06199853056034",
                    AccountType.Seller, "06199853056034",
                    OrderBizType.HosingFund, 10, "6222021001013719502", "ICBC", "Mango", "工商银行", "test", 0);
                Assert.IsTrue(result.Success);
            }

        }

        [TestMethod]
        public void BankCardAuth()
        {
            var result = ServiceDepository.ExternalService.AuthenticateBankCard("mango", "130825198808223713", "13916232461", "6222021001013719503", "ICBC");
            Assert.IsTrue(result.Success);
        }

        [TestMethod]
        public void SMS()
        {
            var result = ServiceDepository.ExternalService.SendSMS("HF_ESFSYSTEM_REG", "", new Dictionary<string, object> { { "code", 123456 } });
            Assert.IsTrue(result.Success);
        }

        [TestMethod]
        public void ConfirmCollection()
        {
            var confirm = ServiceDepository.TradeService.ReconciliationCallback("2015080455902904", ReconciliationStatus.Success, 100, null);
            Assert.IsTrue(confirm.Success);
        }

        [TestMethod]
        public void CallCollection()
        {
            //var cancel = ServiceDepository.TradeService.CancelCollectionOrder("2015032501477514", 0);
            //Assert.IsTrue(cancel.Success);
        }

        [TestMethod]
        public void CallTrade()
        {
            var cancel = ServiceDepository.TradeService.CancelCollectionTrade("2015032506505475", 0);
            Assert.IsTrue(cancel.Success);
        }

        [TestMethod]
        public void AddMissCollection()
        {
            var result = ServiceDepository.TradeService.AddMissCollectionTrade(new MissCollectionTrade
                    {
                        SerialNo = RandomHelper.GetNumber(999999, 6),
                        Amount = 100,
                        PaymentDate = DateTime.Now,
                        PaymentAccount = "622202111101111111"
                    });
            Assert.IsTrue(result.Success);
        }

        [TestMethod]
        public void MissCollection()
        {
            var result = ServiceDepository.TradeService.SubmitTransferCollection(17547, AccountType.Buyer, "06199853056034", OrderBizType.Collection, 0);
            Assert.IsTrue(result.Success);
        }

        [TestMethod]
        public void ConfirmPayment()
        {
            var res = ServiceDepository.TradeService.PaymentCallback("2015052522989966", PaymentStatus.Success, "123", DateTime.Now, null, null);
        }

        [TestMethod]
        public void GetTradeAmount()
        {
            var result = ServiceDepository.AccountService.GetCaseTradeAmount("06202872954890");
        }

        [TestMethod]
        public void GetReceiptTrades()
        {
            var result = ServiceDepository.TradeService.GetPrintReceiptTradesByOrder("2015040902878920");
        }

        [TestMethod]
        public void GetTrades()
        {
            int totalCount = 0;
            var result = ServiceDepository.TradeService.GetTrades(1, 100, out totalCount, ReceptionCenter.PuDong, null, null, null, null, null, null, null);
        }

        [TestMethod]
        public void GetTradeByCase()
        {
            int totalCount = 0;
            var result = ServiceDepository.TradeService.GetTradeByCase(1, 100, out totalCount, "06202872954890", AccountType.Seller, null);
        }

        [TestMethod]
        public void GetAccountBill()
        {
            var beforeMonth = DateTime.Now.AddDays(-(DateTime.Now.Day));
            var days = DateTime.DaysInMonth(beforeMonth.Year, beforeMonth.Month) - 1;
            var startDate = beforeMonth.AddDays(-days).Date;
            var endDate = DateTime.Now.AddDays(-(DateTime.Now.Day - 1)).Date;
            var result = ServiceDepository.AccountService.GetAccountBill(AccountType.Agency, "13392", startDate, endDate);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ConfirmPaidOff()
        {
            var result = ServiceDepository.TradeService.PaidOffCallback("2015060323635249", PaidOffStatus.Failed, "123", "", null);
        }

        [TestMethod]
        public void DeductSchedule()
        {
            //var result = ServiceDepository.TradeService.SubmitDeductSchedule(AccountType.Seller, "06218979739335", OrderBizType.EFQServiceCharge, 10000, 0, new List<OrderBizType> { OrderBizType.HosingFund, OrderBizType.Taxes });

            //var result = ServiceDepository.TradeService.DeleteDeductSchedule(53461, 0, 1);
            var result = ServiceDepository.DeductScheduleDataAccess.GetByAccount(42659,null,null,null);
            int totalCount = 0;
            var result1= ServiceDepository.TradeService.GetDeductSchedules(1, 100, out  totalCount, ReceptionCenter.BaoShan, null, null, null, null);
        }

        [TestMethod]
        public void ChangeBalanceType()
        {
            var result = ServiceDepository.TradeService.ChangeBalanceType(AccountType.Seller, "06236763016090", OrderBizType.HosingFund, OrderBizType.AgencyFee, 100, "test", 0);

        }
    }
}
