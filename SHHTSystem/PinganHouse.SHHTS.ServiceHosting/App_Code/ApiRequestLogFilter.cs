﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;
using System.Web.Http.Filters;
using System.Threading.Tasks;
using System.Web.Http.Controllers;

using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;

namespace PinganHouse.SHHTS.ServiceHosting
{
    public class ApiRequestLogAttribute : ActionFilterAttribute
    {
        const string REQUEST_LOG_FORMATTER = "{0}  {1}\r\n{2}";

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            base.OnActionExecuting(actionContext);

            Log.API(string.Format(REQUEST_LOG_FORMATTER, IPUtils.GetWebClientIp(), HttpContextBase.Request.RawUrl, HttpContextBase.Request.Form));
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            base.OnActionExecuted(actionExecutedContext);
        }


        protected HttpContextBase HttpContextBase
        {
            get
            {
                return new HttpContextWrapper(HttpContext.Current);
            }
        }
    }
}