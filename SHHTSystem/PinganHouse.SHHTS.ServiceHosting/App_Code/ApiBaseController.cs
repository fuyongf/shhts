﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;

namespace PinganHouse.SHHTS.ServiceHosting.Controllers
{
    public class ApiBaseController : ApiController
    {
        protected HttpContextBase HttpContextBase
        {
            get
            {
                return new HttpContextWrapper(HttpContext.Current);
            }
        }
    }
}