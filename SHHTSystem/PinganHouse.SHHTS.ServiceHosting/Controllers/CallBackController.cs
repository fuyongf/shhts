﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;

using PinganHouse.SHHTS.ServiceHosting.Models.Api;

namespace PinganHouse.SHHTS.ServiceHosting.Controllers
{
    public class CallBackController : ApiBaseController
    {
        /// <summary>
        /// 接收对账结果
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HttpPost]
        [ApiRequestLog]
        [Route("api/fs/callback/ret/collection")]
        public FsOperationResult Collection()
        {
            try
            {
                FsOperationResult result = new FsOperationResult { code = "1", memo = "成功" };
                var validate = ServiceDepository.PartnerService.ValidateCallResult("collection", HttpContextBase, null);
                if (!validate.Success)
                {
                    result.code = validate.ResultNo.ToString();
                    result.memo = validate.ResultMessage;
                    Log.Error(string.Format("{0},{1}\r\n{2}\r\n{3}", result.code, result.memo, HttpContextBase.Request.RawUrl, HttpContextBase.Request.Form));
                }
                else
                {
                    var status = (ReconciliationStatus)int.Parse(validate.OtherData["ReconcileStatus"].ToString());
                    OperationResult confirm = null;
                    if (status != ReconciliationStatus.StatusError)
                    {
                        confirm = ServiceDepository.TradeService.ReconciliationCallback(
                           validate.OtherData["TradeNo"].ToString(), status, (decimal)validate.OtherData["Amount"], null);
                    }
                    else
                    {
                        confirm = ServiceDepository.TradeService.AddMissCollectionTrade(new MissCollectionTrade
                        {
                            SerialNo = validate.OtherData["SerialNo"].ToString(),
                            Amount = (decimal)validate.OtherData["Amount"],
                            Channel = PaymentChannel.POS,
                            PaymentDate = (DateTime)validate.OtherData["TradeDate"]
                        });
                    }
                    if (!confirm.Success)
                    {
                        result.code = "-98";
                        result.memo = "内部错误";
                        Log.Error(string.Format("对账结果确认异常,{0},{1},{2},{3}", validate.OtherData["TradeNo"].ToString(), validate.OtherData["SerialNo"].ToString(), confirm.ResultNo, confirm.ResultMessage));
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new FsOperationResult { code = "-99", memo = "内部错误" };
            }
        }

        /// <summary>
        /// 接收掉单收款信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HttpPost]
        [ApiRequestLog]
        [Route("api/fs/callback/push/collection")]
        public FsOperationResult PushCollection()
        {
            try
            {
                FsOperationResult result = new FsOperationResult { code = "1", memo = "成功" };
                var validate = ServiceDepository.PartnerService.ValidateCallResult("miss", HttpContextBase, null);
                if (!validate.Success)
                {
                    result.code = validate.ResultNo.ToString();
                    result.memo = validate.ResultMessage;
                    Log.Error(string.Format("{0},{1}\r\n{2}\r\n{3}", result.code, result.memo, HttpContextBase.Request.RawUrl, HttpContextBase.Request.Form));
                }
                else
                {
                    var confirm = ServiceDepository.TradeService.AddMissCollectionTrade(new MissCollectionTrade
                    {
                        SerialNo = validate.OtherData["SerialNo"].ToString(),
                        Amount = (decimal)validate.OtherData["Amount"],
                        PaymentDate = (DateTime)validate.OtherData["PaymentTime"],
                        PaymentAccount = validate.OtherData["AccountNo"].ToString(),
                        BankName = validate.OtherData["BankId"] != null ? validate.OtherData["BankId"].ToString() : null,
                        PayerName = validate.OtherData["AccountName"] != null ? validate.OtherData["AccountName"].ToString() : null,
                        Channel = (PaymentChannel)(int)validate.OtherData["PaymentType"]
                    });
                    if (!confirm.Success)
                    {
                        result.code = "-98";
                        result.memo = "内部错误";
                        Log.Error(string.Format("转账数据接收异常{0},{1},{2}", validate.OtherData["SerialNo"].ToString(), confirm.ResultNo, confirm.ResultMessage));
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new FsOperationResult { code = "-99", memo = "内部错误" };
            }
        }

        /// <summary>
        /// 接收代付结果
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HttpPost]
        [ApiRequestLog]
        [Route("api/fs/callback/ret/pay")]
        public FsOperationResult Pay()
        {
            try
            {
                FsOperationResult result = new FsOperationResult { code = "1", memo = "成功" };
                var validate = ServiceDepository.PartnerService.ValidateCallResult("pay", HttpContextBase, null);
                if (!validate.Success)
                {
                    result.code = validate.ResultNo.ToString();
                    result.memo = validate.ResultMessage;
                    Log.Error(string.Format("{0},{1}\r\n{2}\r\n{3}", result.code, result.memo, HttpContextBase.Request.RawUrl, HttpContextBase.Request.Form));
                }
                else
                {
                    PaymentStatus payStatus = (PaymentStatus)(int.Parse(validate.OtherData["PayStatus"].ToString()) - 1);

                    var confirm = ServiceDepository.TradeService.PaymentCallback(
                        validate.OtherData["TradeNo"].ToString(),
                        payStatus, validate.OtherData["FinanceNum"].ToString(),
                        validate.OtherData.ContainsKey("PaymentTime") ? (DateTime)validate.OtherData["PaymentTime"] : new Nullable<DateTime>(),
                        validate.OtherData.ContainsKey("RejectDesc") ? validate.OtherData["RejectDesc"].ToString() : null, null);
                    if (!confirm.Success)
                    {
                        result.code = "-98";
                        result.memo = "内部错误";
                        Log.Error(string.Format("支付回调确认异常{0},{1},{2}", validate.OtherData["TradeNo"].ToString(), confirm.ResultNo, confirm.ResultMessage));
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new FsOperationResult { code = "-99", memo = "内部错误" };
            }
        }

        /// <summary>
        /// 接收销帐结果
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HttpPost]
        [ApiRequestLog]
        [Route("api/fs/callback/ret/paidoff")]
        public FsOperationResult PaidOff()
        {
            try
            {
                FsOperationResult result = new FsOperationResult { code = "1", memo = "成功" };
                var validate = ServiceDepository.PartnerService.ValidateCallResult("paidoff", HttpContextBase, null);
                if (!validate.Success)
                {
                    result.code = validate.ResultNo.ToString();
                    result.memo = validate.ResultMessage;
                    Log.Error(string.Format("{0},{1}\r\n{2}\r\n{3}", result.code, result.memo, HttpContextBase.Request.RawUrl, HttpContextBase.Request.Form));
                }
                else
                {
                    PaidOffStatus payStatus = (PaidOffStatus)(int.Parse(validate.OtherData["AuditStatus"].ToString()));

                    var confirm = ServiceDepository.TradeService.PaidOffCallback(
                        validate.OtherData["TradeNo"].ToString(),
                        payStatus, validate.OtherData["FinanceNum"].ToString(),
                        validate.OtherData.ContainsKey("RejectDesc") ? validate.OtherData["RejectDesc"].ToString() : null, null);
                    if (!confirm.Success)
                    {
                        result.code = "-98";
                        result.memo = "内部错误";
                        Log.Error(string.Format("销帐回调确认异常{0},{1},{2}", validate.OtherData["TradeNo"].ToString(), confirm.ResultNo, confirm.ResultMessage));
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new FsOperationResult { code = "-99", memo = "内部错误" };
            }
        }
    }
}
