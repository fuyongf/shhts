﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Newtonsoft.Json;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Entities;

namespace PinganHouse.SHHTS.ServiceHosting.Areas.Admin.Controllers
{
    public class SettingController : Controller
    {
        // GET: Admin/Setting
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Tree() 
        {
            List<Hashtable> nodes = new List<Hashtable>();

            Hashtable root = new Hashtable();
            root.Add("id", string.Empty);
            root.Add("name", "系统配置");
            root.Add("rootId", "Root");
            root.Add("isParent", true);
            root.Add("icon", Url.Content("~/Content/zTree/images/diy/1_close.png"));
            nodes.Add(root);

            ViewBag.TreeRootJson = JsonConvert.SerializeObject(nodes); //SerializeHelper.SerializeObjectToJSON(nodes);
            return View();
        }

        public ActionResult Separator() 
        {
            return View();
        }

        public ActionResult Node()
        {
            if (string.IsNullOrEmpty(Request["id"]))
                return View();
            return View(ServiceDepository.SettingService.GetSettingItem(Request["id"]));
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Node(string id, string name, string desc, string path, string value)
        {
            if (string.IsNullOrEmpty(id))
            {
                ViewBag.ResultMessage = "非法操作";
                return View();
            }
            Setting setting = ServiceDepository.SettingService.GetSettingItem(id);
            if (setting == null)
            {
                ViewBag.ResultMessage = "保存失败，配置信息不存在";
                return View();
            }
            if (ServiceDepository.SettingService.ModifySetting(id, value, desc))
            {
                ViewBag.ResultMessage = "保存成功";
            }
            else
            {
                ViewBag.ResultMessage = "保存失败";
            }
            setting.Value = value;
            setting.Description = desc;
            return View(setting);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Add(string parentId, string name, string desc, string path, string value)
        {
            Setting setting = null;
            try
            {
                setting = ServiceDepository.SettingService.CreateSettingItem(name, parentId, value, desc, null);
                if (setting != null)
                {
                    ViewBag.ResultMessage = "添加成功";
                }
                else
                {
                    ViewBag.ResultMessage = "添加失败";
                }
            }
            catch
            {
                ViewBag.ResultMessage = "添加失败";
            }
            return View("Node", setting);
        }

        [HttpPost]
        public ActionResult Remove(string id)
        {
            Setting setting = ServiceDepository.SettingService.GetSettingItem(id);
            if (setting == null)
            {
                ViewBag.ResultMessage = "删除失败，配置不存在";
                return View("Node");
            }
            var children = ServiceDepository.SettingService.GetSettingItems(id);
            if (children != null && children.Count() > 0)
            {
                ViewBag.ResultMessage = "删除失败，此参数含有子节点，请先删除子节点";
                return View("Node", setting);
            }
            if (!ServiceDepository.SettingService.DeleteSettingItem(id))
            {
                ViewBag.ResultMessage = "删除失败，系统错误";
                return View("Node", setting);
            }
            ViewBag.ResultMessage = "删除成功";
            return View("Node");
        }

        [HttpPost]
        public string LoadChildren(string id, string rootId)
        {
            return GetTreeNodeJson(rootId, id);
        }

        /// <summary>
        /// 根据根节点ID和父节点ID获取子节点
        /// </summary>
        /// <param name="rootId"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        private string GetTreeNodeJson(string rootId, string parentId = null)
        {
            List<Hashtable> nodes = new List<Hashtable>();

            #region 内置节点
            Hashtable addNode = new Hashtable();
            addNode.Add("name", "[增加节点]");
            addNode.Add("id", "-1");
            addNode.Add("url", string.Format(Url.Content("~/Admin/Setting/Node?action=add&rootId={0}&pid={1}"), rootId, parentId));
            addNode.Add("target", "treeFrame");
            addNode.Add("rootId", rootId);
            addNode.Add("isParent", false);
            addNode.Add("icon", Url.Content("~/Content/zTree/images/diy/2.png"));
            nodes.Add(addNode);

            Hashtable refreshNode = new Hashtable();
            refreshNode.Add("name", "[刷新]");
            refreshNode.Add("id", "-2");
            refreshNode.Add("treeType", rootId);
            refreshNode.Add("isParent", false);
            refreshNode.Add("icon", Url.Content("~/Content/zTree/images/diy/9.png"));
            nodes.Add(refreshNode);
            #endregion

            var children = ServiceDepository.SettingService.GetSettingItems(parentId);
            if (children != null)
            {
                foreach (var item in children)
                {
                    Hashtable node = new Hashtable();
                    node.Add("id", item.Id);
                    node.Add("name", item.Name);
                    node.Add("url", string.Format(Url.Content("~/Admin/Setting/Node?action=edit&rootId={0}&id={1}"), rootId, item.Id));
                    node.Add("target", "treeFrame");
                    node.Add("rootId", rootId);
                    node.Add("isParent", true);
                    node.Add("icon",Url.Content("~/Content/zTree/images/diy/6.png"));
                    nodes.Add(node);
                }
            }
            return JsonConvert.SerializeObject(nodes);
        }
    }
}