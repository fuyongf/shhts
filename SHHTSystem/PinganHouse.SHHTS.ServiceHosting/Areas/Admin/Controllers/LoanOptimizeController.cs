﻿using PinganHouse.SHHTS.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PinganHouse.SHHTS.ServiceHosting.Areas.Admin.Controllers
{
    public class LoanOptimizeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Interests = ServiceDepository.SettingService.GetSettingItemsByPath("/Biz/LoanOptimize/Interest");
            ViewBag.BankPolicies = ServiceDepository.SettingService.GetSettingItemsByPath("/Biz/LoanOptimize/BankPolicy");
            return View();
        }

        /// <summary>
        /// 更新利率表
        /// </summary>
        /// <param name="name">节点名称.</param>
        /// <param name="index">序号</param>
        /// <param name="value">修改的值</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateInterest(string name, int index, string value)
        {
            try
            {
                var ino = ServiceDepository.SettingService.GetSettingItemByPath(string.Format("/Biz/LoanOptimize/Interest/{0}/{1}", name, index));
                if (ino == null)
                    return Json(new { error = 1, msg = "不存在此参数配置" });

                ServiceDepository.SettingService.ModifySetting(ino.Id, value, ino.Description);

                return Json(new { error = 0 });

            }
            catch (Exception ex)
            {
                return Json(new { error = 1, msg = ex.Message });
            }
        }

        /// <summary>
        /// 更新银行策略
        /// </summary>
        /// <param name="name">节点名称.</param>
        /// <param name="index">序号</param>
        /// <param name="value">修改的值</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateBankPolicy(string name, int index, string value)
        {
            try
            {
                var ino = ServiceDepository.SettingService.GetSettingItemByPath(string.Format("/Biz/LoanOptimize/BankPolicy/{0}/{1}", name, index));
                if (ino == null)
                    return Json(new { error = 1, msg = "不存在此参数配置" });

                ServiceDepository.SettingService.ModifySetting(ino.Id, value, ino.Description);

                return Json(new { error = 0 });

            }
            catch (Exception ex)
            {
                return Json(new { error = 1, msg = ex.Message });
            }
        }
    }
}