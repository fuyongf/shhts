﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.ServiceHosting.Areas.Test.Controllers
{
    public class FsController : Controller
    {
        // GET: Test/Default
        public ActionResult Case()
        {
            int totalCount = 0;

            PinganHouse.SHHTS.DataTransferObjects.Filters.CaseFilter cf = new DataTransferObjects.Filters.CaseFilter();
            cf.ReceptionCenters.Add(ReceptionCenter.PuDong);
            cf.Statuses = new List<CaseStatus>(new CaseStatus[] {CaseStatus.DK1 , CaseStatus.DK10 , CaseStatus.DK2 , CaseStatus.DK3 , CaseStatus.DK4 , CaseStatus.DK5 ,
                CaseStatus.DK6 , CaseStatus.DK7 , CaseStatus.DK8 , CaseStatus.DK9 , CaseStatus.HA1 , CaseStatus.HA2 ,
                CaseStatus.HA3 , CaseStatus.HA4 , CaseStatus.HA5 , CaseStatus.HA6 , CaseStatus.HD1 , CaseStatus.HD2 ,
                CaseStatus.HD3 , CaseStatus.HD4 , CaseStatus.HD5 , CaseStatus.HD6 , CaseStatus.JY1 , CaseStatus.JY10 ,
                CaseStatus.JY11 , CaseStatus.JY12 , CaseStatus.JY2 , CaseStatus.JY3 , CaseStatus.JY4 , CaseStatus.JY5 , CaseStatus.JY6 ,
                CaseStatus.JY7 , CaseStatus.JY8 , CaseStatus.JY9 , CaseStatus.YJ1 , CaseStatus.ZB1 , CaseStatus.ZB10 , CaseStatus.ZB11 ,
                CaseStatus.ZB12 , CaseStatus.ZB2 , CaseStatus.ZB3 , CaseStatus.ZB4 , CaseStatus.ZB5 , CaseStatus.ZB6 ,
                CaseStatus.ZB7 , CaseStatus.ZB8 , CaseStatus.ZB9});


            var cases = ServiceDepository.CaseService.GetPaginatedList(cf, 1, 1000, out totalCount);

            return View(cases);
        }

        public ActionResult Order()
        {
            string caseId = RouteData.Values["id"].ToString();
            var co = ServiceDepository.TradeService.GetOrders(AccountType.Buyer, caseId, null, null, null, null);
            //var eo = ServiceDepository.TradeService.GetOrders(AccountType.Seller, caseId, OrderType.Payment, null, null, null);
            //var co1 = ServiceDepository.TradeService.GetOrders(AccountType.Seller, caseId, OrderType.Collection, null, null, null);
            //var eo1 = ServiceDepository.TradeService.GetOrders(AccountType.Buyer, caseId, OrderType.Payment, null, null, null);
            var list = new List<Order>();
            if (co != null)
                list.AddRange(co);
            //if (eo != null)
            //    list.AddRange(eo);
            //if (co1 != null)
            //    list.AddRange(co1);
            //if (eo1 != null)
            //    list.AddRange(eo1);
            return View(list);
        }

        public ActionResult Trade()
        {
            if (!string.IsNullOrWhiteSpace(Request.QueryString["TradeNo"]))
            {
                var result = ServiceDepository.TradeService.CancelCollectionTrade(Request.QueryString["TradeNo"], 19255);
                if (result.Success)
                {
                    ViewBag.Message = "撤销成功";
                }
                else
                {
                    ViewBag.Message = result.ResultMessage;
                }
            }
            return View(ServiceDepository.TradeService.GetTradeByOrder(RouteData.Values["id"].ToString()));
        }

        public ActionResult Miss()
        {
            return View(ServiceDepository.TradeService.GetMissCollectionTrade(null, null, null));
        }

        [HttpGet]
        public ActionResult CO()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CO(string caseId, int accountType, decimal amount, string remark)
        {
            try
            {
                var result = ServiceDepository.TradeService.SubmitCollection((AccountType)accountType, caseId,
                    amount, OrderBizType.Collection, PaymentChannel.POS, remark, 19255);
                if (result.Success)
                    ViewBag.Message = "订单提交成功";
                else
                    ViewBag.Message = result.ResultMessage;
            }
            catch (Exception e)
            {
                ViewBag.Message = "系统错误";
            }
            return View();
        }

        [HttpGet]
        public ActionResult EO()
        {
            return View();
        }

        [HttpPost]
        public ActionResult EO(string caseId, int accountType, int receiveType, int orderType, decimal amount, string remark, string accountNo, string bankCode, string accountName, string bankName)
        {
            try
            {
                if (accountType == receiveType)
                {
                    ViewBag.Message = "收款方和付款方不能相同";
                }
                else
                {
                    var result = ServiceDepository.TradeService.SubmitEntrustPay((AccountType)accountType, caseId,
                        (AccountType)receiveType, caseId,
                        (OrderBizType)orderType,
                        amount, accountNo, bankCode, accountName, bankName, remark, 19255);
                    if (result.Success)
                    {
                        string orderNo = result.OtherData["OrderNo"].ToString();
                        result = ServiceDepository.TradeService.PaymentOrderAudit(ReceptionCenter.PuDong, orderNo, AuditStatus.Audited, "测试审核", 42772, "测试审核员");
                        if (result.Success)
                        {
                            result = ServiceDepository.TradeService.PaymentOrderAudit(ReceptionCenter.PuDong, orderNo, AuditStatus.ReAudited, "测试审核", 42772, "测试审核员");
                            if (result.Success)
                                ViewBag.Message = "订单创建成功";
                            else
                                ViewBag.Message = "订单审核失败";
                        }
                        else
                            ViewBag.Message = "订单审核失败";
                    }
                    else
                        ViewBag.Message = result.ResultMessage;
                }
            }
            catch (Exception e)
            {
                ViewBag.Message = "系统错误";
            }
            return View();
        }

        [HttpGet]
        public ActionResult PO()
        {
            return View();
        }

        [HttpPost]
        public ActionResult PO(string caseId, int accountType, int receiveType, int orderType, decimal amount, string remark, string accountNo, string bankCode, string accountName, string bankName)
        {
            try
            {
                if (accountType == receiveType)
                {
                    ViewBag.Message = "收款方和付款方不能相同";
                }
                else
                {
                    var result = ServiceDepository.TradeService.SubmitPaidOff((AccountType)accountType, caseId,
                        (AccountType)receiveType, caseId,
                        (OrderBizType)orderType,
                        amount, accountNo, bankCode, accountName, bankName, remark, 19255);
                    if (result.Success)
                    {
                        string orderNo = result.OtherData["OrderNo"].ToString();
                        result = ServiceDepository.TradeService.PaymentOrderAudit(ReceptionCenter.PuDong, orderNo, AuditStatus.Audited, "测试审核", 42772, "测试审核员");
                        if (result.Success)
                        {
                            result = ServiceDepository.TradeService.PaymentOrderAudit(ReceptionCenter.PuDong, orderNo, AuditStatus.ReAudited, "测试审核", 42772, "测试审核员");
                            if (result.Success)
                                ViewBag.Message = "订单创建成功";
                            else
                                ViewBag.Message = "订单审核失败";
                        }
                        else
                            ViewBag.Message = "订单审核失败";
                    }
                    else
                        ViewBag.Message = result.ResultMessage;
                }
            }
            catch (Exception e)
            {
                ViewBag.Message = "系统错误";
            }
            return View();
        }

        [HttpGet]
        public ActionResult ST()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ST(string orderNo, decimal amount, string serialNo, string terminalNo, string bankAccountNo, string bankCode, string accountName, string paymentDate, string remark)
        {
            var result = ServiceDepository.TradeService.SubmitPosTrade(orderNo, serialNo, terminalNo, amount, bankAccountNo, bankCode, accountName, string.Empty, DateTime.Parse(paymentDate), remark, 19255);
            if (result.Success)
                ViewBag.Message = "提交成功";
            else
                ViewBag.Message = result.ResultMessage;
            return View();
        }

        [HttpGet]
        public ActionResult RE()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RE(long sysNo, int accountType, string caseId, string bankAccount, string accountName, string terminaNo)
        {
            OperationResult result = null;
            var miss = ServiceDepository.MissCollectionTradeDataAccess.Get(sysNo);
            if (miss.Channel == PaymentChannel.POS)
            {
                result = ServiceDepository.TradeService.SubmitCollection((AccountType)accountType, caseId, miss.Amount, miss.Amount, OrderBizType.Collection, PaymentChannel.POS, miss.SerialNo, terminaNo, bankAccount, string.Empty, accountName, string.Empty, miss.PaymentDate.Value, "补单", 19255);
                if (result.Success)
                {
                    string tradeNo = result.OtherData["TradeNo"].ToString();
                    result = ServiceDepository.TradeService.ConfirmMissCollectionTrade(sysNo, tradeNo, 19255);
                }
            }
            else
            {
                result = ServiceDepository.TradeService.SubmitTransferCollection(sysNo, (AccountType)accountType, caseId, OrderBizType.Collection, 19255);
            }
            if (result.Success)
                ViewBag.Message = "补单成功";
            else
                ViewBag.Message = result.ResultMessage;
            return View();
        }
    }
}