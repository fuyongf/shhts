﻿using System.Web.Mvc;

namespace PinganHouse.SHHTS.ServiceHosting.Areas.Test
{
    public class TestAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Test";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Test_default",
                "Test/{controller}/{action}/{id}",
                new { action = "case", id = UrlParameter.Optional }
            );
        }
    }
}