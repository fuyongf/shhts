﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinganHouse.SHHTS.ServiceHosting.Models.Api
{
    public class FsOperationResult
    {
        private string _code = string.Empty;
        public string code
        {
            get { return _code; }
            set { _code = value; }
        }

        private string _memo = string.Empty;
        public string memo
        {
            get { return _memo; }
            set { _memo = value; }
        }

        private IList _data = new List<object>();
        public IList data
        {
            get { return _data; }
            set { _data = value; }
        }
    }
}