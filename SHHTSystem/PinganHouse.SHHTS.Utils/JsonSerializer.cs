﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;

namespace PinganHouse.SHHTS.Utils
{
    public static class JsonSerializer
    {
        public static string SerializeToJson(object input) 
        {
            return JsonConvert.SerializeObject(input);
        }

        public static IDictionary<string, object> DeserializeToDictionary(string json) 
        {
            return JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        }

        public static T DeserializeToObject<T>(string json) 
        {
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}
