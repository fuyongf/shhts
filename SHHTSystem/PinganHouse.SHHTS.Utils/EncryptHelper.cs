﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace PinganHouse.SHHTS.Utils
{
    public static  class EncryptHelper
    {
        readonly static byte[] _sKey64 = { 10, 6, 91, 78, 78, 18, 108, 32 };
        readonly static byte[] _sIv64 = { 55, 53, 243, 79, 60, 78, 99, 103 };
        /// <summary>
        /// 与ASP兼容的MD5加密算法
        /// </summary>
        /// <param name="s">字符集(utf-8, gb2312)</param>
        /// <returns></returns>
        public static string GetMD5(string s, string charset= "utf-8")
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] t = md5.ComputeHash(Encoding.GetEncoding(charset).GetBytes(s));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < t.Length; i++)
            {
                sb.Append(t[i].ToString("x2"));
            }
            return sb.ToString();
        }

        public static string GetSHA1(string val)
        {
            if (string.IsNullOrWhiteSpace(val))
                throw new ArgumentException("val is nul");
            SHA1 sha = new SHA1CryptoServiceProvider();
            byte[] dataToHash = new ASCIIEncoding().GetBytes(val);
            byte[] dataHashed = sha.ComputeHash(dataToHash);
            return  BitConverter.ToString(dataHashed).Replace("-", "").ToLower();
        }
        public static string GetBase64(string s, string charset = "utf-8") 
        {
            if (string.IsNullOrEmpty(s))
                return null;
            return Convert.ToBase64String(Encoding.GetEncoding(charset).GetBytes(s));
        }

        public static string Base64ToString(string s, string charset = "utf-8") 
        {
            if (string.IsNullOrEmpty(s))
                return null;
            return Encoding.GetEncoding(charset).GetString(Convert.FromBase64String(s));
        }

        /// <summary>
        /// 进行DES加密。
        /// </summary>
        /// <param name="source">要加密的字符串。</param>
        /// <param name="key">密钥，且必须为8位。</param>
        /// <returns>以Base64格式返回的加密字符串。</returns>
        public static string Encrypt(string source, string key)
        {
            using (var des = new DESCryptoServiceProvider())
            {
                byte[] inputByteArray = Encoding.UTF8.GetBytes(source);
                des.Key = Encoding.ASCII.GetBytes(key);
                des.IV = Encoding.ASCII.GetBytes(key);
                var ms = new System.IO.MemoryStream();
                using (var cs = new CryptoStream(ms, des.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(inputByteArray, 0, inputByteArray.Length);
                    cs.FlushFinalBlock();
                    cs.Close();
                }
                string str = Convert.ToBase64String(ms.ToArray());
                ms.Close();
                return str;
            }
        }

        /// <summary>
        /// 进行DES解密。
        /// </summary>
        /// <param name="source">要解密的以Base64</param>
        /// <param name="key">密钥，且必须为8位。</param>
        /// <returns>已解密的字符串。</returns>
        public static string Decrypt(string source, string key)
        {
            byte[] inputByteArray = Convert.FromBase64String(source);
            using (var des = new DESCryptoServiceProvider())
            {
                des.Key = Encoding.ASCII.GetBytes(key);
                des.IV = Encoding.ASCII.GetBytes(key);
                var ms = new System.IO.MemoryStream();
                using (var cs = new CryptoStream(ms, des.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(inputByteArray, 0, inputByteArray.Length);
                    cs.FlushFinalBlock();
                    cs.Close();
                }
                string str = Encoding.UTF8.GetString(ms.ToArray());
                ms.Close();
                return str;
            }
        }

        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="source">需解密的数据</param>
        /// <returns></returns>
        public static string Decrypt(string source)
        {
            return Decrypt(source, _sKey64, _sIv64);
        }

        /// <summary>
        /// 进行加密
        /// </summary>
        /// <param name="source">需加密的数据</param>
        /// <returns></returns>
        public static string Encrypt(string source)
        {
            return Encrypt(source, _sKey64, _sIv64);
        }

        /// <summary>
        /// 进行DES解密。
        /// </summary>
        /// <param name="source">要解密的以Base64</param>
        /// <param name="sKey64">密钥，且必须为8位。</param>
        /// <param name="sIv64">密钥，且必须为8位。</param>
        /// <returns>已解密的字符串。</returns>
        public static string Decrypt(string source, byte[] sKey64, byte[] sIv64)
        {
            byte[] inputByteArray = Convert.FromBase64String(source);
            using (var des = new DESCryptoServiceProvider())
            {
                des.Key = sKey64;
                des.IV = sIv64;
                var ms = new System.IO.MemoryStream();
                using (var cs = new CryptoStream(ms, des.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(inputByteArray, 0, inputByteArray.Length);
                    cs.FlushFinalBlock();
                    cs.Close();
                }
                string str = Encoding.UTF8.GetString(ms.ToArray());
                ms.Close();
                return str;
            }
        }

        /// <summary>
        /// 进行DES加密。
        /// </summary>
        /// <param name="source">要加密的字符串。</param>
        /// <param name="sKey64">密钥，且必须为8位。</param>
        /// <param name="sIv64">密钥，且必须为8位。</param>
        /// <returns>以Base64格式返回的加密字符串。</returns>
        public static string Encrypt(string source, byte[] sKey64, byte[] sIv64)
        {
            using (var des = new DESCryptoServiceProvider())
            {
                byte[] inputByteArray = Encoding.UTF8.GetBytes(source);
                des.Key = sKey64;
                des.IV = sIv64;
                var ms = new System.IO.MemoryStream();
                using (var cs = new CryptoStream(ms, des.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(inputByteArray, 0, inputByteArray.Length);
                    cs.FlushFinalBlock();
                    cs.Close();
                }
                
                string str = Convert.ToBase64String(ms.ToArray());
                ms.Close();
                return str;
            }
        }
 
    }
}
