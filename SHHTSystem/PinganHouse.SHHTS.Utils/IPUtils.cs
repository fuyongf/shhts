﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.Web;

namespace PinganHouse.SHHTS.Utils
{
    public sealed class IPUtils
    {
        private static string m_ServerIp;

        public static bool CurIsWebApp()
        {
            if (HttpContext.Current != null && HttpContext.Current.Request != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string HttpRequestAppPath()
        {
            if (HttpContext.Current != null && HttpContext.Current.Request != null)
            {
                return HttpContext.Current.Request.ApplicationPath;
            }

            return string.Empty;
        }

        public static string HttpRequestMappedAppPath()
        {
            if (HttpContext.Current != null && HttpContext.Current.Request != null)
            {
                return HttpContext.Current.Server.MapPath(HttpContext.Current.Request.ApplicationPath);
            }

            return string.Empty;
        }

        public static string HttpRequestIp()
        {
            if (HttpContext.Current != null && HttpContext.Current.Request != null)
            {
                return HttpContext.Current.Request.ServerVariables["LOCAL_ADDR"].ToString();
            }
            return string.Empty;
        }

        public static string GetServerCurrentIp()
        {
            if (m_ServerIp == null)
            {
                string host = Dns.GetHostName();
                IPAddress[] serverIps = Dns.GetHostEntry(host).AddressList;

                foreach (IPAddress ipAddr in serverIps)
                {
                    if (ipAddr.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) //只限ipv4
                    {
                        m_ServerIp = ipAddr.ToString();
                        break;
                    }
                }
            }
            return m_ServerIp;
        }

        public static string GetWebClientIp()
        {
            if (HttpContext.Current == null
                || HttpContext.Current.Request == null
                || HttpContext.Current.Request.ServerVariables == null)
                return "";

            string ip = null;

            var headers = new[] { "Cdn-Src-Ip", "x-forwarded-for", "Proxy-Client-IP", "WL-Proxy-Client-IP", "HTTP_CLIENT_IP", "HTTP_X_FORWARDED_FOR" };
            foreach (var header in headers)
            {
                ip = HttpContext.Current.Request.Headers[header];
                if (!string.IsNullOrEmpty(ip) && string.Compare(ip, "unknown", true) != 0)
                {
                    break;
                }
            }

            if (string.IsNullOrEmpty(ip))
            {
                ip = HttpContext.Current.Request.UserHostAddress;
            }
            return ip;

            /** 
            关键就在HTTP_X_FORWARDED_FOR
            使用不同种类代理服务器，上面的信息会有所不同：

            一、没有使用代理服务器的情况：
            REMOTE_ADDR = 您的 IP
            HTTP_VIA = 没数值或不显示
            HTTP_X_FORWARDED_FOR = 没数值或不显示

            二、使用透明代理服务器的情况：Transparent Proxies
            REMOTE_ADDR = 代理服务器 IP 
            HTTP_VIA = 代理服务器 IP
            HTTP_X_FORWARDED_FOR = 您的真实 IP
            这类代理服务器还是将您的信息转发给您的访问对象，无法达到隐藏真实身份的目的。

            三、使用普通匿名代理服务器的情况：Anonymous Proxies
            REMOTE_ADDR = 代理服务器 IP 
            HTTP_VIA = 代理服务器 IP
            HTTP_X_FORWARDED_FOR = 代理服务器 IP
            隐藏了您的真实IP，但是向访问对象透露了您是使用代理服务器访问他们的。

            四、使用欺骗性代理服务器的情况：Distorting Proxies
            REMOTE_ADDR = 代理服务器 IP 
            HTTP_VIA = 代理服务器 IP 
            HTTP_X_FORWARDED_FOR = 随机的 IP
            告诉了访问对象您使用了代理服务器，但编造了一个虚假的随机IP代替您的真实IP欺骗它。

            五、使用高匿名代理服务器的情况：High Anonymity Proxies (Elite proxies)
            REMOTE_ADDR = 代理服务器 IP
            HTTP_VIA = 没数值或不显示
            HTTP_X_FORWARDED_FOR = 没数值或不显示 
            **/
        }

        public static int FreeTcpPort()
        {
            TcpListener l = new TcpListener(IPAddress.Loopback, 0);
            l.Start();
            int port = ((IPEndPoint)l.LocalEndpoint).Port;
            l.Stop();
            return port;
        }

        public static int FreeTcpPort(int begin, int end, List<int> usedPorts = null)
        {
            List<int> portUsed = PortInUsed();

            for (int i = begin; i <= end; i++)
            {
                if (!portUsed.Contains(i) && usedPorts != null && !usedPorts.Contains(i))
                    return i;
            }
            throw new Exception(string.Format("Don't have any available port between {0} and {1}", begin, end));
        }
        /// <summary>
        /// 获取操作系统已用的端口号
        /// </summary>
        /// <returns></returns>
        private static List<int> PortInUsed()
        {
            //获取本地计算机的网络连接和通信统计数据的信息
            IPGlobalProperties ipGlobalProperties = IPGlobalProperties.GetIPGlobalProperties();

            //返回本地计算机上的所有Tcp监听程序
            IPEndPoint[] ipsTCP = ipGlobalProperties.GetActiveTcpListeners();

            //返回本地计算机上的所有UDP监听程序
            IPEndPoint[] ipsUDP = ipGlobalProperties.GetActiveUdpListeners();

            //返回本地计算机上的Internet协议版本4(IPV4 传输控制协议(TCP)连接的信息。
            TcpConnectionInformation[] tcpConnInfoArray = ipGlobalProperties.GetActiveTcpConnections();

            List<int> allPorts = new List<int>();
            foreach (IPEndPoint ep in ipsTCP) allPorts.Add(ep.Port);
            foreach (IPEndPoint ep in ipsUDP) allPorts.Add(ep.Port);
            foreach (TcpConnectionInformation conn in tcpConnInfoArray) allPorts.Add(conn.LocalEndPoint.Port);

            return allPorts;
        }

        public static string GetCurrentHost()
        {
            if (HttpContext.Current != null && HttpContext.Current.Request != null)
            {
                return HttpContext.Current.Request.Url.Host;
            }
            return string.Empty;
        }

        public static string GetDomainName(string urlString)
        {
            return new Uri(urlString).Host;
        }
    }
}
