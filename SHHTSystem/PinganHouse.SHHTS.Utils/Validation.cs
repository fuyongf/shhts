﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace PinganHouse.SHHTS.Utils
{
    public static class Validation
    {
        public static bool Email(string email) 
        {
            if (string.IsNullOrEmpty(email))
                return false;
            return new Regex(@"^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", RegexOptions.IgnoreCase).IsMatch(email);
        }

        public static bool Mobile(string mobile)
        {
            if (string.IsNullOrEmpty(mobile))
                return false;
            return new Regex(@"^(0|86|17951)?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$").IsMatch(mobile);
        }
    }
}
