﻿using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;

namespace PinganHouse.SHHTS.Utils
{
    public static class HttpUtils
    {
        /// <summary>
        /// 通讯函数
        /// </summary>
        /// <param name="url">请求Url</param>
        /// <param name="para">请求参数</param>
        /// <returns></returns>
        public static string SendPostRequest(string url, string paras, int timeout, Encoding reqEncoding, Encoding respEncoding, string contentType = null)
        {
            if (String.IsNullOrEmpty(url))
            {
                return String.Empty;
            }
            if (paras == null)
            {
                paras = String.Empty;
            }
            byte[] data = reqEncoding.GetBytes(paras);
            Stream requestStream = null;
            try
            {
                HttpWebRequest request = null;
                //如果是发送HTTPS请求  
                if (url.StartsWith("https", StringComparison.OrdinalIgnoreCase))
                {
                    ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);
                    request = WebRequest.Create(url) as HttpWebRequest;
                    request.ProtocolVersion = HttpVersion.Version10;
                }
                else
                {
                    request = WebRequest.Create(url) as HttpWebRequest;
                }  
                request.Method = "POST";
                request.Timeout = timeout;
                request.ContentType = String.IsNullOrEmpty(contentType) ? "application/x-www-form-urlencoded;" : contentType;
                request.ContentLength = data.Length;

                // Send the data.  
                if (!String.IsNullOrEmpty(paras) && data.Length > 0)
                {
                    requestStream = request.GetRequestStream();
                    requestStream.Write(data, 0, data.Length);
                    requestStream.Close();
                }
                //Response
                HttpWebResponse myResponse = (HttpWebResponse)request.GetResponse();
                StreamReader reader = new StreamReader(myResponse.GetResponseStream(), respEncoding);
                string content = reader.ReadToEnd();

                //Dispose
                if (requestStream != null)
                {
                    requestStream.Dispose();
                }
                request.Abort();
                return content;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 通讯函数
        /// </summary>
        /// <param name="url">请求Url</param>
        /// <param name="para">请求参数</param>
        /// <returns></returns>
        public static string SendPostRequest(string url, string paras, int timeout, Encoding reqEncoding, Encoding respEncoding, string contentType = null, bool allowWriteStreamBuffering = true, WebProxy proxy = null)
        {
            if (String.IsNullOrEmpty(url))
            {
                return String.Empty;
            }
            byte[] data = reqEncoding.GetBytes(paras);
            Stream requestStream = null;
            try
            {
                HttpWebRequest request = null;
                //如果是发送HTTPS请求  
                if (url.StartsWith("https", StringComparison.OrdinalIgnoreCase))
                {
                    ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);
                    request = WebRequest.Create(url) as HttpWebRequest;
                    request.ProtocolVersion = HttpVersion.Version10;
                }
                else
                {
                    request = WebRequest.Create(url) as HttpWebRequest;
                }  
                request.Method = "POST";
                request.Timeout = timeout;
                request.ContentType = String.IsNullOrEmpty(contentType) ? "application/x-www-form-urlencoded;" : contentType;
                request.ContentLength = data.Length;
                request.AllowWriteStreamBuffering = allowWriteStreamBuffering;
                request.Proxy = proxy;

                // Send the data.  
                if (!String.IsNullOrEmpty(paras) && data.Length > 0)
                {
                    requestStream = request.GetRequestStream();
                    requestStream.Write(data, 0, data.Length);
                    requestStream.Close();
                }

                //Response
                HttpWebResponse myResponse = (HttpWebResponse)request.GetResponse();
                StreamReader reader = new StreamReader(myResponse.GetResponseStream(), respEncoding);
                string content = reader.ReadToEnd();

                //Dispose
                if (requestStream != null)
                {
                    requestStream.Dispose();
                }
                request.Abort();
                return content;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 通讯函数
        /// </summary>
        /// <param name="url">请求Url</param>
        /// <returns></returns>
        public static string SendGetRequest(string url, string paras, int timeout, Encoding encode, RemoteCertificateValidationCallback callBack = null, string userAgent = null)
        {
            if (String.IsNullOrEmpty(url))
            {
                return String.Empty;
            }
            if (!url.Contains("?") && !String.IsNullOrEmpty(paras))
            {
                url = url + "?";
            }
            string requestUrl = url + paras;

            HttpWebResponse response = null;
            Stream stream = null;
            StreamReader reader = null;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestUrl);
                request.Timeout = timeout;
                request.Method = "GET";
                if (userAgent != null)
                {
                    request.UserAgent = userAgent;
                }
                if (callBack != null)
                {
                    ServicePointManager.ServerCertificateValidationCallback = callBack;
                }
                response = (HttpWebResponse)request.GetResponse();
                stream = response.GetResponseStream();
                reader = new StreamReader(stream, encode);
                StringBuilder strBuilder = new StringBuilder();
                while (-1 != reader.Peek())
                {
                    strBuilder.Append(reader.ReadLine());
                }
                return strBuilder.ToString();
            }
            catch
            {
                throw;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (stream != null)
                {
                    stream.Close();
                    stream.Dispose();
                }
                if (response != null)
                {
                    response.Close();
                }
            }
        }

        /// <summary>
        /// 解码url
        /// </summary>
        /// <param name="content">要解码的内容</param>
        /// <param name="charset">字符集</param>
        /// <param name="decodeTimes">解码次数</param>
        /// <returns></returns>
        public static string UrlDecode(string content, string charset, int decodeTimes)
        {
            string finalContent = content;

            charset = String.IsNullOrEmpty(charset) ? "gb2312" : charset;

            for (int i = 0; i < decodeTimes; i++)
            {
                finalContent = HttpUtility.UrlDecode(finalContent, Encoding.GetEncoding(charset));
            }

            return finalContent;
        }

        private static bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
        {
            return true; //总是接受  
        }  
    }
}
