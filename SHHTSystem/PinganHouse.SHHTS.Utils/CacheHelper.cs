﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;

namespace PinganHouse.SHHTS.Utils
{
    public static class CacheHelper
    {
        public static void Set(string key, object value, DateTime expires)
        {
            HttpRuntime.Cache.Insert(key, value, null, expires, TimeSpan.Zero, CacheItemPriority.Normal, null);
        }

        public static object Get(string key)
        {
            return HttpRuntime.Cache.Get(key);
        }
    }
}
