﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Utils
{
    public static class TimeStamp
    {
        private static long BaseTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1)).Ticks;
        public static int Get(DateTime? time = null)
        {
            if (time == null)
                time = DateTime.Now;
            if (time.Value.Ticks < BaseTime)
                throw new ArgumentException("时间不能小于1970年1月1日");
            return (int)((time.Value.Ticks - BaseTime) / 10000000);
        }

        public static DateTime ConvertToDateTime(long timestamp)
        {
            if (timestamp < 0)
                throw new ArgumentException("timestamp不能小于0");
            return new DateTime(BaseTime + (timestamp * 10000000));
        }
    }
}
