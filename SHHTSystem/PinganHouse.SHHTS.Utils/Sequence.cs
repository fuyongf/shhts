﻿using PinganHouse.SHHTS.Utils.Sequences;

namespace PinganHouse.SHHTS.Utils
{
    /// <summary>
    /// Sequence服务
    /// </summary>
    public class Sequence
    {
        /// <summary>
        /// Sequence领域名称
        /// </summary>
        private const string SeqName = "PinganHouse.SHHTS";

        /// <summary>
        /// 返回单个Sequence编号
        /// </summary>
        /// <returns>单个Sequence编号</returns>
        public static long Get()
        {
            return SequenceImp.Get(SeqName, 1)[0];
        }

        /// <summary>
        /// 
        /// </s返回单个Sequence编号ummary>
        /// <param name="seqName">Sequence领域名称</param>
        /// <returns></returns>
        public static long Get(string seqName)
        {
            return SequenceImp.Get(seqName, 1)[0];
        }

        /// <summary>
        /// 取系统配置SysNo
        /// </summary>
        /// <returns></returns>
        //public static long GetSettings()
        //{
        //    return SequenceImp.Get("Settings", 1)[0];
        //}

        /// <summary>
        /// 返回一组Sequence编号
        /// </summary>
        /// <param name="total">需要个数</param>
        /// <returns>Sequence数组</returns>
        public static long[] Get(int total)
        {
            var se = SequenceImp.Get(SeqName, total);
            var start = se[0];
            var len = se[1] - start + 1;
            var seqs = new long[len];
            for (long i = 0; i < len; i++)
                seqs[i] = start + i;
            return seqs;
        }

    }
}
