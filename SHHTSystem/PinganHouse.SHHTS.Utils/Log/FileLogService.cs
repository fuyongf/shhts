﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using log4net;
using log4net.Config;

namespace PinganHouse.SHHTS.Utils.Log
{
    internal class FileLogService
    {
        readonly static string Server = Environment.MachineName;

        static FileLogService() 
        {
            string logConfig = string.Format("{0}/Config/log4net.config", AppDomain.CurrentDomain.SetupInformation.ApplicationBase);
            XmlConfigurator.ConfigureAndWatch(new FileInfo(logConfig));
        }

        public void PublishByOperationLog(string msg, LogCategory logCategory)
        {
            ILog logger = LogManager.GetLogger("OperationLogger");
            if (logger.IsInfoEnabled)
            {
                logger.Info(MsgFormat(msg, logCategory.ToString()));
            }
        }

        public void PulishByApplicationLog(string msg, LogLevel logLevel)
        {
            switch (logLevel)
            {
                case LogLevel.Error:
                    this.Error(msg);
                    break;
                default:
                    this.Info(msg);
                    break;
            }
        }

        /// <summary>
        /// 记录系统跟踪调试信息
        /// </summary>
        /// <param name="msg"></param>
        private void Info(string msg)
        {
            ILog log = LogManager.GetLogger("InfoAppender");
            if (log.IsInfoEnabled)
                log.Info(msg);
        }

        /// <summary>
        /// 记录错误信息
        /// </summary>
        /// <param name="msg"></param>
        private void Error(string msg)
        {
            ILog log = LogManager.GetLogger("ErrorAppender");
            if (log.IsErrorEnabled)
            {
                log.Error(msg);
            }
        }

        /// <summary>
        /// 服务器--日记类型：日记内容
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="logType"></param>
        /// <returns></returns>
        private string MsgFormat(string msg, string logType)
        {
            return string.Format("[{0}]--#{1}#：{2}", Server, logType, msg);
        }

    }
}
