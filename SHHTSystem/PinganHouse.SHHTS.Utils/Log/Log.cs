﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using log4net.Config;

namespace PinganHouse.SHHTS.Utils.Log
{
    /// <summary>
    /// 日志类别
    /// </summary>
    public enum LogCategory
    {
        Info = 0,
        Dao = 1,
        Entity = 2,
        Service = 3,
        Client = 4,
        API =5,
        Error = 6
    }

    public enum LogLevel
    {
        All = 0,
        Debug = 1,
        Info = 2,
        Error = 3,
    }

    public class Log
    {
        readonly static FileLogService logService = new FileLogService();

        public static void Dao(string msg)
        {
            WriteLog(msg, LogCategory.Dao);
        }

        public static void Entity(string msg)
        {
            WriteLog(msg, LogCategory.Entity);
        }

        public static void Service(string msg)
        {
            WriteLog(msg, LogCategory.Service);
        }

        public static void API(string msg)
        {
            WriteLog(msg, LogCategory.API);
        }

        public static void Client(string msg)
        {
            WriteLog(msg, LogCategory.Client);
        }

        private static void WriteLog(string msg, LogCategory category) 
        {
            if (!string.IsNullOrEmpty(msg)) 
            {
                logService.PublishByOperationLog(msg, category);
            }
        }

        #region 系统日记信息
        /// <summary>
        /// 记录系统跟踪调试信息
        /// </summary>
        /// <param name="msg"></param>
        public static void Info(string msg)
        {
            logService.PulishByApplicationLog(msg, LogLevel.Info);
        }
        /// <summary>
        /// 记录错误信息
        /// </summary>
        /// <param name="msg"></param>
        public static void Error(string msg)
        {
            logService.PulishByApplicationLog(msg, LogLevel.Error); ;
        }

        /// <summary>
        /// 记录错误信息
        /// </summary>
        /// <param name="e"></param>
        public static void Error(Exception e)
        {
            string msg = string.Format("Meggase:{0}{1}Stack:{2}", e.Message, Environment.NewLine, e.StackTrace);
            Error(msg);
        }
        #endregion
    }
}
