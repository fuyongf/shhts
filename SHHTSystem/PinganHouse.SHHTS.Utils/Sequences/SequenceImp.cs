﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace PinganHouse.SHHTS.Utils.Sequences
{
    internal class SequenceImp
    {
        private static readonly string ConnString = string.Empty;

        private const string GetSeqSp = "GetSequenceSP";
        private const string SequenceConnectionName = "DefaultSequence";

        static SequenceImp()
        {
            //数据库连接、及初始化数据库连接
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings[SequenceConnectionName];
            if (connection == null)
            {
                throw new ArgumentNullException("ConnectionStrings load failed!", new ArgumentNullException(SequenceConnectionName));
            }
            ConnString = connection.ConnectionString;
        }

        public static long[] Get(string seqName, int total)
        {
            var parameters = new SqlParameter[4];
            parameters[0] = new SqlParameter("@SeqName", SqlDbType.VarChar);
            parameters[1] = new SqlParameter("@SeqNum", SqlDbType.Int);
            parameters[2] = new SqlParameter("@SeqStart", SqlDbType.BigInt) { Direction = ParameterDirection.Output };
            parameters[3] = new SqlParameter("@SeqEnd", SqlDbType.BigInt) { Direction = ParameterDirection.Output };

            parameters[0].Value = seqName;
            parameters[1].Value = total;

            SqlHelper.ExecuteNonQuery(ConnString, CommandType.StoredProcedure, GetSeqSp, parameters);

            var num = (long)parameters[2].Value;
            var num2 = (long)parameters[3].Value;
            //Log.Write(LogAction.Write, seqName, num + " " + num2);
            if ((num2 - num) == (total - 1))
            {
                return new[] { num, num2 };
            }

            string msg = string.Concat(new object[] { "多取", seqName, "返回异常,返回数量和指定数量不合:total=", total, " start=", num, " end=", num2 });
            //Log.Error(msg);
            throw new DataException(msg);
        }
    }
}
