﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Utils
{
    /// <summary>
    /// create by yuan
    /// 2015-02-11
    /// </summary>
    public class Generator
    {
        const int Version = 1;
        const int Type = 1;
        const string SeqName = "PinganHouse.Generator";
        /// <summary>
        /// 基线时间
        /// </summary>
        private static readonly DateTime BaseDateTime = new DateTime(2015, 2, 1);

        /// <summary>
        /// 权值
        /// </summary>
        private static readonly int[] Weight = new[] { 4, 7, 6, 9, 4, 6, 3, 1, 3, 6, 9, 3, 2 };



        /// <summary>
        /// 创建案件编号
        /// </summary>
        /// <param name="areaId">区域编号</param>
        /// <param name="seqNo">SEQ序号</param>
        /// <returns></returns>
        public static string GenerateCaseId(uint areaId)
        {
            //转化为二进制字符串
            var versionStr = IntToBinary(Version, 3);
            var typeStr = IntToBinary(Type, 3);
            var areaStr = IntToBinary((int)areaId, 9);
            long seqNo = Sequence.Get(SeqName);
            var seqNoStr = LongToBinary(seqNo, 16);
            //计算基线时间差()
            int daycount = DateTime.Now.Subtract(BaseDateTime).Days;

            if (daycount < 0)
                throw new SystemException("时间基线错误提示");
            //时间天数转化为二进制位
            var dayCountBinary = IntToBinary(daycount, 11);

            var caseIdStr = string.Concat(versionStr, typeStr, dayCountBinary, areaStr, seqNoStr);

            var caseIdTemp = Convert.ToUInt64(caseIdStr, 2);
            //获取检验位
            var checkNum = GetCheckDigit(caseIdTemp);
            //得到最终的案件编号
            string caseId = caseIdTemp + checkNum.ToString(CultureInfo.InvariantCulture);
            return caseId.PadLeft(14, '0');
        }

        /// <summary>
        /// 将整型转换为定长的二进制字符串
        /// </summary>
        /// <param name="temp">将转化的整数</param>
        /// <param name="count">截取位数</param>
        /// <returns>返回二进制字符串</returns>
        private static string IntToBinary(int temp, int count)
        {
            return LongToBinary(temp, count);
        }

        /// <summary>
        /// 将整型转换为定长的二进制字符串
        /// </summary>
        /// <param name="temp">将转化的整数</param>
        /// <param name="count">截取位数</param>
        /// <returns>返回二进制字符串</returns>
        private static string LongToBinary(long temp, int count)
        {
            if (count <= 0)
                throw new ArgumentOutOfRangeException("count", "参数不合法");

            var tempBit = Convert.ToString(temp, 2);

            string binary = tempBit.Length < count
                ? tempBit
                : tempBit.Substring(tempBit.Length - count, count);

            return binary.PadLeft(count, '0');
        }

        /// <summary>
        /// 计算校验位
        /// </summary>
        /// <param name="caseId">需要将要的二进制字符串</param>
        /// <returns>返回校验位</returns>
        private static ulong GetCheckDigit(ulong caseId)
        {
            var ints = new ulong[13];

            ulong temp = caseId;
            ulong total = 0;

            if (caseId == 0)
                throw new ArgumentOutOfRangeException("caseId", "参数不合法");

            for (int i = 12; i > 0 || i == 0; i--)
            {
                var pows = (ulong)Math.Pow(10, i);
                ints[i] = temp / pows;
                temp = temp % pows;
            }

            for (int i = 0; i < 13; i++)
            {
                total += ints[i] * (uint)Weight[i];
            }

            return (uint)total % 7;
        }

        /// <summary>
        /// 生成短信验证码
        /// </summary>
        /// <returns></returns>
        public static string GenerateSmsCheckCode()
        {
            return RandomHelper.GetNumber(9999999, 7);
        }
    }

}
