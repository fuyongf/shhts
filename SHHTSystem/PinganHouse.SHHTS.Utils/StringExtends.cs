﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Utils
{
    public static class StringExtends
    {
        public static bool IsEmail(this string str) 
        {
            return Validation.Email(str);
        }

        public static string TrimStart(this string str, string target)
        {
            if (string.IsNullOrEmpty(target) || string.IsNullOrWhiteSpace(str)|| target.Length > str.Length)
                return str;
            if (str.StartsWith(target, StringComparison.OrdinalIgnoreCase))
                return str.Substring(target.Length);
            return str;
        }

        public static string TrimEnd(this string str, string target)
        {
            if (string.IsNullOrEmpty(target) || string.IsNullOrWhiteSpace(str) || target.Length > str.Length)
                return str;
            if (str.EndsWith(target, StringComparison.OrdinalIgnoreCase))
                return str.Substring(0, str.Length-target.Length);
            return str;
        }

        public static  DateTime ConvertFromUnixTimeStamp(this string timestamp)
        {
            if (string.IsNullOrEmpty(timestamp))
                throw new ArgumentException("timestamp");
            return new DateTime(TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1)).Ticks + (long.Parse(timestamp) * 10000000));
        }
    }
}
