﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Utils
{
    public class RandomHelper
    {
        private static readonly Random m_Random = new Random();
        private static readonly object m_SyncRoot = new object();

        public static int GetNumber(int max)
        {
            lock (m_SyncRoot)
            {
                return m_Random.Next(max);
            }
        }

        public static string GetNumber(int max, int minLen)
        {
            int num = 0;
            lock (m_SyncRoot)
            {
                num = m_Random.Next(max);
            }
            return num.ToString().PadLeft(minLen, '0');
        }
    }
}
