﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Reflection;

using PinganHouse.SHHTS.Utils.Log;
using System.Data;

namespace PinganHouse.SHHTS.Utils
{
    public class ObjectMapper
    {
        private static Dictionary<Type, PropertyInfo[]> ObjectProperties = new Dictionary<Type, PropertyInfo[]>();
        private static object SyncRoot = new object();
        /// <summary>
        /// 对象间转换(浅复制)
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <typeparam name="TInput"></typeparam>
        /// <param name="input"></param>
        /// <returns></returns>
        public static T Copy<T>(object input) where T : new()
        {
            if (input == null)
                throw new ArgumentNullException("input", "input object can't be null.");
            Type inputType = input.GetType();
            Type resultType = typeof(T);
            PropertyInfo[] resultProperties = GetTypeProperties(resultType);
            PropertyInfo[] properties = GetTypeProperties(inputType);
            return Get<T>(resultProperties, properties, input);
   

            //T result = new T();

            //PropertyInfo[] properties = GetTypeProperties(inputType);

            //if (properties != null && properties.Length > 0)
            //{
            //    PropertyInfo[] resultProperties = GetTypeProperties(resultType);

            //    foreach (PropertyInfo p in properties)
            //    {
            //        if (!p.CanRead)
            //            continue;
            //        PropertyInfo resultProperty = resultProperties.Where(m => m.Name == p.Name).FirstOrDefault();
            //        if (resultProperty == null || !resultProperty.CanWrite)
            //            continue;
            //        try
            //        {
            //            resultProperty.SetValue(result, p.GetValue(input, null), null);
            //        }
            //        catch (Exception ex)
            //        {
            //            Log.Log.Error(string.Format("system error at copy property '{0}' when convet type '{1}' => '{2}' ,{3}", p.Name, inputType.FullName, resultType.FullName, ex.Message));
            //        }
            //    }
            //}
            //return result;
        }

        public static List<T2> Copy<T1, T2>(IList<T1> inputs) where T2 : new()
        {

            if (inputs == null || inputs.Count == 0)
                throw new ArgumentNullException("inputs", "inputs can't be null.");
            Type inputType = inputs[0].GetType();
            Type resultType = typeof(T2);
            List<T2> rl = new List<T2>();
            PropertyInfo[] properties = GetTypeProperties(inputType);
            PropertyInfo[] resultProperties = GetTypeProperties(resultType);
            foreach (var input in inputs)
            {
                if (input != null)
                    rl.Add(Get<T2>(resultProperties, properties, input));
            }
            return rl;
        }
        private static T Get<T>(PropertyInfo[] resultProperties, PropertyInfo[] inputProperties, object input)
            where T : new()
        {
            T result = new T();
            if (inputProperties != null && inputProperties.Length > 0)
            {

                foreach (PropertyInfo p in inputProperties)
                {
                    if (!p.CanRead)
                        continue;
                    PropertyInfo resultProperty = resultProperties.Where(m => m.Name == p.Name).FirstOrDefault();
                    if (resultProperty == null || !resultProperty.CanWrite)
                        continue;
                    try
                    {
                        resultProperty.SetValue(result, p.GetValue(input, null), null);
                    }
                    catch (Exception ex)
                    {
                        Log.Log.Error(string.Format("system error at copy property '{0}' when convet type '{1}' => '{2}' ,{3}", p.Name, input.GetType().FullName, typeof(T).FullName, ex.Message));
                    }
                }
            }
            return result;
        }


        /// <summary>
        /// 对象名称必须列名一致
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="input"></param>
        /// <returns></returns>
        public static T Map<T>(DataRow input) where T : new()
        {
            if (input == null)
                throw new ArgumentNullException("input", "input object can't be null.");
            Type resultType = typeof(T);
            PropertyInfo[] resultProperties = GetTypeProperties(resultType);
            return Get<T>(resultProperties, input);
        }


        public static IList<T> Map<T>(IEnumerable<DataRow> inputs)
            where T : new()
        {
            if (inputs == null || inputs.Count() == 0)
                throw new ArgumentNullException("input", "input object can't be null.");
            Type resultType = typeof(T);
            PropertyInfo[] resultProperties = GetTypeProperties(resultType);
            return Get<T>(resultProperties, inputs);
        }

        public static IDictionary<T,T1> Map<T, T1>(IEnumerable<DataRow> inputs)
            where T : new()
            where T1:new ()
        {
            if (inputs == null || inputs.Count() == 0)
                throw new ArgumentNullException("input", "input object can't be null.");
            Type resultType = typeof(T);
            Type resultType1 = typeof(T1);
            PropertyInfo[] resultTProperties = GetTypeProperties(resultType);
            PropertyInfo[] resultT1Properties = GetTypeProperties(resultType1);
            return Get<T, T1>(resultTProperties,resultT1Properties, inputs);
        }

        private static Dictionary<T, T1> Get<T, T1>(PropertyInfo[] resultTProperties, PropertyInfo[] resultT1Properties, IEnumerable<DataRow> rows)
            where T : new()
            where T1 : new()
        {
            var dic = new Dictionary<T, T1>();
            foreach (var row in rows)
            {
                dic.Add(Get<T>(resultTProperties, row), Get<T1>(resultT1Properties, row));
            }
            return dic;
        }

        private static IList<T> Get<T>(PropertyInfo[] resultProperties, IEnumerable<DataRow> rows)
            where T:new()
        {
            var rl = new List<T>();
            foreach (var row in rows)
            {
                rl.Add(Get<T>(resultProperties, row));
            }
            return rl;
        }

        private static T Get<T>(PropertyInfo[] resultProperties, DataRow row)
            where T : new()
        {
            T result = new T();
            if (resultProperties != null && resultProperties.Length > 0)
            {
                foreach (PropertyInfo p in resultProperties)
                {
                    if (!p.CanRead)
                        continue;
                    try
                    {
                        p.SetValue(result, row[p.Name], null);
                    }
                    catch (Exception ex)
                    {
                        Log.Log.Error(string.Format("system error at map property '{0}' when get value  FullName:{1} msg：{2}", p.Name, typeof(T).FullName, ex.Message));
                    }
                }
            }
            return result;
        }

        private static PropertyInfo[] GetTypeProperties(Type type) 
        {
            if (!ObjectProperties.ContainsKey(type))
            {
                lock (SyncRoot)
                {
                    if (!ObjectProperties.ContainsKey(type)) 
                    {
                        ObjectProperties.Add(type, type.GetProperties(BindingFlags.Instance | BindingFlags.Public));
                    }
                }
            }
            return ObjectProperties[type];
        }
    }
}
