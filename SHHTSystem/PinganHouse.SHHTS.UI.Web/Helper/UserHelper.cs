﻿using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using System.Collections.Generic;
using System.Web;

namespace PinganHouse.SHHTS.WebUI.Helper
{
    public static class UserHelper
    {
        public const string CurrentAgentKey = "CurrentAgent";
        public const string CurrentCustomerKey = "CurrentCustomer";
        const string WXUserKey = "WXUSer";
        /// <summary>
        /// 获取客户
        /// </summary>
        /// <returns></returns>
        public static IList<string> GetCurrentUser()
        {
            var session = HttpContext.Current.Session[CurrentCustomerKey] as string;
            if (string.IsNullOrWhiteSpace(session))
                return null;
            return session.Split(',');
        }

        public static void SetCurrentUser(string user, string key)
        {
            HttpContext.Current.Session[key] = user;
        }

        /// <summary>
        /// 获取中介
        /// </summary>
        /// <returns></returns>
        public static string GetAgentId()
        {
            return HttpContext.Current.Session[CurrentAgentKey] as string;
        }

        public static void SetWXUser(IDictionary<string, object> data)
        {
            if (data == null || data.Count == 0)
                return;
            HttpContext.Current.Session[WXUserKey] = JsonSerializer.SerializeToJson(data);
        }

        public static IDictionary<string, object> GetWXUser()
        {
            var json = HttpContext.Current.Session[WXUserKey] as string;
            if (string.IsNullOrWhiteSpace(json))
                return null;
            return JsonSerializer.DeserializeToDictionary(json);
        }

    }
}