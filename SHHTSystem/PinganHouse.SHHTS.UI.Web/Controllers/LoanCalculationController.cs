﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.WebUI.Models;

namespace PinganHouse.SHHTS.WebUI.Controllers
{
    public class LoanCalculationController : Controller
    {
        // GET: LoanCalculation
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult IndexZH()
        {
            return View();
        }

        public ActionResult Search()
        {
            try
            {
                var daiKuanLeiXing = Request.Form["hidDaiKuanLeiXing"];
                ViewData["daiKuanLeiXing"] = daiKuanLeiXing;
                double daiKuan = 0;    //贷款总额
                double.TryParse(Request.Form["txtDaiKuan"].ToString(), out daiKuan);
                ViewData["daiKuan"] = daiKuan;
                int anJieNianShu = 0;   //按揭年数
                int.TryParse(Request.Form["selAnJieNianShu"], out anJieNianShu);
                ViewData["anJieNianShu"] = anJieNianShu;
                double liLv = 0; //贷款利率
                double liLvZheKou = 0; //利率折扣
                double.TryParse(Request.Form["txtLiLv"], out liLv);
                double.TryParse(Request.Form["selLiLvZheKou"], out liLvZheKou);
                liLv = liLv * 0.01 * liLvZheKou;
                ViewData["liLv"] = liLv;
                string huanKuanFangShi = Request.Form["selHuanKuanFangShi"];
                ViewData["huanKuanFangShi"] = huanKuanFangShi;

                //贷款总额（元）
                ViewData["daiKuanYuan"] = daiKuan * 10000;
             
                if (daiKuanLeiXing != "zh")
                {

                    IList<LoanModel> moneyList = new List<LoanModel>();
                    if (huanKuanFangShi == "1")
                    {
                        var money = TotalMoney(anJieNianShu*12, daiKuan*10000, liLv);
                        moneyList = money.Item1;
                        ViewData["Interest"] = money.Item2;
                        ViewData["firstMonthMoney"] = money.Item3;
                    }
                    else
                    {
                        var money = MonthMoney(anJieNianShu*12, daiKuan*10000, liLv);
                        moneyList = money.Item1;
                        ViewData["Interest"] = money.Item2;
                        ViewData["firstMonthMoney"] = money.Item3;
                    }
                    IList<LoanModel> result = moneyList.Where(money => money.Year == 1).OrderBy(m => m.Month).ToList();


                    return View(result);
                }
                else
                {
                    double daiKuan_gjj = 0;    //贷款总额
                    double.TryParse(Request.Form["txtDaiKuan_gjj"].ToString(), out daiKuan_gjj);
                    ViewData["daiKuan_gjj"] = daiKuan_gjj;
                    int anJieNianShu_gjj = 0;   //按揭年数
                    int.TryParse(Request.Form["selAnJieNianShu_gjj"], out anJieNianShu_gjj);
                    ViewData["anJieNianShu_gjj"] = anJieNianShu_gjj;
                    double liLv_gjj = 0; //贷款利率
                    double liLvZheKou_gjj = 0; //利率折扣
                    double.TryParse(Request.Form["txtLiLv_gjj"], out liLv_gjj);
                    double.TryParse(Request.Form["selLiLvZheKou_gjj"], out liLvZheKou_gjj);
                    liLv_gjj = liLv_gjj * 0.01 * liLvZheKou_gjj;
                    ViewData["liLv_gjj"] = liLv_gjj;

                    IList<LoanModel> moneyList = new List<LoanModel>();

                    if (huanKuanFangShi == "1")
                    {
                        var money = TotalMoneyZH(anJieNianShu*12, daiKuan*10000, liLv, anJieNianShu_gjj*12, daiKuan_gjj*10000, liLv_gjj);
                        moneyList = money.Item1;
                        ViewData["Interest"] = money.Item2;
                        ViewData["firstMonthMoney"] = money.Item3;
                    }
                    else
                    {
                        var money = MonthMoneyZH(anJieNianShu*12, daiKuan*10000, liLv, anJieNianShu_gjj*12, daiKuan_gjj*10000, liLv_gjj);
                        moneyList = money.Item1;
                        ViewData["Interest"] = money.Item2;
                        ViewData["firstMonthMoney"] = money.Item3;
                    }

                    IList<LoanModel> result = moneyList.Where(money => money.Year == 1).OrderBy(m => m.Month).ToList();


                    return View(result);
                }



            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }


        public ActionResult GetLoanList()
        {
            var model = new AjaxModel();

            try
            {
                var daiKuanLeiXing = Request["daiKuanLeiXing"];
                int year = 0;
                int.TryParse(Request["year"], out year);
                double daiKuan = 0;    //贷款总额
                double.TryParse(Request["DaiKuan"], out daiKuan);
                int anJieNianShu = 0;   //按揭年数
                int.TryParse(Request["AnJieNianShu"], out anJieNianShu);
                double liLv = 0; //贷款利率
                double.TryParse(Request["LiLv"], out liLv);
                string huanKuanFangShi = Request["huanKuanFangShi"];

                if (daiKuanLeiXing != "zh")
                {
                    IList<LoanModel> moneyList = new List<LoanModel>();
                    if (huanKuanFangShi == "1")
                    {
                        moneyList = TotalMoney(anJieNianShu * 12, daiKuan * 10000, liLv).Item1;
                    }
                    else
                    {
                        moneyList = MonthMoney(anJieNianShu * 12, daiKuan * 10000, liLv).Item1;
                    }
                    IList<LoanModel> result =
                        moneyList.Where(money => money.Year == year).OrderBy(m => m.Month).ToList();

                    model.data = result;
                    model.hasError = false;
                }
                else
                {
                    double daiKuan_gjj = 0;    //贷款总额
                    double.TryParse(Request.Form["daiKuan_gjj"].ToString(), out daiKuan_gjj);
                    int anJieNianShu_gjj = 0;   //按揭年数
                    int.TryParse(Request.Form["anJieNianShu_gjj"], out anJieNianShu_gjj);
                    double liLv_gjj = 0; //贷款利率
                    double.TryParse(Request.Form["liLv_gjj"], out liLv_gjj);

                    IList<LoanModel> moneyList = new List<LoanModel>();

                    if (huanKuanFangShi == "1")
                    {
                        moneyList = TotalMoneyZH(anJieNianShu * 12, daiKuan * 10000, liLv, anJieNianShu_gjj * 12, daiKuan_gjj * 10000, liLv_gjj).Item1;
                    }
                    else
                    {
                        moneyList = MonthMoneyZH(anJieNianShu * 12, daiKuan * 10000, liLv, anJieNianShu_gjj * 12, daiKuan_gjj * 10000, liLv_gjj).Item1;
                    }
                    IList<LoanModel> result =
                        moneyList.Where(money => money.Year == year).OrderBy(m => m.Month).ToList();

                    model.data = result;
                    model.hasError = false;
                }
            }
            catch (Exception e)
            {
                model.hasError = true;
                model.error = e.Message;
            }

            return Json(model);
        }

        /// <summary>
        /// 方式：等额本金还款
        /// </summary>
        /// <param name="deadline">期限</param>
        /// <param name="money">总金额</param>
        /// <param name="dRate">年利率</param>
        /// <returns></returns>
        private Tuple<IList<LoanModel>,double,double> MonthMoney(int deadline, double money, double rate)
        {
            double[] sparePrin = new double[deadline];//剩余本金

            var principal = Math.Round(money / deadline, 0);
            double interest = 0;
            double firstMonthMoney = 0;

            IList<LoanModel> LoanModelList = new List<LoanModel>();
            for (int i = 0; i < deadline; i++)
            {
                var m = new LoanModel();
                m.Id = i + 1;
                m.Year = i / 12 + 1;
                m.Month = i % 12 + 1;
                m.Principal = principal;
                if (i == 0)
                {
                    sparePrin[i] = money - money / deadline;//剩余本金
                    if (sparePrin[i] < 0) sparePrin[i] = 0;
                    m.Interest = Math.Round(money * (rate / 12), 0);
                    interest += money * (rate / 12);
                    firstMonthMoney = m.Principal + money * (rate / 12);
                }
                else
                {
                    sparePrin[i] = sparePrin[i - 1] - money / deadline;//剩余本金
                    if (sparePrin[i] < 0) sparePrin[i] = 0;
                    m.Interest = Math.Round(sparePrin[i - 1] * (rate / 12), 0);
                    interest += sparePrin[i - 1] * (rate / 12);
                }
                m.Loan = Math.Round(sparePrin[i], 0);
                LoanModelList.Add(m);
            }

            return new Tuple<IList<LoanModel>, double, double>(LoanModelList, Math.Round(interest,0), Math.Round(firstMonthMoney,0));
        }

        /// <summary>
        /// 方式：等额本息还款
        /// </summary>
        /// <param name="deadline">期限</param>
        /// <param name="money">总金额</param>
        /// <param name="rate">年利率</param>
        /// <returns></returns>
        private Tuple<IList<LoanModel>, double,double> TotalMoney(int deadline, double money, double rate)
        {
            int i;
            double tlnAcct = 0, tdepAcct = 0;
            double interest = 0;
            double firstMonthMoney = 0;
            IList<LoanModel> LoanModelList = new List<LoanModel>();

            tlnAcct = money;

            for (i = 0; i < deadline; i++)
            {

                var m = new LoanModel();
                m.Id = i + 1;
                m.Year = i / 12 + 1;
                m.Month = i % 12 + 1;
                m.Principal = Math.Round((Math.Pow((1 + rate / 12), i + 1) - Math.Pow((1 + rate / 12), i)) / (Math.Pow((1 + rate / 12), deadline) - 1) * money, 0);
                m.Interest = Math.Round(tlnAcct * rate / 12, 0);
                interest += tlnAcct * rate / 12;
                m.Loan = Math.Round(tlnAcct - ((Math.Pow((1 + rate / 12), i + 1) - Math.Pow((1 + rate / 12), i)) / (Math.Pow((1 + rate / 12), deadline) - 1) * money),0);
                if (m.Loan < 0 || i == (deadline - 1)) m.Loan = 0;
                tlnAcct = tlnAcct - ((Math.Pow((1 + rate / 12), i + 1) - Math.Pow((1 + rate / 12), i)) / (Math.Pow((1 + rate / 12), deadline) - 1) * money);
                LoanModelList.Add(m);
                if (i == 0)
                {
                    firstMonthMoney = ((Math.Pow((1 + rate / 12), i + 1) - Math.Pow((1 + rate / 12), i)) / (Math.Pow((1 + rate / 12), deadline) - 1) * money) + (tlnAcct * rate / 12);
                }
            }



            return new Tuple<IList<LoanModel>, double, double>(LoanModelList, Math.Round(interest,0), Math.Round(firstMonthMoney,0));
        }

        /// <summary>
        /// 方式：等额本息还款(组合贷款）
        /// </summary>
        /// <param name="deadline">期限</param>
        /// <param name="money">总金额</param>
        /// <param name="rate">年利率</param>
        /// <param name="deadline_gjj">期限</param>
        /// <param name="money_gjj">总金额</param>
        /// <param name="rate_gjj">年利率</param>
        /// <returns></returns>
        private Tuple<IList<LoanModel>, double,double> TotalMoneyZH(int deadline, double money, double rate, int deadline_gjj, double money_gjj, double rate_gjj)
        {
            double tlnAcct = 0, tlnAcct_gjj = 0, tdepAcct = 0, loan = 0;
            double interest = 0;
            double firstMonthMoney = 0;
            IList<LoanModel> LoanModelList = new List<LoanModel>();
            tlnAcct = money;
            tlnAcct_gjj = money_gjj;

            var maxDeadLine = Math.Max(deadline, deadline_gjj);

            for (int i = 0; i < maxDeadLine; i++)
            {
                var m = new LoanModel();
                m.Id = i + 1;
                m.Year = i / 12 + 1;
                m.Month = i % 12 + 1;
                if (i < deadline)
                {
                    m.Principal = m.Principal + (Math.Pow((1 + rate / 12), i + 1) - Math.Pow((1 + rate / 12), i)) / (Math.Pow((1 + rate / 12), deadline) - 1) * money;
                    interest += m.Interest + tlnAcct * rate / 12; 
                    m.Interest = m.Interest + tlnAcct * rate / 12;
                }
                if (i < deadline_gjj)
                {
                    m.Principal = m.Principal + (Math.Pow((1 + rate_gjj / 12), i + 1) - Math.Pow((1 + rate_gjj / 12), i)) / (Math.Pow((1 + rate_gjj / 12), deadline_gjj) - 1) * money_gjj;
                    interest += m.Interest + tlnAcct_gjj * rate_gjj / 12;
                    m.Interest = m.Interest + tlnAcct_gjj * rate_gjj / 12;
                }
                tlnAcct = tlnAcct -
                          (Math.Pow((1 + rate / 12), i + 1) - Math.Pow((1 + rate / 12), i)) /
                          (Math.Pow((1 + rate / 12), deadline) - 1) * money;
                tlnAcct_gjj = tlnAcct_gjj -
                              (Math.Pow((1 + rate_gjj / 12), i + 1) - Math.Pow((1 + rate_gjj / 12), i)) /
                              (Math.Pow((1 + rate_gjj / 12), deadline_gjj) - 1) * money_gjj;
                m.Loan = tlnAcct + tlnAcct_gjj;
                if (m.Loan < 0 || i == (deadline - 1)) m.Loan = 0;

                m.Principal = Math.Round(m.Principal, 0);
                m.Interest = Math.Round(m.Interest, 0);
                m.Loan = Math.Round(m.Loan, 0);
                LoanModelList.Add(m);
                if (i == 0)
                {
                    firstMonthMoney = m.Principal + m.Interest;
                }
            }


            return new Tuple<IList<LoanModel>, double, double>(LoanModelList, interest, firstMonthMoney);
        }

        /// <summary>
        /// 方式：等额本金还款(组合贷款)
        /// </summary>
        /// <param name="deadline">期限</param>
        /// <param name="money">总金额</param>
        /// <param name="dRate">年利率</param>
        /// <param name="deadline_gjj">公积金期限</param>
        /// <param name="money_gjj">公积金总金额</param>
        /// <param name="dRate_gjj">公积金年利率</param>
        /// <returns></returns>
        private Tuple<IList<LoanModel>, double,double> MonthMoneyZH(int deadline, double money, double rate, int deadline_gjj, double money_gjj, double rate_gjj)
        {
            double[] sparePrin = new double[deadline];//剩余本金
            double[] sparePrin_gjj = new double[deadline_gjj];//剩余本金
            double interest = 0;
            double firstMonthMoney = 0;
            var principal = money / deadline;
            var principal_gjj = money_gjj / deadline_gjj;


            IList<LoanModel> LoanModelList = new List<LoanModel>();

            var maxDeadLine = Math.Max(deadline, deadline_gjj);

            for (int i = 0; i < maxDeadLine; i++)
            {
                var m = new LoanModel();
                m.Id = i + 1;
                m.Year = i / 12 + 1;
                m.Month = i % 12 + 1;

                if (i < deadline)
                {
                    m.Principal = m.Principal + principal;
                    if (i == 0)
                    {
                        sparePrin[i] = money - money / deadline;//剩余本金
                        if (sparePrin[i] < 0) sparePrin[i] = 0;
                        interest += m.Interest + money * (rate / 12);
                        m.Interest = m.Interest + money * (rate / 12);
                    }
                    else
                    {
                        sparePrin[i] = sparePrin[i - 1] - money / deadline;//剩余本金
                        if (sparePrin[i] < 0) sparePrin[i] = 0;
                        interest += m.Interest + sparePrin[i - 1] * (rate / 12);
                        m.Interest = m.Interest + sparePrin[i - 1] * (rate / 12);
                    }
                    m.Loan = m.Loan + sparePrin[i];
                }

                if (i < deadline_gjj)
                {
                    m.Principal = m.Principal + principal_gjj;
                    if (i == 0)
                    {
                        sparePrin_gjj[i] = money_gjj - money_gjj / deadline_gjj;//剩余本金
                        if (sparePrin_gjj[i] < 0) sparePrin_gjj[i] = 0;
                        interest += m.Interest + money_gjj * (rate_gjj / 12);
                        m.Interest = m.Interest + money_gjj * (rate_gjj / 12);
                    }
                    else
                    {
                        sparePrin_gjj[i] = sparePrin_gjj[i - 1] - money_gjj / deadline_gjj;//剩余本金
                        if (sparePrin_gjj[i] < 0) sparePrin_gjj[i] = 0;
                        interest += m.Interest + sparePrin_gjj[i - 1] * (rate_gjj / 12);
                        m.Interest = m.Interest + sparePrin_gjj[i - 1] * (rate_gjj / 12);
                    }
                    m.Loan = m.Loan + sparePrin_gjj[i];
                }

                m.Principal = Math.Round(m.Principal, 0);
                m.Interest = Math.Round(m.Interest, 0);
                m.Loan = Math.Round(m.Loan, 0);

                LoanModelList.Add(m);
                
                if (i == 0)
                {
                    firstMonthMoney = m.Principal + m.Interest;
                }
            }


            return new Tuple<IList<LoanModel>, double, double>(LoanModelList, interest, firstMonthMoney);
        }

    }
}