﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PinganHouse.SHHTS.WebUI.Controllers
{
    public class AgentCaseSearchController : Controller
    {
        // GET: AgentCaseSearch
        public ActionResult Index()
        {
            return View();
        }

        // GET: AgentCaseSearch/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: AgentCaseSearch/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AgentCaseSearch/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: AgentCaseSearch/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: AgentCaseSearch/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: AgentCaseSearch/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: AgentCaseSearch/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        // GET: AgentCaseSearch/Edit/5
        public ActionResult Search()
        {
            return View();
        }

        // POST: AgentCaseSearch/Edit/5
        [HttpPost]
        public ActionResult Search(FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
