﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PinganHouse.SHHTS.WebUI.Controllers
{
    public class LoanOptimizeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetPlans(string ipss)
        {
            OInputs ips = Newtonsoft.Json.JsonConvert.DeserializeObject<OInputs>(ipss);

            IList<LoanOptimizePlan> plans = new List<LoanOptimizePlan>();

            try
            {
                plans= LoanOptimizeServiceProxy.GetPlans(ips);
            }
            catch(Exception ex)
            {
                ViewBag.Error = ex.Message;
            }

            return View(plans);
        }
    }
}