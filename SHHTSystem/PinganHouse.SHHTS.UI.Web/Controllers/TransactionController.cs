﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.WebUI.Filter;
using PinganHouse.SHHTS.WebUI.Helper;
using PinganHouse.SHHTS.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PinganHouse.SHHTS.WebUI.Controllers
{
    public class TransactionController : Controller
    {
        /// <summary>
        /// 分页显示数
        /// </summary>
        const int m_pageSize = 30;

        /// <summary>
        /// 用户集合
        /// </summary>
        private List<long> m_User;

        // GET: Transaction
        [OAuth2Filter]
        public ActionResult Index(string caseId,int isMine=0)
        {
            var tradeDetailList = new List<TradeDetailInfo>();
            ViewData["IsMine"] = isMine;
            ViewData["CaseId"] = caseId;
            try
            {
                m_User = ConvetDataType(UserHelper.GetCurrentUser());
                //m_User = new List<long>() { 51794 };
                int cnt=0;
                var tradeDetails = TradeServiceProxy.GetOrders(1, m_pageSize, out cnt, Convert.ToBoolean(isMine), m_User, caseId);
                if (tradeDetails != null) tradeDetailList = TradeDetailInfo.GetTradeDetailInfoList(tradeDetails);
            }
            catch (Exception){}
            return View(tradeDetailList);
        }

        /// <summary>
        /// 交易详情
        /// </summary>
        /// <param name="tradeNo">流水号</param>
        /// <param name="caseId">案件号</param>
        /// <param name="tenementAddress">物业地址</param>
        /// <returns></returns>
        public ActionResult TransferDetails(string tradeNo,string caseId, string orderNo)
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("Order", null);
            dictionary.Add("Case", null);
            dictionary.Add("TradeDetail", null);
            try
            {
                var order = TradeServiceProxy.GetOrderInfo(orderNo);
                dictionary["Order"] = order;
                var tradeDetail= TradeServiceProxy.GetTradeInfo(tradeNo);
                dictionary["TradeDetail"] = tradeDetail;
                var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(caseId);
                dictionary["Case"] = caseDto;
            }
            catch (Exception){}
            return View(dictionary);
        }

        /// <summary>
        /// 获取交易流水
        /// </summary>
        /// <returns></returns>
        public ActionResult GetTradeModeAjaxList()
        {
            var model = new AjaxModel();
            try
            {
                //判定页数
                int pageIndex = 0;
                int.TryParse(Request["pageIndex"],out pageIndex);
                //判定案件编号
                var caseId = Request["caseId"];
                //盘点是否我的
                int isMine = 0;
                int.TryParse(Request["isMine"], out isMine);

                int cnt = 0;
                //m_User = new List<long>() { 51794 };
                m_User = ConvetDataType(UserHelper.GetCurrentUser());
                var tradeDetails = TradeServiceProxy.GetOrders(pageIndex, m_pageSize, out cnt, Convert.ToBoolean(isMine), m_User, caseId);
                model.data = TradeDetailInfo.GetTradeDetailInfoList(tradeDetails);
                model.hasError = false;
            }
            catch (Exception e)
            {
                model.hasError = true;
                model.error = e.Message;
            }
            return Json(model);
        }

        /// <summary>
        /// 转换
        /// </summary>
        /// <param name="sourceData"></param>
        /// <returns></returns>
        private List<long> ConvetDataType(IList<string> sourceData)
        {
            List<long> list = new List<long>();
            foreach (var source in sourceData)
            {
                list.Add(long.Parse(source));
            }
            return list;
        }
    
    }
}