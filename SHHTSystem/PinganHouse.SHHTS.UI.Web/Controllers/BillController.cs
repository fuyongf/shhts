﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.WebUI.Filter;
using PinganHouse.SHHTS.WebUI.Helper;
using PinganHouse.SHHTS.WebUI.Models;

namespace PinganHouse.SHHTS.WebUI.Views
{
    public class BillController : Controller
    {

        [OAuth2QYFilter(AgentType.AgentCompany)]
        public ActionResult Index(string companyId)
        {
            try
            {
                var year = DateTime.Today.Year;
                if (Request["Year"] != null)
                {
                    year = int.Parse(Request["Year"]);
                }
                var month = DateTime.Today.Month;
                if (Request["Month"] != null)
                {
                    month = int.Parse(Request["Month"]);
                }
                var bill = GetAccountBill(year, month, companyId);

                if (bill != null)
                {
                    ViewData["PaymentAmount"] = bill.PaymentAmount.ToString("F0");
                    ViewData["CollectionAmount"] = bill.CollectionAmount.ToString("F0");
                    ViewData["AccountSysNo"] = bill.AccountSysNo;
                    return View(bill.OrderDetails);
                }
                else
                {
                    ViewData["PaymentAmount"] = 0;
                    ViewData["CollectionAmount"] = 0;
                    ViewData["AccountSysNo"] = 0;
                    return View();
                }


                

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }


        private AccountBill GetAccountBill(int year, int month,string companyId=null)
        {
            if (month.ToString().Length == 1)
            {
                ViewData["CurrentMonth"] = "0"+month.ToString();
            }
            else
            {
                ViewData["CurrentMonth"] = month.ToString();    
            }
            
            ViewData["CurrentYear"] = year;
            
            var startDate = DateTime.Parse(year + "-" + month.ToString() + "-" + "01");
            //var startDate = DateTime.Parse(year + "-" + "04" + "-" + "01");
            var endDate = startDate.AddMonths(1);
            
            if (string.IsNullOrEmpty(companyId))
            {
                string userId = UserHelper.GetAgentId();
                //string userId = "A154946150603314";
                var agency = AgentServiceProxy.GetAgentSatff(userId);
                if (agency != null)
                {
                    ViewData["CurrentCompanyId"] = agency.AgentCompanySysNo;
                }
                else
                {
                    ViewData["CurrentCompanyId"] = 0;
                }
            }
            else
            {
                ViewData["CurrentCompanyId"] = companyId;
            }

           
            //ViewData["CurrentCompanyId"] = "13392";

            var bill = new AccountBill();
            bill = AccountServiceProxy.GetAccountBill(AccountType.Agency, ViewData["CurrentCompanyId"].ToString(), startDate,endDate);
            return bill;
        }

        [OAuth2QYFilter(AgentType.AgentCompany)]
        public ActionResult GetBill(string companyId)
        {
            var model = new AjaxModel();
            try
            {
                var year = DateTime.Today.Year;
                if (Request.Form["Year"] != null)
                {
                    year = int.Parse(Request.Form["Year"]);
                }
                var month = DateTime.Today.Month;
                if (Request.Form["Month"] != null)
                {
                    month = int.Parse(Request.Form["Month"]);
                }
                var bill = GetAccountBill(year,month);

                if (bill != null)
                {
                    ViewData["PaymentAmount"] = bill.PaymentAmount.ToString("F0");
                    ViewData["CollectionAmount"] = bill.CollectionAmount.ToString("F0");
                    ViewData["AccountSysNo"] = bill.AccountSysNo;
                }
                else
                {
                    ViewData["PaymentAmount"] = 0;
                    ViewData["CollectionAmount"] = 0;
                    ViewData["AccountSysNo"] = 0;
                }


                model.data = bill.OrderDetails;
                model.hasError = false;

                return Json(model);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                model.hasError = true;
                model.error = ex.Message;
                return null;
            }
        }

        [OAuth2QYFilter(AgentType.AgentCompany)]
        public ActionResult Detail(long id)
        {
            var year = DateTime.Today.Year;
            if (Request["Year"] != null)
            {
                year = int.Parse(Request["Year"]);
            }
            var month = DateTime.Today.Month;
            if (Request["Month"] != null)
            {
                month = int.Parse(Request["Month"]);
            }
            var bill = GetAccountBill(year,month);

            foreach (var order in bill.OrderDetails)
            {
                if (order.SysNo == id)
                {
                    ViewData["OrderDate"] = order.ModifyDate.Value.ToString("MM-dd") + " " + order.ModifyDate.Value.ToString("dddd", new System.Globalization.CultureInfo("zh-cn"));
                    ViewData["OrderBizType"] = EnumExtends.GetDescription(order.OrderBizType);
                    if (order.OrderType == OrderType.Collection ||
                        (order.OrderType == OrderType.Payment && order.OppositeAccount == bill.AccountSysNo))
                    {
                        ViewData["OrderAmount"] = "+" + order.OrderAmount.ToString("F0");
                    }
                    else
                    {
                        ViewData["OrderAmount"] = "-" + order.OrderAmount.ToString("F0");
                    }
                    ViewData["TenementAddress"] = order.TenementAddress;
                    ViewData["Remark"] = order.Remark;
                    ViewData["CaseId"] = order.CaseId;
                    ViewData["AgentName"] = order.AgentName;
                }
            }
            return View();
        }

        [OAuth2QYFilter(AgentType.AgentCompany)]
        public ActionResult Report()
        {
            IList<BillReportModel> reportList = new List<BillReportModel>();
            decimal income = 0;
            decimal pay = 0;
            try
            {
                var year = DateTime.Today.Year;
                if (Request["Year"] != null)
                {
                    year = int.Parse(Request["Year"]);
                }
                var month = DateTime.Today.Month;
                if (Request["Month"] != null)
                {
                    month = int.Parse(Request["Month"]);
                }
                var bill = GetAccountBill(year,month);
                if (bill != null)
                {
                    ViewData["AccountSysNo"] = bill.AccountSysNo;

                    foreach (int emCode in Enum.GetValues(typeof (OrderBizType)))
                    {
                        var reportModel = new BillReportModel();
                        foreach (var order in bill.OrderDetails)
                        {
                            if (order.OrderBizType.GetHashCode() == emCode)
                            {
                                reportModel.BizType = order.OrderBizType;

                                if (order.OrderType == OrderType.Collection ||
                                    (order.OrderType == OrderType.Payment && order.OppositeAccount == bill.AccountSysNo))
                                {
                                    reportModel.Amount += order.OrderAmount;
                                }
                                else
                                {
                                    reportModel.Amount -= order.OrderAmount;
                                }
                            }
                        }
                        if (reportModel.Amount != 0)
                        {
                            reportList.Add(reportModel);
                            if (reportModel.Amount > 0)
                            {
                                income += reportModel.Amount;
                            }
                            else
                            {
                                pay += reportModel.Amount;
                            }
                        }
                    }
                }
                ViewData["Income"] = income.ToString("F0");
                ViewData["Pay"] = pay.ToString("F0");
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
            return View(reportList);
        }

        [OAuth2QYFilter(AgentType.AgentCompany)]
        public ActionResult GetReport()
        {
            var model = new AjaxModel();
            try
            {
                IList<BillReportModel> reportList = new List<BillReportModel>();
                decimal income = 0;
                decimal pay = 0;

                var year = DateTime.Today.Year;
                if (Request.Form["Year"] != null)
                {
                    year = int.Parse(Request.Form["Year"]);
                }
                var month = DateTime.Today.Month;
                if (Request.Form["Month"] != null)
                {
                    month = int.Parse(Request.Form["Month"]);
                }
                var bill = GetAccountBill(year, month);

                ViewData["AccountSysNo"] = bill.AccountSysNo;
                foreach (int emCode in Enum.GetValues(typeof(OrderBizType)))
                {
                    var reportModel = new BillReportModel();
                    foreach (var order in bill.OrderDetails)
                    {
                        if (order.OrderBizType.GetHashCode() == emCode)
                        {
                            reportModel.BizType = order.OrderBizType;

                            if (order.OrderType == OrderType.Collection ||
                                (order.OrderType == OrderType.Payment && order.OppositeAccount == bill.AccountSysNo))
                            {
                                reportModel.Amount += order.OrderAmount;
                            }
                            else
                            {
                                reportModel.Amount -= order.OrderAmount;
                            }
                        }
                    }
                    if (reportModel.Amount != 0)
                    {
                        reportList.Add(reportModel);
                        if (reportModel.Amount > 0)
                        {
                            income += reportModel.Amount;
                        }
                        else
                        {
                            pay += reportModel.Amount;
                        }
                    }
                }
                
                model.data = reportList;
                model.hasError = false;

                return Json(model);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                model.hasError = true;
                model.error = ex.Message;
                return null;
            }
        }

        [OAuth2QYFilter(AgentType.AgentCompany)]
        public ActionResult ReportDetail()
        {
            IList<OrderBilledInfo> orderList = new List<OrderBilledInfo>();
            decimal amount = 0;
            var bizType = string.Empty;
            if (Request["bizType"] != null)
            {
                bizType= EnumExtends.GetDescription((OrderBizType) Enum.Parse(typeof (OrderBizType), Request["bizType"]));
            }
            ViewData["BizType"] = bizType;
            try
            {
                var year = DateTime.Today.Year;
                if (Request["Year"] != null)
                {
                    year = int.Parse(Request["Year"]);
                }
                var month = DateTime.Today.Month;
                if (Request["Month"] != null)
                {
                    month = int.Parse(Request["Month"]);
                }

                

                var bill = GetAccountBill(year, month);
                if (bill != null)
                {
                    ViewData["AccountSysNo"] = bill.AccountSysNo;
                    foreach (var order in bill.OrderDetails)
                    {
                        if (order.OrderBizType == (OrderBizType) Enum.Parse(typeof (OrderBizType), Request["bizType"]))
                        {
                            if (order.OrderType == OrderType.Collection ||
                                (order.OrderType == OrderType.Payment && order.OppositeAccount == bill.AccountSysNo))
                            {
                                amount += order.OrderAmount;
                            }
                            else
                            {
                                amount -= order.OrderAmount;
                            }
                            orderList.Add(order);
                        }
                    }
                    ViewData["Amount"] = amount.ToString("F0");
                    
                }
            }

            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }

            return View(orderList);
        }


    }
}