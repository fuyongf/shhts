﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.WebUI.Controllers
{
    public class CaptchaController : Controller
    {
        // GET: Captcha
        public ActionResult RequestCaptcha(string t, BusinessCacheType b)
        {
            var newCaptcha = CaptchaProxyService.GetInstanse().RequetCaptcha(t, b);
            return this.File(newCaptcha.WriteToStream(), "image/gif");
        }

        public ActionResult ValidateCaptcha(string k, string captcha)
        {
            var result = CaptchaProxyService.GetInstanse().ValidateCaptcha(k, captcha);
            return this.Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ValidateCaptcha(string t, BusinessCacheType b,  string captcha)
        {
            var result = CaptchaProxyService.GetInstanse().ValidateCaptcha(t, b, captcha);
            return this.Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}