﻿using Newtonsoft.Json;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.WebUI.Filter;
using PinganHouse.SHHTS.WebUI.Helper;
using PinganHouse.SHHTS.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PinganHouse.SHHTS.WebUI.Controllers
{
    public class CaseController : Controller
    {
        /// <summary>
        /// 分页数据
        /// </summary>
        private int m_pageSize =25;

        #region 经纪公司相关页面
        [OAuth2QYFilter(AgentType.AgentCompany)]
        // GET: Case
        public ActionResult CaseAgentCompanyIndex(List<CaseModel> caseList)
        {
            var searchModel = new SearchModel();
            var status = 0;
            //判断状态
            int.TryParse(Request["status"],out status);
            switch (status)
            {
                case 1://进行中
                    ViewData["Status"] = 1;
                    searchModel.Status = 1;
                    break;
                case 2://一结束
                    ViewData["Status"] = 2;
                    searchModel.Status = 2;
                    break;
                default:
                    ViewData["Status"] = "allCase";
                    searchModel.Status = 0;
                    break;
            }
            ViewData["SearchModel"] = JsonConvert.SerializeObject(searchModel);
            //caseList = CaseModel.GetCaseModelList(1, m_pageSize, "C155542160305498", AgentType.AgentCompany, searchModel);
            caseList = CaseModel.GetCaseModelList(1, m_pageSize, UserHelper.GetAgentId(), AgentType.AgentCompany, searchModel);
            return View(caseList);
        }

        /// <summary>
        /// 经济公司案件查询
        /// </summary>
        /// <returns></returns>
        public ActionResult CaseAgentCompanySearch()
        {
            return View();
        }

        /// <summary>
        /// 获取案件详情
        /// </summary>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CaseAgentCompanySearchIndex(FormCollection values)
        {
            try
            {
                var searchModel = new SearchModel();
                //案件编号
                searchModel.CaseId = values["caseId"];
                //区域
                searchModel.Area = values["area"];
                //地址
                searchModel.Address = values["address"];
                //状态栏
                searchModel.Status = Convert.ToInt32(values["status"]);
                ViewData["Status"] = searchModel.Status;
                //开始时间
                var starttime = values["starttime"];
                if (!string.IsNullOrEmpty(starttime))
                {
                    searchModel.StartTime = Convert.ToDateTime(starttime);
                }
                //结束时间
                var endtime = values["endtime"];
                if (!string.IsNullOrEmpty(endtime))
                {
                    searchModel.EndTime = Convert.ToDateTime(endtime);
                }
                //经纪人
                searchModel.StaffName = values["staffName"];
                //经纪人电话
                searchModel.StaffPhone = values["staffPhone"];
                ViewData["SearchModel"] = JsonConvert.SerializeObject(searchModel);
                //获取案件
                var caseList = CaseModel.GetCaseModelList(1, m_pageSize, UserHelper.GetAgentId(), AgentType.AgentCompany, searchModel);
                return View("CaseAgentCompanyIndex", caseList);
            }
            catch (Exception)
            {
                return View("CaseAgentCompanyIndex", new List<CaseModel>());
            }
        }

        /// <summary>
        /// Ajax请求数据
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetAgentCompanyCaseModeAjaxList()
        {
            var model = new AjaxModel();
            try
            {
                int pageIndex = 0;
                int.TryParse(Request["pageIndex"], out pageIndex);
                var searchModel = JsonConvert.DeserializeObject<SearchModel>(Request["searchModel"]);
                if (searchModel == null) searchModel = new SearchModel();
                var caseList = CaseModel.GetCaseModelList(pageIndex, m_pageSize, UserHelper.GetAgentId(), AgentType.AgentCompany, searchModel);
                model.data = caseList;
                model.hasError = false;
            }
            catch (Exception e)
            {
                model.hasError = true;
                model.error = e.Message;
            }
            return Json(model);
        }
        #endregion 

        #region 经纪人相关页面

        [OAuth2QYFilter(AgentType.Agency)]
        // GET: Case
        public ActionResult CaseStaffIndex(List<CaseModel> caseList)
        {
            var searchModel = new SearchModel();
            var status = 0;
            //判断状态
            int.TryParse(Request["status"], out status);
            switch (status)
            {
                case 1://进行中
                    ViewData["Status"] = 1;
                    searchModel.Status = 1;
                    break;
                case 2://一结束
                    ViewData["Status"] = 2;
                    searchModel.Status = 2;
                    break;
                default:
                    ViewData["Status"] = "allCase";
                    searchModel.Status = 0;
                    break;
            }
            ViewData["SearchModel"] = JsonConvert.SerializeObject(searchModel);
            caseList = CaseModel.GetCaseModelList(1, m_pageSize, UserHelper.GetAgentId(), AgentType.Agency, searchModel);
            return View(caseList);
        }

        /// <summary>
        /// 案件查询
        /// </summary>
        /// <returns></returns>
        public ActionResult CaseStaffSearch()
        {
            return View();
        }

        /// <summary>
        /// 获取案件详情
        /// </summary>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CaseStaffSearchIndex(FormCollection values)
        {
            try
            {
                var searchModel = new SearchModel();
                //案件编号
                searchModel.CaseId = values["caseId"];
                //区域
                searchModel.Area = values["area"];
                //地址
                searchModel.Address = values["address"];
                //状态栏
                searchModel.Status = Convert.ToInt32(values["status"]);
                //开始时间
                var starttime = values["starttime"];
                if (!string.IsNullOrEmpty(starttime))
                {
                    searchModel.StartTime = Convert.ToDateTime(starttime);
                }
                //结束时间
                var endtime = values["endtime"];
                if (!string.IsNullOrEmpty(endtime))
                {
                    searchModel.EndTime = Convert.ToDateTime(endtime);
                }
                //经纪人
                searchModel.StaffName = values["staffName"];
                //经纪人电话
                searchModel.StaffPhone = values["staffPhone"];
                ViewData["SearchModel"] = JsonConvert.SerializeObject(searchModel);
                //获取案件
                var caseList = CaseModel.GetCaseModelList(1, m_pageSize, UserHelper.GetAgentId(), AgentType.Agency, searchModel);
                return View("CaseStaffIndex", caseList);
            }
            catch (Exception)
            {
                return View("CaseStaffIndex", new List<CaseModel>());
            }
        }

        /// <summary>
        /// Ajax请求数据
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetStaffCaseModeAjaxList()
        {
            var model = new AjaxModel();
            try
            {
                int pageIndex = 0;
                int.TryParse(Request["pageIndex"], out pageIndex);
                var searchModel = JsonConvert.DeserializeObject<SearchModel>(Request["searchModel"]);
                var caseList = CaseModel.GetCaseModelList(pageIndex, m_pageSize, UserHelper.GetAgentId(), AgentType.Agency, searchModel);
                model.data = caseList;
                model.hasError = false;
            }
            catch (Exception e)
            {
                model.hasError = true;
                model.error = e.Message;
            }
            return Json(model);
        }
        #endregion

        /// <summary>
        /// 案件详情
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public ActionResult QYCaseDetails(string caseId, string caseType, int isManager = 0)
        {
            try
            {
                ViewData["CaseType"] = caseType;
                ViewData["IsManager"] = isManager;
                var caseModel = CaseModel.GetCaseModel(caseId, caseType);
                return View(caseModel);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// 案件详情
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public ActionResult CaseDetails(string caseId,string caseType,int isManager=0)
        {
            try
            {
                ViewData["CaseType"] = caseType;
                ViewData["IsManager"] = isManager;
                var caseModel = CaseModel.GetCaseModel(caseId, caseType);
                return View(caseModel);
            }
            catch (Exception)
            {
                return null;
            }
        }


        /// <summary>
        /// 案件详情
        /// </summary>
        /// <returns></returns>
        public ActionResult ShowProcess(string path, string sendDate, string preProcess, string nextProcess, string nextDate, string receptionCenter, int index, string address)
        {
            var processModel = new CaseProcessModel
            {
                SendDate = sendDate,
                PreProcess = preProcess,
                NextProcess = nextProcess,
                NextDate = nextDate
            };
            if (!string.IsNullOrWhiteSpace(path))
            {
                var material = ConfigServiceProxy.GetMaterial(path);
                if (!string.IsNullOrWhiteSpace(material))
                {
                    processModel.Material = material.Split('@')[index];
                }
            }
            processModel.Address = address;
            return View(processModel);
        }
    }
}