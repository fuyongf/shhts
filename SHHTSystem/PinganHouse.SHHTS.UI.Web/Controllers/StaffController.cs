﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.WebUI.Models;
using PinganHouse.SHHTS.WebUI.Filter;
using PinganHouse.SHHTS.WebUI.Helper;

namespace PinganHouse.SHHTS.WebUI.Controllers
{
    public class StaffController : Controller
    {
        [OAuth2QYFilter(AgentType.AgentCompany)]
        public ActionResult Index()
        {
            return View();
        }

        [OAuth2QYFilter(AgentType.AgentCompany)]
        public ActionResult List()
        {
            try
            {
                var staffName = Request["txtStaffName"];
                ViewData["staffName"] = staffName;
                var staffMobile = Request["txtStaffMobile"];
                ViewData["staffMobile"] = staffMobile;
                var identityNo = Request["txtIdentityNo"];
                ViewData["identityNo"] = identityNo;
                var totalCount = 0;
                if (identityNo == "") identityNo = null;
                if (staffMobile == "") staffMobile = null;

                IList<AgentStaff> agentStaffList = new List<AgentStaff>();
                string userId = UserHelper.GetAgentId();
                //string userId = "A153739130527606";
                var agency = AgentServiceProxy.GetAgentSatff(userId);
                long agentCompanySysNo = 0;
                if (agency != null)
                {
                    agentCompanySysNo = agency.AgentCompanySysNo;
                }
                agentStaffList = AgentServiceProxy.GetAgentStaffs(1, 10, out totalCount, agentCompanySysNo, null, staffName, identityNo,
                    null, staffMobile);

                ViewData["totalCount"] = totalCount;

                return View(agentStaffList);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }

        [OAuth2QYFilter(AgentType.AgentCompany)]
        public ActionResult GetAjaxList()
        {
            var model = new AjaxModel();
            try
            {
                int pageIndex = 0;
                int.TryParse(Request["pageIndex"], out pageIndex);
                var staffName = Request.Form["staffName"];
                var staffMobile = Request.Form["staffMobile"];
                var identityNo = Request.Form["identityNo"];
                if (identityNo == "") identityNo = null;
                if (staffMobile == "") staffMobile = null;

                var totalCount = 0;
                string userId = UserHelper.GetAgentId();
                var agency = AgentServiceProxy.GetAgentSatff(userId);

                var staffList = AgentServiceProxy.GetAgentStaffs(pageIndex, 10, out totalCount, agency.AgentCompanySysNo, null, staffName,
                    identityNo, null, staffMobile);
                model.data = staffList;
                model.hasError = false;
            }
            catch (Exception e)
            {
                model.hasError = true;
                model.error = e.Message;
            }
            return Json(model);
        }

        [OAuth2QYFilter(AgentType.AgentCompany)]
        public ActionResult Detail()
        {
            try
            {
                long staffNo = 0;
                long.TryParse(Request["id"], out staffNo);
                AgentStaff staff = AgentServiceProxy.GetAgentSatff(staffNo);
                if (staff.IsManager)
                {
                    ViewData["staffGrade"] = "管理者";
                }
                else
                {
                    ViewData["staffGrade"] = "员工";
                }
                AgentCompany company = AgentServiceProxy.GetAgentCompany(staff.AgentCompanySysNo);
                if (company!=null)
                { 
                    ViewData["companyName"] = company.CompanyName;
                    ViewData["staffName"] = Request["name"];
                    ViewData["staffMobile"] = Request["mobile"];
                    ViewData["identityNo"] = Request["identityNo"];
                    ViewData["staffIsManager"] = staff.IsManager;
                }

                string userId = UserHelper.GetAgentId();
                //string userId = "A153739130527606";
                var agency = AgentServiceProxy.GetAgentSatff(userId);
                ViewData["IsManager"] = agency.IsManager;
                return View(staff);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }

        [HttpPost, OAuth2QYFilter(AgentType.AgentCompany)]
        public ActionResult Delete()
        {
            var model = new AjaxModel();
            try
            {
                string userId = UserHelper.GetAgentId();
                //string userId = "A153739130527606";
                var agency = AgentServiceProxy.GetAgentSatff(userId);
                if (agency.IsManager)
                {
                    long sysNo = 0;
                    long.TryParse(Request["id"], out sysNo);

                    var result = AgentServiceProxy.DeleteStaff(sysNo, UserHelper.GetAgentId());
                    if (!result.Success)
                    {
                        model.hasError = true;
                        model.error = result.ResultMessage;
                    }
                    
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);

            }
            return Json(model);
        }

    }
}
