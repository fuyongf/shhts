﻿using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.WebUI.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.WebUI.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Customer
        public ActionResult GetCustomer()
        {
            var user = UserHelper.GetWXUser();
            ViewBag.NickName= user["NickName"];
            ViewBag.OpenId = user["OpenId"];
            return View();
        }

        [HttpPost]
        public ActionResult GetCustomer(string identityNo, CertificateType certificateType, string captcha, string t)
        {
            var user = UserHelper.GetWXUser();
            var openId = user["OpenId"] as string;
            ViewBag.OpenId = openId;
            ViewBag.NickName = user["NickName"];
            var result = ParterProxyService.GetInstanse().CheckUser(openId, identityNo, certificateType, captcha, t);
            if(!result.Success)
                return View();
            return RedirectToAction("BindCustomer", "Customer", JsonSerializer.SerializeToJson(result.OtherData));
        }

        /// <summary>
        /// 页面
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="mobile"></param>
        /// <returns></returns>
        public ActionResult BindCustomer(string userId, string mobile)
        {
            ViewBag.UserId = userId;
            ViewBag.Mobile = mobile;
            return View();
        }

        /// <summary>
        /// ajax 请求
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="mobile"></param>
        /// <returns></returns>
        public ActionResult SendBindCaptcha(string userId, string mobile)
        {
            var result = ParterProxyService.GetInstanse().SendBindCaptcha(userId, mobile);
            return this.Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// ajax post请求
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="captcha"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Bind(string userId, string captcha, string token)
        {
            var user = UserHelper.GetWXUser();
            var openId = user["OpenId"] as string;
            var result = ParterProxyService.GetInstanse().BindUser(openId, userId, captcha, token);
            return this.Json(result);
        }
    }
}