﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Web;
using System.Web.Mvc;

namespace PinganHouse.SHHTS.WebUI.Controllers
{
    public class TaxCalculationController : Controller
    {
        // GET: TaxCalculation
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Result()
        {
            decimal area = 0;
            decimal price = 0;
            int priceType = 0;
            decimal originalPrice = 0;
            int houseType = 0;
            bool fiveYear;
            bool twoYear;
            bool first;
            bool onlyOne;

            decimal.TryParse(Request.Form["txtArea"], out area);
            decimal.TryParse(Request.Form["txtTotalPrice"], out price);
            price = price * 10000;

            string notarized = "";
            decimal loan = 0;
            string commissionedNotarized = "";

            decimal buyerTotal = 0;
            decimal sellerTotal = 0;

            int.TryParse(Request.Form["rdoPriceType"], out priceType);

            decimal.TryParse(Request.Form["txtOriginalPrice"], out originalPrice);
            originalPrice = originalPrice * 10000;

            int.TryParse(Request.Form["rdoHouseType"], out houseType);

            if (Request.Form["selYears"] == "1")
            {
                twoYear = false;
                fiveYear = false;
            }
            else if (Request.Form["selYears"] == "2")
            {
                twoYear = true;
                fiveYear = false;
            }
            else 
            {
                twoYear = true;
                fiveYear = true;
            }

            if (Request.Form["First"] == "on")
            {
                first = true;
            }
            else
            {
                first = false;
            }

            if (Request.Form["OnlyOne"] == "on")
            {
                onlyOne = true;
            }
            else
            {
                onlyOne = false;
            }


            if (Request.Form["Loan"] == "on")
            {
                decimal.TryParse(Request.Form["txtLoan"], out loan);
                loan = loan * 10000;
            }
            else
            {
                loan = 0;
            }


            //买方契税
            var deedTax = GetDeedTax(houseType, fiveYear, first, area, price);
            ViewData["DeedTax"] = deedTax.Item1.ToString("N");
            buyerTotal = buyerTotal + deedTax.Item1;

            //买方权证印花税
            var certificateStamDuty = GetCertificateStamDuty();
            ViewData["CertificateStamDuty"] = certificateStamDuty.Item1.ToString("N");
            buyerTotal = buyerTotal + certificateStamDuty.Item1;

            //买方登记费
            var registrationFee = GetRegistrationFee();
            ViewData["RegistrationFee"] = registrationFee.Item1.ToString("N");
            buyerTotal = buyerTotal + registrationFee.Item1;

            //买方图纸费
            var blueprintFee = GetBlueprintFee();
            ViewData["BlueprintFee"] = blueprintFee.Item1.ToString("N");
            buyerTotal = buyerTotal + blueprintFee.Item1;

            if (Request.Form["Loan"] == "on")
            {
                //买方抵押登记费
                var mortgageRegistrationFee = GetMortgageRegistrationFee();
                ViewData["MortgageRegistrationFee"] = mortgageRegistrationFee.Item1.ToString("N");
                buyerTotal = buyerTotal + mortgageRegistrationFee.Item1;
            }
            else
            {
                ViewData["MortgageRegistrationFee"] = "0.00";
            }

            //买方交易手续费
            var buyerTransactionFee = GetBuyerTransactionFee(area);
            ViewData["BuyerTransactionFee"] = buyerTransactionFee.Item1.ToString("N");
            buyerTotal = buyerTotal + buyerTransactionFee.Item1;


            if (Request.Form["Notarized"]=="on")
            {
                //公证费
                var notaryFee = GetNotaryFee(price);
                if (Request.Form["rdoNotarized"]=="1")
                {
                    //买方承担
                    ViewData["BuyerNotaryFee"] = notaryFee.Item1.ToString("N");
                    buyerTotal = buyerTotal + notaryFee.Item1;

                    ViewData["SellerNotaryFee"] = "0.00";
                }
                else if (Request.Form["rdoNotarized"] == "2")
                {
                    //卖方承担
                    ViewData["SellerNotaryFee"] = notaryFee.Item1.ToString("N");
                    sellerTotal = sellerTotal + notaryFee.Item1;

                    ViewData["BuyerNotaryFee"] = "0.00";
                }
                else if (Request.Form["rdoNotarized"] == "3")
                {
                    //卖买双方各半
                    ViewData["BuyerNotaryFee"] = (notaryFee.Item1 / 2).ToString("N");
                    buyerTotal = buyerTotal + (notaryFee.Item1 / 2);

                    ViewData["SellerNotaryFee"] = (notaryFee.Item1 / 2).ToString("N");
                    sellerTotal = sellerTotal + (notaryFee.Item1 / 2);

                }
                else
                {
                    //买方公证费
                    ViewData["BuyerNotaryFee"] = "0.00";
                    
                    //卖方公证费
                    ViewData["SellerNotaryFee"] = "0.00";
                }
            }
            else
            {
                //买方公证费
                ViewData["BuyerNotaryFee"] = "0.00";
               
                //卖方公证费
                ViewData["SellerNotaryFee"] = "0.00";
            }

            if (Request.Form["CommissionedNotarized"]=="on")
            {
                if (Request.Form["rdoCommissionedNotarized"]=="1")
                {
                    //买方委托公证费
                    var buyerCommissionedNotaryFee = GetBuyerCommissionedNotaryFee();
                    ViewData["BuyerCommissionedNotaryFee"] = buyerCommissionedNotaryFee.Item1.ToString("N");
                    buyerTotal = buyerTotal + buyerCommissionedNotaryFee.Item1;
                }
                else
                {
                    ViewData["BuyerCommissionedNotaryFee"] = "0.00";
                   
                }

                if (Request.Form["rdoCommissionedNotarized"] == "2")
                {
                    //卖方委托公证费
                    var sellerCommissionedNotaryFee = GetSellerCommissionedNotaryFee();
                    ViewData["SellerCommissionedNotaryFee"] = sellerCommissionedNotaryFee.Item1.ToString("N");
                    sellerTotal = sellerTotal + sellerCommissionedNotaryFee.Item1;
                }
                else
                {
                    ViewData["SellerCommissionedNotaryFee"] = "0.00";
                }

                if (Request.Form["rdoCommissionedNotarized"] == "3")
                {
                    //买方委托公证费
                    var buyerCommissionedNotaryFee = GetBuyerCommissionedNotaryFee();
                    ViewData["BuyerCommissionedNotaryFee"] = buyerCommissionedNotaryFee.Item1.ToString("N");
                    buyerTotal = buyerTotal + buyerCommissionedNotaryFee.Item1;

                    //卖方委托公证费
                    var sellerCommissionedNotaryFee = GetSellerCommissionedNotaryFee();
                    ViewData["SellerCommissionedNotaryFee"] = sellerCommissionedNotaryFee.Item1.ToString("N");
                    sellerTotal = sellerTotal + sellerCommissionedNotaryFee.Item1;
                }
                else
                {
                    ViewData["SellerCommissionedNotaryFee"] = "0.00";
                }
            }
            else
            {
                //买方委托公证费
                ViewData["BuyerCommissionedNotaryFee"] = "0.00";
                
                //卖方委托公证费
                ViewData["SellerCommissionedNotaryFee"] = "0.00";
            }



            if (Request.Form["Loan"]=="on")
            {
                if (Request.Form["LoanNotarized"]=="on")
                {
                    //买方贷款公证费
                    var creditNotarizationFee = GetCreditNotarizationFee(loan);
                    ViewData["CreditNotarizationFee"] = creditNotarizationFee.Item1.ToString("N");
                    buyerTotal = buyerTotal + creditNotarizationFee.Item1;
                }
                else
                {
                    ViewData["CreditNotarizationFee"] = "0.00";
                    
                }

                //买方贷款评估费
                var loanAssessmentFee = GetLoanAssessmentFee(price);
                ViewData["LoanAssessmentFee"] = loanAssessmentFee.Item1.ToString("N");
                buyerTotal = buyerTotal + loanAssessmentFee.Item1;
            }
            else
            {
                //买方贷款公证费
                ViewData["CreditNotarizationFee"] = "0.00";
                

                //买方贷款评估费
                ViewData["LoanAssessmentFee"] = "0.00";
                
            }

            //卖方营业税
            var sellerBusinessTax = GetSellerBusinessTax(houseType, twoYear, price, originalPrice);
            ViewData["SellerBusinessTax"] = sellerBusinessTax.Item1.ToString("N");
            sellerTotal = sellerTotal + sellerBusinessTax.Item1;

            //卖方个人所得税
            var sellerIncomeTax = GetSellerIncomeTax(priceType, fiveYear, houseType, onlyOne, price, originalPrice);
            ViewData["SellerIncomeTax"] = sellerIncomeTax.Item1.ToString("N");
            sellerTotal = sellerTotal + sellerIncomeTax.Item1;

            //卖方交易手续费
            var sellerTransactionFee = GetSellerTransactionFee(area);
            ViewData["SellerTransactionFee"] = sellerTransactionFee.Item1.ToString("N");
            sellerTotal = sellerTotal + sellerTransactionFee.Item1;

            ViewData["BuyerTotal"] = buyerTotal.ToString("N");
            
            ViewData["SellerTotal"] = sellerTotal.ToString("N");
            

            return View();
        }


        /// <summary>
        /// 计算卖方个人所得税
        /// </summary>
        /// <param name="priceType">价格类型：1总价；2差价</param>
        /// <param name="isFiveYear">是否满5年：true满5年；false未满5年</param>
        /// <param name="houseType">住宅类型：1普通住宅；2非普通住宅；3经济适用房</param>
        /// <param name="isOnlyOne">是否唯一住房：true是；false否</param>
        /// <param name="totalPrice">网签合同总价</param>
        /// <param name="originalPrice">原购入价</param>
        /// <returns></returns>
        private Tuple<decimal, string> GetSellerIncomeTax(int priceType, bool isFiveYear, int houseType, bool isOnlyOne, decimal totalPrice, decimal originalPrice = 0)
        {
            if (priceType == 2 && totalPrice <= originalPrice)
            {
                return new Tuple<decimal, string>(0, "");
            }

            //(普通住宅 & 未满5年 & 总价) 或者 （普通住宅 & 满5年 & 非唯一住房 & 总价）
            if ((houseType == 1 && isFiveYear == false && priceType == 1)
                || (houseType == 1 && isFiveYear == true && isOnlyOne == false && priceType == 1))
            {
                return new Tuple<decimal, string>(totalPrice * (decimal)0.01, "网签合同价*1%");
            }

            //(普通住宅 & 未满5年 & 差价) 或者 （普通住宅 & 满5年 & 非唯一住房 & 差价） 或者 （非普通住宅 & 未满5年 & 差价） 或者 （非普通住宅 & 满5年 & 非唯一住房 & 差价）
            if ((houseType == 1 && isFiveYear == false && priceType == 2)
                || (houseType == 1 && isFiveYear == true && isOnlyOne == false && priceType == 2)
                || (houseType == 2 && isFiveYear == false && priceType == 2)
                || (houseType == 2 && isFiveYear == true && isOnlyOne == false && priceType == 2))
            {
                return new Tuple<decimal, string>((totalPrice - originalPrice) * (decimal)0.2, "(网签合同价-原购入价)*20%");
            }

            //（普通住宅 & 满5年 & 唯一住房） 或者 （非普通住宅 & 满5年 & 唯一住房 ）
            if ((houseType == 1 && isFiveYear == true && isOnlyOne == true)
                || houseType == 2 && isFiveYear == true && isOnlyOne == true)
            {
                return new Tuple<decimal, string>(0, "");
            }

            //(非普通住宅 & 未满5年 & 总价) 或者 （非普通住宅 & 满5年 & 非唯一住房 & 总价）
            if ((houseType == 2 && isFiveYear == false && priceType == 1)
                || (houseType == 2 && isFiveYear == true && isOnlyOne == false && priceType == 1))
            {

                return new Tuple<decimal, string>(totalPrice * (decimal)0.02, "网签合同价*2%");
            }

            return new Tuple<decimal, string>(0, "");
        }

        /// <summary>
        /// 计算权证印花税
        /// </summary>
        /// <returns></returns>
        private Tuple<decimal, string> GetCertificateStamDuty()
        {
            const decimal result = 5;

            return new Tuple<decimal, string>(result, "固定金额");
        }

        /// <summary>
        /// 计算卖方营业税
        /// </summary>
        /// <param name="houseType">住宅类型：1普通住宅；2非普通住宅；3经济适用房</param>
        /// <param name="isTwoYear">是否满2年：true满2年；false未满2年</param>
        /// <param name="totalPrice">网签合同总价</param>
        /// <param name="originalPrice">原购入价</param>
        /// <returns></returns>
        private Tuple<decimal, string> GetSellerBusinessTax(int houseType, bool isTwoYear, decimal totalPrice, decimal originalPrice = 0)
        {
            decimal result = 0;

            //（普通住宅 & 未满2年） 或者 （非普通住宅 & 未满2年）
            if ((houseType == 1 && isTwoYear == false) || (houseType == 2 && isTwoYear == false))
            {
                return new Tuple<decimal, string>(totalPrice * (decimal)0.0565, "网签合同价*5.65%");
            }

            // 非普通住宅 & 满2年
            if (houseType == 2 && isTwoYear == true)
            {
                if (totalPrice <= originalPrice)
                {
                    return new Tuple<decimal, string>(0, "");
                }
                else
                {
                    return new Tuple<decimal, string>((totalPrice - originalPrice) * (decimal)0.0565, "(网签合同价-原购入价)*5.65%");    
                }
                
            }

            //普通住宅 & 满2年
            if (houseType == 1 && isTwoYear == true)
            {
                return new Tuple<decimal, string>(0, "");
            }


            return new Tuple<decimal, string>(0, "");
        }




        /// <summary>
        /// 计算买方契税
        /// </summary>
        /// <param name="houseType">住宅类型：1普通住宅；2非普通住宅；3经济适用房</param>
        /// <param name="isFiveYear">是否满5年：true满5年；false未满5年</param>
        /// <param name="isFirst">是否首次购房：true是首次；false不是首次</param>
        /// <param name="area">建筑面积</param>
        /// <param name="totalPrice">网签合同总价</param>
        /// <returns></returns>
        private Tuple<decimal, string> GetDeedTax(int houseType, bool isFiveYear, bool isFirst, decimal area, decimal totalPrice)
        {
            // （普通住宅 & 未满5年 & 首次购房 & 建筑面积<=90 ） 或者 （普通住宅 & 满5年 & 首次购房 & 建筑面积<=90）
            if ((houseType == 1 && isFiveYear == false && isFirst == true && area <= 90)
                || (houseType == 1 && isFiveYear == true && isFirst == true && area <= 90))
            {
                return new Tuple<decimal, string>(totalPrice * (decimal)0.01, "网签合同价*1% ");
            }

            // （普通住宅 & 未满5年 & 建筑面积>90 & 首次购房） 或者 （普通住宅 & 满5年 & 建筑面积>90 & 首次购房）
            if ((houseType == 1 && isFiveYear == false && area > 90 && isFirst == true)
                || (houseType == 1 && isFiveYear == true && area > 90 && isFirst == true))
            {
                return new Tuple<decimal, string>(totalPrice * (decimal)0.015, "网签合同价*1.5%");
            }

            // （普通住宅 & 未满5年 & 非首次购房） 或者 （普通住宅 & 满5年 & 非首次购房）或者（非普通住宅 & 满5年）或者（非普通住宅 & 不满5年）
            if ((houseType == 1 && isFiveYear == false && isFirst == false)
                || (houseType == 1 && isFiveYear == true && isFirst == false)
                || (houseType == 2 && isFiveYear == true)
                || (houseType == 2 && isFiveYear == false))
            {
                return new Tuple<decimal, string>(totalPrice * (decimal)0.03, "网签合同价*3%");
            }

            return new Tuple<decimal, string>(0, "");
        }


        /// <summary>
        /// 计算买方登记费
        /// </summary>
        /// <returns></returns>
        private Tuple<decimal, string> GetRegistrationFee()
        {
            return new Tuple<decimal, string>(80, "固定金额");
        }

        /// <summary>
        /// 计算买方图纸费
        /// </summary>
        /// <returns></returns>
        private Tuple<decimal, string> GetBlueprintFee()
        {
            return new Tuple<decimal, string>(25, "固定金额");
        }


        /// <summary>
        /// 计算抵押登记费
        /// </summary>
        /// <returns></returns>
        private Tuple<decimal, string> GetMortgageRegistrationFee()
        {
            return new Tuple<decimal, string>(80, "固定金额");
        }


        /// <summary>
        /// 计算买方交易手续费
        /// </summary>
        /// <param name="area"></param>
        /// <returns></returns>
        private Tuple<decimal, string> GetBuyerTransactionFee(decimal area)
        {
            return new Tuple<decimal, string>(area * (decimal)2.5, "建筑面积*2.5元");
        }

        /// <summary>
        /// 计算公证费
        /// </summary>
        /// <param name="totalPrice"></param>
        /// <returns></returns>
        private Tuple<decimal, string> GetNotaryFee(decimal totalPrice)
        {
            if (totalPrice <= 500 * 10000)
            {
                return new Tuple<decimal, string>(totalPrice * (decimal)0.0025 + 250, "合同价*0.25%+250");
            }
            else if (500 * 10000 < totalPrice && totalPrice <= 1000 * 10000)
            {
                return new Tuple<decimal, string>(totalPrice * (decimal)0.002 + 2750, "合同价*0.2%+2750");
            }
            else if (1000 * 10000 < totalPrice && totalPrice <= 2000 * 10000)
            {
                return new Tuple<decimal, string>(totalPrice * (decimal)0.0015 + 7750, "合同价*0.15%+7750");
            }
            else if (2000 * 10000 < totalPrice)
            {
                return new Tuple<decimal, string>(totalPrice * (decimal)0.001 + 17750, "合同价*0.1%+17750");
            }
            return new Tuple<decimal, string>(0, "");
        }


        /// <summary>
        /// 计算买方委托公证费
        /// </summary>
        /// <returns></returns>
        private Tuple<decimal, string> GetBuyerCommissionedNotaryFee()
        {
            return new Tuple<decimal, string>(500, "固定金额");
        }

        /// <summary>
        /// 计算买方贷款公证费
        /// </summary>
        /// <param name="loan"></param>
        /// <returns></returns>
        private Tuple<decimal, string> GetCreditNotarizationFee(decimal loan)
        {
            return new Tuple<decimal, string>(loan * (decimal)0.003, "贷款金额*3‰");
        }

        /// <summary>
        /// 计算买方贷款评估费
        /// </summary>
        /// <param name="totalPrice"></param>
        /// <returns></returns>
        private Tuple<decimal, string> GetLoanAssessmentFee(decimal totalPrice)
        {
            return new Tuple<decimal, string>(totalPrice * (decimal)0.001, "网签合同价*1‰");
        }

        /// <summary>
        /// 计算卖方交易手续费
        /// </summary>
        /// <param name="area"></param>
        /// <returns></returns>
        private Tuple<decimal, string> GetSellerTransactionFee(decimal area)
        {
            return new Tuple<decimal, string>(area * (decimal)2.5, "[建筑面积]*2.5元");
        }



        /// <summary>
        /// 计算卖方委托公证费
        /// </summary>
        /// <returns></returns>
        private Tuple<decimal, string> GetSellerCommissionedNotaryFee()
        {
            return new Tuple<decimal, string>(500, "固定金额");
        }
    
    }
}