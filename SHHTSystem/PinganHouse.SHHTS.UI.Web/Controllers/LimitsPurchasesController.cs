﻿using PinganHouse.SHHTS.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace PinganHouse.SHHTS.WebUI.Controllers
{
    public class LimitsPurchasesController : Controller
    {
        // GET: LimitsPurchases
        public ActionResult Index()
        {
            return View();
        }

        // GET: LimitsPurchases/Details/5
        public ActionResult Details(string id)
        {
            return View();
        }

        // GET: LimitsPurchases/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LimitsPurchases/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: LimitsPurchases/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: LimitsPurchases/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: LimitsPurchases/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: LimitsPurchases/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        // GET: LimitsPurchases/Delete/5

        public ActionResult ForeignSearch()
        {
            return View();
        }

        public ActionResult NoShanghPeopleSearch()
        {
            return View();
        }
        public ActionResult ShanghPeopleSearch()
        {
            return View();
        }
    }
}
