﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.DataTransferObjects.External;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.WebUI.Filter;
using PinganHouse.SHHTS.WebUI.Helper;
using System;
using System.Collections;
using System.ComponentModel;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace PinganHouse.SHHTS.WebUI.Controllers
{
    public partial class ParterController : Controller
    {
        /// <summary>
        /// 微信公众号号验证入口
        /// </summary>
        /// <param name="appNo"></param>
        /// <returns></returns>
        [HttpGet]
        public string AcceptMessage(string appNo)
        {
            try
            {
                return this.VerifySignature(this.HttpContext);
            }
            catch
            {
                return "";
            }

        }

        [HttpPost]
        public string AcceptMessage()
        {
            try
            {
                var result = this.CheckSource(this.HttpContext);
                if (!result.Success)
                    return "";
                XmlDocument doc = new XmlDocument();
                doc.Load(this.HttpContext.Request.InputStream);
                result = ParterProxyService.GetInstanse().AcceptMessage(Map<WXMessage>(doc));
                return result.ResultMessage;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return "";    
        }

        [HttpGet]
        public ActionResult AccessCode(string code, string state, string appNo, string returnUrl)
        {
            var result = ParterProxyService.GetInstanse().ValidateAuth2CallResult(code, appNo);
            return this.VerificationResult(result, returnUrl);   
        }

        private ActionResult VerificationResult(OperationResult result, string returnUrl)
        {
            UserHelper.SetWXUser(result.OtherData);
            if (!result.Success)
            {
                //跳转身份绑定页面
                //if(result.ResultNo==999
                Log.Info(result.ResultMessage);
                return this.Json("亲~，您尚未关注房屋交易，请联系平安好房绑定您的房屋交易，客服4008681111转1。", JsonRequestBehavior.AllowGet);
            }
            UserHelper.SetCurrentUser(result.ResultMessage, UserHelper.CurrentCustomerKey);
            return this.Redirect(returnUrl);
        }

        private string VerifySignature(HttpContextBase ctx)
        {

            var result = CheckSource(ctx);
            if (!result.Success)
                return "";
            return ctx.Request.QueryString["echostr"];
        }

        private T Map<T>(XmlDocument input) where T : new()
        {
            var result = new T();
            var resultProperties = typeof(T).GetProperties();
            foreach (var resultProperty in resultProperties)
            {
                if (!resultProperty.CanWrite)
                    continue;
                XmlNode node = input.SelectSingleNode(string.Format("/xml/{0}", resultProperty.Name));
                if (node == null || string.IsNullOrWhiteSpace(node.InnerText))
                    continue;
                var targetType = resultProperty.PropertyType;
                object val = node.InnerText;
                //unix时间戳转换为windows
                if (targetType == typeof(DateTime))
                {
                    val = node.InnerText.ConvertFromUnixTimeStamp();
                }
                else if (targetType != typeof(string))
                {
                    TypeConverter converter = TypeDescriptor.GetConverter(targetType);
                    val = converter.ConvertFromString(null, null, node.InnerText);
                }
                resultProperty.SetValue(result, val, null);
            }
            return result;
        }

        //private OperationResult CheckSource(HttpContextBase ctx)
        //{
        //    string token, aesKey;
        //    var result = ConfigServiceProxy.GetWXKeys(out token, out aesKey);
        //    if (!result.Success)
        //        return result;
        //    var queryString = ctx.Request.QueryString;
        //    var timestamp = queryString["timestamp"];
        //    var signature = queryString["signature"];
        //    var nonce = queryString["nonce"];

        //    var dic = new System.Collections.Generic.SortedDictionary<string, int>();
        //    dic[timestamp] = 1;
        //    dic[token] = 1;
        //    dic[nonce] = 1;
        //    StringBuilder sb = new StringBuilder();
        //    foreach (var d in dic)
        //    {
        //        sb.Append(d.Key);
        //    }

        //    string hash = EncryptHelper.GetSHA1(sb.ToString());
        //    if (hash == signature)
        //        return new OperationResult(0);
        //    return new OperationResult(1, "验证失败");
        //}


    }
}