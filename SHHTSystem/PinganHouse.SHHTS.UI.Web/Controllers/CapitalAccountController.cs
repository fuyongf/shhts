﻿using PinganHouse.SHHTS.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PinganHouse.SHHTS.WebUI.Controllers
{
    public class CapitalAccountController : Controller
    {
        // GET: CapitalAccount
        public ActionResult Index()
        {
            return View();
        }

        // GET: CapitalAccount/Details/5
        public ActionResult Details(string id)
        {
            CapitalAccountModel cm = new CapitalAccountModel();
            cm.BuyerPayInfo = new HouseTaxComissionMoney()
            {
                PrePayHouseMoney=300,
                ActualPayHouseMoney=280,
                BalanceHouseMoney=-20,

                PrePayTax = 20,
                ActualPayTax=20,
                BalanceTax=0,

                PrePayCommissions=10,
                ActualPayCommissions=8,
                BalanceComissions=-2
            };
            cm.SellerPayInfo = new HouseTaxComissionMoney()
            {
                PrePayHouseMoney = 600,
                ActualPayHouseMoney = 480,
                BalanceHouseMoney = -120,

                PrePayTax = 50,
                ActualPayTax = 20,
                BalanceTax = -30,

                PrePayCommissions = 18,
                ActualPayCommissions = 28,
                BalanceComissions = 10
            };
            cm.Address = "上海市浦东新区浦电路360号201室";
            cm.CaseId = "0612948596990";
            cm.NetworkContractPrice = 600;
            return View(cm);
        }

        // GET: CapitalAccount/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CapitalAccount/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: CapitalAccount/Edit/5
        public ActionResult Edit(string id)
        {
            return View();
        }

        // POST: CapitalAccount/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: CapitalAccount/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: CapitalAccount/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
