﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.DataTransferObjects.External;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.WebUI.Helper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace PinganHouse.SHHTS.WebUI.Controllers
{
    public partial class ParterController : Controller
    {
        const string replyMessageFormat = @"<xml>
                                            <Encrypt><![CDATA[{0}]]></Encrypt>
                                            <MsgSignature><![CDATA[{1}]]></MsgSignature>
                                            <TimeStamp><![CDATA[{2}]]></TimeStamp>
                                            <Nonce><![CDATA[{3}]]></Nonce>
                                          </xml>";

        /// <summary>
        /// 微信企业号验证入口
        /// </summary>
        /// <param name="appNo"></param>
        /// <returns></returns>
        [HttpGet]
        public string AcceptQYMessage(string appNo, string route)
        {
            try
            {
                var echostr = HttpUtility.UrlDecode(this.HttpContext.Request.QueryString["echostr"], Encoding.UTF8);
                return VerifyQYSignature(this.HttpContext, echostr).ResultMessage;
            }
            catch 
            {
                return "";
            }

        }

        [HttpPost]
        public string AcceptQYMessage()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(this.HttpContext.Request.InputStream);
                //校验并解密
                OperationResult result = VerifyQYSignature(this.HttpContext, doc.FirstChild["Encrypt"].InnerText);
                if (result.Success)
                {
                    string aesKey = result.OtherData["AESKey"].ToString();
                    string token = result.OtherData["Token"].ToString();
                    var message = Map<QYWXMessage>(result.ResultMessage);
                    result = ParterProxyService.GetInstanse().AcceptQYMessage(message);
                    return SetReplyMessage(result.ResultMessage, message.ToUserName, token, aesKey);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

            return "";
        }


        [HttpGet]
        public ActionResult AccessQYCode(string code, string state, string appNo, string returnUrl, int agentId)
        {
            var result = ParterProxyService.GetInstanse().ValidateAuth2CallResult(code, appNo, agentId);
            return this.VerificationQYResult(result, returnUrl);
        }


        private ActionResult VerificationQYResult(OperationResult result, string returnUrl)
        {
            if (!result.Success)
            {
                Log.Info(result.ResultMessage);
                return this.Json("亲~， 请稍后再试", JsonRequestBehavior.AllowGet);
            }

            UserHelper.SetCurrentUser(result.ResultMessage, UserHelper.CurrentAgentKey);
            return this.Redirect(returnUrl);
        }
        /// <summary>
        /// 生成回复消息密文
        /// </summary>
        /// <param name="message">消息明文</param>
        /// <param name="key">appId or CorpId</param>
        /// <param name="token">密钥</param>
        /// <param name="aesKey">编码key</param>
        /// <returns></returns>
        private string SetReplyMessage(string message, string key, string token, string aesKey)
        {
            if (string.IsNullOrWhiteSpace(message) || string.IsNullOrWhiteSpace(key) ||
                string.IsNullOrWhiteSpace(token) || string.IsNullOrWhiteSpace(aesKey))
                return "";
            var msgEncrypt = Cryptography.AESEncrypt(message, aesKey, key);
            var timestamp = TimeStamp.Get();
            var nonce = new Random(timestamp).Next();
            string sign = GenarateSinature(msgEncrypt, timestamp.ToString(), nonce.ToString(), token);
            string replyMessageFormat = @"<xml>
                                            <Encrypt><![CDATA[{0}]]></Encrypt>
                                            <MsgSignature><![CDATA[{1}]]></MsgSignature>
                                            <TimeStamp><![CDATA[{2}]]></TimeStamp>
                                            <Nonce><![CDATA[{3}]]></Nonce>
                                          </xml>";
            return string.Format(replyMessageFormat, msgEncrypt, sign, timestamp, nonce);
        }

        private T Map<T>(string xml) where T : new()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return Map<T>(doc);
        }

        private OperationResult VerifyQYSignature(HttpContextBase ctx, string msgEncrypt)
        {
            try
            {
                var result = CheckSource(ctx, true, msgEncrypt);
                if (result.Success)
                    result.ResultMessage = Cryptography.AESDecrypt(msgEncrypt, result.OtherData["AESKey"].ToString());
                return result;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-1,ex.Message);
            }
        }

        private OperationResult CheckSource(HttpContextBase ctx, bool qy=false, string msgEncrypt=null)
        {
            string token, aesKey;
            var result = ConfigServiceProxy.GetWXKeys(out token, out aesKey);
            if (!result.Success)
                return result;
            var queryString = ctx.Request.QueryString;
            var timestamp = queryString["timestamp"];
            var signature = queryString["signature"];
            var nonce = queryString["nonce"];

            //是否是企业
            if (qy)
            {               
                timestamp = HttpUtility.UrlDecode(queryString["timestamp"], Encoding.UTF8);
                signature = HttpUtility.UrlDecode(queryString["msg_signature"], Encoding.UTF8);
                nonce = HttpUtility.UrlDecode(queryString["nonce"], Encoding.UTF8);                  
            }

            string sign = GenarateSinature(msgEncrypt, timestamp, nonce, token);
            if (string.Compare(sign, signature, true) != 0)
                return new OperationResult(1, "验证失败");

            result.OtherData["AESKey"] = aesKey;
            result.OtherData["Token"] = token;
            return result;
        }

        private string GenarateSinature(string msgEncrypt, string timestamp, string nonce, string token)
        {
            var dic = new System.Collections.Generic.SortedDictionary<string, int>();
            if (!string.IsNullOrWhiteSpace(msgEncrypt))
                dic[msgEncrypt] = 1;  
            dic[timestamp] = 1;
            dic[token] = 1;
            dic[nonce] = 1;
            StringBuilder sb = new StringBuilder();
            foreach (var d in dic)
            {
                sb.Append(d.Key);
            }
            return EncryptHelper.GetSHA1(sb.ToString());
        }
    }
}