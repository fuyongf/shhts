﻿using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.WebUI.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;

namespace PinganHouse.SHHTS.WebUI.Filter
{
    public class OAuth2Filter : AuthorizeAttribute
    {
        static readonly string CallBackUrl = string.Format("http://{0}/Parter/AccessCode", ConfigurationManager.AppSettings["Host"]);// = "http://test.erpweixin.pinganhaofang.com/Parter/AccessCode";
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var user = UserHelper.GetCurrentUser();
            if (user == null || user.Count == 0)
                return false;
            return true;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            // requestUrl获取的是从负载均衡请求到iis的请求url，而不是客户端的请求，如iis并不是设置80端口（域名一样），而负载均衡仅是监听该域名的80端口。
            // var requestUrl = HttpUtility.UrlEncode(filterContext.RequestContext.HttpContext.Request.Url.AbsoluteUri);
            var url = filterContext.RequestContext.HttpContext.Request.Url;
            //负载均衡设备通过参数route=enterprise来路由不同端口的web站点（80及81）
            //去掉端口
            var requestUrl = HttpUtility.UrlEncode(string.Format("{0}://{1}{2}", url.Scheme, url.Host, url.PathAndQuery));           
            string redirectUrl = string.Format("{0}/?returnUrl={1}&appNo=weixin", CallBackUrl, requestUrl);
            var oAuth2Url = ParterProxyService.GetInstanse().GetAuthRequestUrl(redirectUrl, "weixin", "wx_Authorize");
            filterContext.HttpContext.Response.Redirect(oAuth2Url);
            base.HandleUnauthorizedRequest(filterContext);
        }
    }
}