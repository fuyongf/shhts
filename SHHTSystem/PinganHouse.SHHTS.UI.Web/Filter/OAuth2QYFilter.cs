﻿using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.WebUI.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;

namespace PinganHouse.SHHTS.WebUI.Filter
{
    public class OAuth2QYFilter : AuthorizeAttribute
    {
        static readonly string CallBackUrl = string.Format("http://{0}/Parter/AccessQYCode", ConfigurationManager.AppSettings["Host"]);// = "http://test.erpweixin.pinganhaofang.com/Parter/AccessQYCode";
        protected AgentType AgentType { get; private set; }
        public OAuth2QYFilter(AgentType agentType)
        {
            this.AgentType = agentType;
        }
       
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var user = UserHelper.GetAgentId();
            if (string.IsNullOrWhiteSpace(user))
                return false;
            return true;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            
            var url = filterContext.RequestContext.HttpContext.Request.Url;
            //负载均衡设备通过参数route=enterprise来路由不同端口的web站点（80及81）
            //去掉端口
            var requestUrl = HttpUtility.UrlEncode(string.Format("{0}://{1}{2}", url.Scheme, url.Host, url.PathAndQuery));
            string redirectUrl = string.Format("{0}/?route=enterprise&returnUrl={1}&appNo=weixin&agentId={2}", CallBackUrl, requestUrl, (int)this.AgentType);
            var oAuth2Url = ParterProxyService.GetInstanse().GetAuthRequestUrl(redirectUrl, "weixin", "wx_QYAuthorize");
            filterContext.HttpContext.Response.Redirect(oAuth2Url);
            base.HandleUnauthorizedRequest(filterContext);   
        }
    }
}