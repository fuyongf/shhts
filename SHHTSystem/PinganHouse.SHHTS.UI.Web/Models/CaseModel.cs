﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinganHouse.SHHTS.WebUI.Models
{
    public class CaseModel
    {
        //审税过户
        /// <summary>
        /// 页面状态
        /// </summary>
        private static CaseStatus auditTaxStatus = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 |
                                    CaseStatus.ZB1 | 
                                    CaseStatus.JY1 |
                                    CaseStatus.JY2 |
                                    CaseStatus.JY3 |
                                    CaseStatus.JY4 |
                                    CaseStatus.JY6 |
                                    CaseStatus.JY7 |
                                    CaseStatus.JY8 |
                                    CaseStatus.JY9 |
                                    CaseStatus.JY10 |
                                    CaseStatus.JY11 |
                                    CaseStatus.JY12 |
                                    CaseStatus.JY11A1 |
                                    CaseStatus.ZB5 |
                                    CaseStatus.ZB6 |
                                    CaseStatus.ZB7 |
                                    CaseStatus.ZB13;
        //买方贷款
        private static CaseStatus buyerStatus = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.DK1 | CaseStatus.DK2 | CaseStatus.DK3 | CaseStatus.DK4 | CaseStatus.ZB11
            | CaseStatus.DK5 | CaseStatus.DK6 | CaseStatus.DK7 | CaseStatus.DK8 | CaseStatus.DK9 | CaseStatus.DK10
            | CaseStatus.JY5 | CaseStatus.ZB2 | CaseStatus.ZB7;

        //卖方还贷
        private static CaseStatus sellerStatus = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.ZB3 | CaseStatus.HD1 | CaseStatus.HD2 | CaseStatus.HD3
            | CaseStatus.HD4 | CaseStatus.HD5 | CaseStatus.HD6 | CaseStatus.ZB12;

        #region 构造函数
        /// <summary>
        /// 构造函数
        /// </summary>
        public CaseModel()
        {
            DicEvent = new Dictionary<CaseStatus, CaseEvent>();
            NextCaseStatus =EnumExtends.GetDescription(CaseStatus.Unkown);
        }
        #endregion

        #region 案件属性
        /// <summary>
        /// 案件编号
        /// </summary>
        public string CaseId { get; set; }

        /// <summary>
        /// 产权证
        /// </summary>
        public string TenementContract { get; set; }

        /// <summary>
        /// 物业地址
        /// </summary>
        public string TenementAddress { get; set; }

        /// <summary>
        /// 权利人
        /// </summary>
        public string SellerName { get; set; }

        /// <summary>
        /// 楼名称
        /// </summary>
        public string TenementName { get; set; }

        /// <summary>
        /// 楼面积
        /// </summary>
        public decimal? CoveredArea { get; set; }

        /// <summary>
        /// 案件创建时间
        /// </summary>
        public string CreateDate { get; set; }

        /// <summary>
        /// 记录案件的当前状态
        /// </summary>
        public CaseStatus CaseStatus { get; set; }

        /// <summary>
        /// 案件当前状态
        /// </summary>
        public string CurrentCaseStatus { get; set; }

        /// <summary>
        /// 案件以下各状态状态
        /// </summary>
        public string NextCaseStatus { get; set; }

        /// <summary>
        /// 案件状态集合
        /// </summary>
        public Dictionary<CaseStatus, CaseEvent> DicEvent { get; set; }

        /// <summary>
        /// 是否已经贷款
        /// </summary>
        public bool Loaned { get; set; }

        /// <summary>
        /// 是否已经还贷款
        /// </summary>
        public bool ReLoaned { get; set; }

        #region 不同工作台案件进展
        /// <summary>
        /// 审税最新进展
        /// </summary>
        public string AuditTaxStutas { get; set; }
        /// <summary>
        /// 贷款最新进展
        /// </summary>
        public string LoanStutas { get; set; }
        /// <summary>
        /// 还贷最新进展
        /// </summary>
        public string ReLoanStutas { get; set; }
        #endregion

        #endregion

        #region 获取案件情况
        /// <summary>
        /// 获取案件情况
        /// </summary>
        public static CaseModel GetCaseModel(string caseId, string caseType)
        {
            var model=new CaseModel(){CaseId=caseId};
            try
            {
                var caseDto = new CaseDto() { CaseId = caseId };
                var caseReMarkDto = new CaseRemark();
                switch (caseType)
                {
                    case "buyer":
                        caseDto = CaseProxyService.GetInstanse().GetCaseByCaseIdAndCaseStatus(caseId,buyerStatus);                       
                        VerificationProcess(caseId,model);
                        GetNextCaseStatus(model,caseType, caseDto.CaseStatus);
                        break;
                    case "seller":
                        caseDto = CaseProxyService.GetInstanse().GetCaseByCaseIdAndCaseStatus(caseId, sellerStatus);
                        VerificationProcess(caseId, model);
                        GetNextCaseStatus(model,caseType, caseDto.CaseStatus);
                        break;
                    default:
                        caseDto = CaseProxyService.GetInstanse().GetCaseByCaseIdAndCaseStatus(caseId, auditTaxStatus);
                        VerificationProcess(caseId, model);
                        GetNextCaseStatus(model,caseType, caseDto.CaseStatus);
                        break;
                }                
                if (caseDto != null)
                {
                    model.CurrentCaseStatus =EnumExtends.GetDescription(caseDto.CaseStatus);
                    model.CaseStatus = caseDto.CaseStatus;
                    model.CaseId = caseDto.CaseId;
                    model.TenementContract = caseDto.TenementContract;
                    model.TenementAddress = caseDto.TenementAddress;
                    model.SellerName = caseDto.SellerName;
                    caseReMarkDto = CaseProxyService.GetInstanse().GetRemarkBySysNo(caseDto.SysNo);
                }
                if (caseReMarkDto != null)
                {
                    model.TenementName = caseReMarkDto.TenementName;
                    model.CoveredArea = caseReMarkDto.CoveredArea;
                }
            }
            catch (Exception) { }
            return model;
        }
        #endregion

        #region 获取案件列表
        /// <summary>
        /// 获取案件列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="searchModel"></param>
        /// <returns></returns>
        public static List<CaseModel> GetCaseModelList(int pageIndex, int pageSize,string userId,AgentType agentType,SearchModel searchModel)
        {
            var caseModelList = new List<CaseModel>();
            try
            {
                var cnt = 0;               
                var allStatus =  CaseStatus.YJ1
                    | CaseStatus.ZB2 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.HA3 | CaseStatus.HA4
                    | CaseStatus.HA5 | CaseStatus.HA6 | CaseStatus.ZB1 | CaseStatus.JY1 | CaseStatus.JY2
                    | CaseStatus.JY3 | CaseStatus.JY4 | CaseStatus.JY5 | CaseStatus.JY6 | CaseStatus.JY7
                    | CaseStatus.JY8 | CaseStatus.JY9 | CaseStatus.JY10 | CaseStatus.JY11 | CaseStatus.JY12
                    | CaseStatus.ZB3 | CaseStatus.HD1 | CaseStatus.HD2 | CaseStatus.HD3 | CaseStatus.HD4
                    | CaseStatus.HD5 | CaseStatus.HD6 | CaseStatus.DK1 | CaseStatus.DK2 | CaseStatus.DK3
                    | CaseStatus.DK4 | CaseStatus.DK5 | CaseStatus.DK6 | CaseStatus.DK7 | CaseStatus.DK8
                    | CaseStatus.DK9 | CaseStatus.DK10 | CaseStatus.ZB4 | CaseStatus.ZB5 | CaseStatus.ZB6
                    | CaseStatus.ZB7 | CaseStatus.ZB8 | CaseStatus.ZB9 | CaseStatus.ZB10 | CaseStatus.ZB11 | CaseStatus.ZB12
                    | CaseStatus.ZB13 | CaseStatus.JY11A1;


                var finishedStatus = CaseStatus.ZB13 | CaseStatus.ZB7 | CaseStatus.ZB2;

                IList<CaseDto> caseList = null;
                switch (searchModel.Status)
                {
                    case 1://进行中
                        caseList = CaseProxyService.GetInstanse().GetPaginatedList(pageIndex, pageSize, out cnt, userId
                           , agentType, allStatus, new List<CaseStatus> { auditTaxStatus, buyerStatus, sellerStatus }
                           , searchModel.CaseId, searchModel.Address, searchModel.StartTime, searchModel.EndTime
                           , searchModel.StaffPhone, searchModel.StaffName, finishedStatus);
                        break;
                    case 2://结束
                        caseList = CaseProxyService.GetInstanse().GetPaginatedList(pageIndex, pageSize, out cnt, userId
                           , agentType, finishedStatus, new List<CaseStatus> { auditTaxStatus, buyerStatus, sellerStatus }
                           , searchModel.CaseId, searchModel.Address, searchModel.StartTime, searchModel.EndTime
                           , searchModel.StaffPhone, searchModel.StaffName);
                        break;
                    default :
                        caseList = CaseProxyService.GetInstanse().GetPaginatedList(pageIndex, pageSize, out cnt, userId
                           , agentType, allStatus, new List<CaseStatus> { auditTaxStatus, buyerStatus, sellerStatus }
                           , searchModel.CaseId, searchModel.Address, searchModel.StartTime, searchModel.EndTime
                           , searchModel.StaffPhone, searchModel.StaffName);
                        break;
                }
                if (caseList != null)
                {
                    //赋值
                    foreach (var caseDto in caseList)
                    {
                        var model = new CaseModel();
                        model.CurrentCaseStatus = EnumExtends.GetDescription(caseDto.CaseStatus);
                        model.CaseId = caseDto.CaseId;
                        model.TenementContract = caseDto.TenementContract;
                        model.TenementAddress = caseDto.TenementAddress;
                        model.SellerName = caseDto.SellerName;
                        model.CreateDate = caseDto.CreateDate.ToString("yyyy-MM-dd");
                        //审税务状态
                        if (caseDto.OtherData.ContainsKey(((long)auditTaxStatus).ToString()))
                        {
                            var auditTaxStutas = Convert.ToString(caseDto.OtherData[((long)auditTaxStatus).ToString()]);
                            model.AuditTaxStutas = EnumExtends.GetDescription((CaseStatus)Enum.Parse(typeof(CaseStatus), auditTaxStutas));
                        }
                        else
                        {
                            model.AuditTaxStutas = "未知";
                        }
                        //买家贷款

                        if (caseDto.OtherData.ContainsKey(((long)buyerStatus).ToString()))
                        {
                            var loanStutas = Convert.ToString(caseDto.OtherData[((long)buyerStatus).ToString()]);
                            model.LoanStutas = EnumExtends.GetDescription((CaseStatus)Enum.Parse(typeof(CaseStatus), loanStutas));
                        }
                        else
                        {
                            model.LoanStutas = "未知";
                        }
                        //卖家贷款

                        if (caseDto.OtherData.ContainsKey(((long)sellerStatus).ToString()))
                        {
                            var reLoanStutas = Convert.ToString(caseDto.OtherData[((long)sellerStatus).ToString()]);
                            model.ReLoanStutas = EnumExtends.GetDescription((CaseStatus)Enum.Parse(typeof(CaseStatus), reLoanStutas));
                        }
                        else
                        {
                            model.ReLoanStutas = "未知";
                        }
                        caseModelList.Add(model);
                    }
                }
            }
            catch (Exception)
            {

            }
            return caseModelList;

        }
        #endregion

        #region 获取案件的下一个状态
        /// <summary>
        /// 获取案件的下一个状态
        /// </summary>
        /// <param name="caseType"></param>
        /// <param name="caseStatus"></param>
        private static void GetNextCaseStatus(CaseModel model,string caseType, CaseStatus caseStatus)
        {
            try
            {
                switch (caseType)
                {
                    case "buyer":
                        foreach (var item in dicBuyerStatus)
                        {
                            if (item.Key != caseStatus) continue;
                            model.NextCaseStatus = EnumExtends.GetDescription(item.Value);
                            if (item.Key == CaseStatus.DK8 || item.Key == CaseStatus.ZB11)
                            {
                                model.NextCaseStatus = string.Empty;
                            }
                        }
                        break;
                    case "seller":
                        foreach (var item in dicSellerStatus)
                        {
                            if (item.Key != caseStatus) continue;
                            model.NextCaseStatus = EnumExtends.GetDescription(item.Value);
                            if (item.Key == CaseStatus.HD6)
                            {
                                model.NextCaseStatus = string.Empty;
                            }
                        }
                        break;
                    default:
                        foreach (var item in dicAuditTaxStatus)
                        {
                            if (item.Key != caseStatus) continue;
                            model.NextCaseStatus = EnumExtends.GetDescription(item.Value);
                            if (item.Key == CaseStatus.ZB13 || item.Key == CaseStatus.ZB7)
                            {
                                model.NextCaseStatus = string.Empty;
                            }
                        }
                        break;
                }
            }
            catch (Exception) { }
        }

        //买方贷款
        static Dictionary<CaseStatus, CaseStatus> dicBuyerStatus = new Dictionary<CaseStatus, CaseStatus>()
        {
            //等待签约
            {CaseStatus.YJ1,CaseStatus.HA1},
            //已签约
            {CaseStatus.HA1,CaseStatus.HA2},
            //发起贷款申请
            {CaseStatus.HA2,CaseStatus.DK1},
            //发起贷款申请--等待客户签约
            {CaseStatus.DK1,CaseStatus.DK2},
            //等待客户签约--贷款合同签定
            {CaseStatus.DK2,CaseStatus.DK3},
            //贷款合同签定--贷款银行审批中
            {CaseStatus.DK3,CaseStatus.DK4},
            //贷款银行审批中--审批通过
            {CaseStatus.DK4,CaseStatus.DK5},
            //审批通过--贷款合同等送过户
            {CaseStatus.DK5,CaseStatus.DK6},
            //贷款合同等送过户--等出产证(他证)
            {CaseStatus.DK6,CaseStatus.JY5},
            //等出产证(他证)--等待银行放款
            {CaseStatus.JY5,CaseStatus.DK7},
            //等待银行放款--银行已放款
            {CaseStatus.DK7,CaseStatus.DK8},
            //银行已放款
            {CaseStatus.DK8,CaseStatus.Unkown},
            //贷款取消
            {CaseStatus.ZB11,CaseStatus.Unkown}
        };

        //卖方贷款
        static Dictionary<CaseStatus, CaseStatus> dicSellerStatus = new Dictionary<CaseStatus, CaseStatus>()
        {
            //等待签约
            {CaseStatus.YJ1,CaseStatus.HA1},
            //已签约
            {CaseStatus.HA1,CaseStatus.HA2},
            //发起还贷申请
            {CaseStatus.HA2,CaseStatus.ZB3},
            //发起还贷申请--等待预约还款
            {CaseStatus.ZB3,CaseStatus.HD1},
            //等待预约还款--已确定还款时间
            {CaseStatus.HD1,CaseStatus.HD2},
            //已确定还款时间--业主贷款已还清
            {CaseStatus.HD2,CaseStatus.HD3},
            //业主贷款已还清--业主贷款已结清
            {CaseStatus.HD3,CaseStatus.HD4},
            //业主贷款已结清--业主抵押已注销
            {CaseStatus.HD4,CaseStatus.HD5},
            //业主抵押已注销--还贷完成
            {CaseStatus.HD5,CaseStatus.HD6},
            //还贷完成
            {CaseStatus.HD6,CaseStatus.Unkown}
        };

        static Dictionary<CaseStatus, CaseStatus> dicAuditTaxStatus = new Dictionary<CaseStatus, CaseStatus>()
        {
            //等待签约
            {CaseStatus.YJ1,CaseStatus.HA1},
            //已签约
            {CaseStatus.HA1,CaseStatus.HA2},
            //发起贷款申请
            {CaseStatus.HA2,CaseStatus.ZB1},
            //已约审税限购时间--接受审税限购预约
            {CaseStatus.ZB1,CaseStatus.JY1},
            //已约审税限购时间--已核价
            {CaseStatus.JY1,CaseStatus.JY2},
            //已核价--已递交审税限购材料
            {CaseStatus.JY2,CaseStatus.JY3},
            //已递交审税限购材料--限购查询中
            {CaseStatus.JY3,CaseStatus.JY4},
            //限购查询中--等待预约过户
            {CaseStatus.JY4,CaseStatus.JY6},
            //等待预约过户--等待过户
            {CaseStatus.JY6,CaseStatus.JY8},
            //等待过户--客户已到场
            {CaseStatus.JY8,CaseStatus.JY7},
            //客户已到场--税费已缴清
            {CaseStatus.JY7,CaseStatus.JY10},
            //税费已缴清--等出产证(他证)
            {CaseStatus.JY10,CaseStatus.JY11},
            //等出产证(他证)--过户完成
            {CaseStatus.JY11,CaseStatus.JY12},
            //等出产证(他证)--过户完成
            {CaseStatus.JY11A1,CaseStatus.JY12},
            //限购--限购查询
            {CaseStatus.JY9,CaseStatus.JY4},
            //过户完成
            {CaseStatus.JY12,CaseStatus.ZB8},
            //结案
            {CaseStatus.ZB8,CaseStatus.ZB13},
            //结案-结束
            {CaseStatus.ZB13,CaseStatus.Unkown},
            //取消-结束
            {CaseStatus.ZB7,CaseStatus.Unkown}
        };
        #endregion

        #region 确定案件的状态
        /// <summary>
        /// 确定案件进行的状态
        /// </summary>
        /// <param name="auditTaxStatus"></param>
        private static void VerificationProcess(string caseId,CaseModel model)
        {
            try
            {
              var eventList= CaseProxyService.GetInstanse().GetCaseEvents(caseId,null,null);
              foreach (var item in eventList)
              {
                  if (model.DicEvent.ContainsKey(item.EventType)) continue;
                  model.DicEvent.Add(item.EventType,item);
              }
              //是否已经买家家还贷
              if (!model.DicEvent.ContainsKey(CaseStatus.DK2))
              {
                  model.Loaned = true;
              }
              //是否已经卖家还贷
              if (!model.DicEvent.ContainsKey(CaseStatus.HD1))
              {
                  model.ReLoaned = true;
              }
            }
            catch (Exception) { }
        }

        #endregion
    }


    /// <summary>
    /// 案件进度详情页
    /// </summary>
    public class CaseProcessModel
    {
        /// <summary>
        /// 所需材料
        /// </summary>
        public string Material { get; set; }

        /// <summary>
        /// 发送消息时间
        /// </summary>
        public string SendDate { get; set; }

        /// <summary>
        /// 前一流程
        /// </summary>
        public string PreProcess { get; set; }

        /// <summary>
        /// 下一流程
        /// </summary>
        public string NextProcess { get; set; }

        /// <summary>
        /// 预计下次完成时间
        /// </summary>
        public string NextDate { get; set; }

        /// <summary>
        /// 办理地址
        /// </summary>
        public string Address { get; set; }
    }
}