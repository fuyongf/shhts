﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.WebUI.Models
{
    public class BillReportModel
    {
        public OrderBizType BizType { get; set; }

        public decimal Amount { get; set; }

    }
}