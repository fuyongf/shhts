﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinganHouse.SHHTS.WebUI.Models
{
    /// <summary>
    /// 案件搜索
    /// </summary>
    public class SearchModel
    {
        /// <summary>
        /// 案件ID
        /// </summary>
        public string CaseId { get; set; }

        /// <summary>
        /// 区域
        /// </summary>
        public string Area { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 查询的状态
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime? StartTime { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime? EndTime { get; set; }

        /// <summary>
        /// 经纪人姓名
        /// </summary>
        public string StaffName { get; set; }

        /// <summary>
        /// 经纪人电话
        /// </summary>
        public string StaffPhone { get; set; }
    }
}