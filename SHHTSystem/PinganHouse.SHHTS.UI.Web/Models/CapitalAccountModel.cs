﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinganHouse.SHHTS.WebUI.Models
{
    public class CapitalAccountModel
    {
        /// <summary>
        /// 案件编号
        /// </summary>
        public string CaseId { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 网签价格
        /// </summary>
        public double NetworkContractPrice { get; set; }
        /// <summary>
        /// 买方付费信息
        /// </summary>
        public HouseTaxComissionMoney BuyerPayInfo { get; set; }
        /// <summary>
        /// 卖方付费信息
        /// </summary>
        public HouseTaxComissionMoney SellerPayInfo { get; set; }
    }

    public class HouseTaxComissionMoney
    {
        /// <summary>
        /// 预付房款
        /// </summary>
        public double PrePayHouseMoney { get; set; }

        /// <summary>
        /// 实付房款
        /// </summary>
        public double ActualPayHouseMoney { get; set; }

        /// <summary>
        /// 结余房款
        /// </summary>
        public double BalanceHouseMoney { get; set; }

        /// <summary>
        /// 预付税费
        /// </summary>
        public double PrePayTax { get; set; }

        /// <summary>
        /// 实付税费
        /// </summary>
        public double ActualPayTax { get; set; }

        /// <summary>
        /// 结余税费
        /// </summary>
        public double BalanceTax { get; set; }

        /// <summary>
        /// 预付佣金
        /// </summary>
        public double PrePayCommissions { get; set; }

        /// <summary>
        /// 实付佣金
        /// </summary>
        public double ActualPayCommissions { get; set; }

        /// <summary>
        /// 结余佣金
        /// </summary>
        public double BalanceComissions { get; set; }
    }
}