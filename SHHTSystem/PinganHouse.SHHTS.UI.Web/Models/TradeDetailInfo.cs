﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinganHouse.SHHTS.WebUI.Models
{
    public class TradeDetailInfo
    {
        private static TradeDetailInfo _instance = null;
        private static readonly object SynObject = new object();
        TradeDetailInfo() { }
        /// <summary>
        /// Gets the instance.
        /// </summary>
        public static TradeDetailInfo Instance
        {
            get
            {
                // Syn operation.
                lock (SynObject)
                {
                    return _instance ?? (_instance = new TradeDetailInfo());
                }
            }
        }

        /// <summary>
        /// 流水编号
        /// </summary>
        public string TradeNo { get; set; }

        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 案件CASEID
        /// </summary>
        public string CaseId { get; set; }
        
        /// <summary>
        /// 流水金额
        /// </summary>
        public string Amount { get; set; }

        /// <summary>
        /// 支付方式
        /// </summary>
        public string Payment { get; set; }

        /// <summary>
        /// 物业地址
        /// </summary>
        public string TenementAddress { get; set; }

        /// <summary>
        /// 支付方式
        /// </summary>
        public string OrderBizTypeDes { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public string CreateDateDes { get; set; }

        /// <summary>
        /// 流水状态
        /// </summary>
        public string Status { get; set; }

        public static List<TradeDetailInfo> GetTradeDetailInfoList(IList<TradePaginatedInfo> tradePaginatedInfoList)
        {
            var tradeDetailInfoList = new List<TradeDetailInfo>();
            try
            {
                foreach (var tradePaginatedInfo in tradePaginatedInfoList)
                {
                    var tradeDetailInfo = new TradeDetailInfo();
                    switch (tradePaginatedInfo.OrderType)
                    {
                        case PinganHouse.SHHTS.Enumerations.OrderType.Collection:
                            tradeDetailInfo.Payment = tradePaginatedInfo.AccountType == PinganHouse.SHHTS.Enumerations.AccountType.Buyer ? "买方支付" : "卖方支付";
                            break;
                        case PinganHouse.SHHTS.Enumerations.OrderType.Payment:
                            tradeDetailInfo.Payment = "转付";
                            break;
                    }
                    tradeDetailInfo.TradeNo = tradePaginatedInfo.TradeNo;
                    tradeDetailInfo.OrderNo = tradePaginatedInfo.OrderNo;
                    tradeDetailInfo.CaseId = tradePaginatedInfo.CaseId;
                    tradeDetailInfo.TenementAddress = tradePaginatedInfo.TenementAddress;
                    tradeDetailInfo.OrderBizTypeDes = PinganHouse.SHHTS.Utils.EnumExtends.GetDescription(tradePaginatedInfo.OrderBizType);
                    tradeDetailInfo.CreateDateDes = tradePaginatedInfo.CreateDate.ToString("MM-dd");
                    tradeDetailInfo.Amount =Convert.ToString(Math.Abs(tradePaginatedInfo.Amount));
                    tradeDetailInfo.Status = PinganHouse.SHHTS.Utils.EnumExtends.GetDescription(tradePaginatedInfo.Status);
                    tradeDetailInfoList.Add(tradeDetailInfo);
                }
            }
            catch (Exception)
            {

            }
            return tradeDetailInfoList;
        }
    }
}