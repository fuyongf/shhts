﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinganHouse.SHHTS.WebUI.Models
{
    public class LoanModel
    {
        /// <summary>
        /// 序号(期数)
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 贷款年
        /// </summary>
        public int Year { get; set; }
        /// <summary>
        /// 贷款月份
        /// </summary>
        public int Month { get; set; }
        
        /// <summary>
        /// 月供本金
        /// </summary>
        public double Principal { get; set; }
        
        /// <summary>
        /// 月供利息
        /// </summary>
        public double Interest { get; set; }
        
        /// <summary>
        /// 剩余贷款
        /// </summary>
        public double Loan { get; set; }


    }
}