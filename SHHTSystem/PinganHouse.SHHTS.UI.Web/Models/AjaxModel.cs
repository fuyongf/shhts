﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinganHouse.SHHTS.WebUI.Models
{
    public class AjaxModel
    {
        /// <summary>
        /// 是否有错
        /// </summary>
        public bool hasError { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        public string error { get; set; }

        /// <summary>
        /// 返回数据
        /// </summary>
        public object data { get; set; }
    }
}