﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinganHouse.SHHTS.WebUI.Models
{
    public class LimitsPurchasesModel
    {
        /// <summary>
        /// 本人2011年1月28日前与父母拥有住房
        /// </summary>
        public int SelfBeforeOwnerAndParent { get; set; }

        /// <summary>
        /// 本人2011年1月28日后与父母拥有住房
        /// </summary>
        public int SelfAfterOwnerAndParent { get; set; }

        /// <summary>
        /// 配偶2011年1月28日前与父母拥有住房
        /// </summary>
        public int SpouseBeforeOwnerAndParent { get; set; }

        /// <summary>
        /// 配偶2011年1月28日后与父母拥有住房
        /// </summary>
        public int SpouseAfterOwnerAndParent { get; set; }

        /// <summary>
        /// 本人,配偶,未成年子女佣有住房
        /// </summary>
        public int ChildrenOwner { get; set; }

        /// <summary>
        /// 现有住房套数
        /// </summary>
        public int CurrentOwner { get; set; }
        /// <summary>
        /// 客户类型
        /// </summary>
        public CustomerTypeEnum CustomerType { get; set; }

        /// <summary>
        /// 婚姻情况
        /// </summary>
        public bool IsMarried { get; set; }

        /// <summary>
        /// 缴税情况
        /// </summary>
        public bool IsTax { get; set; }

        /// <summary>
        /// 可以购买套数
        /// </summary>
        public int CanBuyNum { get; set; }


    }

    public enum CustomerTypeEnum
    {
        /// <summary>
        /// 上海人
        /// </summary>
        SH=0,
        /// <summary>
        /// 非上海人
        /// </summary>
        NSH=1,
        /// <summary>
        /// 境外人士
        /// </summary>
        FN=2
    }
}