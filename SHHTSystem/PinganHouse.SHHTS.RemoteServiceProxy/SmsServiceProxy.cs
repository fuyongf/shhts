﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using HTB.DevFx;

using PinganHouse.SHHTS.DataTransferObjects;

using PinganHouse.SHHTS.RemoteServiceProxy.Log;
using PinganHouse.SHHTS.RemoteServiceProxy.ServiceContracts;
using PinganHouse.SHHTS.RemoteServiceProxy.Utils;

namespace PinganHouse.SHHTS.RemoteServiceProxy
{
    public class SmsServiceProxy
    {
        static ISmsService SmsService
        {
            get
            {
                return ServiceHelper.GetObject<ISmsService>("SmsProxyService");
            }
        }

        /// <summary>
        /// 发送注册验证码
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public static OperationResult SendRegCode(string mobile)
        {
            try
            {
                return SmsService.SendRegCode(mobile);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 重置登陆密码验证码
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public static OperationResult SendUpdatePwdCode(string mobile)
        {
            try
            {
                return SmsService.SendUpdatePwdCode(mobile);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }
        /// 
        /// <summary>
        /// 发送客户绑定验证码短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <returns></returns>
        public static OperationResult SendBindCaptcha(string mobile)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(mobile))
                    return new OperationResult(1, "手机号码为空");
                return SmsService.SendBindCaptcha(mobile);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-1, "系统错误(-1)");
            }
        }

        /// <summary>
        /// 发送解绑短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="name"></param>
        /// <param name="unbindMobile"></param>
        /// <param name="tailNum"></param>
        /// <returns></returns>
        public static OperationResult SendUnBindMessage(string mobile, string name, string tel, string idTailNum)
        {
            try
            {
                return SmsService.SendUnBindMessage(mobile, name, tel, idTailNum);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 发送绑定短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="name"></param>
        /// <param name="unbindMobile"></param>
        /// <param name="tailNum"></param>
        /// <returns></returns>
        public static OperationResult SendBindMessage(string mobile, string name, string tel, string idTailNum)
        {
            try
            {
                return SmsService.SendBindMessage(mobile, name, tel, idTailNum);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 发送签约短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="name"></param>
        /// <param name="tel"></param>
        /// <param name="idTailNum"></param>
        /// <param name="signDate"></param>
        /// <param name="address"></param>
        /// <returns></returns>
        public static OperationResult SendSignMessage(string mobile, string name, string tel, string idTailNum, DateTime signDate, string address)
        {
            try
            {
                return SmsService.SendSignMessage(mobile, name, tel, idTailNum, signDate, address);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 发送佣金支付短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="name"></param>
        /// <param name="tel"></param>
        /// <param name="address"></param>
        /// <param name="agentFee"></param>
        /// <returns></returns>
        public static OperationResult SendPayAgentFeeMessage(string mobile, string name, string tel, string address, decimal agentFee)
        {
            try
            {
                return SmsService.SendPayAgentFeeMessage(mobile, name, tel, address, agentFee);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 发送贷款材料通知
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="name"></param>
        /// <param name="tel"></param>
        /// <returns></returns>
        public static OperationResult SendCreditMaterialNotice(string mobile, string name, string tel)
        {
            try
            {
                return SmsService.SendCreditMaterialNotice(mobile, name, tel);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 发送交易材料通知
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="name"></param>
        /// <param name="tel"></param>
        /// <returns></returns>
        public static OperationResult SendTradeMaterialNotice(string mobile, string name, string tel)
        {
            try
            {
                return SmsService.SendTradeMaterialNotice(mobile, name, tel);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 发送立案预约短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="name"></param>
        /// <param name="tel"></param>
        /// <param name="date"></param>
        /// <param name="center"></param>
        /// <returns></returns>
        public static OperationResult SendCaseArrangeMessage(string mobile, string name, string tel, DateTime date, string center)
        {
            try
            {
                return SmsService.SendCaseArrangeMessage(mobile, name, tel, date, center);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 发送过户预约短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="name"></param>
        /// <param name="tel"></param>
        /// <param name="date"></param>
        /// <param name="center"></param>
        /// <returns></returns>
        public static OperationResult SendTransferArrangeMessage(string mobile, string name, string tel, DateTime date, string center)
        {
            try
            {
                return SmsService.SendTransferArrangeMessage(mobile, name, tel, date, center);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 发送票据寄出通知
        /// </summary>
        /// <param name="mobile"></param>
        /// <returns></returns>
        public static OperationResult SendPostBillNotice(string mobile)
        {
            try
            {
                return SmsService.SendPostBillNotice(mobile);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 发送收款通知
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="date"></param>
        /// <param name="bankCardTailNum"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public static OperationResult SendCollectionNotice(string mobile, DateTime date, string bankCardTailNum, decimal amount)
        {
            try
            {
                return SmsService.SendCollectionNotice(mobile, date, bankCardTailNum, amount);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 发送代扣短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public static OperationResult SendEntrustPayNotice(string mobile, decimal amount)
        {
            try
            {
                return SmsService.SendEntrustPayNotice(mobile, amount);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 发送转账通知
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="amount"></param>
        /// <param name="bankCardTailNum"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public static OperationResult SendWithdrawNotice(string mobile, decimal amount, string bankCardTailNum, DateTime date)
        {
            try
            {
                return SmsService.SendWithdrawNotice(mobile, amount, bankCardTailNum, date);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 发送立案确认短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="name"></param>
        /// <param name="name1"></param>
        /// <param name="tel"></param>
        /// <returns></returns>
        public static OperationResult SendConfirmCaseMessage(string mobile, string name, string name1, string tel)
        {
            try
            {
                return SmsService.SendConfirmCaseMessage(mobile, name, name1, tel);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 发送还贷预约短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public static OperationResult SendRepayArrangeMessage(string mobile, DateTime date)
        {
            try
            {
                return SmsService.SendRepayArrangeMessage(mobile, date);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 发送限购结果短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static OperationResult SendLimitBuyMessage(string mobile, string result)
        {
            try
            {
                return SmsService.SendLimitBuyMessage(mobile, result);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 发送交房通知
        /// </summary>
        /// <param name="mobile"></param>
        /// <returns></returns>
        public static OperationResult SendDeliveryHouseNotice(string mobile)
        {
            try
            {
                return SmsService.SendDeliveryHouseNotice(mobile);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 发送领产证通知
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="address"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public static OperationResult SendGetCredentialNotice(string mobile, string address, string date)
        {
            try
            {
                return SmsService.SendGetCredentialNotice(mobile, address, date);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 发送交易过户办理通知
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="address"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public static OperationResult SendHandleTransferNotice(string mobile, string address, string date, string center, string name, string tel)
        {
            try
            {
                return SmsService.SendHandleTransferNotice(mobile, address, date, center, name, tel);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 发送贷款审批通过通知
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="name"></param>
        /// <param name="tel"></param>
        /// <returns></returns>
        public static OperationResult SendCreditPassNotice(string mobile, string name, string tel)
        {
            try
            {
                return SmsService.SendCreditPassNotice(mobile, name, tel);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 发送还贷预约2短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="amount"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public static OperationResult SendRepayArrange2Message(string mobile, decimal amount, DateTime date)
        {
            try
            {
                return SmsService.SendRepayArrange2Message(mobile, amount, date);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 发送还贷预约3短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="amount"></param>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <returns></returns>
        public static OperationResult SendRepayArrange3Message(string mobile, decimal amount, DateTime date1, DateTime date2)
        {
            try
            {
                return SmsService.SendRepayArrange3Message(mobile, amount, date1, date2);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 自行预约还贷3短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="amount"></param>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <returns></returns>
        public static OperationResult SendSelfRepay3Message(string mobile, decimal amount, DateTime date1, DateTime date2)
        {
            try
            {
                return SmsService.SendSelfRepay3Message(mobile, amount, date1, date2);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 自行预约还贷2短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="amount"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public static OperationResult SendSelfRepay2Message(string mobile, decimal amount, DateTime date)
        {
            try
            {
                return SmsService.SendSelfRepay2Message(mobile, amount, date);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 自行预约还贷1短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="amount"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public static OperationResult SendSelfRepayMessage(string mobile, DateTime date)
        {
            try
            {
                return SmsService.SendSelfRepayMessage(mobile, date);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }
    }
}
