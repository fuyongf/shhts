﻿using HTB.DevFx;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy.Log;
using PinganHouse.SHHTS.RemoteServiceProxy.ServiceContracts;
using PinganHouse.SHHTS.RemoteServiceProxy.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.RemoteServiceProxy
{
    /// <summary>
    /// 案件代理类
    /// </summary>
    public class CaseProxyService
    {
        readonly static CaseProxyService caseProxyService = new CaseProxyService();
        private CaseProxyService()
        { }

        protected ICaseService CaseService
        {
            get { return ServiceHelper.GetObject<ICaseService>("CaseRemoteService"); }
        }

        public static CaseProxyService GetInstanse()
        {
            return caseProxyService;
        }

        public OperationResult GenerateCase(CaseDto caseInfo)
        {
            try
            {
                CheckCase(caseInfo);
                return CaseService.GenerateCase(caseInfo);
            }
            catch (ArgumentException e)
            {
                return new OperationResult(2, e.Message);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return new OperationResult(-1, "系统内部错误");
            }

        }

        private bool CheckCase(CaseDto caseInfo)
        {
            if (caseInfo == null)
                throw new ArgumentException("case is null");
            if (caseInfo.ReceptionCenter <= 0)
                throw new ArgumentException("reception center is invalid");
            if (caseInfo.Buyers == null || caseInfo.Buyers.Count == 0)
                throw new ArgumentException("buyer is null");
            if (caseInfo.Sellers == null || caseInfo.Sellers.Count == 0)
                throw new ArgumentException("seller is null");
            if (caseInfo.AgencySysNo <= 0)
                throw new ArgumentException("agency is null");
            if (string.IsNullOrWhiteSpace(caseInfo.TenementContract))
                throw new ArgumentException("TenementContract is null");
            if (string.IsNullOrWhiteSpace(caseInfo.TenementAddress))
                throw new ArgumentException("TenementAddress is null");
            return true;
        }

        /// <summary>
        /// 获取案件
        /// </summary>
        /// <param name="caseId">案件编号</param>
        /// <returns></returns>
        public CaseDto GetCaseByCaseId(string caseId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(caseId))
                    return null;
                return CaseService.GetCaseByCaseId(caseId);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return null;
            }

        }


        /// <summary>
        /// 获取案件
        /// </summary>
        /// <param name="caseId">案件编号</param>
        /// <returns></returns>
        public CaseDto GetCaseByCaseId(string caseId, ReceptionCenter receptionCenter)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(caseId) || receptionCenter == ReceptionCenter.Unkonw)
                    return null;
                return CaseService.GetCaseByCaseId(caseId, receptionCenter);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return null;
            }

        }

        /// <summary>
        /// 获取案件，包含上下家信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="receptionCenter"></param>
        /// <returns></returns>
        public CaseWithCustomer GetCaseWithCustomer(string caseId, ReceptionCenter receptionCenter)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(caseId) || receptionCenter == ReceptionCenter.Unkonw)
                    return null;
                return CaseService.GetCaseWithCustomer(caseId, receptionCenter);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return null;
            }
        }

        public CaseDto GetCaseAndProcess(string caseId, CaseStatus caseStatus, ref Dictionary<CaseStatus, Tuple<string, DateTime>> processes)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(caseId) || caseStatus == CaseStatus.Unkown)
                    return null;
                return CaseService.GetCaseAndProcess(caseId, caseStatus, out processes);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 获取案件信息，包含案件状态
        /// </summary>
        /// <param name="caseId">案件id</param>
        /// <param name="caseStatus">案件状态</param>
        /// <returns></returns>
        public CaseDto GetCaseByCaseIdAndCaseStatus(string caseId, CaseStatus caseStatus)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(caseId) || caseStatus == CaseStatus.Unkown)
                    return null;
                return CaseService.GetCaseByCaseIdAndCaseStatus(caseId, caseStatus);

            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return null;
            }
        }

        public CaseDto GetCase(long sysno)
        {
            try
            {
                if (sysno <= 0)
                    return null;
                return CaseService.GetCase(sysno);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 是否有效案件存在物业标识
        /// </summary>
        /// <param name="tenementContract"></param>
        /// <returns></returns>
        public bool ExistTenementContract(string tenementContract)
        {
            try
            {
                return CaseService.ExistTenementContract(tenementContract);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                throw new Exception("系统错误");
            }
        }

        [Obsolete]
        public OperationResult UpdateCaseStatus(string caseId, CaseStatus caseStatus, long modifyUserSysNo, Dictionary<string, object> data = null)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(caseId) || caseStatus == CaseStatus.Unkown)
                    return new OperationResult(1, "parameter is invalid");
                return CaseService.UpdateCaseStatus(caseId, caseStatus, modifyUserSysNo, data);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return new OperationResult(-1, "系统错误");
            }
        }

        public OperationResult UpdateCaseStatus(string caseId, string action, long modifyUserSysNo, Dictionary<string, object> otherData = null)
        {
            try
            {
                return CaseService.UpdateCaseStatus(caseId, action, modifyUserSysNo, otherData);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return new OperationResult(-1, "系统错误");
            }
        }

        /// <summary>
        /// 获取案件跟进信息列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="receptionCenter">所属业务受理中心</param>
        /// <param name="operationStatus">所需状态信息</param>
        /// <param name="categorys">状态类别列表</param>
        /// <param name="proccessType">案件进度</param>
        /// <param name="startDate">开始时间</param>
        /// <param name="lastTrailer">最后跟进人</param>
        /// <returns></returns>
        public IList<CaseTrailPaged> GetCaseTrailPaged(int pageIndex, int pageSize, out int totalCount, ReceptionCenter receptionCenter, CaseStatus operationStatus, IEnumerable<CaseStatus> categorys = null, ProccessType? proccessType = null, int? days = null, string lastTrailer = null, Dictionary<string, object> data = null)
        {
            totalCount = 0;
            try
            {
                DateTime? startDate = null;
                if (days != null)
                    startDate = DateTime.Now.AddDays(-days.Value);
                return CaseService.GetCaseTrailPaged(pageIndex, pageSize, out totalCount, receptionCenter, operationStatus, categorys, proccessType, startDate, lastTrailer, data);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return null;
            }
        }



        public OperationResult Update(string caseId, List<long> buyers, List<long> sellers, long agencySysNo, long modifyUserSysNo, string tenementAddress = null, string tenementContract = null, Dictionary<string, object> data = null)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(caseId))
                    return new OperationResult(1, "caseId is invalid");
                if (buyers == null || buyers.Count == 0)
                    return new OperationResult(2, "buyers is invalid");
                if (sellers == null || sellers.Count == 0)
                    return new OperationResult(3, "sellers is invalid");
                return CaseService.Update(caseId, buyers, sellers, agencySysNo, modifyUserSysNo, tenementAddress, tenementContract, data);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return new OperationResult(-1, "系统错误");
            }
        }

        public OperationResult DeleteCaseByCaseId(string caseId, string modifyUserNo)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(caseId))
                    return new OperationResult(1, "caseId is null");
                return CaseService.DeleteCaseByCaseId(caseId, modifyUserNo);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return new OperationResult(-1, "系统内部错误");
            }
        }


        /// <summary>
        /// 写跟进
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="proccessType"></param>
        /// <param name="remark"></param>
        /// <param name="createUserSysNo"></param>
        /// <returns></returns>
        public OperationResult SetCaseTrail(string caseId, ProccessType proccessType, string remark, long createUserSysNo)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(caseId))
                    return new OperationResult(1, "caseId is null");
                var caseTrail = new CaseTrailDto { CaseId = caseId, CreateUserSysNo = createUserSysNo, ProccessType = proccessType, Remark = remark };
                return CaseService.SetCaseTrail(caseTrail);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return new OperationResult(-1, "系统内部错误");
            }
        }
        /// <summary>
        /// 获取案件列表
        /// </summary>
        /// <param name="pageIndex">当前页数</param>
        /// <param name="pageSize">页行数</param>
        /// <param name="totalCount">总数</param>
        /// <param name="receptionCenter">所属管理中心</param>
        /// <param name="allCaseStatus">所属状态组</param>
        /// <param name="caseStatus">所需查询查询状态</param>
        /// <param name="caseId">案件编号</param>
        /// <param name="tenementAddress">物业地址</param>
        /// <param name="buyerName">买家姓名</param>
        /// <param name="agencyName">中介姓名</param>
        /// <param name="startDate">开始时间</param>
        /// <param name="endDate">结束时间</param>
        /// <param name="operationStatus">所需状态的信息</param>
        /// <param name="finished">是否案件事件完结</param>
        /// <param name="belongUserSysNo">案件所属人</param>
        /// <param name="topStatus">类别中事件初始状态（未被接单）</param>
        /// <param name="operatorName">受理人（接单人） 如需根据受理人查询案件，请传递topStatus</param>
        /// <param name="creatorName">creatorName 案件创建者</param>
        /// <returns></returns>
        [Obsolete]
        public IList<CaseDto> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, ReceptionCenter receptionCenter, CaseStatus allCaseStatus, CaseStatus? caseStatus = null, string caseId = null, string tenementAddress = null, string buyerName = null, string agencyName = null,
             DateTime? startDate = null, DateTime? endDate = null, CaseStatus? operationStatus = null, bool? finished = null, long? belongUserSysNo = null, CaseStatus? topStatus = null, string sellerName = null, string operatorName = null, string creatorName = null)
        {
            totalCount = 0;
            try
            {
                var dic = new Dictionary<string, object>();
                if (!string.IsNullOrWhiteSpace(tenementAddress))
                    dic["tenementAddress"] = tenementAddress;
                if (!string.IsNullOrWhiteSpace(buyerName))
                    dic["buyerName"] = buyerName;
                if (!string.IsNullOrWhiteSpace(agencyName))
                    dic["agencyName"] = agencyName;
                if (!string.IsNullOrWhiteSpace(sellerName))
                    dic["sellerName"] = sellerName;
                if (!string.IsNullOrWhiteSpace(operatorName))
                    dic["operatorName"] = operatorName;
                if (!string.IsNullOrWhiteSpace(creatorName))
                    dic["creatorName"] = creatorName;
                return CaseService.GetPaginatedList(pageIndex, pageSize, out totalCount, receptionCenter, allCaseStatus, belongUserSysNo, topStatus, caseStatus, caseId, startDate, endDate, operationStatus, finished, dic);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return null;
            }
        }

        [Obsolete]
        public IList<CaseDto> GetCases(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, QueryStringType? queryStringType = null, string queryString = null)
        {
            totalCount = 0;
            try
            {
                return CaseService.GetCases(pageIndex, pageSize, out totalCount, center, queryStringType, queryString);

            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 获取案件列表.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="totalCount">The total count.</param>
        /// <returns></returns>
        public IList<CaseDto> GetPaginatedList(PinganHouse.SHHTS.DataTransferObjects.Filters.CaseFilter filter, int pageIndex, int pageSize, out int totalCount)
        {
            return CaseService.GetPaginatedList(filter, pageIndex, pageSize, out totalCount);
        }

        /// <summary>
        /// 获取经纪人或者经纪公司名下案件
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="userId"></param>
        /// <param name="agentType"></param>
        /// <param name="allCaseStatus"></param>
        /// <param name="caseId"></param>
        /// <param name="tenementAddress"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        [Obsolete]
        public IList<CaseDto> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, string userId, AgentType agentType, CaseStatus allCaseStatus, IEnumerable<CaseStatus> categorys, string caseId = null, string tenementAddress = null,
                    DateTime? startDate = null, DateTime? endDate = null, string mobilePhone = null, string agentName = null, CaseStatus? excludeCaseStatus = null)
        {
            totalCount = 0;
            try
            {
                var data = new Dictionary<string, object>();
                if (!string.IsNullOrWhiteSpace(mobilePhone))
                    data["MoblePhone"] = mobilePhone;
                if (!string.IsNullOrWhiteSpace(agentName))
                    data["AgentName"] = agentName;
                if (allCaseStatus == CaseStatus.Unkown)
                    return null;
                if (startDate != null && endDate != null && startDate.Value > endDate.Value)
                    return null;
                if (string.IsNullOrWhiteSpace(userId))
                    return null;
                return CaseService.GetPaginatedList(pageIndex, pageSize, out totalCount, userId, agentType, allCaseStatus, categorys, caseId, tenementAddress, startDate, endDate, data, excludeCaseStatus);

            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 根据案件编号获取案件事件信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public IList<CaseEvent> GetCaseEvents(string caseId, int pageIndex, int pageSize, out int totalCount, params CaseStatus[] status)
        {
            totalCount = 0;
            try
            {
                return CaseService.GetCaseEvents(caseId, pageIndex, pageSize, out totalCount, status);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        public IList<CaseEvent> GetCaseEvents(string caseId, CaseStatus? currentCaseStatus, CaseStatus? preCaseStatus)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(caseId))
                    return null;
                return CaseService.GetCaseEvents(caseId, currentCaseStatus, preCaseStatus);

            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.ToString());
                return null;
            }
        }

        [Obsolete]
        public OperationResult UpdateCaseEvent(long sysno, Dictionary<string, object> dic, long modifyUserSysNo)
        {
            if (sysno == 0 || dic == null)
                return new OperationResult(1, "param is invalid");
            try
            {
                return CaseService.UpdateCaseEvent(sysno, dic, modifyUserSysNo);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.ToString());
                return new OperationResult(-2, "系统内部错误");
            }
        }

        public CaseAttachmentDto GetCaseAttenment(string caseId)
        {
            if (string.IsNullOrWhiteSpace(caseId))
                return null;
            try
            {
                return CaseService.GetCaseAttenment(caseId);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 获取产证号及物业地址
        /// key 产证号
        /// value 物业地址
        /// </summary>
        /// <param name="queryStringType"></param>
        /// <param name="queryString"></param>
        /// <param name="data"></param>
        /// <returns></returns>

        public IDictionary<string, string> GetTenementContracts(QueryStringType queryStringType, string queryString, ReceptionCenter center, Dictionary<string, object> data = null)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(queryString) || center == ReceptionCenter.Unkonw)
                    return null;
                return CaseService.GetTenementContracts(queryStringType, queryString, center, data);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 完善买家信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="customer"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        public OperationResult PerfectBuyer(string caseId, Buyer buyer, IDictionary<long, string> mobiles, IDictionary<long, string> emails, long operatorSysNo)
        {
            try
            {
                return CaseService.PerfectBuyer(caseId, buyer, mobiles, emails, operatorSysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统异常");
            }
        }

        /// <summary>
        /// 完善卖家信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="customer"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        public OperationResult PrefectSeller(string caseId, Seller seller, IDictionary<long, string> mobiles, IDictionary<long, string> emails, long operatorSysNo)
        {
            try
            {
                return CaseService.PrefectSeller(caseId, seller, mobiles, emails, operatorSysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统异常");
            }
        }

        /// <summary>
        /// 获取买家信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public Buyer GetBuyer(string caseId)
        {
            try
            {
                return CaseService.GetBuyer(caseId);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 获取卖家信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public Seller GetSeller(string caseId)
        {
            try
            {
                return CaseService.GetSeller(caseId);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 获取案件上下家信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public Tuple<Seller, Buyer> GetCaseCustomers(string caseId)
        {
            try
            {
                return CaseService.GetCaseCustomers(caseId);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 模糊查询案件编号
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageCount"></param>
        /// <param name="totalCount"></param>
        /// <param name="center"></param>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public IList<string> GetCaseIds(int pageIndex, int pageCount, out int totalCount, ReceptionCenter center, string caseId)
        {
            try
            {
                return CaseService.GetCaseIds(pageIndex, pageCount, out totalCount, center, caseId);
            }
            catch (Exception e)
            {
                totalCount = 0;
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        #region Remark
        /// <summary>
        /// 完善案件扩展信息
        /// </summary>
        /// <param name="caseSysNo"></param>
        /// <param name="remark"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        public OperationResult PerfectCaseInfo(long caseSysNo, CaseRemarkModel remark, long? operatorSysNo)
        {
            if ((remark.IsFundTrusteeship != null && remark.IsFundTrusteeship.Value && string.IsNullOrWhiteSpace(remark.FtContractNo)) ||
                (remark.IsCommissionTrusteeship != null && remark.IsCommissionTrusteeship.Value && string.IsNullOrWhiteSpace(remark.CtContractNo)))
                return new OperationResult(-1, "托管合同号不能为空");
            var caseRemark = new CaseRemark
            {
                SysNo = caseSysNo,
                TenementName = remark.TenementName,
                LoopLinePosition = remark.LoopLinePosition,
                CoveredArea = remark.CoveredArea,
                RoomCount = remark.RoomCount,
                HallCount = remark.HallCount,
                ToiletCount = remark.ToiletCount,
                FloorCount = remark.FloorCount,
                BuiltCompletedDate = remark.BuiltCompletedDate,
                MediatorPrice = remark.MediatorPrice,
                NetlabelPrice = remark.NetlabelPrice,
                RealPrice = remark.RealPrice,
                IsLowPrice = remark.IsLowPrice,
                TradeType = remark.TradeType,
                TenementNature = remark.TenementNature,
                ResideType = remark.ResideType,
                FirstTimeBuy = remark.FirstTimeBuy,
                PurchaseYears = remark.PurchaseYears,
                OnlyHousing = remark.OnlyHousing,
                CarportAddress = remark.CarportAddress,
                CarportArea = remark.CarportArea,
                CarportPrice = remark.CarportPrice,
                IsRepairLandPrice = remark.IsRepairLandPrice,
                IsNSAApproval = remark.IsNSAApproval,
                IsProtectiveBuilding = remark.IsProtectiveBuilding,
                IsNotarize = remark.IsNotarize,
                Notary = remark.Notary,
                NotaryContact = remark.NotaryContact,
                BuyOverYears = remark.BuyOverYears,
                CarportRawPrice = remark.CarportRawPrice,
                TenementRawPrice = remark.TenementRawPrice,
                NotarySysNo = remark.NotarySysNo,

                CreateDate = DateTime.Now,
                CreateUserSysNo = operatorSysNo
            };
            try
            {
                return CaseService.PerfectCaseInfo(caseRemark,
                    remark.IsFundTrusteeship, remark.FtContractNo, remark.IsCommissionTrusteeship, remark.CtContractNo,
                    remark.TrusteeshipAccounts);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
            }
            return new OperationResult(-5, "系统错误");
        }

        [Obsolete]
        public OperationResult ModifyNetlabelPrice(long caseSysNo, decimal price, long operatorSysNo)
        {
            var caseRemark = new CaseRemark
            {
                SysNo = caseSysNo,
                NetlabelPrice = price,
                CreateDate = DateTime.Now,
                CreateUserSysNo = operatorSysNo
            };
            try
            {
                return CaseService.PerfectCaseInfo(caseRemark);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
            }
            return new OperationResult(-5, "系统错误");
        }
        /// <summary>
        /// 根据案件系统编号获取扩展信息
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        public CaseRemark GetRemarkBySysNo(long sysNo)
        {
            try
            {
                return CaseService.GetRemarkBySysNo(sysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
            }
            return null;
        }

        /// <summary>
        /// 根据案件系统编号获取客户托管资金帐号
        /// </summary>
        /// <param name="caseSysNo"></param>
        /// <param name="customerType"></param>
        /// <param name="accountType"></param>
        /// <returns></returns>
        public IEnumerable<CustomerBankAccount> GetCustomerBankAccounts(long caseSysNo, CustomerType? customerType = null, BankAccountPurpose? accountType = null)
        {
            try
            {
                return CaseService.GetCustomerBankAccounts(caseSysNo, customerType, accountType);
            }
            catch (Exception e)
            {
                Log.LogHandler.Write(e.ToString());
                throw;
            }
        }

        public CaseRemark GetRemark(string tenementContract, ReceptionCenter center, Dictionary<string, object> data = null)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(tenementContract) || center == ReceptionCenter.Unkonw)
                    return null;
                return CaseService.GetRemark(tenementContract, center, data);
            }
            catch (Exception e)
            {
                Log.LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 添加客户银行账户信息
        /// </summary>
        /// <param name="caseId">案件编号</param>
        /// <param name="type">客户类型</param>
        /// <param name="purpose">账户用途</param>
        /// <param name="bankCode">银行编码</param>
        /// <param name="bankName">银行名称</param>
        /// <param name="subBankName">支行名称</param>
        /// <param name="bankAccount">卡号|帐号</param>
        /// <param name="accountName">开户名</param>
        /// <param name="isMainAcccount">是否是主帐号</param>
        /// <param name="operatorSysNo">操作人</param>
        /// <param name="needAuth">是否需要鉴权</param>
        /// <param name="identityNo">身份证号，如<see cref="needAuth"/>为false，可不传</param>
        /// <param name="mobile">手机号，如<see cref="needAuth"/>为false或非四要素鉴权，可不传</param>
        /// <returns></returns>
        public OperationResult AddCustomerBankAccount(string caseId, CustomerType type, IEnumerable<BankAccountPurpose> purpose, string bankCode, string bankName, string subBankName, string bankAccount, string accountName, bool isMainAcccount, long operatorSysNo, bool needAuth = false, string identityNo = null, string mobile = null)
        {
            try
            {
                if (purpose == null || purpose.Count() == 0)
                    return new OperationResult(-1, "账户用途错误");
                if (string.IsNullOrWhiteSpace(bankCode) || string.IsNullOrWhiteSpace(bankName) || string.IsNullOrWhiteSpace(subBankName) || string.IsNullOrWhiteSpace(accountName))
                    return new OperationResult(-2, "账户信息错误");
                if (needAuth && string.IsNullOrWhiteSpace(identityNo))
                    return new OperationResult(-3, "身份证不能为空");
                return CaseService.AddCustomerBankAccount(caseId, type, purpose, bankCode, bankName, subBankName, bankAccount, accountName, isMainAcccount, operatorSysNo, needAuth, identityNo, mobile);
            }
            catch (Exception e)
            {
                Log.LogHandler.Write(e.ToString());
                throw;
            }
        }
        #endregion
    }
}
