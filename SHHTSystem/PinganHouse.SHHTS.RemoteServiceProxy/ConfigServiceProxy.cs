﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using HTB.DevFx;

using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.DataTransferObjects;

using PinganHouse.SHHTS.RemoteServiceProxy.Log;
using PinganHouse.SHHTS.RemoteServiceProxy.ServiceContracts;
using PinganHouse.SHHTS.RemoteServiceProxy.Utils;

namespace PinganHouse.SHHTS.RemoteServiceProxy
{
    public class ConfigServiceProxy
    {
        static IConfigService ConfigService
        {
            get
            {
                return ServiceHelper.GetObject<IConfigService>("ConfigRemoteService");
            }
        }

        /// <summary>
        /// 获取银行列表
        /// </summary>
        /// <returns></returns>
        public static IDictionary GetBanks()
        {
            try
            {
                return ConfigService.GetBanks();
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 获取贷款银行列表
        /// </summary>
        /// <returns></returns>
        public static IDictionary GetBankDic()
        {
            try
            {
                return ConfigService.GetBankDic();
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 获取鉴权银行列表
        /// </summary>
        /// <returns></returns>
        [Obsolete("use GetIdentifyBankCfgs instead")]
        public static IDictionary GetIdentifyBanks() 
        {
            try
            {
                return ConfigService.GetIdentifyBanks();
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 获取鉴权银行配置列表
        /// </summary>
        /// <returns></returns>
        public static IList<BankIdentifyCfg> GetIdentifyBankCfgs()
        {
            try
            {
                return ConfigService.GetIdentifyBankCfgs();
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 根据银行简码获取银行名称
        /// </summary>
        /// <param name="bankCode"></param>
        /// <returns></returns>
        public static string GetBankNameByCode(string bankCode)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(bankCode))
                    return null;
                return ConfigService.GetBankNameByCode(bankCode);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 获取公积金管理中心地址
        /// </summary>
        /// <returns></returns>
        public static IList<string> GetProvidentFundAddress() 
        {
            try
            {
                return ConfigService.GetProvidentFundAddress();
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return null;
            }
        }


        public static string GetMaterial(string materialPath)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(materialPath))
                    return string.Empty;
                return ConfigService.GetMaterial(materialPath);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return string.Empty;
            }
        }

        public static string GetReceptionCenter(string receptionCenter)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(receptionCenter))
                    return string.Empty;
                return ConfigService.GetReceptionCenter(receptionCenter);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return string.Empty;
            }
        }

        /// <summary>
        /// 获取贷款专员
        /// </summary>
        /// <returns></returns>
        public static IList<string> GetCommissioners()
        {
            try
            {
                return ConfigService.GetCommissioners();
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 获取省市区信息
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static IList<string> GetRegionInfo(string path = "")
        {
            return ConfigService.GetRegionInfo(path);
        }

        /// <summary>
        /// 获取自动更新地址
        /// </summary>
        /// <returns></returns>
        public static string GetAutoUpdateUrl()
        {
            try
            {
                return ConfigService.GetAutoUpdateUrl();
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return null;
            }
        }
        public static string GetNewAutoUpdateUrl()
        {
            try
            {
                return ConfigService.GetNewAutoUpdateUrl();
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        public static OperationResult GetWXKeys(out string token, out string aesKey, string appNo=null)
        {
            token = aesKey = string.Empty;
            try
            {
                var key = ConfigService.GetWXKeys(appNo);
                if (key == null)
                    return new OperationResult(1, "未存在相关配置");
                token = key.Item1;
                aesKey = key.Item2;
                return new OperationResult(0);

            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-1, "系统内部错误");
            }           
        }

        /// <summary>
        /// 获取客户资金变更条件
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static IDictionary<string, IList<string>> GetChangeFundCondition(ChangeFundType type)
        {
            return ConfigService.GetChangeFundCondition(type);
        }

        ///// <summary>
        ///// 根据订单业务类型获取资金走向
        ///// </summary>
        ///// <param name="tradeType"></param>
        ///// <returns></returns>
        //public FundDirection GetFundDirection(OrderBizType tradeType)
        //{
        //    return ConfigService.GetFundDirection(tradeType);
        //}

        #region 打印相关
        /// <summary>
        /// 获取E房钱打印模版
        /// </summary>
        /// <returns></returns>
        public static IList<PrintTemplate> GetEFangqianPrintTemplates() 
        {
            try
            {
                return ConfigService.GetEFangqianPrintTemplates();
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                throw;
            }
        }

        /// <summary>
        /// 获取打印模版信息
        /// </summary>
        /// <param name="templateId">模版编号</param>
        /// <param name="pageNo">页码</param>
        /// <returns></returns>
        public static Dictionary<string, PrintTemplateValue> GetPrintTemplate(string templateId, string pageNo) 
        {
            try
            {
                return ConfigService.GetPrintTemplate(templateId, pageNo);
            }
            catch (Exception e) 
            {
                LogHandler.Write(e.ToString());
                throw;
            }
        }
        #endregion

        /// <summary>
        /// 获取二手房交易管理系统关于信息
        /// </summary>
        /// <returns></returns>
        public static string GetSHHTMSAbooutInfo() 
        {
            try
            {
                return ConfigService.GetSHHTMSAbooutInfo();
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                throw;
            }
        }
    }
}
