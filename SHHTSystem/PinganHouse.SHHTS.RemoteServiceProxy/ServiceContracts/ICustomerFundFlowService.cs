﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ServiceModel;

using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.RemoteServiceProxy.ServiceContracts
{
    [ServiceContract]
    public interface ICustomerFundFlowService
    {
        /// <summary>
        /// 添加资金流转信息
        /// </summary>
        /// <param name="flow"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult AddFundFlow(CustomerFundFlow flow);

        /// <summary>
        /// 获取客户资金流转信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="customerType"></param>
        /// <returns></returns>
        [OperationContract]
        IList<CustomerFundFlow> GetFundFlows(string caseId, CustomerType? customerType = null);

        /// <summary>
        /// 删除客户资金流转信息
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult DeleteFundFlow(long sysNo, long operatorSysNo);
    }
}
