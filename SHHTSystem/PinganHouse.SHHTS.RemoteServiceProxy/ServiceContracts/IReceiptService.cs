﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace PinganHouse.SHHTS.RemoteServiceProxy.ServiceContracts
{
    [ServiceContract(Name = "TradeService")]
    public interface IReceiptService
    {
        /// <summary>
        /// 根据订单编号获取可打印收据的流水
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        [OperationContract]
        IList<ReceiptTradeInfo> GetPrintReceiptTradesByOrder(string orderNo);

        /// <summary>
        /// 保存票据内容
        /// </summary>
        /// <param name="trades"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult GenerateReceipt(string orderNo, long createUserSysNo, out ReceiptView receiptDto, IDictionary<string, object> data = null, params string[] tradeNos);

        /// <summary>
        /// 预览票据内容
        /// </summary>
        /// <param name="trades"></param>
        /// <returns></returns>
        [OperationContract]
        ReceiptView PreviewReceipt(string orderNo, params string[] tradeNos);

        /// <summary>
        /// 预览票据
        /// </summary>
        /// <param name="receiptSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        ReceiptView GetReceiptInfo(long receiptSysNo);

        /// <summary>
        /// 重打票据
        /// </summary>
        /// <param name="receiptSysNo"></param>
        /// <param name="createUserSysNo"></param>
        /// <param name="ReceiptDto"></param>
        /// <param name="remark"></param>
        /// <returns></returns>

        [OperationContract]
        OperationResult RePrintReceipt(long receiptSysNo, long createUserSysNo, out ReceiptView ReceiptDto, string remark = null);

        /// <summary>
        /// 获取订单收据
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="center"></param>
        /// <param name="caseId"></param>
        /// <param name="orderNo"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        [OperationContract]
        IList<Receipt> GetReceipts(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, ReceiptStatus? status, string receiptNo);


        /// <summary>
        /// 根据订单获取收据信息
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        [OperationContract]
        IList<ReceiptView> GetReceiptsByOrderNo(string orderNo);

        /// <summary>
        /// 根据验真码获取收据信息
        /// </summary>
        /// <param name="securityCode"></param>
        /// <returns></returns>
        [OperationContract]
        ReceiptView GetReceiptBySecurityCode(string securityCode);

        /// <summary>
        /// 验证开收据权限
        /// </summary>
        /// <param name="identity"></param>
        /// <returns></returns>
        [OperationContract]
        bool AuthProvideReceiptPermission(string identity);

        /// <summary>
        /// 回收收据
        /// </summary>
        /// <param name="receiptSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult RecycleReceipt(long receiptSysNo, long operatorSysNo);

        /// <summary>
        /// 作废收据
        /// </summary>
        /// <param name="receiptSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult AbolishReceipt(long receiptSysNo, long operatorSysNo);
    }

    [ServiceContract]
    public interface IReceiptServiceV2
    {
        /// <summary>
        /// 验证开收据权限
        /// </summary>
        /// <param name="identity"></param>
        /// <returns></returns>
        [OperationContract]
        bool AuthProvideReceiptPermission(string identity);

        /// <summary>
        ///  保存票据内容
        /// </summary>
        /// <param name="accountType"></param>
        /// <param name="subjectId"></param>
        /// <param name="bizType"></param>
        /// <param name="amount"></param>
        /// <param name="operatorSysNo"></param>
        /// <param name="receiptDto"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult GenerateReceipt(AccountType accountType, string subjectId, OrderBizType bizType, decimal amount, long operatorSysNo, out ReceiptView receiptDto);

        /// <summary>
        /// 预览票据内容
        /// </summary>
        /// <param name="accountType"></param>
        /// <param name="subjectId"></param>
        /// <param name="bizType"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        [OperationContract]
        ReceiptView PreviewReceipt(AccountType accountType, string subjectId, OrderBizType bizType, decimal amount);

        /// <summary>
        /// 预览票据
        /// </summary>
        /// <param name="receiptSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        ReceiptView GetReceiptInfo(long receiptSysNo);

        /// <summary>
        /// 重打票据
        /// </summary>
        /// <param name="receiptSysNo"></param>
        /// <param name="createUserSysNo"></param>
        /// <param name="ReceiptDto"></param>
        /// <param name="remark"></param>
        /// <returns></returns>

        [OperationContract]
        OperationResult RePrintReceipt(long receiptSysNo, long createUserSysNo, out ReceiptView receipt, string remark = null);

        /// <summary>
        /// 获取订单收据
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="center"></param>
        /// <param name="caseId"></param>
        /// <param name="orderNo"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        [OperationContract]
        IList<Receipt> GetReceipts(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, ReceiptStatus? status, string receiptNo);

        /// <summary>
        /// 根据验真码获取收据信息
        /// </summary>
        /// <param name="securityCode"></param>
        /// <returns></returns>
        [OperationContract]
        ReceiptView GetReceiptBySecurityCode(string securityCode);

        /// <summary>
        /// 回收收据
        /// </summary>
        /// <param name="receiptSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult RecycleReceipt(long receiptSysNo, long operatorSysNo);

        /// <summary>
        /// 作废收据
        /// </summary>
        /// <param name="receiptSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult AbolishReceipt(long receiptSysNo, long operatorSysNo);
    }
}
