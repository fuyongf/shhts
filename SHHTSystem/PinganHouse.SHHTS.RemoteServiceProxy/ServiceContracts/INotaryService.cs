﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.DataTransferObjects.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.RemoteServiceProxy.ServiceContracts
{
    [ServiceContract]
    public interface INotaryService
    {
        [OperationContract]
        IList<NotaryDto> Search(NotaryFilter filter);

        [OperationContract]
        NotaryDto Get(long sysNo);

        [OperationContract]
        NotaryDto Insert(string name, string mobile, string officeName, long createUserSysNo);
        [OperationContract]
        OperationResult Update(long sysNo, string name, string mobile, string officeName, long modifyUserSysNo);

        [OperationContract(Name = "GetByNameAndMobile")]
        NotaryDto Get(string name, string mobile);
    }
}
