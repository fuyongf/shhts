﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.DataTransferObjects;

namespace PinganHouse.SHHTS.RemoteServiceProxy.ServiceContracts
{
    [ServiceContract]
    public interface ISystemService
    {
        /// <summary>
        /// 检测服务网络情况
        /// </summary>
        /// <returns></returns>
         [OperationContract]
        OperationResult NetDetection();
    }
}
