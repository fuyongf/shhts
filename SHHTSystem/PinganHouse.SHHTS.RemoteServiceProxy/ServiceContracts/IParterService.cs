﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.DataTransferObjects.External;
using PinganHouse.SHHTS.Enumerations;
using System.Collections.Generic;
using System.ServiceModel;

namespace PinganHouse.SHHTS.RemoteServiceProxy.ServiceContracts
{
    [ServiceContract]
    public interface IParterService
    { 
        
        /// <summary>
        /// 接收微信推送消息
        /// </summary>
        /// <param name="ctx"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult AcceptMessage(WXMessage message);

        /// <summary>
        /// 接收企业应用推送消息
        /// </summary>
        /// <param name="ctx"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult AcceptQYMessage(QYWXMessage message);

        /// <summary>
        /// 生成客户二维码
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        string GetQrCodeUrl(long userSysNo);

        /// <summary>
        /// 生成永久二维码
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract(Name = "GetPermanentQrCodeUrl")]
        string GetQrCodeUrl(string param);

        /// <summary>
        /// 获取auth2请求地址
        /// </summary>
        /// <param name="redirectUrl">回调地址</param>
        /// <param name="state">
        /// 回调会传递回来的参数
        /// a-zA-Z0-9的参数值，最多128字节
        /// </param>
        /// <returns></returns>
        [OperationContract]
        OperationResult GetAuthRequestUrl(string redirectUrl, string state, string appNo);

        /// <summary>
        /// 验证微信获取绑定系统用户
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult ValidateAuth2CallResult(string code);

        /// <summary>
        /// 检测用户
        /// </summary>
        /// <param name="openId">用户唯一标识</param>
        /// <param name="token">服务端分配的key</param>
        /// <param name="identityNo">证件号</param>
        /// <param name="certificateType">证件类型</param>
        /// <param name="captcha">验证码</param>
        /// <returns>if is success， the mobile's value is OtherData["Mobile"] and userId's value is OtherData["UserId"]</returns>
        [OperationContract]
        OperationResult CheckUser(string openId, string identityNo, CertificateType certificateType, string captcha, string token);

        /// <summary>
        /// 发送短信验证码
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="mobile"></param>
        /// <returns>if success is true, resultMessage is key</returns>
        [OperationContract]
        OperationResult SendBindCaptcha(string userId, string mobile);

        /// <summary>
        /// 绑定用户
        /// </summary>
        /// <param name="openId">微信用户唯一标识</param>
        /// <param name="userId">系统用户唯一标识</param>
        /// <param name="captcha">验证码</param>
        /// <param name="token">服务端分配的key</param>
        /// <returns></returns>
        [OperationContract]
        OperationResult BindUser(string openId, string userId, string captcha, string token);

        /// <summary>
        /// 解绑
        /// </summary>
        /// <param name="userSysNo">用户系统编号</param>
        /// <returns></returns>
        [OperationContract]
        OperationResult RelieveBind(long userSysNo, long? modifyUserSysNo);

        /// <summary>
        /// 验证微信获取绑定系统用户
        /// </summary>
        /// <param name="code"></param>
        /// <param name="agentid">企业应用id</param>
        /// <returns></returns>
        [OperationContract(Name = "ValidateQYAuth2CallResult")]
        OperationResult ValidateAuth2CallResult(string code, int agentid);

        /// <summary>
        /// 创建微信菜单
        /// </summary>
        /// <param name="menus"></param>
        /// <param name="agentType">企业号应用id</param>
        /// <returns></returns>
        [OperationContract]
        OperationResult CreateMenus(IList<Dictionary<string, object>> menus, AgentType? agentType = null);

        /// <summary>
        /// 创建企业号部门
        /// </summary>
        /// <param name="name">部门名称</param>
        /// <param name="parentId">
        /// 上级部门id
        /// 如是顶级节点， parentId为1</param>
        /// <param name="id">
        /// 部门id 整形
        /// 如未指定，由系统自动生成
        /// </param>
        /// <param name="order">
        /// 排序顺序
        /// order值小的排序靠前
        /// </param>
        /// <returns></returns>
        [OperationContract]
        OperationResult CreateDepartment(string name, int parentId, int order, int? id = null);

        /// <summary>
        /// 删除部门
        /// 不能删除根部门；
        /// 不能删除含有子部门、成员的部门
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult DeleteDepartment(string id);

        /// <summary>
        /// 邀请成员关注
        /// </summary>
        /// <param name="userId">成员id</param>
        /// <param name="invateContent">邀请内容</param>
        /// <returns></returns>
        [OperationContract]
        OperationResult InviteUserSubscribe(string userId, string invateContent = null);

        /// <summary>
        /// 创建企业号成员
        /// </summary>
        /// <param name="userId">
        /// 成员UserID。
        /// 对应管理端的帐号，企业内必须唯一。长度为1~64个字符
        /// </param>
        /// <param name="name">
        /// 成员名称。长度为1~64个字符
        /// </param>
        /// <param name="mobile">
        /// 手机号码。企业内必须唯一，
        /// mobile/weixinid/email三者不能同时为空
        /// </param>
        /// <param name="email">
        /// 邮箱。长度为0~64个字符。企业内必须唯一
        /// </param>
        /// <param name="weixinid">
        /// 微信号。企业内必须唯一。（注意：是微信号，不是微信的名字）
        /// </param>
        /// <param name="position">
        /// 职位信息。长度为0~64个字符
        /// </param>
        /// <param name="departmentId">
        /// 成员所属部门id列表
        /// </param>
        /// <returns></returns>
        [OperationContract]
        OperationResult CreateUser(string userId, string name, string mobile = null, string email = null, string weixinid = null, string position = null, IEnumerable<string> departmentIds = null, string identityNo = null);

    }
}
