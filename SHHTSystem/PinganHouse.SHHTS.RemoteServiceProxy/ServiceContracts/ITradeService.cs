﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.RemoteServiceProxy.ServiceContracts
{
    [ServiceContract(Name = "TradeService")]
    public interface ITradeService
    {
        #region 查询相关

        /// <summary>
        /// 根据订单号获取订单信息
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        [OperationContract]
        Order GetOrderInfo(string orderNo);

        [OperationContract]
        OrderDetail GetOrderDetail(string orderNo);

        /// <summary>
        /// 根据交易编号获取交易信息
        /// </summary>
        /// <param name="tradeNo"></param>
        /// <returns></returns>
        [OperationContract]
        TradeDetail GetTradeInfo(string tradeNo);

        /// <summary>
        /// 获取账户订单
        /// </summary>
        /// <param name="accountType"></param>
        /// <param name="accountSubjectId"></param>
        /// <param name="orderType"></param>
        /// <param name="tradeType"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [OperationContract]
        IList<Order> GetOrders(AccountType accountType, string accountSubjectId, OrderType? orderType, OrderBizType? tradeType, DateTime? startDate, DateTime? endDate);

        /// <summary>
        /// 获取订单列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="center"></param>
        /// <param name="caseId"></param>
        /// <param name="accountType"></param>
        /// <param name="orderType"></param>
        /// <param name="channel"></param>
        /// <param name="orderNo"></param>
        /// <param name="createUserName"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [OperationContract(Name = "GetPaginatedOrders")]
        IList<OrderPaginatedInfo> GetOrders(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, string caseId, AccountType? accountType, OrderType? orderType, PaymentChannel? channel, string orderNo, string createUserName, DateTime? startDate, DateTime? endDate);

        /// <summary>
        /// 根据客户系统编号获取订单列表(for:wechat)
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="customerSysNos"></param>
        /// <param name="caseId"></param>
        /// <returns></returns>
        [OperationContract(Name = "GetPaginatedOrdersForWeChat")]
        IList<TradePaginatedInfo> GetOrders(int pageIndex, int pageSize, out int totalCount, bool isSelf, IEnumerable<long> customerSysNos, string caseId);

        /// <summary>
        /// 获取退款订单信息
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="center"></param>
        /// <param name="orderNo"></param>
        /// <param name="createUserName"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [OperationContract]
        IList<RefundOrderPaginatedInfo> GetRefundOrders(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, string orderNo, string createUserName, DateTime? startDate, DateTime? endDate);

        /// <summary>
        /// 根据订单获取交易明细
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        [OperationContract]
        IList<TradeDetail> GetTradeByOrder(string orderNo);

        /// <summary>
        /// 获取交易流水
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="center"></param>
        /// <param name="channel"></param>
        /// <param name="tradeNo"></param>
        /// <param name="caseId"></param>
        /// <param name="bankAccountNo"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [OperationContract]
        IList<TradePaginatedInfo> GetTrades(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, PaymentChannel? channel, string orderNo, string tradeNo, string caseId, string bankAccountNo, DateTime? startDate, DateTime? endDate);


        /// <summary>
        /// 根据订单号获取Pos票据
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        [OperationContract]
        IList<BillDetail> GetBillByOrder(string orderNo, BillCategory? category, BillType? type, bool? isBilled = null);

        /// <summary>
        /// 根据案件查询流水
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="caseId"></param>
        /// <param name="orderType"></param>
        /// <returns></returns>
        [OperationContract]
        IList<TradePaginatedInfo> GetTradeByCase(int pageIndex, int pageSize, out int totalCount, string caseId, AccountType? accountType, OrderType? orderType);
        #endregion

        /// <summary>
        /// 提交充值订单
        /// </summary>
        /// <param name="accountType">账户类型</param>
        /// <param name="accountSubjectId">账户主体编号</param>
        /// <param name="amount">金额</param>
        /// <param name="remark">备注</param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SubmitCollection(AccountType accountType, string accountSubjectId, decimal amount, OrderBizType bizType, PaymentChannel channel, string remark, long operatorSysNo);

        ///// <summary>
        ///// 提交交易流水，且创建充值订单
        ///// </summary>
        ///// <param name="accountType">账户类型</param>
        ///// <param name="accountSubjectId">账户主体编号</param>
        ///// <param name="totalAmount">订单总额</param>
        ///// <param name="totalAmount">交易金额</param>
        ///// <param name="channel">支付方式</param>
        ///// <param name="serialNo">交易流水号</param>
        ///// <param name="terminalNo">终端号(POS方式)</param>
        ///// <param name="bankAccountNo">银行帐号</param>
        ///// <param name="bankCode">银行代码</param>
        ///// <param name="accountName">开户名</param>
        ///// <param name="bankName">开户行</param>
        ///// <param name="paymentDate">支付时间</param>
        ///// <param name="remark">备注</param>
        ///// <returns></returns>
        //[OperationContract(Name = "SubmitCollectionWithTrade")]
        //OperationResult SubmitCollection(AccountType accountType, string accountSubjectId, decimal totalAmount, decimal tradeAmount, OrderBizType bizType, PaymentChannel channel, string serialNo, string terminalNo, string bankAccountNo, string bankCode, string accountName, string bankName, DateTime paymentDate, string remark, long operatorSysNo);

        /// <summary>
        /// 提交代收交易明细
        /// </summary>
        /// <param name="orderNo">订单编号</param>
        /// <param name="serialNo">交易流水号</param>
        /// <param name="terminalNo">终端号(POS方式)</param>
        /// <param name="amount">交易金额</param>
        /// <param name="bankAccountNo">银行帐号</param>
        /// <param name="bankCode">银行代码</param>
        /// <param name="accountName">开户名</param>
        /// <param name="bankName">开户行</param>
        /// <param name="paymentDate">支付时间</param>
        /// <param name="remark">备注</param>
        /// <returns></returns>
        //[OperationContract]
        //OperationResult SubmitCollectionTrade(string orderNo, string serialNo, string terminalNo, decimal amount, string bankAccountNo, string bankCode, string accountName, string bankName, DateTime paymentDate, string remark, long operatorSysNo);

        /// <summary>
        /// 提交POS收款交易
        /// </summary>
        /// <param name="orderNo"></param>
        /// <param name="serialNo"></param>
        /// <param name="terminalNo"></param>
        /// <param name="amount"></param>
        /// <param name="bankAccountNo"></param>
        /// <param name="bankCode"></param>
        /// <param name="accountName"></param>
        /// <param name="bankName"></param>
        /// <param name="paymentDate"></param>
        /// <param name="remark"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SubmitPosTrade(string orderNo, string serialNo, string terminalNo, decimal amount, string bankAccountNo, string bankCode, string accountName, string bankName, DateTime paymentDate, string remark, long operatorSysNo);

        ///// <summary>
        ///// 提交代付申请
        ///// </summary>
        ///// <param name="accountType">账户类型</param>
        ///// <param name="accountSubjectId">账户主体编号</param>
        ///// <param name="orderType">订单业务类型</param>
        ///// <param name="amount">订单金额</param>
        ///// <param name="bankAccountNo">收款银行帐号</param>
        ///// <param name="bankCode">收款银行代码</param>
        ///// <param name="accountName">收款人</param>
        ///// <param name="bankName">收款开户行</param>
        ///// <param name="remark">备注</param>
        ///// <returns></returns>
        //[OperationContract]
        //OperationResult SubmitEntrustPay(AccountType accountType, string accountSubjectId, OrderBizType tradeType, decimal amount, string bankAccountNo, string bankCode, string accountName, string bankName, string remark, long operatorSysNo);

        /// <summary>
        /// 提交代付申请
        /// </summary>
        /// <param name="payerType">付款账户类型</param>
        /// <param name="payerSubjectId">付款账户主体编号</param>
        /// <param name="payeeType">收款账户类型</param>
        /// <param name="payeeSubjectId">收款账户主体编号</param>
        /// <param name="orderType">订单业务类型</param>
        /// <param name="amount">订单金额</param>
        /// <param name="bankAccountNo">收款银行帐号</param>
        /// <param name="bankCode">收款银行代码</param>
        /// <param name="accountName">收款人</param>
        /// <param name="bankName">收款开户行</param>
        /// <param name="remark">备注</param>
        /// <returns></returns>
        [OperationContract(Name = "SubmitEntrustPayWithTransfer")]
        OperationResult SubmitEntrustPay(AccountType payerType, string payerSubjectId, AccountType payeeType, string payeeSubjectId, OrderBizType tradeType, decimal amount, string bankAccountNo, string bankCode, string accountName, string bankName, string remark, long operatorSysNo);

        /// <summary>
        /// 提交销帐申请
        /// </summary>
        /// <param name="payerType">付款账户类型</param>
        /// <param name="payerSubjectId">付款账户主体编号</param>
        /// <param name="payeeType">收款账户类型</param>
        /// <param name="payeeSubjectId">收款账户主体编号</param>
        /// <param name="orderType">订单业务类型</param>
        /// <param name="amount">订单金额</param>
        /// <param name="bankAccountNo">收款银行帐号</param>
        /// <param name="bankCode">收款银行代码</param>
        /// <param name="accountName">收款人</param>
        /// <param name="bankName">收款开户行</param>
        /// <param name="remark">备注</param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SubmitPaidOff(AccountType payerType, string payerSubjectId, AccountType payeeType, string payeeSubjectId, OrderBizType tradeType, decimal amount, string bankAccountNo, string bankCode, string accountName, string bankName, string remark, long operatorSysNo);

        /// <summary>
        /// 确认交易流水
        /// </summary>
        /// <param name="tradeNo"></param>
        /// <param name="status"></param>
        /// <param name="amount"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        OperationResult ConfirmCollectionTrade(string tradeNo, ReconciliationStatus status, decimal amount, long? operatorSysNo);

        /// <summary>
        /// 确认代付交易
        /// </summary>
        /// <param name="tradeNo"></param>
        /// <param name="status"></param>
        /// <param name="billNo"></param>
        /// <param name="paymentTime"></param>
        /// <param name="rejectReason"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        OperationResult ConfirmPaymentTrade(string tradeNo, TradeStatus status, string billNo, DateTime? paymentTime, string rejectReason, long? operatorSysNo);

        /// <summary>
        /// 取消代收订单
        /// </summary>
        /// <param name="orderNo"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        //[OperationContract]
        //OperationResult CancelCollectionOrder(string orderNo, long operatorSysNo);

        /// <summary>
        /// 撤销代收交易
        /// </summary>
        /// <param name="tradeNo"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult CancelCollectionTrade(string tradeNo, long operatorSysNo);

        /// <summary>
        /// 撤销代收交易
        /// </summary>
        /// <param name="tradeNo"></param>
        /// <param name="bill"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        [OperationContract(Name = "CancelCollectionTradeWithBill")]
        OperationResult CancelCollectionTrade(string tradeNo, BillDetail bill, long operatorSysNo);

        /// <summary>
        /// 创建收款掉单信息
        /// </summary>
        /// <param name="trade"></param>
        /// <returns></returns>
        OperationResult AddMissCollectionTrade(MissCollectionTrade trade);

        /// <summary>
        /// 获取收款掉单信息
        /// </summary>
        /// <param name="confirmed"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [OperationContract]
        IList<MissCollectionTrade> GetMissCollectionTrade(bool? confirmed, DateTime? startDate, DateTime? endDate);

        /// <summary>
        /// 根据银行卡号获取转账掉单数据
        /// </summary>
        /// <param name="accountNo"></param>
        /// <returns></returns>
        [OperationContract]
        IList<MissCollectionTrade> GetMissTransferTradeByAccountNo(string accountNo);

        /// <summary>
        /// 收款补单
        /// </summary>
        /// <param name="sysNo">掉单记录系统编号</param>
        /// <param name="accountType"></param>
        /// <param name="accountSubjectId"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SubmitTransferCollection(long sysNo, AccountType accountType, string accountSubjectId, OrderBizType bizType, long operatorSysNo);

        /// <summary>
        /// 收款补单
        /// </summary>
        /// <param name="sysNo">掉单记录系统编号</param>
        /// <param name="orderNo"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        //[OperationContract(Name = "ReplenishCollectionTradeByOrder")]
        //OperationResult ReplenishCollectionTrade(long sysNo, string orderNo, long operatorSysNo);

        /// <summary>
        /// 审核收款掉单记录
        /// </summary>
        /// <param name="sysNo">掉单记录系统编号</param>
        /// <param name="tradeNo"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        //[OperationContract]
        //OperationResult ConfirmMissCollectionTrade(long sysNo, string tradeNo, long operatorSysNo);

        /// <summary>
        /// 提交退款订单
        /// </summary>
        /// <param name="accountType"></param>
        /// <param name="subjectId"></param>
        /// <param name="amount"></param>
        /// <param name="bankAccountNo"></param>
        /// <param name="bankCode"></param>
        /// <param name="accountName"></param>
        /// <param name="bankName"></param>
        /// <param name="remark"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SubmitRefundOrder(AccountType accountType, string subjectId, decimal amount, string bankAccountNo, string bankCode, string accountName, string bankName, string remark, long operatorSysNo);

        /// <summary>
        /// 提交退款
        /// </summary>
        /// <param name="orderNo"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SubmitRefund(string orderNo, string remark, long operatorSysNo);

        /// <summary>
        /// 提交消费订单
        /// </summary>
        /// <param name="accountType"></param>
        /// <param name="subjectId"></param>
        /// <param name="amount"></param>
        /// <param name="bizType"></param>
        /// <param name="channel"></param>
        /// <param name="remark"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SubmitExpendOrder(AccountType accountType, string subjectId, decimal amount, OrderBizType bizType, PaymentChannel channel, string remark, long operatorSysNo);

        /// <summary>
        /// 提交转账消费订单
        /// </summary>
        /// <param name="sysNo"></param>
        /// <param name="accountType"></param>
        /// <param name="accountSubjectId"></param>
        /// <param name="bizType"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SubmitTransferExpend(long sysNo, AccountType accountType, string accountSubjectId, OrderBizType bizType, long operatorSysNo);

        /// <summary>
        /// 提交提现请求
        /// </summary>
        /// <param name="accountType"></param>
        /// <param name="subjectId"></param>
        /// <param name="bizType"></param>
        /// <param name="amount"></param>
        /// <param name="bankAccountNo"></param>
        /// <param name="bankCode"></param>
        /// <param name="accountName"></param>
        /// <param name="bankName"></param>
        /// <param name="remark"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SubmitWithdraw(AccountType accountType, string subjectId, OrderBizType bizType, decimal amount, string bankAccountNo, string bankCode, string accountName, string bankName, string remark, long operatorSysNo);

        #region 订单审核

        /// <summary>
        /// 付款订单审核
        /// </summary>
        /// <param name="center"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult PaymentOrderAudit(ReceptionCenter center, string orderNo, AuditStatus status, string remark, long operatorSysNo, string operatorName);

        /// <summary>
        /// 提现订单审核
        /// </summary>
        /// <param name="center"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult WithdrawOrderAudit(ReceptionCenter center, string orderNo, AuditStatus status, string remark, long operatorSysNo, string operatorName);

        /// <summary>
        /// 退款订单审核
        /// </summary>
        /// <param name="center"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult RefundOrderAudit(ReceptionCenter center, string orderNo, AuditStatus status, string remark, long operatorSysNo, string operatorName);

        /// <summary>
        /// 获取订单审核日志
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        [OperationContract]
        IList<AuditLog> GetOrderAuditLog(string orderNo);
        #endregion

        /// <summary>
        /// 提交扣款计划申请
        /// </summary>
        /// <param name="accountType">扣款账户类型</param>
        /// <param name="subjectId">扣款账户编号</param>
        /// <param name="deductType">扣款用途</param>
        /// <param name="amount">扣款金额</param>
        /// <param name="limitSource">限制扣款源交易类型，如果为空，则不限制</param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SubmitDeductSchedule(AccountType accountType, string subjectId, OrderBizType deductType, decimal amount, string remark, long operatorSysNo, IEnumerable<OrderBizType> limitSource);

        /// <summary>
        /// 删除扣款计划
        /// </summary>
        /// <param name="sysNo"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult DeleteDeductSchedule(long sysNo, long operatorSysNo, long verson);

        [OperationContract]
        IList<DeductSchedule> GetDeductSchedules(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, string caseId, bool? finished, DateTime? startDate, DateTime? endDate);


        /// <summary>
        /// 子账户划转(账户余额用途变更)
        /// </summary>
        /// <param name="accountType"></param>
        /// <param name="subjectId"></param>
        /// <param name="formType"></param>
        /// <param name="toType"></param>
        /// <param name="changeAmount"></param>
        /// <param name="remark"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult ChangeBalanceType(AccountType accountType, string subjectId, OrderBizType formType, OrderBizType toType, decimal changeAmount, string remark, long operatorSysNo);
    }
}
