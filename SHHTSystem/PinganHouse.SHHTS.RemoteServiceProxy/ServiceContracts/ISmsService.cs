﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

using PinganHouse.SHHTS.DataTransferObjects;

namespace PinganHouse.SHHTS.RemoteServiceProxy.ServiceContracts
{
    [ServiceContract]
    public interface ISmsService
    {
        /// <summary>
        /// 发送注册验证码
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendRegCode(string mobile);

        /// <summary>
        /// 重置登陆密码验证码
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendUpdatePwdCode(string mobile);


        /// <summary>
        /// 发送绑定验证码
        /// </summary>
        /// <param name="mobile">手机号码</param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendBindCaptcha(string mobile);

        /// <summary>
        /// 发送解绑短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="name"></param>
        /// <param name="unbindMobile"></param>
        /// <param name="tailNum"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendUnBindMessage(string mobile, string name, string tel, string idTailNum);

        /// <summary>
        /// 发送绑定短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="name"></param>
        /// <param name="unbindMobile"></param>
        /// <param name="tailNum"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendBindMessage(string mobile, string name, string tel, string idTailNum);

        /// <summary>
        /// 发送签约短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="name"></param>
        /// <param name="tel"></param>
        /// <param name="idTailNum"></param>
        /// <param name="signDate"></param>
        /// <param name="address"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendSignMessage(string mobile, string name, string tel, string idTailNum, DateTime signDate, string address);

        /// <summary>
        /// 发送佣金支付短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="name"></param>
        /// <param name="tel"></param>
        /// <param name="address"></param>
        /// <param name="agentFee"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendPayAgentFeeMessage(string mobile, string name, string tel, string address, decimal agentFee);

        /// <summary>
        /// 发送贷款材料通知
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="name"></param>
        /// <param name="tel"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendCreditMaterialNotice(string mobile, string name, string tel);

        /// <summary>
        /// 发送交易材料通知
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="name"></param>
        /// <param name="tel"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendTradeMaterialNotice(string mobile, string name, string tel);

        /// <summary>
        /// 发送立案预约短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="name"></param>
        /// <param name="tel"></param>
        /// <param name="date"></param>
        /// <param name="center"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendCaseArrangeMessage(string mobile, string name, string tel, DateTime date, string center);

        /// <summary>
        /// 发送过户预约短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="name"></param>
        /// <param name="tel"></param>
        /// <param name="date"></param>
        /// <param name="center"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendTransferArrangeMessage(string mobile, string name, string tel, DateTime date, string center);

        /// <summary>
        /// 发送票据寄出通知
        /// </summary>
        /// <param name="mobile"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendPostBillNotice(string mobile);

        /// <summary>
        /// 发送收款通知
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="date"></param>
        /// <param name="bankCardTailNum"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendCollectionNotice(string mobile, DateTime date, string bankCardTailNum, decimal amount);

        /// <summary>
        /// 发送代扣短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendEntrustPayNotice(string mobile, decimal amount);

        /// <summary>
        /// 发送转账通知
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="amount"></param>
        /// <param name="bankCardTailNum"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendWithdrawNotice(string mobile, decimal amount, string bankCardTailNum, DateTime date);

        /// <summary>
        /// 发送立案确认短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="name"></param>
        /// <param name="name1"></param>
        /// <param name="tel"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendConfirmCaseMessage(string mobile, string name, string name1, string tel);

        /// <summary>
        /// 发送还贷预约短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendRepayArrangeMessage(string mobile, DateTime date);

        /// <summary>
        /// 发送限购结果短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendLimitBuyMessage(string mobile, string result);

        /// <summary>
        /// 发送交房通知
        /// </summary>
        /// <param name="mobile"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendDeliveryHouseNotice(string mobile);

        /// <summary>
        /// 发送领产证通知
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="address"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendGetCredentialNotice(string mobile, string address, string date);

        /// <summary>
        /// 发送交易过户办理通知
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="address"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendHandleTransferNotice(string mobile, string address, string date, string center, string name, string tel);

        /// <summary>
        /// 发送贷款审批通过通知
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="name"></param>
        /// <param name="tel"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendCreditPassNotice(string mobile, string name, string tel);

        /// <summary>
        /// 发送还贷预约2短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="amount"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendRepayArrange2Message(string mobile, decimal amount, DateTime date);

        /// <summary>
        /// 发送还贷预约3短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="amount"></param>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendRepayArrange3Message(string mobile, decimal amount, DateTime date1, DateTime date2);

        /// <summary>
        /// 自行预约还贷3短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="amount"></param>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendSelfRepay3Message(string mobile, decimal amount, DateTime date1, DateTime date2);

        /// <summary>
        /// 自行预约还贷2短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="amount"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendSelfRepay2Message(string mobile, decimal amount, DateTime date);

        /// <summary>
        /// 自行预约还贷1短信
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="amount"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendSelfRepayMessage(string mobile, DateTime date);
    }
}
