﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.RemoteServiceProxy.ServiceContracts
{
    [ServiceContract]
    public interface ICaptchaService
    {
        /// <summary>
        /// 校验验证码
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult ValidateCaptcha(string key, string value);

        /// <summary>
        /// 请求验证码
        /// </summary>
        /// <param name="token"></param>
        /// <param name="businessType"></param>
        /// <returns></returns>
        [OperationContract]
        Captcha RequetCaptcha(string token, BusinessCacheType businessType);

                /// <summary>
        /// 请求验证吗
        /// </summary>
        /// <param name="token">标识</param>
        /// <param name="businessType">验证类型</param>
        /// <param name="factor">随机因子</param>
        /// <param name="length">验证码长度</param>
        /// <returns></returns>
        [OperationContract(Name = "RequetCustomCaptcha")]
        Captcha RequetCaptcha(string token, BusinessCacheType businessType, string factor, int length);

        /// <summary>
        /// 校验验证码
        /// </summary>
        /// <param name="token"></param>
        /// <param name="businessType">业务类型</param>
        /// <param name="value"></param>
        /// <returns></returns>
        [OperationContract(Name = "ValidateCaptchaByType")]
        OperationResult ValidateCaptcha(string token, BusinessCacheType businessType, string value);
    }
}
