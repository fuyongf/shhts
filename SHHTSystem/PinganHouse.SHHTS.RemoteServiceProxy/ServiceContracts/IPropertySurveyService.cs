﻿using PinganHouse.SHHTS.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace PinganHouse.SHHTS.RemoteServiceProxy.ServiceContracts
{
    [ServiceContract]
    public interface IPropertySurveyService
    {
        /// <summary>
        /// 添加产调结果
        /// </summary>
        /// <param name="propertySurveyDto"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult AddPropertySurvey(PropertySurveyDto propertySurveyDto, string tenementAddress, List<long> proprietors);

        /// <summary>
        /// 获取产调记录
        /// </summary>
        /// <param name="tenementContract"></param>
        /// <returns></returns>
        [OperationContract]
        Tuple<PropertyCertificateDto, IList<PropertySurveyDto>> GetPropertySurvey(string tenementContract);


        /// <summary>
        /// 获取产调列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="tenementContract"></param>
        /// <param name="tenementAddress"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        [OperationContract]
        IList<Tuple<PropertyCertificateDto, IEnumerable<PropertySurveyDto>>> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, string tenementContract = null,
                string tenementAddress = null, DateTime? startDate = null, DateTime? endDate = null, IDictionary<string, object> data = null);
    }
}
