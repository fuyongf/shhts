﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy.Log;
using PinganHouse.SHHTS.RemoteServiceProxy.ServiceContracts;
using PinganHouse.SHHTS.RemoteServiceProxy.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.RemoteServiceProxy
{
    public class CaptchaProxyService
    {
        readonly static CaptchaProxyService captchaProxyService = new CaptchaProxyService();
        private CaptchaProxyService()
        { }

        protected ICaptchaService CaptchaService
        {
            get { return ServiceHelper.GetObject<ICaptchaService>("CaptchaRemoteService"); }
        }

        public static CaptchaProxyService GetInstanse()
        {
            return captchaProxyService;
        }

        public Captcha RequetCaptcha(string token, BusinessCacheType businessType)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(token))
                    return null;
                return CaptchaService.RequetCaptcha(token, businessType);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.ToString());
                return null;
            }
        }

        public Captcha RequetCaptcha(string token, BusinessCacheType businessType, string factor, int length)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(token))
                    return null;
                return CaptchaService.RequetCaptcha(token, businessType, factor, length);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.ToString());
                return null;
            }
        }

        public OperationResult ValidateCaptcha(string token, BusinessCacheType businessType, string value)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(token))
                    return new OperationResult(1, "token is null");
                if (string.IsNullOrWhiteSpace(value))
                    return new OperationResult(2, "value is null");
                return CaptchaService.ValidateCaptcha(token, businessType, value);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.ToString());
                return new OperationResult(-999, "系统内部错误(-999)");
            }

        }


        public OperationResult ValidateCaptcha(string key, string value)
        {

            try
            {
                if (string.IsNullOrWhiteSpace(key))
                    return new OperationResult(1, "key is null");
                if (string.IsNullOrWhiteSpace(value))
                    return new OperationResult(2, "value is null");
                return CaptchaService.ValidateCaptcha(key, value);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.ToString());
                return new OperationResult(-999, "系统内部错误(-999)");
            }

        }
    }
}
