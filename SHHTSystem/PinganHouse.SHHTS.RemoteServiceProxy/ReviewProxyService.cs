﻿using HTB.DevFx;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy.Log;
using PinganHouse.SHHTS.RemoteServiceProxy.ServiceContracts;
using PinganHouse.SHHTS.RemoteServiceProxy.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.RemoteServiceProxy
{
    public class ReviewProxyService
    {
        readonly static ReviewProxyService reviewProxyService = new ReviewProxyService();
        private ReviewProxyService()
        { }

        protected IReviewService ReviewService
        {
            get { return ServiceHelper.GetObject<IReviewService>("ReviewProxyService"); }
        }

        public static ReviewProxyService GetInstanse()
        {
            return reviewProxyService;
        }

        /// <summary>
        /// 获取复核规则
        /// </summary>
        /// <param name="reviewCategory">复核类别</param>
        /// <param name="affiliationNo">复核行为实例</param>
        /// <param name="reviewDto">复核规则</param>
        /// <returns> success is true need review</returns>
        public ReviewDto ReviewRule(ReviewCategory reviewCategory, string affiliationNo, long? createUserSysNo)
        {
            ReviewDto reviewDto = null;
            try
            {
                if (reviewCategory == ReviewCategory.UnKonw)
                    return reviewDto;
                var result = ReviewService.ReviewRule(reviewCategory, affiliationNo, createUserSysNo, out reviewDto);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
            }
            return reviewDto;
        }

        public OperationResult ValidateReviewRule(long sysno, long reviewerSysNo, ReviewMode reviewMode, out ReviewDto reviewDto)
        {
            reviewDto=null;
            try
            {
                if (sysno <= 0 || reviewerSysNo <= 0)
                    return new OperationResult(1,"参数异常");
                return ReviewService.ValidateReviewRule(sysno, reviewerSysNo, reviewMode, out reviewDto);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return new OperationResult(-1, "系统内部错误(-999)");
            }
        }
    }
}
