﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using HTB.DevFx;

using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.DataTransferObjects;

using PinganHouse.SHHTS.RemoteServiceProxy.Log;
using PinganHouse.SHHTS.RemoteServiceProxy.ServiceContracts;
using PinganHouse.SHHTS.RemoteServiceProxy.Utils;

namespace PinganHouse.SHHTS.RemoteServiceProxy
{
    public class AccountServiceProxy
    {
        static IAccountService AccountService
        {
            get
            {
                return ServiceHelper.GetObject<IAccountService>("AccountProxyService");
            }
        }

        /// <summary>
        /// 获取账户余额
        /// </summary>
        /// <param name="accountType">账户类型</param>
        /// <param name="accountSubjectId">账户主体编号</param>
        /// <returns></returns>
        public static decimal GetBalance(AccountType accountType, string accountSubjectId)
        {
            try
            {
                return AccountService.GetBalance(accountType, accountSubjectId);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                throw;
            }
        }

        /// <summary>
        /// 获取账户信息
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        public static Account Get(long sysNo)
        {
            try
            {
                return AccountService.Get(sysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 根据类型获取账户信息
        /// </summary>
        /// <param name="type"></param>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        public static Account Get(AccountType type, string subjectId)
        {
            try
            {
                return AccountService.Get(type, subjectId);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 获取资金台帐
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="center"></param>
        /// <param name="caseId"></param>
        /// <param name="tenementNo"></param>
        /// <param name="tenementAddr"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static IList<AccountLedger> GetAccounts(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, string caseId, string tenementNo, string tenementAddr, DateTime? startDate, DateTime? endDate)
        {
            try
            {
                return AccountService.GetAccounts(pageIndex, pageSize, out totalCount, center, caseId, tenementNo, tenementAddr, startDate, endDate);
            }
            catch (Exception e)
            {
                totalCount = 0;
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 银行卡鉴权
        /// </summary>
        /// <param name="bankCode"></param>
        /// <param name="accountNo"></param>
        /// <param name="name"></param>
        /// <param name="identityNo"></param>
        /// <param name="mobile"></param>
        /// <returns></returns>
        public static OperationResult AuthenticateBankCard(string caseId, string bankCode, string cardNo, string name, string identityNo, string mobile, long operatorSysNo)
        {
            if (string.IsNullOrWhiteSpace(bankCode) || string.IsNullOrWhiteSpace(cardNo) || string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(identityNo) || string.IsNullOrWhiteSpace(mobile))
                return new OperationResult(-1, "参数错误");
            try
            {
                return AccountService.AuthenticateBankCard(caseId, bankCode, cardNo, name, identityNo, mobile, operatorSysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                throw;
            }
        }

        /// <summary>
        /// 注销已鉴权银行卡信息
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        public static OperationResult DeleteAuthedBankCard(long sysNo, long operatorSysNo)
        {
            try
            {
                return AccountService.DeleteAuthedBankCard(sysNo, operatorSysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                throw;
            }
        }

        /// <summary>
        /// 获取银行卡列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="accountNo"></param>
        /// <param name="accountName"></param>
        /// <param name="identityNo"></param>
        /// <param name="mobile"></param>
        /// <param name="isAuth"></param>
        /// <returns></returns>
        //public static IList<AuthedBankCard> GetAuthedBankCards(int pageIndex, int pageSize, out int totalCount, string caseId, string cardNo, string accountName, string identityNo, string mobile)
        //{
        //    try
        //    {
        //        return AccountService.GetAuthedBankCards(pageIndex, pageSize, out totalCount, caseId, cardNo, accountName, identityNo, mobile);
        //    }
        //    catch (Exception e)
        //    {
        //        totalCount = 0;
        //        LogHandler.Write(e.ToString());
        //        return null;
        //    }
        //}

        /// <summary>
        /// 根据案件获取已鉴权的银行卡信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public static AuthedBankCard GetAuthedBankCardByCase(string caseId)
        {
            if (string.IsNullOrWhiteSpace(caseId))
                return null;
            try
            {
                return AccountService.GetAuthedBankCardByCase(caseId);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                throw;
            }
        }

        /// <summary>
        /// 获取案件上下家收付总额
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public static Tuple<IDictionary<OrderType, decimal>, Dictionary<OrderType, decimal>> GetCaseTradeAmount(string caseId)
        {
            try
            {
                return AccountService.GetCaseTradeAmount(caseId);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                throw;
            }
        }

        /// <summary>
        /// 获取账户账单
        /// </summary>
        /// <param name="accountType"></param>
        /// <param name="subjectId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static AccountBill GetAccountBill(AccountType accountType, string subjectId, DateTime? startDate, DateTime? endDate) 
        {
            try
            {
                return AccountService.GetAccountBill(accountType, subjectId, startDate, endDate);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                throw;
            }
        }
    }
}
