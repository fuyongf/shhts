﻿using HTB.DevFx;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy.ServiceContracts;
using PinganHouse.SHHTS.RemoteServiceProxy.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.RemoteServiceProxy
{
    public class LoanOptimizeServiceProxy
    {
        static ILoanOptimizeService LoanOptimizeService
        {
            get
            {
                return ServiceHelper.GetObject<ILoanOptimizeService>("LoanOptimizeService");
            }
        }

        /// <summary>
        /// 获取全部可行方案.
        /// </summary>
        /// <param name="ips">用户输入数据</param>
        /// <returns></returns>
        public static IList<LoanOptimizePlan> GetPlans(OInputs ips)
        {
            return LoanOptimizeService.GetPlans(ips);
        }
    }
}
