﻿using HTB.DevFx;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy.Log;
using PinganHouse.SHHTS.RemoteServiceProxy.ServiceContracts;
using PinganHouse.SHHTS.RemoteServiceProxy.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.RemoteServiceProxy
{
    public class PropertySurveyProxyService
    {
        readonly static PropertySurveyProxyService propertySurveyProxyService = new PropertySurveyProxyService();
        private PropertySurveyProxyService()
        { }

        protected IPropertySurveyService PropertySurveyService
        {
            get { return ServiceHelper.GetObject<IPropertySurveyService>("PropertySurveyProxyService"); }
        }

        public static PropertySurveyProxyService GetInstanse()
        {
            return propertySurveyProxyService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertySurveyDto"></param>
        /// <param name="tenementAddress">物业地址</param>
        /// <param name="Proprietors">业主</param>
        /// <returns></returns>
        public OperationResult AddPropertySurvey(PropertySurveyDto propertySurveyDto, string tenementAddress, List<long> Proprietors)
        {
            if (propertySurveyDto == null)
                return new OperationResult(1, "param is null");
            if (string.IsNullOrWhiteSpace(propertySurveyDto.TenementContract))
                return new OperationResult(2, "TenementContract is null");
            try
            {
                return PropertySurveyService.AddPropertySurvey(propertySurveyDto, tenementAddress, Proprietors);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return new OperationResult(-1,"系统内部错误");
            }
        }

        public Tuple<PropertyCertificateDto, IList<PropertySurveyDto>> GetPropertySurvey(string tenementContract)
        {
            if (string.IsNullOrWhiteSpace(tenementContract))
                return null;
            try
            {
                return PropertySurveyService.GetPropertySurvey(tenementContract);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return null;
            }
        }

        public IList<Tuple<PropertyCertificateDto, IEnumerable<PropertySurveyDto>>> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, string tenementContract = null,
                string tenementAddress = null, DateTime? startDate = null, DateTime? endDate = null, IDictionary<string, object> data = null)
        {
            if (pageIndex <= 0)
                pageIndex = 1;
            if (pageSize <= 0 || pageSize > 1000)
                pageSize = 10;
            totalCount = 0;
            try
            {
                return PropertySurveyService.GetPaginatedList(pageIndex, pageSize, out totalCount, tenementContract, tenementAddress, startDate, endDate, data);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return null;
            }
        }
    }
}
