﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.RemoteServiceProxy.Log
{
    public interface ILogService
    {
        /// <summary>
        /// 记录日志
        /// </summary>
        /// <param name="message"></param>
        void Write(string message);
    }
}
