﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using HTB.DevFx;

using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.DataTransferObjects;

using PinganHouse.SHHTS.RemoteServiceProxy.Log;
using PinganHouse.SHHTS.RemoteServiceProxy.ServiceContracts;
using PinganHouse.SHHTS.RemoteServiceProxy.Utils;

namespace PinganHouse.SHHTS.RemoteServiceProxy
{
    public class AgentServiceProxy
    {
        static IAgentService AgentService
        {
            get
            {
                return ServiceHelper.GetObject<IAgentService>("AgentRemoteService");
            }
        }

        static IUserService UserService
        {
            get
            {
                return ServiceHelper.GetObject<IUserService>("UserRemoteService");
            }
        }

        /// <summary>
        /// 创建中介公司
        /// </summary>
        /// <param name="agentSysNo"></param>
        /// <returns></returns>
        public static OperationResult AgentJoint(long agentSysNo, long operatorSysNo)
        {
            try
            {
                return AgentService.AgentJoint(agentSysNo, operatorSysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 创建中介公司
        /// </summary>
        /// <param name="agentCompany"></param>
        /// <returns></returns>
        public static OperationResult AgentJoint(AgentCompanyModel agentCompany, long operatorSysNo, AgentJointStatus status = AgentJointStatus.Jointing)
        {
            if (string.IsNullOrWhiteSpace(agentCompany.CompanyName) ||
                string.IsNullOrWhiteSpace(agentCompany.Principal))
                return new OperationResult(-1, "信息不完整");
            if (status == AgentJointStatus.Cancelled)
                return new OperationResult(-2, "中介公司状态错误");
            var dto = new AgentCompany
            {
                CompanyName = agentCompany.CompanyName,
                Address = agentCompany.Address,
                Principal = agentCompany.Principal,
                Status = status,
                ContactInfo = agentCompany.ContactInfo,
                JointContractNo = agentCompany.JointContractNo,
                ReceiveAccount = agentCompany.ReceiveAccount,
                ReceiveBank = agentCompany.ReceiveBank,
                ReceiveSubBank = agentCompany.ReceiveSubBank,
                ReceiveName = agentCompany.ReceiveName,
                CreateDate = DateTime.Now,
                CreateUserSysNo = operatorSysNo,
                JointDate = (agentCompany.JointDate == null && status == AgentJointStatus.Jointing) ? DateTime.Now : new Nullable<DateTime>(),
            };
            try
            {
                return AgentService.AgentJoint(dto);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 中介公司解约
        /// </summary>
        /// <param name="agentSysNo"></param>
        /// <param name="cancelReson"></param>
        /// <returns></returns>
        public static OperationResult AgentCancel(long agentSysNo, string cancelReson, long operatorSysNo)
        {
            try
            {
                return AgentService.AgentCancel(agentSysNo, cancelReson, operatorSysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 根据系统编号获取中介公司信息
        /// </summary>
        /// <param name="agenSysNo"></param>
        /// <returns></returns>
        public static AgentCompany GetAgentCompany(long agentSysNo)
        {
            try
            {
                return AgentService.GetAgentCompany(agentSysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 获取中介公司列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="condition"></param>
        /// <param name="matchingType"></param>
        /// <returns></returns>
        public static IList<AgentCompany> GetAgentCompanyList(string condition, AgentJointStatus? status, int pageIndex, int pageSize, out int totalCount)
        {
            try
            {
                return AgentService.GetAgentCompanyList(condition, status, pageIndex, pageSize, out totalCount);
            }
            catch (Exception e)
            {
                totalCount = 0;
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 修改中介公司信息
        /// </summary>
        /// <param name="companySysNo"></param>
        /// <param name="companyName"></param>
        /// <param name="legalPersonName"></param>
        /// <param name="address"></param>
        /// <param name="contactInfo"></param>
        /// <returns></returns>
        public OperationResult ModifyCompanyInfo(long companySysNo, string companyName, string principal, string address, string contactInfo, long operatorSysNo)
        {
            try
            {
                return AgentService.ModifyCompanyInfo(companySysNo, companyName, principal, address, contactInfo, operatorSysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        #region 经纪人
        /// <summary>
        /// 创建经纪人信息
        /// </summary>
        /// <param name="agentStaff"></param>
        /// <returns></returns>
        public static OperationResult CreateAgentStaff(AgentStaffModel agentStaff, long operatorSysNo)
        {
            if (agentStaff == null || string.IsNullOrWhiteSpace(agentStaff.Name) ||
                (string.IsNullOrWhiteSpace(agentStaff.Mobile) && string.IsNullOrWhiteSpace(agentStaff.Mobile1)) ||
                (!string.IsNullOrEmpty(agentStaff.LoginName) && string.IsNullOrWhiteSpace(agentStaff.Password)))
                return new OperationResult(-1, "经纪人身份信息不完整");
            if (agentStaff.AgentCompanySysNo == 0)
                return new OperationResult(-2, "请选择中介公司");
            var dto = new AgentStaff
            {
                RealName = agentStaff.Name,
                Mobile = agentStaff.Mobile,
                Mobile1 = agentStaff.Mobile1,
                Gender = agentStaff.Gender,
                CertificateType = agentStaff.CertificateType,
                IdentityNo = agentStaff.IdentityNo,
                TelPhone = agentStaff.TelPhone,

                //LoginId = agentStaff.LoginName,

                AgentCompanySysNo = agentStaff.AgentCompanySysNo,
                IsManager = agentStaff.IsManager,
                //AuthDisabled = !agentStaff.IsManager,

                CreateUserSysNo = operatorSysNo,
                CreateDate = DateTime.Now
            };

            try
            {
                return UserService.CreateAgentStaff(dto, agentStaff.LoginName, agentStaff.Password);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 根据系统编号获取用户
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        public static AgentStaff GetAgentSatff(long sysNo)
        {
            try
            {
                return UserService.GetAgentSatff(sysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 根据用户ID获取用户
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        //public static AgentStaff GetAgentSatff(string userId)
        //{
        //    try
        //    {
        //        return UserService.GetAgentSatff(userId);
        //    }
        //    catch (Exception e)
        //    {
        //        LogHandler.Write(e.ToString());
        //        return null;
        //    }
        //}

        /// <summary>
        /// 分页获取中介公司员工信息
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="agentCompanySysNo"></param>
        /// <param name="condition"></param>
        /// <param name="matchingType"></param>
        /// <returns></returns>
        public static IList<AgentStaff> GetAgentStaffs(int pageIndex, int pageSize, out int totalCount, long? agentCompanySysNo, bool? isManager, string condition = null, string identityNo = null, string name = null, string mobile = null)
        {
            try
            {
                return UserService.GetAgentStaffs(pageIndex, pageSize, out totalCount, agentCompanySysNo, isManager, condition, identityNo, name, mobile);
            }
            catch (Exception e)
            {
                totalCount = 0;
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 分页获取中介公司员工信息
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="agentCompanySysNo"></param>
        /// <param name="condition"></param>
        /// <param name="matchingType"></param>
        /// <returns></returns>
        public static IList<AgentStaffQueryInfo> GetAgentStaffsWithCompany(int pageIndex, int pageSize, out int totalCount, long? agentCompanySysNo, bool? isManager, string condition = null, string identityNo = null, string name = null, string mobile = null)
        {
            try
            {
                return UserService.GetAgentStaffsWithCompany(pageIndex, pageSize, out totalCount, agentCompanySysNo, isManager, condition, identityNo, name, mobile);
            }
            catch (Exception e)
            {
                totalCount = 0;
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 修改中介员工信息
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public static OperationResult ModifyAgentStaff(AgentStaff agentStaff)
        {
            try
            {
                return UserService.ModifyAgentStaff(agentStaff);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 删除中介员工
        /// </summary>
        /// <param name="sysNo">经纪人系统编号</param>
        /// <param name="mangerUserId">管理员用户编号</param>
        /// <returns></returns>
        public static OperationResult DeleteStaff(long sysNo, string operatorUserId) 
        {
            try
            {
                return UserService.DeleteAgentStaff(sysNo, operatorUserId);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 根据经纪人userid获取用户
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        public static AgentStaff GetAgentSatff(string userId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(userId))
                    return null;
                return UserService.GetAgentSatff(userId);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return null;
            }
        }
        #endregion
    }
}
