﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.RemoteServiceProxy
{
    public class AgentCompanyModel
    {
        /// <summary>
        /// 公司名称
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        ///负责人
        /// </summary>
        public string Principal { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 联系方式
        /// </summary>
        public string ContactInfo { get; set; }

        /// <summary>
        /// 合作协议编号
        /// </summary>
        public string JointContractNo { get; set; }

        /// <summary>
        /// 合作开始时间
        /// </summary>
        public DateTime? JointDate { get; set; }

        /// <summary>
        /// 收款银行简码
        /// </summary>
        public string ReceiveBank { get; set; }

        /// <summary>
        /// 收款支行
        /// </summary>
        public string ReceiveSubBank { get; set; }

        /// <summary>
        /// 收款人
        /// </summary>
        public string ReceiveName { get; set; }

        /// <summary>
        /// 收款帐号
        /// </summary>
        public string ReceiveAccount { get; set; }
    }
}
