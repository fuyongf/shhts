﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.RemoteServiceProxy
{
    public class PinganStaffModel : UserIdentityModel
    {
        /// <summary>
        /// 移动电话
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// 固定电话
        /// </summary>
        public string TelPhone { get; set; }

        /// <summary>
        /// 平安UM帐号
        /// </summary>
        public string UMCode { get; set; }

        /// <summary>
        /// 平安工号
        /// </summary>
        public string StaffNo { get; set; }

        /// <summary>
        /// 所属受理中心
        /// </summary>
        public ReceptionCenter ReceptionCenter { get; set; }

        /// <summary>
        /// 岗位
        /// </summary>
        public Duty Duty { get; set; }

        /// <summary>
        /// 平安工卡编号
        /// </summary>
        public string StaffCardNo { get; set; }
    }
}
