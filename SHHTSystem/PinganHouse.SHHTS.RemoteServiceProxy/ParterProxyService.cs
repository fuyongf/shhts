﻿using HTB.DevFx;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.DataTransferObjects.External;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy.Log;
using PinganHouse.SHHTS.RemoteServiceProxy.ServiceContracts;
using PinganHouse.SHHTS.RemoteServiceProxy.Utils;
using System;
using System.Collections.Generic;

namespace PinganHouse.SHHTS.RemoteServiceProxy
{
    public class ParterProxyService
    {
         readonly static ParterProxyService parterProxyService = new ParterProxyService();
         private ParterProxyService()
        { }

         protected IParterService ParterService
        {
            get { return ServiceHelper.GetObject<IParterService>("ParterProxyService"); }
        }

        public static ParterProxyService GetInstanse()
        {
            return parterProxyService;
        }

        public OperationResult AcceptMessage(WXMessage message)
        {
            try 
            {
                //过滤消息类型
                if (message.MsgType != WXMessageType.Event || message.Event.Value == EventType.View)
                    return new OperationResult(1);
                return ParterService.AcceptMessage(message);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.ToString());
                return new OperationResult(-1, "系统内部错误");
            }
        }

        public OperationResult AcceptQYMessage(QYWXMessage message)
        {
            try
            {
                //过滤消息类型
                if (message.MsgType != WXMessageType.Event || message.Event.Value == EventType.View)
                    return new OperationResult(1);
                return ParterService.AcceptQYMessage(message);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.ToString());
                return new OperationResult(-1, "系统内部错误");
            }
        }

        /// <summary>
        /// 获取带参数的临时二维码
        /// 1800S过期
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        public string GetQrCodeUrl(long param)
        {
            try
            {
                if (param <= 0)
                    return string.Empty;
                return ParterService.GetQrCodeUrl(param);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.ToString());
                return string.Empty;
            }
        }

        /// <summary>
        /// 获取永久二维码
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public string GetPermanentQrCodeUrl(string param)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(param) || param.Length>64)
                    return string.Empty;
                return ParterService.GetQrCodeUrl(param);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.ToString());
                return string.Empty;
            }
        }

        public string GetAuthRequestUrl(string redirectUrl, string state, string appNo)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(redirectUrl))
                    return null;
                if (string.IsNullOrWhiteSpace(appNo))
                    return null;
                var result = ParterService.GetAuthRequestUrl(redirectUrl, state, appNo);
                if (result.Success)
                    return result.ResultMessage;
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.ToString());              
            }
            return null;
        }

        public OperationResult ValidateAuth2CallResult(string code, string appNo)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(code))
                    return new OperationResult(1, "用户未授权");
                if (string.Compare(appNo, "weixin", true) != 0)
                    return new OperationResult(2, "应用编号异常");
                return  ParterService.ValidateAuth2CallResult(code);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.ToString());
                return new OperationResult(-1, "系统内部错误(-1)");
            }         
        }

        /// <summary>
        /// 检测用户
        /// </summary>
        /// <param name="openId">用户唯一标识</param>
        /// <param name="token">服务端分配的key</param>
        /// <param name="identityNo">证件号</param>
        /// <param name="certificateType">证件类型</param>
        /// <param name="captcha">验证码</param>
        /// <returns>if is success， the mobile's value is OtherData["Mobile"] and userId's value is OtherData["UserId"]</returns>
        public OperationResult CheckUser(string openId, string identityNo, CertificateType certificateType, string captcha, string token)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(openId))
                    return new OperationResult(1, "用户唯一标识为空");
                if (string.IsNullOrWhiteSpace(captcha))
                    return new OperationResult(2, "验证码错误,请重新输入");
                if (string.IsNullOrWhiteSpace(token))
                    return new OperationResult(3, "token 为空");
                if (string.IsNullOrWhiteSpace(identityNo))
                    return new OperationResult(4, "证件号为空");
                return ParterService.CheckUser(openId, identityNo, certificateType, captcha, token);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.ToString());
                return new OperationResult(-1, "系统内部错误(-1)");
            }     
        }

        public OperationResult SendBindCaptcha(string userId, string mobile)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(userId))
                    return new OperationResult(1, "参数错误");
                if (string.IsNullOrWhiteSpace(mobile))
                    return new OperationResult(2, "手机号码错误");;
                return ParterService.SendBindCaptcha(userId, mobile);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.ToString());
                return new OperationResult(-1, "系统内部错误(-1)");
            }
        }

        /// <summary>
        /// 绑定用户
        /// </summary>
        /// <param name="openId">微信用户唯一标识</param>
        /// <param name="userId">系统用户唯一标识</param>
        /// <param name="captcha">验证码</param>
        /// <param name="token">服务端分配的key</param>
        /// <returns></returns>
        public OperationResult BindUser(string openId, string userId, string captcha, string token)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(openId))
                    return new OperationResult(1, "用户唯一标识为空");
                if (string.IsNullOrWhiteSpace(captcha))
                    return new OperationResult(2, "验证码错误,请重新输入");
                if (string.IsNullOrWhiteSpace(token))
                    return new OperationResult(3, "token 为空");
                if (string.IsNullOrWhiteSpace(userId))
                    return new OperationResult(4, "用户编号异常");
                return ParterService.BindUser(openId, userId, captcha, token);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.ToString());
                return new OperationResult(-1, "系统内部错误(-1)");
            }  
        }

        public OperationResult RelieveBind(long userSysNo, long? modifyUserSysNo)
        {
            try
            {
                if (userSysNo <= 0)
                    return new OperationResult(1, "绑定用户系统编号错误");
                return ParterService.RelieveBind(userSysNo, modifyUserSysNo);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.ToString());
                return new OperationResult(-1, "系统内部错误(-1)");
            }
        }

        public OperationResult ValidateAuth2CallResult(string code, string appNo, int agentId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(code))
                    return new OperationResult(1, "用户未授权");
                if (string.Compare(appNo, "weixin", true) != 0)
                    return new OperationResult(2, "应用编号异常");
                return ParterService.ValidateAuth2CallResult(code, agentId);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.ToString());
                return new OperationResult(-1, "系统内部错误");
            }
        }

        public OperationResult CreateMenus(IList<Dictionary<string, object>> menus, AgentType? agentType = null)
        {
            return ParterService.CreateMenus(menus, agentType);
        }
        /// <summary>
        /// 创建企业号部门
        /// </summary>
        /// <param name="name">部门名称</param>
        /// <param name="parentId">
        /// 上级部门id
        /// 如是顶级节点， parentId为1</param>
        /// <param name="id">
        /// 部门id 整形
        /// 如未指定，由系统自动生成
        /// </param>
        /// <param name="order">
        /// 排序顺序
        /// order值小的排序靠前
        /// </param>
        /// <returns></returns>
        public OperationResult CreateDepartment(string name, int parentId, int order, int? id = null)
        {
            if (string.IsNullOrWhiteSpace(name))
                return new OperationResult(1, "部门名称为空");
            if (name.Length > 64)
                name = name.Substring(0, 64);
            if (parentId <= 0)
                return new OperationResult(2, "上级部门编号异常");
            return ParterService.CreateDepartment(name, parentId, order, id);
        }

        public void DeleteDepartment(string did)
        {
            var result = ParterService.DeleteDepartment(did);
        }

        public OperationResult CreateUser(string userId, string name, string mobile = null, string email = null, string weixinid = null, string position = null, IEnumerable<string> departmentIds = null, string identityNo = null)
        {
            return ParterService.CreateUser(userId, name, mobile, email, weixinid, position, departmentIds, identityNo);
        }

        public OperationResult InviteUserSubscribe(string userId, string invateContent = null)
        {
            return ParterService.InviteUserSubscribe(userId, invateContent);
        }
    }
}
