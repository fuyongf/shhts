﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTB.DevFx;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy.Log;
using PinganHouse.SHHTS.RemoteServiceProxy.ServiceContracts;
using PinganHouse.SHHTS.RemoteServiceProxy.Utils;

namespace PinganHouse.SHHTS.RemoteServiceProxy
{
    public class UserServiceProxy
    {
        private const string USERNAME_CACHEKEY_PREFIX = "UN_";

        static IUserService UserService
        {
            get
            {
                return ServiceHelper.GetObject<IUserService>("UserRemoteService");
            }
        }

        /// <summary>
        /// 获取用户真实姓名
        /// </summary>
        /// <param name="userSysNo">用户系统编号(自然人系统编号，如：Customer.UserSysNo)</param>
        /// <returns></returns>
        public static string GetUserName(long userSysNo)
        {
            try
            {
                string name = null;
                name = CacheHelper<string>.Instance.Get(string.Concat(USERNAME_CACHEKEY_PREFIX, userSysNo));
                if (name == null)
                    name = UserService.GetUserName(userSysNo);
                if (name != null)
                    CacheHelper<string>.Instance.Set(string.Concat(USERNAME_CACHEKEY_PREFIX, userSysNo), name, DateTime.Now.AddHours(1));
                return name;
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="loginId"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static OperationResult Login(string loginId, string password)
        {
            if (string.IsNullOrWhiteSpace(loginId) || string.IsNullOrWhiteSpace(password))
                return new OperationResult(-1, "用户名密码不能为空");
            try
            {
                return UserService.Login(loginId, password.Trim());
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        public static OperationResult ChangePassword(long userSysNo, string oldPassword, string newPassword)
        {
            if (string.IsNullOrWhiteSpace(oldPassword) || string.IsNullOrWhiteSpace(newPassword))
                return new OperationResult(-1);
            try
            {
                return UserService.ChangePassword(userSysNo, oldPassword.Trim(), newPassword.Trim());
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-4, "操作失败");
            }
        }

        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="condition"></param>
        /// <param name="matchingType"></param>
        /// <returns></returns>
        public static IList<User> GetUsers(int pageIndex, int pageSize, out int totalCount, UserType? userType = null, string condition = null)
        {
            try
            {
                return UserService.GetUsers(pageIndex, pageSize, out totalCount, userType, condition);
            }
            catch (Exception e)
            {
                totalCount = 0;
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 获取身份认证信息
        /// </summary>
        /// <param name="identityNo"></param>
        /// <returns></returns>
        public static UserIdentity GetUserIdentityInfo(string identityNo)
        {
            return UserService.GetUserIdentity(identityNo);
        }

        /// <summary>
        /// 根据用户编号获取身份认证信息
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        public static UserIdentity GetUserIdentityInfo(long userSysNo, UserType userType)
        {
            return UserService.GetUserIdentityByUser(userSysNo, userType);
        }

        /// <summary>
        /// 绑定用户身份认证信息
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <param name="identity"></param>
        /// <returns></returns>
        public static OperationResult BindUserIdentity(long userSysNo, UserType userType, UserIdentityModel identity, long? operatorSysNo = null)
        {
            if (identity == null || string.IsNullOrWhiteSpace(identity.IdentityNo) || string.IsNullOrWhiteSpace(identity.Name))
                return new OperationResult(-1, "身份认证信息不完整");
            try
            {
                return UserService.BindUserIdentity(userSysNo, userType, new UserIdentity
                {
                    CertificateType = identity.CertificateType,
                    Nationality = identity.Nationality,
                    Id = identity.IdentityNo,
                    Name = identity.Name,
                    Gender = identity.Gender,
                    Birthday = identity.Birthday,
                    Address = identity.Address,
                    VisaAgency = identity.VisaAgency,
                    Nation = identity.Nation,
                    EffectiveDate = identity.EffectiveDate,
                    ExpiryDate = identity.ExpiryDate,
                    Photo = identity.Photo,
                    IsTrusted = identity.IsTrusted,
                    CreateUserSysNo = operatorSysNo,
                    CreateDate = DateTime.Now
                });
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 采集指纹信息
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <param name="fpCode"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        public static OperationResult CollectFingerprint(long userSysNo, string fpCode, string tag, long operatorSysNo)
        {
            try
            {
                return UserService.CollectFinger(userSysNo, fpCode, tag, operatorSysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 删除指纹信息
        /// </summary>
        /// <param name="fpSysNo"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        public static OperationResult DeleteFingerprint(long fpSysNo, long operatorSysNo)
        {
            try
            {
                return UserService.DeleteFinger(fpSysNo, operatorSysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 根据用户系统编号获取指纹信息
        /// </summary>
        /// <param name="userSysNo">用户系统编号(自然人系统编号，如：Customer.UserSysNo)</param>
        /// <returns></returns>
        public static IList<UserFingerprint> GetFingerByUserSysNo(long userSysNo) 
        {
            try
            {
                return UserService.GetFingerByUserSysNo(userSysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                throw;
            }
        }

        /// <summary>
        /// 更新发送短信标识
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <param name="isSendSms"></param>
        /// <returns></returns>
        public OperationResult UpdateSendSmsFlag(long userSysNo, bool isSendSms, long? createUserSysNo)
        {
            try
            {
                if (userSysNo <= 0)
                    return new OperationResult(1,"用户系统编号异常");
                return UserService.UpdateSendSmsFlag(userSysNo, isSendSms, createUserSysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                throw;
            }
        }

    }

    public class CustomerServiceProxy
    {
        static IUserService UserService
        {
            get
            {
                return ObjectService.GetObject<IUserService>();
            }
        }

        /// <summary>
        /// 创建买家信息
        /// </summary>
        /// <param name="buyer"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        public static OperationResult CreateBuyer(UserIdentityModel buyer, long? operatorSysNo)
        {
            if (buyer == null || string.IsNullOrWhiteSpace(buyer.IdentityNo) || string.IsNullOrWhiteSpace(buyer.Name))
                return new OperationResult(-1, "客户身份信息不完整");
            var identity = new UserIdentity
            {
                CertificateType = buyer.CertificateType,
                Nationality = buyer.Nationality,
                Id = buyer.IdentityNo,
                Name = buyer.Name,
                Gender = buyer.Gender,
                Birthday = buyer.Birthday,
                Address = buyer.Address,
                VisaAgency = buyer.VisaAgency,
                Nation = buyer.Nation,
                EffectiveDate = buyer.EffectiveDate,
                ExpiryDate = buyer.ExpiryDate,
                Photo = buyer.Photo,
                IsTrusted = buyer.IsTrusted,
                CreateUserSysNo = operatorSysNo,
                CreateDate = DateTime.Now
            };
            var customer = new Customer
            {
                CustomerType = CustomerType.Buyer,
                RealName = buyer.Name,
                Gender = buyer.Gender,
                CertificateType = buyer.CertificateType,
                IdentityNo = buyer.IdentityNo,

                CreateUserSysNo = operatorSysNo,
                CreateDate = DateTime.Now
            };

            try
            {
                return UserService.CreateCustomer(customer, identity);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 创建卖家信息
        /// </summary>
        /// <param name="seller"></param>
        /// <returns></returns>
        public static OperationResult CreateSeller(UserIdentityModel seller, long? operatorSysNo)
        {
            if (seller == null || string.IsNullOrWhiteSpace(seller.IdentityNo) || string.IsNullOrWhiteSpace(seller.Name))
                return new OperationResult(-1, "客户身份信息不完整");
            var identity = new UserIdentity
            {
                CertificateType = seller.CertificateType,
                Nationality = seller.Nationality,
                Id = seller.IdentityNo,
                Name = seller.Name,
                Gender = seller.Gender,
                Birthday = seller.Birthday,
                Address = seller.Address,
                VisaAgency = seller.VisaAgency,
                Nation = seller.Nation,
                EffectiveDate = seller.EffectiveDate,
                ExpiryDate = seller.ExpiryDate,
                Photo = seller.Photo,
                IsTrusted = seller.IsTrusted,
                CreateUserSysNo = operatorSysNo,
                CreateDate = DateTime.Now
            };
            var customer = new Customer
            {
                CustomerType = CustomerType.Seller,
                RealName = seller.Name,
                //Mobile = seller.Mobile,
                //TelPhone = seller.TelPhone,
                Gender = seller.Gender,
                CertificateType = seller.CertificateType,
                IdentityNo = seller.IdentityNo,

                CreateUserSysNo = operatorSysNo,
                CreateDate = DateTime.Now
            };

            try
            {
                return UserService.CreateCustomer(customer, identity);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }
        /// <summary>
        /// 创建卖家信息
        /// </summary>
        /// <param name="seller"></param>
        /// <returns></returns>
        public static OperationResult CreateUser(UserIdentityModel seller, long? operatorSysNo)
        {
            if (seller == null || string.IsNullOrWhiteSpace(seller.IdentityNo) || string.IsNullOrWhiteSpace(seller.Name))
                return new OperationResult(-1, "客户身份信息不完整");
            var identity = new UserIdentity
            {
                CertificateType = seller.CertificateType,
                Nationality = seller.Nationality,
                Id = seller.IdentityNo,
                Name = seller.Name,
                Gender = seller.Gender,
                Birthday = seller.Birthday,
                Address = seller.Address,
                VisaAgency = seller.VisaAgency,
                Nation = seller.Nation,
                EffectiveDate = seller.EffectiveDate,
                ExpiryDate = seller.ExpiryDate,
                Photo = seller.Photo,
                IsTrusted = seller.IsTrusted,
                CreateUserSysNo = operatorSysNo,
                CreateDate = DateTime.Now
            };
            var customer = new Customer
            {
                CustomerType = CustomerType.Seller,
                RealName = seller.Name,
                //Mobile = seller.Mobile,
                //TelPhone = seller.TelPhone,
                Gender = seller.Gender,
                CertificateType = seller.CertificateType,
                IdentityNo = seller.IdentityNo,

                CreateUserSysNo = operatorSysNo,
                CreateDate = DateTime.Now
            };

            try
            {
                return UserService.CreateCustomer(customer, identity);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        //public static OperationResult PerfectBuyer(string caseId, BuyerModel buyer, IDictionary<long, string> mobiles, IDictionary<long, string> emails, long? operatorSysNo)
        //{
        //    if (string.IsNullOrWhiteSpace(caseId))
        //        return new OperationResult(-1, "案件编号错误");

        //    try
        //    {
        //        var customer = new Customer
        //        {
        //            //Mobile = buyer.Mobile,
        //            //TelPhone = buyer.TelPhone,
        //            CustomerNature = buyer.CustomerNature,
        //            PostAddress = buyer.PostAddress,
        //            PostProvince = buyer.PostProvince,
        //            PostCity = buyer.PostCity,
        //            PostArea = buyer.PostArea,
        //            PostTime = buyer.PostTime,
        //            AddressType = buyer.AddressType,
        //            PostTimeType = buyer.PostTimeType,
        //            PostCode = buyer.PostCode,
        //            Recipients = buyer.Recipients,
        //            RecipientsMobile = buyer.RecipientsMobile,
        //            IsCredit = buyer.IsCredit,
        //            CreditType = buyer.CreditType,
        //            SelfCredit = buyer.SelfCredit,
        //            SelfCreditReson = buyer.SelfCreditReson,
        //            ModifyDate = DateTime.Now,
        //            ModifyUserSysNo = operatorSysNo
        //        };
        //        return UserService.PerfectBuyer(caseId, customer, mobiles, emails, operatorSysNo);
        //    }
        //    catch (Exception e)
        //    {
        //        LogHandler.Write(e.ToString());
        //    }
        //    return new OperationResult(-4, "系统错误");
        //}

        //public static OperationResult PerfectSeller(string caseId, SellerModel seller, IDictionary<long, string> mobiles, IDictionary<long, string> emails, long? operatorSysNo)
        //{
        //    if (string.IsNullOrWhiteSpace(caseId))
        //        return new OperationResult(-1, "案件编号错误");

        //    try
        //    {
        //        var customer = new Customer
        //        {
        //            //Mobile = seller.Mobile,
        //            //TelPhone = seller.TelPhone,
        //            CustomerNature = seller.CustomerNature,
        //            PostAddress = seller.PostAddress,
        //            PostProvince = seller.PostProvince,
        //            PostCity = seller.PostCity,
        //            PostArea = seller.PostArea,
        //            PostTime = seller.PostTime,
        //            AddressType = seller.AddressType,
        //            PostTimeType = seller.PostTimeType,
        //            PostCode = seller.PostCode,
        //            Recipients = seller.Recipients,
        //            RecipientsMobile = seller.RecipientsMobile,
        //            IsGuaranty = seller.IsGuaranty,
        //            IsSelfRefund = seller.IsSelfRefund,

        //            ModifyDate = DateTime.Now,
        //            ModifyUserSysNo = operatorSysNo
        //        };
        //        return UserService.PrefectSeller(caseId, customer, mobiles, emails, operatorSysNo);
        //    }
        //    catch (Exception e)
        //    {
        //        LogHandler.Write(e.ToString());
        //    }
        //    return new OperationResult(-4, "系统错误");
        //}

        /// <summary>
        /// 创建客户信息
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        //public static OperationResult CreateCustomer(CustomerModel customer) 
        //{
        //    try
        //    {
        //        customer.UserType = UserType.Customer;
        //        return UserService.CreateCustomer(customer);
        //    }
        //    catch (Exception e) 
        //    {
        //        LogHandler.Write(e.ToString());
        //        return new OperationResult(-5, "系统错误");
        //    }
        //}

        /// <summary>
        /// 根据系统编号获取用户
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        public static Customer GetCustomer(long sysNo)
        {
            try
            {
                return UserService.GetCustomer(sysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 根据用户ID获取用户
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        //public static Customer GetCustomer(string userId)
        //{
        //    try
        //    {
        //        return UserService.GetCustomer(userId);
        //    }
        //    catch (Exception e)
        //    {
        //        LogHandler.Write(e.ToString());
        //        return null;
        //    }
        //}

        /// <summary>
        /// 根据案件编号获取客户信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public static IList<Customer> GetCustomerByCase(string caseId)
        {
            try
            {
                return UserService.GetCustomerByCase(caseId);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 分页获取用户信息
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="customerType"></param>
        /// <param name="condition"></param>
        /// <param name="matchingType"></param>
        /// <returns></returns>
        public static IList<Customer> GetCustomers(int pageIndex, int pageSize, out int totalCount, CustomerType? customerType = null, string condition = null)
        {
            try
            {
                return UserService.GetCustomers(pageIndex, pageSize, out totalCount, customerType, condition);
            }
            catch (Exception e)
            {
                totalCount = 0;
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 根据用户系统编号获取证件号码
        /// </summary>
        /// <param name="userSysNos"></param>
        /// <returns></returns>
        public static IDictionary<long, string> GetUserIdentityNoByCustomerSysNos(params long[] userSysNos)
        {
            try
            {
                return UserService.GetUserIdentityNoByCustomerSysNos(userSysNos);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                throw;
            }
        }

        /// <summary>
        /// 根据手机获取客户及案件
        /// </summary>
        /// <param name="mobile">手机号码</param>
        /// <returns>key:自然人系统编号, value:客户明细</returns>
        public static Dictionary<long, IList<CustomerDetail>> GetCustomerDetails(string mobile)
        {
            if (string.IsNullOrWhiteSpace(mobile))
                return null;
            try
            {
                return UserService.GetCustomerDetails(mobile);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return null;
            }
        }

        public static Dictionary<long, IList<CustomerDetail>> GetCustomerDetailsByIdentityNo(string identityNo)
        {
            if (string.IsNullOrWhiteSpace(identityNo))
                return null;
            try
            {
                return UserService.GetCustomerDetailsByIdentityNo(identityNo);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return null;
            }
        }


        /// <summary>
        /// 修改客户信息
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        //public static OperationResult ModifyCustomer(Customer customer)
        //{
        //    try
        //    {
        //        return UserService.ModifyCustomer(customer);
        //    }
        //    catch (Exception e)
        //    {
        //        LogHandler.Write(e.ToString());
        //        return new OperationResult(-5, "系统错误");
        //    }
        //}

        //    /// <summary>
        //    /// 增加客户标签
        //    /// </summary>
        //    /// <param name="customerTags"></param>
        //    /// <returns></returns>
        //    public static OperationResult AddCustomerTag(IEnumerable<CustomerTag> customerTags)
        //    {
        //        try
        //        {
        //            return UserService.AddCustomerTag(customerTags);
        //        }
        //        catch (Exception e)
        //        {
        //            LogHandler.Write(e.ToString());
        //            return new OperationResult(-5, "系统错误");
        //        }
        //    }

        //    /// <summary>
        //    /// 删除客户标签
        //    /// </summary>
        //    /// <param name="customerSysNo"></param>
        //    /// <param name="tag"></param>
        //    /// <returns></returns>
        //    public static OperationResult RemoveCustomerTag(long customerSysNo, IEnumerable<string> tags)
        //    {
        //        try
        //        {
        //            return UserService.RemoveCustomerTag(customerSysNo, tags);
        //        }
        //        catch (Exception e)
        //        {
        //            LogHandler.Write(e.ToString());
        //            return new OperationResult(-5, "系统错误");
        //        }
        //    }

        //    /// <summary>
        //    /// 获取所有客户标签
        //    /// </summary>
        //    /// <returns></returns>
        //    public static IList<string> GetCustomerTags()
        //    {
        //        try
        //        {
        //            return UserService.GetCustomerTags();
        //        }
        //        catch (Exception e)
        //        {
        //            LogHandler.Write(e.ToString());
        //            return null;
        //        }
        //    }

        //    /// <summary>
        //    /// 获取指定客户的标签
        //    /// </summary>
        //    /// <param name="customerSysNo"></param>
        //    /// <returns></returns>
        //    public static IList<CustomerTag> GetCustomerTags(long customerSysNo)
        //    {
        //        try
        //        {
        //            return UserService.GetCustomerTags(customerSysNo);
        //        }
        //        catch (Exception e)
        //        {
        //            LogHandler.Write(e.ToString());
        //            return null;
        //        }
        //    }
    }

    public class PinganStaffServiceProxy
    {
        static IUserService UserService
        {
            get
            {
                return ObjectService.GetObject<IUserService>();
            }
        }

        /// <summary>
        /// 创建平安员工信息
        /// </summary>
        /// <param name="pinganStaff"></param>
        /// <returns></returns>
        public static OperationResult CreatePinganStaff(PinganStaffModel pinganStaff, string passWord, long? operatorSysNo)
        {
            if (pinganStaff == null || string.IsNullOrWhiteSpace(pinganStaff.Name) || string.IsNullOrWhiteSpace(pinganStaff.UMCode))
                return new OperationResult(-1, "员工身份信息不完整");
            var dto = new PinganStaff
            {
                RealName = pinganStaff.Name,
                Mobile = pinganStaff.Mobile,
                Gender = pinganStaff.Gender,
                CertificateType = pinganStaff.CertificateType,
                IdentityNo = pinganStaff.IdentityNo,
                TelPhone = pinganStaff.TelPhone,

                UMCode = pinganStaff.UMCode,
                ReceptionCenter = pinganStaff.ReceptionCenter,
                StaffNo = pinganStaff.StaffNo,
                StaffCardNo = pinganStaff.StaffCardNo,
                Duty = pinganStaff.Duty,

                CreateUserSysNo = operatorSysNo,
                CreateDate = DateTime.Now
            };

            try
            {
                return UserService.CreatePinganStaff(dto, passWord);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 根据系统编号获取用户
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        public static PinganStaff GetPinganStaff(long sysNo)
        {
            try
            {
                return UserService.GetPinganStaff(sysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 根据用户ID获取用户
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        //public static PinganStaff GetPinganStaff(string userId)
        //{
        //    try
        //    {
        //        return UserService.GetPinganStaff(userId);
        //    }
        //    catch (Exception e)
        //    {
        //        LogHandler.Write(e.ToString());
        //        return null;
        //    }
        //}

        /// <summary>
        /// 分页获取平安员工信息
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="condition"></param>
        /// <param name="matchingType"></param>
        /// <returns></returns>
        public static IList<PinganStaff> GetPinganStaffs(int pageIndex, int pageSize, out int totalCount, string condition = null)
        {
            try
            {
                return UserService.GetPinganStaffs(pageIndex, pageSize, out totalCount, condition);
            }
            catch (Exception e)
            {
                totalCount = 0;
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 修改平安员工信息
        /// </summary>
        /// <param name="pinganStaff"></param>
        /// <returns></returns>
        public static OperationResult ModifyPinganStaff(PinganStaff pinganStaff)
        {
            try
            {
                return UserService.ModifyPinganStaff(pinganStaff);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 根据UM帐号获取员工信息
        /// </summary>
        /// <param name="um"></param>
        /// <returns></returns>
        public static PinganStaff GetByUMCode(string um)
        {
            if (string.IsNullOrWhiteSpace(um))
                return null;
            return UserService.GetByPinganStaffUMCode(um);
        }

        public static PinganStaff GetByStaffCardNo(string staffCardNo)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(staffCardNo))
                    return null;
                return UserService.GetByStaffCardNo(staffCardNo);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.ToString());
            }
            return null;
        }

        public static OperationResult GetStaffByFingerprint(string fpCode)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(fpCode))
                    return new OperationResult(1, "指纹为空");
                return UserService.GetStaffByFingerprint(fpCode);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.ToString());
                return new OperationResult(-1, "系统内部错误");
            }
        }
 
    }

    public class ThirdpartyUserServiceProxy
    {
        static IUserService UserService
        {
            get
            {
                return ObjectService.GetObject<IUserService>();
            }
        }

        /// <summary>
        /// 创建第三方公司员工信息
        /// </summary>
        /// <param name="tUser"></param>
        /// <returns></returns>
        public static OperationResult CreateThirdpartyUser(ThirdpartyUserModel tUser, long? operatorSysNo)
        {
            if (tUser == null || string.IsNullOrWhiteSpace(tUser.Name))
                return new OperationResult(-1, "员工身份信息不完整");
            var dto = new ThirdpartyUser
            {
                RealName = tUser.Name,
                Mobile = tUser.Mobile,
                Gender = tUser.Gender,
                IdentityNo = tUser.IdentityNo,
                TelPhone = tUser.TelPhone,

                //LoginId = tUser.LoginName,

                TpName = tUser.TpName,

                CreateUserSysNo = operatorSysNo,
                CreateDate = DateTime.Now
            };

            try
            {
                return UserService.CreateThirdpartyUser(dto, tUser.LoginName, tUser.Password);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        /// <summary>
        /// 根据系统编号获取用户
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        public static ThirdpartyUser GetThirdpartyUser(long sysNo)
        {
            try
            {
                return UserService.GetThirdpartyUser(sysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 根据用户ID获取用户
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        //public static ThirdpartyUser GetThirdpartyUser(string userId)
        //{
        //    try
        //    {
        //        return UserService.GetThirdpartyUser(userId);
        //    }
        //    catch (Exception e)
        //    {
        //        LogHandler.Write(e.ToString());
        //        return null;
        //    }
        //}

        /// <summary>
        /// 分页获取第三方用户信息
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="condition"></param>
        /// <param name="matchingType"></param>
        /// <returns></returns>
        public static IList<ThirdpartyUser> GetThirdpartyUsers(int pageIndex, int pageSize, out int totalCount, string condition = null, string tpName = null)
        {
            try
            {
                return UserService.GetThirdpartyUsers(pageIndex, pageSize, out totalCount, condition, tpName);
            }
            catch (Exception e)
            {
                totalCount = 0;
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 修改第三方员工信息
        /// </summary>
        /// <param name="tpUser"></param>
        /// <returns></returns>
        public static OperationResult ModifyThirdpartyUser(ThirdpartyUser tpUser)
        {
            try
            {
                return UserService.ModifyThirdpartyUser(tpUser);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }
    }
}
