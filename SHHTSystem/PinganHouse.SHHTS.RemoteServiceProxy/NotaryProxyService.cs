﻿using PinganHouse.SHHTS.RemoteServiceProxy.ServiceContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.DataTransferObjects.Filters;
using PinganHouse.SHHTS.RemoteServiceProxy.Utils;
using PinganHouse.SHHTS.RemoteServiceProxy.Log;

namespace PinganHouse.SHHTS.RemoteServiceProxy
{
    public class NotaryProxyService
    {
        readonly static NotaryProxyService notaryProxyService = new NotaryProxyService();

        public static NotaryProxyService GetInstanse()
        {
            return notaryProxyService;
        }

        protected INotaryService NotaryService
        {
            get { return ServiceHelper.GetObject<INotaryService>("NotaryProxyService"); }
        }

        public NotaryDto Get(long sysNo)
        {
            return NotaryService.Get(sysNo);
        }

        public NotaryDto Get(string name, string mobile)
        {
            return NotaryService.Get(name, mobile);
        }

        public NotaryDto Insert(string name, string mobile, string officeName, long createUserSysNo)
        {
            return NotaryService.Insert(name, mobile, officeName, createUserSysNo);
        }

        public IList<NotaryDto> Search(NotaryFilter filter)
        {
            return NotaryService.Search(filter);
        }

        public OperationResult Update(long sysNo, string name, string mobile, string officeName, long modifyUserSysNo)
        {
            try
            {
                NotaryService.Update(sysNo, name, mobile, officeName, modifyUserSysNo);

                return new OperationResult(0);
            }
            catch (Exception e)
            {
                return new OperationResult(-1, e.Message);
            }
        }
    }
}
