﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using HTB.DevFx;

namespace PinganHouse.SHHTS.RemoteServiceProxy.Utils
{
    internal class ServiceHelper
    {
        public static T GetObject<T>(string alias)
        {
            return ObjectService.GetObject<T>(string.Concat(alias, AppDomain.CurrentDomain.GetData("ServiceSuffix") as string));
        }
    }
}
