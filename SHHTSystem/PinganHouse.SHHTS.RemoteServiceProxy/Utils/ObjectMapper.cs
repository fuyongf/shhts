﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Reflection;

namespace PinganHouse.SHHTS.RemoteServiceProxy.Utils
{
    public class ObjectMapper
    {
        private static Dictionary<Type, PropertyInfo[]> ObjectProperties = new Dictionary<Type, PropertyInfo[]>();
        private static object SyncRoot = new object();
        /// <summary>
        /// 对象间转换(浅复制)
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <typeparam name="TInput"></typeparam>
        /// <param name="input"></param>
        /// <returns></returns>
        public static T Copy<T>(object input) where T : new()
        {
            if (input == null)
                throw new ArgumentNullException("input", "input object can't be null.");
            Type inputType = input.GetType();
            Type resultType = typeof(T);
            PropertyInfo[] resultProperties = GetTypeProperties(resultType);
            PropertyInfo[] properties = GetTypeProperties(inputType);
            return Get<T>(resultProperties, properties, input);
        }

        private static T Get<T>(PropertyInfo[] resultProperties, PropertyInfo[] inputProperties, object input)
            where T : new()
        {
            T result = new T();
            if (inputProperties != null && inputProperties.Length > 0)
            {

                foreach (PropertyInfo p in inputProperties)
                {
                    if (!p.CanRead)
                        continue;
                    PropertyInfo resultProperty = resultProperties.Where(m => m.Name == p.Name).FirstOrDefault();
                    if (resultProperty == null || !resultProperty.CanWrite)
                        continue;
                    try
                    {
                        resultProperty.SetValue(result, p.GetValue(input, null), null);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            return result;
        }
   

        private static PropertyInfo[] GetTypeProperties(Type type)
        {
            if (!ObjectProperties.ContainsKey(type))
            {
                lock (SyncRoot)
                {
                    if (!ObjectProperties.ContainsKey(type))
                    {
                        ObjectProperties.Add(type, type.GetProperties(BindingFlags.Instance | BindingFlags.Public));
                    }
                }
            }
            return ObjectProperties[type];
        }
    }
}
