﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using HTB.DevFx;

using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.DataTransferObjects;

using PinganHouse.SHHTS.RemoteServiceProxy.Log;
using PinganHouse.SHHTS.RemoteServiceProxy.ServiceContracts;
using PinganHouse.SHHTS.RemoteServiceProxy.Utils;

namespace PinganHouse.SHHTS.RemoteServiceProxy
{
    public class TradeServiceProxy
    {
        static ITradeService TradeService
        {
            get
            {
                return ServiceHelper.GetObject<ITradeService>("TradeProxyService");
            }
        }

        /// <summary>
        /// 根据订单号获取订单信息
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        public static Order GetOrderInfo(string orderNo)
        {
            return TradeService.GetOrderInfo(orderNo);
        }

        public static OrderDetail GetOrderDetail(string orderNo)
        {
            return TradeService.GetOrderDetail(orderNo);
        }

        /// <summary>
        /// 根据交易编号获取交易信息
        /// </summary>
        /// <param name="tradeNo"></param>
        /// <returns></returns>
        public static TradeDetail GetTradeInfo(string tradeNo)
        {
            return TradeService.GetTradeInfo(tradeNo);
        }

        /// <summary>
        /// 获取订单列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="center"></param>
        /// <param name="caseId"></param>
        /// <param name="accountType"></param>
        /// <param name="orderType"></param>
        /// <param name="channel"></param>
        /// <param name="orderNo"></param>
        /// <param name="createUserName"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static IList<OrderPaginatedInfo> GetOrders(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, string caseId, AccountType? accountType, OrderType? orderType, PaymentChannel? channel, string orderNo, string createUserName, DateTime? startDate, DateTime? endDate)
        {
            return TradeService.GetOrders(pageIndex, pageSize, out totalCount, center, caseId, accountType, orderType, channel, orderNo, createUserName, startDate, endDate);
        }

        /// <summary>
        /// 根据客户系统编号获取订单列表(for:wechat)
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="customerSysNos"></param>
        /// <param name="caseId">案件编号(可空，如不为空则只取指定案件下传入客户编号的交易信息)</param>
        /// <returns></returns>
        public static IList<TradePaginatedInfo> GetOrders(int pageIndex, int pageSize, out int totalCount, bool isSelf, IEnumerable<long> customerSysNos, string caseId)
        {
            return TradeService.GetOrders(pageIndex, pageSize, out totalCount, isSelf, customerSysNos, caseId);
        }

        /// <summary>
        /// 获取账户订单
        /// </summary>
        /// <param name="accountType"></param>
        /// <param name="accountSubjectId"></param>
        /// <param name="orderType"></param>
        /// <param name="tradeType"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static IList<Order> GetOrders(AccountType accountType, string accountSubjectId, OrderType orderType, OrderBizType? tradeType, DateTime? startDate, DateTime? endDate)
        {
            return TradeService.GetOrders(accountType, accountSubjectId, orderType, tradeType, startDate, endDate);
        }

        /// <summary>
        /// 获取退款订单信息
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="center"></param>
        /// <param name="orderNo"></param>
        /// <param name="createUserName"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static IList<RefundOrderPaginatedInfo> GetRefundOrders(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, string orderNo, string createUserName, DateTime? startDate, DateTime? endDate)
        {
            return TradeService.GetRefundOrders(pageIndex, pageSize, out totalCount, center, orderNo, createUserName, startDate, endDate);
        }

        /// <summary>
        /// 根据订单获取交易明细
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        public static IList<TradeDetail> GetTradeByOrder(string orderNo)
        {
            return TradeService.GetTradeByOrder(orderNo);
        }

        /// <summary>
        /// 获取交易流水
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="center"></param>
        /// <param name="channel"></param>
        /// <param name="tradeNo"></param>
        /// <param name="caseId"></param>
        /// <param name="bankAccountNo"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static IList<TradePaginatedInfo> GetTrades(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, PaymentChannel? channel, string orderNo, string tradeNo, string caseId, string bankAccountNo, DateTime? startDate, DateTime? endDate)
        {
            return TradeService.GetTrades(pageIndex, pageSize, out totalCount, center, channel, orderNo, tradeNo, caseId, bankAccountNo, startDate, endDate);
        }

        /// <summary>
        /// 根据订单号获取Pos票据
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        public static IList<BillDetail> GetPosBillByOrder(string orderNo, bool? isBilled = null)
        {
            if (string.IsNullOrWhiteSpace(orderNo))
                return null;
            return TradeService.GetBillByOrder(orderNo, BillCategory.POS, BillType.Expened, isBilled);
        }

        /// <summary>
        /// 根据案件查询流水
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="caseId"></param>
        /// <param name="orderType"></param>
        /// <returns></returns>
        public static IList<TradePaginatedInfo> GetTradeByCase(int pageIndex, int pageSize, out int totalCount, string caseId, AccountType? accountType, OrderType? orderType)
        {
            totalCount = 0;
            if (string.IsNullOrWhiteSpace(caseId))
                return null;
            try
            {
                return TradeService.GetTradeByCase(pageIndex, pageSize, out totalCount, caseId, accountType, orderType);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 提交充值订单
        /// </summary>
        /// <param name="accountType">账户类型</param>
        /// <param name="accountSubjectId">账户主体编号</param>
        /// <param name="amount">金额</param>
        /// <param name="channel">支付方式</param>
        /// <param name="remark">备注</param>
        /// <returns></returns>
        public static OperationResult SubmitCollection(AccountType accountType, string accountSubjectId, decimal amount, OrderBizType bizType, PaymentChannel channel, string remark, long operatorSysNo)
        {
            try
            {
                return TradeService.SubmitCollection(accountType, accountSubjectId, amount, bizType, channel, remark, operatorSysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统异常");
            }
        }

        ///// <summary>
        ///// 提交交易流水，且创建充值订单
        ///// </summary>
        ///// <param name="accountType">账户类型</param>
        ///// <param name="accountSubjectId">账户主体编号</param>
        ///// <param name="totalAmount">订单总额</param>
        ///// <param name="totalAmount">交易金额</param>
        ///// <param name="channel">支付方式</param>
        ///// <param name="serialNo">交易流水号</param>
        ///// <param name="terminalNo">终端号(POS方式)</param>
        ///// <param name="bankAccountNo">银行帐号</param>
        ///// <param name="bankCode">银行代码</param>
        ///// <param name="accountName">开户名</param>
        ///// <param name="bankName">开户行</param>
        ///// <param name="paymentDate">支付时间</param>
        ///// <param name="remark">备注</param>
        ///// <returns></returns>
        //public static OperationResult SubmitCollection(AccountType accountType, string accountSubjectId, decimal totalAmount, decimal tradeAmount, OrderBizType bizType, PaymentChannel channel, string serialNo, string terminalNo, string bankAccountNo, string bankCode, string accountName, string bankName, DateTime paymentDate, string remark, long operatorSysNo)
        //{
        //    try
        //    {
        //        return TradeService.SubmitCollection(accountType, accountSubjectId, totalAmount, tradeAmount, bizType, channel, serialNo,
        //            terminalNo, bankAccountNo, bankCode, accountName, bankName, paymentDate, remark, operatorSysNo);
        //    }
        //    catch (Exception e)
        //    {
        //        LogHandler.Write(e.ToString());
        //        return new OperationResult(-5, "系统异常");
        //    }
        //}

        /// <summary>
        /// 提交代收交易明细
        /// </summary>
        /// <param name="orderNo">订单编号</param>
        /// <param name="serialNo">交易流水号</param>
        /// <param name="terminalNo">终端号(POS方式)</param>
        /// <param name="amount">交易金额</param>
        /// <param name="bankAccountNo">银行帐号</param>
        /// <param name="bankCode">银行代码</param>
        /// <param name="accountName">开户名</param>
        /// <param name="bankName">开户行</param>
        /// <param name="paymentDate">支付时间</param>
        /// <param name="remark">备注</param>
        /// <returns></returns>
        public static OperationResult SubmitPosTrade(string orderNo, string serialNo, string terminalNo, decimal amount, string bankAccountNo, string bankCode, string accountName, string bankName, DateTime paymentDate, string remark, long operatorSysNo)
        {
            try
            {
                return TradeService.SubmitPosTrade(orderNo, serialNo, terminalNo, amount, bankAccountNo, bankCode,
                    accountName, bankName, paymentDate, remark, operatorSysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统异常");
            }
        }

        /// <summary>
        /// 提交代付申请
        /// </summary>
        /// <param name="accountType">账户类型</param>
        /// <param name="accountSubjectId">账户主体编号</param>
        /// <param name="orderType">订单业务类型</param>
        /// <param name="amount">订单金额</param>
        /// <param name="bankAccountNo">收款银行帐号</param>
        /// <param name="bankCode">收款银行代码</param>
        /// <param name="accountName">收款人</param>
        /// <param name="bankName">收款开户行</param>
        /// <param name="remark">备注</param>
        /// <returns></returns>
        //public static OperationResult SubmitEntrustPay(AccountType accountType, string accountSubjectId, OrderBizType tradeType, decimal amount, string bankAccountNo, string bankCode, string accountName, string bankName, string remark, long operatorSysNo)
        //{
        //    try
        //    {
        //        return TradeService.SubmitEntrustPay(accountType, accountSubjectId, tradeType, amount, bankAccountNo, bankCode, accountName, bankName, remark, operatorSysNo);
        //    }
        //    catch (Exception e)
        //    {
        //        LogHandler.Write(e.ToString());
        //        return new OperationResult(-5, "系统异常");
        //    }
        //}

        /// <summary>
        /// 提交代付申请
        /// </summary>
        /// <param name="payerType">付款账户类型</param>
        /// <param name="payerSubjectId">付款账户主体编号</param>
        /// <param name="payeeType">收款账户类型</param>
        /// <param name="payeeSubjectId">收款账户主体编号</param>
        /// <param name="orderType">订单业务类型</param>
        /// <param name="amount">订单金额</param>
        /// <param name="bankAccountNo">收款银行帐号</param>
        /// <param name="bankCode">收款银行代码</param>
        /// <param name="accountName">收款人</param>
        /// <param name="bankName">收款开户行</param>
        /// <param name="remark">备注</param>
        /// <returns></returns>
        public static OperationResult SubmitEntrustPay(AccountType payerType, string payerSubjectId, AccountType payeeType, string payeeSubjectId, OrderBizType tradeType, decimal amount, string bankAccountNo, string bankCode, string accountName, string bankName, string remark, long operatorSysNo)
        {
            try
            {
                return TradeService.SubmitEntrustPay(payerType, payerSubjectId, payeeType, payeeSubjectId, tradeType, amount, bankAccountNo, bankCode, accountName, bankName, remark, operatorSysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统异常");
            }
        }

        /// <summary>
        /// 提交销帐申请
        /// </summary>
        /// <param name="payerType">付款账户类型</param>
        /// <param name="payerSubjectId">付款账户主体编号</param>
        /// <param name="payeeType">收款账户类型</param>
        /// <param name="payeeSubjectId">收款账户主体编号</param>
        /// <param name="orderType">订单业务类型</param>
        /// <param name="amount">订单金额</param>
        /// <param name="bankAccountNo">收款银行帐号</param>
        /// <param name="bankCode">收款银行代码</param>
        /// <param name="accountName">收款人</param>
        /// <param name="bankName">收款开户行</param>
        /// <param name="remark">备注</param>
        /// <returns></returns>
        public static OperationResult SubmitPaidOff(AccountType payerType, string payerSubjectId, AccountType payeeType, string payeeSubjectId, OrderBizType tradeType, decimal amount, string bankAccountNo, string bankCode, string accountName, string bankName, string remark, long operatorSysNo)
        {
            try
            {
                return TradeService.SubmitPaidOff(payerType, payerSubjectId, payeeType, payeeSubjectId, tradeType, amount, bankAccountNo, bankCode, accountName, bankName, remark, operatorSysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统异常");
            }
        }

        /// <summary>
        /// 提交提现请求
        /// </summary>
        /// <param name="accountType"></param>
        /// <param name="subjectId"></param>
        /// <param name="bizType"></param>
        /// <param name="amount"></param>
        /// <param name="bankAccountNo"></param>
        /// <param name="bankCode"></param>
        /// <param name="accountName"></param>
        /// <param name="bankName"></param>
        /// <param name="remark"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        public static OperationResult SubmitWithdraw(AccountType accountType, string subjectId, OrderBizType bizType, decimal amount, string bankAccountNo, string bankCode, string accountName, string bankName, string remark, long operatorSysNo)
        {
            try
            {
                return TradeService.SubmitWithdraw(accountType, subjectId, bizType, amount, bankAccountNo, bankCode, accountName, bankName, remark, operatorSysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统异常");
            }
        }

        /// <summary>
        /// 取消代收订单
        /// </summary>
        /// <param name="orderNo"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        //public static OperationResult CancelCollectionOrder(string orderNo, long operatorSysNo)
        //{
        //    try
        //    {
        //        return TradeService.CancelCollectionOrder(orderNo, operatorSysNo);
        //    }
        //    catch (Exception e)
        //    {
        //        LogHandler.Write(e.ToString());
        //        return new OperationResult(-5, "系统异常");
        //    }
        //}

        /// <summary>
        /// 撤销代收交易
        /// </summary>
        /// <param name="tradeNo"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        public static OperationResult CancelCollectionTrade(string tradeNo, long operatorSysNo)
        {
            try
            {
                return TradeService.CancelCollectionTrade(tradeNo, operatorSysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统异常");
            }
        }

        /// <summary>
        /// 撤销代收交易
        /// </summary>
        /// <param name="tradeNo"></param>
        /// <param name="bill"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        public static OperationResult CancelCollectionTrade(string tradeNo, BillDetail bill, long operatorSysNo)
        {
            try
            {
                return TradeService.CancelCollectionTrade(tradeNo, bill, operatorSysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统异常");
            }
        }

        /// <summary>
        /// 获取收款掉单信息
        /// </summary>
        /// <param name="confirmed"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static IList<MissCollectionTrade> GetMissCollectionTrade(bool? confirmed, DateTime? startDate, DateTime? endDate)
        {
            return TradeService.GetMissCollectionTrade(confirmed, startDate, endDate);
        }

        /// <summary>
        /// 根据银行卡号获取转账掉单数据
        /// </summary>
        /// <param name="accountNo"></param>
        /// <returns></returns>
        public static IList<MissCollectionTrade> GetMissTransferTradeByAccountNo(string accountNo)
        {
            if (string.IsNullOrWhiteSpace(accountNo))
                return null;
            return TradeService.GetMissTransferTradeByAccountNo(accountNo);
        }

        /// <summary>
        /// 收款补单
        /// </summary>
        /// <param name="sysNo">掉单记录系统编号</param>
        /// <param name="accountType"></param>
        /// <param name="accountSubjectId"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        public static OperationResult SubmitTransferCollection(long sysNo, AccountType accountType, string accountSubjectId, OrderBizType bizType, long operatorSysNo)
        {
            try
            {
                return TradeService.SubmitTransferCollection(sysNo, accountType, accountSubjectId, bizType, operatorSysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统异常");
            }
        }

        /// <summary>
        /// 收款补单
        /// </summary>
        /// <param name="sysNo">掉单记录系统编号</param>
        /// <param name="orderNo"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        //public static OperationResult ReplenishCollectionTrade(long sysNo, string orderNo, long operatorSysNo)
        //{
        //    try
        //    {
        //        return TradeService.ReplenishCollectionTrade(sysNo, orderNo, operatorSysNo);
        //    }
        //    catch (Exception e)
        //    {
        //        LogHandler.Write(e.ToString());
        //        return new OperationResult(-5, "系统异常");
        //    }
        //}

        /// <summary>
        /// 审核收款掉单记录
        /// </summary>
        /// <param name="sysNo">掉单记录系统编号</param>
        /// <param name="tradeNo"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        //public static OperationResult ConfirmMissCollectionTrade(long sysNo, string tradeNo, long operatorSysNo)
        //{
        //    try
        //    {
        //        return TradeService.ConfirmMissCollectionTrade(sysNo, tradeNo, operatorSysNo);
        //    }
        //    catch (Exception e)
        //    {
        //        LogHandler.Write(e.ToString());
        //        return new OperationResult(-5, "系统异常");
        //    }
        //}

        /// <summary>
        /// 提交退款订单
        /// </summary>
        /// <param name="accountType"></param>
        /// <param name="subjectId"></param>
        /// <param name="amount"></param>
        /// <param name="bankAccountNo"></param>
        /// <param name="bankCode"></param>
        /// <param name="accountName"></param>
        /// <param name="bankName"></param>
        /// <param name="remark"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        public static OperationResult SubmitRefundOrder(AccountType accountType, string subjectId, decimal amount, string bankAccountNo, string bankCode, string accountName, string bankName, string remark, long operatorSysNo)
        {
            try
            {
                return TradeService.SubmitRefundOrder(accountType, subjectId, amount, bankAccountNo, bankCode, accountName, bankName, remark, operatorSysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统异常");
            }
        }

        /// <summary>
        /// 提交退款
        /// </summary>
        /// <param name="orderNo"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        public static OperationResult SubmitRefund(string orderNo, string remark, long operatorSysNo)
        {
            try
            {
                return TradeService.SubmitRefund(orderNo, remark, operatorSysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统异常");
            }
        }

        /// <summary>
        /// 付款订单审核
        /// </summary>
        /// <param name="center"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static OperationResult PaymentOrderAudit(ReceptionCenter center, string orderNo, AuditStatus status, long operatorSysNo, string operatorName, string remark = null)
        {
            try
            {
                return TradeService.PaymentOrderAudit(center, orderNo, status, remark, operatorSysNo, operatorName);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统异常");
            }
        }

        /// <summary>
        /// 提现订单审核
        /// </summary>
        /// <param name="center"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static OperationResult WithdrawOrderAudit(ReceptionCenter center, string orderNo, AuditStatus status, long operatorSysNo, string operatorName, string remark = null) 
        {
            try
            {
                return TradeService.WithdrawOrderAudit(center, orderNo, status, remark, operatorSysNo, operatorName);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统异常");
            }
        }

        /// <summary>
        /// 退款订单审核
        /// </summary>
        /// <param name="center"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static OperationResult RefundOrderAudit(ReceptionCenter center, string orderNo, AuditStatus status, long operatorSysNo, string operatorName, string remark = null)
        {
            try
            {
                return TradeService.RefundOrderAudit(center, orderNo, status, remark, operatorSysNo, operatorName);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统异常");
            }
        }

        /// <summary>
        /// 获取订单审核日志
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        public static IList<AuditLog> GetOrderAuditLog(string orderNo)
        {
            try
            {
                return TradeService.GetOrderAuditLog(orderNo);
            }
            catch (Exception e)
            {
                Log.LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 提交消费订单
        /// </summary>
        /// <param name="accountType"></param>
        /// <param name="subjectId"></param>
        /// <param name="amount"></param>
        /// <param name="bizType"></param>
        /// <param name="channel"></param>
        /// <param name="remark"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        public static OperationResult SubmitExpendOrder(AccountType accountType, string subjectId, decimal amount, OrderBizType bizType, PaymentChannel channel, string remark, long operatorSysNo)
        {
            try
            {
                return TradeService.SubmitExpendOrder(accountType, subjectId, amount, bizType, channel, remark, operatorSysNo);
            }
            catch (Exception e)
            {
                Log.LogHandler.Write(e.ToString());
                throw;
            }
        }

        /// <summary>
        /// 提交转账消费订单
        /// </summary>
        /// <param name="sysNo"></param>
        /// <param name="accountType"></param>
        /// <param name="accountSubjectId"></param>
        /// <param name="bizType"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        public static OperationResult SubmitTransferExpend(long sysNo, AccountType accountType, string accountSubjectId, OrderBizType bizType, long operatorSysNo)
        {
            try
            {
                return TradeService.SubmitTransferExpend(sysNo, accountType, accountSubjectId, bizType, operatorSysNo);
            }
            catch (Exception e)
            {
                Log.LogHandler.Write(e.ToString());
                throw;
            }
        }

        /// <summary>
        /// 提交扣款计划申请
        /// </summary>
        /// <param name="accountType">扣款账户类型</param>
        /// <param name="subjectId">扣款账户编号</param>
        /// <param name="deductType">扣款用途</param>
        /// <param name="amount">扣款金额</param>
        /// <param name="operatorSysNo">操作人</param>
        /// <param name="limitSource">限制扣款源交易类型，如果为空，则不限制</param>
        /// <returns></returns>
        public static OperationResult SubmitDeductSchedule(AccountType accountType, string subjectId, OrderBizType deductType, decimal amount, string remark, long operatorSysNo, params OrderBizType[] limitSource)
        {
            try
            {
                return TradeService.SubmitDeductSchedule(accountType, subjectId, deductType, amount, remark, operatorSysNo, limitSource);
            }
            catch (Exception e)
            {
                Log.LogHandler.Write(e.ToString());
                throw;
            }
        }

        /// <summary>
        /// 删除扣款计划
        /// </summary>
        /// <param name="sysNo"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        public static OperationResult DeleteDeductSchedule(long sysNo, long operatorSysNo, long verson)
        {
            try
            {
                return TradeService.DeleteDeductSchedule(sysNo, operatorSysNo, verson);
            }
            catch (Exception e)
            {
                Log.LogHandler.Write(e.ToString());
                throw;
            }
        }

        public static IList<DeductSchedule> GetDeductSchedules(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, string caseId = null, bool? finished = null, DateTime? startDate = null, DateTime? endDate = null)
        {
            try
            {
                return TradeService.GetDeductSchedules(pageIndex, pageSize, out totalCount, center, caseId, finished, startDate, endDate);
            }
            catch (Exception e)
            {
                Log.LogHandler.Write(e.ToString());
                throw;
            }
        }

        /// <summary>
        /// 子账户划转(账户余额用途变更)
        /// </summary>
        /// <param name="accountType"></param>
        /// <param name="subjectId"></param>
        /// <param name="formType"></param>
        /// <param name="toType"></param>
        /// <param name="changeAmount"></param>
        /// <param name="remark"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        public static OperationResult ChangeBalanceType(AccountType accountType, string subjectId, OrderBizType formType, OrderBizType toType, decimal changeAmount, string remark, long operatorSysNo)
        {
            try
            {
                return TradeService.ChangeBalanceType(accountType, subjectId, formType, toType, changeAmount, remark, operatorSysNo);
            }
            catch (Exception e)
            {
                Log.LogHandler.Write(e.ToString());
                throw;
            }
        }
    }
}
