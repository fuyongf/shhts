﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

using HTB.DevFx;

using PinganHouse.SHHTS.DataTransferObjects;

using PinganHouse.SHHTS.RemoteServiceProxy.Log;
using PinganHouse.SHHTS.RemoteServiceProxy.ServiceContracts;
using PinganHouse.SHHTS.RemoteServiceProxy.Utils;

namespace PinganHouse.SHHTS.RemoteServiceProxy
{
    public class SystemServiceProxy
    {
        static ISystemService SystemService
        {
            get
            {
                return ServiceHelper.GetObject<ISystemService>("SystemProxyService");
            }
        }

        /// <summary>
        /// 检测服务网络情况
        /// </summary>
        /// <returns></returns>
        public static OperationResult NetDetection()
        {
            try
            {
                return SystemService.NetDetection();
            }
            catch (EndpointNotFoundException)
            {
                return new OperationResult(-1, "网络错误");
            }
            catch (TimeoutException)
            {
                return new OperationResult(-2, "网络连接超时");
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                throw;
            }
        }
    }
}
