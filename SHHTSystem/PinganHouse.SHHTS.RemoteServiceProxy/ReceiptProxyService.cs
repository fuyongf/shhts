﻿using HTB.DevFx;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy.Log;
using PinganHouse.SHHTS.RemoteServiceProxy.ServiceContracts;
using PinganHouse.SHHTS.RemoteServiceProxy.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.RemoteServiceProxy
{
    public class ReceiptProxyService
    {

        readonly static ReceiptProxyService receiptProxyService = new ReceiptProxyService();
        private ReceiptProxyService()
        { }

        protected IReceiptService ReceiptService
        {
            get { return ServiceHelper.GetObject<IReceiptService>("ReceiptProxyService"); }
        }

        public static ReceiptProxyService GetInstanse()
        {
            return receiptProxyService;
        }


        public OperationResult GenerateReceipt(string orderNo, long createUserSysNo, string address, string postCode, string mobilePhone, ref ReceiptView receiptDto, params string[] tradeNos)
        {
            if (string.IsNullOrWhiteSpace(orderNo) && (tradeNos == null || tradeNos.Length == 0))
                return new OperationResult(1, "参数异常");
            try
            {
                var data = new Dictionary<string, object> { { "MobilePhone", mobilePhone }, { "Address", address }, { "PostCode", postCode } };
                return ReceiptService.GenerateReceipt(orderNo, createUserSysNo, out receiptDto, data, tradeNos);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return new OperationResult(-1, "系统内部错误");
            }
        }

        public ReceiptView PreviewReceipt(string orderNo, params string[] tradeNos)
        {
            if (string.IsNullOrWhiteSpace(orderNo) && (tradeNos == null || tradeNos.Length == 0))
                return null;
            try
            {
                return ReceiptService.PreviewReceipt(orderNo, tradeNos);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return null;
            }
        }

        public ReceiptView GetReceiptInfo(long receiptSysNo)
        {
            if (receiptSysNo <= 0)
                return null;
            try
            {
                return ReceiptService.GetReceiptInfo(receiptSysNo);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 根据订单获取收据信息
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        public IList<ReceiptView> GetReceiptsByOrderNo(string orderNo)
        {
            if (string.IsNullOrWhiteSpace(orderNo))
                return null;
            try
            {
                return ReceiptService.GetReceiptsByOrderNo(orderNo);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return null;
            }
        }

        public OperationResult RePrintReceipt(long receiptSysNo, long createUserSysNo, ref ReceiptView receiptDto, string remark = null)
        {
            if (receiptSysNo <= 0)
                return new OperationResult(1, "参数异常");
            try
            {
                return ReceiptService.RePrintReceipt(receiptSysNo, createUserSysNo, out receiptDto, remark);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return new OperationResult(-1, "系统内部错误");
            }
        }

        public IList<Receipt> GetPaginatedList(int pageIndex, int pageSize, ref int totalCount, ReceptionCenter center, ReceiptStatus? status, string receiptNo)
        {
            try
            {
                return ReceiptService.GetReceipts(pageIndex, pageSize, out totalCount, center, status, receiptNo);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 根据验真码获取收据信息
        /// </summary>
        /// <param name="securityCode"></param>
        /// <returns></returns>
        public ReceiptView GetReceiptBySecurityCode(string securityCode)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(securityCode))
                    return null;
                return ReceiptService.GetReceiptBySecurityCode(securityCode);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 验证开收据权限
        /// </summary>
        /// <param name="identity"></param>
        /// <returns></returns>
        public bool AuthProvideReceiptPermission(string identity)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(identity))
                    return false;
                return ReceiptService.AuthProvideReceiptPermission(identity);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// 回收收据
        /// </summary>
        /// <param name="receiptSysNo"></param>
        /// <returns></returns>
        public OperationResult RecycleReceipt(long receiptSysNo, long operatorSysNo)
        {
            try
            {
                return ReceiptService.RecycleReceipt(receiptSysNo, operatorSysNo);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// 作废收据
        /// </summary>
        /// <param name="receiptSysNo"></param>
        /// <returns></returns>
        public OperationResult AbolishReceipt(long receiptSysNo, long operatorSysNo)
        {
            try
            {
                return ReceiptService.AbolishReceipt(receiptSysNo, operatorSysNo);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                throw;
            }
        }

        public IList<ReceiptTradeInfo> GetPrintReceiptTradesByOrder(string orderNo)
        {
            try
            {
                return ReceiptService.GetPrintReceiptTradesByOrder(orderNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                throw;
            }
        }
    }
}
