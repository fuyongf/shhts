﻿using HTB.DevFx;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy.Log;
using PinganHouse.SHHTS.RemoteServiceProxy.ServiceContracts;
using PinganHouse.SHHTS.RemoteServiceProxy.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace PinganHouse.SHHTS.RemoteServiceProxy
{
    public class AttachmentProxyService
    {
        private Queue<AttachmentContent> m_FileQueue = new Queue<AttachmentContent>(UPLOAD_QUEUE_LENGTH);

        private Thread m_UploadThread = null;
        private const int UPLOAD_INTERVAL = 100;
        private const int UPLOAD_QUEUE_LENGTH = 10;

        readonly static AttachmentProxyService attachmentProxyService = new AttachmentProxyService();

        #region 事件
        /// <summary>
        /// 委托
        /// </summary>
        public delegate void StartEventHandler(Object sender, UploadEventArgs e);
        public delegate void ProcessingEventHandler(Object sender, UploadEventArgs e);
        public delegate void BreakOffEventHandler(Object sender, UploadEventArgs e);
        public delegate void CompletedEventHandler(Object sender, UploadEventArgs e);

        /// <summary>
        /// 事件
        /// </summary>
        public event StartEventHandler OnStart;
        public event CompletedEventHandler OnCompleted;

        /// <summary>
        /// 上传相关事件参数
        /// </summary>
        public class UploadEventArgs : EventArgs
        {
            private AttachmentContent m_AttachInfo;
            private UploadAttachmentResult m_UploadResult;
            private Exception m_Exception;

            public UploadEventArgs(AttachmentContent attachInfo, UploadAttachmentResult uploadResult, Exception ex = null)
            {
                this.m_AttachInfo = attachInfo;
                this.m_UploadResult = uploadResult;
                this.m_Exception = ex;
            }

            public AttachmentContent AttachInfo
            {
                get
                {
                    return m_AttachInfo;
                }
            }
            public UploadAttachmentResult UploadResult { get { return m_UploadResult; } }
            public Exception Exception { get { return m_Exception; } }
        }

        protected virtual void Start(UploadEventArgs e)
        {
            if (OnStart != null)
                OnStart(this, e);
        }
        protected virtual void Completed(UploadEventArgs e)
        {
            if (OnCompleted != null)
                OnCompleted(this, e);
        }
        #endregion

        private AttachmentProxyService()
        {
            m_UploadThread = new Thread(new ThreadStart(BackGroundUpload));
            m_UploadThread.IsBackground = true;
            m_UploadThread.Start();
        }
        protected IAttachmentService AttachmentService
        {
            get { return ServiceHelper.GetObject<IAttachmentService>("AttachmentRemoteService"); }
        }
        public static AttachmentProxyService GetInstanse()
        {
            return attachmentProxyService;
        }
        public UploadAttachmentResult AddAttachment(AttachmentContent attachmentInfo, bool async = false)
        {
            if (attachmentInfo == null || string.IsNullOrWhiteSpace(attachmentInfo.AssociationNo))
                return new UploadAttachmentResult { ResultMessage = "attachment is invalid" };
            if (attachmentInfo.AttachmentContentData == null)
                return new UploadAttachmentResult { ResultMessage = "attachment's content is null" };
            try
            {
                attachmentInfo.AttachmentContentData.Seek(0, SeekOrigin.Begin);
                if (async)
                {
                    m_FileQueue.Enqueue(attachmentInfo);
                    return null;
                }
                return AttachmentService.AddAttachment(attachmentInfo);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return new UploadAttachmentResult { ResultMessage = "系统错误" };
            }
        }


        public AttachmentDtos GetAttachments(string associationNo, int pageIndex, int pageCount, AttachmentType attachmentType = AttachmentType.未知)
        {
            var query = new AttachmentQuery
            {
                AssociationNo = associationNo,
                AttachmentType = attachmentType,
                PageCount = pageCount,
                PageIndex = pageIndex,
            };
            return GetAttachments(query);
        }

        private AttachmentDtos GetAttachments(AttachmentQuery query)
        {
            if (string.IsNullOrWhiteSpace(query.AssociationNo) || query.PageIndex <= 0 || query.PageCount <= 0)
                return null;
            try
            {
                return AttachmentService.GetAttachments(query);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return null;
            }
        }

        public FileDownloadMessage DownloadAttachment(string fileId)
        {
            if (string.IsNullOrWhiteSpace(fileId))
                return null;
            try
            {
                AttachmentQuery query = new AttachmentQuery { FileId = fileId, };
                return AttachmentService.DownloadAttachment(query);
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 获取附件信息
        /// </summary>
        /// <param name="bizNo"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        public IDictionary<AttachmentType, IList<string>> GetAttachments(string bizNo, IEnumerable<AttachmentType> types)
        {
            if (string.IsNullOrWhiteSpace(bizNo))
                return null;
            try
            {
                var attachs = AttachmentService.GetAttachments(bizNo, types);
                if (attachs == null)
                    return null;
                var result = new Dictionary<AttachmentType, IList<string>>();
                foreach (var item in attachs)
                {
                    if (result.ContainsKey(item.AttachmentType))
                        result[item.AttachmentType].Add(item.FileId);
                    else
                        result.Add(item.AttachmentType, new List<string> { item.FileId });
                }
                return result;
            }
            catch (Exception e)
            {
                Log.LogHandler.Write(e.ToString());
                throw;
            }
        }

        public bool DeleteAttachment(string fileId, long modifyUserSysNo)
        {
            if (string.IsNullOrWhiteSpace(fileId))
                return false;
            try
            {
                var query = new DeleteAttachmentElement { FileId = fileId, ModifyUserSysNo = modifyUserSysNo };
                var result = AttachmentService.DeleteAttachment(query);
                return result.Success;
            }
            catch (Exception ex)
            {
                LogHandler.Write(ex.Message);
                return false;
            }
        }

        private void BackGroundUpload()
        {
            while (true)
            {
                if (m_FileQueue.Count > 0)
                {
                    AttachmentContent fileInfo = m_FileQueue.Dequeue();
                    try
                    {
                        var result = AddAttachment(fileInfo);
                        Completed(new UploadEventArgs(fileInfo, result));
                    }
                    catch (Exception e)
                    {
                        Completed(new UploadEventArgs(fileInfo, null, e));
                    }
                }
                Thread.Sleep(UPLOAD_INTERVAL);
            }
        }
    }
}
