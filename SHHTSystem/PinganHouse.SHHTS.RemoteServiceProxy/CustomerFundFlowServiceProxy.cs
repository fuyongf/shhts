﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using HTB.DevFx;

using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.DataTransferObjects;

using PinganHouse.SHHTS.RemoteServiceProxy.Log;
using PinganHouse.SHHTS.RemoteServiceProxy.ServiceContracts;
using PinganHouse.SHHTS.RemoteServiceProxy.Utils;

namespace PinganHouse.SHHTS.RemoteServiceProxy
{
    public class CustomerFundFlowServiceProxy
    {
        static ICustomerFundFlowService CustomerFundFlowService
        {
            get
            {
                return ServiceHelper.GetObject<ICustomerFundFlowService>("CustomerFundFlowRemoteService");
            }
        }

        /// <summary>
        /// 添加资金流转信息
        /// </summary>
        /// <param name="flow"></param>
        /// <returns></returns>
        public static OperationResult Create(CustomerFundFlow flow)
        {
            try
            {
                return CustomerFundFlowService.AddFundFlow(flow);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return new OperationResult(-5, "系统错误");
            }
        }

        // <summary>
        /// 获取客户资金流转信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="customerType"></param>
        /// <returns></returns>
        public static IList<CustomerFundFlow> GetFlows(string caseId, CustomerType? customerType = null)
        {
            if (string.IsNullOrEmpty(caseId))
                return null;
            try
            {
                return CustomerFundFlowService.GetFundFlows(caseId, customerType);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// 删除客户资金流转信息
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        public static OperationResult DeleteFlows(long sysNo, long operatorSysNo)
        {
            try
            {
                return CustomerFundFlowService.DeleteFundFlow(sysNo, operatorSysNo);
            }
            catch (Exception e)
            {
                LogHandler.Write(e.ToString());
                return null;
            }
        }
    }
}
