﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Credit
{
    /// <summary>
    /// LoanApprovalDialog.xaml 的交互逻辑
    /// </summary>
    public partial class LoanApprovalDialog : Window
    {
        /// <summary>
        /// 抵押注销成功-- 卖方抵押已注销 HD5
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="paras"></param>
        /// <param name="dialog"></param>
        /// <param name="reason">不通过原因</param>
        public delegate void DK5DK9Handle(string caseId, bool adopt,string reason,LoanApprovalDialog dialog,Dictionary<string, object> paras=null);
        /// <summary>
        /// 资料补齐原因
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="reason"></param>
        public delegate void DK10Handle(string caseId,string reason,LoanApprovalDialog dialog);
        //贷款审核事件
        public event DK5DK9Handle DK5DK9Event;
        //贷款资料补齐
        public event DK10Handle DK10Event;
        //资料补齐事件

        //案件对象
        private CaseDto m_CaseDto;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="caseId"></param>
        public LoanApprovalDialog(CaseDto caseDto, IList<DataTransferObjects.CaseEvent> caseEvent)
        {
            InitializeComponent();
            this.m_CaseDto = caseDto;
            this.DataContext = caseDto;
            if (caseEvent.Count > 0)
            {
                this.lab_CreateDate.Content = caseEvent[0].CreateDate.ToString("yyyy-MM-dd HH:mm");
            }
            if (!string.IsNullOrEmpty(caseDto.BuyerName))
            {
                lab_Buyer.Content = caseDto.BuyerName.Split(',')[0];
            }
        }

        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        /// <summary>
        /// 贷款通过
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            //是否通过
            bool IsAuditOk = false;
            IsAuditOk=Convert.ToBoolean(auditOk.IsChecked);

            //是否不通过
            bool IsAuditUnOk=false;
            IsAuditUnOk=Convert.ToBoolean(auditUnOk.IsChecked);

            //是否资料补齐
            bool IsAuditOther=false;
            IsAuditOther=Convert.ToBoolean(auditOther.IsChecked);

            if (!IsAuditOk && !IsAuditUnOk && !IsAuditOther)
            {
                MessageBox.Show("请选择审核结果.","系统提示",MessageBoxButton.OK,MessageBoxImage.Warning);
                return; 
            }
            if (IsAuditOk)
            {
                var paras = new Dictionary<string, object>();
                var check = CheckAuditOkResult(out paras);
                if (DK5DK9Event != null && check)
                {
                    DK5DK9Event(m_CaseDto.CaseId, true, null, this, paras);
                }
            }
            else if (IsAuditUnOk)
            {
                btnUnOk_Click(sender, e);
            }
            else if (IsAuditOther)
            {
                btnOther_Click(sender, e);
            }

        }

        /// <summary>
        /// 检查
        /// </summary>
        private bool CheckAuditOkResult(out Dictionary<string, object>  dictionaryData)
        {
            //加入字典
            var paras = new Dictionary<string, object>();
            try
            {
                //贷款金额
                decimal money;
                decimal.TryParse(txt_DKJE.Text.Trim(), out money);
                if (money <= 0)
                {
                    MessageBox.Show("请输入贷款金额.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txt_DKJE.Focus();
                    return false;
                }
                paras.Add("Money", money);
                //贷款成数
                int percentage;
                int.TryParse(txt_DKCS.Text.Trim(), out percentage);
                if (percentage <= 0 || percentage >= 100)
                {
                    MessageBox.Show("请输入正确的贷款成数.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txt_DKCS.Focus();
                    return false;
                }
                paras.Add("Percentage", percentage);
                //贷款年限
                int limitYear;
                int.TryParse(txt_DKNX.Text.Trim(), out limitYear);
                if (limitYear <= 0 || limitYear > 30)
                {
                    MessageBox.Show("请输入正确的贷款年限.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txt_DKNX.Focus();
                    return false;
                }
                paras.Add("LimitYear", limitYear);
                //贷款折扣
                int discount;
                int.TryParse(txt_DKZK.Text.Trim(), out discount);
                if (discount <= 0 || discount >= 100)
                {
                    MessageBox.Show("请输入正确的贷款折扣.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txt_DKZK.Focus();
                    return false;
                }
                paras.Add("Discount", discount);
                //贷款银行
                paras.Add("ResponseObject", Convert.ToString(cb_ResponseObject.SelectedValue));
                //贷款备注
                var remark = txtReason.Text.Trim();
                paras.Add("Remark", remark);
                dictionaryData = paras;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                dictionaryData = paras;
            }
        }

        /// <summary>
        /// 贷款不通过
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUnOk_Click(object sender, RoutedEventArgs e)
        {
            if (DK5DK9Event != null)
            {
                if (!string.IsNullOrEmpty(txtReason.Text.Trim()))
                {
                    DK5DK9Event(m_CaseDto.CaseId, false,txtReason.Text.Trim(),this);
                }
                else {
                    MessageBox.Show("贷款不通过需要填写相关原因.","系统提示",MessageBoxButton.OK,MessageBoxImage.Warning);
                }               
            }
        }

        /// <summary>
        /// 贷款资料补齐
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOther_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtReason.Text.Trim()))
            {
                if (DK10Event != null)
                {
                    DK10Event(m_CaseDto.CaseId,txtReason.Text.Trim(), this);
                }
            }
            else
            {
                MessageBox.Show("补齐资料需要填写相关内容.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        /// <summary>
        /// 页面加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                PageHelper.PageActiveLoadingControl(this.ctrlLoading, "正在获取,请稍候...");
                //加载数据
                var result = await LoadPageData();
                //获取银行
                var bank = result["BANK"] as IDictionary;
                cb_ResponseObject.ItemsSource = bank;
                cb_ResponseObject.DisplayMemberPath = "Value";
                cb_ResponseObject.SelectedValuePath = "Key";
                cb_ResponseObject.SelectedIndex = 0;

                //贷款信息
                var loanCaseEventList = result["DK3"] as IList<CaseEvent>;
                if (loanCaseEventList != null && loanCaseEventList.Count > 0)
                {
                    var loanResult = loanCaseEventList[0].OtherDatas;
                    //贷款金额
                    if (loanResult.ContainsKey("Money"))
                    {
                        txt_DKJE.Text = Convert.ToString(loanResult["Money"]);
                    }
                    //贷款成数
                    if (loanResult.ContainsKey("Percentage"))
                    {
                        txt_DKCS.Text = Convert.ToString(loanResult["Percentage"]);
                    }
                    //贷款年限
                    if (loanResult.ContainsKey("LimitYear"))
                    {
                        txt_DKNX.Text = Convert.ToString(loanResult["LimitYear"]);
                    }
                    //贷款折扣
                    if (loanResult.ContainsKey("Discount"))
                    {
                        txt_DKZK.Text = Convert.ToString(loanResult["Discount"]);
                    }
                    //贷款备注
                    if (loanResult.ContainsKey("Remark"))
                    {
                        txtReason.Text = Convert.ToString(loanResult["Remark"]);
                    }
                }

                //设置历史贷款银行
                var dictionary = await SetPageData();
                if (dictionary.ContainsKey(true))
                {
                    cb_ResponseObject.IsEnabled = false;
                    cb_ResponseObject.SelectedValue = dictionary[true];
                }
                
                //历史审核信息
                var reason = result["DK10"] as string;
                if (!string.IsNullOrEmpty(reason))
                {
                    scrollViewer.Visibility = Visibility.Visible;
                    this.Height = 350.0;
                    rowReason.Height = new GridLength(50, GridUnitType.Pixel);
                    auditInfo.Text = Convert.ToString(reason);
                }

            }
            catch (Exception ex)
            {

            }
            finally
            {
                this.ctrlLoading.IsBusy = false;
            }
        }

        /// <summary>
        /// 加载页面基本数据
        /// </summary>
        /// <returns></returns>
        private Task<Dictionary<string,Object>> LoadPageData()
        {
            var para = new Dictionary<string, Object>();
            return Task.Run(() =>
            {
                #region 加载银行
                var banks = ConfigServiceProxy.GetBankDic();
                para.Add("BANK", banks);
                #endregion

                #region 加载历史审核结论
                var reason = new StringBuilder();
                var caseEventList = CaseProxyService.GetInstanse().GetCaseEvents(m_CaseDto.CaseId, CaseStatus.DK10, null);
                if (caseEventList != null && caseEventList.Count > 0)
                {                  
                    var cnt = caseEventList.Count;
                    var i = 1;
                    foreach (var eventData in caseEventList)
                    {
                        var dic = eventData.OtherDatas;
                        if (!dic.ContainsKey("AuditReason")) continue;
                        reason.Append(i + ". 理由：");
                        reason.Append(dic["AuditReason"].ToString());
                        if (dic.ContainsKey("CreateUser"))
                        {
                            reason.Append("  操作人:");
                            reason.Append(dic["CreateUser"].ToString());
                        }
                        if (dic.ContainsKey("CreateDate"))
                        {
                            reason.Append("  操作时间:");
                            reason.Append(dic["CreateDate"].ToString());
                        }
                        reason.Append(System.Environment.NewLine);
                        i++;
                    }                   
                }
                para.Add("DK10", Convert.ToString(reason));
                #endregion

                #region 获取贷款信息
               var loanEventList = CaseProxyService.GetInstanse().GetCaseEvents(m_CaseDto.CaseId, CaseStatus.DK3, null);
               para.Add("DK3", loanEventList);
                #endregion
                
              return para;
            });
        }

        /// <summary>
        /// 返货历史推送银行
        /// </summary>
        /// <returns></returns>
        private Task<Dictionary<bool,string>> SetPageData()
        {
            return Task.Run(() =>
            {
                var dictionary = new Dictionary<bool, string>();
                //***********************************************加载历史贷款数据
                var loanEventList = CaseProxyService.GetInstanse().GetCaseEvents(m_CaseDto.CaseId, CaseStatus.DK3, null);
                if (loanEventList != null && loanEventList.Count > 0)
                {
                    var loanEven = loanEventList[0];
                    if (loanEven.OtherDatas.ContainsKey("ResponseObject"))
                    {
                        dictionary.Add(true, Convert.ToString(loanEven.OtherDatas["ResponseObject"]));
                        return dictionary;
                    }
                }
                return dictionary;
            });
        }

        /// <summary>
        /// 审核通过
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void auditOk_Checked(object sender, RoutedEventArgs e)
        {
            auditLoan.Visibility = Visibility.Visible;
            this.Height = 550.0;
            lab_audit.Text = "贷款备注：";
            
        }

        /// <summary>
        /// 审核不通过
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void auditOk_Unchecked(object sender, RoutedEventArgs e)
        {
            auditLoan.Visibility = Visibility.Collapsed;
            this.Height = 300.0;
            lab_audit.Text = "审核备注：";
        }

        /// <summary>
        /// 验证是否数字输入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text_TextChanged(object sender, TextChangedEventArgs e)
        {
            //屏蔽中文输入和非法字符粘贴输入
            TextBox textBox = sender as TextBox;
            TextChange[] change = new TextChange[e.Changes.Count];
            e.Changes.CopyTo(change, 0);

            int offset = change[0].Offset;
            if (change[0].AddedLength > 0)
            {
                double num = 0;
                if (!double.TryParse(textBox.Text, out num))
                {
                    ThreadPool.QueueUserWorkItem((obj) =>
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            textBox.Text = string.Empty;
                            textBox.Select(offset, 0);

                        });
                    });
                }
            }
        }

        /// <summary>
        /// 验证输入字符
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;

            //屏蔽非法按键
            if ((e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) || e.Key == Key.Decimal)
            {
                if (txt.Text.Contains(".") && e.Key == Key.Decimal)
                {
                    e.Handled = true;
                    return;
                }
                e.Handled = false;
            }
            else if (((e.Key >= Key.D0 && e.Key <= Key.D9) || e.Key == Key.OemPeriod) && e.KeyboardDevice.Modifiers != ModifierKeys.Shift)
            {
                if (txt.Text.Contains(".") && e.Key == Key.OemPeriod)
                {
                    e.Handled = true;
                    return;
                }
                e.Handled = false;
            }
            else if (e.Key == Key.Tab)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
