﻿using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PinganHouse.SHHTS.UI.WPF.Main.PreCheck;
using PinganHouse.SHHTS.UI.WPF.Main.Dialog;
using PinganHouse.SHHTS.UI.WPF.Main.Nuclear;

namespace PinganHouse.SHHTS.UI.WPF.Main.Credit
{
    /// <summary>
    /// CreditListPage.xaml 的交互逻辑
    /// </summary>
    public partial class CreditListPage : Page
    {
       //分页大小
        private int PageSize = 10;
        CaseStatus status = CaseStatus.DK1 | CaseStatus.DK2 | CaseStatus.DK3 | CaseStatus.DK4 | CaseStatus.ZB11
            | CaseStatus.DK5 | CaseStatus.DK6 | CaseStatus.DK7 | CaseStatus.DK8 | CaseStatus.DK9 | CaseStatus.DK10
            | CaseStatus.JY5|CaseStatus.ZB7;
        public CreditListPage()
        {
            InitializeComponent();
            this.GridPager.PageNumChanged = GridPager_PagerIndexChanged;
            //获取案件状态
            var list = EnumHelper.InitCaseStatusToCombobox(status);
            caseStatusCb.ItemsSource = list;
            caseStatusCb.DisplayMemberPath = "DisplayMember";
            caseStatusCb.SelectedValuePath = "ValueMember";
            caseStatusCb.SelectedValue = 0;
        }

        /// <summary>
        /// 搜索按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchBtn_Click(object sender, RoutedEventArgs e)
        {
            GridPager.PageIndex = 1;
        }

        /// <summary>
        /// 分页事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridPager_PagerIndexChanged(int pageIndex)
        {
            PageHelper.PageActiveLoadingControl(this.loading, "正在加载数据,请稍后...");
            this.GetDataToBindCtrl();
        }

        /// <summary>
        /// 加载数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            GridPager.PageIndex = 1;
        }
        /// <summary>
        /// 添加附件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AttachmentBtn_Click(object sender, RoutedEventArgs e)
        {
            //案件编号
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            var attachments = new UploadAttachment(caseId);
            PageHelper.PageNavigateHelper(this, attachments);
        }

        #region 页面数据取得方法
        /// <summary>
        /// 异步获取系统数据
        /// </summary>
        private async void GetDataToBindCtrl()
        {
            try
            {
                //案件编号
                var caseId = string.IsNullOrEmpty(this.txtCaseId.Text.Trim()) ? null : this.txtCaseId.Text.Trim();
                //案件地址
                var address = string.IsNullOrEmpty(this.txtAddress.Text.Trim()) ? null : this.txtAddress.Text.Trim();
                //卖家
                var buyerName = string.IsNullOrEmpty(this.txtBuyer.Text.Trim()) ? null : this.txtBuyer.Text.Trim();
                //经纪人
                var agencyName = string.IsNullOrEmpty(this.txtAgency.Text.Trim()) ? null : this.txtAgency.Text.Trim();
                //创建人
                var createrName = string.IsNullOrEmpty(this.txtCreater.Text.Trim()) ? null : this.txtCreater.Text.Trim();
                //开始时间
                DateTime startTime = DateTime.MinValue;
                if (!string.IsNullOrEmpty(dateStart.Text))
                {
                    DateTime.TryParse(dateStart.Text, out startTime);
                }
                //结束时间
                DateTime endTime = DateTime.MinValue;
                if (!string.IsNullOrEmpty(dateEnd.Text))
                {
                    DateTime.TryParse(dateEnd.Text, out endTime);
                }
                //案件状态选择
                var caseStatus = (CaseStatus)Enum.Parse(typeof(CaseStatus), Convert.ToString(caseStatusCb.SelectedValue));
                CaseStatus? selectStatus = caseStatus;
                if (caseStatus == CaseStatus.Unkown) { selectStatus = null; }
                var tupleModel = await GetDataGridData(caseId, address, buyerName, agencyName, startTime, endTime, selectStatus,createrName);
                this.dataGrid.DataContext = tupleModel.Item2;
                GridPager.Visibility = Visibility.Visible;
                if (tupleModel.Item1 % PageSize != 0)
                {
                    GridPager.PageCount = tupleModel.Item1 / PageSize + 1;
                }
                else { GridPager.PageCount = tupleModel.Item1 / PageSize; }
                this.loading.IsBusy = false;
            }
            catch (Exception)
            {
                this.loading.IsBusy = false;
                MessageBox.Show("加载数据发生系统异常.", "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        /// <summary>
        /// 获取页面数据
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="size"></param>
        /// <param name="caseId"></param>
        /// <param name="address"></param>
        /// <param name="buyerName"></param>
        /// <param name="agencyName"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        private Task<Tuple<int, List<CastModel>>> GetDataGridData(string caseId, string address, string buyerName, string agencyName,
            DateTime startTime, DateTime endTime, CaseStatus? selectStatus, string createrName)
        {
            return Task.Run(() =>
            {
                int cnt = 0;
                var caseModelList = new List<CastModel>();
                if (startTime == DateTime.MinValue && endTime == DateTime.MinValue)
                {
                    var caseList = CaseProxyService.GetInstanse().GetPaginatedList(GridPager.PageIndex, PageSize, out cnt
                        , ConfigHelper.GetCurrentReceptionCenter(), status, selectStatus
                        , caseId, address, buyerName, agencyName,operationStatus: CaseStatus.DK2, creatorName: createrName);
                    if (caseList != null)
                    {
                        caseModelList = CastModel.InitData(caseList,CaseStatus.DK2);
                    }
                }
                else if (startTime != DateTime.MinValue && endTime != DateTime.MinValue)
                {
                    var caseList = CaseProxyService.GetInstanse().GetPaginatedList(GridPager.PageIndex, PageSize, out cnt
                        , ConfigHelper.GetCurrentReceptionCenter(), status, selectStatus
                        , caseId, address, buyerName, agencyName, startTime, endTime, operationStatus: CaseStatus.DK2, creatorName: createrName);
                    if (caseList != null)
                    {
                        caseModelList = CastModel.InitData(caseList, CaseStatus.DK2);
                    }
                }
                else if (startTime != DateTime.MinValue && endTime == DateTime.MinValue)
                {
                    var caseList = CaseProxyService.GetInstanse().GetPaginatedList(GridPager.PageIndex, PageSize, out cnt
                        , ConfigHelper.GetCurrentReceptionCenter(), status, selectStatus
                        , caseId, address, buyerName, agencyName, startTime, operationStatus: CaseStatus.DK2, creatorName: createrName);
                    if (caseList != null)
                    {
                        caseModelList = CastModel.InitData(caseList, CaseStatus.DK2);
                    }
                }
                else if (startTime == DateTime.MinValue && endTime != DateTime.MinValue)
                {
                    var caseList = CaseProxyService.GetInstanse().GetPaginatedList(GridPager.PageIndex, PageSize, out cnt
                        , ConfigHelper.GetCurrentReceptionCenter(), status, selectStatus
                        , caseId, address, buyerName, agencyName, null, endTime, operationStatus: CaseStatus.DK2, creatorName: createrName);
                    if (caseList != null)
                    {
                        caseModelList = CastModel.InitData(caseList, CaseStatus.DK2);
                    }
                }
                var dicResult = new Tuple<int, List<CastModel>>(cnt, caseModelList);
                return dicResult;
            });
        }
        #endregion

        /// <summary>
        /// 立即接单 --> 等待客户签约(DK2)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDK2_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var caseId = Convert.ToString(((Button)sender).CommandParameter);
                var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.DK2, LoginHelper.CurrentUser.SysNo);
                if (result.Success)
                {
                    MessageBox.Show("接单成功.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    GridPager_PagerIndexChanged(GridPager.PageIndex);
                }
                else
                {
                    MessageBox.Show("接单失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("操作发生系统异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 签定贷款合同 --> 贷款合同签定(DK3)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDK3_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var caseId = Convert.ToString(((Button)sender).CommandParameter);
                var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(caseId);
                var caseEvent = CaseProxyService.GetInstanse().GetCaseEvents(caseId, CaseStatus.HA2, null);
                if (caseDto == null || caseEvent == null)
                {
                    throw new Exception("为查询到相关数据.");
                }
                var loanContractDialog = new LoanContractDialog(caseDto, caseEvent);
                loanContractDialog.DK3Event+=loanContractDialog_DK3Event;
                loanContractDialog.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("操作发生系统异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 签订贷款合同
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="paras"></param>
        /// <param name="dialog"></param>
        private void loanContractDialog_DK3Event(string caseId, Dictionary<string, object> paras, LoanContractDialog dialog)
        {
            try
            {
                var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.DK3, LoginHelper.CurrentUser.SysNo, paras);
                if (result.Success)
                {
                    MessageBox.Show("签定贷款合同操作成功.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    GridPager_PagerIndexChanged(GridPager.PageIndex);
                }
                else
                {
                    MessageBox.Show("签定贷款合同操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                dialog.DialogResult = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("操作发生系统异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 资料已齐--> 贷款银行审批中(DK4)-贷款资料已齐
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDK4_Click(object sender, RoutedEventArgs e)
        {
            var msgResult = MessageBox.Show("确认贷款资料已齐？", "系统询问", MessageBoxButton.YesNo, MessageBoxImage.Question);
            try
            {
                if (msgResult == MessageBoxResult.Yes)
                {
                    var caseId = Convert.ToString(((Button)sender).CommandParameter);
                    var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.DK4, LoginHelper.CurrentUser.SysNo);
                    if (result.Success)
                    {
                        MessageBox.Show("确认资料已齐操作成功.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                        GridPager_PagerIndexChanged(GridPager.PageIndex);
                    }
                    else
                    {
                        MessageBox.Show("确认资料已齐操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("操作发生系统异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 贷款审批 --> 贷款审批通过(DK5，DK9)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDK5_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var caseId = Convert.ToString(((Button)sender).CommandParameter);
                var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(caseId);
                var caseEvent = CaseProxyService.GetInstanse().GetCaseEvents(caseId, CaseStatus.HA2, null);
                if (caseDto == null || caseEvent == null)
                {
                    throw new Exception("为查询到相关数据.");
                }
                var loanApprovalDialog = new LoanApprovalDialog(caseDto, caseEvent);
                loanApprovalDialog.DK5DK9Event += loanApprovalDialog_DK5DK9Event;
                loanApprovalDialog.DK10Event += loanApprovalDialog_DK10Event;
                loanApprovalDialog.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("操作发生系统异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 贷款资料补齐
        /// </summary>
        /// <param name="caseId">案件Id</param>
        /// <param name="reason">贷款资料补齐原因</param>
        /// <param name="dialog">贷款审核对话框</param>
        void loanApprovalDialog_DK10Event(string caseId,string reason, LoanApprovalDialog dialog)
        {
            try
            {
                var dic = new Dictionary<string, object>();
                dic.Add("AuditReason", reason);
                dic.Add("CreateUser", LoginHelper.CurrentUser.RealName);
                dic.Add("CreateDate", DateTime.Now.ToString("yyyy-MM-dd"));
                var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.DK10, LoginHelper.CurrentUser.SysNo,dic);
                if (result.Success)
                {
                    MessageBox.Show("贷款资料补齐操作成功.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    dialog.DialogResult = true;
                    GridPager_PagerIndexChanged(GridPager.PageIndex);
                }
                else
                {
                    MessageBox.Show("贷款资料补齐操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("操作发生系统异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 贷款审批
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="adopt"></param>
        /// <param name="reason"></param>
        /// <param name="dialog"></param>
        private void loanApprovalDialog_DK5DK9Event(string caseId, bool adopt, string reason, LoanApprovalDialog dialog,Dictionary<string, object> paras=null)
        {
            try
            {
                var status = CaseStatus.DK5;
                Dictionary<string, object> dic = null;
                //如果贷款不通过...
                if (!adopt) {
                    status = CaseStatus.DK9;
                    dic = new Dictionary<string, object>();
                    dic.Add("Reason",reason);
                }
                else
                {
                    dic = paras;
                }
                var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, status, LoginHelper.CurrentUser.SysNo,dic);
                if (result.Success)
                {
                    MessageBox.Show("贷款审批操作成功.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    dialog.DialogResult = true;
                    GridPager_PagerIndexChanged(GridPager.PageIndex);
                }
                else
                {
                    MessageBox.Show("贷款审批操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("操作发生系统异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 贷款合同送达 --> 贷款合同等送过户(DK6)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDK6_Click(object sender, RoutedEventArgs e)
        {
            var msgResult = MessageBox.Show("确认贷款合同送达？", "系统询问", MessageBoxButton.YesNo, MessageBoxImage.Question);
            try
            {
                if (msgResult == MessageBoxResult.Yes)
                {
                    var caseId = Convert.ToString(((Button)sender).CommandParameter);
                    var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.DK6, LoginHelper.CurrentUser.SysNo);
                    if (result.Success)
                    {
                        MessageBox.Show("贷款合同送达操作成功.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                        GridPager_PagerIndexChanged(GridPager.PageIndex);
                    }
                    else
                    {
                        MessageBox.Show("贷款合同送达操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("操作发生系统异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 贷款合同送过户 --> 等出产证(他证)(JY11)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnJY5_Click(object sender, RoutedEventArgs e)
        {
            var msgResult = MessageBox.Show("确认贷款合同送过户？", "系统询问", MessageBoxButton.YesNo, MessageBoxImage.Question);
            try
            {
                if (msgResult == MessageBoxResult.Yes)
                {
                    var caseId = Convert.ToString(((Button)sender).CommandParameter);
                    var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.JY5, LoginHelper.CurrentUser.SysNo);
                    if (result.Success)
                    {
                        MessageBox.Show("贷款合同送过户操作成功.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                        GridPager_PagerIndexChanged(GridPager.PageIndex);
                    }
                    else
                    {
                        MessageBox.Show("贷款合同送过户达操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("操作发生系统异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDK7_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var caseId = Convert.ToString(((Button)sender).CommandParameter);
                var caseEventJY11 = CaseProxyService.GetInstanse().GetCaseEvents(caseId, CaseStatus.JY12, CaseStatus.JY11);
                var caseEventJY13 = CaseProxyService.GetInstanse().GetCaseEvents(caseId, CaseStatus.JY12, CaseStatus.JY11A1);
                if (caseEventJY11 == null && caseEventJY13==null)
                {
                    MessageBox.Show("请等待审税领取他证.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
                #region 检查是否收取他证件
                var msgResult = MessageBox.Show("确认他证已收？", "系统询问", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msgResult == MessageBoxResult.Yes)
                {
                    try
                    {
                        var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.DK7, LoginHelper.CurrentUser.SysNo);
                        if (result.Success)
                        {
                            MessageBox.Show("他证已收操作成功.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                            GridPager_PagerIndexChanged(GridPager.PageIndex);
                        }
                        else
                        {
                            MessageBox.Show("他证已收操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("操作发生系统异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }catch(Exception ex1){
                MessageBox.Show("操作发生系统异常：" + ex1.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            #endregion
        }


        /// <summary>
        /// 已放款 --> 银行已放款(DK8)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDK8_Click(object sender, RoutedEventArgs e)
        {
            var msgResult = MessageBox.Show("确认已放款？", "系统询问", MessageBoxButton.YesNo, MessageBoxImage.Question);
            try
            {
                var caseId = Convert.ToString(((Button)sender).CommandParameter);
                var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.DK8, LoginHelper.CurrentUser.SysNo);
                if (result.Success)
                {
                    MessageBox.Show("已放款确认操作成功.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    GridPager_PagerIndexChanged(GridPager.PageIndex);
                }
                else
                {
                    MessageBox.Show("已放款确认操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("操作发生系统异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 案件跳转填写明细
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RequestNavigate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var link = e.Source as Hyperlink;
                if (null != link)
                {
                    var caseId = Convert.ToString(link.NavigateUri);
                    var caseInfo = new CaseDetailInfo(caseId);
                    PageHelper.PageNavigateHelper(this, caseInfo);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("显示明细发生错误", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            e.Handled = true;
        }
    }
}
