﻿using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPF.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Credit
{
	/// <summary>
	/// MortgageCancelSuccess.xaml 的交互逻辑
	/// </summary>
	public partial class MortgageCancelSuccess : Window
	{

          /// <summary>
        /// 抵押注销成功 HD5-- 还贷完成 HD6
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="paras"></param>
        /// <param name="dialog"></param>
        public delegate void HD6Handle(string caseId, Dictionary<string, object> paras, MortgageCancelSuccess dialog);
        //确定预约事件
        public event HD6Handle HD6Event;

        /// <summary>
        /// 案件和签约对象
        /// </summary>
        private DataTransferObjects.CaseDto m_CaseDto;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="caseDto"></param>
        /// <param name="signingTime"></param>
        /// <param name="attn"></param>
        /// <param name="attnTime"></param>
        public MortgageCancelSuccess(DataTransferObjects.CaseDto caseDto, string signingTime,string attn,string attnTime)
        {
            // TODO: Complete member initialization
            this.InitializeComponent();
            this.m_CaseDto = caseDto;
            this.LayoutRoot.DataContext = caseDto;
            this.lab_CreateDate.Content = signingTime;
            this.lab_JBR.Content = attn;
            this.lab_JBRSJ.Content = attnTime;
            if (!string.IsNullOrEmpty(caseDto.SellerName))
            {
                lab_Seller.Content = caseDto.SellerName.Split(',')[0];
            }
        }

        /// <summary>
        /// 取消抵押注册
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancle_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        /// <summary>
        /// 确定抵押注销成功
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            var dic = new Dictionary<string, object>();
            if (HD6Event != null)
            {
                HD6Event(m_CaseDto.CaseId, dic, this);
            }
        }

        /// <summary>
        /// 上传附件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUpload_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var upload = new UploadFileHelper();
                var fileNum = 0;
                upload.UploadFile(m_CaseDto.CaseId, AttachmentType.产调证, out fileNum);
            }
            catch (Exception ex)
            {
                MessageBox.Show("上传附件操作发生系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        /// <summary>
        /// 高拍仪上传
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCamera_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var captruePage = new CaptureImgeDialog(m_CaseDto.CaseId, AttachmentType.产调证);
                captruePage.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("上传文件操作发生异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
	}
}