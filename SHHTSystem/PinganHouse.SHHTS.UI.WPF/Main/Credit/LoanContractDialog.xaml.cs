﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Credit
{
    /// <summary>
    /// LoanContractDialog.xaml 的交互逻辑
    /// </summary>
    public partial class LoanContractDialog : Window
    {
        /// <summary>
        /// 案件对象
        /// </summary>
        private CaseDto m_CaseDto;
        /// <summary>
        /// 贷款合同签订（DK3）
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="paras"></param>
        /// <param name="dialog"></param>
        public delegate void DK3Handle(string caseId, Dictionary<string, object> paras, LoanContractDialog dialog);
        //确定贷款合同签订事件
        public event DK3Handle DK3Event;

        /// <summary>
        /// 贷款合同签订
        /// </summary>
        /// <param name="caseId"></param>
        public LoanContractDialog(CaseDto caseDto, IList<DataTransferObjects.CaseEvent> caseEvent)
        {
            InitializeComponent();
            this.m_CaseDto = caseDto;
            this.DataContext = caseDto;
            if (caseEvent.Count > 0)
            {
                this.lab_CreateDate.Content = caseEvent[0].CreateDate.ToString("yyyy-MM-dd HH:mm");
            }
            if (!string.IsNullOrEmpty(caseDto.BuyerName))
            {
                lab_Buyer.Content = caseDto.BuyerName.Split(',')[0];
            }
        }

        /// <summary>
        /// 确定贷款
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            var paras = new Dictionary<string, object>();
            var check = CheckAuditOkResult(out paras);
            if (check)
            {
                if (DK3Event != null)
                {
                    DK3Event(m_CaseDto.CaseId, paras, this);
                }
            }
        }

        /// <summary>
        /// 检查
        /// </summary>
        private bool CheckAuditOkResult(out Dictionary<string, object> dictionaryData)
        {
            //加入字典
            var paras = new Dictionary<string, object>();
            try
            {
                //贷款金额
                decimal money;
                decimal.TryParse(txt_DKJE.Text.Trim(), out money);
                if (money <= 0)
                {
                    MessageBox.Show("请输入贷款金额.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txt_DKJE.Focus();
                    return false;
                }
                paras.Add("Money", money);

                //确定贷款银行
                var responseObject = Convert.ToString(cb_ResponseObject.SelectedValue);
                if (string.IsNullOrEmpty(responseObject) || responseObject == "A-NULL")
                {
                    MessageBox.Show("请选择银行.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txt_DKZK.Focus();
                    return false;
                }
                //贷款银行
                paras.Add("ResponseObject", responseObject);

                //贷款成数
                int percentage;
                int.TryParse(txt_DKCS.Text.Trim(), out percentage);
                if (percentage <= 0 || percentage >= 100)
                {
                    MessageBox.Show("请输入正确的贷款成数.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txt_DKCS.Focus();
                    return false;
                }
                paras.Add("Percentage", percentage);
                //贷款年限
                int limitYear;
                int.TryParse(txt_DKNX.Text.Trim(), out limitYear);
                if (limitYear <= 0 || limitYear > 30)
                {
                    MessageBox.Show("请输入正确的贷款年限.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txt_DKNX.Focus();
                    return false;
                }
                paras.Add("LimitYear", limitYear);
                //贷款折扣
                int discount;
                int.TryParse(txt_DKZK.Text.Trim(), out discount);
                if (discount <= 0 || discount >= 100)
                {
                    MessageBox.Show("请输入正确的贷款折扣.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txt_DKZK.Focus();
                    return false;
                }
                paras.Add("Discount", discount);

                //贷款备注
                var remark = txt_DKBZ.Text.Trim();
                paras.Add("Remark", remark);
                dictionaryData = paras;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                dictionaryData = paras;
            }
        }

        /// <summary>
        /// 取消贷款
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        /// <summary>
        /// 验证是否数字输入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text_TextChanged(object sender, TextChangedEventArgs e)
        {
            //屏蔽中文输入和非法字符粘贴输入
            TextBox textBox = sender as TextBox;
            TextChange[] change = new TextChange[e.Changes.Count];
            e.Changes.CopyTo(change, 0);

            int offset = change[0].Offset;
            if (change[0].AddedLength > 0)
            {
                double num = 0;
                if (!double.TryParse(textBox.Text, out num))
                {
                    ThreadPool.QueueUserWorkItem((obj) =>
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            textBox.Text = string.Empty;
                            textBox.Select(offset, 0);

                        });
                    });
                }
            }
        }

        /// <summary>
        /// 验证输入字符
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;

            //屏蔽非法按键
            if ((e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) || e.Key == Key.Decimal)
            {
                if (txt.Text.Contains(".") && e.Key == Key.Decimal)
                {
                    e.Handled = true;
                    return;
                }
                e.Handled = false;
            }
            else if (((e.Key >= Key.D0 && e.Key <= Key.D9) || e.Key == Key.OemPeriod) && e.KeyboardDevice.Modifiers != ModifierKeys.Shift)
            {
                if (txt.Text.Contains(".") && e.Key == Key.OemPeriod)
                {
                    e.Handled = true;
                    return;
                }
                e.Handled = false;
            }
            else if (e.Key == Key.Tab)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// 页面加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                PageHelper.PageActiveLoadingControl(this.ctrlLoading, "正在获取,请稍候...");
                //获取银行
                var banks = ConfigServiceProxy.GetBankDic();
                banks.Add("A-NULL", "请选择");
                cb_ResponseObject.ItemsSource = banks;
                cb_ResponseObject.DisplayMemberPath = "Value";
                cb_ResponseObject.SelectedValuePath = "Key";
                cb_ResponseObject.SelectedValue = "A-NULL";

                //设置历史贷款银行
                var dictionary = await SetPageData();
                if (dictionary.ContainsKey(true))
                {
                    cb_ResponseObject.IsEnabled = false;
                    cb_ResponseObject.SelectedValue = dictionary[true];
                }

            }
            catch (Exception ex)
            {

            }
            finally
            {
                this.ctrlLoading.IsBusy = false;
            }
        }

        /// <summary>
        /// 返货历史推送银行
        /// </summary>
        /// <returns></returns>
        private Task<Dictionary<bool, string>> SetPageData()
        {
            return Task.Run(() =>
            {
                var dictionary = new Dictionary<bool, string>();
                //***********************************************加载历史贷款数据
                var loanEventList = CaseProxyService.GetInstanse().GetCaseEvents(m_CaseDto.CaseId, CaseStatus.DK1, null);
                if (loanEventList != null && loanEventList.Count > 0)
                {
                    var loanEven = loanEventList[0];
                    if (loanEven.OtherDatas.ContainsKey("LoanObject"))
                    {
                        var loanObject = (LoanObject)Enum.Parse(typeof(LoanObject), Convert.ToString(loanEven.OtherDatas["LoanObject"]), false);
                        if (loanObject == LoanObject.Bank)
                        {
                            dictionary.Add(true, Convert.ToString(loanEven.OtherDatas["ResponseObject"]));
                            return dictionary;
                        }
                    }
                }
                return dictionary;
            });
        }
    }
}
