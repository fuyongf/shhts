﻿using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PinganHouse.SHHTS.UI.WPF.Main.PreCheck;
using PinganHouse.SHHTS.UI.WPF.Main.Dialog;
using PinganHouse.SHHTS.UI.WPF.Main.Nuclear;

namespace PinganHouse.SHHTS.UI.WPF.Main.Credit
{
    /// <summary>
    /// 申请还贷 的交互逻辑
    /// </summary>
    public partial class ApplyForCreditPage : Page
    {
        //分页大小
        private int PageSize = 10;
        CaseStatus status =CaseStatus.ZB3|CaseStatus.HD1 | CaseStatus.HD2 | CaseStatus.HD3
            | CaseStatus.HD4 | CaseStatus.HD5 | CaseStatus.HD6 | CaseStatus.ZB12;
        public ApplyForCreditPage()
        {
            InitializeComponent();
            this.GridPager.PageNumChanged = GridPager_PagerIndexChanged;
            //获取案件状态
            var list = EnumHelper.InitCaseStatusToCombobox(status);
            caseStatusCb.ItemsSource = list;
            caseStatusCb.DisplayMemberPath = "DisplayMember";
            caseStatusCb.SelectedValuePath = "ValueMember";
            caseStatusCb.SelectedValue = 0;
        }

        /// <summary>
        /// 搜索按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchBtn_Click(object sender, RoutedEventArgs e)
        {
            GridPager.PageIndex = 1;
        }

        /// <summary>
        /// 分页事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridPager_PagerIndexChanged(int pageIndex)
        {
            PageHelper.PageActiveLoadingControl(this.loading, "正在加载数据,请稍后...");
            this.GetDataToBindCtrl();
        }

        /// <summary>
        /// 添加附件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AttachmentBtn_Click(object sender, RoutedEventArgs e)
        {
            //案件编号
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            var attachments = new UploadAttachment(caseId);
            PageHelper.PageNavigateHelper(this, attachments);
        }

        /// <summary>
        /// 页面加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ApplyForCredit_Loaded(object sender, RoutedEventArgs e)
        {
            GridPager.PageIndex = 1;
        }

        #region 页面数据取得方法
        /// <summary>
        /// 异步获取系统数据
        /// </summary>
        private async void GetDataToBindCtrl()
        {
            try
            {
                //案件编号
                var caseId = string.IsNullOrEmpty(this.txtCaseId.Text.Trim()) ? null : this.txtCaseId.Text.Trim();
                //案件地址
                var address = string.IsNullOrEmpty(this.txtAddress.Text.Trim()) ? null : this.txtAddress.Text.Trim();
                //卖家
                var buyerName = string.IsNullOrEmpty(this.txtBuyer.Text.Trim()) ? null : this.txtBuyer.Text.Trim();
                //经纪人
                var agencyName = string.IsNullOrEmpty(this.txtAgency.Text.Trim()) ? null : this.txtAgency.Text.Trim();
                //创建人
                var createrName = string.IsNullOrEmpty(this.txtCreater.Text.Trim()) ? null : this.txtCreater.Text.Trim();
                //开始时间
                DateTime startTime = DateTime.MinValue;
                if (!string.IsNullOrEmpty(dateStart.Text))
                {
                    DateTime.TryParse(dateStart.Text, out startTime);
                }
                //结束时间
                DateTime endTime = DateTime.MinValue;
                if (!string.IsNullOrEmpty(dateEnd.Text))
                {
                    DateTime.TryParse(dateEnd.Text, out endTime);
                }
                //案件状态选择
                var caseStatus = (CaseStatus)Enum.Parse(typeof(CaseStatus), Convert.ToString(caseStatusCb.SelectedValue));
                CaseStatus? selectStatus = caseStatus;
                if (caseStatus == CaseStatus.Unkown) { selectStatus = null; }
                var tupleModel = await GetDataGridData(caseId, address, buyerName, agencyName, startTime, endTime, selectStatus,createrName);
                this.dataGrid.DataContext = tupleModel.Item2;
                GridPager.Visibility = Visibility.Visible;
                if (tupleModel.Item1 % PageSize != 0)
                {
                    GridPager.PageCount = tupleModel.Item1 / PageSize + 1;
                }
                else { GridPager.PageCount = tupleModel.Item1 / PageSize; }
                this.loading.IsBusy = false;
            }
            catch (Exception)
            {
                this.loading.IsBusy = false;
                MessageBox.Show("加载数据发生系统异常.", "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        /// <summary>
        /// 获取页面数据
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="size"></param>
        /// <param name="caseId"></param>
        /// <param name="address"></param>
        /// <param name="buyerName"></param>
        /// <param name="agencyName"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        private Task<Tuple<int, List<CastModel>>> GetDataGridData(string caseId, string address, string buyerName, string agencyName,
            DateTime startTime, DateTime endTime, CaseStatus? selectStatus, string createrName)
        {
            return Task.Run(() =>
            {
                int cnt = 0;
                var caseModelList = new List<CastModel>();
                if (startTime == DateTime.MinValue && endTime == DateTime.MinValue)
                {
                    var caseList = CaseProxyService.GetInstanse().GetPaginatedList(GridPager.PageIndex, PageSize, out cnt
                        , ConfigHelper.GetCurrentReceptionCenter(), status, selectStatus
                        , caseId, address, buyerName, agencyName,operationStatus:CaseStatus.HD1,creatorName:createrName);
                    if (caseList != null)
                    {
                        caseModelList = CastModel.InitData(caseList,CaseStatus.HD1);
                    }
                }
                else if (startTime != DateTime.MinValue && endTime != DateTime.MinValue)
                {
                    var caseList = CaseProxyService.GetInstanse().GetPaginatedList(GridPager.PageIndex, PageSize, out cnt
                        , ConfigHelper.GetCurrentReceptionCenter(), status, selectStatus
                        , caseId, address, buyerName, agencyName, startTime, endTime, operationStatus: CaseStatus.HD1, creatorName: createrName);
                    if (caseList != null)
                    {
                        caseModelList = CastModel.InitData(caseList, CaseStatus.HD1);
                    }
                }
                else if (startTime != DateTime.MinValue && endTime == DateTime.MinValue)
                {
                    var caseList = CaseProxyService.GetInstanse().GetPaginatedList(GridPager.PageIndex, PageSize, out cnt
                        , ConfigHelper.GetCurrentReceptionCenter(), status, selectStatus
                        , caseId, address, buyerName, agencyName, startTime, operationStatus: CaseStatus.HD1, creatorName: createrName);
                    if (caseList != null)
                    {
                        caseModelList = CastModel.InitData(caseList, CaseStatus.HD1);
                    }
                }
                else if (startTime == DateTime.MinValue && endTime != DateTime.MinValue)
                {
                    var caseList = CaseProxyService.GetInstanse().GetPaginatedList(GridPager.PageIndex, PageSize, out cnt
                        , ConfigHelper.GetCurrentReceptionCenter(), status, selectStatus
                        , caseId, address, buyerName, agencyName, null, endTime, operationStatus: CaseStatus.HD1, creatorName: createrName);
                    if (caseList != null)
                    {
                        caseModelList = CastModel.InitData(caseList, CaseStatus.HD1);
                    }
                }
                var dicResult = new Tuple<int, List<CastModel>>(cnt, caseModelList);
                return dicResult;
            });
        }
        #endregion

        #region  约定还款时间(ZB3)--等待预约还款（HD1）
        /// <summary>
        /// 约定还款时间(ZB3)--等待预约还款（HD1）
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HD1_Click(object sender, RoutedEventArgs e)
        {
            try {
                var caseId = Convert.ToString(((Button)sender).CommandParameter);
                var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.HD1, LoginHelper.CurrentUser.SysNo);
                if (result.Success)
                {
                    MessageBox.Show("接单成功.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    GridPager_PagerIndexChanged(GridPager.PageIndex);
                }
                else
                {
                    MessageBox.Show("接单失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("操作发生系统异常："+ex.Message,"系统错误",MessageBoxButton.OK,MessageBoxImage.Error);
            }
        }

        #endregion

        #region 约定还款时间(HD1) 预约还款时间(HD2)
        /// <summary>
        /// 已还款
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HD2_Click(object sender, RoutedEventArgs e)
        {
            try {
                var caseId = Convert.ToString(((Button)sender).CommandParameter);
                var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(caseId);
                var caseEvent = CaseProxyService.GetInstanse().GetCaseEvents(caseId, CaseStatus.HA2, null);
                if (caseDto == null || caseEvent == null)
                {
                    throw new Exception("为查询到相关数据.");
                }
                var reserveBankDialog = new ReserveBankDialog(caseDto, caseEvent);
                reserveBankDialog.HD2Event += reserveBankDialog_HD2Event;
                reserveBankDialog.ShowDialog();
            }
            catch(Exception ex)
            {
                MessageBox.Show("操作发生系统异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        /// <summary>
        ///  约定还款时间(HD1)--预约还款时间（HD2）
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="paras"></param>
        /// <param name="dialog"></param>
        private void reserveBankDialog_HD2Event(string caseId, Dictionary<string, object> paras, ReserveBankDialog dialog)
        {
            try
            {
                var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.HD2, LoginHelper.CurrentUser.SysNo, paras);
                if (result.Success)
                {
                    MessageBox.Show("预约还款操作成功.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    dialog.DialogResult = true;
                    GridPager_PagerIndexChanged(GridPager.PageIndex);
                }
                else
                {
                    MessageBox.Show("预约还款操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("操作发生系统异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        #endregion

        #region 已还款HD2 --  卖方贷款已还清(HD3)
        private void HD3_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var msgResult = MessageBox.Show("确认已还款操作?", "系统确认", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msgResult == MessageBoxResult.Yes)
                {
                    var caseId = Convert.ToString(((Button)sender).CommandParameter);
                    var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.HD3, LoginHelper.CurrentUser.SysNo);
                    if (result.Success)
                    {
                        MessageBox.Show("已还款操作成功.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                        GridPager_PagerIndexChanged(GridPager.PageIndex);
                    }
                    else
                    {
                        MessageBox.Show("已还款操作成功：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("操作发生系统异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        #endregion

        #region 抵押注销 卖方贷款已结清(HD4)
        private void HD4_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var caseId = Convert.ToString(((Button)sender).CommandParameter);
                var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(caseId);
                var caseEvent = CaseProxyService.GetInstanse().GetCaseEvents(caseId, CaseStatus.HA2, null);
                if (caseDto == null || caseEvent == null)
                {
                    throw new Exception("为查询到相关数据.");
                }
                var settlementVouchersDialog = new SettlementVouchersDialog(caseDto, caseEvent);
                settlementVouchersDialog.HD4Event += settlementVouchersDialog_HD4Event;
                settlementVouchersDialog.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("操作发生系统异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        /// <summary>
        /// 卖方贷款已结清(HD4)
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="paras"></param>
        /// <param name="dialog"></param>
        private void settlementVouchersDialog_HD4Event(string caseId, Dictionary<string, object> paras, SettlementVouchersDialog dialog)
        {
            try
            {
                var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.HD4, LoginHelper.CurrentUser.SysNo, paras);
                if (result.Success)
                {
                    MessageBox.Show("出结清凭证操作成功.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    dialog.DialogResult = true;
                    GridPager_PagerIndexChanged(GridPager.PageIndex);
                }
                else
                {
                    MessageBox.Show("出结清凭证操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("操作发生系统异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        #endregion

        #region 抵押注销 - 卖方抵押已注销 HD5
        /// <summary>
        /// 抵押注销成功
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HD5_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var caseId = Convert.ToString(((Button)sender).CommandParameter);
                var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(caseId);
                var caseEvent = CaseProxyService.GetInstanse().GetCaseEvents(caseId, CaseStatus.HA2, null);
                if (caseDto == null || caseEvent == null)
                {
                    throw new Exception("为查询到相关数据.");
                }
                var mortgageCancelDialog = new MortgageCancelDialog(caseDto, caseEvent);
                mortgageCancelDialog.HD5Event += mortgageCancelDialog_HD5Event;
                mortgageCancelDialog.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("操作发生系统异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 抵押注销
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="paras"></param>
        /// <param name="dialog"></param>
        private void mortgageCancelDialog_HD5Event(string caseId, Dictionary<string, object> paras, MortgageCancelDialog dialog)
        {
            try
            {
                var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.HD5, LoginHelper.CurrentUser.SysNo, paras);
                if (result.Success)
                {
                    MessageBox.Show("抵押注销操作成功.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    dialog.DialogResult = true;
                    GridPager_PagerIndexChanged(GridPager.PageIndex);
                }
                else
                {
                    MessageBox.Show("抵押注销操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("操作发生系统异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #endregion

        #region 抵押注销成功
        private void HD6_Click(object sender, RoutedEventArgs e)
        {
            try {
                var caseId = Convert.ToString(((Button)sender).CommandParameter);
                var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(caseId);
                var caseEvent = CaseProxyService.GetInstanse().GetCaseEvents(caseId, CaseStatus.HA2, null);
                if (caseDto == null || caseEvent == null || caseEvent.Count == 0)
                {
                    throw new Exception("没有查询到该案件相关数据.");
                }
                var signTime = caseEvent[0].CreateDate.ToString("yyyy-MM-dd HH:mm");
                var caseEvent1 = CaseProxyService.GetInstanse().GetCaseEvents(caseId, CaseStatus.HD5, null);
                if (caseEvent1 == null || caseEvent1.Count == 0)
                {
                    throw new Exception("没有查询到该案件的抵押注销信息相关数据.");
                }
                var otherData = caseEvent1[0].OtherDatas;
                var attn = string.Empty;
                var attnTime = string.Empty;
                if (otherData.ContainsKey("Attn"))
                {
                    attn = otherData["Attn"].ToString();
                }
                else { 
                    throw new Exception("没有查询到该案件的抵押注销信息相关数据.");
                }
                if (otherData.ContainsKey("AttnTime"))
                {
                    attnTime = (DateTime.Parse(otherData["AttnTime"].ToString())).ToString("yyyy-MM-dd");
                }
                else { 
                    throw new Exception("没有查询到该案件的抵押注销信息相关数据."); 
                }
                var mortgageCancelSuccess = new MortgageCancelSuccess(caseDto, signTime, attn, attnTime);
                mortgageCancelSuccess.HD6Event += mortgageCancelSuccess_HD6Event;
                mortgageCancelSuccess.ShowDialog();            
            }
            catch (Exception ex) {
                MessageBox.Show("操作发生系统异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        /// <summary>
        /// 抵押注销成功HD5--还贷完成HD6
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="paras"></param>
        /// <param name="dialog"></param>
        private void mortgageCancelSuccess_HD6Event(string caseId, Dictionary<string, object> paras, MortgageCancelSuccess dialog)
        {
            try
            {
                var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.HD6, LoginHelper.CurrentUser.SysNo, paras);
                if (result.Success)
                {
                    MessageBox.Show("抵押注销成功操作成功.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    dialog.DialogResult = true;
                    GridPager_PagerIndexChanged(GridPager.PageIndex);
                }
                else
                {
                    MessageBox.Show("抵押注销成功操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("操作发生系统异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        #endregion

        #region  显示案件详细
        /// <summary>
        /// 案件跳转填写明细
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RequestNavigate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var link = e.Source as Hyperlink;
                if (null != link)
                {
                    var caseId = Convert.ToString(link.NavigateUri);
                    var caseInfo = new CaseDetailInfo(caseId);
                    PageHelper.PageNavigateHelper(this, caseInfo);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("显示明细发生错误", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            e.Handled = true;
        }
        #endregion
    }
}
