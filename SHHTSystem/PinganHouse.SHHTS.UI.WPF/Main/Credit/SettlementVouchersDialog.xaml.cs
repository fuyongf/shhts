﻿using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPF.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Credit
{
	/// <summary>
	/// SettlementVouchersDialog.xaml 的交互逻辑
	/// </summary>
	public partial class SettlementVouchersDialog : Window
	{
        /// <summary>
        /// 出结清凭证HD3-- 卖方贷款已还清 HD4
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="paras"></param>
        /// <param name="dialog"></param>
        public delegate void HD4Handle(string caseId, Dictionary<string, object> paras, SettlementVouchersDialog dialog);
        //确定预约事件
        public event HD4Handle HD4Event;

        /// <summary>
        /// 
        /// </summary>
        private DataTransferObjects.CaseDto m_CaseDto;
        private IList<DataTransferObjects.CaseEvent> m_CaseEvent;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="caseDto"></param>
        /// <param name="caseEvent"></param>
        public SettlementVouchersDialog(DataTransferObjects.CaseDto caseDto, IList<DataTransferObjects.CaseEvent> caseEvent)
        {
            // TODO: Complete member initialization
            this.InitializeComponent();
            this.m_CaseDto = caseDto;
            this.m_CaseEvent = caseEvent;
            this.LayoutRoot.DataContext = caseDto;
            if (caseEvent.Count > 0)
            {
                this.lab_CreateDate.Text = caseEvent[0].CreateDate.ToString("yyyy-MM-dd HH:mm");
            }
            if (!string.IsNullOrEmpty(caseDto.SellerName))
            {
                lab_Seller.Text = caseDto.SellerName.Split(',')[0];
            }
        }

        /// <summary>
        /// 取消操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancle_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        /// <summary>
        /// 确定操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            var name = txt_Name.Text.Trim();
            if (string.IsNullOrEmpty(name))
            {
                MessageBox.Show("确认人必须填写.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                var dic = new Dictionary<string, object>();              
                if (HD4Event != null)
                {
                    HD4Event(m_CaseDto.CaseId, dic, this);
                }
            }
        }

        /// <summary>
        /// 上传附件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUpload_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var upload = new UploadFileHelper();
                var fileNum = 0;
                upload.UploadFile(m_CaseDto.CaseId, AttachmentType.上家提前还款结算凭证, out fileNum);
            }
            catch (Exception ex)
            {
                MessageBox.Show("上传附件操作发生系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        /// <summary>
        /// 高拍仪上传
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCamera_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var captruePage = new CaptureImgeDialog(m_CaseDto.CaseId, AttachmentType.上家提前还款结算凭证);
                captruePage.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("上传文件操作发生异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
	}
}