﻿using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPF.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Credit
{
	/// <summary>
	/// MortgageCancelDialog.xaml 的交互逻辑
	/// </summary>
	public partial class MortgageCancelDialog : Window
	{
        /// <summary>
        /// 卖方贷款已结清（HD4）- 抵押注销（HD5）
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="paras"></param>
        /// <param name="dialog"></param>
        public delegate void HD5Handle(string caseId, Dictionary<string, object> paras, MortgageCancelDialog dialog);
        //确定预约事件
        public event HD5Handle HD5Event;

        /// <summary>
        /// 案件和签约对象
        /// </summary>
        private DataTransferObjects.CaseDto m_CaseDto;
        private IList<DataTransferObjects.CaseEvent> m_CaseEvent;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="caseDto"></param>
        /// <param name="caseEvent"></param>
        public MortgageCancelDialog(DataTransferObjects.CaseDto caseDto, IList<DataTransferObjects.CaseEvent> caseEvent)
        {
            // TODO: Complete member initialization
            this.InitializeComponent();
            this.m_CaseDto = caseDto;
            this.m_CaseEvent = caseEvent;
            this.LayoutRoot.DataContext = caseDto;
            if (caseEvent.Count > 0)
            {
                this.lab_CreateDate.Content = caseEvent[0].CreateDate.ToString("yyyy-MM-dd HH:mm");
            }
            if (!string.IsNullOrEmpty(caseDto.SellerName))
            {
                lab_Seller.Content = caseDto.SellerName.Split(',')[0];
            }
        }

        /// <summary>
        /// 取消抵押注销
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancle_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        /// <summary>
        /// 确认抵押注销
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            var attn = txt_Attn.Text.Trim();
            var attnTime = string.Empty;
            if (!string.IsNullOrEmpty(dp_AttnTime.Text.Trim()))
            {
                attnTime = dp_AttnTime.Text.Trim();
            }
            if (string.IsNullOrEmpty(attn) || string.IsNullOrEmpty(attnTime))
            {
                MessageBox.Show("经办人和注销时间必须都填写.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                var dic = new Dictionary<string, object>();
                dic.Add("Attn",attn);
                dic.Add("AttnTime", attnTime);
                if (HD5Event != null)
                {
                    HD5Event(m_CaseDto.CaseId, dic, this);
                }
            }
        }

        /// <summary>
        /// 上传附件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUpload_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var upload = new UploadFileHelper();
                var fileNum = 0;
                upload.UploadFile(m_CaseDto.CaseId, AttachmentType.收件收据注销抵押, out fileNum);
            }
            catch (Exception ex)
            {
                MessageBox.Show("上传附件操作发生系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        /// <summary>
        /// 高拍仪上传
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCamera_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var captruePage = new CaptureImgeDialog(m_CaseDto.CaseId, AttachmentType.收件收据注销抵押);
                captruePage.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("上传文件操作发生异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

	}
}