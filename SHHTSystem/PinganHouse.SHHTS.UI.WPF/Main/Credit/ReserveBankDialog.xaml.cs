﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPF.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Credit
{
	/// <summary>
	/// ReserveBankDialog.xaml 的交互逻辑
	/// </summary>
	public partial class ReserveBankDialog : Window
	{
        /// <summary>
        /// 约定还款时间HD1-- 下一个状态[已确定还款时间 HD2
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="paras"></param>
        /// <param name="dialog"></param>
        public delegate void HD2Handle(string caseId, Dictionary<string, object> paras, ReserveBankDialog dialog);
        //确定预约事件
        public event HD2Handle HD2Event;

        //案件对象
        private CaseDto m_CaseDto;
        //签约信息
        private IList<DataTransferObjects.CaseEvent> caseEvent;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="caseDto"></param>
        /// <param name="caseEvent"></param>
        public ReserveBankDialog(CaseDto caseDto, IList<DataTransferObjects.CaseEvent> caseEvent)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            this.m_CaseDto = caseDto;
            this.caseEvent = caseEvent;
            this.LayoutRoot.DataContext = caseDto;
            if (caseEvent.Count > 0)
            {
                this.lab_CreateDate.Content = caseEvent[0].CreateDate.ToString("yyyy-MM-dd HH:mm");
            }
            if (!string.IsNullOrEmpty(caseDto.SellerName))
            {
                lab_Seller.Content = caseDto.SellerName.Split(',')[0];
            }
        }

        /// <summary>
        /// 确定还款
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            var attn = txt_JBR.Text.Trim();
            string attnTime = dp_YYHKSJ.Text.Trim();
            var remark = txtRemark.Text.Trim();
            if (string.IsNullOrEmpty(attn) || string.IsNullOrEmpty(attnTime))
            {
                MessageBox.Show("经办人和预约还款时间必须填写.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                var dic = new Dictionary<string, object>();
                dic.Add("Attn", attn);
                dic.Add("AttnTime", attnTime);
                dic.Add("Remark", remark);
                if (HD2Event != null)
                {
                    HD2Event(m_CaseDto.CaseId, dic, this);
                }
            }
        }

        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancle_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        /// <summary>
        /// 上传附件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUpload_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var upload = new UploadFileHelper();
                var fileNum = 0;
                upload.UploadFile(m_CaseDto.CaseId, AttachmentType.预约还贷申请, out fileNum);
            }
            catch (Exception ex)
            {
                MessageBox.Show("上传附件操作发生系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 高拍仪上传
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCamera_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var captruePage = new CaptureImgeDialog(m_CaseDto.CaseId, AttachmentType.预约还贷申请);
                captruePage.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("上传文件操作发生异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
	}
}