﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HTB.DevFx.Utils;
using NPOI.OpenXmlFormats.Wordprocessing;
using NPOI.SS.Formula.PTG;
using NPOI.XWPF.UserModel;
using HorizontalAlignment = System.Windows.HorizontalAlignment;
using MessageBox = System.Windows.MessageBox;

namespace PinganHouse.SHHTS.UI.WPF.Main.Calculation
{
    /// <summary>
    /// TaxCalculation.xaml 的交互逻辑
    /// </summary>
    public partial class TaxCalculation : Page
    {


        public TaxCalculation()
        {
            InitializeComponent();

        }



        /// <summary>
        /// 重置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnReset_OnClick(object sender, RoutedEventArgs e)
        {
            txtArea.Text = "";
            txtUnitPrice.Text = "";
            txtTotalPrice.Text = "";
            rdoTotalPrice.IsChecked = true;
            rdoDifferencePrice.IsChecked = false;
            txtOriginalPrice.Text = "";

            rdoAverageHouse.IsChecked = true;
            rdoExtraordinaryHouse.IsChecked = false;
            spTwoYear.IsEnabled = true;
            rdoTwoYearNo.IsChecked = true;
            spFiveYear.IsEnabled = false;

            rdoFirstNo.IsChecked = true;
            rdoOnlyOneNo.IsChecked = true;

            rdoNotarizedNo.IsChecked = true;
            rdoLoanNo.IsChecked = true;
            rdoCommissionedNotarizedNo.IsChecked = true;

        }

        /// <summary>
        /// 计算
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCalculate_OnClick(object sender, RoutedEventArgs e)
        {

            decimal area = 0;
            decimal price = 0;
            int priceType = 0;
            decimal originalPrice = 0;
            int houseType = 0;
            bool fiveYear;
            bool twoYear;
            bool first;
            bool onlyOne;

            decimal.TryParse(txtArea.Text.Trim(), out area);
            decimal.TryParse(txtTotalPrice.Text.Trim(), out price);
            price = price * 10000;

            string notarized = "";
            decimal loan = 0;
            string commissionedNotarized = "";

            decimal buyerTotal = 0;
            decimal sellerTotal = 0;

            if (rdoTotalPrice.IsChecked == true)
            {
                priceType = 1;

            }
            else if (rdoDifferencePrice.IsChecked == true)
            {
                priceType = 2;

            }

            decimal.TryParse(txtOriginalPrice.Text.Trim(), out originalPrice);
            originalPrice = originalPrice * 10000;

            if (rdoAverageHouse.IsChecked == true)
            {
                houseType = 1;
            }
            else if (rdoExtraordinaryHouse.IsChecked == true)
            {
                houseType = 2;
            }

            if (rdoFiveYearYes.IsChecked == true)
            {
                fiveYear = true;
            }
            else
            {
                fiveYear = false;
            }

            if (rdoTwoYearYes.IsChecked == true || rdoFiveYearYes.IsChecked == true)
            {
                twoYear = true;
            }
            else
            {
                twoYear = false;
            }

            if (rdoFirstYes.IsChecked == true)
            {
                first = true;
            }
            else
            {
                first = false;
            }

            if (rdoOnlyOneYes.IsChecked == true)
            {
                onlyOne = true;
            }
            else
            {
                onlyOne = false;
            }

            if (area == 0)
            {
                MessageBox.Show("请输入房屋建筑面积", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (price == 0)
            {
                MessageBox.Show("请输入房屋总价", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }


            if (rdoLoanYes.IsChecked == true)
            {
                decimal.TryParse(txtLoan.Text.Trim(), out loan);
                loan = loan * 10000;
            }
            else
            {
                loan = 0;
            }

            txtUnitPrice.Text = (price/area).ToString("N");

            //买方契税
            var deedTax = GetDeedTax(houseType, fiveYear, first, area, price);
            lblDeedTax.Content = deedTax.Item1.ToString("N");
            lblDeedTaxIntro.Content = deedTax.Item2;
            buyerTotal = buyerTotal + deedTax.Item1;

            //买方权证印花税
            var certificateStamDuty = GetCertificateStamDuty();
            lblCertificateStamDuty.Content = certificateStamDuty.Item1.ToString("N");
            lblCertificateStamDutyIntro.Content = certificateStamDuty.Item2;
            buyerTotal = buyerTotal + certificateStamDuty.Item1;

            //买方登记费
            var registrationFee = GetRegistrationFee();
            lblRegistrationFee.Content = registrationFee.Item1.ToString("N");
            lblRegistrationFeeIntro.Content = registrationFee.Item2;
            buyerTotal = buyerTotal + registrationFee.Item1;

            //买方图纸费
            var blueprintFee = GetBlueprintFee();
            lblBlueprintFee.Content = blueprintFee.Item1.ToString("N");
            lblBlueprintFeeIntro.Content = blueprintFee.Item2;
            buyerTotal = buyerTotal + blueprintFee.Item1;

            if (rdoLoanYes.IsChecked == true)
            {
                //买方抵押登记费
                var mortgageRegistrationFee = GetMortgageRegistrationFee();
                lblMortgageRegistrationFee.HorizontalContentAlignment = HorizontalAlignment.Right;
                lblMortgageRegistrationFee.Content = mortgageRegistrationFee.Item1.ToString("N");
                lblMortgageRegistrationFeeIntro.Content = mortgageRegistrationFee.Item2;
                buyerTotal = buyerTotal + mortgageRegistrationFee.Item1;
            }
            else
            {
                lblMortgageRegistrationFee.Content = "/";
                lblMortgageRegistrationFeeIntro.Content = "";
                lblMortgageRegistrationFee.HorizontalContentAlignment = HorizontalAlignment.Center;
            }

            //买方交易手续费
            var buyerTransactionFee = GetBuyerTransactionFee(area);
            lblBuyerTransactionFee.Content = buyerTransactionFee.Item1.ToString("N");
            lblBuyerTransactionFeeIntro.Content = buyerTransactionFee.Item2;
            buyerTotal = buyerTotal + buyerTransactionFee.Item1;




            if (rdoNotarizedYes.IsChecked == true)
            {
                //公证费
                var notaryFee = GetNotaryFee(price);
                if (rdoNotarizedBuyer.IsChecked == true)
                {
                    //买方承担
                    lblBuyerNotaryFee.Content = notaryFee.Item1.ToString("N");
                    lblBuyerNotaryFee.HorizontalContentAlignment = HorizontalAlignment.Right;
                    lblBuyerNotaryFeeIntro.Content = notaryFee.Item2;
                    buyerTotal = buyerTotal + notaryFee.Item1;

                    lblSellerNotaryFee.Content = "/";
                    lblSellerNotaryFeeIntro.Content = "";
                    lblSellerNotaryFee.HorizontalContentAlignment = HorizontalAlignment.Center;
                }
                else if (rdoNotarizedSeller.IsChecked == true)
                {
                    //卖方承担
                    lblSellerNotaryFee.Content = notaryFee.Item1.ToString("N");
                    lblSellerNotaryFee.HorizontalContentAlignment = HorizontalAlignment.Right;
                    lblSellerNotaryFeeIntro.Content = notaryFee.Item2;
                    buyerTotal = buyerTotal + notaryFee.Item1;

                    lblBuyerNotaryFee.Content = "/";
                    lblBuyerNotaryFeeIntro.Content = "";
                    lblBuyerNotaryFee.HorizontalContentAlignment = HorizontalAlignment.Center;
                }
                else if (rdoNotarizedBoth.IsChecked == true)
                {
                    //卖买双方各半
                    lblBuyerNotaryFee.HorizontalContentAlignment = HorizontalAlignment.Right;
                    lblBuyerNotaryFee.Content = (notaryFee.Item1 / 2).ToString("N");
                    lblBuyerNotaryFeeIntro.Content = notaryFee.Item2 + "  (买卖方各半)";
                    buyerTotal = buyerTotal + (notaryFee.Item1 / 2);

                    lblSellerNotaryFee.HorizontalContentAlignment = HorizontalAlignment.Right;
                    lblSellerNotaryFee.Content = (notaryFee.Item1 / 2).ToString("N");
                    lblSellerNotaryFeeIntro.Content = notaryFee.Item2 + "  (买卖方各半)";
                    sellerTotal = sellerTotal + (notaryFee.Item1 / 2);

                }
                else
                {
                    //买方公证费
                    lblBuyerNotaryFee.Content = "/";
                    lblBuyerNotaryFeeIntro.Content = "";
                    lblBuyerNotaryFee.HorizontalContentAlignment = HorizontalAlignment.Center;

                    //卖方公证费
                    lblSellerNotaryFee.Content = "/";
                    lblSellerNotaryFeeIntro.Content = "";
                    lblSellerNotaryFee.HorizontalContentAlignment = HorizontalAlignment.Center;
                }
            }
            else
            {
                //买方公证费
                lblBuyerNotaryFee.Content = "/";
                lblBuyerNotaryFeeIntro.Content = "";
                lblBuyerNotaryFee.HorizontalContentAlignment = HorizontalAlignment.Center;

                //卖方公证费
                lblSellerNotaryFee.Content = "/";
                lblSellerNotaryFeeIntro.Content = "";
                lblSellerNotaryFee.HorizontalContentAlignment = HorizontalAlignment.Center;
            }

            if (rdoCommissionedNotarizedYes.IsChecked == true)
            {
                if (chkCommissionedNotarizedBuyer.IsChecked == true)
                {
                    //买方委托公证费
                    var buyerCommissionedNotaryFee = GetBuyerCommissionedNotaryFee();
                    lblBuyerCommissionedNotaryFee.HorizontalContentAlignment = HorizontalAlignment.Right;
                    lblBuyerCommissionedNotaryFee.Content = buyerCommissionedNotaryFee.Item1.ToString("N");
                    lblBuyerCommissionedNotaryFeeIntro.Content = buyerCommissionedNotaryFee.Item2;
                    buyerTotal = buyerTotal + buyerCommissionedNotaryFee.Item1;
                }
                else
                {
                    lblBuyerCommissionedNotaryFee.Content = "/";
                    lblBuyerCommissionedNotaryFeeIntro.Content = "";
                    lblBuyerCommissionedNotaryFee.HorizontalContentAlignment = HorizontalAlignment.Center;
                }

                if (chkCommissionedNotarizedSeller.IsChecked == true)
                {
                    //卖方委托公证费
                    var sellerCommissionedNotaryFee = GetSellerCommissionedNotaryFee();
                    lblSellerCommissionedNotaryFee.HorizontalContentAlignment = HorizontalAlignment.Right;
                    lblSellerCommissionedNotaryFee.Content = sellerCommissionedNotaryFee.Item1.ToString("N");
                    lblSellerCommissionedNotaryFeeIntro.Content = sellerCommissionedNotaryFee.Item2;
                    sellerTotal = sellerTotal + sellerCommissionedNotaryFee.Item1;
                }
                else
                {
                    lblSellerCommissionedNotaryFee.Content = "/";
                    lblSellerCommissionedNotaryFeeIntro.Content = "";
                    lblSellerCommissionedNotaryFee.HorizontalContentAlignment = HorizontalAlignment.Center;
                }
            }
            else
            {
                //买方委托公证费
                lblBuyerCommissionedNotaryFee.Content = "/";
                lblBuyerCommissionedNotaryFeeIntro.Content = "";
                lblBuyerCommissionedNotaryFee.HorizontalContentAlignment = HorizontalAlignment.Center;

                //卖方委托公证费
                lblSellerCommissionedNotaryFee.Content = "/";
                lblSellerCommissionedNotaryFeeIntro.Content = "";
                lblSellerCommissionedNotaryFee.HorizontalContentAlignment = HorizontalAlignment.Center;
            }



            if (rdoLoanYes.IsChecked == true)
            {
                if (rdoLoanNotarizedYes.IsChecked == true)
                {
                    //买方贷款公证费
                    var creditNotarizationFee = GetCreditNotarizationFee(loan);
                    lblCreditNotarizationFee.HorizontalContentAlignment = HorizontalAlignment.Right;
                    lblCreditNotarizationFee.Content = creditNotarizationFee.Item1.ToString("N");
                    lblCreditNotarizationFeeIntro.Content = creditNotarizationFee.Item2;
                    buyerTotal = buyerTotal + creditNotarizationFee.Item1;
                }
                else
                {
                    lblCreditNotarizationFee.Content = "/";
                    lblCreditNotarizationFeeIntro.Content = "";
                    lblCreditNotarizationFee.HorizontalContentAlignment = HorizontalAlignment.Center;
                }

                //买方贷款评估费
                var loanAssessmentFee = GetLoanAssessmentFee(price);
                lblLoanAssessmentFee.HorizontalContentAlignment = HorizontalAlignment.Right;
                lblLoanAssessmentFee.Content = loanAssessmentFee.Item1.ToString("N");
                lblLoanAssessmentFeeIntro.Content = loanAssessmentFee.Item2;
                buyerTotal = buyerTotal + loanAssessmentFee.Item1;
            }
            else
            {
                //买方贷款公证费
                lblCreditNotarizationFee.Content = "/";
                lblCreditNotarizationFeeIntro.Content = "";
                lblCreditNotarizationFee.HorizontalContentAlignment = HorizontalAlignment.Center;

                //买方贷款评估费
                lblLoanAssessmentFee.Content = "/";
                lblLoanAssessmentFeeIntro.Content = "";
                lblLoanAssessmentFee.HorizontalContentAlignment = HorizontalAlignment.Center;
            }

            //卖方营业税
            var sellerBusinessTax = GetSellerBusinessTax(houseType, twoYear, price, originalPrice);
            lblSellerBusinessTax.Content = sellerBusinessTax.Item1.ToString("N");
            lblSellerBusinessTaxIntro.Content = sellerBusinessTax.Item2;
            sellerTotal = sellerTotal + sellerBusinessTax.Item1;

            //卖方个人所得税
            var sellerIncomeTax = GetSellerIncomeTax(priceType, fiveYear, houseType, onlyOne, price, originalPrice);
            lblSellerIncomeTax.Content = sellerIncomeTax.Item1.ToString("N");
            lblSellerIncomeTaxIntro.Content = sellerIncomeTax.Item2;
            sellerTotal = sellerTotal + sellerIncomeTax.Item1;

            //卖方交易手续费
            var sellerTransactionFee = GetSellerTransactionFee(area);
            lblSellerTransactionFee.Content = sellerTransactionFee.Item1.ToString("N");
            lblSellerTransactionFeeIntro.Content = sellerTransactionFee.Item2;
            sellerTotal = sellerTotal + sellerTransactionFee.Item1;

            lblBuyerTotal.Content = buyerTotal.ToString("N");
            lblBuyerTotalIntro.Content = ConvertSum(buyerTotal.ToString());

            lblSellerTotal.Content = sellerTotal.ToString("N");
            lblSellerTotalIntro.Content = ConvertSum(sellerTotal.ToString());

        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RdoPriceType_OnClick(object sender, RoutedEventArgs e)
        {
            //if (rdoDifferencePrice.IsChecked == true)
            //{
            //    grdOriginalPrice.Visibility = Visibility.Visible;
            //}
            //else
            //{
            //    grdOriginalPrice.Visibility = Visibility.Collapsed;
            //}
        }

        /// <summary>
        /// 计算卖方个人所得税
        /// </summary>
        /// <param name="priceType">价格类型：1总价；2差价</param>
        /// <param name="isFiveYear">是否满5年：true满5年；false未满5年</param>
        /// <param name="houseType">住宅类型：1普通住宅；2非普通住宅；3经济适用房</param>
        /// <param name="isOnlyOne">是否唯一住房：true是；false否</param>
        /// <param name="totalPrice">网签合同总价</param>
        /// <param name="originalPrice">原购入价</param>
        /// <returns></returns>
        private Tuple<decimal, string> GetSellerIncomeTax(int priceType, bool isFiveYear, int houseType, bool isOnlyOne, decimal totalPrice, decimal originalPrice = 0)
        {
            if (priceType == 2 && totalPrice <= originalPrice)
            {
                return new Tuple<decimal, string>(0, "");
            }

            //(普通住宅 & 未满5年 & 总价) 或者 （普通住宅 & 满5年 & 非唯一住房 & 总价）
            if ((houseType == 1 && isFiveYear == false && priceType == 1)
                || (houseType == 1 && isFiveYear == true && isOnlyOne == false && priceType == 1))
            {
                return new Tuple<decimal, string>(totalPrice * (decimal)0.01, "网签合同价*1%");
            }

            //(普通住宅 & 未满5年 & 差价) 或者 （普通住宅 & 满5年 & 非唯一住房 & 差价） 或者 （非普通住宅 & 未满5年 & 差价） 或者 （非普通住宅 & 满5年 & 非唯一住房 & 差价）
            if ((houseType == 1 && isFiveYear == false && priceType == 2)
                || (houseType == 1 && isFiveYear == true && isOnlyOne == false && priceType == 2)
                || (houseType == 2 && isFiveYear == false && priceType == 2)
                || (houseType == 2 && isFiveYear == true && isOnlyOne == false && priceType == 2))
            {
                return new Tuple<decimal, string>((totalPrice - originalPrice) * (decimal)0.2, "(网签合同价-原购入价)*20%");
            }

            //（普通住宅 & 满5年 & 唯一住房） 或者 （非普通住宅 & 满5年 & 唯一住房 ）
            if ((houseType == 1 && isFiveYear == true && isOnlyOne == true)
                || houseType == 2 && isFiveYear == true && isOnlyOne == true)
            {
                return new Tuple<decimal, string>(0, "");
            }

            //(非普通住宅 & 未满5年 & 总价) 或者 （非普通住宅 & 满5年 & 非唯一住房 & 总价）
            if ((houseType == 2 && isFiveYear == false && priceType == 1)
                || (houseType == 2 && isFiveYear == true && isOnlyOne == false && priceType == 1))
            {

                return new Tuple<decimal, string>(totalPrice * (decimal)0.02, "网签合同价*2%");
            }

            return new Tuple<decimal, string>(0, "");
        }

        /// <summary>
        /// 计算权证印花税
        /// </summary>
        /// <returns></returns>
        private Tuple<decimal, string> GetCertificateStamDuty()
        {
            const decimal result = 5;

            return new Tuple<decimal, string>(result, "固定金额");
        }

        /// <summary>
        /// 计算卖方营业税
        /// </summary>
        /// <param name="houseType">住宅类型：1普通住宅；2非普通住宅；3经济适用房</param>
        /// <param name="isTwoYear">是否满2年：true满2年；false未满2年</param>
        /// <param name="totalPrice">网签合同总价</param>
        /// <param name="originalPrice">原购入价</param>
        /// <returns></returns>
        private Tuple<decimal, string> GetSellerBusinessTax(int houseType, bool isTwoYear, decimal totalPrice, decimal originalPrice = 0)
        {
            decimal result = 0;
           
            //（普通住宅 & 未满2年） 或者 （非普通住宅 & 未满2年）
            if ((houseType == 1 && isTwoYear == false) || (houseType == 2 && isTwoYear == false))
            {
                return new Tuple<decimal, string>(totalPrice * (decimal)0.0565, "网签合同价*5.65%");
            }

            // 非普通住宅 & 满2年
            if (houseType == 2 && isTwoYear == true)
            {
                if (totalPrice <= originalPrice)
                {
                    return new Tuple<decimal, string>(0, "");
                }
                else
                {
                    return new Tuple<decimal, string>((totalPrice - originalPrice) * (decimal)0.0565, "(网签合同价-原购入价)*5.65%");    
                }
                
            }

            //普通住宅 & 满2年
            if (houseType == 1 && isTwoYear == true)
            {
                return new Tuple<decimal, string>(0, "");
            }


            return new Tuple<decimal, string>(0, "");
        }




        /// <summary>
        /// 计算买方契税
        /// </summary>
        /// <param name="houseType">住宅类型：1普通住宅；2非普通住宅；3经济适用房</param>
        /// <param name="isFiveYear">是否满5年：true满5年；false未满5年</param>
        /// <param name="isFirst">是否首次购房：true是首次；false不是首次</param>
        /// <param name="area">建筑面积</param>
        /// <param name="totalPrice">网签合同总价</param>
        /// <returns></returns>
        private Tuple<decimal, string> GetDeedTax(int houseType, bool isFiveYear, bool isFirst, decimal area, decimal totalPrice)
        {
            // （普通住宅 & 未满5年 & 首次购房 & 建筑面积<=90 ） 或者 （普通住宅 & 满5年 & 首次购房 & 建筑面积<=90）
            if ((houseType == 1 && isFiveYear == false && isFirst == true && area <= 90)
                || (houseType == 1 && isFiveYear == true && isFirst == true && area <= 90))
            {
                return new Tuple<decimal, string>(totalPrice * (decimal)0.01, "网签合同价*1% ");
            }

            // （普通住宅 & 未满5年 & 建筑面积>90 & 首次购房） 或者 （普通住宅 & 满5年 & 建筑面积>90 & 首次购房）
            if ((houseType == 1 && isFiveYear == false && area > 90 && isFirst == true)
                || (houseType == 1 && isFiveYear == true && area > 90 && isFirst == true))
            {
                return new Tuple<decimal, string>(totalPrice * (decimal)0.015, "网签合同价*1.5%");
            }

            // （普通住宅 & 未满5年 & 非首次购房） 或者 （普通住宅 & 满5年 & 非首次购房）或者（非普通住宅 & 满5年）或者（非普通住宅 & 不满5年）
            if ((houseType == 1 && isFiveYear == false && isFirst == false)
                || (houseType == 1 && isFiveYear == true && isFirst == false)
                || (houseType == 2 && isFiveYear == true)
                || (houseType == 2 && isFiveYear == false))
            {
                return new Tuple<decimal, string>(totalPrice * (decimal)0.03, "网签合同价*3%");
            }

            return new Tuple<decimal, string>(0, "");
        }


        /// <summary>
        /// 计算买方登记费
        /// </summary>
        /// <returns></returns>
        private Tuple<decimal, string> GetRegistrationFee()
        {
            return new Tuple<decimal, string>(80, "固定金额");
        }

        /// <summary>
        /// 计算买方图纸费
        /// </summary>
        /// <returns></returns>
        private Tuple<decimal, string> GetBlueprintFee()
        {
            return new Tuple<decimal, string>(25, "固定金额");
        }


        /// <summary>
        /// 计算抵押登记费
        /// </summary>
        /// <returns></returns>
        private Tuple<decimal, string> GetMortgageRegistrationFee()
        {
            return new Tuple<decimal, string>(80, "固定金额");
        }


        /// <summary>
        /// 计算买方交易手续费
        /// </summary>
        /// <param name="area"></param>
        /// <returns></returns>
        private Tuple<decimal, string> GetBuyerTransactionFee(decimal area)
        {
            return new Tuple<decimal, string>(area * (decimal)2.5, "建筑面积*2.5元");
        }

        /// <summary>
        /// 计算公证费
        /// </summary>
        /// <param name="totalPrice"></param>
        /// <returns></returns>
        private Tuple<decimal, string> GetNotaryFee(decimal totalPrice)
        {
            if (totalPrice <= 500 * 10000)
            {
                return new Tuple<decimal, string>(totalPrice * (decimal)0.0025 + 250, "合同价*0.25%+250");
            }
            else if (500 * 10000 < totalPrice && totalPrice <= 1000 * 10000)
            {
                return new Tuple<decimal, string>(totalPrice * (decimal)0.002 + 2750, "合同价*0.2%+2750");
            }
            else if (1000 * 10000 < totalPrice && totalPrice <= 2000 * 10000)
            {
                return new Tuple<decimal, string>(totalPrice * (decimal)0.0015 + 7750, "合同价*0.15%+7750");
            }
            else if (2000 * 10000 < totalPrice)
            {
                return new Tuple<decimal, string>(totalPrice * (decimal)0.001 + 17750, "合同价*0.1%+17750");
            }
            return new Tuple<decimal, string>(0, "");
        }


        /// <summary>
        /// 计算买方委托公证费
        /// </summary>
        /// <returns></returns>
        private Tuple<decimal, string> GetBuyerCommissionedNotaryFee()
        {
            return new Tuple<decimal, string>(500, "固定金额");
        }

        /// <summary>
        /// 计算买方贷款公证费
        /// </summary>
        /// <param name="loan"></param>
        /// <returns></returns>
        private Tuple<decimal, string> GetCreditNotarizationFee(decimal loan)
        {
            return new Tuple<decimal, string>(loan * (decimal)0.003, "贷款金额*3‰");
        }

        /// <summary>
        /// 计算买方贷款评估费
        /// </summary>
        /// <param name="totalPrice"></param>
        /// <returns></returns>
        private Tuple<decimal, string> GetLoanAssessmentFee(decimal totalPrice)
        {
            return new Tuple<decimal, string>(totalPrice * (decimal)0.001, "网签合同价*1‰");
        }

        /// <summary>
        /// 计算卖方交易手续费
        /// </summary>
        /// <param name="area"></param>
        /// <returns></returns>
        private Tuple<decimal, string> GetSellerTransactionFee(decimal area)
        {
            return new Tuple<decimal, string>(area * (decimal)2.5, "[建筑面积]*2.5元");
        }



        /// <summary>
        /// 计算卖方委托公证费
        /// </summary>
        /// <returns></returns>
        private Tuple<decimal, string> GetSellerCommissionedNotaryFee()
        {
            return new Tuple<decimal, string>(500, "固定金额");
        }












        private void TxtUnitPrice_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            //GetTotalPrice();
        }

        private void TxtArea_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            //if (txtUnitPrice.Text != "")
            //{
            //    GetTotalPrice();
            //}


        }

        private void GetTotalPrice()
        {
            decimal area = 0;
            decimal unitPrice = 0;

            decimal.TryParse(txtArea.Text.Trim(), out area);
            decimal.TryParse(txtUnitPrice.Text.Trim(), out unitPrice);

            if (area != 0 && unitPrice != 0)
            {
                txtTotalPrice.Text = (area * unitPrice / 10000).ToString();
            }
            else
            {
                txtTotalPrice.Text = "";
            }
        }

        private void RdoNotarizedYes_OnChecked(object sender, RoutedEventArgs e)
        {
            spNotarized.IsEnabled = true;
        }

        private void RdoNotarizedNo_OnChecked(object sender, RoutedEventArgs e)
        {
            spNotarized.IsEnabled = false;
            rdoNotarizedBuyer.IsChecked = false;
            rdoNotarizedSeller.IsChecked = false;
            rdoNotarizedBoth.IsChecked = false;
        }

        private void RdoLoanYes_OnChecked(object sender, RoutedEventArgs e)
        {
            spLoan.IsEnabled = true;
            spLoanNotarized.IsEnabled = true;
        }

        private void RdoLoanNo_OnChecked(object sender, RoutedEventArgs e)
        {
            spLoan.IsEnabled = false;
            spLoanNotarized.IsEnabled = false;
            txtLoan.Text = "";
            rdoLoanNotarizedYes.IsChecked = false;
            rdoLoanNotarizedNo.IsChecked = false;

        }

        private void RdoCommissionedNotarizedYes_OnChecked(object sender, RoutedEventArgs e)
        {
            spCommissionedNotarized.IsEnabled = true;
        }

        private void RdoCommissionedNotarizedNo_OnChecked(object sender, RoutedEventArgs e)
        {
            spCommissionedNotarized.IsEnabled = false;
            chkCommissionedNotarizedBuyer.IsChecked = false;
            chkCommissionedNotarizedSeller.IsChecked = false;
        }

        ///
        /// 转换数字金额主函数（包括小数）
        ///
        /// 数字字符串
        /// 转换成中文大写后的字符串或者出错信息提示字符串
        public string ConvertSum(string str)
        {
            if (!IsPositveDecimal(str))
                return "输入的不是正数字！";
            if (Double.Parse(str) > 999999999999.99)
                return "数字太大，无法换算，请输入一万亿元以下的金额";
            char[] ch = new char[1];
            ch[0] = '.'; //小数点
            string[] splitstr = null; //定义按小数点分割后的字符串数组
            splitstr = str.Split(ch[0]);//按小数点分割字符串
            if (splitstr.Length == 1) //只有整数部分
                return ConvertData(str) + "圆整";
            else //有小数部分
            {
                string rstr;
                rstr = ConvertData(splitstr[0]) + "圆";//转换整数部分
                rstr += ConvertXiaoShu(splitstr[1]);//转换小数部分
                return rstr;
            }

        }

        ///
        /// 判断是否是正数字字符串
        ///
        /// 判断字符串
        /// 如果是数字，返回true，否则返回false
        public bool IsPositveDecimal(string str)
        {
            Decimal d;
            try
            {
                d = Decimal.Parse(str);

            }
            catch (Exception)
            {
                return false;
            }
            if (d > 0)
                return true;
            else
                return false;
        }

        ///
        /// 转换数字（整数）
        ///
        /// 需要转换的整数数字字符串
        /// 转换成中文大写后的字符串
        public string ConvertData(string str)
        {
            string tmpstr = "";
            string rstr = "";
            int strlen = str.Length;
            if (strlen <= 4)//数字长度小于四位
            {
                rstr = ConvertDigit(str);

            }
            else
            {

                if (strlen <= 8)//数字长度大于四位，小于八位
                {
                    tmpstr = str.Substring(strlen - 4, 4);//先截取最后四位数字
                    rstr = ConvertDigit(tmpstr);//转换最后四位数字
                    tmpstr = str.Substring(0, strlen - 4);//截取其余数字
                    //将两次转换的数字加上萬后相连接
                    rstr = String.Concat(ConvertDigit(tmpstr) + "萬", rstr);
                    rstr = rstr.Replace("零萬", "萬");
                    rstr = rstr.Replace("零零", "零");

                }
                else
                    if (strlen <= 12)//数字长度大于八位，小于十二位
                    {
                        tmpstr = str.Substring(strlen - 4, 4);//先截取最后四位数字
                        rstr = ConvertDigit(tmpstr);//转换最后四位数字
                        tmpstr = str.Substring(strlen - 8, 4);//再截取四位数字
                        rstr = String.Concat(ConvertDigit(tmpstr) + "萬", rstr);
                        tmpstr = str.Substring(0, strlen - 8);
                        rstr = String.Concat(ConvertDigit(tmpstr) + "億", rstr);
                        rstr = rstr.Replace("零億", "億");
                        rstr = rstr.Replace("零萬", "零");
                        rstr = rstr.Replace("零零", "零");
                        rstr = rstr.Replace("零零", "零");
                    }
            }
            strlen = rstr.Length;
            if (strlen >= 2)
            {
                switch (rstr.Substring(strlen - 2, 2))
                {
                    case "佰零": rstr = rstr.Substring(0, strlen - 2) + "佰"; break;
                    case "仟零": rstr = rstr.Substring(0, strlen - 2) + "仟"; break;
                    case "萬零": rstr = rstr.Substring(0, strlen - 2) + "萬"; break;
                    case "億零": rstr = rstr.Substring(0, strlen - 2) + "億"; break;

                }
            }

            return rstr;
        }
        ///
        /// 转换数字（小数部分）
        ///
        /// 需要转换的小数部分数字字符串
        /// 转换成中文大写后的字符串
        public string ConvertXiaoShu(string str)
        {
            int strlen = str.Length;
            string rstr;
            if (strlen == 1)
            {
                rstr = ConvertChinese(str) + "角";
                return rstr;
            }
            else
            {
                string tmpstr = str.Substring(0, 1);
                rstr = ConvertChinese(tmpstr) + "角";
                tmpstr = str.Substring(1, 1);
                rstr += ConvertChinese(tmpstr) + "分";
                rstr = rstr.Replace("零分", "");
                rstr = rstr.Replace("零角", "");
                return rstr;
            }


        }

        ///
        /// 转换数字
        ///
        /// 转换的字符串（四位以内）
        ///
        public string ConvertDigit(string str)
        {
            int strlen = str.Length;
            string rstr = "";
            switch (strlen)
            {
                case 1: rstr = ConvertChinese(str); break;
                case 2: rstr = Convert2Digit(str); break;
                case 3: rstr = Convert3Digit(str); break;
                case 4: rstr = Convert4Digit(str); break;
            }
            rstr = rstr.Replace("拾零", "拾");
            strlen = rstr.Length;

            return rstr;
        }


        ///
        /// 转换四位数字
        ///
        public string Convert4Digit(string str)
        {
            string str1 = str.Substring(0, 1);
            string str2 = str.Substring(1, 1);
            string str3 = str.Substring(2, 1);
            string str4 = str.Substring(3, 1);
            string rstring = "";
            rstring += ConvertChinese(str1) + "仟";
            rstring += ConvertChinese(str2) + "佰";
            rstring += ConvertChinese(str3) + "拾";
            rstring += ConvertChinese(str4);
            rstring = rstring.Replace("零仟", "零");
            rstring = rstring.Replace("零佰", "零");
            rstring = rstring.Replace("零拾", "零");
            rstring = rstring.Replace("零零", "零");
            rstring = rstring.Replace("零零", "零");
            rstring = rstring.Replace("零零", "零");
            return rstring;
        }
        ///
        /// 转换三位数字
        ///
        public string Convert3Digit(string str)
        {
            string str1 = str.Substring(0, 1);
            string str2 = str.Substring(1, 1);
            string str3 = str.Substring(2, 1);
            string rstring = "";
            rstring += ConvertChinese(str1) + "佰";
            rstring += ConvertChinese(str2) + "拾";
            rstring += ConvertChinese(str3);
            rstring = rstring.Replace("零佰", "零");
            rstring = rstring.Replace("零拾", "零");
            rstring = rstring.Replace("零零", "零");
            rstring = rstring.Replace("零零", "零");
            return rstring;
        }
        ///
        /// 转换二位数字
        ///
        public string Convert2Digit(string str)
        {
            string str1 = str.Substring(0, 1);
            string str2 = str.Substring(1, 1);
            string rstring = "";
            rstring += ConvertChinese(str1) + "拾";
            rstring += ConvertChinese(str2);
            rstring = rstring.Replace("零拾", "零");
            rstring = rstring.Replace("零零", "零");
            return rstring;
        }
        ///
        /// 将一位数字转换成中文大写数字
        ///
        public string ConvertChinese(string str)
        {
            //"零壹贰叁肆伍陆柒捌玖拾佰仟萬億圆整角分"
            string cstr = "";
            switch (str)
            {
                case "0": cstr = "零"; break;
                case "1": cstr = "壹"; break;
                case "2": cstr = "贰"; break;
                case "3": cstr = "叁"; break;
                case "4": cstr = "肆"; break;
                case "5": cstr = "伍"; break;
                case "6": cstr = "陆"; break;
                case "7": cstr = "柒"; break;
                case "8": cstr = "捌"; break;
                case "9": cstr = "玖"; break;
            }
            return (cstr);
        }

        private void BtnBuyerDownload_OnClick(object sender, RoutedEventArgs e)
        {

            var doc = new XWPFDocument();

            XWPFTable table = doc.CreateTable(13, 4);
            table.Width = 5000;

            CT_Tc cttc = table.GetRow(0).GetCell(0).GetCTTc();
            CT_TcPr m_Pr = cttc.AddNewTcPr();
            m_Pr.tcW = new CT_TblWidth();
            m_Pr.tcW.w = "530";//单元格宽
            m_Pr.tcW.type = ST_TblWidth.dxa;
            table.GetRow(0).GetCell(0).SetColor("#F6F6F6");
            table.GetRow(0).GetCell(0).SetText("承担方");
            table.GetRow(0).GetCell(0).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            cttc.GetPList()[0].AddNewPPr().AddNewJc().val = ST_Jc.center;

            cttc = table.GetRow(0).GetCell(1).GetCTTc();
            m_Pr = cttc.AddNewTcPr();
            m_Pr.tcW = new CT_TblWidth();
            m_Pr.tcW.w = "833";//单元格宽
            m_Pr.tcW.type = ST_TblWidth.dxa;
            table.GetRow(0).GetCell(1).SetColor("#F6F6F6");
            table.GetRow(0).GetCell(1).SetText("费用名称");
            table.GetRow(0).GetCell(1).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            cttc.GetPList()[0].AddNewPPr().AddNewJc().val = ST_Jc.center;

            cttc = table.GetRow(0).GetCell(2).GetCTTc();
            m_Pr = cttc.AddNewTcPr();
            m_Pr.tcW = new CT_TblWidth();
            m_Pr.tcW.w = "757";//单元格宽
            m_Pr.tcW.type = ST_TblWidth.dxa;
            table.GetRow(0).GetCell(2).SetColor("#F6F6F6");
            table.GetRow(0).GetCell(2).SetText("金额（元）");
            table.GetRow(0).GetCell(2).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            cttc.GetPList()[0].AddNewPPr().AddNewJc().val = ST_Jc.center;

            cttc = table.GetRow(0).GetCell(3).GetCTTc();
            m_Pr = cttc.AddNewTcPr();
            m_Pr.tcW = new CT_TblWidth();
            m_Pr.tcW.w = "2878";//单元格宽
            m_Pr.tcW.type = ST_TblWidth.dxa;
            table.GetRow(0).GetCell(3).SetColor("#F6F6F6");
            table.GetRow(0).GetCell(3).SetText("说明");
            table.GetRow(0).GetCell(3).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            cttc.GetPList()[0].AddNewPPr().AddNewJc().val = ST_Jc.center;

            //设置行高
            foreach (XWPFTableRow row in table.Rows)
            {
                row.Height = 400;
            }

            cttc = table.GetRow(1).GetCell(0).GetCTTc();
            m_Pr = cttc.AddNewTcPr();
            m_Pr.AddNewVMerge().val = ST_Merge.restart;
            m_Pr.AddNewVAlign().val = ST_VerticalJc.center;//垂直
            cttc.GetPList()[0].AddNewPPr().AddNewJc().val = ST_Jc.center;
            cttc.GetPList()[0].AddNewR().AddNewT().Value = "买方";
            table.GetRow(2).GetCell(0).GetCTTc().AddNewTcPr().AddNewVMerge().val = ST_Merge.@continue;
            table.GetRow(3).GetCell(0).GetCTTc().AddNewTcPr().AddNewVMerge().val = ST_Merge.@continue;
            table.GetRow(4).GetCell(0).GetCTTc().AddNewTcPr().AddNewVMerge().val = ST_Merge.@continue;
            table.GetRow(5).GetCell(0).GetCTTc().AddNewTcPr().AddNewVMerge().val = ST_Merge.@continue;
            table.GetRow(6).GetCell(0).GetCTTc().AddNewTcPr().AddNewVMerge().val = ST_Merge.@continue;
            table.GetRow(7).GetCell(0).GetCTTc().AddNewTcPr().AddNewVMerge().val = ST_Merge.@continue;
            table.GetRow(8).GetCell(0).GetCTTc().AddNewTcPr().AddNewVMerge().val = ST_Merge.@continue;
            table.GetRow(9).GetCell(0).GetCTTc().AddNewTcPr().AddNewVMerge().val = ST_Merge.@continue;
            table.GetRow(10).GetCell(0).GetCTTc().AddNewTcPr().AddNewVMerge().val = ST_Merge.@continue;
            table.GetRow(11).GetCell(0).GetCTTc().AddNewTcPr().AddNewVMerge().val = ST_Merge.@continue;

            cttc = table.GetRow(12).GetCell(0).GetCTTc();
            table.GetRow(12).GetCell(0).SetText("合计");
            table.GetRow(12).GetCell(0).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            cttc.GetPList()[0].AddNewPPr().AddNewJc().val = ST_Jc.center;


            table.GetRow(1).GetCell(1).SetText("契税");
            table.GetRow(1).GetCell(1).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(1).GetCell(2).SetText(lblDeedTax.Content.ToString());
            table.GetRow(1).GetCell(3).SetText(lblDeedTaxIntro.Content.ToString());
            table.GetRow(1).GetCell(2).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(1).GetCell(3).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);

            cttc = table.GetRow(2).GetCell(2).GetCTTc();
            table.GetRow(2).GetCell(1).SetText("房产税");
            table.GetRow(2).GetCell(1).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(2).GetCell(2).SetText(lblHouseTax.Content.ToString());
            table.GetRow(2).GetCell(3).SetText(lblHouseTaxIntro.Content.ToString());
            table.GetRow(2).GetCell(2).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(2).GetCell(3).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            //cttc.GetPList()[0].AddNewPPr().AddNewJc().val = ST_Jc.center;

            table.GetRow(3).GetCell(1).SetText("权证印花税");
            table.GetRow(3).GetCell(1).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(3).GetCell(2).SetText(lblCertificateStamDuty.Content.ToString());
            table.GetRow(3).GetCell(3).SetText(lblCertificateStamDutyIntro.Content.ToString());
            table.GetRow(3).GetCell(2).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(3).GetCell(3).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);

            table.GetRow(4).GetCell(1).SetText("登记费");
            table.GetRow(4).GetCell(1).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(4).GetCell(2).SetText(lblRegistrationFee.Content.ToString());
            table.GetRow(4).GetCell(3).SetText(lblRegistrationFeeIntro.Content.ToString());
            table.GetRow(4).GetCell(2).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(4).GetCell(3).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);

            table.GetRow(5).GetCell(1).SetText("图纸费");
            table.GetRow(5).GetCell(1).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(5).GetCell(2).SetText(lblBlueprintFee.Content.ToString());
            table.GetRow(5).GetCell(3).SetText(lblBlueprintFeeIntro.Content.ToString());
            table.GetRow(5).GetCell(2).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(5).GetCell(3).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);

            table.GetRow(6).GetCell(1).SetText("抵押登记费");
            table.GetRow(6).GetCell(1).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(6).GetCell(2).SetText(lblMortgageRegistrationFee.Content.ToString());
            table.GetRow(6).GetCell(3).SetText(lblMortgageRegistrationFeeIntro.Content.ToString());
            table.GetRow(6).GetCell(2).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(6).GetCell(3).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);

            table.GetRow(7).GetCell(1).SetText("交易手续费");
            table.GetRow(7).GetCell(1).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(7).GetCell(2).SetText(lblBuyerTransactionFee.Content.ToString());
            table.GetRow(7).GetCell(3).SetText(lblBuyerTransactionFeeIntro.Content.ToString());
            table.GetRow(7).GetCell(2).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(7).GetCell(3).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);

            table.GetRow(8).GetCell(1).SetText("公证费");
            table.GetRow(8).GetCell(1).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(8).GetCell(2).SetText(lblBuyerNotaryFee.Content.ToString());
            table.GetRow(8).GetCell(3).SetText(lblBuyerNotaryFeeIntro.Content.ToString());
            table.GetRow(8).GetCell(2).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(8).GetCell(3).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);

            table.GetRow(9).GetCell(1).SetText("委托公证费");
            table.GetRow(9).GetCell(1).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(9).GetCell(2).SetText(lblBuyerCommissionedNotaryFee.Content.ToString());
            table.GetRow(9).GetCell(3).SetText(lblBuyerCommissionedNotaryFeeIntro.Content.ToString());
            table.GetRow(9).GetCell(2).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(9).GetCell(3).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);

            table.GetRow(10).GetCell(1).SetText("贷款公证费");
            table.GetRow(10).GetCell(1).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(10).GetCell(2).SetText(lblCreditNotarizationFee.Content.ToString());
            table.GetRow(10).GetCell(3).SetText(lblCreditNotarizationFeeIntro.Content.ToString());
            table.GetRow(10).GetCell(2).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(10).GetCell(3).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);

            table.GetRow(11).GetCell(1).SetText("贷款评估费");
            table.GetRow(11).GetCell(1).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(11).GetCell(2).SetText(lblLoanAssessmentFee.Content.ToString());
            table.GetRow(11).GetCell(3).SetText(lblLoanAssessmentFeeIntro.Content.ToString());
            table.GetRow(11).GetCell(2).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(11).GetCell(3).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);

            cttc = table.GetRow(12).GetCell(1).GetCTTc();
            table.GetRow(12).GetCell(1).SetText("/");
            table.GetRow(12).GetCell(1).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            cttc.GetPList()[0].AddNewPPr().AddNewJc().val = ST_Jc.center;
            table.GetRow(12).GetCell(2).SetText(lblBuyerTotal.Content.ToString());
            table.GetRow(12).GetCell(3).SetText(lblBuyerTotalIntro.Content.ToString());
            table.GetRow(12).GetCell(2).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(12).GetCell(3).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);

            XWPFParagraph para1 = doc.CreateParagraph();
            XWPFRun r1 = para1.CreateRun();
            r1.SetText("实际税费以税务机关核定征收为准");
            r1.GetCTR().AddNewRPr().AddNewColor().val = "#CCCCCC";//字体颜色
            para1.FillBackgroundColor = "#FFFFFF";

            doc.CreateParagraph();
            doc.CreateParagraph();

            XWPFParagraph para2 = doc.CreateParagraph();
            XWPFRun r2 = para2.CreateRun();
            r2.SetText("买方客户签字：______________时间：_______年_______月_______日");
            para2.FillBackgroundColor = "#FFFFFF";
            para2.Alignment = ParagraphAlignment.RIGHT;


            var dialog = new SaveFileDialog();
            dialog.Filter = " docx files(*.docx)|*.docx|All files(*.*)|*.*";
            dialog.FileName = "买方税费试算";
            try
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    using (FileStream out1 = new FileStream(Convert.ToString(dialog.FileName), FileMode.Create))
                    {
                        doc.Write(out1);
                        out1.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }


        }

        private void BtnSellerDownload_OnClick(object sender, RoutedEventArgs e)
        {
            var doc = new XWPFDocument();

            XWPFTable table = doc.CreateTable(7, 4);
            table.Width = 5000;

            CT_Tc cttc = table.GetRow(0).GetCell(0).GetCTTc();
            CT_TcPr m_Pr = cttc.AddNewTcPr();
            m_Pr.tcW = new CT_TblWidth();
            m_Pr.tcW.w = "530";//单元格宽
            m_Pr.tcW.type = ST_TblWidth.dxa;
            table.GetRow(0).GetCell(0).SetColor("#F6F6F6");
            table.GetRow(0).GetCell(0).SetText("承担方");
            table.GetRow(0).GetCell(0).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            cttc.GetPList()[0].AddNewPPr().AddNewJc().val = ST_Jc.center;

            cttc = table.GetRow(0).GetCell(1).GetCTTc();
            m_Pr = cttc.AddNewTcPr();
            m_Pr.tcW = new CT_TblWidth();
            m_Pr.tcW.w = "833";//单元格宽
            m_Pr.tcW.type = ST_TblWidth.dxa;
            table.GetRow(0).GetCell(1).SetColor("#F6F6F6");
            table.GetRow(0).GetCell(1).SetText("费用名称");
            table.GetRow(0).GetCell(1).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            cttc.GetPList()[0].AddNewPPr().AddNewJc().val = ST_Jc.center;

            cttc = table.GetRow(0).GetCell(2).GetCTTc();
            m_Pr = cttc.AddNewTcPr();
            m_Pr.tcW = new CT_TblWidth();
            m_Pr.tcW.w = "757";//单元格宽
            m_Pr.tcW.type = ST_TblWidth.dxa;
            table.GetRow(0).GetCell(2).SetColor("#F6F6F6");
            table.GetRow(0).GetCell(2).SetText("金额（元）");
            table.GetRow(0).GetCell(2).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            cttc.GetPList()[0].AddNewPPr().AddNewJc().val = ST_Jc.center;

            cttc = table.GetRow(0).GetCell(3).GetCTTc();
            m_Pr = cttc.AddNewTcPr();
            m_Pr.tcW = new CT_TblWidth();
            m_Pr.tcW.w = "2878";//单元格宽
            m_Pr.tcW.type = ST_TblWidth.dxa;
            table.GetRow(0).GetCell(3).SetColor("#F6F6F6");
            table.GetRow(0).GetCell(3).SetText("说明");
            table.GetRow(0).GetCell(3).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            cttc.GetPList()[0].AddNewPPr().AddNewJc().val = ST_Jc.center;

            //设置行高
            foreach (XWPFTableRow row in table.Rows)
            {
                row.Height = 400;
            }

            cttc = table.GetRow(1).GetCell(0).GetCTTc();
            m_Pr = cttc.AddNewTcPr();
            m_Pr.AddNewVMerge().val = ST_Merge.restart;
            m_Pr.AddNewVAlign().val = ST_VerticalJc.center;//垂直
            cttc.GetPList()[0].AddNewPPr().AddNewJc().val = ST_Jc.center;
            cttc.GetPList()[0].AddNewR().AddNewT().Value = "卖方";
            table.GetRow(2).GetCell(0).GetCTTc().AddNewTcPr().AddNewVMerge().val = ST_Merge.@continue;
            table.GetRow(3).GetCell(0).GetCTTc().AddNewTcPr().AddNewVMerge().val = ST_Merge.@continue;
            table.GetRow(4).GetCell(0).GetCTTc().AddNewTcPr().AddNewVMerge().val = ST_Merge.@continue;
            table.GetRow(5).GetCell(0).GetCTTc().AddNewTcPr().AddNewVMerge().val = ST_Merge.@continue;

            cttc = table.GetRow(6).GetCell(0).GetCTTc();
            table.GetRow(6).GetCell(0).SetText("合计");
            table.GetRow(6).GetCell(0).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            cttc.GetPList()[0].AddNewPPr().AddNewJc().val = ST_Jc.center;


            table.GetRow(1).GetCell(1).SetText("营业税");
            table.GetRow(1).GetCell(1).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(1).GetCell(2).SetText(lblSellerBusinessTax.Content.ToString());
            table.GetRow(1).GetCell(3).SetText(lblSellerBusinessTaxIntro.Content.ToString());
            table.GetRow(1).GetCell(2).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(1).GetCell(3).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);

            cttc = table.GetRow(2).GetCell(2).GetCTTc();
            table.GetRow(2).GetCell(1).SetText("个人所得税");
            table.GetRow(2).GetCell(1).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(2).GetCell(2).SetText(lblSellerIncomeTax.Content.ToString());
            table.GetRow(2).GetCell(3).SetText(lblSellerIncomeTaxIntro.Content.ToString());
            table.GetRow(2).GetCell(2).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(2).GetCell(3).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            //cttc.GetPList()[0].AddNewPPr().AddNewJc().val = ST_Jc.center;

            table.GetRow(3).GetCell(1).SetText("交易手续费");
            table.GetRow(3).GetCell(1).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(3).GetCell(2).SetText(lblSellerTransactionFee.Content.ToString());
            table.GetRow(3).GetCell(3).SetText(lblSellerTransactionFeeIntro.Content.ToString());
            table.GetRow(3).GetCell(2).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(3).GetCell(3).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);

            table.GetRow(4).GetCell(1).SetText("公证费");
            table.GetRow(4).GetCell(1).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(4).GetCell(2).SetText(lblSellerNotaryFee.Content.ToString());
            table.GetRow(4).GetCell(3).SetText(lblSellerNotaryFeeIntro.Content.ToString());
            table.GetRow(4).GetCell(2).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(4).GetCell(3).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);

            table.GetRow(5).GetCell(1).SetText("委托公证费");
            table.GetRow(5).GetCell(1).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(5).GetCell(2).SetText(lblSellerCommissionedNotaryFee.Content.ToString());
            table.GetRow(5).GetCell(3).SetText(lblSellerCommissionedNotaryFeeIntro.Content.ToString());
            table.GetRow(5).GetCell(2).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(5).GetCell(3).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);

            cttc = table.GetRow(6).GetCell(1).GetCTTc();
            table.GetRow(6).GetCell(1).SetText("/");
            table.GetRow(6).GetCell(1).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            cttc.GetPList()[0].AddNewPPr().AddNewJc().val = ST_Jc.center;
            table.GetRow(6).GetCell(2).SetText(lblBuyerTotal.Content.ToString());
            table.GetRow(6).GetCell(3).SetText(lblBuyerTotalIntro.Content.ToString());
            table.GetRow(6).GetCell(2).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            table.GetRow(6).GetCell(3).SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);

            XWPFParagraph para1 = doc.CreateParagraph();
            XWPFRun r1 = para1.CreateRun();
            r1.SetText("实际税费以税务机关核定征收为准");
            r1.GetCTR().AddNewRPr().AddNewColor().val = "#CCCCCC";//字体颜色
            para1.FillBackgroundColor = "#FFFFFF";

            doc.CreateParagraph();
            doc.CreateParagraph();

            XWPFParagraph para2 = doc.CreateParagraph();
            XWPFRun r2 = para2.CreateRun();
            r2.SetText("卖方客户签字：______________时间：_______年_______月_______日");
            para2.FillBackgroundColor = "#FFFFFF";
            para2.Alignment = ParagraphAlignment.RIGHT;


            var dialog = new SaveFileDialog();
            dialog.Filter = " docx files(*.docx)|*.docx|All files(*.*)|*.*";
            dialog.FileName = "卖方税费试算";
            try
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    using (FileStream out1 = new FileStream(Convert.ToString(dialog.FileName), FileMode.Create))
                    {
                        doc.Write(out1);
                        out1.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void RdoFiveYearYes_OnChecked(object sender, RoutedEventArgs e)
        {
            spTwoYear.IsEnabled = true;
            rdoTwoYearYes.IsChecked = true;
            rdoTwoYearNo.IsChecked = false;
        }

        private void RdoFiveYearNo_OnChecked(object sender, RoutedEventArgs e)
        {
            //if (spTwoYear != null)
            //{
            //    spTwoYear.IsEnabled = true;
            //    rdoTwoYearNo.IsChecked = true;
            //}

        }

        private void RdoTwoYearYes_OnChecked(object sender, RoutedEventArgs e)
        {
            if (spFiveYear != null)
            {
                spFiveYear.IsEnabled = true;
                rdoFiveYearNo.IsChecked = false;
                rdoFiveYearYes.IsChecked = false;
            }
        }

        private void RdoTwoYearNo_OnChecked(object sender, RoutedEventArgs e)
        {
            if (spFiveYear != null)
            {
                spFiveYear.IsEnabled = false;
                rdoFiveYearNo.IsChecked = false;
                rdoFiveYearYes.IsChecked = false;
            }
        }
    }
}
