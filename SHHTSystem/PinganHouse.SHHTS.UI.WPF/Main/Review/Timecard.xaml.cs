﻿using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.ReadCard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Review
{
    /// <summary>
    /// Timecard.xaml 的交互逻辑
    /// </summary>
    public partial class Timecard : ReviewBaseWindow
    {
        public Timecard(long reviewSysNo, string reviewName)
            : base(reviewSysNo, reviewName)
        {
            InitializeComponent();
            MessageBox.Show("请找以下人员刷工牌复核：" + reviewName, "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
            this.Activated += ShowStaffCardReader;
        }
     
        private void ValidateReviewRule(object sender, EventArgs e)
        {
            //是否读卡错误
            if (((StaffId)sender).IsReadError)
            {
                this.ReBuild();
                return;
            }              
            string staffCardNo = ((StaffId)sender).StaffNo;
            if (string.IsNullOrWhiteSpace(staffCardNo))
            {
                this.Close();
                return;
            }


            var staff = PinganStaffServiceProxy.GetByStaffCardNo(staffCardNo);
            if (staff == null)
            {            
                MessageBox.Show("该工牌还未绑定, 请重新扫描", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                this.ReBuild();
                return;
            }
            this.ValidateReviewRule(staff.UserSysNo, ReviewMode.Timecard);
        }

        public void ShowStaffCardReader(object sender, EventArgs e)
        {
            ReBuild();
        }
        protected override void ReBuild()
        {
            this.Hide();
            StaffId staffWindow = new StaffId();
            staffWindow.Closed -= ValidateReviewRule;
            staffWindow.Closed += ValidateReviewRule;
            staffWindow.ShowDialog();
        }
    }
}
