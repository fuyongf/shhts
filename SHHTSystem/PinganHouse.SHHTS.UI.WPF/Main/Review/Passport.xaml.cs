﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Review
{
    /// <summary>
    /// Passport.xaml 的交互逻辑
    /// </summary>
    public partial class Passport : ReviewBaseWindow
    {
        public Passport(long reviewSysNo, string reviewName)
            : base(reviewSysNo, reviewName)
        {
            InitializeComponent();
            this.lbl_reviewer.Content = this.ReviewerName;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.userName.Focus();
            if (string.IsNullOrWhiteSpace(this.userName.Text))
            {
                MessageBox.Show("复核失败：用户名不可为空", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (string.IsNullOrWhiteSpace(this.password.Password))
            {
                MessageBox.Show("复核失败：密码不可为空", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                this.password.Focus();
                return;
            }
            var result = UserServiceProxy.Login(this.userName.Text.Trim(), this.password.Password.Trim());
            if (!result.Success)
            {
                MessageBox.Show("复核失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                ReBuild();
                return;
            }
            var user = (User)result.OtherData["UserInfo"];
            this.ValidateReviewRule(user.SysNo, ReviewMode.Passport);
            //ReviewDto reviewDto=null;
            //var reviewResult = ReviewProxyService.GetInstanse().ValidateReviewRule(this.ReviewSysNo, user.SysNo, ReviewMode.Passport, out reviewDto);
            //if(!reviewResult.Success)
            //{
            //    MessageBox.Show("复核失败：" + reviewResult.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    Clear();
            //    return;
            //}
            //this.Success=true;
            //if (reviewDto != null)
            //{
            //    this.Hide();
            //    this.ShowDialog(reviewDto.ReviewMode, string.Join(",", reviewDto.ReviewName));
            //}
            //this.Close();
        }

        protected override void ReBuild()
        {
            this.password.Password = "";
            this.userName.Text = "";       
        }
        /// <summary>
        /// Enter 事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Button_Click(sender, e);
            }
        }
    }
}
