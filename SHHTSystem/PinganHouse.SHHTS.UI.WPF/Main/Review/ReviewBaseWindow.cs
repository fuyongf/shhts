﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PinganHouse.SHHTS.UI.WPF.Main.Review
{
    public class ReviewBaseWindow : Window
    {
        /// <summary>
        /// 复核编号
        /// </summary>
        public long ReviewSysNo { get; private set; }

        /// <summary>
        /// 复核人
        /// </summary>
        public string ReviewerName { get; private set; }

        private bool success;
        public bool Success
        {
            get
            {
                if (reviewWindow != null)
                    success = reviewWindow.Success;
                return success;
            }
            protected set { this.success = value; }
        }

        private ReviewBaseWindow reviewWindow;

        public ReviewBaseWindow(long reviewSysNo, string reviewerName)
            : base()
        {
            this.ReviewSysNo = reviewSysNo;
            this.ReviewerName = reviewerName;
        }

        public bool? ShowDialog(ReviewMode reviewMode, string reviewerName=null)
        {
            if (string.IsNullOrWhiteSpace(reviewerName))
                reviewerName = ReviewerName;
            reviewWindow = System.Activator.CreateInstance(typeof(ReviewBaseWindow).Assembly.GetName().Name, string.Format("{0}.{1}", typeof(ReviewBaseWindow).Namespace, reviewMode.ToString()), true, System.Reflection.BindingFlags.Default, null, new object[] { ReviewSysNo, reviewerName }, null, null).Unwrap() as ReviewBaseWindow;
            if (reviewWindow == null)
                throw new ArgumentException("构造复核窗口失败");
            return reviewWindow.ShowDialog();
        }

        protected virtual void ValidateReviewRule(long reviewerSysNo, ReviewMode reviewMode)
        {
            ReviewDto reviewDto = null;
            var reviewResult = ReviewProxyService.GetInstanse().ValidateReviewRule(this.ReviewSysNo, reviewerSysNo, reviewMode, out reviewDto);
            if (!reviewResult.Success)
            {
                MessageBox.Show("复核失败：" + reviewResult.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                this.ReBuild();
                return;
            }
            this.Success = true;
            if (reviewDto != null)
            {
                this.Hide();
                this.ShowDialog(reviewDto.ReviewMode, string.Join(",", reviewDto.ReviewName));
            }
            this.Close();
        }

        protected virtual void ReBuild() { }
    }
}
