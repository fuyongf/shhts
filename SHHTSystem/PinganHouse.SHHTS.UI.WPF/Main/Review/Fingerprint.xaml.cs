﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.ReadCard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Review
{
    /// <summary>
    /// Fingerprint.xaml 的交互逻辑
    /// </summary>
    public partial class Fingerprint : ReviewBaseWindow
    {  
        public Fingerprint(long reviewSysNo, string reviewName)
            : base(reviewSysNo, reviewName)
        {
            InitializeComponent();
            MessageBox.Show("请找以下人员指纹复核：" + reviewName, "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
            this.Activated += ShowFingerRead;
        }
        private void ValidateReviewRule(object sender, EventArgs e)
        {
            var fingerTemplate = ((FingerRead)sender).fingerTemplate;
            if (string.IsNullOrWhiteSpace(fingerTemplate))
            {
                this.Close();
                return;
            }


            var result = PinganStaffServiceProxy.GetStaffByFingerprint(fingerTemplate);
            if(!result.Success)
            {
                MessageBox.Show("复核失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            var staff = result.OtherData["UserInfo"] as User;
            this.ValidateReviewRule(staff.SysNo, ReviewMode.Fingerprint);
        }

        public void ShowFingerRead(object sender, EventArgs e)
        {
            ReBuild();
        }
        protected override void ReBuild()
        {
            this.Hide();
            FingerRead fingerReader = new FingerRead();
            fingerReader.Closed -= ValidateReviewRule;
            fingerReader.Closed += ValidateReviewRule;
            fingerReader.ShowDialog();
        }
    }
}
