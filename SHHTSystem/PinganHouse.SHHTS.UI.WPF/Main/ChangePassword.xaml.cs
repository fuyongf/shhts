﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Model;

namespace PinganHouse.SHHTS.UI.WPF.Main
{
    /// <summary>
    /// ChangePassword.xaml 的交互逻辑
    /// </summary>
    public partial class ChangePassword : Window
    {
        public ChangePassword()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 窗体加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Pb_OldPwd.Focus();
        }

        /// <summary>
        /// 确认按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnConfirm_Click(object sender, RoutedEventArgs e)
        {
            //旧密码
            string oldPwd = Pb_OldPwd.Password.ToString().Trim();
            //新密码
            string newPwd = Pb_NewPwd.Password.ToString().Trim();
            //确认密码
            string confirmPwd = Pb_ConfirmPwd.Password.ToString().Trim();

            #region 验证
            //旧密码验证
            if (string.IsNullOrEmpty(oldPwd))
            {
                MessageBox.Show("旧密码不能为空，请重新输入.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                Pb_OldPwd.Focus();
                return;
            }
            //新密码验证
            if (string.IsNullOrEmpty(newPwd))
            {
                MessageBox.Show("新密码不能为空，请重新输入.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                Pb_NewPwd.Focus();
                return;
            }
            //确认密码验证
            if (string.IsNullOrEmpty(confirmPwd))
            {
                MessageBox.Show("确认密码不能为空，请重新输入.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                Pb_ConfirmPwd.Focus();
                return;
            }
            if (confirmPwd != newPwd)
            {
                MessageBox.Show("确认密码与新密码不一致，请重新输入", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                Pb_ConfirmPwd.Focus();
                return;
            }
            #endregion


            OperationResult result = UserServiceProxy.ChangePassword(LoginHelper.CurrentUser.SysNo, oldPwd, newPwd);
            if (result.Success)
            {
                MessageBox.Show("修改密码成功！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();
                //Pb_OldPwd.Password = "";
                //Pb_NewPwd.Password = "";
                //Pb_ConfirmPwd.Password = "";
                //Pb_OldPwd.Focus();
                //return;
            }
            else
            {
                MessageBox.Show("修改密码失败！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                Pb_OldPwd.Focus();
                return;
            }
        }

        private void Pb_ConfirmPwd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                //旧密码
                string oldPwd = Pb_OldPwd.Password.ToString().Trim();
                //新密码
                string newPwd = Pb_NewPwd.Password.ToString().Trim();
                //确认密码
                string confirmPwd = Pb_ConfirmPwd.Password.ToString().Trim();

                #region 验证
                //旧密码验证
                if (string.IsNullOrEmpty(oldPwd))
                {
                    MessageBox.Show("旧密码不能为空，请重新输入.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    Pb_OldPwd.Focus();
                    return;
                }
                //新密码验证
                if (string.IsNullOrEmpty(newPwd))
                {
                    MessageBox.Show("新密码不能为空，请重新输入.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    Pb_NewPwd.Focus();
                    return;
                }
                //确认密码验证
                if (string.IsNullOrEmpty(confirmPwd))
                {
                    MessageBox.Show("确认密码不能为空，请重新输入.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    Pb_ConfirmPwd.Focus();
                    return;
                }
                if (confirmPwd != newPwd)
                {
                    MessageBox.Show("确认密码与新密码不一致，请重新输入", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    Pb_ConfirmPwd.Focus();
                    return;
                }
                #endregion


                OperationResult result = UserServiceProxy.ChangePassword(LoginHelper.CurrentUser.SysNo, oldPwd, newPwd);
                if (result.Success)
                {
                    MessageBox.Show("修改密码成功！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("修改密码失败！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    Pb_OldPwd.Focus();
                    return;
                }
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
