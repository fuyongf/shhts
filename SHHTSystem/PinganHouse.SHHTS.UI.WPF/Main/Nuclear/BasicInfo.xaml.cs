﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Nuclear
{
    /// <summary>
    /// BasicInfo.xaml 的交互逻辑
    /// </summary>
    public partial class BasicInfo : UserControl
    {
        public delegate void Handle(object sender, EventArgs e);
        public Handle NavClickEvent;

        /// <summary>
        /// 构造函数
        /// </summary>
        public BasicInfo()
        {
            InitializeComponent();
        }

        #region 取值赋值

        /// <summary>
        /// 显示赋值
        /// </summary>
        /// <param name="caseDto"></param>
        /// <param name="caseRemarkDto"></param>
        /// <param name="bankAccounts"></param>
        /// <param name="agentCompany"></param>
        /// <param name="agencyDto"></param>
        public void InitCtrlData(CaseDto caseDto, CaseRemark caseRemarkDto, IEnumerable<CustomerBankAccount> bankAccounts, AgentCompany agentCompany, AgentStaff agencyDto)
        {
            try
            {
                //案件基础信息
                if (caseDto != null)
                {
                    //产权证号
                    txt_CQZH.Text = caseDto.TenementContract;
                    //物业地址
                    txt_WYDZ.Text = caseDto.TenementAddress;
                    //资金托管合同
                    txt_ZJTG.Text= caseDto.FundTrusteeshipContract;
                    //佣金托管合同
                    txt_YJTG.Text = caseDto.CommissionTrusteeshipContract;
                }

                #region 案件基础信息
                //案件附属信息
                if (caseRemarkDto != null)
                {
                    //物业名称
                    txt_WYMC.Text = caseRemarkDto.TenementName;
                    if (caseRemarkDto.LoopLinePosition != null)
                    {
                        //环线位置
                        txt_HXWZ.Text = EnumHelper.GetEnumDesc((LinkLocation)Enum.Parse(typeof(LinkLocation), caseRemarkDto.LoopLinePosition));
                    }
                    //建筑面积
                    decimal coveredArea;
                    if (caseRemarkDto.CoveredArea != null)
                    {
                        decimal.TryParse(caseRemarkDto.CoveredArea.ToString(), out coveredArea);
                        txt_JZMJ.Text = string.Format("{0:####.#}", coveredArea) + "㎡";
                    }
                    //确定房型
                    var house = string.Empty;
                    if (!string.IsNullOrEmpty(Convert.ToString(caseRemarkDto.RoomCount)))
                    {
                        house += Convert.ToString(caseRemarkDto.RoomCount) + "房";
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(caseRemarkDto.HallCount)))
                    {
                        house += Convert.ToString(caseRemarkDto.HallCount) + "厅";
                    }
                    if (caseRemarkDto.ToiletCount>0)
                    {
                        house += Convert.ToString(caseRemarkDto.ToiletCount) + "卫";
                    }
                    if (!string.IsNullOrEmpty(house))
                    {
                        //房型
                        txt_FX.Text = house;
                    }
                    //层数
                    txt_CS.Text = Convert.ToString(caseRemarkDto.FloorCount);
                    //竣工日期
                    if (caseRemarkDto.BuiltCompletedDate != null)
                    {
                        txt_JGRQ.Text = ((DateTime)caseRemarkDto.BuiltCompletedDate).ToString("yyyy");
                    }
                    else
                    {
                        txt_JGRQ.Text = "无";
                    }
                    //居间价格
                    decimal mediatorPrice;
                    if (caseRemarkDto.MediatorPrice != null)
                    {
                        decimal.TryParse(caseRemarkDto.MediatorPrice.ToString(), out mediatorPrice);
                        txt_JJJG.Text = string.Format("{0:N0}", mediatorPrice);
                    }
                    //网签价格
                    decimal netlabelPrice;
                    if (caseRemarkDto.NetlabelPrice != null)
                    {
                        decimal.TryParse(caseRemarkDto.NetlabelPrice.ToString(), out netlabelPrice);
                        txt_WQJG.Text = string.Format("{0:N0}", netlabelPrice);
                    }
                    //实际成交价格
                    decimal realPrice;
                    if (caseRemarkDto.RealPrice != null)
                    {
                        decimal.TryParse(caseRemarkDto.RealPrice.ToString(), out realPrice);
                         txt_SJCJJG.Text  = string.Format("{0:N0}", realPrice);
                    }
                    //交易类型
                    txt_JYLX.Text = EnumHelper.GetEnumDesc(caseRemarkDto.TradeType);
                    //居住类型
                    cb_JZLX.Text = EnumHelper.GetEnumDesc(caseRemarkDto.ResideType);
                    //是否首次购买
                    if(caseRemarkDto.FirstTimeBuy!=null){
                        txt_BuyFirst.Text = Convert.ToBoolean(caseRemarkDto.FirstTimeBuy) ? "是" : "否";
                    }
                    //购入年限
                    if (caseRemarkDto.PurchaseYears != null)
                    {
                        txt_PurchaseYears.Text = Convert.ToString(caseRemarkDto.PurchaseYears)+"年";
                    }
                    //是否唯一住房
                    if (caseRemarkDto.OnlyHousing != null)
                    {
                        txt_BuyFirst.Text = Convert.ToBoolean(caseRemarkDto.OnlyHousing) ? "是" : "否";
                    }
                    //车位信息
                    txt_CWXX.Text = caseRemarkDto.CarportAddress;
                    //车位面积
                    if (caseRemarkDto.CarportArea != null)
                    {
                        decimal carportArea = Convert.ToDecimal(caseRemarkDto.CarportArea);
                        txt_CWXX.Text +=carportArea > 0 ? "/"+(int)carportArea+"平方米" : "";
                    }
                    //车位价格
                    if (caseRemarkDto.CarportPrice != null)
                    {
                        decimal carportPrice = Convert.ToDecimal(caseRemarkDto.CarportPrice);
                        txt_CWXX.Text += carportPrice > 0 ? "/" + "合同价" + (int)carportPrice : "元";
                    }
                    // //是否唯一住房
                    if(caseRemarkDto.OnlyHousing!=null){
                        txt_OnlyOne.Text = Convert.ToBoolean(caseRemarkDto.OnlyHousing) ? "是" : "否";
                    }
                    //是否补偿低价
                    if (caseRemarkDto.IsRepairLandPrice != null)
                    {
                        txt_BCDJ.Text = Convert.ToBoolean(caseRemarkDto.IsRepairLandPrice) ? "是" : "否";
                    }
                    //是否国安审批
                    if (caseRemarkDto.IsNSAApproval != null)
                    {
                        txt_GASP.Text = Convert.ToBoolean(caseRemarkDto.IsNSAApproval) ? "是" : "否";
                    }
                    //是否历史保护建筑
                    if (caseRemarkDto.IsNotarize != null)
                    {
                        txt_LSBH.Text = Convert.ToBoolean(caseRemarkDto.IsNotarize) ? "是" : "否";
                    }
                    //是否公证
                    if (caseRemarkDto.IsProtectiveBuilding != null)
                    {
                        txt_SFGZ.Text = Convert.ToBoolean(caseRemarkDto.IsProtectiveBuilding) ? "是" : "否";
                    }
                    //公证员信息
                    if (!string.IsNullOrEmpty(caseRemarkDto.Notary))
                    {
                        txt_GZYXX.Text = caseRemarkDto.Notary;
                        if (!string.IsNullOrEmpty(caseRemarkDto.NotaryContact))
                        {
                            txt_GZYXX.Text +="/" + caseRemarkDto.NotaryContact;
                        }
                    }
                }
                //经纪人信息
                if (agencyDto != null)
                {
                    txt_JJR.Text = agencyDto.RealName;
                    txt_SJ.Text = agencyDto.Mobile;
                    txt_SSMD.Text = agentCompany.CompanyName;
                }
                //经纪公司信息
                if (agentCompany != null)
                {
                    txt_ZJSKZH.Text =ConfigServiceProxy.GetBankNameByCode(agentCompany.ReceiveBank)+" "+agentCompany.ReceiveSubBank;
                    txt_ZJYHZH.Text =agentCompany.ReceiveName +" "+agentCompany.ReceiveAccount;
                }
                #endregion

                #region 银行卡信息绑定
                //银行卡信息
                if (bankAccounts != null)
                {
                    foreach (var bankAccount in bankAccounts)
                    {
                        switch (bankAccount.CustomerType)
                        {
                            case CustomerType.Buyer:
                                if (bankAccount.AccountType == BankAccountPurpose.Payment)
                                {
                                    switch (bankAccount.BizType)
                                    {
                                        case OrderBizType.HosingFund:
                                            txt_MJFKYH.Text = Utils.BankAccountFormat(bankAccount.AccountNo);
                                            txt_MJFKHM.Text = bankAccount.AccountName;
                                            if (string.IsNullOrEmpty(bankAccount.BankName))
                                            {
                                                txt_MJFK.Text = bankAccount.SubBankName;
                                            }
                                            else { txt_MJFK.Text = bankAccount.BankName + "|" + bankAccount.SubBankName; }
                                            break;
                                        case OrderBizType.AgencyFee:
                                            txt_YJ_MJFKYH.Text = Utils.BankAccountFormat(bankAccount.AccountNo);
                                            txt_YJ_MJFKHM.Text = bankAccount.AccountName;
                                            //判断银行
                                            if (string.IsNullOrEmpty(bankAccount.BankName)) {
                                                txt_YJ_MJFK.Text = bankAccount.SubBankName;
                                            }
                                            else
                                            {                                                
                                                txt_YJ_MJFK.Text = bankAccount.BankName + "|" + bankAccount.SubBankName;
                                            }
                                            break;
                                    }

                                }
                                break;
                            case CustomerType.Seller:
                                if (bankAccount.AccountType == BankAccountPurpose.Payment)
                                {
                                    switch (bankAccount.BizType)
                                    {
                                        case OrderBizType.HosingFund:
                                            txt_ZJTG_YZFKYH.Text = Utils.BankAccountFormat(bankAccount.AccountNo);
                                                txt_ZJTG_YZFKHM.Text = bankAccount.AccountName;
                                                if (string.IsNullOrEmpty(bankAccount.BankName))
                                                {
                                                    txt_ZJTG_YZFK.Text =  bankAccount.SubBankName;
                                                }
                                                else
                                                {
                                                    txt_ZJTG_YZFK.Text = bankAccount.BankName + "|" + bankAccount.SubBankName;
                                                }
                                               
                                            break;
                                        case OrderBizType.AgencyFee:
                                            txt_YZFKYH.Text = Utils.BankAccountFormat(bankAccount.AccountNo);
                                            txt_YZFKHM.Text = bankAccount.AccountName;
                                            if (string.IsNullOrEmpty(bankAccount.BankName))
                                            {
                                                txt_YZFK.Text =  bankAccount.SubBankName;
                                            }
                                            else
                                            {
                                                txt_YZFK.Text = bankAccount.BankName + "|" + bankAccount.SubBankName;
                                            }
                                            
                                            break;
                                    }
                                }
                                else if (bankAccount.AccountType == BankAccountPurpose.Receive)
                                {
                                    txt_YZSKYH.Text = Utils.BankAccountFormat(bankAccount.AccountNo);
                                    txt_YZSKHM.Text = bankAccount.AccountName;
                                    if (string.IsNullOrEmpty(bankAccount.BankName))
                                    {
                                        txt_YZSK.Text = bankAccount.SubBankName;
                                    }
                                    else
                                    {
                                        txt_YZSK.Text = bankAccount.BankName + "|" + bankAccount.SubBankName;
                                    }
                                }
                                break;
                        }
                    }
                }
                #endregion
            }
            catch(Exception e)
            {
                throw e;
            }
        }
        #endregion

        /// <summary>
        /// 点击按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (NavClickEvent != null)
            {
                NavClickEvent(sender,e);
            }
        }
    }
}
