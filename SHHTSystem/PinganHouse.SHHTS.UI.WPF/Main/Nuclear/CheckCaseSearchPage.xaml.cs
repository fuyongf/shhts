﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Main.Dialog;
using PinganHouse.SHHTS.UI.WPF.Main.Nuclear;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PinganHouse.SHHTS.UI.WPF.Main.PreCheck;

namespace PinganHouse.SHHTS.UI.WPF.Main.Nuclear
{
    /// <summary>
    /// CheckCaseSearchPage.xaml 的交互逻辑
    /// </summary>
    public partial class CheckCaseSearchPage : Page
    {
        /// <summary>
        /// 页面状态
        /// </summary>
        CaseStatus status = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.HA3
                | CaseStatus.HA4 | CaseStatus.HA5 | CaseStatus.HA6
                | CaseStatus.ZB2 | CaseStatus.ZB5 | CaseStatus.ZB6
                | CaseStatus.ZB7 | CaseStatus.ZB13;

        private int PageSize = 10;
        private Window window=null;
        private ScanBarCodeDialog scanBarCodeDialog = new ScanBarCodeDialog();    

        /// <summary>
        /// 默认构造
        /// </summary>
        public CheckCaseSearchPage()
        {
            InitializeComponent();
            this.GridPager.PageNumChanged = GridPager_PagerIndexChanged;
            //加载案件状态
            var list = EnumHelper.InitCaseStatusToCombobox(status);
            if (list==null)
            {
                MessageBox.Show("初始化失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            caseStatusCb.ItemsSource = list;
            caseStatusCb.DisplayMemberPath = "DisplayMember";
            caseStatusCb.SelectedValuePath = "ValueMember";
            caseStatusCb.SelectedValue = 0;
        }

        #region 页面数据取得方法
        /// <summary>
        /// 异步获取系统数据
        /// </summary>
        private async void GetDataToBindCtrl()
        {
            try
            {
                //案件编号
                var caseId = string.IsNullOrEmpty(this.txtCaseId.Text.Trim()) ? null : this.txtCaseId.Text.Trim();
                //案件地址
                var address = string.IsNullOrEmpty(this.txtAddress.Text.Trim()) ? null : this.txtAddress.Text.Trim();
                //卖家
                var buyerName = string.IsNullOrEmpty(this.txtBuyer.Text.Trim()) ? null : this.txtBuyer.Text.Trim();
                //经纪人
                var agencyName = string.IsNullOrEmpty(this.txtAgency.Text.Trim()) ? null : this.txtAgency.Text.Trim();
                //创建人
                var createrName = string.IsNullOrEmpty(this.txtCreater.Text.Trim()) ? null : this.txtCreater.Text.Trim();
                //开始时间
                DateTime startTime = DateTime.MinValue;
                if (!string.IsNullOrEmpty(dateStart.Text))
                {
                    DateTime.TryParse(dateStart.Text, out startTime);
                }
                //结束时间
                DateTime endTime = DateTime.MinValue;
                if (!string.IsNullOrEmpty(dateEnd.Text))
                {
                    DateTime.TryParse(dateEnd.Text, out endTime);
                }
                //案件状态选择
                var caseStatus = (CaseStatus)Enum.Parse(typeof(CaseStatus), Convert.ToString(caseStatusCb.SelectedValue));
                CaseStatus? selectStatus = caseStatus;
                if (caseStatus == CaseStatus.Unkown) { selectStatus = null; }
                var tupleModel = await GetDataGridData(caseId, address, buyerName, agencyName, startTime, endTime, selectStatus,createrName);
                this.dataGrid.DataContext = tupleModel.Item2;
                GridPager.Visibility = Visibility.Visible;
                if (tupleModel.Item1 % PageSize != 0)
                {
                    GridPager.PageCount = tupleModel.Item1 / PageSize + 1;
                }
                else { GridPager.PageCount = tupleModel.Item1 / PageSize; }
                this.loading.IsBusy = false;
            }
            catch (Exception)
            {
                this.loading.IsBusy = false;
                MessageBox.Show("加载数据发生系统异常.", "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        /// <summary>
        /// 获取页面数据
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="size"></param>
        /// <param name="caseId"></param>
        /// <param name="address"></param>
        /// <param name="buyerName"></param>
        /// <param name="agencyName"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        private Task<Tuple<int, List<CastModel>>> GetDataGridData(string caseId, string address, string buyerName, string agencyName,
            DateTime startTime, DateTime endTime, CaseStatus? selectStatus, string createrName)
        {
            return Task.Run(() =>
            {
                int cnt = 0;
                var caseModelList = new List<CastModel>();
                if (startTime == DateTime.MinValue && endTime == DateTime.MinValue)
                {
                    var caseList = CaseProxyService.GetInstanse().GetPaginatedList(GridPager.PageIndex, PageSize, out cnt
                        , ConfigHelper.GetCurrentReceptionCenter(), status, selectStatus
                        , caseId, address, buyerName, agencyName, operationStatus: CaseStatus.HA1, creatorName: createrName);
                    if (caseList != null)
                    {
                        caseModelList = CastModel.InitData(caseList, CaseStatus.HA1);
                    }
                }
                else if (startTime != DateTime.MinValue && endTime != DateTime.MinValue)
                {
                    var caseList = CaseProxyService.GetInstanse().GetPaginatedList(GridPager.PageIndex, PageSize, out cnt
                        , ConfigHelper.GetCurrentReceptionCenter(), status, selectStatus
                        , caseId, address, buyerName, agencyName, startTime, endTime, operationStatus: CaseStatus.HA1, creatorName: createrName);
                    if (caseList != null)
                    {
                        caseModelList = CastModel.InitData(caseList, CaseStatus.HA1);
                    }
                }
                else if (startTime != DateTime.MinValue && endTime == DateTime.MinValue)
                {
                    var caseList = CaseProxyService.GetInstanse().GetPaginatedList(GridPager.PageIndex, PageSize, out cnt
                        , ConfigHelper.GetCurrentReceptionCenter(), status, selectStatus
                        , caseId, address, buyerName, agencyName, startTime, operationStatus: CaseStatus.HA1, creatorName: createrName);
                    if (caseList != null)
                    {
                        caseModelList = CastModel.InitData(caseList,CaseStatus.HA1);
                    }
                }
                else if (startTime == DateTime.MinValue && endTime != DateTime.MinValue)
                {
                    var caseList = CaseProxyService.GetInstanse().GetPaginatedList(GridPager.PageIndex, PageSize, out cnt
                        , ConfigHelper.GetCurrentReceptionCenter(), status, selectStatus
                        , caseId, address, buyerName, agencyName, null, endTime, operationStatus: CaseStatus.HA1, creatorName: createrName);
                    if (caseList != null)
                    {
                        caseModelList = CastModel.InitData(caseList, CaseStatus.HA1);
                    }
                }
                var dicResult = new Tuple<int, List<CastModel>>(cnt, caseModelList);
                return dicResult;
            });
        }
        #endregion

        #region 页面按钮事件

        /// <summary>
        /// 新增案件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddCase_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, "Main/PreCheck/AddCasePage.xaml");
        }

        /// <summary>
        /// 查询搜索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchBtn_Click(object sender, RoutedEventArgs e)
        {
            GridPager.PageIndex = 1;
        }

        /// <summary>
        /// 添加附件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AttachmentBtn_Click(object sender, RoutedEventArgs e)
        {
            //案件编号
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            var attachments = new UploadAttachment(caseId);
            PageHelper.PageNavigateHelper(this, attachments);
        }

        /// <summary>
        /// 打印条码
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PrintCode_Click(object sender, RoutedEventArgs e)
        {
            //案件编号
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
        }

        /// <summary>
        /// 取消签约
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZB2_Click(object sender, RoutedEventArgs e)
        {
            //案件编号
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            var cancelContractDialog = new CancelContractDialog(caseId);
            bool? dialogResult=cancelContractDialog.ShowDialog();
            if (dialogResult.HasValue && dialogResult.Value)
            {
                var dic = new Dictionary<string, object>();
                dic.Add("Reason", cancelContractDialog.Reason);
                var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.ZB5, LoginHelper.CurrentUser.SysNo, dic);
                if (result.Success)
                {
                    MessageBox.Show("取消签约成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    GridPager_PagerIndexChanged(this.GridPager.PageIndex);
                }
                else
                {
                    MessageBox.Show("取消签约失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                }            
            }
        }

        /// <summary>
        /// 立即接单--签约中
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HA1_Click(object sender, RoutedEventArgs e)
        {
            //案件编号
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            var confrimReceiveCaseDialog = new ConfrimReceiveCaseDialog(caseId);
            bool? dialogResult = confrimReceiveCaseDialog.ShowDialog();
            if (dialogResult.HasValue && dialogResult.Value)
            {
                var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.HA1, LoginHelper.CurrentUser.SysNo);
                if (result.Success)
                {
                    MessageBox.Show("接单成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    GridPager_PagerIndexChanged(GridPager.PageIndex);
                }
                else
                {
                    MessageBox.Show("接单失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        /// <summary>
        /// 完成签约--已签约
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HA2_Click(object sender, RoutedEventArgs e)
        {
            //案件编号
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            var confirmContractDialog = new ConfirmContractDialog(caseId);
            confirmContractDialog.HA2Event+=confirmContractDialog_HA2Event;
            confirmContractDialog.ShowDialog();
        }

        /// <summary>
        /// 确认完成签约
        /// </summary>
        /// <param name="caseId">案件编号</param>
        /// <param name="isTrialTax">是否立即审税</param>
        /// <param name="isLoan">是否申请贷款</param>
        /// <param name="paras">贷款推送对象</param>
        private void confirmContractDialog_HA2Event(string caseId, bool isTrialTax, bool isLoan, Dictionary<string, object> paras,ConfirmContractDialog dialog)
        {
            try
            {
                var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.HA2, LoginHelper.CurrentUser.SysNo);
                if (result.Success)
                {
                    //是否审税
                    var trialTaxResult= ZB1ZB4_TrialTax(caseId, isTrialTax);
                    //申请贷款
                    var loanResult=DK1ZB9_Loan(caseId, isLoan, paras);
                    if (trialTaxResult.Success && loanResult.Success)
                    {
                        MessageBox.Show("确认完成签约,审税，申请贷款操作成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                        dialog.DialogResult = true;
                    }
                    else {
                        if (!trialTaxResult.Success && !loanResult.Success) {
                            MessageBox.Show("签约成功" + System.Environment.NewLine + "审税和申请贷款操作均失败：" + System.Environment.NewLine 
                                + "审税[" + trialTaxResult.ResultMessage + "]" + System.Environment.NewLine 
                                + "贷款[" + loanResult.ResultMessage + "]", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                            dialog.DialogResult = true;
                        }
                        else if (!trialTaxResult.Success) {
                            MessageBox.Show("审税操作失败：" + trialTaxResult.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                            dialog.DialogResult = true;                       
                        }
                        else if (!loanResult.Success) {
                            MessageBox.Show("申请贷款操作失败：" + loanResult.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                            dialog.DialogResult = true;                     
                        }
                    }
                }
                else {
                    MessageBox.Show("确认完成签约操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    dialog.DialogResult = false;
                }
                GridPager_PagerIndexChanged(this.GridPager.PageIndex);
            }
            catch (Exception ex) {
                MessageBox.Show("确认完成签约操作发生错误：" + ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                dialog.DialogResult = false;
            }
        }

        /// <summary>
        /// 是否贷款
        /// </summary>
        /// <param name="isLoan">是否贷款</param>
        /// <param name="paras"></param>
        private OperationResult DK1ZB9_Loan(string caseId, bool isLoan, Dictionary<string, object> paras)
        {
            try
            {
                var status = CaseStatus.ZB9;
                var result = new OperationResult(1,null);
                if (isLoan)
                {
                    status = CaseStatus.DK1;
                    result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, status, LoginHelper.CurrentUser.SysNo, paras);
                }
                else
                {
                    result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, status, LoginHelper.CurrentUser.SysNo);
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("申请贷款操作发生错误：" + ex.Message);
            }
        }

        /// <summary>
        /// 是否立即审税
        /// </summary>
        /// <param name="caseId">案件编号</param>
        /// <param name="isTrialTax">是否立即审税</param>
        private OperationResult ZB1ZB4_TrialTax(string caseId, bool isTrialTax)
        {
            try
            {
                var status= CaseStatus.ZB1;
                if(!isTrialTax){status= CaseStatus.ZB4;}
                var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, status, LoginHelper.CurrentUser.SysNo);
                return result;
            }
            catch (Exception ex) {
                throw new Exception("立即审税操作发生错误：" + ex.Message);
            }
        }

        /// <summary>
        /// 新建报告--报告填写中
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HA3_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //案件编号
                var caseId = Convert.ToString(((Button)sender).CommandParameter);
                var caseInfo = new CaseBasicInfo(caseId);
                PageHelper.PageNavigateHelper(this, caseInfo);               
            }
            catch (Exception ex)
            {
                MessageBox.Show("新建报告发生错误：" + ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 提交报告--立案报告已提交
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HA4_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //案件编号
                var caseId = Convert.ToString(((Button)sender).CommandParameter);
                var caseInfo = new CaseBasicInfo(caseId);
                PageHelper.PageNavigateHelper(this, caseInfo);
            }
            catch (Exception ex) {
                MessageBox.Show("提交报告发生错误：" + ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 审核报告
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VerifyReport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //案件编号
                var caseId = Convert.ToString(((Button)sender).CommandParameter);
                var approvalReportDialog = new ApprovalReportDialog(caseId);
                approvalReportDialog.HA5Event+=approvalReportDialog_HA5Event;
                approvalReportDialog.HA6Event +=approvalReportDialog_HA6Event;
                approvalReportDialog.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("审核报告发生错误：" + ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 报告审核不成功--提交报告？
        /// </summary>
        /// <param name="caseId">案件编号</param>
        /// <param name="reason">原因</param>
        private void approvalReportDialog_HA6Event(string caseId, string reason)
        {
            try
            {
                var dic = new Dictionary<string, object>();
                dic.Add("AuditReason", reason);
                dic.Add("CreateUser", LoginHelper.CurrentUser.RealName);
                dic.Add("CreateDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm"));
                var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.HA6, LoginHelper.CurrentUser.SysNo,dic);
                if (result.Success)
                {
                    MessageBox.Show("立案审批不通过操作成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    GridPager_PagerIndexChanged(this.GridPager.PageIndex);
                }
                else
                {
                    MessageBox.Show("立案审批不通过操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                }  
            }
            catch (Exception e) {
                MessageBox.Show("立案审批不通过操作发生错误：" + e.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);       
            }
        }

        /// <summary>
        /// 审核报告通过---立案结束
        /// </summary>
        /// <param name="caseId">案件编号</param>
        /// <param name="isMortgage">是否抵押</param>
        private void approvalReportDialog_HA5Event(string caseId, bool isMortgage)
        {
            try
            {
                var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.HA5, LoginHelper.CurrentUser.SysNo);
                if (result.Success)
                {
                    var mortgageResult = Mortgage_ZB3ZB10Event(caseId, isMortgage);
                    if (mortgageResult.Success)
                    {
                        MessageBox.Show("审核报告通过操作成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("审核报告通过操作成功"+System.Environment.NewLine
                            +"是否抵押还贷操作失败："+mortgageResult.ResultMessage, 
                            "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    }                    
                    GridPager_PagerIndexChanged(this.GridPager.PageIndex);
                }
                else
                {
                    MessageBox.Show("审核报告通过操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            catch (Exception e) {
                MessageBox.Show("审核报告通过操作发生错误：" + e.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);            
            }
        }

        /// <summary>
        /// 是否有抵押贷款  有抵押--发起还贷(HD1)/无抵押 ZB10
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="isMortgage"></param>
        private OperationResult Mortgage_ZB3ZB10Event(string caseId, bool isMortgage)
        {
            try
            {
                var status = CaseStatus.ZB10;
                if (isMortgage) { status = CaseStatus.ZB3; }
                var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, status, LoginHelper.CurrentUser.SysNo);
                return result;
            }
            catch (Exception ex) {
                return new OperationResult(1, "处理是否有抵押操作时候发生系统异常:"+ex.Message);
            }
        }
        #endregion

        /// <summary>
        /// 案件跳转填写明细
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RequestNavigate_Click(object sender, RoutedEventArgs e)
        {
            try {
                var link = e.Source as Hyperlink;
                if (null != link)
                {
                    var caseId = Convert.ToString(link.NavigateUri);
                    var caseInfo = new CaseDetailInfo(caseId);
                    PageHelper.PageNavigateHelper(this, caseInfo);
                }     
            }
            catch (Exception)
            {
                MessageBox.Show("显示明细发生系统异常.","系统异常",MessageBoxButton.OK,MessageBoxImage.Error);
            }
            e.Handled = true;
        }

        /// <summary>
        /// 分页插件事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridPager_PagerIndexChanged(int pageIndex)
        {
            PageHelper.PageActiveLoadingControl(this.loading, "正在加载数据,请稍后...");
            this.GetDataToBindCtrl();
        }

        /// <summary>
        /// 加载页面数据...
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Body_Loaded(object sender, RoutedEventArgs e)
        {
            GridPager.PageIndex = 1;
            Window window = Utils.GetParentWindow(this);
            this.window = window;
            window.KeyDown += this.onKeyDown;
        }

        private void Page_Body_Unloaded(object sender, RoutedEventArgs e)
        {
            if (window != null)
            {
                window.KeyDown -= this.onKeyDown;
            }
        }

        /// <summary>
        /// 双击行时间
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var item = this.dataGrid.CurrentItem;
                if (item != null)
                {
                    var caseId = ((CaseDto)item).CaseId;
                    var caseInfo = new CaseBasicInfo(caseId);
                    PageHelper.PageNavigateHelper(this, caseInfo);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("显示明细发生错误", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void onKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                string keyBar = new ScanBarCodeDialog().showScanBarCodeDialog(Utils.GetParentWindow(this));
                txtCaseId.Text = keyBar;
                SearchBtn_Click(null, null);
            }
        }

    }
}
