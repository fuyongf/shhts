﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Nuclear
{
    /// <summary>
    /// ApprovalReportDialog.xaml 的交互逻辑
    /// </summary>
    public partial class ApprovalReportDialog : Window
    {

        /// <summary>
        /// 页面状态 -- 核案过来的状态
        /// </summary>
        const CaseStatus m_Status = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.HA3
                | CaseStatus.HA4 | CaseStatus.HA5 | CaseStatus.HA6 | CaseStatus.ZB2;

        /// <summary>
        /// 审核通过---立案结束
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="isMortgage"></param>
        public delegate void HA5Handle(string caseId, bool isMortgage);
        public event HA5Handle HA5Event;

        /// <summary>
        /// 审核通过---立案结束
        /// </summary>
        /// <param name="x"></param>
        public delegate void HA6Handle(string caseId, string reason);
        public event HA6Handle HA6Event;

        /// <summary>
        /// 案件编号
        /// </summary>
        private string m_CaseId;

        /// <summary>
        /// 是否抵押
        /// </summary>
        private bool m_isMortgage;

        public ApprovalReportDialog(string caseId)
        {
            InitializeComponent();
            m_CaseId = caseId;
            ctrl_BasicInfo.NavClickEvent += NavButton_Click;
            ctrl_CustomerInfo.NavClickEvent += NavButton_Click;
            ctrl_TransactionInfo.m_CaseId = caseId;
            ctrl_TransactionInfo.NavClickEvent += NavButton_Click;
        }

        /// <summary>
        /// Tab切换
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavButton_Click(object sender, EventArgs e)
        {
            var lab = sender as Button;
            switch (lab.Tag.ToString())
            {
                case "1":
                    tab_BasicInfo.IsSelected = true;
                    break;
                case "2":
                    tab_CustomerInfo.IsSelected = true;
                    break;
                case "3":
                    tab_TransactionInfo.IsSelected = true;
                    break;
            }
        }
        
        /// <summary>
        /// 通过审批报告
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// 在此处添加事件处理程序实现。           
            if (HA5Event != null)
            {
                HA5Event(m_CaseId,m_isMortgage);
                this.DialogResult = true;
            }
        }

        /// <summary>
        /// 审批报告不通过
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancle_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// 在此处添加事件处理程序实现。
            if (string.IsNullOrEmpty(txt_Reason.Text.Trim()))
            {
                MessageBox.Show("请填写审批不通过原因.","系统提示",MessageBoxButton.OK,MessageBoxImage.Warning);
            }
            else {
                if (HA6Event != null)
                {
                    HA6Event(m_CaseId, txt_Reason.Text.Trim());
                    this.DialogResult = false;
                }
            }
        }


        /// <summary>
        /// 加载拒绝原因
        /// </summary>
        /// <param name="caseEventList"></param>
        private void LoadCaseReson(IList<CaseEvent> caseEventList)
        {
            try
            {
                if (caseEventList == null || caseEventList.Count == 0)
                    return;
                var reason = new StringBuilder();
                var cnt = caseEventList.Count;
                var i = 1;
                foreach (var eventData in caseEventList)
                {
                    var dic = eventData.OtherDatas;
                    if (!dic.ContainsKey("AuditReason")) continue;
                    reason.Append(i + ". 理由：");
                    reason.Append(dic["AuditReason"].ToString());
                    if (dic.ContainsKey("CreateUser")) {
                        reason.Append("  操作人：");
                        reason.Append(dic["CreateUser"].ToString());
                    }
                    if (dic.ContainsKey("CreateDate")) {
                        reason.Append("  操作时间：");
                        reason.Append(Convert.ToDateTime(dic["CreateDate"]).ToString("yyyy-MM-dd HH:mm"));                  
                    }
                    reason.Append(System.Environment.NewLine);
                    i++;
                }
                this.Dispatcher.Invoke(new Action(
                 delegate
                 {
                     auditReason.Text =Convert.ToString(reason);
                 }));
            }
            catch (Exception) { }
        }

        /// <summary>
        /// 格式化银行卡显示
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        private string BankAccountFormat(string txt)
        {
            try
            {
                txt = txt.Replace(" ", "");
                var size = txt.Length / 4;
                var newstr = new StringBuilder();
                for (int n = 0; n < size; n++)
                {
                    newstr.Append(txt.Substring(n * 4, 4));
                    newstr.Append(" ");
                }
                newstr.Append(txt.Substring(size * 4, txt.Length % 4));
                return newstr.ToString();
            }
            catch (Exception)
            {
                return txt;
            }
        }

  
        /// <summary>
        /// 页面加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void ApprovalReport_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                PageHelper.PageActiveLoadingControl(this.ctrlLoading, "加载数据,请稍候...");
                //案件对象
                var caseDto = await NuclearHelper.Instance.GetCaseDto(m_CaseId, m_Status);
                //案件附属对象
                var caseRemarkDto = await NuclearHelper.Instance.GetCaseRemarkDto(caseDto.SysNo);
                //中介信息
                var agencyDto = await NuclearHelper.Instance.GetAgentStaff(caseDto.AgencySysNo);
                //中介公司信息
                var agentCompany = await NuclearHelper.Instance.GetAgentCompany(caseDto.CompanySysNo);
                //银行卡信息
                var bankAccounts = await NuclearHelper.Instance.GetCustomerBankAccounts(caseDto.SysNo);
                //案件基础信息
                ctrl_BasicInfo.InitCtrlData(caseDto, caseRemarkDto, bankAccounts, agentCompany, agencyDto);
                //获取上下家信息
                var customers = await NuclearHelper.Instance.GetCustomers(m_CaseId);
                //上下家信息
                ctrl_CustomerInfo.InitCtrlData(customers.Item1, customers.Item2);
                //案件完成时间
                var dicCaseStatus = await NuclearHelper.Instance.GetCaseProcess(m_CaseId, m_Status);
                //计算时间
                ctrlProcessCtrl.BindingDataToProcess(dicCaseStatus);
                //获取案件审核拒绝条件
                var caseEventList = CaseProxyService.GetInstanse().GetCaseEvents(m_CaseId, CaseStatus.HA6, CaseStatus.HA4);
                //组合历史审核原因
                LoadCaseReson(caseEventList);
                //获取是否有抵押信息
                if (customers == null) return;
                var seller = customers.Item1;
                if (seller != null)
                {
                    m_isMortgage = Convert.ToBoolean(seller.IsGuaranty);
                }
            }
            catch (Exception ex) {
                MessageBox.Show("加载客户信息发生系统异常："+ex.Message,"系统提示",MessageBoxButton.OK,MessageBoxImage.Error);
            }
            finally
            {
                this.ctrlLoading.IsBusy = false;
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
