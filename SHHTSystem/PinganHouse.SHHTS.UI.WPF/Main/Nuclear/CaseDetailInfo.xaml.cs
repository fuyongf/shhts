﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Main.PreCheck;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Nuclear
{
    /// <summary>
    /// CaseDetailInfo.xaml 的交互逻辑
    /// </summary>
    public partial class CaseDetailInfo : Page
    {
        /// <summary>
        /// 案件编号
        /// </summary>
        private string m_CaseId = string.Empty;
        private Window window = null;

        /// <summary>
        /// 页面状态 -- 核案过来的状态
        /// </summary>
        const CaseStatus m_Status = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.HA3
                | CaseStatus.HA4 | CaseStatus.HA5 | CaseStatus.HA6 | CaseStatus.ZB2;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="caseId"></param>
        public CaseDetailInfo(string caseId)
        {
            InitializeComponent();
            m_CaseId = caseId;
            txt_CaseInfo.Text = "案件编号："+caseId;
            ctrl_BasicInfo.NavClickEvent += NavButton_Click;
            ctrl_CustomerInfo.NavClickEvent += NavButton_Click;
            ctrl_TransactionInfo.m_CaseId = caseId;
            ctrl_TransactionInfo.NavClickEvent += NavButton_Click;
            ctrl_Navigation.UploadEvent += ctrl_Navigation_UploadEvent;
            ctrl_Navigation.AttachmentEvent += ctrl_Navigation_AttachmentEvent;
            ctrl_Navigation.OperationEvent+=ctrl_Navigation_OperationEvent;
        }

        #region 自定义导航事件
        /// <summary>
        /// 基本信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ctrl_Navigation_OperationEvent(object sender, EventArgs e)
        {
            var status = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.ZB2;
            var attachments = new OperationInfos(m_CaseId, status);
            PageHelper.PageNavigateHelper(this, attachments);
        }
        /// <summary>
        /// 上传页面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ctrl_Navigation_UploadEvent(object sender, EventArgs e)
        {
            var attachments = new UploadAttachment(m_CaseId);
            PageHelper.PageNavigateHelper(this, attachments);
        }
        void ctrl_Navigation_AttachmentEvent(object sender, EventArgs e)
        {
            var attachments = new CaseAttachmentsInfo(m_CaseId);
            PageHelper.PageNavigateHelper(this, attachments);
        }
        #endregion

        /// <summary>
        /// 页面加载获取数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                PageHelper.PageActiveLoadingControl(this.ctrlLoading, "加载数据,请稍候...");
                //案件对象
                var caseDto =await NuclearHelper.Instance.GetCaseDto(m_CaseId, m_Status);
                //案件附属对象
                var caseRemarkDto = await NuclearHelper.Instance.GetCaseRemarkDto(caseDto.SysNo);
                //中介信息
                var agencyDto = await NuclearHelper.Instance.GetAgentStaff(caseDto.AgencySysNo);
                //中介公司信息
                var agentCompany = await NuclearHelper.Instance.GetAgentCompany(caseDto.CompanySysNo);
                //银行卡信息
                var bankAccounts = await NuclearHelper.Instance.GetCustomerBankAccounts(caseDto.SysNo);
                //案件基础信息
                ctrl_BasicInfo.InitCtrlData(caseDto, caseRemarkDto, bankAccounts, agentCompany, agencyDto);
                //获取上下家信息
                var customers = await NuclearHelper.Instance.GetCustomers(m_CaseId);
                //上下家信息
                ctrl_CustomerInfo.InitCtrlData(customers.Item1, customers.Item2);
                //案件完成时间
                var dicCaseStatus = await NuclearHelper.Instance.GetCaseProcess(m_CaseId, m_Status);
                //计算时间
                BindingDataToProcess(dicCaseStatus);
                //注册Key事件
                Window window = Utils.GetParentWindow(this);
                this.window = window;
                window.KeyDown += this.Page_KeyDown;

            }
            catch (Exception)
            {
                MessageBox.Show("获取案件数据发生系统异常.", "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                this.ctrlLoading.IsBusy = false;
            }
            
        }

        /// <summary>
        /// 计算时间
        /// </summary>
        /// <param name="dicCaseStatus"></param>
        private void BindingDataToProcess(Dictionary<CaseStatus, Tuple<string, DateTime>> dicCaseStatus)
        {
            try
            {
                //签约中
                DateTime? time_HA1=null;
                DateTime? time_HA2=null;
                DateTime? time_HA3 = null ;
                DateTime? time_HA4=null;
                DateTime? time_HA5=null;
                var backgroundColor = (Color) ColorConverter.ConvertFromString("#B5F05A22");
                var brush=new SolidColorBrush(backgroundColor);
                foreach (var item in dicCaseStatus)
                {
                    switch (item.Key)
                    {
                        case CaseStatus.HA1:
                            txt_HA1.Text = item.Value.Item2.ToString("yyyy.MM.dd HH:mm");
                            time_HA1 = item.Value.Item2;
                            break;
                        case CaseStatus.HA2:
                            txt_HA2.Text = item.Value.Item2.ToString("yyyy.MM.dd HH:mm");
                            time_HA2 = item.Value.Item2;
                            btn_HA2.Background = brush;
                            time_HA2 = item.Value.Item2;
                            CalculationTime(CaseStatus.HA1, CaseStatus.HA2, time_HA1, time_HA2);
                            break;
                        case CaseStatus.HA3:
                            txt_HA3.Text = item.Value.Item2.ToString("yyyy.MM.dd HH:mm");
                            time_HA3 = item.Value.Item2;
                            btn_HA3.Background = brush; 
                            time_HA3 = item.Value.Item2;
                            CalculationTime(CaseStatus.HA2, CaseStatus.HA3, time_HA2, time_HA3);
                            break;
                        case CaseStatus.HA4:
                            txt_HA4.Text = item.Value.Item2.ToString("yyyy.MM.dd HH:mm");
                            time_HA4 = item.Value.Item2;
                            btn_HA4.Background = brush; 
                            time_HA4 = item.Value.Item2;
                            CalculationTime(CaseStatus.HA3, CaseStatus.HA4, time_HA3, time_HA4);
                            break;
                        case CaseStatus.HA5:
                            txt_HA5.Text = item.Value.Item2.ToString("yyyy.MM.dd HH:mm");
                            time_HA5 = item.Value.Item2;
                            btn_HA5.Background = brush; 
                            time_HA5 = item.Value.Item2;
                            CalculationTime(CaseStatus.HA4, CaseStatus.HA5, time_HA4, time_HA5);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// 计算时间差
        /// </summary>
        private void CalculationTime(CaseStatus startStatus,CaseStatus endStatus,DateTime? startTime,DateTime? endTime)
        {
            try {
                var tspan = (TimeSpan)(endTime - startTime);
                var txt = tspan.Days.ToString() + "天" + tspan.Hours.ToString() + "时" +
                                        tspan.Minutes.ToString() + "分" + tspan.Seconds.ToString() + "秒";
                switch (endStatus)
                {
                    case CaseStatus.HA2:
                        txt_HA2_HA1.Text = txt;
                        image_HA2_HA1.Visibility = Visibility.Visible;
                        break;
                    case CaseStatus.HA3:
                        txt_HA3_HA2.Text = txt;
                        image_HA3_HA2.Visibility = Visibility.Visible;
                        break;
                    case CaseStatus.HA4:
                        txt_HA4_HA3.Text = txt;
                        image_HA4_HA3.Visibility = Visibility.Visible;
                        break;
                    case CaseStatus.HA5:
                        txt_HA5_HA4.Text = txt;
                        image_HA5_HA4.Visibility = Visibility.Visible;
                        break;
                }
            }
            catch (Exception) { }
        }

        /// <summary>
        /// 格式化银行卡显示
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        private string BankAccountFormat(string txt)
        {
            try
            {
                if (txt == null) return txt;
                txt = txt.Replace(" ", "");
                var size = txt.Length / 4;
                var newstr = new StringBuilder();
                for (int n = 0; n < size; n++)
                {
                    newstr.Append(txt.Substring(n * 4, 4));
                    newstr.Append(" ");
                }
                newstr.Append(txt.Substring(size * 4, txt.Length % 4));
                return newstr.ToString();
            }
            catch (Exception)
            {
                return txt;
            }
        }

        /// <summary>
        /// 切换Tab选项目卡
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var lab = sender as Button;
            switch (lab.Tag.ToString())
            {
                case "1":
                    tab_BasicInfo.IsSelected = true;
                    break;
                case "2":
                    tab_CustomerInfo.IsSelected = true;
                    break;
                case "3":
                    tab_TransactionInfo.IsSelected = true;
                    break;
            }
        }

        private void NavButton_Click(object sender, EventArgs e)
        {
            Button_Click(sender,null);
        }

        /// <summary>
        /// 捕获ESC
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_KeyDown(object sender, KeyEventArgs e)
        {
            try {
                if (e.Key == Key.Escape)
                {
                    PageHelper.PageNavigateHelper(this, new CheckCaseSearchPage());
                }
            }
            catch (Exception) { }
        }
  
    }
}
