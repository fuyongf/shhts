﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Model;

namespace PinganHouse.SHHTS.UI.WPF.Main.Nuclear
{
    /// <summary>
    /// FundFlowDiaolg.xaml 的交互逻辑
    /// </summary>
    public partial class FundFlowDiaolg : Window
    {
        public delegate void SaveHandle(CustomerFundFlow flow);

        public event SaveHandle SaveEvent;

        private CustomerType _customerType;
        private String _caseId;
        private decimal _accountBalance = 0;
        private ChangeFundType _fundType;
        private IDictionary<string, IList<string>> _fundCondition;    //出(入款)款条件
        private IDictionary<string, IList<string>> _fundCondition1;
        private IDictionary<string, IList<string>> _fundCondition2;

        public FundFlowDiaolg(CustomerType customerType, String caseId, ChangeFundType fundType)
        {
            this.InitializeComponent();

            _customerType = customerType;
            _caseId = caseId;
            _fundType = fundType;
            _fundCondition = ConfigServiceProxy.GetChangeFundCondition(fundType);

            if (fundType == ChangeFundType.Collection)
            {
                if (customerType == CustomerType.Seller)
                {
                    this.Title = "添加卖方收款项";
                    lblDeposit.IsEnabled = false;
                    txtDeposit.IsEnabled = false;
                }
                else if (customerType == CustomerType.Buyer)
                {
                    this.Title = "添加买方收款项";
                }
            }
            else if (fundType == ChangeFundType.Payment)
            {
                if (customerType == CustomerType.Seller)
                {
                    this.Title = "添加卖方出款项";
                    lblDeposit.IsEnabled = false;
                    txtDeposit.IsEnabled = false;
                }
                else if (customerType == CustomerType.Buyer)
                {
                    this.Title = "添加买方出款项";
                }
            }

            var _customerFundFlowList = CustomerFundFlowServiceProxy.GetFlows(_caseId);


            foreach (var customerFundFlow in _customerFundFlowList.Where(customerFundFlow => customerFundFlow.CustomerType == customerType).OrderByDescending(customerFundFlow => customerFundFlow.CreateDate))
            {
                _accountBalance += customerFundFlow.HouseFund;
                _accountBalance += customerFundFlow.Taxes;
                _accountBalance += customerFundFlow.Brokerage;
                _accountBalance += customerFundFlow.DecorateCompensation;
                _accountBalance += customerFundFlow.OtherCharges;
            }

            cbConditionName1.DataContext = _fundCondition;
        }

        private void BtnEnter_Click(object sender, RoutedEventArgs e)
        {
            var flow = GetFormData();
            if (flow != null)
            {
                var result = CustomerFundFlowServiceProxy.Create(flow);
                if (result.Success)
                {
                    SaveEvent(flow);
                    this.Close();
                }
                else
                {
                    MessageBox.Show(result.ResultMessage);
                }
            }

        }


        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            if (SaveEvent != null)
            {
                var flow = GetFormData();
                if (flow != null)
                {
                    var result = CustomerFundFlowServiceProxy.Create(flow);
                    if (result.Success)
                    {
                        txtHouseFund.Text = "";
                        txtTaxes.Text = "";
                        txtBrokerage.Text = "";
                        txtDecorateCompensation.Text = "";
                        txtOtherCharges.Text = "";
                        lblAmount.Content = "";
                        txtDeposit.Text = "";
                        rdoTrusteeShip.IsChecked = false;
                        rdoLoan.IsChecked = false;
                        rdoSelfPay.IsChecked = false;

                    }
                    else
                    {
                        MessageBox.Show(result.ResultMessage);
                    }
                    SaveEvent(flow);
                }
            }
        }


        private CustomerFundFlow GetFormData()
        {
            #region 输入数据格式验证

            String errorMessage = String.Empty;

            Decimal houseFund = 0;
            if (!String.IsNullOrEmpty(txtHouseFund.Text))
            {
                if (!Decimal.TryParse(txtHouseFund.Text.Trim(), out houseFund) || houseFund < 0)
                {
                    errorMessage += "房款必须是大于0的数字\r\n";
                    houseFund = 0;
                }
            }


            Decimal taxes = 0;
            if (!String.IsNullOrEmpty(txtTaxes.Text))
            {
                if (!Decimal.TryParse(txtTaxes.Text.Trim(), out taxes) || taxes < 0)
                {
                    errorMessage += "税费必须是大于0的数字\r\n";
                    taxes = 0;
                }
            }

            Decimal brokerage = 0;
            if (!String.IsNullOrEmpty(txtBrokerage.Text))
            {
                if (!Decimal.TryParse(txtBrokerage.Text.Trim(), out brokerage) || brokerage < 0)
                {
                    errorMessage += "中介佣金必须是大于0的数字\r\n";
                    brokerage = 0;
                }
            }

            Decimal decorateCompensation = 0;
            if (!String.IsNullOrEmpty(txtDecorateCompensation.Text))
            {
                if (!Decimal.TryParse(txtDecorateCompensation.Text.Trim(), out decorateCompensation) || decorateCompensation < 0)
                {
                    errorMessage += "装修补偿必须是大于0的数字\r\n";
                    decorateCompensation = 0;

                }
            }

            Decimal deposit = 0;
            if (_customerType == CustomerType.Buyer)
            {
                if (!String.IsNullOrEmpty(txtDeposit.Text))
                {
                    if (!Decimal.TryParse(txtDeposit.Text.Trim(), out deposit) || deposit < 0)
                    {
                        errorMessage += "定金必须是大于0的数字\r\n";
                        deposit = 0;

                    }
                }
            }

            Decimal otherCharges = 0;
            if (!String.IsNullOrEmpty(txtOtherCharges.Text))
            {
                if (!Decimal.TryParse(txtOtherCharges.Text.Trim(), out otherCharges) || otherCharges < 0)
                {
                    errorMessage += "其他费用必须是大于0的数字\r\n";
                    otherCharges = 0;
                }
            }

            if (houseFund == 0 && taxes == 0 && brokerage == 0 && decorateCompensation == 0 && otherCharges == 0 && deposit == 0)
            {
                errorMessage += "所有数据不能为空并且不能都是0\r\n";
            }

            if (rdoTrusteeShip.IsChecked == false && rdoLoan.IsChecked == false && rdoSelfPay.IsChecked == false)
            {
                errorMessage += "请选择支付方式\r\n";
            }

            int errorCondition = 0;
            var regexString = "^[0-9]*[1-9][0-9]*$";
            if (txtDays1.Visibility == Visibility.Visible)
            {
                if (!Regex.IsMatch(txtDays1.Text.Trim(), regexString))
                {
                    errorCondition += 1;
                }
            }
            if (datePicker1.Visibility == Visibility.Visible)
            {
                if (datePicker1.Text.Trim() == "")
                {
                    errorCondition += 1;
                }
            }
            if (txtDays2.Visibility == Visibility.Visible)
            {
                if (!Regex.IsMatch(txtDays2.Text.Trim(), regexString))
                {
                    errorCondition += 1;
                }
            }
            if (datePicker2.Visibility == Visibility.Visible)
            {
                if (datePicker2.Text.Trim() == "")
                {
                    errorCondition += 1;
                }
            }

            if (spSysNo1.Visibility == Visibility.Visible)
            {
                if (!Regex.IsMatch(txtSysNo1.Text.Trim(), regexString))
                {
                    errorCondition += 1;
                }
            }

            if (spSysNo2.Visibility == Visibility.Visible)
            {
                if (!Regex.IsMatch(txtSysNo2.Text.Trim(), regexString))
                {
                    errorCondition += 1;
                }
            }

            if (errorCondition > 0)
            {
                errorMessage += "条件不完整\r\n";
            }

            if (!String.IsNullOrEmpty(errorMessage))
            {
                MessageBox.Show(errorMessage);
            }

            #endregion

            if (String.IsNullOrEmpty(errorMessage))
            {
                var conditions = new StringBuilder();
                conditions.Append(cbConditionName1.SelectedValue);
                if (spSysNo1.Visibility == Visibility.Visible)
                {
                    conditions.Append(txtSysNo1.Text.Trim() + "的款项");
                }

                conditions.Append(" ");
                if (txtDays1.Visibility == Visibility.Visible)
                {
                    conditions.Append(txtDays1.Text + cbConditionValue1.SelectedValue);
                }
                else if (datePicker1.Visibility == Visibility.Visible)
                {
                    conditions.Append(datePicker1.Text.Trim());
                    conditions.Append(" " + cbConditionValue1.SelectedValue);

                }
                else
                {
                    conditions.Append(cbConditionValue1.SelectedValue);
                }




                if (stkPanelConditions2.Visibility == Visibility.Visible)
                {
                    conditions.Append(" 且 ");
                    conditions.Append(cbConditionName2.SelectedValue);
                    if (spSysNo1.Visibility == Visibility.Visible)
                    {
                        conditions.Append(txtSysNo2.Text.Trim() + "的款项");
                    }
                    conditions.Append(" ");
                    //conditions.Append(cbConditionValue2.SelectedValue);
                    if (txtDays2.Visibility == Visibility.Visible)
                    {
                        conditions.Append(txtDays2.Text + cbConditionValue2.SelectedValue);
                    }
                    else if (datePicker2.Visibility == Visibility.Visible)
                    {
                        conditions.Append(datePicker2.Text.Trim());
                        conditions.Append(" " + cbConditionValue2.SelectedValue);

                    }
                    else
                    {
                        conditions.Append(cbConditionValue2.SelectedValue);
                    }
                }

                if (_fundType == ChangeFundType.Payment)
                {
                    houseFund = houseFund * -1;
                    taxes = taxes * -1;
                    brokerage = brokerage * -1;
                    decorateCompensation = decorateCompensation * -1;
                    otherCharges = otherCharges * -1;
                    deposit = deposit * -1;
                }



                var flow = new CustomerFundFlow();
                flow.CaseId = _caseId;
                flow.CustomerType = _customerType;
                flow.ConditionType = conditions.ToString();
                flow.HouseFund = houseFund;
                flow.Taxes = taxes;
                flow.Brokerage = brokerage;
                flow.Earnest = deposit;
                flow.DecorateCompensation = decorateCompensation;
                flow.OtherCharges = otherCharges;
                flow.Amount = houseFund + taxes + brokerage + decorateCompensation + otherCharges + deposit;
                flow.AccountBalance = _accountBalance + flow.Amount;
                flow.CreateDate = DateTime.Now;
                flow.CreateUserSysNo = LoginHelper.CurrentUser.SysNo;

                if (rdoTrusteeShip.IsChecked == true)
                {
                    flow.PaymentChannel = (FundPlanPayChannel)int.Parse(rdoTrusteeShip.Tag.ToString());
                }
                else if (rdoLoan.IsChecked == true)
                {
                    flow.PaymentChannel = (FundPlanPayChannel)int.Parse(rdoLoan.Tag.ToString());
                }
                else if (rdoSelfPay.IsChecked == true)
                {
                    flow.PaymentChannel = (FundPlanPayChannel)int.Parse(rdoSelfPay.Tag.ToString());
                }

                return flow;
            }
            else
            {
                return null;
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();

        }

        private void btnAddConditions_Click(object sender, RoutedEventArgs e)
        {
            if (stkPanelConditions2.Visibility == Visibility.Collapsed)
            {
                stkPanelConditions2.Visibility = Visibility.Visible;

                _fundCondition1 = new Dictionary<string, IList<string>>(_fundCondition);
                if (cbConditionName2.SelectedValue != null)
                    _fundCondition1.Remove(cbConditionName2.SelectedValue.ToString());
                cbConditionName1.DataContext = _fundCondition1;

            }
            else if (stkPanelConditions2.Visibility == Visibility.Visible)
            {
                stkPanelConditions2.Visibility = Visibility.Collapsed;
                cbConditionName1.DataContext = _fundCondition;
            }
        }

        private void txtHouseFund_TextChanged(object sender, TextChangedEventArgs e)
        {
            Decimal amount = CalculateAmount();
            lblAmount.Content = amount.ToString("C");
        }

        private void txtTaxes_TextChanged(object sender, TextChangedEventArgs e)
        {
            Decimal amount = CalculateAmount();
            lblAmount.Content = amount.ToString("C");
        }

        private void txtBrokerage_TextChanged(object sender, TextChangedEventArgs e)
        {
            Decimal amount = CalculateAmount();
            lblAmount.Content = amount.ToString("C");
        }

        private void txtDeposit_TextChanged(object sender, TextChangedEventArgs e)
        {
            Decimal amount = CalculateAmount();
            lblAmount.Content = amount.ToString("C");
        }
        private void txtDecorateCompensation_TextChanged(object sender, TextChangedEventArgs e)
        {
            Decimal amount = CalculateAmount();
            lblAmount.Content = amount.ToString("C");
        }

        private void txtOtherCharges_TextChanged(object sender, TextChangedEventArgs e)
        {
            Decimal amount = CalculateAmount();
            lblAmount.Content = amount.ToString("C");
        }

        /// <summary>
        /// 计算本次合计
        /// </summary>
        private Decimal CalculateAmount()
        {

            Decimal houseFund = 0;
            if (!String.IsNullOrEmpty(txtHouseFund.Text))
                Decimal.TryParse(txtHouseFund.Text.Trim(), out houseFund);


            Decimal taxes = 0;
            if (!String.IsNullOrEmpty(txtTaxes.Text))
                Decimal.TryParse(txtTaxes.Text.Trim(), out taxes);

            Decimal brokerage = 0;
            if (!String.IsNullOrEmpty(txtBrokerage.Text))
                Decimal.TryParse(txtBrokerage.Text.Trim(), out brokerage);

            Decimal decorateCompensation = 0;
            if (!String.IsNullOrEmpty(txtDecorateCompensation.Text))
                Decimal.TryParse(txtDecorateCompensation.Text.Trim(), out decorateCompensation);

            Decimal deposit = 0;
            if (!String.IsNullOrEmpty(txtDeposit.Text))
                Decimal.TryParse(txtDeposit.Text.Trim(), out deposit);

            Decimal otherCharges = 0;
            if (!String.IsNullOrEmpty(txtOtherCharges.Text))
                Decimal.TryParse(txtOtherCharges.Text.Trim(), out otherCharges);

            return houseFund + taxes + brokerage + decorateCompensation + otherCharges + deposit;
        }

        private Decimal CalculateAccountBalance(decimal amount)
        {
            if (_fundType == ChangeFundType.Payment)
            {
                return _accountBalance + amount * -1;
            }
            return _accountBalance + amount;
        }


        private void cbConditionName1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _fundCondition2 = new Dictionary<string, IList<string>>(_fundCondition);
            var conditionName = (sender as ComboBox).SelectedValue.ToString();
            foreach (var f in _fundCondition)
            {
                if (f.Key == conditionName)
                {
                    cbConditionValue1.DataContext = f.Value;
                    cbConditionValue1.SelectedIndex = 0;
                    if (f.Key == "指定日期")
                    {
                        datePicker1.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        datePicker1.Visibility = Visibility.Collapsed;
                    }

                    if (f.Key == "收到编号为")
                    {
                        spSysNo1.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        spSysNo1.Visibility = Visibility.Collapsed;
                    }
                }

            }


            _fundCondition2.Remove(conditionName);

            cbConditionName2.DataContext = _fundCondition2;

        }

        private void cbConditionName2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _fundCondition1 = new Dictionary<string, IList<string>>(_fundCondition);

            if ((sender as ComboBox).SelectedValue != null)
            {
                var conditionName = (sender as ComboBox).SelectedValue.ToString();
                foreach (var f in _fundCondition)
                {
                    if (f.Key == conditionName)
                    {
                        cbConditionValue2.DataContext = f.Value;
                        cbConditionValue2.SelectedIndex = 0;
                        if (f.Key == "指定日期")
                        {
                            datePicker2.Visibility = Visibility.Visible;
                        }
                        else
                        {
                            datePicker2.Visibility = Visibility.Collapsed;
                        }

                        if (f.Key == "收到编号为")
                        {
                            spSysNo2.Visibility = Visibility.Visible;
                        }
                        else
                        {
                            spSysNo2.Visibility = Visibility.Collapsed;
                        }
                    }
                }

                if (stkPanelConditions2.Visibility == Visibility.Visible)
                {
                    _fundCondition1.Remove(conditionName);
                    cbConditionName1.DataContext = _fundCondition1;
                }
            }

        }


        private void CbConditionValue1_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var cmbBox = (sender as ComboBox);
            if ((sender as ComboBox).SelectedValue != null)
            {
                if (cmbBox.SelectedValue.ToString() != "当日" && cmbBox.SelectedValue.ToString() != "之前")
                {
                    txtDays1.Visibility = Visibility.Visible;
                    txtDays1.Text = "";
                    datePicker1.Visibility = Visibility.Collapsed;
                }
                else
                {
                    txtDays1.Visibility = Visibility.Collapsed;
                    txtDays1.Text = "";
                    datePicker1.Visibility = Visibility.Collapsed;
                }
            }
        }


        private void CbConditionValue2_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var cmbBox = (sender as ComboBox);
            if ((sender as ComboBox).SelectedValue != null)
            {
                if (cmbBox.SelectedValue.ToString() != "当日" && cmbBox.SelectedValue.ToString() != "之前")
                {
                    txtDays2.Visibility = Visibility.Visible;
                    txtDays2.Text = "";
                    datePicker2.Visibility = Visibility.Collapsed;
                }
                else
                {
                    txtDays2.Visibility = Visibility.Collapsed;
                    txtDays2.Text = "";
                    datePicker2.Visibility = Visibility.Collapsed;
                }
            }
        }
    }
}