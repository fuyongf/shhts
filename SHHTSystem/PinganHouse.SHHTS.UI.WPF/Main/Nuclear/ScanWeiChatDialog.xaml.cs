﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;

namespace PinganHouse.SHHTS.UI.WPF.Main.Nuclear
{
    /// <summary>
    /// ScanWeiChatDialog.xaml 的交互逻辑
    /// </summary>
    public partial class ScanWeiChatDialog : Window
    {
        private long userSysNo;
        public ScanWeiChatDialog()
        {
            InitializeComponent();
        }

        public ScanWeiChatDialog(string userSysNo) : this()
        {
            this.userSysNo = long.Parse(userSysNo);
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
            
        }

        private async void ScanWeiChatDialog_OnLoaded(object sender, RoutedEventArgs e)
        {
            try
            {
                PageHelper.PageActiveLoadingControl(this.loading, "正在生成，请稍候...");
                string url = await GetQrCodeUrl(userSysNo);
                if (string.IsNullOrEmpty(url))
                {
                    throw new Exception("未正确获取微信生成链接，无法生成.");
                }
                image.Source = new BitmapImage(new Uri(url));
            }
            catch (Exception ex)
            {
                MessageBox.Show("系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                loading.IsBusy = false;
            }

        }

        /// <summary>
        /// 返回二维码
        /// </summary>
        /// <returns></returns>
        private Task<string> GetQrCodeUrl(long sysNo)
        {
            return Task.Run(() =>
            {
                var url = ParterProxyService.GetInstanse().GetQrCodeUrl(sysNo);
                return url;
            });
        }
    }
}
