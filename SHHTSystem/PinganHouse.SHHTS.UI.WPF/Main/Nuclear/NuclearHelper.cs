﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.UI.WPF.Main.Nuclear
{
    public class NuclearHelper
    {
        private static NuclearHelper _instance = null;
        // Creates an syn object.
        private static readonly object SynObject = new object();
        NuclearHelper() { }
        public static NuclearHelper Instance
        {
            get
            {
                // Double-Checked Locking
                if (null == _instance)
                {
                    lock (SynObject)
                    {
                        if (null == _instance)
                        {
                            _instance = new NuclearHelper();
                        }
                    }
                }
                return _instance;
            }
        }

        /// <summary>
        /// 获取案件实体
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public Task<CaseDto> GetCaseDto(string caseId, CaseStatus status)
        {
            CaseDto caseDto = null;
            return Task.Run(() =>
            {
                caseDto = CaseProxyService.GetInstanse().GetCaseByCaseIdAndCaseStatus(caseId, status);
                return caseDto;
            });
        }

        /// <summary>
        /// 获取案件附属信息
        /// </summary>
        /// <param name="agencySysNo"></param>
        /// <returns></returns>
        public Task<CaseRemark> GetCaseRemarkDto(long agencySysNo)
        {
            CaseRemark caseRemarkDto = null;
            return Task.Run(() =>
            {
                caseRemarkDto = CaseProxyService.GetInstanse().GetRemarkBySysNo(agencySysNo);
                return caseRemarkDto;
            });
        }

        /// <summary>
        /// 获取中介信息
        /// </summary>
        /// <param name="agencySysNo"></param>
        /// <returns></returns>
        public Task<AgentStaff> GetAgentStaff(long agencySysNo)
        {
            AgentStaff agencyDto = null;
            return Task.Run(() =>
            {
                agencyDto = AgentServiceProxy.GetAgentSatff(agencySysNo);
                return agencyDto;
            });
        }

        /// <summary>
        /// 获取中介信息
        /// </summary>
        /// <param name="companySysNo"></param>
        /// <returns></returns>
        public Task<AgentCompany> GetAgentCompany(long companySysNo)
        {
            AgentCompany agentCompany = null;
            return Task.Run(() =>
            {
                agentCompany = AgentServiceProxy.GetAgentCompany(companySysNo);
                return agentCompany;
            });
        }

        /// <summary>
        /// 获取银行卡信息信息
        /// </summary>
        /// <param name="caseSysNo"></param>
        /// <returns></returns>
        public Task<IEnumerable<CustomerBankAccount>> GetCustomerBankAccounts(long caseSysNo)
        {
            IEnumerable<CustomerBankAccount> customerBankAccount = null;
            return Task.Run(() =>
            {
                customerBankAccount = CaseProxyService.GetInstanse().GetCustomerBankAccounts(caseSysNo);
                return customerBankAccount;
            });

        }

        /// <summary>
        /// 获取客户数据
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public Task<Tuple<Seller, Buyer>> GetCustomers(string caseId)
        {
            Tuple<Seller, Buyer>  customers = null;
            return Task.Run(() =>
            {
                customers = CaseProxyService.GetInstanse().GetCaseCustomers(caseId);
                return customers;
            });

        }

        /// <summary>
        /// 获取案件进程时间
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public Task<Dictionary<CaseStatus, Tuple<string, DateTime>>> GetCaseProcess(string caseId,CaseStatus status)
        {
            return Task.Run(() =>
            {
                var dicCaseStatus = new Dictionary<CaseStatus, Tuple<string, DateTime>>();
                var caseProcessDto = CaseProxyService.GetInstanse().GetCaseAndProcess(caseId, status, ref dicCaseStatus);
                return dicCaseStatus;
            });

        }
    }
}
