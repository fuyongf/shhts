﻿using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Nuclear
{
    /// <summary>
    /// ConfrimReceiveCaseDialog.xaml 的交互逻辑
    /// </summary>
    public partial class ConfrimReceiveCaseDialog : Window
    {
        public ConfrimReceiveCaseDialog(string caseId)
        {
            InitializeComponent();
            var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(caseId);
            if (caseDto != null) {
                lab_caseId.Content = caseDto.CaseId;
                lab_cqzh.Content = caseDto.TenementContract;
            }
        }

        /// <summary>
        /// 取消立即接单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancle_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        /// <summary>
        /// 确认立即接单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
