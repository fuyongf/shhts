﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.RemoteServiceProxy.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Nuclear
{
    /// <summary>
    /// TransactionInfo.xaml 的交互逻辑
    /// </summary>
    public partial class TransactionInfo : UserControl
    {
        public delegate void Handle(object sender, EventArgs e);

        /// <summary>
        /// 数据加载完成
        /// </summary>
        public Handle LoadDataCompletedEvent;
        /// <summary>
        /// 导航点击事件
        /// </summary>
        public Handle NavClickEvent;
        /// <summary>
        /// 案件编号
        /// </summary>
        public string m_CaseId { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        public TransactionInfo()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 获取控件所需数据
        /// </summary>
        private void InitCtrlData()
        {
            try
            {
                //获取客户信息
                var customerFundFlowList = CustomerFundFlowServiceProxy.GetFlows(m_CaseId);
                if (customerFundFlowList == null) return;
                //绑定数据
                BindingDataToControls(customerFundFlowList);
            }
            catch (Exception) { }
        }

        /// <summary>
        /// 画面数据绑定
        /// </summary>
        /// <param name="customerFundFlowList"></param>
        private void BindingDataToControls(IList<CustomerFundFlow> customerFundFlowList)
        {
            try
            {
                BindSellerGrid(customerFundFlowList);
                BindBuyerGrid(customerFundFlowList);
                SetAmount(customerFundFlowList);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 加载买方数据
        /// </summary>
        /// <param name="fundFlowList"></param>
        private void BindBuyerGrid(IList<CustomerFundFlow> fundFlowList)
        {
            var flowList = fundFlowList.Where(customerFundFlow => customerFundFlow.CustomerType == CustomerType.Buyer)
                   .OrderByDescending(customerFundFlow => customerFundFlow.CreateDate).ToList();

            #region 计算买方合计列
            decimal houseFundTotal_Buyer = 0;
            decimal taxesTotal_Buyer = 0;
            decimal brokerageTotal_Buyer = 0;
            decimal decorateCompensationTotal_Buyer = 0;
            decimal otherChargesTotal_Buyer = 0;
            decimal amountTotal_Buyer = 0;
            decimal accountBalanceTotal_Buyer = 0;

            foreach (CustomerFundFlow flow in flowList)
            {
                houseFundTotal_Buyer += flow.HouseFund;
                taxesTotal_Buyer += flow.Taxes;
                brokerageTotal_Buyer += flow.Brokerage;
                decorateCompensationTotal_Buyer += flow.DecorateCompensation;
                otherChargesTotal_Buyer += flow.OtherCharges;
                amountTotal_Buyer += flow.Amount;
            }
            CustomerFundFlow totalRow_Buyer = new CustomerFundFlow();
            totalRow_Buyer.ConditionType = "合计";
            totalRow_Buyer.HouseFund = houseFundTotal_Buyer;
            totalRow_Buyer.Taxes = taxesTotal_Buyer;
            totalRow_Buyer.Brokerage = brokerageTotal_Buyer;
            totalRow_Buyer.DecorateCompensation = decorateCompensationTotal_Buyer;
            totalRow_Buyer.OtherCharges = otherChargesTotal_Buyer;
            totalRow_Buyer.Amount = amountTotal_Buyer;
            totalRow_Buyer.AccountBalance = accountBalanceTotal_Buyer;
            totalRow_Buyer.CustomerType = CustomerType.Buyer;
            flowList.Add(totalRow_Buyer);

            #endregion
            var dataList = new List<TransactionCustomerFundFlow>();
            var length=flowList.Count;
            for (var i = 0; i < length; i++)
            {
                var flow = ObjectMapper.Copy<TransactionCustomerFundFlow>(flowList[i]);
                if (i < length - 1)
                {
                    flow.RowNum = i + 1;
                }
                dataList.Add(flow);
            }
            dataGrid_Buyer.DataContext = dataList;
        }

        /// <summary>
        /// 加载卖家数据
        /// </summary>
        /// <param name="fundFlowList"></param>
        private void BindSellerGrid(IList<CustomerFundFlow> fundFlowList)
        {
            var flowList = fundFlowList.Where(customerFundFlow => customerFundFlow.CustomerType == CustomerType.Seller)
                    .OrderByDescending(customerFundFlow => customerFundFlow.CreateDate).ToList();

            #region 计算卖方合计列
            decimal houseFundTotal_Seller = 0;
            decimal taxesTotal_Seller = 0;
            decimal brokerageTotal_Seller = 0;
            decimal decorateCompensationTotal_Seller = 0;
            decimal otherChargesTotal_Seller = 0;
            decimal amountTotal_Seller = 0;
            decimal accountBalanceTotal_Seller = 0;

            foreach (CustomerFundFlow flow in flowList)
            {
                houseFundTotal_Seller += flow.HouseFund;
                taxesTotal_Seller += flow.Taxes;
                brokerageTotal_Seller += flow.Brokerage;
                decorateCompensationTotal_Seller += flow.DecorateCompensation;
                otherChargesTotal_Seller += flow.OtherCharges;
                amountTotal_Seller += flow.Amount;
            }
            CustomerFundFlow totalRow_Seller = new CustomerFundFlow();
            totalRow_Seller.ConditionType = "合计";
            totalRow_Seller.HouseFund = houseFundTotal_Seller;
            totalRow_Seller.Taxes = taxesTotal_Seller;
            totalRow_Seller.Brokerage = brokerageTotal_Seller;
            totalRow_Seller.DecorateCompensation = decorateCompensationTotal_Seller;
            totalRow_Seller.OtherCharges = otherChargesTotal_Seller;
            totalRow_Seller.Amount = amountTotal_Seller;
            totalRow_Seller.AccountBalance = accountBalanceTotal_Seller;
            totalRow_Seller.CustomerType = CustomerType.Seller;
            flowList.Add(totalRow_Seller);
            #endregion
            var dataList = new List<TransactionCustomerFundFlow>();
            var length = flowList.Count;
            for (var i = 0; i < length; i++)
            {
                var flow = ObjectMapper.Copy<TransactionCustomerFundFlow>(flowList[i]);
                if (i < length - 1)
                {
                    flow.RowNum = i + 1;
                }
                dataList.Add(flow);
            }
            dataGrid_Seller.DataContext = dataList;
        }

        /// <summary>
        /// 统计总数
        /// </summary>
        /// <param name="flowList"></param>
        private void SetAmount(IList<CustomerFundFlow> flowList)
        {
            Decimal incomeAmount = 0;
            Decimal expenditureAmount = 0;
            Decimal incomeAmount_Seller = 0;
            Decimal expenditureAmount_Seller = 0;
            Decimal incomeAmount_Buyer = 0;
            Decimal expenditureAmount_Buyer = 0;


            foreach (var flow in flowList)
            {
                if (flow.Amount > 0)
                {
                    incomeAmount += flow.Amount;
                    if (flow.CustomerType == CustomerType.Seller)
                        incomeAmount_Seller += flow.Amount;
                    else if (flow.CustomerType == CustomerType.Buyer)
                        incomeAmount_Buyer += flow.Amount;
                }
                else
                {
                    expenditureAmount += flow.Amount;
                    if (flow.CustomerType == CustomerType.Seller)
                        expenditureAmount_Seller += flow.Amount;
                    else if (flow.CustomerType == CustomerType.Buyer)
                        expenditureAmount_Buyer += flow.Amount;
                }
            }

            lblIncomeAmount_Seller.Content = incomeAmount_Seller.ToString("C");
            lblExpenditureAmount_Seller.Content = expenditureAmount_Seller.ToString("C");

            lblIncomeAmount_Buyer.Content = incomeAmount_Buyer.ToString("C");
            lblExpenditureAmount_Buyer.Content = expenditureAmount_Buyer.ToString("C");

            lblIncomeAmount.Content = incomeAmount.ToString("C");
            lblExpenditureAmount.Content = expenditureAmount.ToString("C");
        }

        /// <summary>
        /// 控件加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                InitCtrlData();
            }
            catch (Exception){

            }
            finally
            {
                if (LoadDataCompletedEvent != null)
                {
                    LoadDataCompletedEvent(sender, e);
                }
            }
        }

        /// <summary>
        /// 导航
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (NavClickEvent != null)
            {
                NavClickEvent(sender, e);
            }
        }

        /// <summary>
        /// 为排序用
        /// </summary>
        public class TransactionCustomerFundFlow : CustomerFundFlow
        {
            public int? RowNum { get; set; }

        }
    }
}
