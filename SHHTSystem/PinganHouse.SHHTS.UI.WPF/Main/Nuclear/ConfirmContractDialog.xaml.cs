﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Nuclear
{
    /// <summary>
    /// ConfirmContractDialog.xaml 的交互逻辑
    /// </summary>
    public partial class ConfirmContractDialog : Window
    {
        /// <summary>
        /// 定义确认完成签约--已签约  事件和对应委托
        /// </summary>
        /// <param name="caseId">案件编号</param>
        /// <param name="isTrialTax">是否立即审税</param>
        /// <param name="isLoan">是否申请贷款</param>
        public delegate void HA2Handle(string caseId, bool isTrialTax, bool isLoan,Dictionary<string ,object> paras,ConfirmContractDialog dialog);

        public event HA2Handle HA2Event;

        /// <summary>
        /// 案件编号
        /// </summary>
        private string m_CaseId;

        /// <summary>
        /// 带参数构造
        /// </summary>
        /// <param name="caseId"></param>
        public ConfirmContractDialog(string caseId)
        {
            InitializeComponent();
            m_CaseId = caseId;
            InitData(caseId);
        }

        /// <summary>
        /// 加载数据
        /// </summary>
        /// <param name="caseId"></param>
        private void InitData(string caseId) {

            //是否申请
            var allege = EnumHelper.InitAllegeToCombobox();
            cb_IsLoan.ItemsSource = allege;
            cb_IsLoan.DisplayMemberPath = "DisplayMember";
            cb_IsLoan.SelectedValuePath = "ValueMember";
            cb_IsLoan.SelectedValue = 1;

            //贷款类型
            var creditType = EnumHelper.InitCreditTypeToCombobox();
            cb_CreditType.ItemsSource = creditType;
            cb_CreditType.DisplayMemberPath = "DisplayMember";
            cb_CreditType.SelectedValuePath = "ValueMember";
            cb_CreditType.SelectedValue = 1;

            //贷款申请对象
            var loanObject = EnumHelper.InitLoanObjectToCombobox();
            cb_LoanObject.ItemsSource = loanObject;
            cb_LoanObject.DisplayMemberPath = "DisplayMember";
            cb_LoanObject.SelectedValuePath = "ValueMember";
            cb_LoanObject.SelectedValue = 1;

            var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(caseId);
            if (caseDto != null)
            {
                lab_caseId.Content = caseDto.CaseId;
                lab_cqzh.Content = caseDto.TenementContract;
            }        
        }

        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancle_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        /// <summary>
        /// 确认完成签约--
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            decimal netlabelPrice = 0;
            decimal.TryParse(txt_WQJG.Text.Trim(), out netlabelPrice);
            if (netlabelPrice <= 0)
            {
                MessageBox.Show("请填写正确的网签价格.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                //是否贷款 
                var isLoan = Convert.ToBoolean(cb_IsLoan.SelectedValue);
                var responseObject = Convert.ToString(cb_ResponseObject.SelectedValue);
                if (isLoan)
                {
                    if (string.IsNullOrEmpty(responseObject) || responseObject == "A-NULL")
                    {
                        MessageBox.Show("请选择银行.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }
                var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(m_CaseId);
                if (caseDto != null)
                {
                    //插入网签价格
                    var result = CaseProxyService.GetInstanse().ModifyNetlabelPrice(caseDto.SysNo, netlabelPrice, LoginHelper.CurrentUser.SysNo);
                    if (!result.Success)
                    {
                        if (MessageBox.Show("保存网签价格失败,是否继续操作", "系统询问", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                        {
                            e.Handled = true;
                            return;
                        }
                    }
                    //是否立即审税
                    bool isTrialTax = (bool)rb_Isss_1.IsChecked;                   
                    var paras = new Dictionary<string, object>();
                    //贷款类型
                    paras.Add("CreditType", cb_CreditType.SelectedValue);
                    //推送对象类型
                    paras.Add("LoanObject", cb_LoanObject.SelectedValue);
                    //推送对象
                    paras.Add("ResponseObject", cb_ResponseObject.SelectedValue);
                    //网签价格
                    paras.Add("NetlabelPrice", netlabelPrice);
                    if (HA2Event != null)
                    {
                        HA2Event(m_CaseId, isTrialTax, isLoan, paras, this);
                    }
                }
                else
                {
                    MessageBox.Show("案件数据获取失败，无法进行操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        /// <summary>
        /// 是否贷款
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoanObject_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cb_LoanObject.SelectedValue != null)
            {
                var loanObject = (LoanObject)Enum.Parse(typeof(LoanObject), cb_LoanObject.SelectedValue.ToString());
                switch (loanObject)
                {
                    case LoanObject.Bank:
                        //获取银行
                        var banks = ConfigServiceProxy.GetBankDic();
                        banks.Add("A-NULL", "请选择");
                        cb_ResponseObject.ItemsSource = banks;
                        cb_ResponseObject.DisplayMemberPath = "Value";
                        cb_ResponseObject.SelectedValuePath = "Key";
                        cb_ResponseObject.SelectedValue = "A-NULL";
                        break;
                    case LoanObject.Commissioner:
                        var commissioner = ConfigServiceProxy.GetCommissioners();
                        cb_ResponseObject.ItemsSource = commissioner;
                        cb_ResponseObject.DisplayMemberPath = null;
                        cb_ResponseObject.SelectedValuePath = null;
                        cb_ResponseObject.SelectedIndex = 0;
                        break;
                }
            }
            else {
                //获取银行
                var banks = ConfigServiceProxy.GetBankDic();
                banks.Add("A-NULL", "请选择");
                cb_ResponseObject.ItemsSource = banks;
                cb_ResponseObject.DisplayMemberPath = "Value";
                cb_ResponseObject.SelectedValuePath = "Key";
                cb_ResponseObject.SelectedIndex = 0;
                cb_ResponseObject.SelectedValue = "A-NULL";
            }
        }

        /// <summary>
        /// 是否贷款
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IsLoan_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try {
                var comboBox = sender as ComboBox;
                var value =Convert.ToString(comboBox.SelectedValue);
                if (string.IsNullOrEmpty(value))
                {
                    return;
                }
                var num = 0;
                int.TryParse(value, out num);
                cb_CreditType.IsEnabled = num>0;
                cb_ResponseObject.IsEnabled = num > 0;
                cb_LoanObject.IsEnabled = num > 0;
            }
            catch (Exception) { }
        }
    }
}
