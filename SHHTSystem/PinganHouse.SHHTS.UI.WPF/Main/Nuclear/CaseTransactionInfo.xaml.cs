﻿using PinganHouse.SHHTS.UI.WPF.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using DataGrid = System.Windows.Controls.DataGrid;
using PinganHouse.SHHTS.UI.WPF.Model;
using MessageBox = System.Windows.MessageBox;

namespace PinganHouse.SHHTS.UI.WPF.Main.Nuclear
{
    /// <summary>
    /// CaseTransactionInfo.xaml 的交互逻辑
    /// </summary>
    public partial class CaseTransactionInfo : Page
    {
        /// <summary>
        /// 案件编号
        /// </summary>
        private string m_CaseId;

        /// <summary>
        /// 页面状态 -- 核案过来的状态 很重要
        /// </summary>
        CaseStatus m_Status = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.HA3
                | CaseStatus.HA4 | CaseStatus.HA5 | CaseStatus.HA6 | CaseStatus.ZB2;

        private IList<CustomerFundFlow> m_CustomerFundFlowList;
        private List<long> m_SelectSellerSysNo = new List<long>();  //保存选中要删除行 
        private List<long> m_SelectBuyerSysNo = new List<long>();

        public CaseTransactionInfo(string caseId)
        {
            InitializeComponent();
            m_CaseId = caseId;
            m_CustomerFundFlowList = CustomerFundFlowServiceProxy.GetFlows(m_CaseId);
        }

        /// <summary>
        /// 取消首页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, "Main/Nuclear/CheckCaseSearchPage.xaml");
        }

        /// <summary>
        /// 上一步
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrevious_Click(object sender, RoutedEventArgs e)
        {
            var page = new CaseCustomerInfo(m_CaseId);
            PageHelper.PageNavigateHelper(this, page);

        }

        /// <summary>
        /// 完成报告提交HA4--审核报告
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnComplete_Click(object sender, RoutedEventArgs e)
        {
            var errorMessage = String.Empty;
            var fundFlowList = CustomerFundFlowServiceProxy.GetFlows(m_CaseId);

            //卖方房款
            //var buyerHouseFund =
            if (
                fundFlowList.Where(customerFundFlow => customerFundFlow.CustomerType == CustomerType.Seller)
                .Sum(s => s.HouseFund) != 0)
            {
                errorMessage += "卖方房款余额不为0\r\n";
            }

            //卖方税费
            if (
                fundFlowList.Where(customerFundFlow => customerFundFlow.CustomerType == CustomerType.Seller)
                .Sum(s => s.Taxes) != 0)
            {
                errorMessage += "卖方税费余额不为0\r\n";
            }


            //卖方佣金
            if (
                fundFlowList.Where(customerFundFlow => customerFundFlow.CustomerType == CustomerType.Seller)
                    .Sum(s => s.Brokerage) != 0)
            {
                errorMessage += "卖方中介佣金余额不为0\r\n";
            }

            //卖方装修补偿
            if (
                fundFlowList.Where(customerFundFlow => customerFundFlow.CustomerType == CustomerType.Seller)
                    .Sum(s => s.DecorateCompensation) != 0)
            {
                errorMessage += "卖方装修补偿余额不为0\r\n";
            }

            //卖方其他费用
            if (
                fundFlowList.Where(customerFundFlow => customerFundFlow.CustomerType == CustomerType.Seller)
                    .Sum(s => s.OtherCharges) != 0)
            {
                errorMessage += "卖方其他费用余额不为0\r\n";
            }

            //买方房款
            if (
                fundFlowList.Where(customerFundFlow => customerFundFlow.CustomerType == CustomerType.Buyer)
                    .Sum(s => s.HouseFund) != 0)
            {
                errorMessage += "买方房款余额不为0\r\n";
            }

            //买方税费
            if (
                fundFlowList.Where(customerFundFlow => customerFundFlow.CustomerType == CustomerType.Buyer)
                .Sum(s => s.Taxes) != 0)
            {
                errorMessage += "买方税费余额不为0\r\n";
            }

            //买方佣金
            if (
                fundFlowList.Where(customerFundFlow => customerFundFlow.CustomerType == CustomerType.Buyer)
                    .Sum(s => s.Brokerage) != 0)
            {
                errorMessage += "买方中介佣金余额不为0\r\n";
            }


            //买方装修补偿
            if (
                fundFlowList.Where(customerFundFlow => customerFundFlow.CustomerType == CustomerType.Buyer)
                    .Sum(s => s.DecorateCompensation) != 0)
            {
                errorMessage += "买方装修补偿余额不为0\r\n";
            }

            //买方其他费用
            if (
                fundFlowList.Where(customerFundFlow => customerFundFlow.CustomerType == CustomerType.Buyer)
                    .Sum(s => s.OtherCharges) != 0)
            {
                errorMessage += "买方其他费用余额不为0\r\n";
            }

            if (!String.IsNullOrEmpty(errorMessage))
            {
                MessageBox.Show("不能提交审核，原因如下：\r\n" + errorMessage);
                return;
            }

            try
            {
                var msgResult = MessageBox.Show("此动作不可撤销处理,确定所有报告内容填写正确并提交审核?", "系统确认", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                if (msgResult == MessageBoxResult.No) return;
                var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseIdAndCaseStatus(m_CaseId, m_Status);
                if (caseDto != null)
                {
                    OperationResult operationResult;
                    switch (caseDto.CaseStatus)
                    {
                        case CaseStatus.HA2:
                            operationResult = CaseProxyService.GetInstanse().UpdateCaseStatus(m_CaseId, CaseStatus.HA3, LoginHelper.CurrentUser.SysNo);
                            operationResult = CaseProxyService.GetInstanse().UpdateCaseStatus(m_CaseId, CaseStatus.HA4, LoginHelper.CurrentUser.SysNo);
                            if (!operationResult.Success)
                            {
                                MessageBox.Show(operationResult.ResultMessage, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
                                return;
                            }
                            break;
                        case CaseStatus.HA3:
                            operationResult = CaseProxyService.GetInstanse().UpdateCaseStatus(m_CaseId, CaseStatus.HA4, LoginHelper.CurrentUser.SysNo);
                            if (!operationResult.Success)
                            {
                                MessageBox.Show(operationResult.ResultMessage, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
                                return;
                            } break;
                        case CaseStatus.HA6:
                            operationResult = CaseProxyService.GetInstanse().UpdateCaseStatus(m_CaseId, CaseStatus.HA4, LoginHelper.CurrentUser.SysNo);
                            if (!operationResult.Success)
                            {
                                MessageBox.Show(operationResult.ResultMessage, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
                                return;
                            } break;
                    }

                }
                else
                {
                    MessageBox.Show("caseDto is null", "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                btnCancel_Click(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show("完成提交报告发生系统异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 添加收款
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSellerAddCollection_Click(object sender, RoutedEventArgs e)
        {

            var fundFlowDiaolg = new FundFlowDiaolg(CustomerType.Seller, m_CaseId, ChangeFundType.Collection);
            fundFlowDiaolg.SaveEvent += fundFlowDiaolg_SaveEvent;
            fundFlowDiaolg.ShowDialog();
        }

        /// <summary>
        /// 添加出款
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSellerAddPayment_Click(object sender, RoutedEventArgs e)
        {

            var fundFlowDiaolg = new FundFlowDiaolg(CustomerType.Seller, m_CaseId, ChangeFundType.Payment);
            fundFlowDiaolg.SaveEvent += fundFlowDiaolg_SaveEvent;
            fundFlowDiaolg.ShowDialog();
        }

        void fundFlowDiaolg_SaveEvent(DataTransferObjects.CustomerFundFlow flow)
        {
            var fundFlowList = CustomerFundFlowServiceProxy.GetFlows(m_CaseId);
            BindSellerGrid(fundFlowList);
            BindBuyerGrid(fundFlowList);
            SetAmount(fundFlowList);
        }

        private void BindBuyerGrid(IList<CustomerFundFlow> fundFlowList)
        {
            var flowList = fundFlowList.Where(customerFundFlow => customerFundFlow.CustomerType == CustomerType.Buyer)
                   .OrderByDescending(customerFundFlow => customerFundFlow.CreateDate).ToList();

            IList<TransactionCustomerFundFlow> transactionFlowList = new List<TransactionCustomerFundFlow>();
            foreach (var flow in flowList)
            {
                var t = new TransactionCustomerFundFlow
                {
                    SysNo = flow.SysNo,
                    ConditionType = flow.ConditionType,
                    HouseFund = flow.HouseFund,
                    Taxes = flow.Taxes,
                    Brokerage = flow.Brokerage,
                    DecorateCompensation = flow.DecorateCompensation,
                    OtherCharges = flow.OtherCharges,
                    Amount = flow.Amount,
                    AccountBalance = flow.AccountBalance,
                    CustomerType = flow.CustomerType,
                    Earnest = flow.Earnest,
                    PaymentChannel = flow.PaymentChannel
                };
                transactionFlowList.Add(t);
            }

            #region 计算买方合计列
            decimal houseFundTotal_Buyer = 0;
            decimal taxesTotal_Buyer = 0;
            decimal brokerageTotal_Buyer = 0;
            decimal decorateCompensationTotal_Buyer = 0;
            decimal otherChargesTotal_Buyer = 0;
            decimal amountTotal_Buyer = 0;
            decimal accountBalanceTotal_Buyer = 0;

            foreach (TransactionCustomerFundFlow flow in transactionFlowList)
            {
                houseFundTotal_Buyer += flow.HouseFund;
                taxesTotal_Buyer += flow.Taxes;
                brokerageTotal_Buyer += flow.Brokerage;
                decorateCompensationTotal_Buyer += flow.DecorateCompensation;
                otherChargesTotal_Buyer += flow.OtherCharges;
                amountTotal_Buyer += flow.Amount;
                //accountBalanceTotal_Buyer += flow.AccountBalance;
            }
            TransactionCustomerFundFlow totalRow_Buyer = new TransactionCustomerFundFlow();
            totalRow_Buyer.ConditionType = "合计";
            totalRow_Buyer.HouseFund = houseFundTotal_Buyer;
            totalRow_Buyer.Taxes = taxesTotal_Buyer;
            totalRow_Buyer.Brokerage = brokerageTotal_Buyer;
            totalRow_Buyer.DecorateCompensation = decorateCompensationTotal_Buyer;
            totalRow_Buyer.OtherCharges = otherChargesTotal_Buyer;
            totalRow_Buyer.Amount = amountTotal_Buyer;
            totalRow_Buyer.AccountBalance = accountBalanceTotal_Buyer;
            totalRow_Buyer.CustomerType = CustomerType.Buyer;
            


            transactionFlowList.Add(totalRow_Buyer);

            dataGrid_Buyer.DataContext = transactionFlowList;

            #endregion

        }

        private void BindSellerGrid(IList<CustomerFundFlow> fundFlowList)
        {
            var flowList = fundFlowList.Where(customerFundFlow => customerFundFlow.CustomerType == CustomerType.Seller)
                    .OrderByDescending(customerFundFlow => customerFundFlow.CreateDate).ToList();

            IList<TransactionCustomerFundFlow> transactionFlowList = new List<TransactionCustomerFundFlow>();
            foreach (var flow in flowList)
            {
                var t = new TransactionCustomerFundFlow
                {
                    SysNo = flow.SysNo,
                    ConditionType = flow.ConditionType,
                    HouseFund = flow.HouseFund,
                    Taxes = flow.Taxes,
                    Brokerage = flow.Brokerage,
                    DecorateCompensation = flow.DecorateCompensation,
                    OtherCharges = flow.OtherCharges,
                    Amount = flow.Amount,
                    AccountBalance = flow.AccountBalance,
                    CustomerType = flow.CustomerType,
                    Earnest = flow.Earnest,
                    PaymentChannel = flow.PaymentChannel
                };
                transactionFlowList.Add(t);
            }


            #region 计算卖方合计列
            decimal houseFundTotal_Seller = 0;
            decimal taxesTotal_Seller = 0;
            decimal brokerageTotal_Seller = 0;
            decimal decorateCompensationTotal_Seller = 0;
            decimal otherChargesTotal_Seller = 0;
            decimal amountTotal_Seller = 0;
            decimal accountBalanceTotal_Seller = 0;

            foreach (TransactionCustomerFundFlow flow in transactionFlowList)
            {
                houseFundTotal_Seller += flow.HouseFund;
                taxesTotal_Seller += flow.Taxes;
                brokerageTotal_Seller += flow.Brokerage;
                decorateCompensationTotal_Seller += flow.DecorateCompensation;
                otherChargesTotal_Seller += flow.OtherCharges;
                amountTotal_Seller += flow.Amount;
                //accountBalanceTotal_Seller += flow.AccountBalance;
            }
            TransactionCustomerFundFlow totalRow_Seller = new TransactionCustomerFundFlow();
            totalRow_Seller.ConditionType = "合计";
            totalRow_Seller.HouseFund = houseFundTotal_Seller;
            totalRow_Seller.Taxes = taxesTotal_Seller;
            totalRow_Seller.Brokerage = brokerageTotal_Seller;
            totalRow_Seller.DecorateCompensation = decorateCompensationTotal_Seller;
            totalRow_Seller.OtherCharges = otherChargesTotal_Seller;
            totalRow_Seller.Amount = amountTotal_Seller;
            totalRow_Seller.AccountBalance = accountBalanceTotal_Seller;
            totalRow_Seller.CustomerType = CustomerType.Seller;


            transactionFlowList.Add(totalRow_Seller);



            dataGrid_Seller.DataContext = transactionFlowList;

            #endregion

        }

        /// <summary>
        /// 添加收款
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBuyerAddCollection_Click(object sender, RoutedEventArgs e)
        {
            var fundFlowDiaolg = new FundFlowDiaolg(CustomerType.Buyer, m_CaseId, ChangeFundType.Collection);
            fundFlowDiaolg.SaveEvent += fundFlowDiaolg_SaveEvent;
            fundFlowDiaolg.ShowDialog();
        }

        /// <summary>
        /// 添加出款
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBuyerAddPayment_Click(object sender, RoutedEventArgs e)
        {
            var fundFlowDiaolg = new FundFlowDiaolg(CustomerType.Buyer, m_CaseId, ChangeFundType.Payment);
            fundFlowDiaolg.SaveEvent += fundFlowDiaolg_SaveEvent;
            fundFlowDiaolg.ShowDialog();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            BindSellerGrid(m_CustomerFundFlowList);
            BindBuyerGrid(m_CustomerFundFlowList);
            SetAmount(m_CustomerFundFlowList);
            //控制按钮活性
            SetControlsStatus();
        }

        /// <summary>
        /// 设置页面按钮的活性非活性
        /// </summary>
        private void SetControlsStatus()
        {
            var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseIdAndCaseStatus(m_CaseId, m_Status);
            if (caseDto == null) return;
            if (caseDto.CaseStatus == CaseStatus.HA5)
            {
                PageHelper.PageControlToDisable(Grid_Body, new System.Windows.Controls.Control[] { btnCancel, btnPrevious });
            }
        }

        private void btnSellerDel_Click(object sender, RoutedEventArgs e)
        {
            foreach (int sysNo in m_SelectSellerSysNo)
            {
                CustomerFundFlowServiceProxy.DeleteFlows(sysNo, LoginHelper.CurrentUser.SysNo);
            }
            var fundFlowList = CustomerFundFlowServiceProxy.GetFlows(m_CaseId);
            BindSellerGrid(fundFlowList);
            SetAmount(fundFlowList);

            m_SelectSellerSysNo.Clear();
        }

        private void btnBuyerDel_Click(object sender, RoutedEventArgs e)
        {
            foreach (int sysNo in m_SelectBuyerSysNo)
            {
                CustomerFundFlowServiceProxy.DeleteFlows(sysNo, LoginHelper.CurrentUser.SysNo);
            }
            var fundFlowList = CustomerFundFlowServiceProxy.GetFlows(m_CaseId);
            BindBuyerGrid(fundFlowList);
            SetAmount(fundFlowList);

            m_SelectBuyerSysNo.Clear();

        }

        private void SellerCheckBox_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.CheckBox checkBox = sender as System.Windows.Controls.CheckBox;
            int sysNo = int.Parse(checkBox.Tag.ToString());
            var bl = checkBox.IsChecked;
            if (bl == true)
            {
                m_SelectSellerSysNo.Add(sysNo);     //如果选中就保存FsysNo  
            }
            else
            {
                m_SelectSellerSysNo.Remove(sysNo);  //如果选中取消就删除里面的sysNo  
            }
        }

        private void BuyerCheckBox_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.CheckBox checkBox = sender as System.Windows.Controls.CheckBox;
            int sysNo = int.Parse(checkBox.Tag.ToString());
            var bl = checkBox.IsChecked;
            if (bl == true)
            {
                m_SelectBuyerSysNo.Add(sysNo);     //如果选中就保存sysNo  
            }
            else
            {
                m_SelectBuyerSysNo.Remove(sysNo);  //如果选中取消就删除里面的sysNo  
            }
        }

        private void SetAmount(IList<CustomerFundFlow> flowList)
        {
            Decimal incomeAmount = 0;
            Decimal expenditureAmount = 0;
            Decimal incomeAmount_Seller = 0;
            Decimal expenditureAmount_Seller = 0;
            Decimal incomeAmount_Buyer = 0;
            Decimal expenditureAmount_Buyer = 0;


            foreach (var flow in flowList)
            {
                if (flow.Amount > 0)
                {
                    incomeAmount += flow.Amount;
                    if (flow.CustomerType == CustomerType.Seller)
                        incomeAmount_Seller += flow.Amount;
                    else if (flow.CustomerType == CustomerType.Buyer)
                        incomeAmount_Buyer += flow.Amount;
                }
                else
                {
                    expenditureAmount += flow.Amount;
                    if (flow.CustomerType == CustomerType.Seller)
                        expenditureAmount_Seller += flow.Amount;
                    else if (flow.CustomerType == CustomerType.Buyer)
                        expenditureAmount_Buyer += flow.Amount;
                }
            }

            lblIncomeAmount_Seller.Content = incomeAmount_Seller.ToString("C");
            lblExpenditureAmount_Seller.Content = expenditureAmount_Seller.ToString("C");

            lblIncomeAmount_Buyer.Content = incomeAmount_Buyer.ToString("C");
            lblExpenditureAmount_Buyer.Content = expenditureAmount_Buyer.ToString("C");

            lblIncomeAmount.Content = incomeAmount.ToString("C");
            lblExpenditureAmount.Content = expenditureAmount.ToString("C");
        }

        private void DataGrid_OnLoadingRow(object sender, DataGridRowEventArgs e)
        {
            DataGridRow dataGridRow = e.Row;

            ((TransactionCustomerFundFlow)(dataGridRow.Item)).RowNum = e.Row.GetIndex() + 1;
            if (((TransactionCustomerFundFlow)(dataGridRow.Item)).ConditionType == "合计")
            {
                dataGridRow.Background = Brushes.White;
                ((TransactionCustomerFundFlow)(dataGridRow.Item)).RowNum = 0;
            }


        }


        private void DataGrid_Buyer_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if ((e.OriginalSource as ScrollViewer) == null)
            {
                var rowIndex = dataGrid_Buyer.SelectedIndex;
                const int columnIndex = 10;
                if (rowIndex >= 0)
                {
                    FrameworkElement item =
                        dataGrid_Buyer.Columns[columnIndex].GetCellContent(dataGrid_Buyer.Items[rowIndex]);
                    DataGridTemplateColumn temp = (dataGrid_Buyer.Columns[columnIndex] as DataGridTemplateColumn);
                    var ck = temp.CellTemplate.FindName("buyerCheckBox", item) as System.Windows.Controls.CheckBox;
                    int sysNo = int.Parse(ck.Tag.ToString());
                    if (sysNo != 0)
                    {
                        if (ck.IsChecked == true)
                        {
                            ck.IsChecked = false;
                            m_SelectBuyerSysNo.Remove(sysNo); //如果选中取消就删除里面的sysNo  
                        }
                        else
                        {
                            ck.IsChecked = true;
                            m_SelectBuyerSysNo.Add(sysNo); //如果选中就保存sysNo 
                        }
                    }
                }
            }
        }

        private void DataGrid_Seller_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if ((e.OriginalSource as ScrollViewer) == null)
            {
                var rowIndex = dataGrid_Seller.SelectedIndex;
                const int columnIndex = 9;
                if (rowIndex >= 0)
                {
                    FrameworkElement item =
                        dataGrid_Seller.Columns[columnIndex].GetCellContent(dataGrid_Seller.Items[rowIndex]);
                    DataGridTemplateColumn temp = (dataGrid_Seller.Columns[columnIndex] as DataGridTemplateColumn);
                    var ck = temp.CellTemplate.FindName("sellerCheckBox", item) as System.Windows.Controls.CheckBox;
                    int sysNo = int.Parse(ck.Tag.ToString());
                    if (sysNo != 0)
                    {
                        if (ck.IsChecked == true)
                        {
                            ck.IsChecked = false;
                            m_SelectSellerSysNo.Remove(sysNo); //如果选中取消就删除里面的sysNo  
                        }
                        else
                        {
                            ck.IsChecked = true;
                            m_SelectSellerSysNo.Add(sysNo); //如果选中就保存sysNo 
                        }
                    }
                }
            }
        }

    }

    [ValueConversion(typeof(string), typeof(string))]
    public class ColorConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Decimal val = (Decimal)value;
            if (val >= 0)
            {
                return "Green";
            }


            return "red";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {

            return "";
        }
    }

    public class ContentConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Decimal val = (Decimal)value;
            if (val == 0)
            {
                return "--";
            }


            return val.ToString("N");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return "";
        }
    }

    public class CheckBoxConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            String val = (String)value;
            if (val == "合计")
            {
                return "Hidden";
            }


            return "Visible";
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return "";
        }

    }

    public class PaymentChannelConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            FundPlanPayChannel val = (FundPlanPayChannel)value;
            if (val == FundPlanPayChannel.Trusteeship)
            {
                return "平安托管";
            }
            else if (val == FundPlanPayChannel.Loan)
            {
                return "银行贷款";
            }
            else if (val == FundPlanPayChannel.SelfPay)
            {
                return "直接支付";
            }
            else if (val == FundPlanPayChannel.Unknown)
            {
                return "";
            }

            return "";
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return "";
        }
        
    }

    public class TransactionCustomerFundFlow : CustomerFundFlow
    {
        public int RowNum { get; set; }

    }
}
