﻿using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Nuclear
{
    /// <summary>
    /// ProcessCtrl.xaml 的交互逻辑
    /// </summary>
    public partial class ProcessCtrl : UserControl
    {
        public ProcessCtrl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 计算时间
        /// </summary>
        /// <param name="dicCaseStatus"></param>
        public void BindingDataToProcess(Dictionary<CaseStatus, Tuple<string, DateTime>> dicCaseStatus)
        {
            try
            {
                //签约中
                DateTime? time_HA1 = null;
                DateTime? time_HA2 = null;
                DateTime? time_HA3 = null;
                DateTime? time_HA4 = null;
                DateTime? time_HA5 = null;
                var backgroundColor = (Color)ColorConverter.ConvertFromString("#B5F05A22");
                var brush = new SolidColorBrush(backgroundColor);
                foreach (var item in dicCaseStatus)
                {
                    switch (item.Key)
                    {
                        case CaseStatus.HA1:
                            txt_HA1.Text = item.Value.Item2.ToString("yyyy.MM.dd HH:mm");
                            time_HA1 = item.Value.Item2;
                            break;
                        case CaseStatus.HA2:
                            txt_HA2.Text = item.Value.Item2.ToString("yyyy.MM.dd HH:mm");
                            time_HA2 = item.Value.Item2;
                            btn_HA2.Background = brush;
                            time_HA2 = item.Value.Item2;
                            CalculationTime(CaseStatus.HA1, CaseStatus.HA2, time_HA1, time_HA2);
                            break;
                        case CaseStatus.HA3:
                            txt_HA3.Text = item.Value.Item2.ToString("yyyy.MM.dd HH:mm");
                            time_HA3 = item.Value.Item2;
                            btn_HA3.Background = brush;
                            time_HA3 = item.Value.Item2;
                            CalculationTime(CaseStatus.HA2, CaseStatus.HA3, time_HA2, time_HA3);
                            break;
                        case CaseStatus.HA4:
                            txt_HA4.Text = item.Value.Item2.ToString("yyyy.MM.dd HH:mm");
                            time_HA4 = item.Value.Item2;
                            btn_HA4.Background = brush;
                            time_HA4 = item.Value.Item2;
                            CalculationTime(CaseStatus.HA3, CaseStatus.HA4, time_HA3, time_HA4);
                            break;
                        case CaseStatus.HA5:
                            txt_HA5.Text = item.Value.Item2.ToString("yyyy.MM.dd HH:mm");
                            time_HA5 = item.Value.Item2;
                            btn_HA5.Background = brush;
                            time_HA5 = item.Value.Item2;
                            CalculationTime(CaseStatus.HA4, CaseStatus.HA5, time_HA4, time_HA5);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// 计算时间差
        /// </summary>
        private void CalculationTime(CaseStatus startStatus, CaseStatus endStatus, DateTime? startTime, DateTime? endTime)
        {
            try
            {
                var tspan = (TimeSpan)(endTime - startTime);
                var txt = tspan.Days.ToString() + "天" + tspan.Hours.ToString() + "时" +
                                        tspan.Minutes.ToString() + "分" + tspan.Seconds.ToString() + "秒";
                switch (endStatus)
                {
                    case CaseStatus.HA2:
                        txt_HA2_HA1.Text = txt;
                        image_HA2_HA1.Visibility = Visibility.Visible;
                        break;
                    case CaseStatus.HA3:
                        txt_HA3_HA2.Text = txt;
                        image_HA3_HA2.Visibility = Visibility.Visible;
                        break;
                    case CaseStatus.HA4:
                        txt_HA4_HA3.Text = txt;
                        image_HA4_HA3.Visibility = Visibility.Visible;
                        break;
                    case CaseStatus.HA5:
                        txt_HA5_HA4.Text = txt;
                        image_HA5_HA4.Visibility = Visibility.Visible;
                        break;
                }
            }
            catch (Exception) { }
        }
    }
}
