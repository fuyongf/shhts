﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using PinganHouse.SHHTS.UI.WPF.Main.Dialog;

namespace PinganHouse.SHHTS.UI.WPF.Main.Nuclear
{
    /// <summary>
    /// CheckCaseBasicInfo.xaml 的交互逻辑
    /// </summary>
    public partial class CaseBasicInfo : Page
    {
        /// <summary>
        /// 案件编号
        /// </summary>
        private string m_CaseId = string.Empty;

        /// <summary>
        /// 页面状态 -- 核案过来的状态
        /// </summary>
        CaseStatus m_Status = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.HA3
                | CaseStatus.HA4 | CaseStatus.HA5 | CaseStatus.HA6 | CaseStatus.ZB2;

        public CaseBasicInfo(string caseId)
        {
            InitializeComponent();
            m_CaseId = caseId;
            InitCaseData(caseId);
        }

        /// <summary>
        /// 初始化页面基础和案件数据
        /// </summary>
        /// <param name="caseId"></param>
        private void InitCaseData(string caseId)
        {
            try
            {
                InitComboboxData();
                //案件对象
                var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseIdAndCaseStatus(caseId, m_Status);
                //案件附属对象
                var caseRemarkDto = CaseProxyService.GetInstanse().GetRemarkBySysNo(caseDto.SysNo);
                //中介信息
                var agencyDto = AgentServiceProxy.GetAgentSatff(caseDto.AgencySysNo);
                //中介公司信息
                var agentCompany = AgentServiceProxy.GetAgentCompany(caseDto.CompanySysNo);
                //银行卡信息
                var bankAccounts = CaseProxyService.GetInstanse().GetCustomerBankAccounts(caseDto.SysNo);
                //绑定数据
                BindingDataToControls(caseDto, caseRemarkDto, bankAccounts, agentCompany, agencyDto);
                //判断账户是否已经鉴权
                //IsAuthenticationToControls();
                //设置控件活性
                if (caseDto.CaseStatus == CaseStatus.HA5 || caseDto.CaseStatus == CaseStatus.ZB2)
                {
                    PageHelper.PageControlToDisable(Grid_Body, new System.Windows.Controls.Control[] { this.btnCancel, this.btnNext });
                }
                //如果没有签约则不可以进行下一步
                if (caseDto.CaseStatus == CaseStatus.HA1 || caseDto.CaseStatus == CaseStatus.YJ1)
                {
                    PageHelper.PageControlToDisable(Grid_Body);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("加载数据发生错误：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 判断是否已经鉴权
        /// </summary>
        private void IsAuthenticationToControls()
        {
            try
            {
                //资金托管---卖方付款账号
                if (!string.IsNullOrEmpty(bank_ZJTG_YZFKKH.Text.Trim()))
                {
                    bank_ZJTG_YZFKKH.Tag = true;
                    PageHelper.PageControlToDisable(W_ZJTG_YZFK, new System.Windows.Controls.Control[] { this.Modify_ZJTG_YZFK });
                }
                //资金托管---买方付款账号
                if (!string.IsNullOrEmpty(bank_ZJTG_MJFKKH.Text.Trim()))
                {
                    bank_ZJTG_MJFKKH.Tag = true;
                    PageHelper.PageControlToDisable(W_ZJTG_MJFK, new System.Windows.Controls.Control[] { this.Modify_ZJTG_MJFK });
                }

                //佣金金托管---卖方付款账号
                if (!string.IsNullOrEmpty(bank_YJ_MJFKKH.Text.Trim()))
                {
                    bank_YJ_MJFKKH.Tag = true;
                    PageHelper.PageControlToDisable(W_YJ_MJFK, new System.Windows.Controls.Control[] { this.Modify_YJ_MJFK });
                }

                //佣金托管---买方付款账号
                if (!string.IsNullOrEmpty(bank_YJ_YZFKKH.Text.Trim()))
                {
                    bank_YJ_YZFKKH.Tag = true;
                    PageHelper.PageControlToDisable(W_YJ_YZFK, new System.Windows.Controls.Control[] { this.Modify_YJ_YZFK });
                }

            }
            catch (Exception)
            {
                MessageBox.Show("初始化帐号鉴权状态失败.", "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 判断是否有鉴权标记
        /// </summary>
        /// <param name="flag"></param>
        private void IsAuthenticationToControls(string flag)
        {
            try
            {
                switch (flag)
                {
                    //资金
                    case "cb_TGZJ":
                        //资金托管---卖方付款账号
                        if (!string.IsNullOrEmpty(bank_ZJTG_YZFKKH.Text.Trim()) && Convert.ToBoolean(bank_ZJTG_YZFKKH.Tag))
                        {
                            PageHelper.PageControlToDisable(W_ZJTG_YZFK, new System.Windows.Controls.Control[] { this.Modify_ZJTG_YZFK });
                        }
                        //资金托管---买方付款账号
                        if (!string.IsNullOrEmpty(bank_ZJTG_MJFKKH.Text.Trim()) && Convert.ToBoolean(bank_ZJTG_MJFKKH.Tag))
                        {
                            PageHelper.PageControlToDisable(W_ZJTG_MJFK, new System.Windows.Controls.Control[] { this.Modify_ZJTG_MJFK });
                        }
                        break;
                    //佣金
                    case "cb_YJTG":
                        //佣金金托管---卖方付款账号
                        if (!string.IsNullOrEmpty(bank_YJ_MJFKKH.Text.Trim()) && Convert.ToBoolean(bank_YJ_MJFKKH.Tag))
                        {
                            PageHelper.PageControlToDisable(W_YJ_MJFK, new System.Windows.Controls.Control[] { this.Modify_YJ_MJFK });
                        }

                        //佣金托管---买方付款账号
                        if (!string.IsNullOrEmpty(bank_YJ_YZFKKH.Text.Trim()) && Convert.ToBoolean(bank_YJ_YZFKKH.Tag))
                        {
                            PageHelper.PageControlToDisable(W_YJ_YZFK, new System.Windows.Controls.Control[] { this.Modify_YJ_YZFK });
                        }
                        break;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("判断帐号鉴权状态失败.", "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 绑定案件基本数据数据
        /// </summary>
        /// <param name="caseDto"></param>
        /// <param name="caseRemarkDto"></param>
        /// <param name="bankAccounts"></param>
        /// <param name="agentCompany"></param>
        /// <param name="agent"></param>
        private void BindingDataToControls(CaseDto caseDto, CaseRemark caseRemarkDto, IEnumerable<CustomerBankAccount> bankAccounts, AgentCompany agentCompany, AgentStaff agent)
        {
            try
            {
                if (caseDto != null)
                {
                    var tenementContracts = caseDto.TenementContract.Split('-');
                    if (tenementContracts.Length > 1)
                    {
                        lab_CQZH.Text = tenementContracts[0];
                        lab_CQZZ.Text = tenementContracts[1];
                    }
                    else
                    {
                        lab_CQZH.Text = caseDto.TenementContract;
                        lab_CQZH.Width = 100;
                        lab_CQZZ.Width = 30;
                        txt_Run.Visibility = Visibility.Collapsed;
                    }
                    txt_TenementAddress.Text = caseDto.TenementAddress;
                    //资金托管
                    cb_TGZJ.IsChecked = caseDto.IsFund;
                    txt_TGZJZH.Text = caseDto.FundTrusteeshipContract;
                    //佣金托管
                    cb_YJTG.IsChecked = caseDto.IsCommission;
                    txt_YJTGZH.Text = caseDto.CommissionTrusteeshipContract;
                }

                if (caseRemarkDto != null)
                {
                    ctrlCaseRemark.DataContext = caseRemarkDto;
                    //物业名称
                    txt_WYMC.Text = caseRemarkDto.TenementName;
                    //环线位置
                    if (!string.IsNullOrEmpty(caseRemarkDto.LoopLinePosition))
                    {
                        cb_HXWZ.SelectedValue = caseRemarkDto.LoopLinePosition;
                    }
                    //建筑面积
                    txt_JZMJ.Text = Convert.ToString(caseRemarkDto.CoveredArea);
                    //房
                    if (caseRemarkDto.RoomCount != null)
                    {
                        cb_FANG.Text = Convert.ToString(caseRemarkDto.RoomCount);
                    }
                    //厅
                    if (caseRemarkDto.HallCount != null)
                    {
                        cb_TING.Text = Convert.ToString(caseRemarkDto.HallCount);
                    }
                    //卫
                    if (caseRemarkDto.ToiletCount != 0)
                    {
                        cb_WEI.Text = Convert.ToString(caseRemarkDto.ToiletCount);
                    }
                    //层数
                    txt_CENG.Text = Convert.ToString(caseRemarkDto.FloorCount);
                    //竣工时间
                    if (caseRemarkDto.BuiltCompletedDate != null)
                    {
                        cb_JGRQ.SelectedValue = (int)Boole.True;
                        dp_JGRQ.Text = Convert.ToDateTime(caseRemarkDto.BuiltCompletedDate).Year.ToString();
                    }
                    //居间价格
                    txt_JJJG.Text = Convert.ToString(caseRemarkDto.MediatorPrice);
                    //网签价格
                    txt_WQJG.Text = Convert.ToString(caseRemarkDto.NetlabelPrice);
                    //真实成交价格
                    txt_JYJG.Text = Convert.ToString(caseRemarkDto.RealPrice);
                    //交易类型
                    this.SetTradeType(caseRemarkDto.TradeType);
                    //居住类型
                    if (caseRemarkDto.ResideType != ResideType.Unknow)
                    {
                        cb_JZLX.SelectedValue = (int)caseRemarkDto.ResideType;
                    }
                    //是否首次购买
                    cb_SCGM.IsChecked = Convert.ToBoolean(caseRemarkDto.FirstTimeBuy);
                    //购入年限
                    txt_GRNX.Text = Convert.ToString(caseRemarkDto.PurchaseYears);
                    //唯一住房
                    cb_WYZF.IsChecked = Convert.ToBoolean(caseRemarkDto.OnlyHousing);
                    //车位地址？
                    txt_CWDZ.Text = caseRemarkDto.CarportAddress;
                    //车位面积？
                    txt_CWMJ.Text = Convert.ToString(caseRemarkDto.CarportArea);
                    //车位价格？
                    txt_CWJG.Text = Convert.ToString(caseRemarkDto.CarportPrice);
                    //是否补偿地价
                    cb_BDJ.IsChecked = Convert.ToBoolean(caseRemarkDto.IsRepairLandPrice);
                    //是否国安审批
                    cb_GASP.IsChecked = Convert.ToBoolean(caseRemarkDto.IsNSAApproval);
                    //是否历史保护建筑
                    cb_LSJZ.IsChecked = Convert.ToBoolean(caseRemarkDto.IsProtectiveBuilding);
                    //是否公正
                    cb_GZ.IsChecked = Convert.ToBoolean(caseRemarkDto.IsNotarize);
                    //公证员
                    txt_GZY.Text = Convert.ToString(caseRemarkDto.Notary);
                    //公正联系方式
                    txt_GZYLXFS.Text = Convert.ToString(caseRemarkDto.NotaryContact);
                }

                #region 经纪人信息
                if (agent != null)
                {
                    lab_Mobile.Content = agent.Mobile;
                    lab_AgencyName.Content = agent.RealName;
                }
                #endregion

                #region 物业公司信息
                if (agentCompany != null)
                {
                    //中介收款账号
                    lab_CJSKZH_1.Text = ConfigServiceProxy.GetBankNameByCode(agentCompany.ReceiveBank)
                        + " " + agentCompany.ReceiveSubBank
                        + " " + agentCompany.ReceiveName;
                    lab_CJSKZH_2.Text = BankAccountFormat(agentCompany.ReceiveAccount);
                    lab_CompanyName.Content = agentCompany.CompanyName;
                }
                #endregion

                #region 银行卡信息绑定
                //银行卡信息
                if (bankAccounts != null)
                {
                    foreach (var bankAccount in bankAccounts)
                    {
                        switch (bankAccount.CustomerType)
                        {
                            case CustomerType.Buyer:
                                if (bankAccount.AccountType == BankAccountPurpose.Payment)
                                {
                                    switch (bankAccount.BizType)
                                    {
                                        case OrderBizType.HosingFund:
                                            bank_ZJTG_MJFKKH.Text = BankAccountFormat(bankAccount.AccountNo);//卡号
                                            bank_ZJTG_MJFKYH.Text = bankAccount.BankName;//银行
                                            bank_ZJTG_MJFKYH.Tag = bankAccount.BankCode;//银编码行
                                            bank_ZJTG_MJFKZH.Text = bankAccount.SubBankName;//支行
                                            bank_ZJTG_MJFKHM.Text = bankAccount.AccountName;//收款人
                                            break;
                                        case OrderBizType.AgencyFee:
                                            bank_YJ_MJFKKH.Text = BankAccountFormat(bankAccount.AccountNo);
                                            bank_YJ_MJFKYH.Text = bankAccount.BankName;
                                            bank_YJ_MJFKYH.Tag = bankAccount.BankCode;
                                            bank_YJ_MJFKZH.Text = bankAccount.SubBankName;
                                            bank_YJ_MJFKHM.Text = bankAccount.AccountName;
                                            break;
                                    }
                                }
                                break;
                            case CustomerType.Seller:
                                if (bankAccount.AccountType == BankAccountPurpose.Payment)
                                {
                                    switch (bankAccount.BizType)
                                    {
                                        case OrderBizType.HosingFund:
                                            bank_ZJTG_YZFKKH.Text = BankAccountFormat(bankAccount.AccountNo);//卡号
                                            bank_ZJTG_YZFKYH.Text = bankAccount.BankName;//银行
                                            bank_ZJTG_YZFKYH.Tag = bankAccount.BankCode;//银编码行
                                            bank_ZJTG_YZFKZH.Text = bankAccount.SubBankName;//支行
                                            bank_ZJTG_YZFKHM.Text = bankAccount.AccountName;
                                            break;
                                        case OrderBizType.AgencyFee:
                                            bank_YJ_YZFKKH.Text = BankAccountFormat(bankAccount.AccountNo);
                                            bank_YJ_YZFKYH.Text = bankAccount.BankName;
                                            bank_YJ_YZFKYH.Tag = bankAccount.BankCode;
                                            bank_YJ_YZFKZH.Text = bankAccount.SubBankName;
                                            bank_YJ_YZFKHM.Text = bankAccount.AccountName;
                                            break;
                                    }
                                }
                                else if (bankAccount.AccountType == BankAccountPurpose.Receive)
                                {
                                    bank_ZJTG_YZSKKH.Text = BankAccountFormat(bankAccount.AccountNo);
                                    bank_ZJTG_YZSKYH.Text = bankAccount.BankName;
                                    bank_ZJTG_YZSKYH.Tag = bankAccount.BankCode;
                                    bank_ZJTG_YZSKZH.Text = bankAccount.SubBankName;
                                    bank_ZJTG_YZSKHM.Text = bankAccount.AccountName;
                                }
                                break;
                        }
                    }
                }
                #endregion
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// 初始化页面Combobox数据
        /// </summary>
        private void InitComboboxData()
        {
            try
            {
                //竣工日期
                var boole = EnumHelper.InitBooleToCombobox();
                cb_JGRQ.ItemsSource = boole;
                cb_JGRQ.DisplayMemberPath = "DisplayMember";
                cb_JGRQ.SelectedValuePath = "ValueMember";
                cb_JGRQ.SelectedIndex = 0;

                //环线位置
                var linkLocation = EnumHelper.InitLinkLocationToCombobox();
                linkLocation.OrderByDescending(item => item.ValueMember).ToList<EnumHelper>();
                cb_HXWZ.ItemsSource = linkLocation;
                cb_HXWZ.DisplayMemberPath = "DisplayMember";
                cb_HXWZ.SelectedValuePath = "ValueMember";
                cb_HXWZ.SelectedValue = -999;

                //居住类型
                var resideType = EnumHelper.InitResideTypeToCombobox();
                linkLocation.OrderByDescending(item => item.ValueMember).ToList<EnumHelper>();
                cb_JZLX.ItemsSource = resideType;
                cb_JZLX.DisplayMemberPath = "DisplayMember";
                cb_JZLX.SelectedValuePath = "ValueMember";
                cb_JZLX.SelectedValue = -999;

                //房
                cb_FANG.SelectedIndex = 1;
                //厅
                cb_TING.SelectedIndex = 1;
                //卫
                cb_WEI.SelectedIndex = 1;
            }
            catch (Exception)
            {
                throw new Exception("初始化页面基础数据错误.");
            }
        }

        /// <summary>
        /// 保存数据进行下一步
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                loading_Ctrl.IsBusy = true;
                loading_Ctrl.Text = "正在处理数据,请稍后...";
                var caseStatus = CaseStatus.Unkown;
                var result = SaveCaseRemark(out caseStatus);
                if (result.Success)
                {
                    //完成签约 和 立案不通过再次提交
                    if (caseStatus == CaseStatus.HA2 || caseStatus == CaseStatus.HA6)
                    {
                        HA3_CaseStatus(m_CaseId);
                    }
                    JumpToNextPage();
                }
                else
                {
                    MessageBox.Show("保存数据失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                loading_Ctrl.IsBusy = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("保存数据发生异常错误：" + ex.Message, "系统异常错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                loading_Ctrl.IsBusy = false;
            }
        }

        /// <summary>
        /// 提交报告---报告填写中
        /// </summary>
        /// <param name="caseId">提交报告</param>
        private void HA3_CaseStatus(string caseId)
        {
            try
            {
                CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.HA3, LoginHelper.CurrentUser.SysNo);
            }
            catch (Exception)
            {
                // To do 暂时不做异常处理;
            }
        }

        /// <summary>
        /// 取消保存返货列表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, "Main/Nuclear/CheckCaseSearchPage.xaml");
        }

        /// <summary>
        /// 保存案件扩展信息
        /// </summary>
        /// <param name="caseStatus">案件当前状态</param>
        /// <returns></returns>
        private OperationResult SaveCaseRemark(out CaseStatus caseStatus)
        {
            caseStatus = CaseStatus.Unkown;
            var checkResult = CheckDataValidity();
            if (!checkResult.Success) return checkResult;
            //获取案件
            var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseIdAndCaseStatus(m_CaseId, m_Status);
            if (caseDto == null) return new OperationResult(1, "未能获取正确的案件数据...");
            caseStatus = caseDto.CaseStatus;
            if (caseStatus == CaseStatus.HA5)
            {
                JumpToNextPage();
            }
            //获取案件扩展属性
            var caseRemark = CaseProxyService.GetInstanse().GetRemarkBySysNo(caseDto.SysNo);
            if (caseRemark == null) caseRemark = new CaseRemark();
            var caseRemarkModel = new CaseRemarkModel
            {
                TenementName = caseRemark.TenementName,//   物业名称
                LoopLinePosition = caseRemark.LoopLinePosition, //环线位置
                ToiletCount = caseRemark.ToiletCount,
                BuiltCompletedDate = caseRemark.BuiltCompletedDate, //竣工时间
                IsLowPrice = caseRemark.IsLowPrice, //低价房
                TenementNature = caseRemark.TenementNature,//物业性质
                ResideType = caseRemark.ResideType,//居住类型
                TradeType = caseRemark.TradeType, //交易类型
                FirstTimeBuy = caseRemark.FirstTimeBuy, //是否首次购买
                PurchaseYears = caseRemark.PurchaseYears,//是否满五年
                OnlyHousing = caseRemark.OnlyHousing,//是否唯一住房
                CarportAddress = caseRemark.CarportAddress,//车位地址
                CarportPrice = caseRemark.CarportPrice,//车位价格
                CarportArea = caseRemark.CarportArea,//车位面积
                IsRepairLandPrice = caseRemark.IsRepairLandPrice,//是否补偿地价
                IsNSAApproval = caseRemark.IsNSAApproval,//国安审批
                IsProtectiveBuilding = caseRemark.IsProtectiveBuilding,//历史建筑
                IsNotarize = caseRemark.IsNotarize,//是否公正
                Notary = caseRemark.Notary,//公证员
                NotaryContact = caseRemark.NotaryContact//公正员联系方式
            };
            //建筑面积
            if (caseRemark.CoveredArea != null)
            {
                caseRemarkModel.CoveredArea = (decimal)caseRemark.CoveredArea;
            }
            //房间数
            if (caseRemark.RoomCount != null)
            {
                caseRemarkModel.RoomCount = (int)caseRemark.RoomCount;
            }
            //厅数
            if (caseRemark.HallCount != null)
            {
                caseRemarkModel.HallCount = (int)caseRemark.HallCount;
            }
            //楼层
            if (caseRemark.FloorCount != null)
            {
                caseRemarkModel.FloorCount = (int)caseRemark.FloorCount;
            }
            //房间价格
            if (caseRemark.MediatorPrice != null)
            {
                caseRemarkModel.MediatorPrice = (decimal)caseRemark.MediatorPrice;
            }
            //网签价格
            if (caseRemark.NetlabelPrice != null)
            {
                caseRemarkModel.NetlabelPrice = (decimal)caseRemark.NetlabelPrice;
            }
            //真实交易价格
            if (caseRemark.RealPrice != null)
            {
                caseRemarkModel.RealPrice = (decimal)caseRemark.RealPrice;
            }

            //*******************************************************************************************
            //物业名称
            caseRemarkModel.TenementName = txt_WYMC.Text.Trim();
            //环线位置
            caseRemarkModel.LoopLinePosition = Convert.ToString(cb_HXWZ.SelectedValue);
            //建筑面积
            decimal coveredArea;
            Decimal.TryParse(txt_JZMJ.Text.Trim(), out coveredArea);
            if (coveredArea > 0)
                caseRemarkModel.CoveredArea = coveredArea;
            //房间数
            int roomCount;
            int.TryParse(Convert.ToString(cb_FANG.SelectionBoxItem), out roomCount);
            caseRemarkModel.RoomCount = roomCount;
            //厅数
            int hallCount;
            int.TryParse(Convert.ToString(cb_TING.SelectionBoxItem), out hallCount);
            caseRemarkModel.HallCount = hallCount;
            //卫生间数
            int toiletCount;
            int.TryParse(Convert.ToString(cb_WEI.SelectionBoxItem), out toiletCount);
            caseRemarkModel.ToiletCount = toiletCount;
            //楼层数
            int floorCount;
            int.TryParse(Convert.ToString(txt_CENG.Text), out floorCount);
            caseRemarkModel.FloorCount = floorCount;
            //居间价格
            decimal mediatorPrice;
            Decimal.TryParse(txt_JJJG.Text.Trim(), out mediatorPrice);
            if (mediatorPrice > 0)
                caseRemarkModel.MediatorPrice = mediatorPrice;
            //网签价格
            decimal netlabelPrice;
            Decimal.TryParse(txt_WQJG.Text.Trim(), out netlabelPrice);
            if (netlabelPrice > 0)
                caseRemarkModel.NetlabelPrice = netlabelPrice;
            //真实成交价格
            decimal realPrice;
            Decimal.TryParse(txt_JYJG.Text.Trim(), out realPrice);
            if (realPrice > 0)
                caseRemarkModel.RealPrice = realPrice;
            //竣工时间
            DateTime builtCompletedDate;
            if (DateTime.TryParse(dp_JGRQ.Text.Trim() + "-01-01", out builtCompletedDate))
            {
                caseRemarkModel.BuiltCompletedDate = builtCompletedDate;
            }
            else
            {
                caseRemarkModel.BuiltCompletedDate = null;
            }
            //是否做低价
            var isLowPrice = ((bool)rb_DGJ.IsChecked);
            if (isLowPrice) { caseRemarkModel.IsLowPrice = null; }
            else
            {
                caseRemarkModel.IsLowPrice = (bool)rb_DGJ_0.IsChecked ? true : false;//是否做低房价
            }
            //交易类型
            caseRemarkModel.TradeType = (TradeType)GetTradeType();
            //居住类型
            var resideType = Convert.ToString(cb_JZLX.SelectedValue);
            if (!resideType.Equals("-999"))
            {
                caseRemarkModel.ResideType = (ResideType)Enum.Parse(typeof(ResideType), resideType);
            }
            //是否首次购买
            caseRemarkModel.FirstTimeBuy = cb_SCGM.IsChecked;
            //购入年限
            int purchaseYears = 0;
            int.TryParse(txt_GRNX.Text.Trim(), out purchaseYears);
            caseRemarkModel.PurchaseYears = purchaseYears;
            //是否唯一住房
            caseRemarkModel.OnlyHousing = cb_WYZF.IsChecked;
            //车位地址
            caseRemarkModel.CarportAddress = txt_CWDZ.Text;

            //车位面积
            decimal carportArea;
            decimal.TryParse(txt_CWMJ.Text.Trim(), out carportArea);
            caseRemarkModel.CarportArea = carportArea;
            //车位价格
            decimal carportPrice;
            decimal.TryParse(txt_CWJG.Text.Trim(), out carportPrice);
            caseRemarkModel.CarportPrice = carportPrice;

            //是否补地
            caseRemarkModel.IsRepairLandPrice = cb_BDJ.IsChecked;
            //是否国安审批
            caseRemarkModel.IsNSAApproval = cb_GASP.IsChecked;
            //是否历史保护
            caseRemarkModel.IsProtectiveBuilding = cb_LSJZ.IsChecked;
            //是否公正
            caseRemarkModel.IsNotarize = cb_GZ.IsChecked;
            //公证员
            caseRemarkModel.Notary = txt_GZY.Text.Trim();
            //公证员联系方式
            caseRemarkModel.NotaryContact = txt_GZYLXFS.Text.Trim();

            //资金/佣金是否托管
            caseRemarkModel.IsCommissionTrusteeship = cb_YJTG.IsChecked;
            caseRemarkModel.IsFundTrusteeship = cb_TGZJ.IsChecked;
            caseRemarkModel.FtContractNo = txt_TGZJZH.Text.Trim();
            caseRemarkModel.CtContractNo = txt_YJTGZH.Text.Trim();

            #region **************资金托管*****************************

            //卖方收款-托管资金
            var sellerReceive = new CustomerBankAccount
            {
                CaseId = m_CaseId,
                CustomerType = CustomerType.Seller,
                BankName = bank_ZJTG_YZSKYH.Text.Trim(),
                SubBankName = bank_ZJTG_YZSKZH.Text.Trim(),
                AccountNo = bank_ZJTG_YZSKKH.Text.Trim().Replace(" ", ""),
                AccountName = bank_ZJTG_YZSKHM.Text.Trim(),
                AccountType = BankAccountPurpose.Receive,
                BizType = OrderBizType.HosingFund,
                CreateUserSysNo = LoginHelper.CurrentUser.SysNo
            };
            if (bank_ZJTG_YZSKYH.Tag != null)
            {
                sellerReceive.BankCode = bank_ZJTG_YZSKYH.Tag.ToString();
            }

            //卖方付款-托管资金
            var sellerPay = new CustomerBankAccount
            {
                CaseId = m_CaseId,
                CustomerType = CustomerType.Seller,
                BankName = bank_ZJTG_YZFKYH.Text.Trim(),
                SubBankName = bank_ZJTG_YZFKZH.Text.Trim(),
                AccountNo = bank_ZJTG_YZFKKH.Text.Trim().Replace(" ", ""),
                AccountName = bank_ZJTG_YZFKHM.Text.Trim(),
                AccountType = BankAccountPurpose.Payment,
                BizType = OrderBizType.HosingFund,
                CreateUserSysNo = LoginHelper.CurrentUser.SysNo
            };
            if (bank_ZJTG_YZFKYH.Tag != null)
            {
                sellerPay.BankCode = bank_ZJTG_YZFKYH.Tag.ToString();
            }

            //买方付款-房款-资金
            var buyerPayment = new CustomerBankAccount
            {
                CaseId = m_CaseId,
                CustomerType = CustomerType.Buyer,
                BankName = bank_ZJTG_MJFKYH.Text.Trim(),
                SubBankName = bank_ZJTG_MJFKZH.Text.Trim(),
                BizType = OrderBizType.HosingFund,
                AccountNo = bank_ZJTG_MJFKKH.Text.Trim().Replace(" ", ""),
                AccountName = bank_ZJTG_MJFKHM.Text.Trim(),
                AccountType = BankAccountPurpose.Payment,
                CreateUserSysNo = LoginHelper.CurrentUser.SysNo
            };
            if (bank_ZJTG_MJFKYH.Tag != null)
            {
                buyerPayment.BankCode = bank_ZJTG_MJFKYH.Tag.ToString();
            }
            #endregion

            #region ***********佣金***********
            //买方付款-佣金-资金
            var buyerPay = new CustomerBankAccount
            {
                CaseId = m_CaseId,
                CustomerType = CustomerType.Buyer,
                BankName = bank_YJ_MJFKYH.Text.Trim(),
                SubBankName = bank_YJ_MJFKZH.Text.Trim(),
                BizType = OrderBizType.AgencyFee,
                AccountNo = bank_YJ_MJFKKH.Text.Trim().Replace(" ", ""),
                AccountName = bank_YJ_MJFKHM.Text.Trim(),
                AccountType = BankAccountPurpose.Payment,
                CreateUserSysNo = LoginHelper.CurrentUser.SysNo
            };
            if (bank_YJ_MJFKYH.Tag != null)
            {
                buyerPay.BankCode = bank_YJ_MJFKYH.Tag.ToString();
            }
            //卖方付款-中介佣金
            var sellerPayment = new CustomerBankAccount
            {
                CaseId = m_CaseId,
                CustomerType = CustomerType.Seller,
                BankName = bank_YJ_YZFKYH.Text.Trim(),
                SubBankName = bank_YJ_YZFKZH.Text.Trim(),
                BizType = OrderBizType.AgencyFee,
                AccountNo = bank_YJ_YZFKKH.Text.Trim().Replace(" ", ""),
                AccountName = bank_YJ_YZFKHM.Text.Trim(),
                AccountType = BankAccountPurpose.Payment,
                CreateUserSysNo = LoginHelper.CurrentUser.SysNo
            };
            if (bank_YJ_YZFKYH.Tag != null)
            {
                sellerPayment.BankCode = bank_YJ_YZFKYH.Tag.ToString();
            }
            #endregion
            var customerBankAccount = new List<CustomerBankAccount>() { sellerReceive, buyerPayment, buyerPay };
            //卖方付款-托管资金
            if (!string.IsNullOrEmpty(sellerPay.BankCode) && !string.IsNullOrEmpty(sellerPay.SubBankName) && !string.IsNullOrEmpty(sellerPay.AccountNo) && !string.IsNullOrEmpty(sellerPay.AccountName))
            {
                customerBankAccount.Add(sellerPay);
            }
            // //卖方付款-中介佣金
            if (!string.IsNullOrEmpty(sellerPayment.BankCode) && !string.IsNullOrEmpty(sellerPayment.SubBankName) && !string.IsNullOrEmpty(sellerPayment.AccountNo) && !string.IsNullOrEmpty(sellerPayment.AccountName))
            {
                customerBankAccount.Add(sellerPayment);
            }
            caseRemarkModel.TrusteeshipAccounts = customerBankAccount.ToArray();// new CustomerBankAccount[] { sellerReceive, sellerPay, buyerPayment, sellerPayment, buyerPay };
            var result = CaseProxyService.GetInstanse().PerfectCaseInfo(caseDto.SysNo, caseRemarkModel, LoginHelper.CurrentUser.SysNo);
            return result;
        }

        /// <summary>
        /// 跳转下一个页面 --上下家通讯信息
        /// </summary>
        private void JumpToNextPage()
        {
            var customerInfo = new CaseCustomerInfo(m_CaseId);
            PageHelper.PageNavigateHelper(this, customerInfo);
        }

        /// <summary>
        /// 获取交易类型
        /// </summary>
        /// <returns></returns>
        private int? GetTradeType()
        {
            if ((bool)TradeType_1.IsChecked)
            {
                return 1;
            }
            if ((bool)TradeType_2.IsChecked)
            {
                return 2;
            }
            if ((bool)TradeType_3.IsChecked)
            {
                return 3;
            }
            return null;
        }

        /// <summary>
        /// 设置交易类型
        /// </summary>
        /// <param name="type"></param>
        private void SetTradeType(TradeType type)
        {
            switch (type)
            {
                case TradeType.Normal:
                    TradeType_1.IsChecked = true;
                    break;
                case TradeType.Second:
                    TradeType_2.IsChecked = true;
                    break;
                case TradeType.Other:
                    TradeType_3.IsChecked = true;
                    break;
            }
        }

        /// <summary>
        /// 验证是否数字输入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text_TextChanged(object sender, TextChangedEventArgs e)
        {
            //屏蔽中文输入和非法字符粘贴输入
            TextBox textBox = sender as TextBox;
            TextChange[] change = new TextChange[e.Changes.Count];
            e.Changes.CopyTo(change, 0);

            int offset = change[0].Offset;
            if (change[0].AddedLength > 0)
            {
                double num = 0;
                if (!double.TryParse(textBox.Text, out num))
                {
                    ThreadPool.QueueUserWorkItem((obj) =>
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            textBox.Text = string.Empty;
                            textBox.Select(offset, 0);

                        });
                    });
                }
            }
        }

        /// <summary>
        /// 格式化银行卡号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BankText_TextChanged(object sender, TextChangedEventArgs e)
        {
            var textBox = sender as TextBox;
            var txt = BankAccountFormat(textBox.Text.Trim());
            textBox.Text = txt.TrimEnd();
            textBox.Select(txt.Length, 0);
        }

        /// <summary>
        /// 格式化银行卡显示
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        private string BankAccountFormat(string txt)
        {
            try
            {
                if (txt == null) return txt;
                txt = txt.Replace(" ", "");
                var size = txt.Length / 4;
                var newstr = new StringBuilder();
                for (int n = 0; n < size; n++)
                {
                    newstr.Append(txt.Substring(n * 4, 4));
                    newstr.Append(" ");
                }
                newstr.Append(txt.Substring(size * 4, txt.Length % 4));
                return newstr.ToString();
            }
            catch (Exception)
            {
                return txt;
            }
        }

        /// <summary>
        /// 验证输入字符
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;

            //屏蔽非法按键
            if ((e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) || e.Key == Key.Decimal)
            {
                if (txt.Text.Contains(".") && e.Key == Key.Decimal)
                {
                    e.Handled = true;
                    return;
                }
                e.Handled = false;
            }
            else if (((e.Key >= Key.D0 && e.Key <= Key.D9) || e.Key == Key.OemPeriod) && e.KeyboardDevice.Modifiers != ModifierKeys.Shift)
            {
                if (txt.Text.Contains(".") && e.Key == Key.OemPeriod)
                {
                    e.Handled = true;
                    return;
                }
                e.Handled = false;
            }
            else if (e.Key == Key.Tab)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// 数据合法性验证
        /// </summary>
        private OperationResult CheckDataValidity()
        {
            var result = new OperationResult(0, null);
            try
            {
                //验证环线位置
                var loopLine = Convert.ToString(cb_HXWZ.SelectedValue);
                if (string.IsNullOrEmpty(loopLine) || loopLine.Equals("-999"))
                {
                    result = new OperationResult(1, "请选择环线位置.");
                    cb_HXWZ.Focus();
                    return result;
                }
                if (cb_JGRQ.SelectedValue != null)
                {
                    int location = 0;
                    int.TryParse(cb_JGRQ.SelectedValue.ToString(), out location);
                    if (location > 0 && string.IsNullOrEmpty(dp_JGRQ.Text.Trim()))
                    {
                        result = new OperationResult(1, "请填写竣工日期.");
                        dp_JGRQ.Focus();
                        return result;
                    }
                }
                //验证竣工日期是否准确
                var completedTime = dp_JGRQ.Text.Trim();
                if (!string.IsNullOrEmpty(completedTime))
                {
                    DateTime time = DateTime.Now;
                    var timeStr = completedTime.Split(new char[] { '-' });
                    if (!DateTime.TryParse(timeStr[0] + "-01-01", out time))
                    {
                        result = new OperationResult(1, "请输入正确的竣工日期.");
                        dp_JGRQ.Focus();
                        return result;
                    }
                    else if (time < Convert.ToDateTime("1949-10-01"))
                    {
                        result = new OperationResult(1, "请输入正确的竣工日期.");
                        dp_JGRQ.Focus();
                        return result;
                    }
                }
                //网签价格
                decimal netlabelPrice;
                Decimal.TryParse(txt_WQJG.Text.Trim(), out netlabelPrice);
                if (netlabelPrice == 0)
                {
                    result = new OperationResult(1, "网签价格必须大于0.");
                    txt_WQJG.Focus();
                    return result;
                }

                var tradeType = GetTradeType();//交易性质
                if (tradeType == null)
                {
                    result = new OperationResult(1, "请选择交易类型.");
                    return result;
                }

                //验证购入年限
                //if (!string.IsNullOrEmpty(txt_GRNX.Text.Trim()))
                //{
                //    if (txt_GRNX.Text.Trim().Length < 4)
                //    {
                //        result = new OperationResult(1, "请填写正确的购入年限.");
                //        return result;
                //    }
                //    else
                //    {
                //        var yearStr = txt_GRNX.Text.Trim().Substring(0, 2);
                //        if (!yearStr.Contains("19"))
                //        {
                //            result = new OperationResult(1, "请填写正确的购入年限.");
                //            return result;
                //        }
                //    }
                //}

                UIElementCollection Childrens = Grid_Body.Children;
                result = EnumControls(Childrens);
                if (!result.Success)
                {
                    return result;
                }
                //验证物业性质
                var value = Convert.ToString(cb_JZLX.SelectedValue);
                if (value == "-999" || string.IsNullOrEmpty(value))
                {
                    result = new OperationResult(1, "请选择房屋类型.");
                    cb_JZLX.Focus();
                    return result;
                }

                //公证员
                if (Convert.ToBoolean(cb_GZ.IsChecked))
                {
                    if (string.IsNullOrEmpty(txt_GZY.Text))
                    {
                        result = new OperationResult(1, "请填写公证员.");
                        txt_GZY.Focus();
                        return result;
                    }
                    if (string.IsNullOrEmpty(txt_GZYLXFS.Text))
                    {
                        result = new OperationResult(1, "请填写公证员联系方式.");
                        txt_GZYLXFS.Focus();
                        return result;
                    }
                }

                #region 托管资金
                //是否托管资金
                if ((bool)cb_TGZJ.IsChecked)
                {
                    //资金托管合同
                    if (string.IsNullOrEmpty(txt_TGZJZH.Text.Trim()))
                    {
                        result = new OperationResult(1, "请填写资金托管合同号.");
                        txt_TGZJZH.Focus();
                        return result;
                    }

                    #region 卖方收款--房款
                    //卖方收款账户-银行                  
                    if (string.IsNullOrEmpty(bank_ZJTG_YZSKYH.Text.Trim()))
                    {
                        result = new OperationResult(1, "请选择卖方收款账户-银行.");
                        bank_ZJTG_YZSKYH.Focus();
                        return result;
                    }
                    //卖方收款账户-支行                   
                    if (string.IsNullOrEmpty(bank_ZJTG_YZSKZH.Text.Trim()))
                    {
                        result = new OperationResult(1, "请填写卖方收款账户-支行名称.");
                        bank_ZJTG_YZSKZH.Focus();
                        return result;
                    }
                    //卖方收款账户-银行账户         
                    if (string.IsNullOrEmpty(bank_ZJTG_YZSKKH.Text.Trim()))
                    {
                        result = new OperationResult(1, "请填写卖方收款账户-银行账号.");
                        bank_ZJTG_YZSKKH.Focus();
                        return result;
                    }
                    //else
                    //{
                    //    var isAuthentication= Convert.ToBoolean(bank_ZJTG_YZSKKH.Tag);
                    //    if (!isAuthentication)
                    //    {
                    //        result = new OperationResult(1, "资金托管-卖方收款账户-银行账号未通过鉴权.");
                    //        bank_ZJTG_YZSKKH.Focus();
                    //        return result;
                    //    }
                    //}
                    //卖方收款账户-银行户名
                    if (string.IsNullOrEmpty(bank_ZJTG_YZSKHM.Text.Trim()))
                    {
                        result = new OperationResult(1, "请填写卖方收款账户-银行户名.");
                        bank_ZJTG_YZSKHM.Focus();
                        return result;
                    }
                    #endregion

                    #region 卖方付款 --房款
                    if (!string.IsNullOrEmpty(bank_ZJTG_YZFKYH.Text.Trim())
                        || !string.IsNullOrEmpty(bank_ZJTG_YZFKZH.Text.Trim())
                        || !string.IsNullOrEmpty(bank_ZJTG_YZFKKH.Text.Trim()) || !string.IsNullOrEmpty(bank_ZJTG_YZFKHM.Text.Trim()))
                    {
                        //卖方收款账户-银行                  
                        if (string.IsNullOrEmpty(bank_ZJTG_YZFKYH.Text.Trim()))
                        {
                            result = new OperationResult(1, "请选择卖方付款账户-银行.");
                            bank_ZJTG_YZFKYH.Focus();
                            return result;
                        }
                        //卖方收款账户-支行                   
                        if (string.IsNullOrEmpty(bank_ZJTG_YZFKZH.Text.Trim()))
                        {
                            result = new OperationResult(1, "请填写卖方付款账户-支行名称.");
                            bank_ZJTG_YZFKZH.Focus();
                            return result;
                        }
                        //卖方收款账户-银行账户         
                        if (string.IsNullOrEmpty(bank_ZJTG_YZFKKH.Text.Trim()))
                        {
                            result = new OperationResult(1, "请填写卖方付款账户-银行账号.");
                            bank_ZJTG_YZFKKH.Focus();
                            return result;
                        }
                        //else
                        //{
                        //    var isAuthentication = Convert.ToBoolean(bank_ZJTG_YZFKKH.Tag);
                        //    if (!isAuthentication)
                        //    {
                        //        result = new OperationResult(1, "资金托管-卖方付款账户-银行账号未通过鉴权.");
                        //        bank_ZJTG_YZFKKH.Focus();
                        //        return result;
                        //    }
                        //}
                        //卖方收款账户-银行户名        
                        if (string.IsNullOrEmpty(bank_ZJTG_YZFKHM.Text.Trim()))
                        {
                            result = new OperationResult(1, "请填写卖方付款账户-银行户名.");
                            bank_ZJTG_YZFKHM.Focus();
                            return result;
                        }
                    }
                    #endregion

                    #region 买方付款-- 房款
                    //买方付款账户-银行                  
                    if (string.IsNullOrEmpty(bank_ZJTG_MJFKYH.Text.Trim()))
                    {
                        result = new OperationResult(1, "请选择买方付款账户-银行.");
                        bank_ZJTG_MJFKZH.Focus();
                        return result;
                    }
                    //买方付款账户-支行                   
                    if (string.IsNullOrEmpty(bank_ZJTG_MJFKZH.Text.Trim()))
                    {
                        result = new OperationResult(1, "请填写买方付款账户-支行名称.");
                        bank_ZJTG_MJFKZH.Focus();
                        return result;
                    }
                    //买方付款账户-银行账户         
                    if (string.IsNullOrEmpty(bank_ZJTG_MJFKKH.Text.Trim()))
                    {
                        result = new OperationResult(1, "请填写买方付款账户-银行账号.");
                        bank_ZJTG_MJFKKH.Focus();
                        return result;
                    }
                    //else
                    //{
                    //    var isAuthentication = Convert.ToBoolean(bank_ZJTG_MJFKKH.Tag);
                    //    if (!isAuthentication)
                    //    {
                    //        result = new OperationResult(1, "资金托管-买方付款账户-银行账号未通过鉴权.");
                    //        bank_ZJTG_MJFKKH.Focus();
                    //        return result;
                    //    }
                    //}
                    //买方付款账户-银行户名        
                    if (string.IsNullOrEmpty(bank_ZJTG_MJFKHM.Text.Trim()))
                    {
                        result = new OperationResult(1, "请填写买方付款账户-银行户名.");
                        bank_ZJTG_MJFKHM.Focus();
                        return result;
                    }
                    #endregion
                }
                #endregion

                #region 佣金托管账户检查
                //是否托管佣金
                if ((bool)cb_YJTG.IsChecked)
                {
                    if (string.IsNullOrEmpty(txt_YJTGZH.Text.Trim()))
                    {
                        result = new OperationResult(1, "请填写佣金托管合同号.");
                        txt_YJTGZH.Focus();
                        return result;
                    }

                    if ((string.IsNullOrEmpty(bank_YJ_MJFKYH.Text.Trim()) && string.IsNullOrEmpty(bank_YJ_MJFKZH.Text.Trim()) &&
                            string.IsNullOrEmpty(bank_YJ_MJFKKH.Text.Trim()) && string.IsNullOrEmpty(bank_YJ_MJFKHM.Text.Trim())) &&
                            (string.IsNullOrEmpty(bank_YJ_YZFKYH.Text.Trim()) && string.IsNullOrEmpty(bank_YJ_YZFKZH.Text.Trim()) &&
                            string.IsNullOrEmpty(bank_YJ_YZFKKH.Text.Trim()) && string.IsNullOrEmpty(bank_YJ_YZFKHM.Text.Trim())))
                    {
                        result = new OperationResult(1, "请输入买方或卖方佣金付款账号.");
                        txt_YJTGZH.Focus();
                        return result;
                    }
                    else
                    {
                        #region 买方付款账户

                        if (string.IsNullOrEmpty(bank_YJ_YZFKYH.Text.Trim()) && string.IsNullOrEmpty(bank_YJ_YZFKZH.Text.Trim()) &&
                            string.IsNullOrEmpty(bank_YJ_YZFKKH.Text.Trim()) && string.IsNullOrEmpty(bank_YJ_YZFKHM.Text.Trim()))
                        {
                            
                            //买方付款账户-银行                  
                            if (string.IsNullOrEmpty(bank_YJ_MJFKYH.Text.Trim()))
                            {
                                result = new OperationResult(1, "请选择买方付款账户-银行.");
                                bank_YJ_MJFKYH.Focus();
                                return result;
                            }
                            //买方付款账户-支行                   
                            if (string.IsNullOrEmpty(bank_YJ_MJFKZH.Text.Trim()))
                            {
                                result = new OperationResult(1, "请填写买方付款账户-支行名称.");
                                bank_YJ_MJFKZH.Focus();
                                return result;
                            }
                            //买方付款账户-银行账户         
                            if (string.IsNullOrEmpty(bank_YJ_MJFKKH.Text.Trim()))
                            {
                                result = new OperationResult(1, "请填写买方付款账户-银行卡号.");
                                bank_YJ_MJFKKH.Focus();
                                return result;
                            }
                            //else
                            //{
                            //    var isAuthentication = Convert.ToBoolean(bank_YJ_MJFKKH.Tag);
                            //    if (!isAuthentication)
                            //    {
                            //        result = new OperationResult(1, "佣金托管-买方付款账户-银行账号未通过鉴权.");
                            //        bank_YJ_MJFKKH.Focus();
                            //        return result;
                            //    }
                            //}
                            //买方付款账户-银行户名         
                            if (string.IsNullOrEmpty(bank_YJ_MJFKHM.Text.Trim()))
                            {
                                result = new OperationResult(1, "请填写买方付款账户-银行户名.");
                                bank_YJ_MJFKHM.Focus();
                                return result;
                            }
                        }
                        #endregion

                        #region  卖方付款账户
                        if (!string.IsNullOrEmpty(bank_YJ_YZFKYH.Text.Trim())
                            || !string.IsNullOrEmpty(bank_YJ_YZFKZH.Text.Trim())
                            || !string.IsNullOrEmpty(bank_YJ_YZFKKH.Text.Trim())
                            || !string.IsNullOrEmpty(bank_YJ_YZFKHM.Text.Trim()))
                        {
                            //卖方付款账户-银行                  
                            if (string.IsNullOrEmpty(bank_YJ_YZFKYH.Text.Trim()))
                            {
                                result = new OperationResult(1, "请选择卖方付款账户-银行.");
                                bank_YJ_YZFKYH.Focus();
                                return result;
                            }
                            //卖方付款账户-支行                   
                            if (string.IsNullOrEmpty(bank_YJ_YZFKZH.Text.Trim()))
                            {
                                result = new OperationResult(1, "请填写卖方付款账户-支行名称.");
                                bank_YJ_YZFKZH.Focus();
                                return result;
                            }
                            //卖方付款账户-银行账户         
                            if (string.IsNullOrEmpty(bank_YJ_YZFKKH.Text.Trim()))
                            {
                                result = new OperationResult(1, "请填写卖方付款账户-银行卡号.");
                                bank_YJ_YZFKKH.Focus();
                                return result;
                            }
                            //else
                            //{
                            //    var isAuthentication = Convert.ToBoolean(bank_YJ_YZFKKH.Tag);
                            //    if (!isAuthentication)
                            //    {
                            //        result = new OperationResult(1, "佣金托管-卖方付款账户-银行账号未通过鉴权.");
                            //        bank_YJ_YZFKKH.Focus();
                            //        return result;
                            //    }
                            //}
                            //卖方付款账户-银行户名      
                            if (string.IsNullOrEmpty(bank_YJ_YZFKHM.Text.Trim()))
                            {
                                result = new OperationResult(1, "请填写卖方付款账户-银行户名.");
                                bank_YJ_YZFKHM.Focus();
                                return result;
                            }
                        }
                        #endregion
                    }
                #endregion
                }
            }
            catch (Exception e)
            {
                result = new OperationResult(1, "验证数据合法性异常错误：" + e.Message);
            }
            return result;
        }

        /// <summary>
        /// 递归验证数据合法性
        /// </summary>
        /// <param name="collections"></param>
        /// <returns></returns>
        private OperationResult EnumControls(UIElementCollection collections)
        {
            var result = new OperationResult(0, null);
            foreach (UIElement ui in collections)
            {
                //ui转成控件,验证Tag是否必须输入
                switch (ui.DependencyObjectType.Name)
                {
                    case "TextBox":
                        var textBox = ui as TextBox;
                        bool validate = true;
                        if (textBox.Tag != null)
                        {
                            bool.TryParse(Convert.ToString(textBox.Tag), out validate);
                            if (validate && string.IsNullOrEmpty(textBox.Text))
                            {
                                result = new OperationResult(1, textBox.ToolTip + "不能为空");
                                textBox.Focus();
                            }
                        }
                        break;
                    case "Grid":
                    case "WrapPanel":
                    case "StackPanel":
                        var panle = ui as System.Windows.Controls.Panel;
                        if (panle != null)
                        {
                            var childrens = panle.Children;
                            result = EnumControls(childrens);
                        }
                        break;
                }
                if (!result.Success) return result;
            }
            return result;
        }

        /// <summary>
        /// 上下家资金托管
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            var checkBox = sender as CheckBox;
            bool checkStatus = false;
            if (checkBox.IsChecked != null)
            {
                checkStatus = (bool)checkBox.IsChecked;
            }
            switch (checkBox.Name)
            {
                case "cb_TGZJ":
                    if (checkStatus)
                    {
                        PageHelper.PageControlToActivate(ZJTG);
                        txt_TGZJZH.Focus();
                    }
                    else
                    {
                        PageHelper.PageControlToDisable(ZJTG, new System.Windows.Controls.Control[] { cb_TGZJ });
                    }
                    break;
                case "cb_YJTG":
                    if (checkStatus)
                    {
                        PageHelper.PageControlToActivate(YJTG);
                        txt_YJTGZH.Focus();
                    }
                    else
                    {
                        PageHelper.PageControlToDisable(YJTG, new System.Windows.Controls.Control[] { cb_YJTG });
                    }
                    break;
            }
            IsAuthenticationToControls(checkBox.Name);
        }

        /// <summary>
        /// 数字键移除焦点格式化赋值
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NumText_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                var textBox = sender as TextBox;
                var txt = textBox.Text.Trim();
                decimal num = 0;
                if (decimal.TryParse(txt, out num))
                {
                    txt = string.Format("{0:N}", num);
                    textBox.Text = txt;
                }
            }
            catch (Exception) { }
        }

        /// <summary>
        /// 选择银行
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSelectBank_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var btn = sender as Button;
                var bankDialog = new BanksDialog(Convert.ToString(btn.Tag));
                bankDialog.SelectBankEvent += bankDialog_SelectBankEventOld;
                bankDialog.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("选择银行操作发生系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 选择银行
        /// </summary>
        /// <param name="bankCode"></param>
        /// <param name="bankName"></param>
        private void bankDialog_SelectBankEventOld(string bankCode, string bankName, string tag)
        {
            switch (tag)
            {
                case "ZJTG_YZSK":
                    bank_ZJTG_YZSKYH.Text = bankName;
                    bank_ZJTG_YZSKYH.Tag = bankCode;
                    bank_ZJTG_YZSKZH.Focusable = true;
                    txt_TGZJZH.Focus();
                    break;
                case "ZJTG_YZFK":
                    bank_ZJTG_YZFKYH.Text = bankName;
                    bank_ZJTG_YZFKYH.Tag = bankCode;
                    bank_ZJTG_YZFKZH.Focus();
                    bank_ZJTG_YZFKZH.IsEnabled = true;
                    break;
                case "ZJTG_MJFK":
                    bank_ZJTG_MJFKYH.Text = bankName;
                    bank_ZJTG_MJFKYH.Tag = bankCode;
                    bank_ZJTG_MJFKZH.Focus();
                    bank_ZJTG_MJFKZH.IsEnabled = true;
                    break;
                case "YJ_MJFK":
                    bank_YJ_MJFKYH.Text = bankName;
                    bank_YJ_MJFKYH.Tag = bankCode;
                    bank_YJ_MJFKZH.Focus();
                    bank_YJ_MJFKZH.IsEnabled = true;
                    break;
                case "YJ_YZFK":
                    bank_YJ_YZFKYH.Text = bankName;
                    bank_YJ_YZFKYH.Tag = bankCode;
                    bank_YJ_YZFKZH.Focus();
                    bank_YJ_YZFKZH.IsEnabled = true;
                    break;
            }
        }

        /// <summary>
        /// 银行鉴权处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Authentication_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var caseId = m_CaseId;
                var accountName = string.Empty;
                var backCode = string.Empty;
                var bankNo = string.Empty;
                var authentication = sender as Button;
                var tag = Convert.ToString(authentication.Tag);
                switch (tag)
                {
                    case "ZJTG_YZSK":
                        backCode = bank_ZJTG_YZSKYH.Tag as string;
                        bankNo = bank_ZJTG_YZSKKH.Text.Trim();
                        accountName = bank_ZJTG_YZSKHM.Text.Trim();
                        break;
                    case "ZJTG_YZFK":
                        backCode = bank_ZJTG_YZFKYH.Tag as string;
                        bankNo = bank_ZJTG_YZFKKH.Text.Trim();
                        accountName = bank_ZJTG_YZFKHM.Text.Trim();
                        break;
                    case "ZJTG_MJFK":
                        backCode = bank_ZJTG_MJFKYH.Tag as string;
                        bankNo = bank_ZJTG_MJFKKH.Text.Trim();
                        accountName = bank_ZJTG_MJFKHM.Text.Trim();
                        break;
                    case "YJ_MJFK":
                        backCode = bank_YJ_MJFKYH.Tag as string;
                        bankNo = bank_YJ_MJFKKH.Text.Trim();
                        accountName = bank_YJ_MJFKHM.Text.Trim();
                        break;
                    case "YJ_YZFK":
                        backCode = bank_YJ_YZFKYH.Tag as string;
                        bankNo = bank_YJ_YZFKKH.Text.Trim();
                        accountName = bank_YJ_YZFKHM.Text.Trim();
                        break;
                }
                var authenticationDialog = new BankAuthenticationDialog(caseId, accountName, backCode, bankNo);
                authenticationDialog.Tag = tag;
                authenticationDialog.OnCompleted += authenticationDialog_OnCompleted;
                authenticationDialog.Show();
            }
            catch (Exception)
            {
                MessageBox.Show("鉴权发生系统异常", "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 完成鉴权处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void authenticationDialog_OnCompleted(object sender, BankAuthenticationDialog.AuthenticationEventArgs e)
        {
            var dialog = sender as BankAuthenticationDialog;
            var tag = Convert.ToString(dialog.Tag);
            switch (tag)
            {
                case "ZJTG_YZFK":
                    bank_ZJTG_YZFKKH.Tag = e.OperationResult.Success;
                    if (e.OperationResult.Success)
                    {
                        PageHelper.PageControlToDisable(W_ZJTG_YZFK, new System.Windows.Controls.Control[] { this.Modify_ZJTG_YZFK });
                    }
                    break;
                case "ZJTG_MJFK":
                    bank_ZJTG_MJFKKH.Tag = e.OperationResult.Success;
                    if (e.OperationResult.Success)
                    {
                        PageHelper.PageControlToDisable(W_ZJTG_MJFK, new System.Windows.Controls.Control[] { this.Modify_ZJTG_MJFK });
                    }
                    break;
                case "YJ_MJFK":
                    bank_YJ_MJFKKH.Tag = e.OperationResult.Success;
                    if (e.OperationResult.Success)
                    {
                        PageHelper.PageControlToDisable(W_YJ_MJFK, new System.Windows.Controls.Control[] { this.Modify_YJ_MJFK });
                    }
                    break;
                case "YJ_YZFK":
                    bank_YJ_YZFKKH.Tag = e.OperationResult.Success;
                    if (e.OperationResult.Success)
                    {
                        PageHelper.PageControlToDisable(W_YJ_YZFK, new System.Windows.Controls.Control[] { this.Modify_YJ_YZFK });
                    }
                    break;
            }
        }

        /// <summary>
        /// 修改账户鉴权
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnModifyBankNo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var modify = sender as Button;
                var tag = Convert.ToString(modify.Tag);
                switch (tag)
                {
                    case "ZJTG_YZSK":
                        bank_ZJTG_YZSKKH.Tag = null;
                        PageHelper.PageControlToActivate(W_ZJTG_YZSK);
                        break;
                    case "ZJTG_YZFK":
                        bank_ZJTG_YZFKKH.Tag = null;
                        PageHelper.PageControlToActivate(W_ZJTG_YZFK);
                        break;
                    case "ZJTG_MJFK":
                        bank_ZJTG_MJFKKH.Tag = null;
                        PageHelper.PageControlToActivate(W_ZJTG_MJFK);
                        break;
                    case "YJ_MJFK":
                        bank_YJ_MJFKKH.Tag = null;
                        PageHelper.PageControlToActivate(W_YJ_MJFK);
                        break;
                    case "YJ_YZFK":
                        bank_YJ_YZFKKH.Tag = null;
                        PageHelper.PageControlToActivate(W_YJ_YZFK);
                        break;
                }
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// 焦点移开事件处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bank_ZJTG_YZFKKH_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                var textBox = sender as TextBox;
                switch (textBox.Name)
                {

                }
            }
            catch (Exception)
            {

            }
        }

        private void txtSelectBank_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                var txt = sender as TextBox;
                var bankDialog = new BanksDialog(Convert.ToString(txt.Name));
                bankDialog.SelectBankEvent += bankDialog_SelectBankEvent;
                bankDialog.ClearBankEvent += bankDialog_ClearBankEvent;
                bankDialog.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("选择银行操作发生系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        void bankDialog_ClearBankEvent(string tag)
        {
            switch (tag)
            {
                case "bank_ZJTG_YZSKYH":
                    bank_ZJTG_YZSKYH.Text = "";
                    bank_ZJTG_YZSKZH.Text = "";
                    bank_ZJTG_YZSKKH.Text = "";
                    bank_ZJTG_YZSKHM.Text = "";
                    //bank_ZJTG_YZSKYH.IsEnabled = true;
                    break;
                case "bank_ZJTG_YZFKYH":
                    bank_ZJTG_YZFKYH.Text = "";
                    bank_ZJTG_YZFKZH.Text = "";
                    bank_ZJTG_YZFKKH.Text = "";
                    bank_ZJTG_YZFKHM.Text = "";
                    //bank_ZJTG_YZFKZH.IsEnabled = true;
                    break;
                case "bank_ZJTG_MJFKYH":
                    bank_ZJTG_MJFKYH.Text = "";
                    bank_ZJTG_MJFKZH.Text = "";
                    bank_ZJTG_MJFKKH.Text = "";
                    bank_ZJTG_MJFKHM.Text = "";
                    //bank_ZJTG_MJFKZH.IsEnabled = true;
                    break;
                case "bank_YJ_MJFKYH":
                    bank_YJ_MJFKYH.Text = "";
                    bank_YJ_MJFKZH.Text = "";
                    bank_YJ_MJFKKH.Text = "";
                    bank_YJ_MJFKHM.Text = "";
                    //bank_YJ_MJFKZH.IsEnabled = true;
                    break;
                case "bank_YJ_YZFKYH":
                    bank_YJ_YZFKYH.Text = "";
                    bank_YJ_YZFKZH.Text = "";
                    bank_YJ_YZFKKH.Text = "";
                    bank_YJ_YZFKHM.Text = "";
                    //bank_YJ_YZFKZH.IsEnabled = true;
                    break;
            }
        }

        /// <summary>
        /// 选择银行
        /// </summary>
        /// <param name="bankCode"></param>
        /// <param name="bankName"></param>
        private void bankDialog_SelectBankEvent(string bankCode, string bankName, string tag)
        {
            switch (tag)
            {
                case "bank_ZJTG_YZSKYH":
                    bank_ZJTG_YZSKYH.Text = bankName;
                    bank_ZJTG_YZSKYH.Tag = bankCode;
                    bank_ZJTG_YZSKZH.Focusable = true;
                    txt_TGZJZH.Focus();
                    break;
                case "bank_ZJTG_YZFKYH":
                    bank_ZJTG_YZFKYH.Text = bankName;
                    bank_ZJTG_YZFKYH.Tag = bankCode;
                    bank_ZJTG_YZFKZH.Focus();
                    bank_ZJTG_YZFKZH.IsEnabled = true;
                    break;
                case "bank_ZJTG_MJFKYH":
                    bank_ZJTG_MJFKYH.Text = bankName;
                    bank_ZJTG_MJFKYH.Tag = bankCode;
                    bank_ZJTG_MJFKZH.Focus();
                    bank_ZJTG_MJFKZH.IsEnabled = true;
                    break;
                case "bank_YJ_MJFKYH":
                    bank_YJ_MJFKYH.Text = bankName;
                    bank_YJ_MJFKYH.Tag = bankCode;
                    bank_YJ_MJFKZH.Focus();
                    bank_YJ_MJFKZH.IsEnabled = true;
                    break;
                case "bank_YJ_YZFKYH":
                    bank_YJ_YZFKYH.Text = bankName;
                    bank_YJ_YZFKYH.Tag = bankCode;
                    bank_YJ_YZFKZH.Focus();
                    bank_YJ_YZFKZH.IsEnabled = true;
                    break;
            }
        }

        private void cb_TGZJ_Click(object sender, RoutedEventArgs e)
        {
            if (cb_TGZJ.IsChecked == null) { return; }
            if (cb_TGZJ.IsChecked.Value == false)
            {
                bank_ZJTG_YZSKYH.Text = "";
                bank_ZJTG_YZSKZH.Text = "";
                bank_ZJTG_YZSKKH.Text = "";
                bank_ZJTG_YZSKHM.Text = "";

                bank_ZJTG_YZFKYH.Text = "";
                bank_ZJTG_YZFKZH.Text = "";
                bank_ZJTG_YZFKKH.Text = "";
                bank_ZJTG_YZFKHM.Text = "";

                bank_ZJTG_MJFKYH.Text = "";
                bank_ZJTG_MJFKZH.Text = "";
                bank_ZJTG_MJFKKH.Text = "";
                bank_ZJTG_MJFKHM.Text = "";

            }
        }

        private void cb_YJTG_Click(object sender, RoutedEventArgs e)
        {
            if (cb_YJTG.IsChecked == null) { return; }
            if (cb_YJTG.IsChecked.Value == false)
            {
                bank_YJ_MJFKYH.Text = "";
                bank_YJ_MJFKZH.Text = "";
                bank_YJ_MJFKKH.Text = "";
                bank_YJ_MJFKHM.Text = "";

                bank_YJ_YZFKYH.Text = "";
                bank_YJ_YZFKZH.Text = "";
                bank_YJ_YZFKKH.Text = "";
                bank_YJ_YZFKHM.Text = "";

            }
        }

    }
}
