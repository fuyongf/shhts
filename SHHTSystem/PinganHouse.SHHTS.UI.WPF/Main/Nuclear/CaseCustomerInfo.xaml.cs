﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Main.Dialog;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;


namespace PinganHouse.SHHTS.UI.WPF.Main.Nuclear
{
    /// <summary>
    /// CaseCustomer.xaml 的交互逻辑
    /// </summary>
    public partial class CaseCustomerInfo : Page
    {
        /// <summary>
        /// 案件数据
        /// </summary>
        private CaseDto m_CaseInfo =null;
        private string bankCode = null;
        //上下家信息
        Seller seller = null;
        Buyer buyer = null;

        /// <summary>
        /// 页面状态 -- 核案过来的状态
        /// </summary>
        CaseStatus m_Status = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.HA3
                | CaseStatus.HA4 | CaseStatus.HA5 | CaseStatus.HA6 | CaseStatus.ZB2;

        public CaseCustomerInfo(string caseId)
        {
            InitializeComponent();
            InitComboboxData();
            InitCaseData(caseId);
        }

        /// <summary>
        /// 初始化数据
        /// </summary>
        /// <param name="caseId"></param>
        private void  InitCaseData(string caseId)
        {
            try
            {               
                //案件对象
                 m_CaseInfo= CaseProxyService.GetInstanse().GetCaseByCaseIdAndCaseStatus(caseId,m_Status);
                //获取客户信息
                 Tuple<Seller, Buyer> sb =  CaseProxyService.GetInstanse().GetCaseCustomers(caseId);
                 if(sb!=null)seller = sb.Item1;
                 if(sb!=null)buyer = sb.Item2;
                 //判断是否有贷款
                 var evensResult = CaseProxyService.GetInstanse().GetCaseEvents(caseId, CaseStatus.DK1, CaseStatus.HA2);
                 if (evensResult != null)
                 {
                     foreach (var even in evensResult)
                     {
                         if (even.CaseId != caseId || even.EventType != CaseStatus.DK1)
                         {
                             continue;
                         }
                         cb_ZBDK.IsEnabled = false;
                         break;
                     }
                 }
                 if (m_CaseInfo.CaseStatus == CaseStatus.HA5)
                 {
                     //设置控件活性非活性
                     PageHelper.PageControlToDisable(Grid_Body, new System.Windows.Controls.Control[] { btnCancel, btnNext, btnPre });
                 }
            }
            catch (Exception ex) {
                MessageBox.Show("加载数据发生错误："+ ex.Message,"系统提示",MessageBoxButton.OK,MessageBoxImage.Error);
            }
        }

        private void RefreshBuyersInfo()
        {
            BuyersInfoStackPanel.Children.Clear();
            if (buyer == null)
            {
                return;
            }

            BuyerPersonalRadioButton.IsChecked = buyer.CustomerNature == CustomerNature.Personal;
            BuyerCompanylRadioButton.IsChecked = buyer.CustomerNature == CustomerNature.Corporation;
            if (buyer.CustomerNature != CustomerNature.Personal && buyer.CustomerNature != CustomerNature.Corporation)
                BuyerPersonalRadioButton.IsChecked = true;
            cb_XJYJDZ.SelectedIndex = (int)buyer.AddressType;
            if (!string.IsNullOrEmpty(buyer.PostProvince)) cb_XJ_Province.Text = buyer.PostProvince;
            if (!string.IsNullOrEmpty(buyer.PostCity)) cb_XJ_City.Text = buyer.PostCity;
            if (!string.IsNullOrEmpty(buyer.PostArea)) cb_XJ_District.Text = buyer.PostArea;
            txt_XJ_Address.Text = buyer.PostAddress;
            txt_XJ_SJR.Text = buyer.Recipients;
            txt_XJ_SJHM.Text = buyer.RecipientsMobile;
            cb_ZBDK.Text = buyer.SelfCredit == true ? "是" : "否";
            rb_QT.IsChecked = buyer.SelfCreditReson == SelfCreditReson.Other;
            rb_ZYQD.IsChecked = buyer.SelfCreditReson == SelfCreditReson.SelfChannel;
            rb_ZJZD.IsChecked = buyer.SelfCreditReson == SelfCreditReson.AgentRecommend;


            int lineNum = 0;
            foreach (Customer customer in buyer.Customers)
            {
                    lineNum++;
                    StackPanel sp = new StackPanel();
                    sp.Margin = new Thickness(0, 2, 0, 0);
                    sp.Orientation = Orientation.Horizontal;

                    Button btn = new Button();
                    btn.Focusable = false;
                    btn.Style = (Style)FindResource("NoMouseOverButtonStyle");
                    btn.Width = 21;
                    btn.Height = 21;
                    btn.BorderThickness = new Thickness(0);
                    btn.CommandParameter = customer.UserSysNo;
                    btn.Click += this.weichatBtnClicked;
                    ImageBrush berriesBrush = new ImageBrush();
                    berriesBrush.ImageSource =
                        new BitmapImage(
                            new Uri("Images/weichatico1.png", UriKind.Relative)
                        );
                    btn.Background = berriesBrush;
                    sp.Children.Add(btn);
                  
                    TextBlock tb = new TextBlock();
                    tb.Text = lineNum + ". " + customer.RealName + " | " +
                        EnumHelper.GetEnumDesc(customer.Gender) + " | " + (EnumHelper.GetEnumDesc(customer.CertificateType)) + " | 证件号码：" +
                        customer.IdentityNo + (customer.CertificateType == CertificateType.IdCard ? (" | 住址：" + customer.Nationality) : (" | 国籍：" + customer.Nationality)) + " | 手机：";
                    sp.Children.Add(tb);
                    TextBox tBox = new TextBox();
                    tBox.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                    tBox.Width = 100;
                    tBox.Text = customer.Mobile;
                    tBox.Tag = customer.SysNo;
                    sp.Children.Add(tBox);
                    //邮箱:
                    TextBlock tb2 = new TextBlock();
                    tb2.Text = " | 邮箱：";
                    sp.Children.Add(tb2);
                    TextBox tBox1 = new TextBox();
                    tBox1.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                    tBox1.Width = 200;
                    tBox1.Text = customer.Email;//邮箱
                    tBox1.Tag = customer.SysNo;
                    sp.Children.Add(tBox1);
                    BuyersInfoStackPanel.Children.Add(sp);
                
            }
        }
        private void RefreshSellersInfo()
        {
            SellersInfoStackPanel.Children.Clear();
            if (seller == null)
            {
                return;
            } 
            SellerPersonalRadioButton.IsChecked = seller.CustomerNature == CustomerNature.Personal;
            SellerCompnayRadioButton.IsChecked = seller.CustomerNature == CustomerNature.Corporation;
            if (seller.CustomerNature != CustomerNature.Personal && seller.CustomerNature != CustomerNature.Corporation)
                    SellerPersonalRadioButton.IsChecked = true;
            cb_SJYJDZ.SelectedIndex = (int)seller.AddressType;
            if (!string.IsNullOrEmpty(seller.PostProvince)) cb_SJ_Province.Text = seller.PostProvince;
            if (!string.IsNullOrEmpty(seller.PostCity)) cb_SJ_City.Text = seller.PostCity;
            if (!string.IsNullOrEmpty(seller.PostArea)) cb_SJ_District.Text = seller.PostArea;
            txt_SJ_Address.Text = seller.PostAddress;
            txt_SJ_SJR.Text = seller.Recipients;
            txt_SJ_SJHM.Text = seller.RecipientsMobile;
            cb_SFDY.Text = seller.IsGuaranty == true ? "是" : "否";
            bankCode = seller.RepayCreditBank;
            var banks = ConfigServiceProxy.GetBanks();
            foreach (var item in banks)
            {
                var bank = new Bank();
                bank.BankCode = Convert.ToString(((System.Collections.DictionaryEntry)(item)).Key);
                if (bankCode == bank.BankCode)
                {
                    TBRepayLoadBank.Text = Convert.ToString(((System.Collections.DictionaryEntry)(item)).Value);
                    break;
                }
            }
            
            TBRepayLoadMoney.Text = seller.RepayCreditAmount.ToString();
            int lineNum = 0;
            foreach (Customer customer in seller.Customers)
            {
                    lineNum++;
                    StackPanel sp = new StackPanel();
                    sp.Margin = new Thickness(0, 2, 0, 0);
                    sp.Orientation = Orientation.Horizontal;
              
                    Button btn = new Button();
                    btn.Focusable = false;
                    btn.Style = (Style)FindResource("NoMouseOverButtonStyle");
                    btn.Width = 21;
                    btn.Height = 21;
                    btn.BorderThickness = new Thickness(0);
                    btn.CommandParameter = customer.UserSysNo;
                    btn.Click += this.weichatBtnClicked;
                    ImageBrush berriesBrush = new ImageBrush();
                    berriesBrush.ImageSource =
                        new BitmapImage(
                            new Uri("Images/weichatico1.png", UriKind.Relative)
                        );
                    btn.Background = berriesBrush;
                    sp.Children.Add(btn);

                    TextBlock tb = new TextBlock();
                    tb.Text = lineNum + ". " + customer.RealName + " | " +
                        EnumHelper.GetEnumDesc(customer.Gender) + " | " + (EnumHelper.GetEnumDesc(customer.CertificateType)) + " | 证件号码：" +
                        customer.IdentityNo + (customer.CertificateType == CertificateType.IdCard ? (" | 住址：" + customer.Nationality) : (" | 国籍：" + customer.Nationality)) + " | 手机：";
                    sp.Children.Add(tb);
                    TextBox tBox = new TextBox();
                    tBox.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                    tBox.Width = 100;
                    tBox.Text = customer.Mobile;
                    tBox.Tag = customer.SysNo;
                    sp.Children.Add(tBox);
                    //邮箱:
                    TextBlock tb2 = new TextBlock();
                    tb2.Text = " | 邮箱：";
                    sp.Children.Add(tb2);
                    TextBox tBox1 = new TextBox();
                    tBox1.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                    tBox1.Width = 200;
                    tBox1.Text = customer.Email;//邮箱
                    tBox1.Tag = customer.SysNo;
                    sp.Children.Add(tBox1);

                    SellersInfoStackPanel.Children.Add(sp);  
                
            }
           
        }

        private void weichatBtnClicked(object sender, RoutedEventArgs e)
        {
            string userSysno = ((Button) sender).CommandParameter.ToString();
            new ScanWeiChatDialog(userSysno).ShowDialog();
        }


        /// <summary>
        /// 初始化页面Combox信息
        /// </summary>
        private void InitComboboxData()
        {
            try
            {
                //是否枚举
                var allege = EnumHelper.InitAllegeToCombobox();
                //allege.Insert(0, new EnumHelper { DisplayMember = "请选择", ValueMember = -1 });

                cb_ZBDK.ItemsSource = allege;
                cb_ZBDK.DisplayMemberPath = "DisplayMember";
                cb_ZBDK.SelectedValuePath = "ValueMember";
                cb_ZBDK.SelectedIndex = 1;

                cb_SFDY.ItemsSource = allege;
                cb_SFDY.DisplayMemberPath = "DisplayMember";
                cb_SFDY.SelectedValuePath = "ValueMember";
                cb_SFDY.SelectedIndex = 1;

                //邮件地址类型
                var addressType = EnumHelper.InitAddressTypeToCombobox();
                //addressType.Insert(0, new EnumHelper { DisplayMember = "请选择", ValueMember = -1 });

                cb_XJYJDZ.ItemsSource = addressType;
                cb_XJYJDZ.DisplayMemberPath = "DisplayMember";
                cb_XJYJDZ.SelectedValuePath = "ValueMember";
                cb_XJYJDZ.SelectedIndex = 1;

                cb_SJYJDZ.ItemsSource = addressType;
                cb_SJYJDZ.DisplayMemberPath = "DisplayMember";
                cb_SJYJDZ.SelectedValuePath = "ValueMember";
                cb_SJYJDZ.SelectedIndex = 1;

                var provinceList = ConfigServiceProxy.GetRegionInfo(null);
                cb_XJ_Province.ItemsSource = provinceList;
                cb_XJ_Province.DisplayMemberPath = null;
                cb_XJ_Province.SelectedValuePath = null;
                cb_XJ_Province.Text = "上海市";

                cb_SJ_Province.ItemsSource = provinceList;
                cb_SJ_Province.DisplayMemberPath = null;
                cb_SJ_Province.SelectedValuePath = null;
                cb_SJ_Province.Text = "上海市";
            }
            catch (Exception e)
            {
                MessageBox.Show("初始化页面基础数据错误："+e.Message,"系统提示",MessageBoxButton.OK,MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, "Main/Nuclear/CheckCaseSearchPage.xaml");
        }

        /// <summary>
        /// 保存并下一步
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (m_CaseInfo.CaseStatus == CaseStatus.HA5) {
                    JumpToNextPage();
                }
                if (!ValidateInput()) return;

                var saveBuyerResult = SaveBuyerInfo();
                var saveSellerResult = SaveSellerInfo();
                if (saveBuyerResult.Success && saveBuyerResult.Success)
                {
                    if (m_CaseInfo.CaseStatus == CaseStatus.HA2 
                        || m_CaseInfo.CaseStatus == CaseStatus.HA6)
                    {
                        HA3_CaseStatus(m_CaseInfo.CaseId);
                    }
                    JumpToNextPage();
                }
            }
            catch (Exception ex) 
            {
                MessageBox.Show("保存数据失败：" + ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }



        /// <summary>
        /// 跳转下一个页面 --上下家通讯信息
        /// </summary>
        private void JumpToNextPage()
        {
            var caseTransactionInfo = new CaseTransactionInfo(m_CaseInfo.CaseId);
            PageHelper.PageNavigateHelper(this, caseTransactionInfo);
        }

        /// <summary>
        /// 新建报告--报告填写中
        /// </summary>
        /// <param name="caseId">案件编号</param>
        private void HA3_CaseStatus(string caseId)
        {
            try
            {
                CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.HA3, LoginHelper.CurrentUser.SysNo);
            }
            catch (Exception)
            {
                // To do 暂时不做异常处理;
            }
        }


        private void btnPre_Click(object sender, RoutedEventArgs e)
        {
            var checkCaseBasicInfo = new CaseBasicInfo(m_CaseInfo.CaseId);
            PageHelper.PageNavigateHelper(this, checkCaseBasicInfo);
        }
        private bool ValidateInput()
        {
            int phoneNumCount = 0;
            int emailNumCount = 0;
            int phoneOrEmail = 0;
            //验证所有电话号码输入：
            foreach (StackPanel sp in BuyersInfoStackPanel.Children)
            {
                phoneOrEmail = 0;
                foreach (UIElement e in sp.Children)
                {
                    if (e is TextBox)
                    {
                        TextBox tb = (TextBox)e;
                        if (phoneOrEmail==0 && !Utils.PhoneNumValidate(tb.Text)|| phoneOrEmail==1&&!Utils.EmailValidate(tb.Text))
                        {
                            MessageBox.Show("买方"+(phoneOrEmail==0?"手机号":"邮箱")+"输入非法", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                            return false;
                        }
                        else if (!string.IsNullOrEmpty(tb.Text)) 
                            if(phoneOrEmail==0)phoneNumCount++;else emailNumCount++;
                        phoneOrEmail++;
                    }
                }
            }
            if (phoneNumCount == 0)
            {
                MessageBox.Show("请至少输入一个买方" + (phoneNumCount == 0 ? "手机号" : "") , "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            phoneNumCount = 0;
            emailNumCount = 0;
            //验证所有电话号码输入：
            foreach (StackPanel sp in SellersInfoStackPanel.Children)
            {
                phoneOrEmail = 0;
                foreach (UIElement e in sp.Children)
                {
                    if (e is TextBox)
                    {
                        TextBox tb = (TextBox)e;
                        if (phoneOrEmail == 0 && !Utils.PhoneNumValidate(tb.Text) || phoneOrEmail == 1 && !Utils.EmailValidate(tb.Text))
                        {
                            MessageBox.Show("卖方" + (phoneOrEmail == 0 ? "手机号" : "邮箱") + "输入非法", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                            return false;
                        }
                        else if (!string.IsNullOrEmpty(tb.Text))
                            if (phoneOrEmail == 0) phoneNumCount++; else emailNumCount++;
                        phoneOrEmail++;
                    
                    }
                }
            }
            if (phoneNumCount == 0)
            {
                MessageBox.Show("请至少输入一个卖方" + (phoneNumCount == 0 ? "手机号" : "") , "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            if (string.IsNullOrEmpty(cb_XJYJDZ.Text))
            {
                MessageBox.Show("请选择买方邮件地址类型", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            if (string.IsNullOrEmpty(cb_XJ_Province.Text) || string.IsNullOrEmpty(cb_XJ_City.Text)||string.IsNullOrEmpty(cb_XJ_District.Text)||string.IsNullOrEmpty(txt_XJ_Address.Text))
            {
                MessageBox.Show("请输入买方邮寄地址", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            if (string.IsNullOrEmpty(txt_XJ_SJR.Text))
            {
                MessageBox.Show("请输入买方收件人", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            if (string.IsNullOrEmpty(txt_XJ_SJHM.Text))
            {
                MessageBox.Show("请输入买方邮寄联系手机号", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            if (!Utils.PhoneNumValidate(txt_XJ_SJHM.Text))
            {
                MessageBox.Show("买方邮寄联系手机号输入非法", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            if (string.IsNullOrEmpty(cb_SJYJDZ.Text))
            {
                MessageBox.Show("请选择卖方邮件地址类型", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            if (string.IsNullOrEmpty(cb_SJ_Province.Text) || string.IsNullOrEmpty(cb_SJ_City.Text) || string.IsNullOrEmpty(cb_SJ_District.Text) || string.IsNullOrEmpty(txt_SJ_Address.Text))
            {
                MessageBox.Show("请输入卖方邮寄地址", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            if (string.IsNullOrEmpty(txt_SJ_SJR.Text))
            {
                MessageBox.Show("请输入卖方收件人", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            if (string.IsNullOrEmpty(txt_SJ_SJHM.Text))
            {
                MessageBox.Show("请输入卖方邮寄联系手机号", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            if (!Utils.PhoneNumValidate(txt_SJ_SJHM.Text))
            {
                MessageBox.Show("卖方邮寄联系手机号输入非法", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            if (cb_ZBDK.Text == "是" && rb_ZYQD.IsChecked == false && rb_ZJZD.IsChecked == false && rb_QT.IsChecked == false)
            {
                MessageBox.Show("请选择买方办理贷款方式", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            if (cb_SFDY.Text == "是")
            {
                if (string.IsNullOrEmpty(bankCode))
                {
                    MessageBox.Show("请选择卖方还款银行", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                    return false;
                }

                if (!string.IsNullOrEmpty(TBRepayLoadMoney.Text))
                {
                    decimal d = 0;
                    if (!decimal.TryParse(TBRepayLoadMoney.Text.ToString(), out d))
                    {
                        MessageBox.Show("还款金额格式错误", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                        return false;
                    }
                }
                else
                {
                    MessageBox.Show("请填写卖家还款金额", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                    return false;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(TBRepayLoadMoney.Text))
                {
                    decimal d = 0;
                    if (!decimal.TryParse(TBRepayLoadMoney.Text.ToString(), out d))
                    {
                        MessageBox.Show("还款金额格式错误", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                        return false;
                    }
                }
            }

         

            return true;
        }
        /// <summary>
        /// 保存买方信息 -- 下家
        /// </summary>
        /// <returns></returns>
        private OperationResult SaveBuyerInfo()
        {
            try {
                Dictionary<long, string> moblies = new Dictionary<long, string>();
                Dictionary<long, string> emails = new Dictionary<long, string>();
                //获取所有的电话,或者邮箱格式
                int phoneOrEmail = 0;
                foreach (StackPanel sp in BuyersInfoStackPanel.Children)
                {
                    phoneOrEmail = 0;
                    foreach (UIElement e in sp.Children)
                    {
                        if (e is TextBox)
                        {
                            TextBox tb = (TextBox)e;
                            long no = long.Parse(tb.Tag.ToString());
                            if (phoneOrEmail == 0) moblies.Add(no, tb.Text);
                            else emails.Add(no, tb.Text);
                            phoneOrEmail++;
                        }
                    }
                }
                buyer.CustomerNature = BuyerPersonalRadioButton.IsChecked==true ? CustomerNature.Personal : CustomerNature.Corporation;
                buyer.AddressType = (AddressType)cb_XJYJDZ.SelectedIndex;
                buyer.PostProvince = cb_XJ_Province.Text;
                buyer.PostCity = cb_XJ_City.Text;
                buyer.PostArea = cb_XJ_District.Text;
                buyer.PostAddress = txt_XJ_Address.Text;
                buyer.Recipients = txt_XJ_SJR.Text;
                buyer.RecipientsMobile = txt_XJ_SJHM.Text;
                buyer.SelfCredit = cb_ZBDK.Text == "是";
                if (rb_QT.IsChecked == true) buyer.SelfCreditReson = SelfCreditReson.Other;
                if (rb_ZYQD.IsChecked == true) buyer.SelfCreditReson = SelfCreditReson.SelfChannel;
                if (rb_ZJZD.IsChecked == true) buyer.SelfCreditReson = SelfCreditReson.AgentRecommend;
                var result = CaseProxyService.GetInstanse().PerfectBuyer(m_CaseInfo.CaseId,buyer, moblies,emails, LoginHelper.CurrentUser.SysNo);
                if (!result.Success) { throw new Exception("买方信息保存失败："+result.ResultMessage);  }
                return result;
            }
            catch (Exception) {
                throw new Exception("买方信息保存发生异常.");
            }
        }

        /// <summary>
        /// 保存卖方信息 -- 上家
        /// </summary>
        /// <returns></returns>
        private OperationResult SaveSellerInfo()
        {
            try
            {
                Dictionary<long, string> moblies = new Dictionary<long, string>();
                Dictionary<long, string> emails = new Dictionary<long, string>();
                //获取所有的电话
                int phoneOrEmail = 0;
                foreach (StackPanel sp in SellersInfoStackPanel.Children)
                {
                    phoneOrEmail = 0;
                    foreach (UIElement e in sp.Children)
                    {
                        if (e is TextBox)
                        {
                            TextBox tb = (TextBox)e;
                            long no = long.Parse(tb.Tag.ToString());
                            if (phoneOrEmail == 0) moblies.Add(no, tb.Text);
                            else emails.Add(no, tb.Text);
                            phoneOrEmail++;
                        }
                    }
                }
                seller.CustomerNature = SellerPersonalRadioButton.IsChecked == true ? CustomerNature.Personal : CustomerNature.Corporation;
                seller.AddressType = (AddressType)cb_SJYJDZ.SelectedIndex;
                seller.PostProvince = cb_SJ_Province.Text;
                seller.PostCity = cb_SJ_City.Text;
                seller.PostArea = cb_SJ_District.Text;
                seller.PostAddress = txt_SJ_Address.Text;
                seller.Recipients = txt_SJ_SJR.Text;
                seller.RecipientsMobile = txt_SJ_SJHM.Text;
                seller.IsGuaranty = cb_SFDY.Text == "是";
                seller.RepayCreditBank = bankCode;
                if(!string.IsNullOrEmpty(TBRepayLoadMoney.Text))  seller.RepayCreditAmount = decimal.Parse(TBRepayLoadMoney.Text.ToString());
                var result = CaseProxyService.GetInstanse().PrefectSeller(m_CaseInfo.CaseId, seller, moblies, emails, LoginHelper.CurrentUser.SysNo);
                if (!result.Success) { throw new Exception("卖方信息保存失败："+result.ResultMessage); }
                return result;

            }
            catch (Exception)
            {
                throw new Exception("卖方信息保存发生异常.");
            }
        }


        /// <summary>
        /// 省选中
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cb_XJ_Province_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var province = cb_XJ_Province.SelectedValue;
            var cityList = ConfigServiceProxy.GetRegionInfo("/" + province);
            cb_XJ_City.ItemsSource = cityList;
            cb_XJ_City.DisplayMemberPath = null;
            cb_XJ_City.SelectedValuePath = null;
            cb_XJ_City.SelectedIndex = 0;

        }

        /// <summary>
        /// 城市选中
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cb_XJ_City_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var province = cb_XJ_Province.SelectedValue;
            var city = cb_XJ_City.SelectedValue;
            var districtList = ConfigServiceProxy.GetRegionInfo("/" + province + "/" + city);
            cb_XJ_District.ItemsSource = districtList;
            cb_XJ_District.DisplayMemberPath = null;
            cb_XJ_District.SelectedValuePath = null;
            cb_XJ_District.SelectedIndex = 0;
        }

        /// <summary>
        /// 省份选中
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cb_SJ_Province_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var province = cb_SJ_Province.SelectedValue;
            var cityList = ConfigServiceProxy.GetRegionInfo("/" + province);
            cb_SJ_City.ItemsSource = cityList;
            cb_SJ_City.DisplayMemberPath = null;
            cb_SJ_City.SelectedValuePath = null;
            cb_SJ_City.SelectedIndex = 0;
        }

        /// <summary>
        /// 城市选中
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cb_SJ_City_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var province = cb_SJ_Province.SelectedValue;
            var city = cb_SJ_City.SelectedValue;
            var districtList = ConfigServiceProxy.GetRegionInfo("/" + province + "/" + city);
            cb_SJ_District.ItemsSource = districtList;
            cb_SJ_District.DisplayMemberPath = null;
            cb_SJ_District.SelectedValuePath = null;
            cb_SJ_District.SelectedIndex = 0;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            RefreshBuyersInfo();
            RefreshSellersInfo();
        }

        private void cb_ZBDK_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            Visibility v = Visibility.Visible;
            EnumHelper eh = (EnumHelper) e.AddedItems[0];
            if(eh.DisplayMember=="否")v=Visibility.Hidden;
            textBox1.Visibility = v;
            rb_QT.Visibility = v;
            rb_ZJZD.Visibility = v;
            rb_ZYQD.Visibility = v;
        }
        private void bankDialog_SelectBankEvent(string bankCode, string bankName, string tag)
        {
            this.bankCode = bankCode;
            TBRepayLoadBank.Text = bankName;
        }

        private void TBRepayLoadBank_GotFocus(object sender, RoutedEventArgs e)
        {
            TBRepayLoadMoney.Focus();
            BanksDialog bankDialog = new BanksDialog("");
            bankDialog.SelectBankEvent += bankDialog_SelectBankEvent;
            bankDialog.ShowDialog();
        }

    }
}
