﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Nuclear
{
    /// <summary>
    /// CustomerInfo.xaml 的交互逻辑
    /// </summary>
    public partial class CustomerInfo : UserControl
    {
        /// <summary>
        /// 页面状态 -- 核案过来的状态
        /// </summary>
        const CaseStatus m_Status = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.HA3
                | CaseStatus.HA4 | CaseStatus.HA5 | CaseStatus.HA6 | CaseStatus.ZB2;

        public delegate void Handle(object sender, EventArgs e);

        /// <summary>
        /// 导航事件
        /// </summary>
        public Handle NavClickEvent;

        /// <summary>
        /// 构造函数
        /// </summary>
        public CustomerInfo()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 页面绑定数据
        /// </summary>
        /// <param name="customers"></param>
        public void InitCtrlData(Seller seller, Buyer buyer)
        {
            try
            {
                #region 绑定买方基础信息
                BuyersInfoStackPanel.Children.Clear();
                int lineNum = 0;
                if (buyer.Customers != null)
                {
                    foreach (Customer customer in buyer.Customers)
                    {
                        lineNum++;
                        StackPanel sp = new StackPanel();
                        sp.Margin = new Thickness(0, 2, 0, 0);
                        sp.Orientation = Orientation.Horizontal;
                        TextBlock tb = new TextBlock();
                        tb.Text = lineNum + ". " + customer.RealName + " | " +
                            EnumHelper.GetEnumDesc(customer.Gender) + " | " + (EnumHelper.GetEnumDesc(customer.CertificateType)) + " | 证件号码：" +
                            customer.IdentityNo + " | 户籍：" + (customer.CertificateType == CertificateType.IdCard ? customer.Nationality : customer.Nationality);
                        if(!string.IsNullOrEmpty(customer.Mobile)){
                            tb.Text +=" | 手机：" + customer.Mobile;
                        }
                        if(!string.IsNullOrEmpty(customer.Email)){
                             tb.Text +=" | 邮箱：" + customer.Email;
                        }
                        sp.Children.Add(tb);
                        BuyersInfoStackPanel.Children.Add(sp);
                    }
                }
                BuyerPersonalRadioButton.IsChecked = buyer.CustomerNature == CustomerNature.Personal;
                BuyerCompanylRadioButton.IsChecked = buyer.CustomerNature == CustomerNature.Corporation;
                if (buyer.CustomerNature != CustomerNature.Personal && buyer.CustomerNature != CustomerNature.Corporation)
                    BuyerPersonalRadioButton.IsChecked = true;
                var address = string.Empty;
                if (!string.IsNullOrEmpty(buyer.PostProvince)) address += buyer.PostProvince;
                if (!string.IsNullOrEmpty(buyer.PostCity)) address += buyer.PostCity;
                if (!string.IsNullOrEmpty(buyer.PostArea)) address += buyer.PostArea;
                address += buyer.PostAddress;
                txt_MJYJDZ.Text = address;
                txt_MJSJR.Text = buyer.Recipients;
                txt_MJSJSJ.Text = buyer.RecipientsMobile;
                txt_MJZBDK.Text = buyer.SelfCredit == true ? "是" : "否";
                txt_MJZBDKYY.Text = EnumHelper.GetEnumDesc(buyer.SelfCreditReson);
                if (buyer.SelfCredit==false || buyer.SelfCredit==null)
                {
                    lab_Reason.Visibility = Visibility.Collapsed;
                    txt_MJZBDKYY.Visibility = Visibility.Collapsed;
                }
                #endregion

                #region 绑定卖家基础信息
                SellersInfoStackPanel.Children.Clear();
                int lineNum_seller = 0;
                if (seller.Customers != null)
                {
                    foreach (Customer customer in seller.Customers)
                    {
                        lineNum_seller++;
                        StackPanel sp = new StackPanel();
                        sp.Margin = new Thickness(0, 2, 0, 0);
                        sp.Orientation = Orientation.Horizontal;
                        TextBlock tb = new TextBlock();
                        tb.Text = lineNum_seller + ". " + customer.RealName + " | " +
                            EnumHelper.GetEnumDesc(customer.Gender) + " | " + (EnumHelper.GetEnumDesc(customer.CertificateType)) + " | 证件号码：" +
                            customer.IdentityNo + " | 户籍：" + customer.Nationality;
                        if(!string.IsNullOrEmpty(customer.Mobile)){
                            tb.Text +=" | 手机：" + customer.Mobile;
                        }
                        if(!string.IsNullOrEmpty(customer.Email)){
                             tb.Text +=" | 邮箱：" + customer.Email;
                        }
                        sp.Children.Add(tb);
                        SellersInfoStackPanel.Children.Add(sp);
                    }
                }
                SellerPersonalRadioButton.IsChecked = seller.CustomerNature == CustomerNature.Personal;
                SellerCompnayRadioButton.IsChecked = seller.CustomerNature == CustomerNature.Corporation;
                if (seller.CustomerNature != CustomerNature.Personal && seller.CustomerNature != CustomerNature.Corporation)
                    SellerPersonalRadioButton.IsChecked = true;
                address = string.Empty;
                if (!string.IsNullOrEmpty(seller.PostProvince)) address += seller.PostProvince;
                if (!string.IsNullOrEmpty(seller.PostCity)) address += seller.PostCity;
                if (!string.IsNullOrEmpty(seller.PostArea)) address += seller.PostArea;
                address += seller.PostAddress;
                txt_YZYJDZ.Text = address;
                txt_YZSJR.Text = seller.Recipients;
                txt_YZSJSJ.Text = seller.RecipientsMobile;
                txt_SFDY.Text = seller.IsGuaranty == true ? "是" : "否";
                txt_BANK.Text = ConfigServiceProxy.GetBankNameByCode(seller.RepayCreditBank);
                txt_Money.Text =Convert.ToString(seller.RepayCreditAmount);
                if (seller.IsGuaranty == false || seller.IsGuaranty ==null)
                {
                    lab_Bank.Visibility = Visibility.Collapsed;
                    lab_Money.Visibility = Visibility.Collapsed;
                    txt_BANK.Visibility = Visibility.Collapsed;
                    txt_Money.Visibility = Visibility.Collapsed;
                }
                #endregion
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 导航
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (NavClickEvent != null)
            {
                NavClickEvent(sender, e);
            }
        }
    }
}
