﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;

using PinganHouse.SHHTS.AccessControl.Proxy;
 
 using PinganHouse.SHHTS.Enumerations;

using PinganHouse.SHHTS.UI.WPF.Main.Dialog;
//using PinganHouse.SHHTS.UI.WPF.Main.SettingPage;
using PinganHouse.SHHTS.UI.WPF.Main.Setting;
 using PinganHouse.SHHTS.UI.WPF.ReadCard;
using PinganHouse.SHHTS.UI.WPF.Main.Agent;
using PinganHouse.SHHTS.UI.WPF.Main;
using PinganHouse.SHHTS.UI.WPF.Main.AuditTax;
using PinganHouse.SHHTS.UI.WPF.Common;


namespace PinganHouse.SHHTS.UI.WPF
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    [SecurityAuthorize]
    public partial class MainWindow : Window
    {
        /// <summary>
        /// 是否跳转登录
        /// </summary>
        public bool _IsBackLogin = true;
        public MainWindow()
        {

            InitializeComponent();
            ShowTitle();
        }


        /// <summary>
        /// 是否返回首页
        /// </summary>
        /// <param name="isBackLogin"></param>
        public MainWindow(bool isBackLogin)
        {
            InitializeComponent();
            _IsBackLogin = isBackLogin;
        }

        //[Permission("10004")]
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var btnType = sender as Button; 
            if (btnType==null)
            {
                MessageBox.Show("很抱歉,系统发生异常.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string pageType = btnType.Tag.ToString();
            if (pageType.Equals("endBusibess") || pageType.Equals("ehousemoney") )
            {
                MessageBox.Show("很抱歉,此模块尚未启用.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            if (!CheckPermission(pageType))
            {
                MessageBox.Show("您不具备此操作权限.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            var mainDetails = new MainDetails(pageType) {WindowState = WindowState.Maximized};
            _IsBackLogin = false;
            mainDetails.Show();
            this.Close();
        }

        private void MenuButton_Click(object sender, RoutedEventArgs e)
        {
            var btnType = sender as MenuItem; 
            if (btnType==null)
            {
                MessageBox.Show("很抱歉,系统发生异常.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string pageType = btnType.Tag.ToString();
            if (pageType.Equals("endBusibess")||pageType.Equals("ehousemoney")) 
            { 
                MessageBox.Show("很抱歉,此模块尚未启用.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            if (!CheckPermission(pageType))
            {
                MessageBox.Show("您不具备此操作权限.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            var mainDetails = new MainDetails(pageType) {WindowState = WindowState.Maximized};
            _IsBackLogin = false;
            mainDetails.Show();
            this.Close();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //判断是否返回首页
            if (_IsBackLogin)
            {
                new PinganHouse.SHHTS.UI.WPF.Login.LoginPage().Show();
            }
        }

        //private void Button_Click_1(object sender, RoutedEventArgs e)
        //{
        //    new SettingPage().Show();
        //}


        [Conditional("DEBUG")]
        private void ShowTitle()
        {
            this.Title = string.Format("{0}(测试)", Utils.GetVersion());
        }
        #region 测试

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
          var staff=new StaffId();
          staff.Closing += staff_Closing;
          staff.ShowDialog();          
        }

        void staff_Closing(object sender, EventArgs e)
        {
            var staffId = ((StaffId) sender).StaffNo;
           Btn.Content = staffId;
        }
        #endregion


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
           
        }

        /// <summary>
        /// 更改密码
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [Permission("10032")]
        private void MenuItem_ChangePwd_Click(object sender, RoutedEventArgs e)
        {
            ChangePassword cp = new ChangePassword();
            cp.ShowDialog();
        }

        /// <summary>
        /// 设置打印机
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [Permission("10033")]
        private void MenuItem_SettingPrint_Click(object sender, RoutedEventArgs e)
        {
            SettingPage sp = new SettingPage();
            sp.ShowDialog();
        }

        private int count ;
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {           
            MoreShow.Visibility = count % 2 == 0 ? Visibility.Visible : Visibility.Collapsed;
            count++;
        }

        /// <summary>
        /// 采集员工卡号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [Permission("10034")]
        private void MenuItem__InputStaffId_Click(object sender, RoutedEventArgs e)
        {
            InputStaffId staffid = new InputStaffId();
            staffid.ShowDialog();
        }

        /// <summary>
        /// 添加中介公司
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [Permission("10036")]
        private void MenuItem_AgentCompany_Click(object sender, RoutedEventArgs e)
        {
            AgentCompany agentCompany = new AgentCompany();
            agentCompany.ShowDialog();
        }

        /// <summary>
        /// 添加中介员工
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [Permission("10037")]
        private void MenuItem_AgentStaff_Click(object sender, RoutedEventArgs e)
        {
            AgentStaff agentStaff = new AgentStaff();
            agentStaff.ShowDialog();
        }

        /// <summary>
        /// 关于
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [Permission("10038")]
        private void MenuItem_About_Click(object sender, RoutedEventArgs e)
        {
            AboutPage aboutPage = new AboutPage();
            aboutPage.ShowDialog();
        }

        #region 权限

        private bool CheckPermission(string tag) 
        {
           return PermissionProxyService.GetInstance().Authorize(GetButtonPermissionNo(tag));
        }

        private string GetButtonPermissionNo(string tag) 
        {
            if (string.IsNullOrEmpty(tag))
                throw new Exception("权限标签未设置");
            switch (tag) 
            {
                case "preview": return "10001";
                case "sign": return "10004";
                case "returnLoan": return "10005";
                case "loanAgent": return "10006";
                case "changeName": return "10007";
                case "endBusibess": return "10008";
                case "chanDiao": return "10009";
                case "hostTable": return "10012";
                case "moneysettle": return "10021";
                //case "ehousemoney": return "10027";
                case "taxCalculator": return "10028";
                case "limitCheck": return "10029";
                case "BankAuthentication": return "10030";
                default: throw new Exception("权限标签未设置");
            }
        }

        #endregion

    }
}
