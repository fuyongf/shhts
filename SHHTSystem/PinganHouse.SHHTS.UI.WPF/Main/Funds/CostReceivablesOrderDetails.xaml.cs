﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds
{
    /// <summary>
    /// CostReceivablesOrderDetails.xaml 的交互逻辑
    /// </summary>
    public partial class CostReceivablesOrderDetails : Page
    {
        private string _orderNo = string.Empty;
        private string _caseId = string.Empty;


        public CostReceivablesOrderDetails(string orderNo, string caseId)
        {
            InitializeComponent();
            _orderNo = orderNo;
            _caseId = caseId;

            try
            {
                var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(_caseId);

                var caseRemarkDto = CaseProxyService.GetInstanse().GetRemarkBySysNo(caseDto.SysNo);

                ctrlCaseInfo.InitCtrlData(caseDto, caseRemarkDto);

                var order = TradeServiceProxy.GetOrderInfo(_orderNo);
                ctrlOrderInfo.InitCtrlData(order);

                var account = AccountServiceProxy.Get(order.AccountSysNo);
                lblAccountType.Content = EnumHelper.GetEnumDesc(account.AccountType);
                lblOrderAmount.Content = order.OrderAmount.ToString("C");
                lblPaymentChannel.Content = EnumHelper.GetEnumDesc(order.PaymentChannel);
                if (order.OrderBizType == OrderBizType.HosingFund)
                {
                    lblOrderBizType.Content = "代收房款";
                }
                else if (order.OrderBizType == OrderBizType.Taxes)
                {
                    lblOrderBizType.Content = "代收税费";
                }
                else if (order.OrderBizType == OrderBizType.ServiceCharge)
                {
                    lblOrderBizType.Content = "代收佣金";
                }
                else if (order.OrderBizType == OrderBizType.Taxes)
                {
                    lblOrderBizType.Content = "代收服务费";
                }

                var tradeList = TradeServiceProxy.GetTradeByOrder(_orderNo);
                dgTradeList.DataContext = tradeList;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 关闭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, new FundSettlementListPage());
        }

        /// <summary>
        /// 打印票据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
