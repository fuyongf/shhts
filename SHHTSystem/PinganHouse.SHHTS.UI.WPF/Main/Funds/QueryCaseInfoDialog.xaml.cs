﻿using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds
{
    /// <summary>
    /// QueryCaseInfoDialog.xaml 的交互逻辑
    /// </summary>
    public partial class QueryCaseInfoDialog : Window
    {
        public delegate void SelectedHandle(string caseId);

        public event SelectedHandle SelectedEvent;

        private QueryStringType _queryStringType;

        public QueryCaseInfoDialog(string searchType, string searchKey)
        {
            InitializeComponent();

            _queryStringType = (QueryStringType)int.Parse(searchType);

            lbProperty.ItemsSource = CaseProxyService.GetInstanse().GetTenementContracts(_queryStringType, searchKey, ConfigHelper.GetCurrentReceptionCenter());

            if (_queryStringType == QueryStringType.TenementContract)
            {
                this.Title = "产证号查询";

                lblQueryType.Content = "1";

            }
            else if (_queryStringType == QueryStringType.TenementAddress)
            {
                this.Title = "物业地址查询";
                lblQueryType.Content = "2";
            }
        }


        private void LbProperty_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var chanQuanHao =((KeyValuePair<string, string>)(lbProperty.SelectedItem)).Key;

            if (SelectedEvent != null)
            {
                var caseRemark = CaseProxyService.GetInstanse().GetRemark(chanQuanHao, ConfigHelper.GetCurrentReceptionCenter());
                if (caseRemark != null)
                {
                    SelectedEvent(caseRemark.OtherData["CaseId"].ToString());
                }
                this.Close();
            }
        }
    }
}
