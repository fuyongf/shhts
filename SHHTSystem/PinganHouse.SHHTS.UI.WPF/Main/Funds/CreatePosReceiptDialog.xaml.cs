﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds
{
    /// <summary>
    /// CreatePosReceiptDialog.xaml 的交互逻辑
    /// </summary>
    public partial class CreatePosReceiptDialog : Window
    {
        private string _orderNo = string.Empty;

        public CreatePosReceiptDialog(string orderNo)
        {
            InitializeComponent();
            _orderNo = orderNo;
        }

        private void BtnSave_OnClick(object sender, RoutedEventArgs e)
        {
            var errorMessage = string.Empty;
            
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(@"^[0-9]\d*$");
            var terminalNo = txtTerminalNo.Text.Trim();
            if (string.IsNullOrEmpty(terminalNo))
            {
                errorMessage += "终端编号不能为空。\r\n";
            }
            else if (reg.IsMatch(terminalNo)==false)
            {
                errorMessage += "终端号必须是数字。\r\n";
            }
            var postSerialNo = txtPosSerialNo.Text.Trim();
            if (string.IsNullOrEmpty(postSerialNo))
            {
                errorMessage += "POS参考号不能为空。\r\n";
            }
            else if (reg.IsMatch(postSerialNo) == false)
            {
                errorMessage += "POS参考号必须是数字。\r\n";
            }
            var cardNo = txtCardNo.Text.Trim();
            if (string.IsNullOrEmpty(cardNo))
            {
                errorMessage += "银行卡号不能为空。\r\n";
            }
            var accountName = txtAccountName.Text.Trim();
            if (string.IsNullOrEmpty(accountName))
            {
                errorMessage += "开户名不能为空。\r\n";
            }
            decimal amount = 0;
            decimal.TryParse(txtAmount.Text.Trim(), out amount);
            if (amount <= 0)
            {
                errorMessage += "金额必须大于0。\r\n";
            }
            else
            {
                if (txtAmount.Text.Trim().IndexOf('.') > -1 && ((txtAmount.Text.Trim().Length - txtAmount.Text.Trim().IndexOf('.') - 1) > 2))
                {
                    errorMessage += "金额的小数位数不能大于两位。\r\n";
                }
            }
            var date = datePicker.SelectedDate;
            if (date == null)
            {
                errorMessage += "日期不能为空\r\n";
            }

            if (!String.IsNullOrEmpty(errorMessage))
            {
                MessageBox.Show(errorMessage, "警告", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            if (postSerialNo.Length != 12)
            {
                var btnResult = MessageBox.Show("POS参考号可能有误！是否继续推送到财务？", "警告", MessageBoxButton.OKCancel);
                if (btnResult == MessageBoxResult.Cancel)
                {
                    return;
                }
            }
            var result = TradeServiceProxy.SubmitPosTrade(
                _orderNo,
                txtPosSerialNo.Text,
                txtTerminalNo.Text,
                amount,
                txtCardNo.Text,
                "",
                txtAccountName.Text,
                "",
                (DateTime)date,
                "",
                LoginHelper.CurrentUser.SysNo
                );

            if (result.Success)
            {
                MessageBox.Show("POS回执单录入成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();
            }
            else
            {
                MessageBox.Show(result.ResultMessage);
            }
        }

        private void BtnCancel_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
