﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds.Ctrl
{
    /// <summary>
    /// TransactionInfoCtrl.xaml 的交互逻辑
    /// </summary>
    public partial class TransactionInfoCtrl : UserControl
    {
        public TransactionInfoCtrl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 加载数据
        /// </summary>
        /// <param name="customerFundFlowList"></param>
        internal void InitCtrlData(IList<DataTransferObjects.CustomerFundFlow> customerFundFlowList)
        {
            try
            {
                BindSellerGrid(customerFundFlowList);
                BindBuyerGrid(customerFundFlowList);
                SetAmount(customerFundFlowList);
            }
            catch (Exception) { }
        }


        /// <summary>
        /// 加载买方数据
        /// </summary>
        /// <param name="fundFlowList"></param>
        private void BindBuyerGrid(IList<CustomerFundFlow> fundFlowList)
        {
            var flowList = fundFlowList.Where(customerFundFlow => customerFundFlow.CustomerType == CustomerType.Buyer)
                   .OrderByDescending(customerFundFlow => customerFundFlow.CreateDate).ToList();

            #region 计算买方合计列
            decimal houseFundTotal_Buyer = 0;
            decimal taxesTotal_Buyer = 0;
            decimal brokerageTotal_Buyer = 0;
            decimal decorateCompensationTotal_Buyer = 0;
            decimal otherChargesTotal_Buyer = 0;
            decimal amountTotal_Buyer = 0;
            decimal accountBalanceTotal_Buyer = 0;
            foreach (CustomerFundFlow flow in flowList)
            {
                houseFundTotal_Buyer += flow.HouseFund;
                taxesTotal_Buyer += flow.Taxes;
                brokerageTotal_Buyer += flow.Brokerage;
                decorateCompensationTotal_Buyer += flow.DecorateCompensation;
                otherChargesTotal_Buyer += flow.OtherCharges;
                amountTotal_Buyer += flow.Amount;
            }
            CustomerFundFlow totalRow_Buyer = new CustomerFundFlow();
            totalRow_Buyer.ConditionType = "合计";
            totalRow_Buyer.HouseFund = houseFundTotal_Buyer;
            totalRow_Buyer.Taxes = taxesTotal_Buyer;
            totalRow_Buyer.Brokerage = brokerageTotal_Buyer;
            totalRow_Buyer.DecorateCompensation = decorateCompensationTotal_Buyer;
            totalRow_Buyer.OtherCharges = otherChargesTotal_Buyer;
            totalRow_Buyer.Amount = amountTotal_Buyer;
            totalRow_Buyer.AccountBalance = accountBalanceTotal_Buyer;
            totalRow_Buyer.CustomerType = CustomerType.Buyer;
            flowList.Add(totalRow_Buyer);
            dataGrid_Buyer.DataContext = flowList;
            #endregion
            dataGrid_Buyer.DataContext = flowList;
        }

        /// <summary>
        /// 加载卖家数据
        /// </summary>
        /// <param name="fundFlowList"></param>
        private void BindSellerGrid(IList<CustomerFundFlow> fundFlowList)
        {
            var flowList = fundFlowList.Where(customerFundFlow => customerFundFlow.CustomerType == CustomerType.Seller)
                    .OrderByDescending(customerFundFlow => customerFundFlow.CreateDate).ToList();
            #region 计算卖方合计列
            decimal houseFundTotal_Seller = 0;
            decimal taxesTotal_Seller = 0;
            decimal brokerageTotal_Seller = 0;
            decimal decorateCompensationTotal_Seller = 0;
            decimal otherChargesTotal_Seller = 0;
            decimal amountTotal_Seller = 0;
            decimal accountBalanceTotal_Seller = 0;

            foreach (CustomerFundFlow flow in flowList)
            {
                houseFundTotal_Seller += flow.HouseFund;
                taxesTotal_Seller += flow.Taxes;
                brokerageTotal_Seller += flow.Brokerage;
                decorateCompensationTotal_Seller += flow.DecorateCompensation;
                otherChargesTotal_Seller += flow.OtherCharges;
                amountTotal_Seller += flow.Amount;
            }
            CustomerFundFlow totalRow_Seller = new CustomerFundFlow();
            totalRow_Seller.ConditionType = "合计";
            totalRow_Seller.HouseFund = houseFundTotal_Seller;
            totalRow_Seller.Taxes = taxesTotal_Seller;
            totalRow_Seller.Brokerage = brokerageTotal_Seller;
            totalRow_Seller.DecorateCompensation = decorateCompensationTotal_Seller;
            totalRow_Seller.OtherCharges = otherChargesTotal_Seller;
            totalRow_Seller.Amount = amountTotal_Seller;
            totalRow_Seller.AccountBalance = accountBalanceTotal_Seller;
            totalRow_Seller.CustomerType = CustomerType.Seller;


            flowList.Add(totalRow_Seller);

            dataGrid_Seller.DataContext = flowList;

            #endregion

        }

        /// <summary>
        /// 统计总数
        /// </summary>
        /// <param name="flowList"></param>
        private void SetAmount(IList<CustomerFundFlow> flowList)
        {
            Decimal incomeAmount = 0;
            Decimal expenditureAmount = 0;
            Decimal incomeAmount_Seller = 0;
            Decimal expenditureAmount_Seller = 0;
            Decimal incomeAmount_Buyer = 0;
            Decimal expenditureAmount_Buyer = 0;


            foreach (var flow in flowList)
            {
                if (flow.Amount > 0)
                {
                    incomeAmount += flow.Amount;
                    if (flow.CustomerType == CustomerType.Seller)
                        incomeAmount_Seller += flow.Amount;
                    else if (flow.CustomerType == CustomerType.Buyer)
                        incomeAmount_Buyer += flow.Amount;
                }
                else
                {
                    expenditureAmount += flow.Amount;
                    if (flow.CustomerType == CustomerType.Seller)
                        expenditureAmount_Seller += flow.Amount;
                    else if (flow.CustomerType == CustomerType.Buyer)
                        expenditureAmount_Buyer += flow.Amount;
                }
            }

            lblIncomeAmount_Seller.Content = incomeAmount_Seller.ToString("C");
            lblExpenditureAmount_Seller.Content = expenditureAmount_Seller.ToString("C");

            lblIncomeAmount_Buyer.Content = incomeAmount_Buyer.ToString("C");
            lblExpenditureAmount_Buyer.Content = expenditureAmount_Buyer.ToString("C");
        }

        /// <summary>
        /// 账户剩余金额统计
        /// </summary>
        /// <param name="buyerAccount">买家账户</param>
        /// <param name="sellerAccount">卖家账户</param>
        internal void InitCtrlAccountData(Account buyerAccount, Account sellerAccount)
        {
            try {
                if (buyerAccount != null) { Amount_Buyer.Content = buyerAccount.Balance.ToString("C"); }
                if (sellerAccount != null) { Amount_Seller.Content = sellerAccount.Balance.ToString("C"); }
            }
            catch (Exception) { }
        }

        /// <summary>
        /// 设置实收实付
        /// </summary>
        /// <param name="tradeAmountData"></param>
        internal void InitTradeAmountData(Tuple<IDictionary<OrderType, decimal>, Dictionary<OrderType, decimal>> tradeAmountData)
        {
            try
            {
                //卖方
                var sellerAccount = tradeAmountData.Item1;
                incomeAmount_Seller.Content = sellerAccount[OrderType.Collection].ToString("C");
                expenditureAmount_Seller.Content = sellerAccount[OrderType.Payment].ToString("C");
                //买方
                var buyerAccount = tradeAmountData.Item2;
                incomeAmount_Buyer.Content = buyerAccount[OrderType.Collection].ToString("C");
                expenditureAmount_Buyer.Content = buyerAccount[OrderType.Payment].ToString("C"); ;
            }
            catch (Exception) { }
        }

    }
}
