﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds.Ctrl
{
    /// <summary>
    /// RefundInfoCtrl.xaml 的交互逻辑
    /// </summary>
    public partial class RefundInfoCtrl : UserControl
    {
        /// <summary>
        /// 退款订单明细
        /// </summary>
        public RefundInfoCtrl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 初始化订单
        /// </summary>
        /// <param name="account">账户信息</param>
        internal void InitCtrlAccountData(Account account)
        {
            try
            {
                //账户信息
                if (account == null) return;
                txtAccountType.Text = EnumHelper.GetEnumDesc(account.AccountType);
            }
            catch (Exception) { }
        }

        /// <summary>
        /// 加载订单数据
        /// </summary>
        /// <param name="orderInfo"></param>
        internal void InitCtrlOrderData(Order orderInfo)
        {
            if (orderInfo == null) return;
            this.Dispatcher.Invoke(new Action(
             delegate
             {
                 //申请人
                 txt_SQR.Text = Convert.ToString(UserServiceProxy.GetUserName(Convert.ToInt64(orderInfo.CreateUserSysNo)));
                 //申请时间
                 txt_Time.Text = orderInfo.CreateDate.ToShortDateString();
                 //订单编号
                 txtOrderNo.Text = orderInfo.OrderNo;
                 //订单类型
                 txtOrderType.Text = EnumHelper.GetEnumDesc(orderInfo.OrderType);
                 //订单金额
                 txtAccount.Text = Convert.ToString(orderInfo.OrderAmount);
                 //资金状态
                 txtOrderStatus.Text = EnumHelper.GetEnumDesc(orderInfo.OrderStatus);
                 //付款性质
                 txtOrderBiz.Text = EnumHelper.GetEnumDesc(orderInfo.OrderBizType);
             }));
        }

        /// <summary>
        /// 加载订单数据
        /// </summary>
        /// <param name="orderInfo"></param>
        internal void InitCtrlTradeDetailData(IList<TradeDetail> tradeDetailList)
        {
            if (tradeDetailList == null) return;
            this.Dispatcher.Invoke(new Action(
             delegate
             {
                 //付款人
                 if (tradeDetailList != null)
                 {
                     var orderTrade = from query in tradeDetailList
                                      where query.Amount > 0
                                      select query;
                     dataGrid.DataContext = orderTrade;
                     var refundTrade = from query in tradeDetailList
                                       where query.Amount < 0
                                       select query;
                     dataGridRefund.DataContext = refundTrade;
                 }

                 if (tradeDetailList != null)
                 {
                     if (tradeDetailList.Count > 0)
                     {
                         txtAccountName.Text = tradeDetailList[0].AccountName;
                     }
                 }
             }));
        }

        /// <summary>
        /// 加载案件信息
        /// </summary>
        /// <param name="caseDto"></param>
        internal void InitCtrlCaseData(CaseDto caseDto)
        {
          this.Dispatcher.Invoke(new Action(
             delegate
             {
                 //案件编号
                 txtCaseId.Text = caseDto.CaseId;
             }));
        }

        /// <summary>
        /// 审核信息
        /// </summary>
        /// <param name="auditLogs">审核流水</param>
        /// <param name="orderStatus">订单状态</param>
        internal void InitCtrlAuditData(IList<AuditLog> auditLogs,OrderStatus orderStatus)
        {
            this.Dispatcher.Invoke(new Action(
               delegate
               {
                   //审核信息
                   ctrlAuditInfo.InitCtrlData(auditLogs, orderStatus);
               }));
        }

    }
}
