﻿using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds.Ctrl
{
    /// <summary>
    /// OrderInfo.xaml 的交互逻辑
    /// </summary>
    public partial class OrderInfoCtrl : UserControl
    {
        public OrderInfoCtrl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderNo">订单编号</param>
        /// <param name="orderType">订单类型</param>
        /// <param name="orderMoney">订单金额</param>
        /// <param name="orderStatus">订单状态</param>
        /// <param name="creater">创建人</param>
        /// <param name="createTime">创建时间</param>
        public void InitCtrlData(string orderNo, string orderType, string orderMoney, string orderStatus, string creater, string createTime)
        {
            this.txt_DDH.Text = orderNo;
            this.txt_DDLX.Text = orderType;
            this.txt_DDZT.Text = orderStatus;
            this.txt_DDJE.Text = orderMoney;
            this.txt_CJRY.Text = creater;
            this.txt_CJSJ.Text = createTime;
        }

        /// <summary>
        /// 初始化订单数据
        /// </summary>
        /// <param name="orderInfo"></param>
        internal void InitCtrlData(DataTransferObjects.Order orderInfo,bool isPayMent=false)
        {
            this.Dispatcher.Invoke(new Action(
                 delegate
                 {
                     this.txt_DDH.Text = orderInfo.OrderNo;
                     this.txt_DDLX.Text = EnumHelper.GetEnumDesc(orderInfo.OrderType);
                     this.txt_DDZT.Text = "--";
                     if (isPayMent)
                     {
                         this.txt_DDZT.Text= EnumHelper.GetEnumDesc(orderInfo.AuditStatus);
                     }
                     var username = UserServiceProxy.GetUserName(Convert.ToInt64(orderInfo.CreateUserSysNo));
                     if (!string.IsNullOrEmpty(username))
                     {
                         this.txt_CJRY.Text = username;
                     }
                     this.txt_DDJE.Text = Convert.ToString(orderInfo.OrderAmount);
                     this.txt_CJSJ.Text = orderInfo.CreateDate.ToShortDateString();
                 }));
        }
    }
}
