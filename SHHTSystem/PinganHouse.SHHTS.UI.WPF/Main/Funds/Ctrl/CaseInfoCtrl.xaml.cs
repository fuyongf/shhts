﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds.Ctrl
{
    /// <summary>
    /// CaseInfo.xaml 的交互逻辑
    /// </summary>
    public partial class CaseInfoCtrl : UserControl
    {
        public CaseInfoCtrl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 加载案件基础数据
        /// </summary>
        /// <param name="caseDto">案件基础信息</param>
        /// <param name="caseRemarkDto">案件附属信息</param>
        /// <param name="company">中介公司</param>
        /// <param name="staff">经纪人</param>
        public void InitCtrlData(CaseDto caseDto, CaseRemark caseRemarkDto)
        {
            try
            {
                this.Dispatcher.Invoke(new Action(
                     delegate
                     {
                         #region 案件基础信息
                         if (caseDto != null)
                         {
                             //产权证号
                             txt_CQZH.Text = caseDto.TenementContract;
                             //物业地址
                             txt_WYDZ.Text = caseDto.TenementAddress;
                             //卖方
                             txt_YZ.Text = caseDto.SellerName.Split(',')[0];
                             //买方
                             txt_MJ.Text = caseDto.BuyerName.Split(',')[0];
                             //案件编号
                             txt_CaseId.Text = caseDto.CaseId;
                             //中介公司
                             txt_ZJ.Text = caseDto.CompanyName;
                             //经纪人
                             txt_JJR.Text = caseDto.AgencyName;
                         }
                         #endregion

                         #region 案件基础信息
                         //案件附属信息
                         if (caseRemarkDto != null)
                         {
                             //物业名称
                             txt_WYMC.Text = caseRemarkDto.TenementName;
                             if (caseRemarkDto.LoopLinePosition != null)
                             {
                                 //环线位置
                                 txt_HXWZ.Text = EnumHelper.GetEnumDesc((LinkLocation)Enum.Parse(typeof(LinkLocation), caseRemarkDto.LoopLinePosition));
                             }
                             //居住类型
                             txt_JZLX.Text = EnumHelper.GetEnumDesc(caseRemarkDto.ResideType); ;
                             //交易类型
                             txt_JYLX.Text = EnumHelper.GetEnumDesc(caseRemarkDto.TradeType);
                         }
                         #endregion                     
                     }));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
