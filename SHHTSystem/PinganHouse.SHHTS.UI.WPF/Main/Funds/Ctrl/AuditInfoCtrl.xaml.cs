﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds.Ctrl
{
    /// <summary>
    /// AuditInfo.xaml 的交互逻辑
    /// </summary>
    public partial class AuditInfoCtrl : UserControl
    {
        /// <summary>
        /// 获取审核信息
        /// </summary>
        /// <param name="orderNo">订单编号</param>
        public AuditInfoCtrl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 审核处理
        /// </summary>
        /// <param name="auditLogList">审核流水</param>
        /// <param name="orderStatus">订单当前状态</param>
        internal void InitCtrlData(IList<AuditLog> auditLogList,OrderStatus orderStatus)
        {
            try
            {
                if (auditLogList == null || auditLogList.Count == 0) { return; }
                auditNothing.Visibility=Visibility.Collapsed;
                var backgroundColor = (Color)ColorConverter.ConvertFromString("#B5F05A22");
                var brush = new SolidColorBrush(backgroundColor);
                foreach (var auditLog in auditLogList)
                {
                    var status = (AuditStatus)auditLog.ChangeValue;
                    var result = string.Empty;
                    switch (status)
                    {
                        //核案审核
                        case AuditStatus.Audited:
                        case AuditStatus.AuditFailed:
                            btn_Audit01.Background = brush;
                            auditRow1.Visibility = Visibility.Visible;
                            audit_Time01.Text = auditLog.CreateDate.ToString("yyyy-MM-dd HH:mm");
                            audit_Person01.Text = auditLog.AuditUserName;
                            txt_AuditName01.Text = auditLog.AuditUserName;
                            image_Audit01.Visibility = Visibility.Visible;
                            audit_Remark01.Text = auditLog.Remark;
                            result = status == AuditStatus.Audited ? "通过" : "驳回";
                            audit_Result01.Text = result;
                            break;
                        //主办审核
                        case AuditStatus.ReAudited:
                        case AuditStatus.ReAuditFailed:
                            btn_Audit02.Background = brush;
                            auditRow2.Visibility = Visibility.Visible;
                            audit_Time02.Text = auditLog.CreateDate.ToString("yyyy-MM-dd HH:mm");
                            audit_Person02.Text = auditLog.AuditUserName;
                            txt_AuditName02.Text = auditLog.AuditUserName;
                            image_Audit02.Visibility = Visibility.Visible;
                            result = status == AuditStatus.ReAudited ? "通过" : "驳回";
                            audit_Result02.Text = result;
                            audit_Remark02.Text = auditLog.Remark;
                            break;
                        //总部审核
                        case AuditStatus.FinanceAudited:
                        case AuditStatus.FinanceAuditFailed:
                            btn_Audit03.Background = brush;
                            auditRow3.Visibility = Visibility.Visible;
                            audit_Time03.Text = auditLog.CreateDate.ToString("yyyy-MM-dd HH:mm");
                            audit_Person03.Text = auditLog.AuditUserName;
                            txt_AuditName03.Text = auditLog.AuditUserName;
                            image_Audit03.Visibility = Visibility.Visible;
                            result = status == AuditStatus.FinanceAudited ? "通过" : "驳回";
                            audit_Result03.Text = result;
                            audit_Remark03.Text = auditLog.Remark;
                            break;
                    }
                    if (orderStatus == OrderStatus.Completed)
                    {
                        btn_Audit04.Background = brush;
                    }
                }
            }
            catch (Exception) { }
        }
    }
}
