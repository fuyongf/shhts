﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds.Ctrl
{
    /// <summary>
    /// PaymentInfo.xaml 的交互逻辑
    /// </summary>
    public partial class PaymentInfoCtrl : UserControl
    {
        public PaymentInfoCtrl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 加载付款信息(出款信息)
        /// </summary>
        /// <param name="aParty">出款方</param>
        /// <param name="aMan">出款人</param>
        /// <param name="aPurpose">出款用途</param>
        /// <param name="money">出款金额</param>
        /// <param name="thePayee">收款方</param>
        /// <param name="payee">收款人</param>
        /// <param name="accountType">账户类别</param>
        /// <param name="account">账户</param>
        public void InitCtrlData(string aParty, string aMan, string aPurpose, string money, string thePayee, string payee, string accountType, string account)
        {
            this.txt_CKF.Text = aParty;
            this.txt_CKYT.Text ="付"+aPurpose;
            this.txt_CKJE.Text = money;
            this.txt_SKF.Text = thePayee;
            this.txt_SKR.Text = payee;
            this.txt_ZHLB.Text = accountType;
            this.txt_ZHXX.Text = account;
        }

        /// <summary>
        /// 加载订单信息
        /// </summary>
        /// <param name="orderInfo"></param>
        internal void InitCtrlData(DataTransferObjects.OrderDetail orderInfo)
        {
            try
            {
                this.Dispatcher.Invoke(new Action(
                     delegate
                     {
                         //出款金额
                         this.txt_CKJE.Text = Convert.ToString(orderInfo.OrderAmount);
                         //出款用途
                         txt_CKYT.Text = "付" + EnumHelper.GetEnumDesc(orderInfo.OrderBizType);
                         //出款方
                         var account = AccountServiceProxy.Get(orderInfo.AccountSysNo);
                         if (account != null)
                         {
                             txt_CKF.Text = EnumHelper.GetEnumDesc(account.AccountType);
                         }
                         //取收款方
                         long oppositeAccount = 0;
                         long.TryParse(Convert.ToString(orderInfo.OppositeAccount), out oppositeAccount);
                         var oppAccount = AccountServiceProxy.Get(oppositeAccount);
                         //收款方
                         if (oppAccount != null)
                         {
                             txt_SKF.Text = EnumHelper.GetEnumDesc(oppAccount.AccountType);
                         }
                         //账户类别
                         txt_ZHLB.Text = "不使用签订协议的银行卡";
                         //收款人
                         txt_SKR.Text = orderInfo.AccountName;
                         var bankName = ConfigServiceProxy.GetBankNameByCode(orderInfo.BankCode);
                         //账户信息
                         txt_ZHXX.Text = bankName + " " + orderInfo.SubBankName + " " + Utils.BankAccountFormat(orderInfo.BankAccountNo);
                     }));
            }
            catch (Exception) { }
        }

        /// <summary>
        /// 确定付款账户类型
        /// </summary>
        /// <param name="customersBankAccount">买卖方资金账户信息</param>
        /// <param name="orderBizType"></param>
        /// <param name="auuountNo"></param>
        internal void DeterminePaymentType( IEnumerable<CustomerBankAccount> customersBankAccount,OrderBizType orderBizType,string accountNo)
        {
            try
            {
                this.Dispatcher.Invoke(new Action(
                     delegate
                     {
                         foreach (var item in customersBankAccount)
                         {
                             if (item.BizType != orderBizType){
                                 continue;
                             }
                             if (item.AccountNo == accountNo)
                             {
                                 txt_ZHLB.Text = "使用签订协议的银行卡";
                             }
                         }
                     }));
            }
            catch (Exception) { }
        }
    }
}
