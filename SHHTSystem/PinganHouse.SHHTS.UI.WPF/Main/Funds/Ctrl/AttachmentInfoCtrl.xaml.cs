﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds.Ctrl
{
    /// <summary>
    /// AttachmentInfo.xaml 的交互逻辑
    /// </summary>
    public partial class AttachmentInfoCtrl : UserControl
    {
        public AttachmentInfoCtrl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileNum">附件数量</param>
        /// <param name="remark">备注</param>
        internal void InitCtrlData(List<string> fileList, string remark)
        {
            try
            {
                this.Dispatcher.Invoke(new Action(
                     delegate
                     {
                         var fileNum = fileList.Count;
                         if (fileNum > 0)
                         {
                             txtFileNum.Content = Convert.ToString(fileNum + "个附件");
                             txtFileNum.IsEnabled = true;
                             txtFileNum.Tag = fileList;
                         }
                         if (!string.IsNullOrEmpty(remark))
                         {
                             txtReMark.Text = "备注：" + remark;
                             wrpRemark.Visibility = Visibility.Visible;
                         }
                     }));
            }
            catch (Exception) { }
        }

        /// <summary>
        /// 下载附件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDown_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var btn = sender as Button;
                if (btn.Tag != null)
                {
                    var fileList=btn.Tag as List<string>;
                    var imageBrowse = new Main.ImageScan.ImageBrowse(fileList);
                    imageBrowse.ShowDialog();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("下载出现错误.", "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
