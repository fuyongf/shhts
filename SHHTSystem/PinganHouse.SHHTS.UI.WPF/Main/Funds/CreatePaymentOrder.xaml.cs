﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Main.Dialog;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds
{
    /// <summary>
    /// CreateTransferOrder.xaml 的交互逻辑
    /// </summary>
    public partial class CreatePaymentOrder : Page
    {
        /// <summary>
        /// 案件编号信息
        /// </summary>
        private string m_CaseId;//="06202872954906";

        /// <summary>
        /// 收款账户信息
        /// </summary>
        private IEnumerable<CustomerBankAccount> m_CustomerBankAccount;

        /// <summary>
        /// 附件集合
        /// </summary>
        private Dictionary<string, System.IO.Stream> m_FileList;

        /// <summary>
        /// 中介公司信息
        /// </summary>
        private AgentCompany m_AgentCompany;

        public CreatePaymentOrder()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 创建出款订单
        /// </summary>
        /// <param name="caseId"></param>
        public CreatePaymentOrder(string caseId)
        {
            InitializeComponent();
            m_CaseId = caseId;
            InitDataToCtrl();
        }

        /// <summary>
        /// 页面基础数据加载
        /// </summary>
        private void InitDataToCtrl()
        {
            try
            {
                //出款用途
                cb_CKYT.ItemsSource = EnumHelper.InitOrderBizTypeToCombobox(true, new List<OrderBizType> { OrderBizType.EFQServiceCharge });
                cb_CKYT.DisplayMemberPath = "DisplayMember";
                cb_CKYT.SelectedValuePath = "ValueMember";
                cb_CKYT.SelectedValue = Convert.ToInt32(OrderBizType.HosingFund);

            }
            catch (Exception)
            {
                MessageBox.Show("初始化数据发生异常.", "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 页面加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                LoadPageData();
            }
            catch (Exception ex)
            {
                this.ctrlLoading.IsBusy = false;
                MessageBox.Show("初始化数据发生异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private async void LoadPageData()
        {
            try
            {
                PageHelper.PageActiveLoadingControl(this.ctrlLoading, "正在加载数据,请稍候...");
                //获取案件信息
                var caseResult = await FundsHelper.Instance.GetCaseData(m_CaseId);
                //案件对象
                var caseDto = caseResult.Item1;
                //案件附属对象
                var caseRemarkDto = caseResult.Item2;
                //加载案件信息
                this.ctrlCaseInfo.InitCtrlData(caseDto, caseRemarkDto);
                //申请人信息
                this.txt_SQR.Text = LoginHelper.CurrentUser.RealName;
                //申请时间
                this.txt_Time.Text = System.DateTime.Now.ToShortDateString();
                //申请人部门
                txt_Dep.Text = string.Empty;
                //中介公司信息
                var agentCompany = AgentServiceProxy.GetAgentCompany(caseDto.CompanySysNo);
                m_AgentCompany = agentCompany;
                //银行卡信息
                var bankAccounts = CaseProxyService.GetInstanse().GetCustomerBankAccounts(caseDto.SysNo);
                m_CustomerBankAccount = bankAccounts;
                //付款方--买家
                cbItem_Buyer.IsSelected = true;
                //收款方--卖家选择
                cbItem_Seller.IsSelected = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.ctrlLoading.IsBusy = false;
            }


        }

        /// <summary>
        /// 切割银行卡
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BankText_TextChanged(object sender, TextChangedEventArgs e)
        {
            var textBox = sender as TextBox;
            var txt = Utils.BankAccountFormat(textBox.Text.Trim());
            textBox.Text = txt.TrimEnd();
            textBox.Select(txt.Length, 0);
        }

        /// <summary>
        /// 限制输入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;

            //屏蔽非法按键
            if ((e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) || e.Key == Key.Decimal)
            {
                if (txt.Text.Contains(".") && e.Key == Key.Decimal)
                {
                    e.Handled = true;
                    return;
                }
                e.Handled = false;
            }
            else if (((e.Key >= Key.D0 && e.Key <= Key.D9) || e.Key == Key.OemPeriod) && e.KeyboardDevice.Modifiers != ModifierKeys.Shift)
            {
                if (txt.Text.Contains(".") && e.Key == Key.OemPeriod)
                {
                    e.Handled = true;
                    return;
                }
                e.Handled = false;
            }
            else if (e.Key == Key.Tab)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// 关闭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, new QueryCaseInfo(m_CaseId));
        }

        /// <summary>
        /// 提交出款订单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PageHelper.PageActiveLoadingControl(this.ctrlLoading, "正在处理,请稍候...");
                //确定出款方
                var accountItem = cb_CKF.SelectedItem as ComboBoxItem;
                if (accountItem == null)
                {
                    MessageBox.Show("请选择出款方", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ctrlLoading.IsBusy = false;
                    return;
                }
                var accountType = Convert.ToString(accountItem.Tag) == "Buyer" ? AccountType.Buyer : AccountType.Seller;
                //出款用途
                var oderBizType = (OrderBizType)Enum.Parse(typeof(OrderBizType), Convert.ToString(cb_CKYT.SelectedValue));
                //出款金额
                decimal amount;
                decimal.TryParse(txt_CKJE.Text.Trim(), out amount);
                if (amount == 0)
                {
                    MessageBox.Show("请填写正确的出款金额", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ctrlLoading.IsBusy = false;
                    return;
                }
                var customerBankAccount = txtBankInfo.Tag as CustomerBankAccount;
                //定义账户相关
                var bankAccountNo = string.Empty;
                var bankCode = string.Empty;
                var bankName = string.Empty;
                var amountName = string.Empty;
                if (customerBankAccount != null)
                {
                    //出款人
                    amountName = customerBankAccount.AccountName;
                    bankAccountNo = customerBankAccount.AccountNo;
                    bankCode = customerBankAccount.BankCode;
                    bankName = customerBankAccount.BankName;
                }
                //检查银行卡信息
                if (string.IsNullOrEmpty(bankAccountNo) || string.IsNullOrEmpty(bankCode))
                {
                    MessageBox.Show("请确定银行卡信息.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ctrlLoading.IsBusy = false;
                    return;
                }
                var item = cb_SKF.SelectedItem as ComboBoxItem;
                AccountType? payeeType = null;
                switch (Convert.ToString(item.Tag))
                {
                    case "Buyer": payeeType = AccountType.Buyer; break;
                    case "Seller": payeeType = AccountType.Seller; break;
                    case "Intermediary": payeeType = AccountType.Agency; break;
                }
                if (payeeType == null)
                {
                    MessageBox.Show("创建出款订单失败：无法确定收款方", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ctrlLoading.IsBusy = false;
                    return;
                }
                //中介公司编码
                var agencySubjectId = m_CaseId;
                //判断是否是中介
                if (payeeType.Value == AccountType.Agency)
                {
                    agencySubjectId = Convert.ToString(m_AgentCompany.SysNo);
                }
                //保存数据
                var result = await SavePayMentData(accountType, payeeType.Value, m_CaseId, agencySubjectId, oderBizType, amount,
                   bankAccountNo, bankCode, amountName, bankName, txtRemark.Text.Trim(), LoginHelper.CurrentUser.SysNo);

                //判断是否成功
                if (!result.Success)
                {
                    MessageBox.Show("创建出款订单失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ctrlLoading.IsBusy = false;
                }
                else
                {
                    //上传图片
                    if (result.OtherData != null)
                    {
                        if (result.OtherData.ContainsKey("OrderNo"))
                        {
                            AsynchronousUpload(Convert.ToString(result.OtherData["OrderNo"]));
                        }
                    }

                    PageHelper.PageNavigateHelper(this, new FundSettlementListPage());
                }
            }
            catch (Exception ex)
            {
                ctrlLoading.IsBusy = false;
                MessageBox.Show("系统操作发生异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 异步上传
        /// </summary>
        private void AsynchronousUpload(string subjectNo)
        {
            //上传附件
            if (m_FileList != null)
            {
                AttachmentProxyService.GetInstanse().OnCompleted += CreatePaymentOrder_OnCompleted;
                foreach (var file in m_FileList)
                {
                    var result = AttachmentProxyService.GetInstanse().AddAttachment(
                        new AttachmentContent()
                        {
                            AttachmentContentData = file.Value,
                            FileName = file.Key,
                            AssociationNo = subjectNo,
                            AttachmentType = AttachmentType.出款票据
                        }, true);
                }
            }
        }

        /// <summary>
        /// 上传完成删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void CreatePaymentOrder_OnCompleted(object sender, AttachmentProxyService.UploadEventArgs e)
        {
            try
            {
                e.AttachInfo.AttachmentContentData.Close();
                e.AttachInfo.AttachmentContentData.Dispose();
                if (File.Exists(e.AttachInfo.FileName))
                {
                    File.Delete(e.AttachInfo.FileName);
                }
            }
            catch (Exception ex) { }
        }


        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="accountType"></param>
        /// <param name="accountSubjectId"></param>
        /// <param name="tradeType"></param>
        /// <param name="amount"></param>
        /// <param name="bankAccountNo"></param>
        /// <param name="bankCode"></param>
        /// <param name="accountName"></param>
        /// <param name="bankName"></param>
        /// <param name="remark"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        private Task<OperationResult> SavePayMentData(AccountType accountType, AccountType payeeType, string accountSubjectId, string agencySubjectId, OrderBizType tradeType, decimal amount, string bankAccountNo, string bankCode, string accountName, string bankName, string remark, long operatorSysNo)
        {
            return Task.Run(() =>
           {
               //提交表单
               return TradeServiceProxy.SubmitEntrustPay(accountType, accountSubjectId, payeeType, agencySubjectId, tradeType, amount, bankAccountNo, bankCode, accountName, bankName, remark, operatorSysNo);
           });
        }


        /// <summary>
        /// 使用第三方协议文件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                if (bankPanle == null) return;
                var radio = sender as RadioButton;
                var backgroundColor = (Color)ColorConverter.ConvertFromString("#C9C9C9");
                var brush = new SolidColorBrush(backgroundColor);
                switch (radio.Name)
                {
                    case "system_Bank":
                        PageHelper.PageControlToDisable(bankPanle, new System.Windows.Controls.Control[] { other_Bank });
                        break;
                    case "other_Bank":
                        backgroundColor = (Color)ColorConverter.ConvertFromString("#FF0000");
                        brush = new SolidColorBrush(backgroundColor);
                        PageHelper.PageControlToActivate(bankPanle);
                        break;
                }
                txt_Tip.Foreground = brush;
            }
            catch (Exception)
            {

            }
        }


        #region 银行选择
        /// <summary>
        /// 选择银行
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var btn = sender as Button;
                var bankDialog = new BanksDialog(Convert.ToString(btn.Tag));
                bankDialog.SelectBankEvent += bankDialog_SelectBankEvent;
                bankDialog.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("选择银行操作发生系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 选择银行
        /// </summary>
        /// <param name="bankCode"></param>
        /// <param name="bankName"></param>
        /// <param name="tag"></param>
        private void bankDialog_SelectBankEvent(string bankCode, string bankName, string tag)
        {
            try
            {
                bank_Info.Text = bankName;
                bank_Info.Tag = bankCode;
            }
            catch (Exception ex)
            {
                MessageBox.Show("选择银行操作发生系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        #endregion

        #region 收款方发生变化
        /// <summary>
        /// 收款方发生变化处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SKF_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                this.txtBankInfo.Text = string.Empty;
                this.txtBankInfo.Tag = null;
                var item = cb_SKF.SelectedItem as ComboBoxItem;
                if (item != null)
                {
                    switch (Convert.ToString(item.Tag))
                    {
                        case "Buyer":
                            //查找收款账户信息
                            GetCustomerAccount(CustomerType.Buyer, BankAccountPurpose.Receive);
                            break;
                        case "Seller":
                            //查询收款账户信息
                            GetCustomerAccount(CustomerType.Seller, BankAccountPurpose.Receive);
                            break;
                        case "Intermediary":
                            //查询中介账户
                            this.txtBankInfo.Tag = new CustomerBankAccount
                            {
                                AccountNo = m_AgentCompany.ReceiveAccount,
                                BankCode = m_AgentCompany.ReceiveBank,
                                BankName = m_AgentCompany.ReceiveBank,
                                SubBankName = m_AgentCompany.ReceiveSubBank,
                                AccountName = m_AgentCompany.ReceiveName
                            };
                            this.txtBankInfo.Text = ConfigServiceProxy.GetBankNameByCode(m_AgentCompany.ReceiveBank);
                            //支行
                            if (!string.IsNullOrEmpty(m_AgentCompany.ReceiveSubBank))
                            {
                                this.txtBankInfo.Text += "  " + m_AgentCompany.ReceiveSubBank;
                            }
                            //收款人
                            if (!string.IsNullOrEmpty(m_AgentCompany.ReceiveName))
                            {
                                this.txtBankInfo.Text += "  " + m_AgentCompany.ReceiveName;
                            }
                            //账户信息
                            this.txtBankInfo.Text += "  " + m_AgentCompany.ReceiveAccount;
                            break;
                    }
                }

            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// 获取账号信息
        /// </summary>
        /// <param name="customerType"></param>
        /// <param name="bankAccountPurpose"></param>
        private void GetCustomerAccount(CustomerType customerType, BankAccountPurpose bankAccountPurpose)
        {
            try
            {
                if (m_CustomerBankAccount == null) return;
                foreach (var item in m_CustomerBankAccount)
                {
                    var customerBankAccount = item as CustomerBankAccount;
                    if (customerBankAccount.CustomerType == customerType
                        && customerBankAccount.AccountType == bankAccountPurpose)
                    {
                        //付款用途
                        //var oderBizType = (OrderBizType)Enum.Parse(typeof(OrderBizType), Convert.ToString(cb_CKYT.SelectedValue));
                        //if (customerBankAccount.BizType != oderBizType) continue;
                        this.txtBankInfo.Tag = item;
                        this.txtBankInfo.Text = item.AccountNo;
                        this.txtBankInfo.Text += "  " + item.AccountName;
                        this.txtBankInfo.Text += "  " + item.BankName + "|" + item.SubBankName;

                    }
                }
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// 获取帐户余额
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cb_CKF_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = cb_CKF.SelectedItem as ComboBoxItem;
            var accountType = AccountType.Buyer;
            //获取余额
            if (item != null)
            {
                switch (Convert.ToString(item.Tag))
                {
                    case "Seller":
                        //查询收款账户信息
                        accountType = AccountType.Seller;
                        break;
                }
            }
            ThreadPool.QueueUserWorkItem((obj) =>
            {
                this.Dispatcher.Invoke(() =>
                {
                    var account = AccountServiceProxy.GetBalance(accountType, m_CaseId);
                    txt_ZHYE.Text = string.Format("{0:N2}", account);
                });
            });

        }
        #endregion

        #region 上传附件
        /// <summary>
        /// 本地上传
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UploadFile_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var uploadFileHelper = new UploadFileHelper();
                var files = uploadFileHelper.SelectUploadFile();
                if (files == null || files.Count == 0) return;
                m_FileList = files;
                fileNum.Tag = null;
                fileNum.Tag = files;
                fileNum.Content = files.Count + "个附件";
            }
            catch (Exception ex)
            {
                MessageBox.Show("选择上传文件发生系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        /// <summary>
        /// 上传附件操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUploadFile_Click(object sender, RoutedEventArgs e)
        {
            var btnUpload = sender as Button;
            if (btnUpload.Tag == null)
            {
                CameraUploadFile_Click(sender, e);
            }
        }

        /// <summary>
        /// 高拍仪器上传
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CameraUploadFile_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var captureImge = new CaptrueImage(AttachmentType.出款票据);
                captureImge.AllUploadedEvent += captureImge_AllUploadedEvent;
            }
            catch (Exception ex)
            {
                MessageBox.Show("选择上传文件发生系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 接收返回的参数
        /// </summary>
        /// <param name="fileStreamList"></param>
        /// <param name="fileNameList"></param>
        /// <param name="captrueImage"></param>
        void captureImge_AllUploadedEvent(Dictionary<string, System.IO.Stream> files, string filePath, CaptrueImage captrueImage, CameraInterfaceInvoke cameraInvoke, UInt32 mDeviceHandle)
        {
            cameraInvoke.func_CloseDevice(mDeviceHandle);
            captrueImage.Close();
            if (files == null || files.Count == 0) return;
            m_FileList = files;
            fileNum.Tag = null;
            fileNum.Tag = files;
            fileNum.Content = files.Count + "个附件";
        }
        #endregion

        #region 银行鉴权处理
        /// <summary>
        /// 银行鉴权处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Authentication_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //银行
                if (string.IsNullOrEmpty(bank_Info.Text.Trim()))
                {
                    MessageBox.Show("请选择银行.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    bank_Info.Focus();
                    return;
                }
                //银行卡号
                if (string.IsNullOrEmpty(bank_BankNo.Text.Trim()))
                {
                    MessageBox.Show("请填写卡号.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    bank_BankNo.Focus();
                    return;
                }
                //账户人
                if (string.IsNullOrEmpty(account_Name.Text.Trim()))
                {
                    MessageBox.Show("请填写账户持有人姓名.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    account_Name.Focus();
                    return;
                }
                var authenticationDialog = new BankAuthenticationDialog(m_CaseId, account_Name.Text.Trim(), Convert.ToString(bank_Info.Tag), bank_BankNo.Text.Trim());
                authenticationDialog.OnCompleted += authenticationDialog_OnCompleted;
                authenticationDialog.ShowDialog();
            }
            catch (Exception) { }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void authenticationDialog_OnCompleted(object sender, BankAuthenticationDialog.AuthenticationEventArgs e)
        {
            if (e.OperationResult.Success)
            {
                authentication.Tag = 1;
            }
        }
        #endregion

        #region E房钱服务费处理
        private void eMoney_Checked(object sender, RoutedEventArgs e)
        {
            txt_eMoney.Text = string.Empty;
            txt_eMoney.Focus();
            txt_eMoney.IsEnabled = true;
        }

        private void eMoney_Unchecked(object sender, RoutedEventArgs e)
        {
            txt_eMoney.Text = string.Empty;
            txt_eMoney.IsEnabled = false;
        }
        #endregion

    }
}
