﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds
{
    /// <summary>
    /// QueryCaseInfo.xaml 的交互逻辑
    /// </summary>
    public partial class QueryCaseInfo : Page
    {
        string _caseId = string.Empty;
        public QueryCaseInfo(string caseId=null)
        {
            InitializeComponent();
            if (!string.IsNullOrWhiteSpace(caseId))
            {
                _caseId = caseId;
                cbSearchType.SelectedValue = 0;
                txtKey.Text = _caseId;
            }
        }

        /// <summary>
        /// Loading数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(_caseId))
                {
                    PageHelper.PageActiveLoadingControl(this.ctrlLoading, "加载数据,请稍候...");
                    var result = await FundsHelper.Instance.GetCaseData(_caseId);
                    if (result.Item1 == null)
                    {
                        _caseId = "";
                        caseInfoCtrl.InitCtrlData(new CaseDto(), new CaseRemark());
                        return;
                    }
                    var caseDto = result.Item1;
                    var caseRemarkDto = result.Item2;
                    caseInfoCtrl.InitCtrlData(caseDto, caseRemarkDto);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("系统获取数据异常.", "系统系统", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                this.ctrlLoading.IsBusy = false;
            }
        }

        /// <summary>
        /// 搜索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSearch_OnClick(object sender, RoutedEventArgs e)
        {
            var searchType = (QueryStringType)int.Parse((cbSearchType.SelectedItem as ComboBoxItem).Tag.ToString());
            if (searchType == QueryStringType.CaseId)
            {
                _caseId = txtKey.Text.Trim();
                Page_Loaded(sender, e);//重新加载数据
            }
            else
            {
                var caseinfoDia = new QueryCaseInfoDialog(((ComboBoxItem)cbSearchType.SelectedItem).Tag.ToString(), txtKey.Text.Trim());
                caseinfoDia.SelectedEvent += caseinfoDia_SelectedEvent;
                caseinfoDia.ShowDialog();

            }

        }

        void caseinfoDia_SelectedEvent(string caseId)
        {
            _caseId = caseId;
            try
            {
                CaseDto caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(_caseId);
                if (caseDto == null)
                {
                    MessageBox.Show("案件未找到！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    CaseRemark caseRemarkDto = CaseProxyService.GetInstanse().GetRemarkBySysNo(caseDto.SysNo);

                    caseInfoCtrl.InitCtrlData(caseDto, caseRemarkDto);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 新建入账订单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCreateIncomeOrder_Click(object sender, RoutedEventArgs e)
        {
            if (_caseId == "")
            {
                MessageBox.Show("请选择正确的案件", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                var createOrder = new CreatePosOrder(_caseId);
                PageHelper.PageNavigateHelper(this, createOrder);

              
            }
        }

        private void BtnCreateOutOrder_OnClick(object sender, RoutedEventArgs e)
        {
            if (_caseId == "")
            {
                MessageBox.Show("请选择正确的案件", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                var createPaymentOrder = new CreatePaymentOrder(_caseId);
                PageHelper.PageNavigateHelper(this, createPaymentOrder);
            }
        }

        private void BtnCancel_OnClick(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, new FundSettlementListPage());
        }

        private void BtnCreateCostOrder_OnClick(object sender, RoutedEventArgs e)
        {
            if (_caseId == "")
            {
                MessageBox.Show("请选择正确的案件", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                var createOrder = new CreateCostOrder(_caseId);
                PageHelper.PageNavigateHelper(this, createOrder);


            }
        }

        private void BtnCreateWriteOffOrder_OnClick(object sender, RoutedEventArgs e)
        {
            if (_caseId == "")
            {
                MessageBox.Show("请选择正确的案件", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                var win = new CreateWriteOffOrder(_caseId);
                PageHelper.PageNavigateHelper(this, win);


            }
        }
    }
}
