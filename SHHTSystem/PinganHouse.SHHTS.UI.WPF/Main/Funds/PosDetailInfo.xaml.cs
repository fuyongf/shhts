﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds
{
    /// <summary>
    /// PosDetailInfo.xaml 的交互逻辑
    /// </summary>
    public partial class PosDetailInfo : Page
    {
        private string _orderNo = string.Empty;
        private string _caseId = string.Empty;
        IList<BillDetail> _billList = new List<BillDetail>();

        public PosDetailInfo(string orderNo, string caseId)
        {
            InitializeComponent();

            _orderNo = orderNo;
            _caseId = caseId;

            try
            {
                var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(_caseId);

                var caseRemarkDto = CaseProxyService.GetInstanse().GetRemarkBySysNo(caseDto.SysNo);

                ctrlCaseInfo.InitCtrlData(caseDto, caseRemarkDto);

                var order = TradeServiceProxy.GetOrderInfo(_orderNo);
                ctrlOrderInfo.InitCtrlData(order);

                var account = AccountServiceProxy.Get(order.AccountSysNo);
                lblAccountType.Content = EnumHelper.GetEnumDesc(account.AccountType);
                lblOrderAmount.Content = order.OrderAmount.ToString("C");
                lblPaymentChannel.Content = EnumHelper.GetEnumDesc(order.PaymentChannel);
                if (order.OrderBizType == OrderBizType.HosingFund)
                {
                    lblOrderBizType.Content = "代收房款";
                }
                else if (order.OrderBizType == OrderBizType.Taxes)
                {
                    lblOrderBizType.Content = "代收税费";
                }
                else if (order.OrderBizType == OrderBizType.ServiceCharge)
                {
                    lblOrderBizType.Content = "代收佣金";
                }
                else if (order.OrderBizType == OrderBizType.Taxes)
                {
                    lblOrderBizType.Content = "代收服务费";
                }
                else
                {
                    lblOrderBizType.Content = EnumHelper.GetEnumDesc(order.OrderBizType);
                }

                _billList = TradeServiceProxy.GetPosBillByOrder(_orderNo,null);
                dgPosList.DataContext = _billList;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BtnClose_OnClick(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, new FundSettlementListPage());
        }

        /// <summary>
        /// 撤销按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCancel_OnClick(object sender, RoutedEventArgs e)
        {

            var tradeNo = Convert.ToString(((Button)sender).CommandParameter);
            var bill = new BillDetail();
            foreach (var b in _billList.Where(b => tradeNo == b.TradeNo))
            {
                bill = b;
            }
            var openWindow = new PosDetailInfoCancelDialog(bill);
            openWindow.SaveEvent += openWindow_SaveEvent;
            openWindow.ShowDialog();

        }

        void openWindow_SaveEvent()
        {
            var posList = TradeServiceProxy.GetPosBillByOrder(_orderNo, null);
            dgPosList.DataContext = posList;
        }

        /// <summary>
        /// 删除按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnDelete_OnClick(object sender, RoutedEventArgs e)
        {
            var msgResult = MessageBox.Show("确认要删除该数据吗？该操作不可恢复，是否继续？", "系统确认", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
            if (msgResult == MessageBoxResult.No) return;

            var tradeNo = Convert.ToString(((Button)sender).CommandParameter);
            var result = TradeServiceProxy.CancelCollectionTrade(tradeNo, LoginHelper.CurrentUser.SysNo);
            if (result.Success)
            {
                MessageBox.Show("删除成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
            PageHelper.PageNavigateHelper(this, new PosDetailInfo(_orderNo, _caseId));
            }
            else
            {
                MessageBox.Show("操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }

    [ValueConversion(typeof(string), typeof(string))]
    public class BilledConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool val = (bool)value;

            if (val)
                return "已开具";
            else
            {
                return "未开具";
            }

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {

            return "";
        }
    }
}
