﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Main.Dialog;
using PinganHouse.SHHTS.UI.WPF.Model;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds
{
    /// <summary>
    /// CostPaymentList.xaml 的交互逻辑
    /// </summary>
    public partial class CostPaymentList : Page
    {
        private string _caseId = string.Empty;
        private readonly int _pageSize = 10;

        public CostPaymentList()
        {
            InitializeComponent();
            
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            GridPager.PageIndex = 1;
            this.GridPager.PageNumChanged = GridPager_PagerIndexChanged;
        }

        /// <summary>
        /// 搜索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSearch_OnClick(object sender, RoutedEventArgs e)
        {
            var searchType = (QueryStringType)int.Parse((cbSearchType.SelectedItem as ComboBoxItem).Tag.ToString());
            if (searchType == QueryStringType.CaseId)
            {
                _caseId = txtKey.Text.Trim();
                Page_Loaded(sender, e);//重新加载数据
            }
            else
            {
                var caseinfoDia = new QueryCaseInfoDialog(((ComboBoxItem)cbSearchType.SelectedItem).Tag.ToString(), txtKey.Text.Trim());
                caseinfoDia.SelectedEvent += caseinfoDia_SelectedEvent;
                caseinfoDia.ShowDialog();

            }

        }

        void caseinfoDia_SelectedEvent(string caseId)
        {
            _caseId = caseId;
            try
            {
                CaseDto caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(_caseId);
                if (caseDto == null)
                {
                    MessageBox.Show("案件未找到！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    GridPager.PageIndex = 1;
                    this.GridPager.PageNumChanged = GridPager_PagerIndexChanged;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 分页插件事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridPager_PagerIndexChanged(int index)
        {
            PageHelper.PageActiveLoadingControl(loading, "正在加载数据,请稍后...");
            var thread = new Thread(GetDataGridData) { IsBackground = true };
            thread.Start();

        }

        #region 页面数据取得方法
        /// <summary>
        /// 获取页面数据
        /// </summary>
        private void GetDataGridData()
        {
            Dispatcher.Invoke(delegate
            {
                try
                {
                    
                    int cnt;


                    var deductScheduleModelList = new List<DeductScheduleModel>();

                    var deductScheduleList = TradeServiceProxy.GetDeductSchedules(GridPager.PageIndex, _pageSize, out cnt, ConfigHelper.GetCurrentReceptionCenter(), _caseId, null, null, null);

                    if (deductScheduleList != null)
                    {
                        deductScheduleModelList = DeductScheduleModel.InitData(deductScheduleList);
                    }

                    this.dataGrid.DataContext = deductScheduleModelList;

                    if (cnt % _pageSize != 0)
                    {
                        GridPager.PageCount = cnt / _pageSize + 1;
                    }
                    else
                    {
                        GridPager.PageCount = cnt / _pageSize;
                    }
                    GridPager.Visibility = Visibility.Visible;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("加载数据发生错误：" + ex, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                finally
                {
                    loading.IsBusy = false;
                }
            });
        }
        #endregion

        /// <summary>
        /// 案件跳转填写明细
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RequestNavigate_Click(object sender, RoutedEventArgs e)
        {
            //try
            //{
            //    var link = e.Source as Hyperlink;
            //    if (null != link)
            //    {
            //        var caseId = Convert.ToString(link.NavigateUri);
            //        var frm = new CaseDetailsDialog(caseId);
            //        frm.Show();
            //    }
            //}
            //catch (Exception)
            //{
            //    MessageBox.Show("显示明细发生错误", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            //}
            //e.Handled = true;
        }

        private void BtnQueryCase_OnClick(object sender, RoutedEventArgs e)
        {
            var queryCasePage = new CostQueryCase();
            PageHelper.PageNavigateHelper(this, queryCasePage);
        }
    }
}
