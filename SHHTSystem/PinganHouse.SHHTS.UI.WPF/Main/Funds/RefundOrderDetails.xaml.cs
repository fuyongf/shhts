﻿using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds
{
    /// <summary>
    /// RefundOrderDetails.xaml 的交互逻辑
    /// </summary>
    public partial class RefundOrderDetails : Page
    {
        /// <summary>
        /// 订单号
        /// </summary>
        private string m_OrderNo;

        /// <summary>
        /// 订单状态
        /// </summary>
        private OrderStatus m_OrderStatus;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="orderNo">订单号</param>
        /// <param name="orderStatus">订单当前状态</param>
        public RefundOrderDetails(string orderNo, OrderStatus orderStatus)
        {
            InitializeComponent();
            m_OrderNo = orderNo;
            m_OrderStatus = orderStatus;
        }

        /// <summary>
        /// 关闭查看
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Close_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, new FundSettlementListPage());
        }

        /// <summary>
        /// 页面数据加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                PageHelper.PageActiveLoadingControl(this.ctrlLoading, "加载数据,请稍候...");
                LoadPageData(m_OrderNo);
            }
            catch (Exception ex)
            {
                MessageBox.Show("加载数据发生系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
                ctrlLoading.IsBusy = false;
            }
        }

        /// <summary>
        /// 加载页面数据
        /// </summary>
        /// <param name="orderNo"></param>
        private async void LoadPageData(string orderNo)
        {
            //获取订单信息
            var orderInfo = await FundsHelper.Instance.GetOederData(orderNo);
            ctrlRefundInfo.InitCtrlOrderData(orderInfo);

            //加载交易信息
            var tradeDetailList = await FundsHelper.Instance.GetTradeByOrder(orderNo);
            ctrlRefundInfo.InitCtrlTradeDetailData(tradeDetailList);

            //加载交易信息
            var account = await FundsHelper.Instance.GetAccount(orderInfo.AccountSysNo);
            ctrlRefundInfo.InitCtrlAccountData(account);

            //获取案件信息
            var caseDto = await FundsHelper.Instance.GetCase(account.SubjectId);
            ctrlRefundInfo.InitCtrlCaseData(caseDto);

            //获取审核状态
            var auditLogList = TradeServiceProxy.GetOrderAuditLog(orderNo);
            ctrlRefundInfo.InitCtrlAuditData(auditLogList, m_OrderStatus);
            ctrlLoading.IsBusy = false;
        }
    }
}
