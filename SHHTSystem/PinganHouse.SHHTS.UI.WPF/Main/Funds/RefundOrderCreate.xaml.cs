﻿using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds
{
    /// <summary>
    /// RefundOrderCreate.xaml 的交互逻辑
    /// </summary>
    public partial class RefundOrderCreate : Page
    {
        public delegate void ClosePageHandel(Page page,int pageIndex, int currentSwitchBtnIndex);
        public event ClosePageHandel ClosePageEvent;
        /// <summary>
        /// 订单编号
        /// </summary>
        private string m_OrderNo;
        private int m_pageIndex;
        private int m_currentSwitchBtnIndex = -1;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="orderNo"></param>
        public RefundOrderCreate(string orderNo, int pageIndex, int currentSwitchBtnIndex)
        {
            InitializeComponent();
            m_pageIndex = pageIndex;
            m_currentSwitchBtnIndex = currentSwitchBtnIndex;
            m_OrderNo = orderNo;
        }

        /// <summary>
        /// 取消退款
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            if (ClosePageEvent != null)
            {
                ClosePageEvent(this,m_pageIndex, m_currentSwitchBtnIndex);
                return;
            }
            PageHelper.PageNavigateHelper(this, new FundSettlementListPage());
        }

        /// <summary>
        /// 确定退款
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
               var result=TradeServiceProxy.SubmitRefund(m_OrderNo, txtRemark.Text.Trim(), LoginHelper.CurrentUser.SysNo);
               if (result.Success)
               {
                   MessageBox.Show("退款申请提交成功.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                   btnOk.IsEnabled = false;
                   if (ClosePageEvent != null)
                   {
                       ClosePageEvent(this,m_pageIndex, m_currentSwitchBtnIndex);
                   }
               }
               else
               {
                   MessageBox.Show("退款申请提交失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
               }
            }
            catch (Exception ex)
            {
                MessageBox.Show("操作发生系统异常："+ex.Message,"系统异常",MessageBoxButton.OK,MessageBoxImage.Error);
            }
        }


        /// <summary>
        /// 数据加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                PageHelper.PageActiveLoadingControl(loadingCtrl, "正在获取数据,请稍后...");
                //申请人信息
                txt_SQR.Text = LoginHelper.CurrentUser.RealName;
                txt_Dep.Text = string.Empty;
                txt_Time.Text = System.DateTime.Now.ToShortDateString();
                Thread thread = new Thread(delegate()
                {
                    GetPageData();
                });
                thread.Start();
                thread.IsBackground = true;
            }
            catch (Exception)
            {
                MessageBox.Show("加载数据发送系统异常","系统异常",MessageBoxButton.OK,MessageBoxImage.Error);
                loadingCtrl.IsBusy = false;
            }
        }

        /// <summary>
        /// 加载页面数据
        /// </summary>
        private void GetPageData()
        {
            this.Dispatcher.Invoke(new Action(
                    delegate
                    {
                        try
                        {
                            //获取订单信息
                            var orderInfo = TradeServiceProxy.GetOrderInfo(m_OrderNo);
                            if (orderInfo == null) return;
                            //订单编号
                            txtOrderNo.Text = orderInfo.OrderNo;
                            //订单类型
                            txtOrderType.Text = EnumHelper.GetEnumDesc(orderInfo.OrderType);
                            //订单金额
                            txtAccount.Text = Convert.ToString(orderInfo.OrderAmount);
                            //资金状态
                            txtOrderStatus.Text = EnumHelper.GetEnumDesc(orderInfo.OrderStatus);
                            //付款性质
                            txtOrderBiz.Text = EnumHelper.GetEnumDesc(orderInfo.OrderBizType);
                            //付款人
                            var tradeDetailList = TradeServiceProxy.GetTradeByOrder(m_OrderNo);
                            if (tradeDetailList != null)
                            {
                                var orderTrade = from query in tradeDetailList
                                                 where query.Amount > 0
                                                 select query;
                                dataGrid.DataContext = orderTrade;
                            }
                            if (tradeDetailList != null)
                            {
                                if (tradeDetailList.Count > 0)
                                {
                                    txtAccountName.Text = tradeDetailList[0].AccountName;
                                }
                            }
                            //账户信息
                            var account = AccountServiceProxy.Get(orderInfo.AccountSysNo);
                            if (account == null) return;
                            txtAccountType.Text = EnumHelper.GetEnumDesc(account.AccountType);
                            //获取案件信息
                            var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(account.SubjectId);
                            if (caseDto == null) return;
                            //案件编号
                            txtCaseId.Text = caseDto.CaseId;
                            loadingCtrl.IsBusy = false;
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            loadingCtrl.IsBusy = false;
                        }
                    }));
               
        }

        private void btnUploadFile_Click(object sender, RoutedEventArgs e)
        {

        }

        private void CameraUploadFile_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
