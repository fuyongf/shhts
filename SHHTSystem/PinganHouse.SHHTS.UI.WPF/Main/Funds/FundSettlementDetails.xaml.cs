﻿using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds
{

    /// <summary>
    /// FundSettlementDetails.xaml 的交互逻辑
    /// </summary>
    public partial class FundSettlementDetails : Page
    {
        /// <summary>
        /// 订单编号
        /// </summary>
        private string m_CaseId;

        public FundSettlementDetails(string caseId)
        {
            InitializeComponent();
            m_CaseId = caseId;
        }

        /// <summary>
        /// 关闭页面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, new FundSettlementListPage());
        }

        /// <summary>
        /// 导出Excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnExcel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PageHelper.PageActiveLoadingControl(loadingCtrl,"正在处理,请稍候...");
                var caseReuslt = await FundsHelper.Instance.GetCaseData(m_CaseId);
                var caseDto = caseReuslt.Item1;
                var caseRemarkDto = caseReuslt.Item2;
                //获取资金流水
                var customerFundFlowList = await FundsHelper.Instance.GetCustomerFundFlowList(caseDto.CaseId);
                //卖方
                var sellerList = customerFundFlowList.Where(customerFundFlow => customerFundFlow.CustomerType == CustomerType.Seller)
                  .OrderByDescending(customerFundFlow => customerFundFlow.CreateDate).ToList();
                //买方
                var buyerList = customerFundFlowList.Where(customerFundFlow => customerFundFlow.CustomerType == CustomerType.Buyer)
                        .OrderByDescending(customerFundFlow => customerFundFlow.CreateDate).ToList();
                //账户余额
                var buyerAccount = await FundsHelper.Instance.GetAccount(caseDto.CaseId,AccountType.Buyer);
                var sellerAccount = await FundsHelper.Instance.GetAccount(caseDto.CaseId, AccountType.Seller);

                //实际应收应付
                var accountData = await FundsHelper.Instance.GetCaseTradeAmount(caseDto.CaseId);
                if (accountData == null)
                {
                    MessageBox.Show("未查询到相关应收应付信息，无法导出Excel","系统提示",MessageBoxButton.OK,MessageBoxImage.Warning);
                    return;
                }
                //创建Excel
                var wk = await CreateExcelHSSFWorkbook(caseDto, caseRemarkDto, sellerList, buyerList, buyerAccount, sellerAccount, accountData);
                this.loadingCtrl.IsBusy = false;
                var dialog = new System.Windows.Forms.SaveFileDialog();
                dialog.Filter = " xls files(*.xls)|*.xls|All files(*.*)|*.*";
                dialog.FileName = DateTime.Now.ToString("yyyy-MM-dd") + "资金台帐";
                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                    using (FileStream stm = File.OpenWrite(Convert.ToString(dialog.FileName)))
                    {
                        wk.Write(stm);
                        MessageBox.Show("提示：创建文件成功成功！","系统提示",MessageBoxButton.OK,MessageBoxImage.Information);
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("导出Excel发生系统异常.","系统异常",MessageBoxButton.OK,MessageBoxImage.Error);
            }
            finally
            {
                this.loadingCtrl.IsBusy = false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="caseDto"></param>
        /// <param name="caseRemarkDto"></param>
        /// <param name="sellerList"></param>
        /// <param name="buyerList"></param>
        /// <returns></returns>
        private Task<HSSFWorkbook> CreateExcelHSSFWorkbook(CaseDto caseDto,CaseRemark caseRemarkDto,
            List<CustomerFundFlow> sellerList, List<CustomerFundFlow> buyerList, 
            Account buyerAccount, Account sellerAccount,
            Tuple<IDictionary<OrderType, decimal>, Dictionary<OrderType, decimal>> accountData)
        {
            try
            {
                return Task.Run(() =>
                {
                    #region Excel基本信息
                    //创建工作薄
                    HSSFWorkbook wk = new HSSFWorkbook();
                    //创建一个名称为mySheet的表
                    ISheet sheet = wk.CreateSheet(m_CaseId);
                    //设置单元的宽度  
                    sheet.SetColumnWidth(0, 15 * 256);
                    sheet.SetColumnWidth(1, 35 * 256);
                    sheet.SetColumnWidth(2, 15 * 256);
                    sheet.SetColumnWidth(3, 35 * 256);
                    sheet.SetColumnWidth(4, 15 * 256);
                    sheet.SetColumnWidth(5, 35 * 256);
                    sheet.SetColumnWidth(6, 15 * 256);
                    sheet.SetColumnWidth(7, 35 * 256);
                    #endregion
                    //定义行数
                    int i = 0;

                    #region 案件信息创建标题
                    IRow rowTopOne = sheet.CreateRow(i);
                    rowTopOne.Height = 20 * 20;
                    ICell topOneCell = rowTopOne.CreateCell(i);
                    topOneCell.CellStyle = Getcellstyle(wk, stylexls.头);
                    topOneCell.SetCellValue("案件信息 - 案件编号 " + m_CaseId);
                    sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 0, 0, 7));
                    i++;
                    #endregion

                    #region  案件基本信息
                    for (var j = 1; j <= 3; j++)
                    {
                        IRow row = sheet.CreateRow(j);
                        row.Height = 20 * 20;
                        for (var cell = 0; cell <= 7; cell++)
                        {
                            ICell icell = row.CreateCell(cell);
                            icell.CellStyle = Getcellstyle(wk, stylexls.默认);
                            if (j == 1)
                            {
                                switch (cell)
                                {
                                    case 0:
                                        icell.SetCellValue("产权证号");
                                        break;
                                    case 2:
                                        icell.SetCellValue("物业地址");
                                        break;
                                    case 4:
                                        icell.SetCellValue("物业名称");
                                        break;
                                    case 6:
                                        icell.SetCellValue("环线位置");
                                        break;
                                    case 1:
                                        icell.SetCellValue(caseDto.TenementContract);
                                        break;
                                    case 3:
                                        icell.SetCellValue(caseDto.TenementAddress);
                                        break;
                                    case 5:
                                        icell.SetCellValue(caseRemarkDto.TenementName);
                                        break;
                                    case 7:
                                        var loopLinePosition = string.Empty;
                                        if (caseRemarkDto.LoopLinePosition != null)
                                        {
                                            //环线位置
                                            loopLinePosition = EnumHelper.GetEnumDesc((LinkLocation)Enum.Parse(typeof(LinkLocation), caseRemarkDto.LoopLinePosition));
                                        }
                                        icell.SetCellValue(loopLinePosition);
                                        break;

                                }
                            }
                            else if (j == 2)
                            {
                                switch (cell)
                                {
                                    case 0:
                                        icell.SetCellValue("卖方");
                                        break;
                                    case 2:
                                        icell.SetCellValue("买方");
                                        break;
                                    case 4:
                                        icell.SetCellValue("中介公司");
                                        break;
                                    case 6:
                                        icell.SetCellValue("经纪人");
                                        break;
                                    case 1:
                                        icell.SetCellValue(caseDto.BuyerName.Split(',')[0]);
                                        break;
                                    case 3:
                                        icell.SetCellValue(caseDto.SellerName.Split(',')[0]);
                                        break;
                                    case 5:
                                        icell.SetCellValue(caseDto.CompanyName);
                                        break;
                                    case 7:
                                        icell.SetCellValue(caseDto.AgencyName);
                                        break;

                                }
                            }
                            else if (j == 3)
                            {
                                switch (cell)
                                {
                                    case 0:
                                        icell.SetCellValue("交易类型");
                                        break;
                                    case 2:
                                        icell.SetCellValue("物业性质");
                                        break;
                                    case 4:
                                        icell.SetCellValue("住宅类型");
                                        break;
                                    case 6:
                                        icell.SetCellValue("..");
                                        break;
                                    case 1:
                                        icell.SetCellValue(EnumHelper.GetEnumDesc(caseRemarkDto.TradeType));
                                        break;
                                    case 3:
                                        icell.SetCellValue(EnumHelper.GetEnumDesc(caseRemarkDto.TenementNature));
                                        break;
                                    case 5:
                                        icell.SetCellValue(EnumHelper.GetEnumDesc(caseRemarkDto.ResideType));
                                        break;
                                    case 7:
                                        icell.SetCellValue("..");
                                        break;
                                }
                            }

                        }
                    }
                    #endregion

                    //确定新的行数
                    i += 3;

                    #region 资金信息创建标题
                    //创建标题
                    IRow rowTopTwo = sheet.CreateRow(i);
                    sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(i, i, 0, 7));
                    rowTopTwo.Height = 20 * 20;
                    ICell topTwoCell = rowTopTwo.CreateCell(0);
                    topTwoCell.CellStyle = Getcellstyle(wk, stylexls.头);
                    topTwoCell.SetCellValue("案件资金信息");
                    i++;
                    #endregion

                    #region 显示买方
                    IRow rowBuyer = sheet.CreateRow(i);
                    rowBuyer.Height = 20 * 20;
                    i++;
                    for (var cell = 0; cell <= 7; cell++)
                    {
                        ICell icellBuyer = rowBuyer.CreateCell(cell);
                        icellBuyer.CellStyle = Getcellstyle(wk, stylexls.默认);
                        switch (cell)
                        {
                            case 0:
                                icellBuyer.SetCellValue("买方");
                                break;
                            case 1:
                                icellBuyer.SetCellValue("收付款达成条件");
                                break;
                            case 2:
                                icellBuyer.SetCellValue("房款");
                                break;
                            case 3:
                                icellBuyer.SetCellValue("税费");
                                break;
                            case 4:
                                icellBuyer.SetCellValue("中介佣金");
                                break;
                            case 5:
                                icellBuyer.SetCellValue("装修补偿");
                                break;
                            case 6:
                                icellBuyer.SetCellValue("其他费用");
                                break;
                            case 7:
                                icellBuyer.SetCellValue("本次合计");
                                break;

                        }
                    }
                    decimal houseFundBuyer = 0;
                    decimal taxesBuyer = 0;
                    decimal brokerageBuyer = 0;
                    decimal decorateCompensationBuyer = 0;
                    decimal otherChargesBuyer = 0;
                    decimal amountBuyer = 0;
                    foreach (var item in buyerList)
                    {
                        IRow rowBuyerDetail = sheet.CreateRow(i);
                        rowBuyerDetail.Height = 20 * 20;
                        for (var cell = 0; cell <= 7; cell++)
                        {
                            ICell icellBuyer = rowBuyerDetail.CreateCell(cell);
                            icellBuyer.CellStyle = Getcellstyle(wk, stylexls.默认);
                            switch (cell)
                            {
                                case 0:
                                    icellBuyer.SetCellValue(string.Empty);
                                    break;
                                case 1:
                                    icellBuyer.SetCellValue(item.ConditionName);
                                    break;
                                case 2:
                                    houseFundBuyer += item.HouseFund;
                                    icellBuyer.CellStyle = Getcellstyle(wk, stylexls.数字, item.HouseFund);
                                    icellBuyer.SetCellValue(Convert.ToString(item.HouseFund));
                                    break;
                                case 3:
                                    taxesBuyer += item.Taxes;
                                    icellBuyer.CellStyle = Getcellstyle(wk, stylexls.数字, item.Taxes);
                                    icellBuyer.SetCellValue(Convert.ToString(item.Taxes));
                                    break;
                                case 4:
                                    brokerageBuyer += item.Brokerage;
                                    icellBuyer.CellStyle = Getcellstyle(wk, stylexls.数字, item.Brokerage);
                                    icellBuyer.SetCellValue(Convert.ToString(item.Brokerage));
                                    break;
                                case 5:
                                    decorateCompensationBuyer += item.DecorateCompensation;
                                    icellBuyer.CellStyle = Getcellstyle(wk, stylexls.数字, item.DecorateCompensation);
                                    icellBuyer.SetCellValue(Convert.ToString(item.DecorateCompensation));
                                    break;
                                case 6:
                                    otherChargesBuyer += item.OtherCharges;
                                    icellBuyer.CellStyle = Getcellstyle(wk, stylexls.数字, item.OtherCharges);
                                    icellBuyer.SetCellValue(Convert.ToString(item.OtherCharges));
                                    break;
                                case 7:
                                    amountBuyer += item.Amount;
                                    icellBuyer.CellStyle = Getcellstyle(wk, stylexls.数字, item.Amount);
                                    icellBuyer.SetCellValue(Convert.ToString(item.Amount));
                                    break;

                            }
                        }
                        //行数加
                        i++;
                    }
                    #endregion

                    #region 买方合计
                    IRow rowBuyerHZ = sheet.CreateRow(i);
                    rowBuyerHZ.Height = 20 * 20;
                    i++;
                    for (var cell = 0; cell <= 7; cell++)
                    {
                        ICell icellBuyer = rowBuyerHZ.CreateCell(cell);
                        icellBuyer.CellStyle = Getcellstyle(wk, stylexls.默认);
                        switch (cell)
                        {
                            case 0:
                                icellBuyer.SetCellValue("--");
                                break;
                            case 1:
                                icellBuyer.SetCellValue("合计");
                                break;
                            case 2:
                                icellBuyer.SetCellValue(houseFundBuyer == 0 ? "--" : Convert.ToString(houseFundBuyer));
                                break;
                            case 3:
                                icellBuyer.SetCellValue(taxesBuyer == 0 ? "--" : Convert.ToString(taxesBuyer));
                                break;
                            case 4:
                                icellBuyer.SetCellValue(brokerageBuyer == 0 ? "--" : Convert.ToString(brokerageBuyer));
                                break;
                            case 5:
                                icellBuyer.SetCellValue(decorateCompensationBuyer == 0 ? "--" : Convert.ToString(decorateCompensationBuyer));
                                break;
                            case 6:
                                icellBuyer.SetCellValue(otherChargesBuyer == 0 ? "--" : Convert.ToString(otherChargesBuyer));
                                break;
                            case 7:
                                icellBuyer.SetCellValue(amountBuyer == 0 ? "--" : Convert.ToString(amountBuyer));
                                break;

                        }
                    }
                    #endregion

                    #region 买方应收应付合计
                    //创建标题
                    IRow rowBuyerStatistics = sheet.CreateRow(i);
                    sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(i, i, 0, 7));
                    rowBuyerStatistics.Height = 20 * 20;
                    ICell accountBuyerStatisticsCell = rowBuyerStatistics.CreateCell(0);
                    accountBuyerStatisticsCell.CellStyle = Getcellstyle(wk, stylexls.头, isRight: true);
                    var txtBuyer = new StringBuilder();
                    var buyAccountTrade = accountData.Item2;
                    txtBuyer.Append("买方应收合计：" + buyerAccount.Balance.ToString("N") + "  ");
                    txtBuyer.Append("买方实收合计：" + buyAccountTrade[OrderType.Collection].ToString("N") + "  ");
                    txtBuyer.Append("买方应付合计：" + buyerAccount.Balance.ToString("N") + "  ");
                    txtBuyer.Append("买方实付合计：" + buyAccountTrade[OrderType.Payment].ToString("N") + "  ");
                    accountBuyerStatisticsCell.SetCellValue(Convert.ToString(txtBuyer));
                    i++;
                    #endregion

                    #region 买方剩余金额统计
                    //创建标题
                    IRow rowAccountBuyer = sheet.CreateRow(i);
                    sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(i, i, 0, 7));
                    rowAccountBuyer.Height = 20 * 20;
                    ICell accountBuyerCell = rowAccountBuyer.CreateCell(0);
                    accountBuyerCell.CellStyle = Getcellstyle(wk, stylexls.头,isRight:true);
                    accountBuyerCell.SetCellValue("买方剩余金额：" + buyerAccount.Balance.ToString("N")+"  ");
                    i++;
                    #endregion 

                    #region 显示卖家
                    IRow rowSeller = sheet.CreateRow(i);
                    rowSeller.Height = 20 * 20;
                    i++;
                    for (var cell = 0; cell <= 7; cell++)
                    {
                        ICell icellSeller = rowSeller.CreateCell(cell);
                        icellSeller.CellStyle = Getcellstyle(wk, stylexls.默认);
                        switch (cell)
                        {
                            case 0:
                                icellSeller.SetCellValue("卖方");
                                break;
                            case 1:
                                icellSeller.SetCellValue("收付款达成条件");
                                break;
                            case 2:
                                icellSeller.SetCellValue("房款");
                                break;
                            case 3:
                                icellSeller.SetCellValue("税费");
                                break;
                            case 4:
                                icellSeller.SetCellValue("中介佣金");
                                break;
                            case 5:
                                icellSeller.SetCellValue("装修补偿");
                                break;
                            case 6:
                                icellSeller.SetCellValue("其他费用");
                                break;
                            case 7:
                                icellSeller.SetCellValue("本次合计");
                                break;

                        }
                    }
                    decimal houseFundSeller = 0;
                    decimal taxesSeller = 0;
                    decimal brokerageSeller = 0;
                    decimal decorateCompensationSeller = 0;
                    decimal otherChargesSeller = 0;
                    decimal amountSeller = 0;
                    foreach (var item in buyerList)
                    {
                        IRow rowSellerDetail = sheet.CreateRow(i);
                        rowSellerDetail.Height = 20 * 20;
                        for (var cell = 0; cell <= 7; cell++)
                        {
                            ICell icellSeller = rowSellerDetail.CreateCell(cell);
                            icellSeller.CellStyle = Getcellstyle(wk, stylexls.默认);
                            switch (cell)
                            {
                                case 0:
                                    icellSeller.SetCellValue(string.Empty);
                                    break;
                                case 1:
                                    icellSeller.SetCellValue(item.ConditionName);
                                    break;
                                case 2:
                                    houseFundSeller += item.HouseFund;
                                    icellSeller.CellStyle = Getcellstyle(wk, stylexls.数字, item.HouseFund);
                                    icellSeller.SetCellValue(Convert.ToString(item.HouseFund));
                                    break;
                                case 3:
                                    taxesSeller += item.Taxes;
                                    icellSeller.CellStyle = Getcellstyle(wk, stylexls.数字, item.Taxes);
                                    icellSeller.SetCellValue(Convert.ToString(item.Taxes));
                                    break;
                                case 4:
                                    brokerageSeller += item.Brokerage;
                                    icellSeller.CellStyle = Getcellstyle(wk, stylexls.数字, item.Brokerage);
                                    icellSeller.SetCellValue(Convert.ToString(item.Brokerage));
                                    break;
                                case 5:
                                    decorateCompensationSeller += item.DecorateCompensation;
                                    icellSeller.CellStyle = Getcellstyle(wk, stylexls.数字, item.DecorateCompensation);
                                    icellSeller.SetCellValue(Convert.ToString(item.DecorateCompensation));
                                    break;
                                case 6:
                                    otherChargesSeller += item.OtherCharges;
                                    icellSeller.CellStyle = Getcellstyle(wk, stylexls.数字, item.OtherCharges);
                                    icellSeller.SetCellValue(Convert.ToString(item.OtherCharges));
                                    break;
                                case 7:
                                    amountSeller += item.Amount;
                                    icellSeller.CellStyle = Getcellstyle(wk, stylexls.数字, item.Amount);
                                    icellSeller.SetCellValue(Convert.ToString(item.Amount));
                                    break;

                            }
                        }
                        //行数加
                        i++;
                    }
                    #endregion

                    #region 卖方合计
                    IRow rowSellerHZ = sheet.CreateRow(i);
                    rowSellerHZ.Height = 20 * 20;
                    i++;
                    for (var cell = 0; cell <= 7; cell++)
                    {
                        ICell icellSeller = rowSellerHZ.CreateCell(cell);
                        icellSeller.CellStyle = Getcellstyle(wk, stylexls.默认);
                        switch (cell)
                        {
                            case 0:
                                icellSeller.SetCellValue("--");
                                break;
                            case 1:
                                icellSeller.SetCellValue("合计");
                                break;
                            case 2:
                                icellSeller.SetCellValue(houseFundSeller == 0 ? "--" : Convert.ToString(houseFundSeller));
                                break;
                            case 3:
                                icellSeller.SetCellValue(taxesSeller == 0 ? "--" : Convert.ToString(taxesSeller));
                                break;
                            case 4:
                                icellSeller.SetCellValue(brokerageSeller == 0 ? "--" : Convert.ToString(brokerageSeller));
                                break;
                            case 5:
                                icellSeller.SetCellValue(decorateCompensationSeller == 0 ? "--" : Convert.ToString(decorateCompensationSeller));
                                break;
                            case 6:
                                icellSeller.SetCellValue(otherChargesSeller == 0 ? "--" : Convert.ToString(otherChargesSeller));
                                break;
                            case 7:
                                icellSeller.SetCellValue(amountSeller == 0 ? "--" : Convert.ToString(amountSeller));
                                break;

                        }
                    }
                    #endregion

                    #region 卖方应收应付合计
                    IRow rowSellerStatistics = sheet.CreateRow(i);
                    sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(i, i, 0, 7));
                    rowSellerStatistics.Height = 20 * 20;
                    ICell accountSellerStatisticsCell = rowSellerStatistics.CreateCell(0);                
                    var txtSeller = new StringBuilder();
                    var sellAccountTrade = accountData.Item1;
                    txtSeller.Append("卖方应收合计：" + sellerAccount.Balance.ToString("N") + "  ");
                    txtSeller.Append("卖方应收合计：" + sellAccountTrade[OrderType.Collection].ToString("N") + "  ");
                    txtSeller.Append("卖方应付合计：" + sellerAccount.Balance.ToString("N") + "  ");
                    txtSeller.Append("卖方应付合计：" + sellAccountTrade[OrderType.Collection].ToString("N") + "  ");
                    accountSellerStatisticsCell.SetCellValue(Convert.ToString(txtSeller));
                    accountSellerStatisticsCell.CellStyle = Getcellstyle(wk, stylexls.头, isRight: true);
                    i++;
                    #endregion

                    #region 卖方剩余金额统计
                    //创建标题
                    IRow rowAccountSeller = sheet.CreateRow(i);
                    sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(i, i, 0, 7));
                    rowAccountSeller.Height = 20 * 20;
                    ICell accountSellerCell = rowAccountSeller.CreateCell(0);
                    accountSellerCell.CellStyle = Getcellstyle(wk, stylexls.头, isRight: true);
                    accountSellerCell.SetCellValue("卖方剩余金额：" + sellerAccount.Balance.ToString("N")+"  ");
                    i++;
                    #endregion 

                  return wk;

                });
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// 页面事件加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                PageHelper.PageActiveLoadingControl(loadingCtrl, "加载数据,请稍后...");
                LoadPageData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("加载数据发生系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }


        /// <summary>
        /// 获取页面数据
        /// </summary>
        private async void LoadPageData()
        {
            try
            {
                //获取案件信息/附属对象
                var caseReuslt = await FundsHelper.Instance.GetCaseData(m_CaseId);
                var caseDto = caseReuslt.Item1;
                var caseRemarkDto = caseReuslt.Item2;
                //初始化案件信息
                ctrlCaseInfo.InitCtrlData(caseDto, caseRemarkDto);

                //获取资金流水
                var customerFundFlowList = await FundsHelper.Instance.GetCustomerFundFlowList(caseDto.CaseId);
                if (customerFundFlowList == null) return;
                ctrlTransactionInfo.InitCtrlData(customerFundFlowList);

                //获取账户
                var buyerAccount = await FundsHelper.Instance.GetAccount(m_CaseId,AccountType.Buyer);
                var sellerAccount = await FundsHelper.Instance.GetAccount(m_CaseId, AccountType.Seller);
                ctrlTransactionInfo.InitCtrlAccountData(buyerAccount, sellerAccount);
                
                //设置实收实付
                var tradeAmountData = await FundsHelper.Instance.GetCaseTradeAmount(m_CaseId);
                ctrlTransactionInfo.InitTradeAmountData(tradeAmountData);

                loadingCtrl.IsBusy = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.loadingCtrl.IsBusy = false;
            }
        }

        #region 定义单元格常用到样式
        public enum stylexls
        {
            头,
            时间,
            数字,
            钱,
            百分比,
            中文大写,
            科学计数法,
            默认
        }
        static ICellStyle Getcellstyle(IWorkbook wb, stylexls str,decimal num=0,bool isRight=false)
        {
            ICellStyle cellStyle = wb.CreateCellStyle();

            //也可以一种字体，写一些公共属性，然后在下面需要时加特殊的  
            IFont font12 = wb.CreateFont();
            font12.FontHeightInPoints = 10;
            font12.FontName = "微软雅黑";
            font12.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            font12.Color = NPOI.HSSF.Util.HSSFColor.White.Index;

            IFont font = wb.CreateFont();
            font.FontName = "微软雅黑";
            font.Color = NPOI.HSSF.Util.HSSFColor.White.Index;

            IFont fontGreen = wb.CreateFont();
            fontGreen.FontName = "微软雅黑";
            fontGreen.Color = NPOI.HSSF.Util.HSSFColor.Green.Index;

            IFont fontRed = wb.CreateFont();
            fontRed.FontName = "微软雅黑";
            fontRed.Color = NPOI.HSSF.Util.HSSFColor.Red.Index;

            //边框  
            cellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            cellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            cellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            cellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;

            //边框颜色  
            cellStyle.BottomBorderColor = HSSFColor.OliveGreen.Brown.Index;
            cellStyle.TopBorderColor = HSSFColor.OliveGreen.Brown.Index;
            cellStyle.LeftBorderColor = HSSFColor.OliveGreen.Brown.Index;
            cellStyle.RightBorderColor = HSSFColor.OliveGreen.Brown.Index;

            cellStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey40Percent.Index;
            //- 这个是填充的模式，可以是网格、花式等。如果需要填充单色，请使用：SOLID_FOREGROUND
            cellStyle.FillPattern = NPOI.SS.UserModel.FillPattern.SolidForeground;

            //水平对齐  
            cellStyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Left;
            if (isRight) cellStyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Right;

            //垂直对齐  
            cellStyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;

            //自动换行  
            cellStyle.WrapText = true;

            //缩进;当设置为1时，前面留的空白太大了。希旺官网改进。或者是我设置的不对  
            cellStyle.Indention = 0;

            //上面基本都是设共公的设置  
            //下面列出了常用的字段类型  
            switch (str)
            {
                case stylexls.头:
                    cellStyle.SetFont(font12);
                    break;
                case stylexls.数字:
                    cellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("0.00");                   
                    if (num < 0){
                        cellStyle.SetFont(fontRed);
                    }
                    else{
                        cellStyle.SetFont(fontGreen);
                    }
                    break;
                case stylexls.默认:
                    cellStyle.SetFont(font);
                    break;
            }
            return cellStyle;
        }
        #endregion  
    }
}
