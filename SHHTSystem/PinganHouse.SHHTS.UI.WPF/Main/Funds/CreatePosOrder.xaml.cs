﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds
{
    /// <summary>
    /// CreatePosOrder.xaml 的交互逻辑
    /// </summary>
    public partial class CreatePosOrder : Page
    {
        private string _caseId = string.Empty;
        private string _orderType = string.Empty;
        private List<string> _sysNoList = new List<string>();
        private IList<MissCollectionTrade> _collectionTradeList = new List<MissCollectionTrade>();
        private decimal _totalAmount = 0;

        public CreatePosOrder(string caseId, string orderType = "pos")
        {
            InitializeComponent();

            _caseId = caseId;
            _orderType = orderType;

            try
            {
                var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(_caseId);

                var caseRemarkDto = CaseProxyService.GetInstanse().GetRemarkBySysNo(caseDto.SysNo);

                caseInfoCtrl.InitCtrlData(caseDto, caseRemarkDto);


                if (_orderType == "pos")
                {
                    rdoPos.IsChecked = true;
                    rdoBank.IsChecked = false;
                }
                else if (_orderType == "bank")
                {
                    rdoPos.IsChecked = false;
                    rdoBank.IsChecked = true;

                }
                else
                {
                    rdoPos.IsChecked = false;
                    rdoBank.IsChecked = false;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void rdoOrderType_OnChecked(object sender, RoutedEventArgs e)
        {
            if (rdoPos.IsChecked == true)
            {
                panelPoseOrder.Visibility = Visibility.Visible;
                panelBankOrder.Visibility = Visibility.Collapsed;
                panelTransferOrder.Visibility = Visibility.Collapsed;
                panelTransferOrderStep2.Visibility = Visibility.Collapsed;

                btnPrevious.Visibility = Visibility.Collapsed;
                btnNext.Visibility = Visibility.Collapsed;
                btnSave.Visibility = Visibility.Visible;
            }
            else if (rdoBank.IsChecked == true)
            {
                panelPoseOrder.Visibility = Visibility.Collapsed;
                panelBankOrder.Visibility = Visibility.Visible;
                panelTransferOrder.Visibility = Visibility.Collapsed;
                panelTransferOrderStep2.Visibility = Visibility.Collapsed;

                btnPrevious.Visibility = Visibility.Collapsed;
                btnNext.Visibility = Visibility.Collapsed;
                btnSave.Visibility = Visibility.Visible;
            }
            else if (rdoTransfer.IsChecked == true)
            {
                panelPoseOrder.Visibility = Visibility.Collapsed;
                panelBankOrder.Visibility = Visibility.Collapsed;
                panelTransferOrder.Visibility = Visibility.Visible;
                panelTransferOrderStep2.Visibility = Visibility.Collapsed;

                btnPrevious.Visibility = Visibility.Collapsed;
                btnSave.Visibility = Visibility.Collapsed;
                btnNext.Visibility = Visibility.Visible;
            }
        }

        private void BtnSave_OnClick(object sender, RoutedEventArgs e)
        {
            if (cbAccountType.SelectedItem == null)
            {
                MessageBox.Show("请选择付款归属", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (rdoPos.IsChecked == true)
            {
                //创建POS订单时的确定事件
                if (cbPaymentFor.SelectedItem == null)
                {
                    MessageBox.Show("请选择付款性质", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                Decimal amount = 0;

                if (!Decimal.TryParse(txtAmount.Text.Trim(), out amount) || amount < 0)
                {
                    MessageBox.Show("付款金额必须大于0", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                
                if (txtAmount.Text.Trim().IndexOf('.')>-1 && ((txtAmount.Text.Trim().Length - txtAmount.Text.Trim().IndexOf('.') - 1) > 2))
                {
                    MessageBox.Show("付款金额的小数位数不能大于两位", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (amount >= 10000000000)
                {
                    MessageBox.Show("付款金额必须小于100亿", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                
                var resultPos = TradeServiceProxy.SubmitCollection((AccountType)int.Parse((cbAccountType.SelectedItem as ComboBoxItem).Tag.ToString())
                    , _caseId
                    , amount
                    , (OrderBizType)int.Parse((cbPaymentFor.SelectedItem as ComboBoxItem).Tag.ToString())
                    , PaymentChannel.POS
                    , ""
                    , LoginHelper.CurrentUser.SysNo);


                if (resultPos.Success)
                {
                    MessageBox.Show("POS订单创建成功。", "系统提示", MessageBoxButton.OK);
                    PageHelper.PageNavigateHelper(this, new FundSettlementListPage());
                }
                else
                {
                    MessageBox.Show("保存失败：" + resultPos.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else if (rdoTransfer.IsChecked == true)
            {
                //创建转账收款订单的确定事件
                if (_totalAmount == 0)
                {
                    MessageBox.Show("请选择转账流水", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                else
                {
                    foreach (var sysNo in _sysNoList)
                    {
                        foreach (var trade in _collectionTradeList)
                        {
                            if (sysNo == trade.SysNo.ToString())
                            {
                                var resultTransfer = TradeServiceProxy.SubmitTransferCollection(long.Parse(sysNo)
                                    , (AccountType)int.Parse((cbAccountType.SelectedItem as ComboBoxItem).Tag.ToString())
                                    , _caseId
                                    , (OrderBizType)int.Parse((cbPaymentFor2.SelectedItem as ComboBoxItem).Tag.ToString())
                                    , LoginHelper.CurrentUser.SysNo);
                                if (!resultTransfer.Success)
                                {
                                    MessageBox.Show("保存失败：" + resultTransfer.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                                    return;
                                }
                            }
                        }
                    }

                    MessageBox.Show("转账收款订单创建成功。", "系统提示", MessageBoxButton.OK);
                    PageHelper.PageNavigateHelper(this, new FundSettlementListPage());

                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, new QueryCaseInfo(_caseId));
        }

        private void BtnSearchBankAccount_OnClick(object sender, RoutedEventArgs e)
        {
            _collectionTradeList = TradeServiceProxy.GetMissTransferTradeByAccountNo(txtAccountNo.Text.Trim());
            gridBankStatement.DataContext = _collectionTradeList;
            _sysNoList = new List<string>();
        }

        private void ChkBankStatement_OnClick(object sender, RoutedEventArgs e)
        {
            CheckBox checkBox = sender as System.Windows.Controls.CheckBox;
            string sysNo = checkBox.Tag.ToString();
            var bl = checkBox.IsChecked;
            if (bl == true)
            {
                _sysNoList.Add(sysNo);
            }
            else
            {
                _sysNoList.Remove(sysNo);
            }
        }

        /// <summary>
        /// 下一步
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnNext_OnClick(object sender, RoutedEventArgs e)
        {
            if (rdoTransfer.IsChecked == true)
            {
                panelTransferOrder.Visibility = Visibility.Collapsed;
                panelTransferOrderStep2.Visibility = Visibility.Visible;

                btnPrevious.Visibility = Visibility.Visible;
                btnSave.Visibility = Visibility.Visible;
                btnNext.Visibility = Visibility.Collapsed;

                _totalAmount = _sysNoList.Sum(sysNo => _collectionTradeList.Where(trade => sysNo == trade.SysNo.ToString()).Sum(trade => trade.Amount));
                lblAmount.Content = _totalAmount.ToString("C");
            }

        }

        private void BtnPrevious_OnClick(object sender, RoutedEventArgs e)
        {
            if (rdoTransfer.IsChecked == true)
            {
                panelTransferOrder.Visibility = Visibility.Visible;
                panelTransferOrderStep2.Visibility = Visibility.Collapsed;

                btnPrevious.Visibility = Visibility.Collapsed;
                btnSave.Visibility = Visibility.Collapsed;
                btnNext.Visibility = Visibility.Visible;


            }

        }
    }
}
