﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds
{
    /// <summary>
    /// PosDetailInfoCancelDialog.xaml 的交互逻辑
    /// </summary>
    public partial class PosDetailInfoCancelDialog : Window
    {
        public delegate void SaveHandle();
        public event SaveHandle SaveEvent;

        private string _tradeNo = string.Empty;
        private BillDetail _bill = null;

        public PosDetailInfoCancelDialog(BillDetail bill)
        {
            InitializeComponent();
            _tradeNo = bill.TradeNo;
            _bill = bill;

            txtTerminalNo.Text = bill.TerminalNo;
            txtPosSerialNo.Text = bill.BillNo;
            txtCardNo.Text = bill.PayAccount;
            txtAccountName.Text = bill.Payer;
            txtAmount.Text = (-1 * bill.Amount).ToString();
            datePicker.SelectedDate = bill.BillDate;


        }

        private void BtnCancel_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnSave_OnClick(object sender, RoutedEventArgs e)
        {
            var msgResult = MessageBox.Show("确认要撤销该订单吗？", "系统确认", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
            if (msgResult == MessageBoxResult.No) return;

            var errorMessage = string.Empty;

            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(@"^[0-9]\d*$");
            var terminalNo = txtTerminalNo.Text.Trim();
            if (string.IsNullOrEmpty(terminalNo))
            {
                errorMessage += "终端编号不能为空。\r\n";
            }
            else if (reg.IsMatch(terminalNo) == false)
            {
                errorMessage += "终端号必须是数字。\r\n";
            }
            var postSerialNo = txtPosSerialNo.Text.Trim();
            if (string.IsNullOrEmpty(postSerialNo))
            {
                errorMessage += "POS流水号不能为空。\r\n";
            }
            else if (reg.IsMatch(postSerialNo) == false)
            {
                errorMessage += "POS参考号必须是数字。\r\n";
            }
            var cardNo = txtCardNo.Text.Trim();
            if (string.IsNullOrEmpty(cardNo))
            {
                errorMessage += "银行卡号不能为空。\r\n";
            }
            var accountName = txtAccountName.Text.Trim();
            if (string.IsNullOrEmpty(accountName))
            {
                errorMessage += "开户名不能为空。\r\n";
            }
            decimal amount = 0;
            decimal.TryParse(txtAmount.Text.Trim(), out amount);
            if (amount == 0)
            {
                errorMessage += "金额不能于0。\r\n";
            }
            var date = datePicker.SelectedDate;
            if (date == null)
            {
                errorMessage += "日期不能为空\r\n";
            }

            if (!String.IsNullOrEmpty(errorMessage))
            {
                MessageBox.Show(errorMessage, "警告", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {

                _bill.TerminalNo = terminalNo;
                _bill.TradeNo = postSerialNo;
                _bill.PayAccount = cardNo;
                _bill.Payer = accountName;
                _bill.Amount = amount;
                _bill.BillDate = (DateTime)date;
                _bill.BillType = BillType.ExpenedCancelled;


                var result = TradeServiceProxy.CancelCollectionTrade(_tradeNo, _bill, LoginHelper.CurrentUser.SysNo);
                if (result.Success)
                {
                    MessageBox.Show("撤销成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    SaveEvent();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
    }
}
