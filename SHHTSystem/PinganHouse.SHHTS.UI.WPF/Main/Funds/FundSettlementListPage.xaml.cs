﻿using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy;
using System.Collections;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Input;
using PinganHouse.SHHTS.UI.WPF.Main.Funds.Win;
using PinganHouse.SHHTS.UI.WPF.Main.PreCheck;
using Button = System.Windows.Controls.Button;
using Label = System.Windows.Controls.Label;
using MessageBox = System.Windows.MessageBox;
using TextBox = System.Windows.Controls.TextBox;
using PinganHouse.SHHTS.AccessControl.Proxy;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds
{
    /// <summary>
    /// FundSettlementListPage.xaml 的交互逻辑        //订单
    //资金流水
    //资金台账
    //票据管理
    //退款管理
    /// </summary>
    public partial class FundSettlementListPage : Page
    {
        private readonly int PageSize = 20;
        List<Button> switchBtns;
        int currentSwitchBtnIndex = -1;
        string[] columnCategray;

        IList dataGridSouceList = null;

        public FundSettlementListPage(int Index = 0)
        {
            InitializeComponent();
            switchBtns = new List<Button>() { btnOrder, btnMoneyFlow, btnMoneyPlatformBook, btnBillManage, btnReturnMange };
            columnCategray = new string[] 
            { 
                ",1,5,", ",2,",",2,",",1,2,5,",",3,",
                ",1,", ",1,", ",1,",  ",1,", ",1,", ",1,", ",1,", "1",
                ",2,", ",2,", ",2,", ",2,", ",2,", ",2,", ",2,", ",2,", ",2,", 
                ",3,", ",3,",  ",3,", ",3,", 
                ",4,", ",4,", ",4,", ",4,", ",4,", ",4,", ",4,",",4,",",4,", ",4,", 
                ",5,", ",5,", ",5,", ",5,", ",5,", ",5,", 
                ",1,4,5,",",4,"
            };
            this.pageControl.PageNumChanged = this.GridPager_PagerIndexChanged;
            switchBtnClicked(btnOrder, null);
            switch(Index)
            {
                case 1:
                    switchBtnClicked(btnMoneyFlow, null);
                    break;
                case 2:
                    switchBtnClicked(btnMoneyPlatformBook, null);
                    break;
                case 3:
                    switchBtnClicked(btnBillManage, null);
                    break;
                case 4:
                    switchBtnClicked(btnReturnMange, null);
                    break;
                default:
                    break;
            }
        }

        private Window parentWindow = null;
        private void Page_Body_Loaded(object sender, RoutedEventArgs e)
        {
            parentWindow = Utils.GetParentWindow(this);
            if (parentWindow != null) parentWindow.KeyDown += this.FundSettlementListPage_OnKeyDown;
        }
        private void FundSettlementListPage_OnUnloaded(object sender, RoutedEventArgs e)
        {
            if (parentWindow != null) parentWindow.KeyDown -= this.FundSettlementListPage_OnKeyDown;
            parentWindow = null;
        }

        private void FundSettlementListPage_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F5)
            {
                startLoadDataAsyn();
            }
        }

        #region switch buttons
        private void switchBtnClicked(object sender, RoutedEventArgs e)
        {
            Button selectedBtn = (Button)sender;
            if (currentSwitchBtnIndex == switchBtns.IndexOf(selectedBtn)) return;
            //init

            if (!CheckPermission(selectedBtn.Name))
            {
                MessageBox.Show("您不具备此操作权限.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            AddOrderArea.Visibility = Visibility.Collapsed;
            SearchLabel1.Visibility = Visibility.Collapsed;
            SearchLabel2.Visibility = Visibility.Collapsed;
            SearchCombox2.Visibility = Visibility.Collapsed;
            SearchCombox1.Visibility = Visibility.Collapsed;
            SearchCombox1.SelectionChanged -= this.SearchCombox1_SelectionChanged;
            SearchTB1.Visibility = Visibility.Collapsed;
            SearchTB2.Visibility = Visibility.Collapsed;
            SearchTB3.Visibility = Visibility.Collapsed; 
            SearchTB4.Visibility = Visibility.Collapsed;
            SearchTB1.Text = null;
            SearchTB4.Text = null;
            SearchTB3.Text = null;
            SearchTB2.Text = null;
            dateStart.Visibility = Visibility.Collapsed;
            dateEnd.Visibility = Visibility.Collapsed;

            foreach (Button btn in switchBtns)
            {
                if (btn != selectedBtn) btn.Style = (Style)FindResource("ShhtsBtGray");
            }
            selectedBtn.Style = (Style)FindResource("ShhtsBtBlue");

            if (selectedBtn == btnOrder)
            {
                AddOrderArea.Visibility = Visibility.Visible;
                SearchLabel1.Visibility = Visibility.Visible;
                SearchLabel2.Visibility = Visibility.Visible;
                SearchLabel1.Content = "订单类型";
                SearchLabel2.Content = "支付方式";

                SearchCombox1.Visibility = Visibility.Visible;
                SearchCombox1.SelectionChanged += this.SearchCombox1_SelectionChanged;
                List<EnumHelper> list = EnumHelper.EnumToEnumHelperList(typeof(OrderType));
                EnumHelper eh = new EnumHelper();
                eh.DisplayMember = "全部类型订单";
                eh.ValueMember = -1;
                list.Insert(0, eh);
                SearchCombox1.ItemsSource = list;
                SearchCombox1.DisplayMemberPath = "DisplayMember";
                SearchCombox1.SelectedValuePath = "ValueMember";
                SearchCombox1.SelectedIndex = 0;

                SearchCombox2.Visibility = Visibility.Visible;
                List<EnumHelper> list2 = EnumHelper.EnumToEnumHelperList(typeof(PaymentChannel));
                EnumHelper eh2 = new EnumHelper();
                eh2.DisplayMember = "全部支付方式";
                eh2.ValueMember = -1;
                list2.Insert(0, eh2);
                SearchCombox2.ItemsSource = list2;
                SearchCombox2.DisplayMemberPath = "DisplayMember";
                SearchCombox2.SelectedValuePath = "ValueMember";
                SearchCombox2.SelectedIndex = 0;

                SearchTB1.Visibility = Visibility.Visible;
                setTextBoxHint(SearchTB1, "订单编号");
                SearchTB2.Visibility = Visibility.Visible;
                setTextBoxHint(SearchTB2, "创建人员");
                SearchTB3.Visibility = Visibility.Visible;
                setTextBoxHint(SearchTB3, "案件编号");
                dateStart.Visibility = Visibility.Visible;
                dateEnd.Visibility = Visibility.Visible;
            }
            else if (selectedBtn == btnMoneyFlow)
            {
                SearchLabel1.Visibility = Visibility.Visible;
                SearchLabel1.Content = "支付方式";

                SearchCombox1.Visibility = Visibility.Visible;
                List<EnumHelper> list = EnumHelper.EnumToEnumHelperList(typeof(PaymentChannel));
                EnumHelper eh = new EnumHelper();
                eh.DisplayMember = "全部支付方式";
                eh.ValueMember = -1;
                list.Insert(0, eh);
                SearchCombox1.ItemsSource = list;
                SearchCombox1.DisplayMemberPath = "DisplayMember";
                SearchCombox1.SelectedValuePath = "ValueMember";
                SearchCombox1.SelectedIndex = 0;

                SearchTB1.Visibility = Visibility.Visible;
                setTextBoxHint(SearchTB1, "流水号");
                SearchTB2.Visibility = Visibility.Visible;
                setTextBoxHint(SearchTB2, "订单编号");
                SearchTB3.Visibility = Visibility.Visible;
                setTextBoxHint(SearchTB3, "案件编号");
                SearchTB4.Visibility = Visibility.Visible;
                setTextBoxHint(SearchTB4, "银行卡号");

                dateStart.Visibility = Visibility.Visible;
                dateEnd.Visibility = Visibility.Visible;
            }
            else if (selectedBtn == btnMoneyPlatformBook)
            {
                SearchTB1.Visibility = Visibility.Visible;
                setTextBoxHint(SearchTB1, "案件编号");
                SearchTB2.Visibility = Visibility.Visible;
                setTextBoxHint(SearchTB2, "产权证号");
                SearchTB3.Visibility = Visibility.Visible;
                setTextBoxHint(SearchTB3, "物业地址");

                dateStart.Visibility = Visibility.Visible;
                dateEnd.Visibility = Visibility.Visible;
            }
            else if (selectedBtn == btnBillManage)
            {
                SearchCombox1.Visibility = Visibility.Visible;
                List<EnumHelper> list = EnumHelper.EnumToEnumHelperList(typeof(ReceiptStatus));
                EnumHelper eh = new EnumHelper();
                eh.DisplayMember = "全部状态";
                eh.ValueMember = -1;
                list.Insert(0, eh);
                SearchCombox1.ItemsSource = list;
                SearchCombox1.DisplayMemberPath = "DisplayMember";
                SearchCombox1.SelectedValuePath = "ValueMember";
                SearchCombox1.SelectedIndex = 0;

                SearchTB1.Visibility = Visibility.Visible;
                setTextBoxHint(SearchTB1, "收据编号");

            }
            else if (selectedBtn == btnReturnMange)
            {
                SearchTB1.Visibility = Visibility.Visible;
                setTextBoxHint(SearchTB1, "订单编号");
                SearchTB2.Visibility = Visibility.Visible;
                setTextBoxHint(SearchTB2, "创建人员");

                dateStart.Visibility = Visibility.Visible;
                dateEnd.Visibility = Visibility.Visible;
            }

            currentSwitchBtnIndex = switchBtns.IndexOf(selectedBtn);
            string column = string.Format(",{0},", currentSwitchBtnIndex + 1);
            int columnCount = dataGrid.Columns.Count - 1;
            for (int i = 0; i < columnCount; i++)
            {
                DataGridColumn dgc = dataGrid.Columns[i];
                string isVisi = columnCategray[i];
                if (isVisi.Contains(column)) dgc.Visibility = Visibility.Visible;
                else dgc.Visibility = Visibility.Collapsed;
            }
            this.pageControl.PageIndex = 1;
        }


        #region 权限

        private bool CheckPermission(string tag)
        {
            return PermissionProxyService.GetInstance().Authorize(GetButtonPermissionNo(tag));
        }

        private string GetButtonPermissionNo(string tag)
        {
            if (string.IsNullOrEmpty(tag))
                throw new Exception("权限标签未设置");
            switch (tag)
            {
                case "btnOrder": return "10022";
                case "btnMoneyFlow": return "10023";
                case "btnMoneyPlatformBook": return "10024";
                case "btnBillManage": return "10025";
                case "btnReturnMange": return "10026";
                default: throw new Exception("权限标签未设置");
            }
        }

        #endregion


        private void SearchCombox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0) return;
            EnumHelper eh = (EnumHelper)e.AddedItems[0];
            if (eh.ValueMember == (long)OrderType.Collection)
            {
                SearchCombox2.IsEnabled = true;
            }
            else
            {
                SearchCombox2.SelectedValue = (long)1;
                SearchCombox2.IsEnabled = false;
            }
        }
        private void setTextBoxHint(TextBox tb, string hint)
        {
            Style style = tb.Style;
            if (style != null)
            {
                ResourceDictionary rd = style.Resources;
                VisualBrush re = (VisualBrush)rd["CueBannerBrush"];
                if (re != null)
                {
                    Label hintLabel = (Label)re.Visual;
                    if (hintLabel != null) hintLabel.Content = hint;
                }
            }
        }
        #endregion
        //创建订单
        private void btnCreateOrder_Click(object sender, RoutedEventArgs e)
        {
            var queryCase = new QueryCaseInfo();
            PageHelper.PageNavigateHelper(this, queryCase);
        }

        //搜索
        private void SearchBtn_Click(object sender, RoutedEventArgs e)
        {
            startLoadDataAsyn();
        }

        private void GridPager_PagerIndexChanged(int pageIndex)
        {
            startLoadDataAsyn();
        }
        private void startLoadDataAsyn()
        {
            //获取搜索条件
            long combox1 = (long)SearchCombox1.SelectedValue;
            long combox2 = (long)SearchCombox2.SelectedValue;
            string tb1 = string.IsNullOrEmpty(this.SearchTB1.Text.Trim()) ? null : this.SearchTB1.Text.Trim();
            string tb2 = string.IsNullOrEmpty(this.SearchTB2.Text.Trim()) ? null : this.SearchTB2.Text.Trim();
            string tb3 = string.IsNullOrEmpty(this.SearchTB3.Text.Trim()) ? null : this.SearchTB3.Text.Trim();
            string tb4 = string.IsNullOrEmpty(this.SearchTB4.Text.Trim()) ? null : this.SearchTB4.Text.Trim();
            DateTime startTime = DateTime.MinValue;
            if (!string.IsNullOrEmpty(dateStart.Text)) DateTime.TryParse(dateStart.Text, out startTime);
            DateTime endTime = DateTime.MinValue;
            if (!string.IsNullOrEmpty(dateEnd.Text)) DateTime.TryParse(dateEnd.Text, out endTime);
            DateTime? start = null;
            DateTime? end = null;
            if (startTime == DateTime.MinValue) start = null;
            else start = startTime;
            if (endTime == DateTime.MinValue) end = null;
            else
            {
                endTime = endTime.AddHours(23).AddMinutes(59);
                end = endTime;
            }
            int pageIndex = this.pageControl.PageIndex;
            Thread thread = new Thread(delegate()
            {
                loadData(pageIndex, combox1, combox2, tb1, tb2, tb3,tb4, start, end);
            });
            thread.IsBackground = true;

            PageHelper.PageActiveLoadingControl(this.loading, "正在加载数据,请稍后...");
            thread.Start();
        }
        private void loadData(int pageIndex, long combox1, long combox2, string tb1, string tb2, string tb3, string tb4, DateTime? start, DateTime? end)
        {
            string errorMsg = null;
            int pageCount = 0;
            dataGridSouceList = null;
            //获取订单
            try
            {
                switch (currentSwitchBtnIndex)
                {
                    case 0:
                        {
                            OrderType? ot = null;
                            if (combox1 != -1) ot = (OrderType)combox1;
                            PaymentChannel? pc = null;
                            if (combox2 != -1) pc = (PaymentChannel)combox2;
                            if (ot != OrderType.Collection) pc = null;
                            dataGridSouceList = (IList)TradeServiceProxy.GetOrders(pageIndex, PageSize, out pageCount,
                                ConfigHelper.GetCurrentReceptionCenter(), tb3, null, ot, pc, 
                                tb1, tb2, start, end);
                        }
                        break;
                    case 1:
                        {
                            //tb4
                            PaymentChannel? pc = null;
                            if (combox1 != -1) pc = (PaymentChannel)combox1;
                           dataGridSouceList = (IList) TradeServiceProxy.GetTrades(pageIndex,PageSize,out pageCount,ConfigHelper.GetCurrentReceptionCenter(),
                               pc,tb2,tb1,tb3,tb4,start,end);
                        }
                        break;
                    case 2:
                        {
                            dataGridSouceList = (IList)AccountServiceProxy.GetAccounts(pageIndex, PageSize, out pageCount, ConfigHelper.GetCurrentReceptionCenter(),
                                tb1, tb2, tb3, start, end);
                        }
                        break;
                    case 3:
                    {
                        ReceiptStatus? rs = null;
                        if (combox1 != -1) rs = (ReceiptStatus) combox1;
                            dataGridSouceList = (IList)ReceiptProxyService.GetInstanse().GetPaginatedList(pageIndex, PageSize, ref pageCount, ConfigHelper.GetCurrentReceptionCenter(),
                               rs, tb1);
                        }
                        break;
                    case 4:
                    {
                            dataGridSouceList = (IList)TradeServiceProxy.GetRefundOrders(pageIndex, PageSize, out pageCount, ConfigHelper.GetCurrentReceptionCenter(), tb1, tb2, start, end);
                        }
                        break;
                    default:
                        break;
                }
               
            }
            catch (Exception ex)
            {
                errorMsg = ex.ToString();
            }
            //主线程更新数据
            this.Dispatcher.Invoke(new Action(delegate()
                {
                    loading.IsBusy = false;
                    if (errorMsg != null)
                    {
                        MessageBox.Show("数据获取错误：" + errorMsg, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
                        this.dataGrid.DataContext = null;
                    }
                    else
                    {
                        int totalCount = pageCount;
                        pageCount =  totalCount/ PageSize;
                        if (totalCount % PageSize != 0) pageCount++;
                        this.dataGrid.ItemsSource = this.dataGridSouceList;
                        this.pageControl.PageCount = pageCount;
                    }
                }));
        }

        //退款
        private void btnRefund_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var orderNo = Convert.ToString(((Button)sender).CommandParameter);
                var page=new RefundOrderCreate(orderNo, pageControl.PageIndex, currentSwitchBtnIndex);
                page.ClosePageEvent+=page_ClosePageEvent;
                PageHelper.PageNavigateHelper(this,page);
            }
            catch (Exception) { }

        }

        /// <summary>
        /// 回调跳转
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="currentSwitchBtnIndex"></param>
        private void page_ClosePageEvent(Page lastPage,int pageIndex, int currentSwitchBtnIndex)
        {
            this.currentSwitchBtnIndex = currentSwitchBtnIndex;
            pageControl.PageIndex = pageIndex;
            PageHelper.PageNavigateHelper(lastPage, this);
        }

        //开收据
        private void btnOpenReceipt_Click(object sender, RoutedEventArgs e)
        {
            string orderNo = Convert.ToString(((Button)sender).CommandParameter);
            if (currentSwitchBtnIndex == 0)
            {
                OrderPaginatedInfo info = (OrderPaginatedInfo) getObjectFromListByNo(dataGridSouceList, orderNo);
                new ChooseReceiptFLowDialog(orderNo,info.CaseId,this.chooseFlowCompleted).ShowDialog();
            }
        }

        private void chooseFlowCompleted(string orderNo, string[] flows)
        {
            ReceiptView rv = null;
            rv = ReceiptProxyService.GetInstanse().PreviewReceipt(orderNo, flows);
            if (rv == null)
            {
                MessageBox.Show("收据预览失败" , "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            WriteOutCollectionReceipt wocp = new WriteOutCollectionReceipt(rv);
            wocp.CreateReceiptEvent += this.createReceiptCompleted;
            wocp.ShowDialog();
          
        }

        private void createReceiptCompleted(string orderNo,string[] trades ,ReceiptView receiptDto, WriteOutCollectionReceipt writeOutCollectionReceipt)
        {
            writeOutCollectionReceipt.Close();
            new CollectionReceipt(orderNo, trades, receiptDto).ShowDialog();
        }

        //回收收据
        private void BtnGetBackReceipt_OnClick(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show( "确定回收收据吗？","回收收据",MessageBoxButton.OKCancel,MessageBoxImage.Question)==MessageBoxResult.OK)
            {
                string errorInfo = null;
                try
                {
                    string orderNo = Convert.ToString(((Button)sender).CommandParameter);
                    OperationResult or =   ReceiptProxyService.GetInstanse().RecycleReceipt(long.Parse(orderNo), LoginHelper.CurrentUser.SysNo);
                    if (or.Success != true) errorInfo = or.ResultMessage;
                }
                catch (Exception ex)
                {
                    errorInfo = ex.Message;
                }
                if (errorInfo != null)
                {
                    MessageBox.Show( "回收失败：" + errorInfo,"系统提示",MessageBoxButton.OK,MessageBoxImage.Error);
                }
                else
                {
                    MessageBox.Show("操作成功","系统提示",MessageBoxButton.OK,MessageBoxImage.Asterisk);
                    startLoadDataAsyn();
                }
            }
        }
        //补开收据
        private void btnReOpenReceipt_Click(object sender, RoutedEventArgs e)
        {
            string rNo = Convert.ToString(((Button)sender).CommandParameter);
            Receipt r = (Receipt)getObjectFromListByNo(dataGridSouceList, rNo);
            //ReceiptView rv = ReceiptProxyService.GetInstanse().PreviewReceipt(r.OrderNo, r.TradeDetails.ToArray());
            ReceiptView rv = ReceiptProxyService.GetInstanse().GetReceiptInfo(r.SysNo);
            if (rv == null)
            {
                MessageBox.Show("不存在收据", "系统异常");
                return;
            }
            new CollectionReceipt(r.OrderNo, r.TradeDetails.ToArray(), rv, true).ShowDialog();
        }
        //作废收据
        private void BtnVoidReceipt_OnClick(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("确定作废收据吗？", "作废收据", MessageBoxButton.OKCancel, MessageBoxImage.Question) == MessageBoxResult.OK)
            {
                string errorInfo = null;
                try
                {
                    string orderNo = Convert.ToString(((Button)sender).CommandParameter);
                    OperationResult or = ReceiptProxyService.GetInstanse().AbolishReceipt(long.Parse(orderNo), LoginHelper.CurrentUser.SysNo);
                    if (or.Success != true) errorInfo = or.ResultMessage;
                }
                catch (Exception ex)
                {
                    errorInfo = ex.Message;
                }
                if (errorInfo != null)
                {
                      MessageBox.Show( "回收失败：" + errorInfo,"系统提示",MessageBoxButton.OK,MessageBoxImage.Error);
                }
                else
                {
                    MessageBox.Show("操作成功","系统提示",MessageBoxButton.OK,MessageBoxImage.Asterisk);
                    startLoadDataAsyn();
                }
            }
        }
        //录入回执
        private void btnInputPOSRetureReceipt_Click(object sender, RoutedEventArgs e)
        {
            var orderNo = Convert.ToString(((Button)sender).CommandParameter);
            CreatePosReceiptDialog posReceiptDialog = new CreatePosReceiptDialog(orderNo);
            posReceiptDialog.ShowDialog();
        }

        //审批
        private void btnApprove_Click(object sender, RoutedEventArgs e)
        {
            if (currentSwitchBtnIndex == 0)//订单出款审批
            {
                string orderNo = Convert.ToString(((Button)sender).CommandParameter);
                var order = (OrderPaginatedInfo)getObjectFromListByNo(dataGridSouceList, orderNo);
                if (order != null)
                    if (order.OrderType == OrderType.PaidOff)
                    {
                        PageHelper.PageNavigateHelper(this, new WriteOffOrderAudit(orderNo, order.AuditStatus, order.OrderStatus));
                    }
                    else
                    {
                        PageHelper.PageNavigateHelper(this, new PaymentOrderAudit(orderNo, order.AuditStatus, order.OrderStatus));    
                    }
                    
            }
            else if (currentSwitchBtnIndex == 4)//退款审批
            {
                string orderNo = Convert.ToString(((Button)sender).CommandParameter);
                RefundOrderPaginatedInfo order = (RefundOrderPaginatedInfo)getObjectFromListByNo(dataGridSouceList, orderNo);
                if (order != null) PageHelper.PageNavigateHelper(this, new RefundOrderAudit(orderNo, order.AuditStatus,order.OrderStatus));
            }
          
        }
        //添加附件
        private void BtnAddAttatch_OnClick(object sender, RoutedEventArgs e)
        {
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            var attachments = new UploadAttachment(caseId);
            PageHelper.PageNavigateHelper(this, attachments);
        }
        private void HyperlinkClicked(object sender, RoutedEventArgs e)
        {
            Hyperlink link = e.Source as Hyperlink;
            if(link==null)return;
            string no = Convert.ToString(link.NavigateUri);
            switch (currentSwitchBtnIndex)
            {
                case 0:
                    {
                        OrderPaginatedInfo order = (OrderPaginatedInfo)getObjectFromListByNo(dataGridSouceList, no);
                        if (order.OrderType == OrderType.Payment)
                        {
                            PageHelper.PageNavigateHelper(this, new PaymentOrderDetails(no, order.OrderStatus));
                        }
                        if(order.OrderType==OrderType.Collection )
                        {
                            if(order.PaymentChannel==PaymentChannel.Transfer) 
                                PageHelper.PageNavigateHelper(this, new ReceivablesOrderDetails(no,order.CaseId));
                            if (order.PaymentChannel == PaymentChannel.POS)
                                PageHelper.PageNavigateHelper(this, new PosDetailInfo(no, order.CaseId));
                        }
                        if (order.OrderType == OrderType.Expend)
                        {
                            if (order.PaymentChannel == PaymentChannel.Transfer)
                            {
                                PageHelper.PageNavigateHelper(this, new CostReceivablesOrderDetails(no, order.CaseId));
                            }
                            if (order.PaymentChannel == PaymentChannel.POS)
                            {
                                PageHelper.PageNavigateHelper(this, new CostPosDetailInfo(no, order.CaseId));
                            }
                        }
                        if (order.OrderType == OrderType.PaidOff)
                        {
                            PageHelper.PageNavigateHelper(this, new WriteOffOrderDetails(no, order.OrderStatus));
                        }
                    }
                    break;
                case 1:
                    {

                    }
                    break;
                case 2:
                    {
                        PageHelper.PageNavigateHelper(this, new FundSettlementDetails(no));
                    }
                    break;
                case 3:
                    {
                        //Receipt order = (Receipt)getObjectFromListByNo(dataGridSouceList, no);
                        //PageHelper.PageNavigateHelper(this, new BillDetailPage(no,order.CaseId));
                    }
                    break;
                case 4:
                    {
                        RefundOrderPaginatedInfo ropi =(RefundOrderPaginatedInfo) getObjectFromListByNo(dataGridSouceList, no);
                        PageHelper.PageNavigateHelper(this, new RefundOrderDetails(no,ropi.OrderStatus));
                    }
                    break;
                default:
                    break;
            }
            e.Handled = true;
        }

        private object getObjectFromListByNo(IList list, string no)
        {
            if (no == null) return null;
            foreach(object o in dataGridSouceList){
                System.Reflection.PropertyInfo propertyInfo = o.GetType().GetProperty("OrderNo"); //获取指定名称的属性
                string value = null;
                  if(propertyInfo!=null) value=  (string)propertyInfo.GetValue(o, null); //获取属性值
                if (value == no) return o;
                propertyInfo = o.GetType().GetProperty("ReceiptNo");
                if (propertyInfo != null) value = (string)propertyInfo.GetValue(o, null);
                if (value == no) return o;
            }
            return null;
        }
    }


}
