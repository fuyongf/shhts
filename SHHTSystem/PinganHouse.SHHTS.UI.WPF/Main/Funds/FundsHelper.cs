﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds
{
    public class FundsHelper
    {

        private static FundsHelper _instance = null;
        // Creates an syn object.
        private static readonly object SynObject = new object();
        FundsHelper() { }
        public static FundsHelper Instance
        {
            get
            {
                // Double-Checked Locking
                if (null == _instance)
                {
                    lock (SynObject)
                    {
                        if (null == _instance)
                        {
                            _instance = new FundsHelper();
                        }
                    }
                }
                return _instance;
            }
        }

        /// <summary>
        /// 获取订单数据
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        public Task<OrderDetail> GetOederData(string orderNo)
        {
            OrderDetail order = null;
            return Task.Run(() =>
            {
                order = TradeServiceProxy.GetOrderDetail(orderNo);
                return order;
            });
        }

        /// <summary>
        /// 获取案件相关数据
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public Task<Tuple<CaseDto, CaseRemark>> GetCaseData(string caseId)
        {
            CaseDto caseDto = null;
            CaseRemark caseRemarkDto = null;
            return Task.Run(() =>
             {
                 //获取案件信息
                 caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(caseId, ConfigHelper.GetCurrentReceptionCenter());
                 //案件附属对象
                 if (caseDto != null)
                     caseRemarkDto = CaseProxyService.GetInstanse().GetRemarkBySysNo(caseDto.SysNo);
                 var dicResult = new Tuple<CaseDto, CaseRemark>(caseDto, caseRemarkDto);
                 return dicResult;
             });
        }

        /// <summary>
        /// 获取案件
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public Task<CaseDto> GetCase(string caseId)
        {
            CaseDto caseDto = null;
            return Task.Run(() =>
            {
                //获取案件信息
                caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(caseId);
                return caseDto;
            });
        }

        /// <summary>
        /// 获取客户资金流水
        /// </summary>
        public Task<IList<CustomerFundFlow>> GetCustomerFundFlowList(string caseId)
        {
            return Task.Run(() =>
             {
                 var customerFundFlowList = CustomerFundFlowServiceProxy.GetFlows(caseId);
                 return customerFundFlowList;
             });

        }

        /// <summary>
        /// 获取交易信息
        /// </summary>
        /// <param name="orderNo">订单信息</param>
        /// <returns></returns>
        public Task<IList<TradeDetail>> GetTradeByOrder(string orderNo)
        {
            return Task.Run(() =>
            {
                var tradeDetailList = TradeServiceProxy.GetTradeByOrder(orderNo);
                return tradeDetailList;
            });
        }

        /// <summary>
        /// 获取账户信息
        /// </summary>
        /// <param name="accountSysNo"></param>
        /// <returns></returns>
        public Task<Account> GetAccount(long accountSysNo)
        {
            return Task.Run(() =>
            {
                var account = AccountServiceProxy.Get(accountSysNo);
                return account;
            });
        }

        /// <summary>
        /// 获取指定账户信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="accountType"></param>
        /// <returns></returns>
        internal Task<Account> GetAccount(string caseId, Enumerations.AccountType accountType)
        {
            return Task.Run(() =>
            {
                var account = AccountServiceProxy.Get(accountType, caseId);
                return account;
            });
        }

        /// <summary>
        /// 获取买卖方银行卡信息
        /// </summary>
        /// <param name="caseSysNo">案件系统编号</param>
        /// <returns></returns>
        internal Task<IEnumerable<CustomerBankAccount>> GetCustomerBankAccounts(long caseSysNo)
        {
            return Task.Run(() =>
            {
                var bankAccounts = CaseProxyService.GetInstanse().GetCustomerBankAccounts(caseSysNo);
                return bankAccounts;
            });

        }

        /// <summary>
        /// 获取实际收付
        /// </summary>
        /// <param name="caseSysNo"></param>
        /// <returns></returns>
        internal Task<Tuple<IDictionary<OrderType, decimal>, Dictionary<OrderType, decimal>>> GetCaseTradeAmount(string caseNo)
        {
            return Task.Run(() =>
            {
                var tradeAmounts = AccountServiceProxy.GetCaseTradeAmount(caseNo);
                return tradeAmounts;
            });

        }

        /// <summary>
        /// 附件查看
        /// </summary>
        /// <param name="associationNo"></param>
        /// <returns></returns>
        internal Task<List<string>> GetAttachment(string associationNo, AttachmentType type)
        {
            return Task.Run(() =>
            {
                var fileList = new List<string>();
                var attachments = AttachmentProxyService.GetInstanse().GetAttachments(associationNo, 1, 100, type);
                if (attachments == null || attachments.TotalCount == 0) return fileList;
                foreach (var attachment in attachments.Attachments)
                {
                    var file = AttachmentProxyService.GetInstanse().DownloadAttachment(attachment.FileId);
                    if (file == null) continue;
                    fileList.Add(file.FileId);
                    file.FileData.Close();

                }
                return fileList;
            });

        }
    }
}
