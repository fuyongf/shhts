﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds
{
    /// <summary>
    /// BillDetailPage.xaml 的交互逻辑
    /// </summary>
    public partial class BillDetailPage : Page
    {
        private string orderNo = null;
        private string caseId = null;
        private IList<ReceiptView> receiptDtos = null;
        public BillDetailPage(string orderNo,string caseId)
        {
            InitializeComponent();
            this.orderNo = orderNo;
            this.caseId = caseId;
        }

        private void btnReOpenReceipt_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BillDetailPage_OnLoaded(object sender, RoutedEventArgs e)
        {
            orderNoLabel.Content = orderNo;
            caseNoLabel.Content = caseId;
            Thread thread = new Thread(delegate()
            {
                receiptDtos = ReceiptProxyService.GetInstanse().GetReceiptsByOrderNo(orderNo);

                this.Dispatcher.Invoke(new Action(
                    delegate()
                    {
                        loading.IsBusy = false;
                        this.dataGrid.DataContext = receiptDtos;
                    }
                    ));
            });
            thread.IsBackground = true;

            PageHelper.PageActiveLoadingControl(this.loading, "正在加载数据,请稍后...");
            thread.Start();
        }
    }
}
