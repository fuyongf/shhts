﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using System.ComponentModel;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds
{
    /// <summary>
    /// ChooseReceiptFLowDialog.xaml 的交互逻辑
    /// </summary>
    public partial class ChooseReceiptFLowDialog : Window
    {

        public delegate void UnSeletedDataGrid();

        private UnSeletedDataGrid unSeletedDataGrid;
        private List<string> checkedBillNo = new List<string>();
        private string orderNo = null;
        private string caseId = null;
        private IList<BillDetailWithChecked> trades = new List<BillDetailWithChecked>();

        public delegate void ChooseCompleted(string orderNo, string[] choosedFowNumbers);

        public ChooseCompleted chooseCompleted;

        public ChooseReceiptFLowDialog(string orderNo, string caseId, ChooseCompleted cc)
        {
            InitializeComponent();
            this.orderNo = orderNo;
            this.caseId = caseId;
            this.chooseCompleted = cc;
            unSeletedDataGrid = this.unSelectedDataGrid;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            IList<string> flows = new List<string>();
            foreach (var v in trades)
            {
                if (v.Checked) flows.Add(v.BillDetail.TradeNo);
            }
            if (flows.Count == 0)
            {
                MessageBox.Show("请勾选打印的流水", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            this.Close();
            //WriteOutCollectionReceipt wocp= new WriteOutCollectionReceipt(orderNo, flows.ToArray());
            if (this.chooseCompleted != null) chooseCompleted(orderNo, flows.ToArray());
        }

        private void ChooseReceiptFLowDialog_OnLoaded(object sender, RoutedEventArgs e)
        {
            LabelLine1.Content = string.Format("订单编号：{0}     案件编号：{1}    （可多选合并打印）", orderNo, caseId);
            Thread thread = new Thread(delegate()
            {
                IList<ReceiptTradeInfo> bds =   ReceiptProxyService.GetInstanse().GetPrintReceiptTradesByOrder(orderNo);
                foreach (var bd in bds)
                {
                    trades.Add(new BillDetailWithChecked(bd));
                }
                this.Dispatcher.Invoke(new Action(
                    delegate()
                    {
                        loading.IsBusy = false;
                        this.dataGrid.DataContext = trades;
                    }
                    ));
            });
            thread.IsBackground = true;
            PageHelper.PageActiveLoadingControl(this.loading, "正在加载数据,请稍后...");
            thread.Start();
        }

        private void DataGrid_OnSelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            DataGridCellInfo info = new DataGridCellInfo();
            if (e.AddedCells.Count > 0) info = (DataGridCellInfo) e.AddedCells[0];
           // else if (e.RemovedCells.Count > 0) info = (DataGridCellInfo) e.RemovedCells[0];
            if (info.Item != null)
            {
                BillDetailWithChecked bill = (BillDetailWithChecked) info.Item;
                bill.Checked = !bill.Checked;
            }
            this.Dispatcher.BeginInvoke(this.unSeletedDataGrid);
        }

        private void unSelectedDataGrid()
        {
            this.dataGrid.SelectedItems.Clear();
        }

    }

    internal class BillDetailWithChecked : INotifyPropertyChanged
    {
        public ReceiptTradeInfo BillDetail { get; set; }

        public BillDetailWithChecked(ReceiptTradeInfo bd)
        {
            this.BillDetail = bd;
        }

        private bool isChecked = false;
        public bool Checked
        {
            get { return isChecked; }
            set
            {
                isChecked = value;
                NotifyPropertyChanged("Checked");
            }
        }
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

    }
    


}
