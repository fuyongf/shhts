﻿using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds
{
    /// <summary>
    /// DeductionOrderDetails.xaml 的交互逻辑
    /// </summary>
    public partial class DeductionOrderDetails : Page
    {
        /// <summary>
        /// 订单编号
        /// </summary>
        private string m_OrderNo;

        /// <summary>
        /// 订单当前状态
        /// </summary>
        private OrderStatus m_OrderStatus;

        public DeductionOrderDetails(string orderNo, OrderStatus orderStatus)
        {
            InitializeComponent();
            m_OrderNo = orderNo;
            m_OrderStatus = orderStatus;
        }

        /// <summary>
        /// 关闭票据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, new FundSettlementListPage());
        }

        /// <summary>
        /// 页面加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                PageHelper.PageActiveLoadingControl(ctrlLoading, "加载数据,请稍后...");
                LoadPageData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("系统加载数据发生异常:"+ex.Message,"系统异常",MessageBoxButton.OK,MessageBoxImage.Error);
            }
        }

        private async void LoadPageData()
        {
            try
            {
                //获取订单信息
                var orderInfo = await FundsHelper.Instance.GetOederData(m_OrderNo);
                if (orderInfo == null) return;
                //加载订单数据
                ctrlOrderInfo.InitCtrlData(orderInfo);

                var fileList = await FundsHelper.Instance.GetAttachment(orderInfo.OrderNo, AttachmentType.出款票据);
                //附件信息
                ctrlAttachmentInfo.InitCtrlData(fileList, orderInfo.Remark);
                //账户信息
                var account = AccountServiceProxy.Get(orderInfo.AccountSysNo);
                if (account == null) return;
                //获取案件信息/附属对象
                var caseReuslt = await FundsHelper.Instance.GetCaseData(account.SubjectId);
                var caseDto = caseReuslt.Item1;
                var caseRemarkDto = caseReuslt.Item2;
                //初始化案件信息
                ctrlCaseInfo.InitCtrlData(caseDto, caseRemarkDto);
                //获取审核状态
                var auditLogList = TradeServiceProxy.GetOrderAuditLog(m_OrderNo);
                ctrlAuditInfo.InitCtrlData(auditLogList, m_OrderStatus);
                ctrlLoading.IsBusy = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ctrlLoading.IsBusy = false;
            }
        }
    }
}
