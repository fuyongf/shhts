﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds
{
    /// <summary>
    /// RefundDeductionOrder.xaml 的交互逻辑
    /// </summary>
    public partial class RefundOrderAudit : Page
    {
        /// <summary>
        /// 当前订单号
        /// </summary>
        private string m_OrderNo;
        /// <summary>
        /// 当前订单的审核状态
        /// </summary>
        private AuditStatus m_AuditStatus;
        /// <summary>
        /// 当前订单的交易状态
        /// </summary>
        private OrderStatus m_OrderStatus;
        /// <summary>
        /// 退款审核
        /// </summary>
        /// <param name="orderNo"></param>
        public RefundOrderAudit(string orderNo, AuditStatus auditStatus, OrderStatus orderStatus)
        {
            InitializeComponent();
            m_OrderNo = orderNo;
            m_AuditStatus = auditStatus;
            m_OrderStatus = orderStatus;

        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                PageHelper.PageActiveLoadingControl(this.ctrlLoading, "加载数据,请稍候...");
                LoadPageData(m_OrderNo);
            }
            catch (Exception ex) {
                MessageBox.Show("加载数据发生系统异常:"+ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
                ctrlLoading.IsBusy = false;
            }
        }

        /// <summary>
        /// 加载页面数据
        /// </summary>
        /// <param name="orderNo"></param>
        private async void LoadPageData(string orderNo)
        {
            //获取订单信息
            var orderInfo =await FundsHelper.Instance.GetOederData(orderNo);
            ctrlRefundInfo.InitCtrlOrderData(orderInfo);

            //加载交易信息
            var tradeDetailList = await FundsHelper.Instance.GetTradeByOrder(orderNo);
            ctrlRefundInfo.InitCtrlTradeDetailData(tradeDetailList);

            //加载交易信息
            var account = await FundsHelper.Instance.GetAccount(orderInfo.AccountSysNo);
            ctrlRefundInfo.InitCtrlAccountData(account);

            //获取案件信息
            var caseDto = await FundsHelper.Instance.GetCase(account.SubjectId);
            ctrlRefundInfo.InitCtrlCaseData(caseDto);

            //获取审核状态
            var auditLogList = TradeServiceProxy.GetOrderAuditLog(orderNo);
            ctrlRefundInfo.InitCtrlAuditData(auditLogList,m_OrderStatus);
            ctrlLoading.IsBusy = false;
        }

        /// <summary>
        /// 关闭画面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, new FundSettlementListPage());
        }

        /// <summary>
        /// 审核退款
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnOk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //是否审核不通过
                var unAudit = Convert.ToBoolean(auditUnOK.IsChecked);
                if (unAudit && string.IsNullOrEmpty(txtRemark.Text.Trim()))
                {
                    MessageBox.Show("审核不通过，请填写备注.", "审核失败", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtRemark.Focus();
                    return;
                }
                PageHelper.PageActiveLoadingControl(this.ctrlLoading, "正在处理，请稍候...");
                var result = new OperationResult(1, string.Empty);
                var newStatus = AuditStatus.Initial;
                switch (m_AuditStatus)
                {
                    case Enumerations.AuditStatus.Initial://初始
                        newStatus = unAudit ? AuditStatus.AuditFailed : AuditStatus.Audited;
                        break;
                    case Enumerations.AuditStatus.Audited: //已审核
                        newStatus = unAudit ? AuditStatus.ReAuditFailed : AuditStatus.ReAudited;
                        break;
                    case Enumerations.AuditStatus.FinanceAudited: //总部财务已经审核
                        MessageBox.Show("订单已经全部审核，无法再次审核", "审核提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        this.ctrlLoading.IsBusy = false;
                        return;
                    case Enumerations.AuditStatus.AuditFailed: //已驳回
                    case Enumerations.AuditStatus.ReAuditFailed: //已驳回
                    case Enumerations.AuditStatus.FinanceAuditFailed: //已驳回
                        MessageBox.Show("订单已经驳回，无法再次审核", "审核提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        this.ctrlLoading.IsBusy = false;
                        return;
                    default:
                        MessageBox.Show("未知退款订单状态，无法审核", "审核提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        this.ctrlLoading.IsBusy = false;
                        return;
                }
                result = await RefundAuditOrder(m_OrderNo, newStatus,txtRemark.Text.Trim());
                if (!result.Success)
                {
                    MessageBox.Show("审核失败：" + result.ResultMessage, "审核失败", MessageBoxButton.OK, MessageBoxImage.Error);
                    this.ctrlLoading.IsBusy = false;
                    return;
                }
                PageHelper.PageNavigateHelper(this, new FundSettlementListPage());
            }
            catch (Exception ex)
            {
                MessageBox.Show("操作发生系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
                this.ctrlLoading.IsBusy = false;
            }
        }

        /// <summary>
        /// 审核订单
        /// </summary>
        /// <param name="orderNo">订单编号</param>
        /// <param name="auditStatus">订单当前状态</param>
        /// <returns></returns>
        private Task<OperationResult> RefundAuditOrder(string orderNo, AuditStatus auditStatus, string remark)
        {
            return Task.Run(() =>
            {
                return TradeServiceProxy.RefundOrderAudit(ConfigHelper.GetCurrentReceptionCenter(), orderNo, auditStatus,
                         LoginHelper.CurrentUser.SysNo, LoginHelper.CurrentUser.RealName,remark);
            });
        }
    }
}
