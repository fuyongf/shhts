﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds
{
    /// <summary>
    /// PaymentOrderDetails.xaml 的交互逻辑
    /// </summary>
    public partial class PaymentOrderDetails : Page
    {
        /// <summary>
        /// 订单编号
        /// </summary>
        private string m_OrderNo;

        /// <summary>
        /// 订单当前状态
        /// </summary>
        private OrderStatus m_OrderStatus;

        /// <summary>
        /// 付款明细页面
        /// </summary>
        /// <param name="orderNo">订单号</param>
        /// <param name="orderStatus">当前订单状态</param>
        public PaymentOrderDetails(string orderNo, OrderStatus orderStatus)
        {
            InitializeComponent();
            m_OrderNo = orderNo;
            m_OrderStatus = orderStatus;
        }

        /// <summary>
        /// 关闭窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, new FundSettlementListPage());
        }

        /// <summary>
        /// 打印收据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// 画面加载初始化数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                 PageHelper.PageActiveLoadingControl(loadingCtrl, "加载数据,请稍后...");
                 LoadPageData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("加载数据发生系统异常:"+ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 获取页面数据
        /// </summary>
        private async void LoadPageData()
        {
            try {
                //获取订单信息
                var orderInfo = await FundsHelper.Instance.GetOederData(m_OrderNo);
                if (orderInfo == null) return;
                //加载订单数据
                ctrlOrderInfo.InitCtrlData(orderInfo,true);

                //加载附件信息
                var fileList = await FundsHelper.Instance.GetAttachment(orderInfo.OrderNo, AttachmentType.出款票据);
                ctrlAttachmentInfo.InitCtrlData(fileList, orderInfo.Remark);

                //订单流水信息
                //var tradeLists =await FundsHelper.Instance.GetTradeByOrder(orderInfo.OrderNo);
                //TradeDetail tradeDetail=null;
                //if (tradeLists != null){
                //    tradeDetail = tradeLists[0];
                //}
                ctrlPaymentInfo.InitCtrlData(orderInfo);

                //账户信息
                var account = AccountServiceProxy.Get(orderInfo.AccountSysNo);
                if (account == null) return;

                //获取案件信息/附属对象
                var caseReuslt = await FundsHelper.Instance.GetCaseData(account.SubjectId);
                var caseDto = caseReuslt.Item1;
                var caseRemarkDto = caseReuslt.Item2;

                //初始化案件信息
                ctrlCaseInfo.InitCtrlData(caseDto, caseRemarkDto);

                //获取资金流水
                var customerFundFlowList = await FundsHelper.Instance.GetCustomerFundFlowList(caseDto.CaseId);
                if (customerFundFlowList == null) return;
                ctrlTransactionInfo.InitCtrlData(customerFundFlowList);

                //获取账户余额
                var buyerAccount = await FundsHelper.Instance.GetAccount(caseDto.CaseId, AccountType.Buyer);
                var sellerAccount = await FundsHelper.Instance.GetAccount(caseDto.CaseId, AccountType.Seller);
                ctrlTransactionInfo.InitCtrlAccountData(buyerAccount, sellerAccount);

                //设置实收实付
                var tradeAmountData = await FundsHelper.Instance.GetCaseTradeAmount(caseDto.CaseId);
                ctrlTransactionInfo.InitTradeAmountData(tradeAmountData);

                //获取审核状态
                var auditLogList = TradeServiceProxy.GetOrderAuditLog(m_OrderNo);
                ctrlAuditInfo.InitCtrlData(auditLogList,m_OrderStatus);

                //银行卡信息
                var bankAccounts = await FundsHelper.Instance.GetCustomerBankAccounts(caseDto.SysNo);
                ctrlPaymentInfo.DeterminePaymentType(bankAccounts, orderInfo.OrderBizType, orderInfo.BankAccountNo);
                loadingCtrl.IsBusy = false;            
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                loadingCtrl.IsBusy = false;  
            }

        }
    }
}
