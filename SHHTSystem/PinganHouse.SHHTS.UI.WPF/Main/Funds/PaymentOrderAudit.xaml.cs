﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds
{
    /// <summary>
    /// FundingOrderApproval.xaml 的交互逻辑
    /// </summary>
    public partial class PaymentOrderAudit : Page
    {
        /// <summary>
        /// 当前订单号
        /// </summary>
        private string m_OrderNo;
        /// <summary>
        /// 当前订单的审核状态
        /// </summary>
        private AuditStatus m_AuditStatus;
        /// <summary>
        /// 当前订单的交易状态
        /// </summary>
        private OrderStatus m_OrderStatus;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="orderNo"></param>
        public PaymentOrderAudit(string orderNo, AuditStatus auditStatus,OrderStatus orderStatus)
        {
            InitializeComponent();
            m_OrderNo = orderNo;
            m_AuditStatus = auditStatus;
            m_OrderStatus = orderStatus;
        }


        /// <summary>
        /// 关闭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, new FundSettlementListPage());
        }

        /// <summary>
        /// 确定审核
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnOk_Click(object sender, RoutedEventArgs e)
        {
            try {
                var unAudit = Convert.ToBoolean(auditUnOK.IsChecked);
                if (unAudit && string.IsNullOrEmpty(txtRemark.Text.Trim()))
                {
                    MessageBox.Show("审核不通过，请填写备注.", "审核失败", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtRemark.Focus();
                    return;                  
                }
                PageHelper.PageActiveLoadingControl(this.loadingCtrl, "正在处理，请稍候...");
                var newStatus = AuditStatus.Initial;
                switch (m_AuditStatus)
                {
                    case Enumerations.AuditStatus.Initial://初始
                        newStatus = unAudit ? AuditStatus.AuditFailed : AuditStatus.Audited;
                        break;
                    case Enumerations.AuditStatus.Audited: //已审核
                        newStatus = unAudit ? AuditStatus.ReAuditFailed : AuditStatus.ReAudited;
                        break;
                    case Enumerations.AuditStatus.FinanceAudited: //总部财务已经审核
                        MessageBox.Show("订单已经全部审核，无法再次审核", "审核提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        this.loadingCtrl.IsBusy = false;
                        return;
                    case Enumerations.AuditStatus.AuditFailed: //已驳回
                    case Enumerations.AuditStatus.ReAuditFailed: //已驳回
                    case Enumerations.AuditStatus.FinanceAuditFailed: //已驳回
                        MessageBox.Show("订单已经驳回，无法再次审核", "审核提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        this.loadingCtrl.IsBusy = false;
                        return;
                    default:
                        MessageBox.Show("未知出款订单状态，无法审核", "审核提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        this.loadingCtrl.IsBusy = false;
                        return;
                }
                var result = await AuditOrder(m_OrderNo, newStatus,txtRemark.Text.Trim());
                if (!result.Success)
                {
                    MessageBox.Show("审核失败："+result.ResultMessage,"审核失败",MessageBoxButton.OK,MessageBoxImage.Error);
                    this.loadingCtrl.IsBusy = false;
                    return;
                }              
                PageHelper.PageNavigateHelper(this, new FundSettlementListPage());
            }
            catch (Exception ex)
            {
                MessageBox.Show("操作发生系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
                this.loadingCtrl.IsBusy = false;
            }
            finally
            {
                this.loadingCtrl.IsBusy = false;
            }
        }

        /// <summary>
        /// 审核订单
        /// </summary>
        /// <param name="orderNo">订单编号</param>
        /// <param name="status">订单当前状态</param>
        /// <returns></returns>
        private Task<OperationResult> AuditOrder(string orderNo, AuditStatus auditStatus, string remark)
        {
            return Task.Run(() =>
            {
                return TradeServiceProxy.PaymentOrderAudit(ConfigHelper.GetCurrentReceptionCenter(), orderNo, auditStatus,
                LoginHelper.CurrentUser.SysNo, LoginHelper.CurrentUser.RealName, remark);
            });
        }

        /// <summary>
        /// 画面加载数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                PageHelper.PageActiveLoadingControl(loadingCtrl, "加载数据,请稍后...");
                LoadPageData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("加载数据发生系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 获取页面数据
        /// </summary>
        private async void LoadPageData()
        {
            try {
                //获取订单信息
                var orderInfo = await FundsHelper.Instance.GetOederData(m_OrderNo);
                if (orderInfo == null) return;
                //加载订单数据
                ctrlOrderInfo.InitCtrlData(orderInfo,true);

                var fileList = await FundsHelper.Instance.GetAttachment(orderInfo.OrderNo, AttachmentType.出款票据);
                //获取附件信息
                ctrlAttachmentInfo.InitCtrlData(fileList, orderInfo.Remark);

                //订单流水信息
                //var tradeLists = await FundsHelper.Instance.GetTradeByOrder(orderInfo.OrderNo);
                //TradeDetail tradeDetail = null;
                //if (tradeLists != null && tradeLists.Count > 0)
                //{
                //    tradeDetail = tradeLists[0];
                //}
                ctrlPaymentInfo.InitCtrlData(orderInfo);

                //账户信息
                var account = AccountServiceProxy.Get(orderInfo.AccountSysNo);
                if (account == null) return;

                //获取案件信息/附属对象
                var caseReuslt = await FundsHelper.Instance.GetCaseData(account.SubjectId);
                var caseDto = caseReuslt.Item1;
                var caseRemarkDto = caseReuslt.Item2;
                ctrlCaseInfo.InitCtrlData(caseDto, caseRemarkDto);

                //获取资金流水
                var customerFundFlowList = await FundsHelper.Instance.GetCustomerFundFlowList(caseDto.CaseId);
                if (customerFundFlowList == null) return;
                ctrlTransactionInfo.InitCtrlData(customerFundFlowList);

                //获取账户
                var buyerAccount = await FundsHelper.Instance.GetAccount(caseDto.CaseId, AccountType.Buyer);
                var sellerAccount = await FundsHelper.Instance.GetAccount(caseDto.CaseId, AccountType.Seller);
                ctrlTransactionInfo.InitCtrlAccountData(buyerAccount, sellerAccount);

                //设置实收实付
                var tradeAmountData = await FundsHelper.Instance.GetCaseTradeAmount(caseDto.CaseId);
                ctrlTransactionInfo.InitTradeAmountData(tradeAmountData);

                //获取审核状态
                var auditLogList = TradeServiceProxy.GetOrderAuditLog(m_OrderNo);
                ctrlAuditInfo.InitCtrlData(auditLogList,m_OrderStatus);

                //银行卡信息
                var bankAccounts = await FundsHelper.Instance.GetCustomerBankAccounts(caseDto.SysNo);
                ctrlPaymentInfo.DeterminePaymentType(bankAccounts, orderInfo.OrderBizType, orderInfo.BankAccountNo);
                loadingCtrl.IsBusy = false;           
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                loadingCtrl.IsBusy = false;   
            }

        }
    }
}
