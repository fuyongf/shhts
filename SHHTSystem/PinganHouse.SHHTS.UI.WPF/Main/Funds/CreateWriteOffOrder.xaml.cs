﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Main.Dialog;
using PinganHouse.SHHTS.UI.WPF.Model;
using System.IO;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds
{
    /// <summary>
    /// CreateWriteOffOrder.xaml 的交互逻辑
    /// </summary>
    public partial class CreateWriteOffOrder : Page
    {
        /// <summary>
        /// 案件编号信息
        /// </summary>
        private string m_CaseId;//="06202872954906";

        /// <summary>
        /// 收款账户信息
        /// </summary>
        private IEnumerable<CustomerBankAccount> m_CustomerBankAccount;

        /// <summary>
        /// 附件集合
        /// </summary>
        private Dictionary<string, System.IO.Stream> m_FileList;

        /// <summary>
        /// 中介公司信息
        /// </summary>
        private AgentCompany m_AgentCompany;


        public CreateWriteOffOrder(string caseId)
        {
            InitializeComponent();
            m_CaseId = caseId;
        }

        /// <summary>
        /// 页面加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                LoadPageData();
            }
            catch (Exception ex)
            {
                this.ctrlLoading.IsBusy = false;
                MessageBox.Show("初始化数据发生异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private async void LoadPageData()
        {
            try
            {
                PageHelper.PageActiveLoadingControl(this.ctrlLoading, "正在加载数据,请稍候...");
                //获取案件信息
                var caseResult = await FundsHelper.Instance.GetCaseData(m_CaseId);
                //案件对象
                var caseDto = caseResult.Item1;
                //案件附属对象
                var caseRemarkDto = caseResult.Item2;
                //加载案件信息
                this.ctrlCaseInfo.InitCtrlData(caseDto, caseRemarkDto);
                //申请人信息
                this.txt_SQR.Text = LoginHelper.CurrentUser.RealName;
                //申请时间
                this.txt_Time.Text = System.DateTime.Now.ToShortDateString();
                //申请人部门
                txt_Dep.Text = string.Empty;
                //中介公司信息
                var agentCompany = AgentServiceProxy.GetAgentCompany(caseDto.CompanySysNo);
                m_AgentCompany = agentCompany;
                //银行卡信息
                var bankAccounts = CaseProxyService.GetInstanse().GetCustomerBankAccounts(caseDto.SysNo);
                m_CustomerBankAccount = bankAccounts;
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.ctrlLoading.IsBusy = false;
            }


        }

      

        /// <summary>
        /// 关闭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, new QueryCaseInfo(m_CaseId));
        }

        /// <summary>
        /// 提交销账订单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PageHelper.PageActiveLoadingControl(this.ctrlLoading, "正在处理,请稍候...");
                //确定销账主体
                var accountItem = cb_CKF.SelectedItem as ComboBoxItem;
                if (accountItem == null)
                {
                    MessageBox.Show("请选择销账主体", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ctrlLoading.IsBusy = false;
                    return;
                }
                var accountType = Convert.ToString(accountItem.Tag) == "Buyer" ? AccountType.Buyer : AccountType.Seller;
                
                //销账原因
                var tradeItem = cb_XZYY.SelectedItem as ComboBoxItem;
                if (tradeItem == null)
                {
                    MessageBox.Show("请选择销账原因", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ctrlLoading.IsBusy = false;
                    return;
                }
                var tradeType = (OrderBizType) int.Parse(tradeItem.Tag.ToString());



                //销账金额
                decimal amount;
                decimal.TryParse(txt_XZJE.Text.Trim(), out amount);
                if (amount == 0)
                {
                    MessageBox.Show("请填写正确的销账金额", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ctrlLoading.IsBusy = false;
                    return;
                }

                //收款人
                var shouKuanRen = txtShouKuanRen.Text;
                if (string.IsNullOrEmpty(shouKuanRen))
                {
                    MessageBox.Show("请填写收款人", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ctrlLoading.IsBusy = false;
                    return;   
                }

                //银行名称
                var bankName = txtBankName.Text;
                if (string.IsNullOrEmpty(bankName))
                {
                    MessageBox.Show("请填写银行名称", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ctrlLoading.IsBusy = false;
                    return;
                }


                //银行账号
                var bankAccount = txtBankAccount.Text;
                if (string.IsNullOrEmpty(bankAccount))
                {
                    MessageBox.Show("请填写银行账号", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ctrlLoading.IsBusy = false;
                    return;
                }

                
                //收款账户类型
                var payeeTypeItem = cb_SKZH.SelectedItem as ComboBoxItem;
                if (payeeTypeItem == null)
                {
                    MessageBox.Show("请选择收款账户", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ctrlLoading.IsBusy = false;
                    return;
                }
                var payeeType = (AccountType)int.Parse(payeeTypeItem.Tag.ToString());

                var payeeSubjectId = m_CaseId;
                if (payeeType == AccountType.Agency)
                {
                    payeeSubjectId = Convert.ToString(m_AgentCompany.SysNo);
                }

                //保存数据
                var result = await SavePaidOffData(accountType, m_CaseId, payeeType, payeeSubjectId, tradeType, amount, bankAccount, null, shouKuanRen, bankName, txtRemark.Text.Trim(), LoginHelper.CurrentUser.SysNo);

                //判断是否成功
                if (!result.Success)
                {
                    MessageBox.Show("创建销账订单失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ctrlLoading.IsBusy = false;
                }
                else
                {
                    //上传图片
                    if (result.OtherData != null)
                    {
                        if (result.OtherData.ContainsKey("OrderNo"))
                        {
                            AsynchronousUpload(Convert.ToString(result.OtherData["OrderNo"]));
                        }
                    }

                    PageHelper.PageNavigateHelper(this, new FundSettlementListPage());
                }
            }
            catch (Exception ex)
            {
                ctrlLoading.IsBusy = false;
                MessageBox.Show("系统操作发生异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 异步上传
        /// </summary>
        private void AsynchronousUpload(string subjectNo)
        {
            //上传附件
            if (m_FileList != null)
            {
                AttachmentProxyService.GetInstanse().OnCompleted += CreatePaymentOrder_OnCompleted;
                foreach (var file in m_FileList)
                {
                    var result = AttachmentProxyService.GetInstanse().AddAttachment(
                        new AttachmentContent()
                        {
                            AttachmentContentData = file.Value,
                            FileName = file.Key,
                            AssociationNo = subjectNo,
                            AttachmentType = AttachmentType.销账附件
                        }, true);
                }
            }
        }

        /// <summary>
        /// 上传完成删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void CreatePaymentOrder_OnCompleted(object sender, AttachmentProxyService.UploadEventArgs e)
        {
            try
            {
                e.AttachInfo.AttachmentContentData.Close();
                e.AttachInfo.AttachmentContentData.Dispose();
                if (File.Exists(e.AttachInfo.FileName))
                {
                    File.Delete(e.AttachInfo.FileName);
                }
            }
            catch (Exception ex) { }
        }


        /// <summary>
        /// 保存数据
        /// </summary>
        /// <returns></returns>
        private Task<OperationResult> SavePaidOffData(AccountType payerType, string payerSubjectId, AccountType payeeType, string payeeSubjectId, OrderBizType tradeType, decimal amount, string bankAccountNo, string bankCode, string accountName, string bankName, string remark, long operatorSysNo)
        {
            return Task.Run(() =>
            {
                //提交表单
                return TradeServiceProxy.SubmitPaidOff(payerType, payerSubjectId, payeeType, payeeSubjectId, tradeType, amount, bankAccountNo, bankCode, accountName, bankName, remark, operatorSysNo);
            });
        }


       

        
        /// <summary>
        /// 获取帐户余额
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cb_CKF_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = cb_CKF.SelectedItem as ComboBoxItem;
            var accountType = AccountType.Buyer;
            //获取余额
            if (item != null)
            {
                switch (Convert.ToString(item.Tag))
                {
                    case "Seller":
                        //查询收款账户信息
                        accountType = AccountType.Seller;
                        break;
                }
            }
            ThreadPool.QueueUserWorkItem((obj) =>
            {
                this.Dispatcher.Invoke(() =>
                {
                    var account = AccountServiceProxy.GetBalance(accountType, m_CaseId);
                    txt_ZHYE.Text = string.Format("{0:N2}", account);
                });
            });

        }

        #region 上传附件
        /// <summary>
        /// 本地上传
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UploadFile_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var uploadFileHelper = new UploadFileHelper();
                var files = uploadFileHelper.SelectUploadFile();
                if (files == null || files.Count == 0) return;
                m_FileList = files;
                fileNum.Tag = null;
                fileNum.Tag = files;
                fileNum.Content = files.Count + "个附件";
            }
            catch (Exception ex)
            {
                MessageBox.Show("选择上传文件发生系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        /// <summary>
        /// 上传附件操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUploadFile_Click(object sender, RoutedEventArgs e)
        {
            var btnUpload = sender as Button;
            if (btnUpload.Tag == null)
            {
                CameraUploadFile_Click(sender, e);
            }
        }

        /// <summary>
        /// 高拍仪器上传
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CameraUploadFile_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var captureImge = new CaptrueImage(AttachmentType.销账附件);
                captureImge.AllUploadedEvent += captureImge_AllUploadedEvent;
            }
            catch (Exception ex)
            {
                MessageBox.Show("选择上传文件发生系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 接收返回的参数
        /// </summary>
        /// <param name="fileStreamList"></param>
        /// <param name="fileNameList"></param>
        /// <param name="captrueImage"></param>
        void captureImge_AllUploadedEvent(Dictionary<string, System.IO.Stream> files, string filePath, CaptrueImage captrueImage, CameraInterfaceInvoke cameraInvoke, UInt32 mDeviceHandle)
        {
            cameraInvoke.func_CloseDevice(mDeviceHandle);
            captrueImage.Close();
            if (files == null || files.Count == 0) return;
            m_FileList = files;
            fileNum.Tag = null;
            fileNum.Tag = files;
            fileNum.Content = files.Count + "个附件";
        }
        #endregion

        
    }
}
