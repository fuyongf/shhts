﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PinganHouse.SHHTS.UI.WPF.Common;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds.Win
{
    /// <summary>
    /// ReadSerialNum.xaml 的交互逻辑
    /// </summary>
    public partial class ReadSerialNum : Window
    {
        private string _serialNum;
        private bool _isCancel;
        private readonly bool _deviceNotExists;
        /// <summary>
        /// 获取设备存在性
        /// </summary>
        public bool DeviceNotExists
        {
            get { return _deviceNotExists; }
        }
        public ReadSerialNum( string serialNum)
        {
            InitializeComponent();
            string errorMessage;

            if (!IdentityCardHelper.OpenDevice(out errorMessage))
            {
                _deviceNotExists = true;
                MessageBox.Show(errorMessage, "提示");
                return;
            }
            _serialNum = serialNum;
            _isCancel = false;
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            if (_deviceNotExists)
                return;
            var thread = new Thread(Read);
            thread.IsBackground = true;
            thread.Start();            
        }

        private void Read()
        {
            StringBuilder retSerialNum = new StringBuilder();
            string errorMessage = string.Empty;
           var ret= IdentityCardHelper.ReadStaffCard(out retSerialNum, out errorMessage);
            if (ret)
            {
                if (MessageBox.Show(errorMessage, "错误", MessageBoxButton.OK, MessageBoxImage.Error) == MessageBoxResult.OK)
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        Close();
                    });
                }
                return;
            }
            _serialNum = retSerialNum.ToString();
            Application.Current.Dispatcher.Invoke(() =>
            {
                Close();
            });

        }
        
        private void Window_Closed(object sender, EventArgs e)
        {
            if (_deviceNotExists)
                return;
            IdentityCardHelper.CloseDevice();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            if (_deviceNotExists)
                return;
            IdentityCardHelper.CloseDevice();
        }
    }
}
