﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Model;
using PinganHouse.SHHTS.UI.WPF.Common;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds.Win
{
    /// <summary>
    /// WriteOutCollectionReceipt.xaml 的交互逻辑
    /// </summary>
    public partial class WriteOutCollectionReceipt : Window
    {
        private string m_OrderNo;
        private string[] m_Trades;
        private ReceiptView m_ReceiptDto;
        public delegate void CreateReceiptHandle(string orderNo, string[] trads, ReceiptView receiptDto, WriteOutCollectionReceipt writeOutCollectionReceipt);
        public event CreateReceiptHandle CreateReceiptEvent;

        public WriteOutCollectionReceipt(ReceiptView receiptDto)
        //public WriteOutCollectionReceipt(string orderNo,string[] trades)
        {
            InitializeComponent();
            //m_OrderNo = orderNo;
            //m_Trades = trades;
            if (receiptDto == null)
            {
                return;
            }
            m_ReceiptDto = receiptDto;
            m_OrderNo = receiptDto.OrderNo;
            m_Trades = receiptDto.TradeDetails.ToArray();
        }

        /// <summary>
        /// 窗体加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //if (string.IsNullOrWhiteSpace(m_OrderNo) && (m_Trades == null || m_Trades.Length == 0))
                //{
                //    return;
                //}

                //m_ReceiptDto = ReceiptProxyService.GetInstanse().PreviewReceipt(m_OrderNo, m_Trades);
                //if (m_ReceiptDto == null)
                //{
                //    return;
                //}
                TblCaseId.Text = m_ReceiptDto.CaseId; //案件号
                TblReceiptDate.Text = m_ReceiptDto.ReceiptDate.ToString("yyyy/MM/dd"); //收据日期
                TblReceiptNo.Text = m_ReceiptDto.ReceiptNo; //收据编号
                TblTenementAddress.Text = m_ReceiptDto.TenementAddress; //物业地址
                TblPaymentAmountUpperCase.Text = ConvertHelper.ConvertMoney(m_ReceiptDto.Money); ; //付款金额（大写）
                TblPaymentAmountLowerCase.Text = m_ReceiptDto.Money.ToString(); //付款金额（小写）

                #region 付款性质
                TblPaymentNature.Text = EnumHelper.GetEnumDesc(m_ReceiptDto.OrderBizType);

                //switch (m_ReceiptDto.OrderBizType)
                //{
                //    case OrderBizType.Collection:
                //        TblPaymentNature.Text = "收款";
                //        break;
                //    case OrderBizType.Refund:
                //        TblPaymentNature.Text = "退款";
                //        break;
                //    case OrderBizType.HosingFund:
                //        TblPaymentNature.Text = "房款";
                //        break;
                //    case OrderBizType.Taxes:
                //        TblPaymentNature.Text = "税费";
                //        break;
                //    case OrderBizType.AgencyFee:
                //        TblPaymentNature.Text = "佣金";
                //        break;
                //    default:
                //        TblPaymentNature.Text = "";
                //        break;
                //}

                #endregion

                #region 付款方式
                TblPaymentMethod.Text = EnumHelper.GetEnumDesc(m_ReceiptDto.PaymentChannel);

                //switch (m_ReceiptDto.PaymentChannel)
                //{
                //    case PaymentChannel.POS:
                //        TblPaymentMethod.Text = "POS";
                //        break;
                //    case PaymentChannel.Transfer:
                //        TblPaymentMethod.Text = "转账";
                //        break;
                //    case PaymentChannel.Entrust:
                //        TblPaymentMethod.Text = "银行代扣";
                //        break;
                //    case PaymentChannel.Gift:
                //        TblPaymentMethod.Text = "礼券";
                //        break;
                //    default:
                //        TblPaymentMethod.Text = "";
                //        break;
                //}

                #endregion

                TblPaymentUser.Text = m_ReceiptDto.Payer; //付款人
                TblAdmissibilityCentral.Text = EnumHelper.GetEnumDesc(ConfigHelper.GetCurrentReceptionCenter()); //受理中心
                TblOpenPeople.Text = LoginHelper.CurrentUser.RealName; //开具人


            }
            catch (Exception ex)
            {
                MessageBox.Show("加载收款数据系统异常:"+ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }

        /// <summary>
        /// 生成收据表单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCreate_Click(object sender, RoutedEventArgs e)
        {
            m_ReceiptDto.Address = TbDetailedAddress.Text.Trim(); //详细地址
            m_ReceiptDto.MobilePhone = TbContactNumber.Text.Trim();//联系电话
            m_ReceiptDto.PostCode=TbPostCode.Text.Trim(); //邮编
            //m_ReceiptDto.CreateName = LoginHelper.CurrentUser.RealName; //开具人
            //m_ReceiptDto.Payee = EnumHelper.GetEnumDesc(LoginHelper._receptionCenter); //受理中心
            if (CreateReceiptEvent != null)
            {
                CreateReceiptEvent(m_OrderNo, m_Trades,m_ReceiptDto, this);
            }
        }

        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

    }
}
