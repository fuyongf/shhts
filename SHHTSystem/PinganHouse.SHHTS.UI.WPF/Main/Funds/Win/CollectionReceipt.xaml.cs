﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ZXing;
using Brush = System.Drawing.Brush;
using Color = System.Drawing.Color;
using PrintDialog = System.Windows.Controls.PrintDialog;
using System.Drawing.Printing;
using System.Windows.Documents;
using System.Windows.Threading;
using System.Collections.Generic;
using PinganHouse.SHHTS.UI.WPF.Main.Review;

namespace PinganHouse.SHHTS.UI.WPF.Main.Funds.Win
{
    /// <summary>
    /// CollectionReceipt.xaml 的交互逻辑
    /// </summary>
    public partial class CollectionReceipt : Window
    {


        private bool ReOpenReceipt = false;
        /// <summary>
        /// 打印机列表
        /// </summary>
        private List<PrintModel> _ListPrint = new List<PrintModel>();

        /// <summary>
        /// 打印机名字
        /// </summary>
        private string _ReceiptPrintName;

        /// <summary>
        /// 打印机名
        /// </summary>
        //private const string PrintName = "HP LaserJet 200 color M251 PCL 6";

        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="pdlg"></param>
        /// <param name="paginator"></param>
        private delegate void DoPrintMethod(PrintDialog pdlg, DocumentPaginator paginator);

        #region 条形码变量
        private static readonly int pointFX = -3;
        //案件编号
        private static readonly Font Count_Font = new Font("OCR-B-10 BT", 15, System.Drawing.FontStyle.Bold, GraphicsUnit.Pixel);
        //案件编号条形码
        private static readonly Font Code128_Font = new Font("Code 128", 40, System.Drawing.FontStyle.Regular, GraphicsUnit.Pixel);
        private readonly static PointF BarLocation = new PointF(0, 15 + pointFX); //条形码
        private readonly static PointF BarCodeLocation = new PointF(13, 55 + pointFX); //编码
        private readonly static Brush MyBrush = new SolidBrush(Color.Black);
        #endregion

        /// <summary>
        /// 订单号
        /// </summary>
        private string m_OrderNo;
        /// <summary>
        ///单据参数
        /// </summary>
        private ReceiptView m_ReceiptDto;
        /// <summary>
        /// 选择的流水号
        /// </summary>
        private string[] m_Trades;

        /// <summary>
        /// 验真码
        /// </summary>
        private string _SecurityCode;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="orderNo"></param>
        /// <param name="trades"></param>
        /// <param name="receiptDto"></param>
        public CollectionReceipt(string orderNo, string[] trades, ReceiptView receiptDto)
        {
            InitializeComponent();
            m_OrderNo = orderNo;
            m_ReceiptDto = receiptDto;
            m_Trades = trades;
            //receiptDto.CaseId = "06202872954991";

            #region 获得本机所有打印机
            var printers = PrinterSettings.InstalledPrinters;//获取本机上的所有打印机
            //PrintModel pm1 = new PrintModel();
            //pm1.PrintNo = -1;
            //pm1.PrintName = "请选择打印机";
            //_ListPrint.Add(pm1);
            PrintModel pm = new PrintModel();
            pm.PrintNo = 0;
            pm.PrintName = "未分配";
            _ListPrint.Add(pm);

            for (int i = 0; i < printers.Count; i++)
            {
                PrintModel print = new PrintModel();
                print.PrintNo = i + 1;
                print.PrintName = printers[i].ToString();
                _ListPrint.Add(print);
            }
            #endregion
            //从注册表加载 收据打印机信息          
            _ReceiptPrintName = PrivateProfileUtils.GetContentValue("ReceiptPrint", "PrintName"); //打印机名
        }

        public CollectionReceipt(string orderNo, string[] trades, ReceiptView rv, bool isReOpen)
            : this(orderNo, trades, rv)
        {
            this.ReOpenReceipt = isReOpen;
        }

        /// <summary>
        /// 窗体加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (m_ReceiptDto == null) { return; }

            this.DataContext = m_ReceiptDto;
            GetData();
        }

        /// <summary>
        /// 窗体加载时要获取的数据
        /// </summary>
        private void GetData()
        {
            //收据日期
            TblReceiptDate.Text = m_ReceiptDto.ReceiptDate.ToString("yyyy/MM/dd");
            //开票人
            this.TblCreate.Text = LoginHelper.CurrentUser.RealName;
            //受理中心
            TblAdmissibilityCentral.Text = EnumHelper.GetEnumDesc(ConfigHelper.GetCurrentReceptionCenter());
            //付款性质
            this.TblPaymentNature.Text = EnumHelper.GetEnumDesc(m_ReceiptDto.OrderBizType);
            //付款方式
            this.TblPaymentMethod.Text = EnumHelper.GetEnumDesc(m_ReceiptDto.PaymentChannel);
            //付款金额（大写）
            txtPaymentAmountUpperCase.Text = ConvertHelper.ConvertMoney(m_ReceiptDto.Money);

            //生成条码
            var bandCode = Code128String.Get(m_ReceiptDto.CaseId);
            string sStr1 = "";
            string sStr2 = "";
            string sStr3 = "";
            string sStr4 = "";
            StringBuilder newStr = new StringBuilder();
            sStr1 += m_ReceiptDto.CaseId.Substring(0, 4);
            newStr.Append(sStr1);
            newStr.Append(" ");
            sStr2 += m_ReceiptDto.CaseId.Substring(4, 4);
            newStr.Append(sStr2);
            newStr.Append(" ");
            sStr3 += m_ReceiptDto.CaseId.Substring(8, 4);
            newStr.Append(sStr3);
            newStr.Append(" ");
            sStr4 += m_ReceiptDto.CaseId.Substring(12, 2);
            newStr.Append(sStr4);
            Bitmap bitmap = new Bitmap(220, 80);
            Graphics g = Graphics.FromImage(bitmap);
            g.DrawString(bandCode, Code128_Font, MyBrush, BarLocation);
            g.DrawString(newStr.ToString(), Count_Font, MyBrush, BarCodeLocation);

            //生成条形码图片
            var writer = new ZXing.Presentation.BarcodeWriter
            {
                Format = BarcodeFormat.CODE_128,
                Options = new ZXing.Common.EncodingOptions
                {
                    Height = (int)imgCaseId.Height,
                    Width = (int)imgCaseId.Width,
                    Margin = 0
                }
            };
            var image = writer.Write(m_ReceiptDto.CaseId);
            imgCaseId.Source = image;
            this.img_Value.Text = newStr.ToString();

            //生成PDF417图片
            var writerPdf = new ZXing.Presentation.BarcodeWriter
            {
                Format = BarcodeFormat.PDF_417,
                Options = new ZXing.Common.EncodingOptions
                {
                    Height = (int)imgCaseIdPdf417.Height,
                    Width = (int)imgCaseIdPdf417.Width,
                    Margin = 0
                }
            };
            var imagePdf = writerPdf.Write("");
            imgCaseIdPdf417.Source = imagePdf;
        }

        /// <summary>
        /// 保存并打印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void BtnPrint_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                #region 复核
                var reviewRule = ReviewProxyService.GetInstanse().ReviewRule(Enumerations.ReviewCategory.Receipt, m_OrderNo, LoginHelper.CurrentUser.SysNo);
                if (reviewRule != null)
                {
                    var reviewDialog = new ReviewBaseWindow(reviewRule.SysNo, string.Join(",", reviewRule.ReviewName));
                    reviewDialog.ShowDialog(reviewRule.ReviewMode);
                    if (!reviewDialog.Success)
                    {
                        MessageBox.Show("复核失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }
                #endregion

                #region 收据打印机信息
                int printCount = 0;

                if (!string.IsNullOrWhiteSpace(_ReceiptPrintName))
                {
                    for (int j = 0; j < _ListPrint.Count; j++)
                    {
                        if (_ReceiptPrintName == _ListPrint[j].PrintName)
                        {
                            printCount++;
                        }
                    }
                }
                else //为空，则 收据打印机未分配
                {
                    MessageBox.Show("该收据打印机未设置，无法进行打印操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (printCount == 0)
                {
                    MessageBox.Show("该收据打印机未设置，无法进行打印操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (_ReceiptPrintName.Contains("未分配"))
                {
                    MessageBox.Show("该收据打印机未设置，无法进行打印操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                #endregion


                PageHelper.PageActiveLoadingControl(this.ctrlLoading, "正在处理,请稍候...");
                var result = await SaveData();
                if (result.Success)
                {
                    BtnPrint.IsEnabled = false;
                    ctrlLoading.IsBusy = false;
                    this.DataContext = m_ReceiptDto;//重新设置数据源
                    _SecurityCode = m_ReceiptDto.SecurityCode; //PDF417验证码
                    PrintVisual();
                }
                else
                {
                    System.Windows.MessageBox.Show("保存收据失败无法打印:" + result.ResultMessage, "系统异常", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                ctrlLoading.IsBusy = false;
            }
            catch (Exception ex)
            {
                ctrlLoading.IsBusy = false;
                System.Windows.MessageBox.Show("操作发生系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 修改数据
        /// </summary>
        /// <returns></returns>
        private Task<OperationResult> SaveData()
        {
            return Task.Run(() =>
            {
                OperationResult result = null;
                if (!ReOpenReceipt)
                {
                    result = ReceiptProxyService.GetInstanse().GenerateReceipt(m_OrderNo,
                        LoginHelper.CurrentUser.SysNo,
                        m_ReceiptDto.Address,
                        m_ReceiptDto.PostCode,
                        m_ReceiptDto.MobilePhone, ref m_ReceiptDto,
                        m_Trades);

                }
                else
                {
                    result = ReceiptProxyService.GetInstanse().RePrintReceipt(m_ReceiptDto.SysNo,LoginHelper.CurrentUser.SysNo,ref m_ReceiptDto, "");
                }

                return result;
            });
        }

        /// <summary>
        /// 调用打印
        /// </summary>
        public void PrintVisual()
        {

            for (int i = 0; i <= 2; i++)
            {
                PropertyReceiptModel _ReceiptPage = new PropertyReceiptModel();
                #region 赋值
                _ReceiptPage.CaseId = m_ReceiptDto.CaseId;
                _ReceiptPage.ReceiptDate = m_ReceiptDto.ReceiptDate.ToString("yyyy/MM/dd"); //收据日期
                _ReceiptPage.ReceiptNo = m_ReceiptDto.ReceiptNo;
                _ReceiptPage.ReceiptSerialNo = m_ReceiptDto.ReceiptSerialNo; //票据流水号
                _ReceiptPage.TenementAddress = m_ReceiptDto.TenementAddress;
                _ReceiptPage.PaymentAmountUpperCase = ConvertHelper.ConvertMoney(m_ReceiptDto.Money);
                _ReceiptPage.Money = m_ReceiptDto.Money.ToString();
                //_ReceiptPage.Money = m_ReceiptDto.Money.ToString().EndsWith(".00") ? m_ReceiptDto.Money.ToString().TrimEnd('0').TrimEnd('0').TrimEnd('.') : m_ReceiptDto.Money.ToString("f2");
                //付款性质
                _ReceiptPage.OrderBizType = EnumHelper.GetEnumDesc(m_ReceiptDto.OrderBizType);
                //付款方式
                _ReceiptPage.PaymentChannel = EnumHelper.GetEnumDesc(m_ReceiptDto.PaymentChannel);
                _ReceiptPage.Payer = m_ReceiptDto.Payer;
                _ReceiptPage.Address = m_ReceiptDto.Address;
                _ReceiptPage.MobilePhone = m_ReceiptDto.MobilePhone;
                _ReceiptPage.PostCode = m_ReceiptDto.PostCode;
                _ReceiptPage.Creater = LoginHelper.CurrentUser.RealName;
                _ReceiptPage.AdmissibilityCentral = EnumHelper.GetEnumDesc(ConfigHelper.GetCurrentReceptionCenter());

                //生成条码
                var bandCode = Code128String.Get(m_ReceiptDto.CaseId);
                string sStr1 = "";
                string sStr2 = "";
                string sStr3 = "";
                string sStr4 = "";
                StringBuilder newStr = new StringBuilder();
                sStr1 += m_ReceiptDto.CaseId.Substring(0, 4);
                newStr.Append(sStr1);
                newStr.Append(" ");
                sStr2 += m_ReceiptDto.CaseId.Substring(4, 4);
                newStr.Append(sStr2);
                newStr.Append(" ");
                sStr3 += m_ReceiptDto.CaseId.Substring(8, 4);
                newStr.Append(sStr3);
                newStr.Append(" ");
                sStr4 += m_ReceiptDto.CaseId.Substring(12, 2);
                newStr.Append(sStr4);
                Bitmap bitmap = new Bitmap(220, 80);
                Graphics g = Graphics.FromImage(bitmap);
                g.DrawString(bandCode, Code128_Font, MyBrush, BarLocation);
                g.DrawString(newStr.ToString(), Count_Font, MyBrush, BarCodeLocation);
                //生成条形码图片
                var writer = new ZXing.Presentation.BarcodeWriter
                {
                    Format = BarcodeFormat.CODE_128,
                    Options = new ZXing.Common.EncodingOptions
                    {
                        Height = (int)imgCaseId.Height,
                        Width = (int)imgCaseId.Width,
                        Margin = 0
                    }
                };
                _ReceiptPage.ImageBarCode = writer.Write(m_ReceiptDto.CaseId);
                _ReceiptPage.CaseIdBarCode = newStr.ToString();

                //生成PDF417图片
                var writerPdf = new ZXing.Presentation.BarcodeWriter
                {
                    Format = BarcodeFormat.PDF_417,
                    Options = new ZXing.Common.EncodingOptions
                    {
                        Height = (int)imgCaseIdPdf417.Height,
                        Width = (int)imgCaseIdPdf417.Width,
                        Margin = 0
                    }
                };
                _ReceiptPage.ImagePdf = writerPdf.Write(_SecurityCode);

                #endregion

                var printDialog = new PrintDialog();

                PrintDialogHelper.SetPrintProperty(printDialog);

                var printQueue = PrintDialogHelper.SelectedPrintServer(System.Environment.MachineName, _ReceiptPrintName);
                if (printQueue != null)
                {
                    printDialog.PrintQueue = printQueue;

                    FlowDocument doc = (FlowDocument)Application.LoadComponent(new Uri("/PinganHouse.SHHTS.UI.WPF;component/Main/Funds/Win/PrintDocument.xaml", UriKind.RelativeOrAbsolute));

                    if (i == 0)
                    {
                        _ReceiptPage.Num = "一";
                        _ReceiptPage.Unite = "客";
                        _ReceiptPage.Unite1 = "户";
                    }
                    else if (i == 1)
                    {
                        _ReceiptPage.Num = "二";
                        _ReceiptPage.Unite = "记";
                        _ReceiptPage.Unite1 = "账";
                    }
                    else if (i == 2)
                    {
                        _ReceiptPage.Num = "三";
                        _ReceiptPage.Unite = "存";
                        _ReceiptPage.Unite1 = "根";
                    }
                    doc.DataContext = _ReceiptPage;
                    Dispatcher.BeginInvoke(new DoPrintMethod(DoPrint), DispatcherPriority.ApplicationIdle, printDialog, ((IDocumentPaginatorSource)doc).DocumentPaginator);
                }
            }
        }

        private void Print()
        {
            PrintDialog pdlg = new PrintDialog();
            FlowDocument doc = (FlowDocument)Application.LoadComponent(new Uri("/PinganHouse.SHHTS.UI.WPF;component/Main/Funds/Win/PrintDocument.xaml", UriKind.RelativeOrAbsolute));

            doc.DataContext = m_ReceiptDto;
            Dispatcher.BeginInvoke(new DoPrintMethod(DoPrint), DispatcherPriority.ApplicationIdle, pdlg, ((IDocumentPaginatorSource)doc).DocumentPaginator);
        }

        /// <summary>
        /// 执行打印
        /// </summary>
        /// <param name="pdlg"></param>
        /// <param name="paginator"></param>
        private void DoPrint(PrintDialog pdlg, DocumentPaginator paginator)
        {
            pdlg.PrintDocument(paginator, "Order Document");
        }


        /// <summary>
        /// 窗体关闭事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closed(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}
