﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using MessageBox = System.Windows.MessageBox;

namespace PinganHouse.SHHTS.UI.WPF.Main
{
    
    /// <summary>
    /// CaptureIdCard.xaml 的交互逻辑
    /// </summary>
    public partial class CaptureIdCard : Window
    {
        #region 全局变量
        /// <summary>
        /// 买方或卖家编号
        /// </summary>
        string _personId;

        private Window window = null;
        CameraInterfaceInvoke _cameraInvoke;
        private UInt32 _mDeviceHandle = 0;
        private Int32 _mPreviewHWnd;
        #endregion
        public CaptureIdCard(string personId)
        {
            InitializeComponent();
            this._personId = personId;
            this.KeyDown += CaptureIdCard_KeyDown;
            FindGaopaiyi();
        }
      
        #region 页面加载
        private void CaptureIdCard_OnLoaded(object sender, RoutedEventArgs e)
        {
            HwndSource hwndSource = PresentationSource.FromVisual(PictureBox) as HwndSource;
            _mPreviewHWnd = hwndSource.Handle.ToInt32();
            //_mPreviewHWnd = this.PictureBox.Handle.ToInt32();           
            _cameraInvoke.func_setPreviewWindow(_mDeviceHandle, _mPreviewHWnd);
            //设置自动裁边
            _cameraInvoke.func_setAutoCrop(_mDeviceHandle, 1);
            if (_cameraInvoke.strErrorMessage != string.Empty)
            {
                MessageBox.Show(_cameraInvoke.strErrorMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                _cameraInvoke.func_setCaptureSizeByIndex(_mDeviceHandle, 6);
                _cameraInvoke.func_startPreview(_mDeviceHandle);                
            }
        }
        /// <summary>
        /// 查找高拍仪
        /// </summary>
        public void FindGaopaiyi()
        {
            //再重新连接高拍仪还会报错
            _cameraInvoke = new CameraInterfaceInvoke("./CameraInterface.dll");
            _mDeviceHandle = _cameraInvoke.func_InitDevice(0);
            string errorMessage = Marshal.PtrToStringAnsi(_cameraInvoke.func_getLastErrorMessage());
            if (errorMessage != null && errorMessage.Equals("初始化摄像头失败 ！"))
            {
                MessageBox.Show("高拍仪未连接或者初始化摄像头失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                this.Show();
                CaptrueIdCardWindow.Width = 700;
            }
        }
        #endregion

        #region 快捷键操作

        private int _enterCount = 0;
        /// <summary>
        /// 快捷键操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void CaptureIdCard_KeyDown(object sender, KeyEventArgs e)
        {
            var keyDown = e.Key;
            _enterCount++;
            switch (keyDown)
            {
                case Key.Enter:                    
                    EnterPress(_enterCount);
                    break;
                case Key.Escape:
                    if (_mDeviceHandle != 0)
                    {
                        _cameraInvoke.func_CloseDevice(_mDeviceHandle);
                    }
                    break;
            }       
          }


        private void EnterPress(int enterCount)
        {
            int switchCount = enterCount%3;
            switch (switchCount)
            {
                case 1:
                    CaptureImg(Img1,0);
                    break;
                case 2:
                    CaptureImg(Img2,1);
                    break;
                case 0:
                    if (_mDeviceHandle != 0)
                    {
                        _cameraInvoke.func_CloseDevice(_mDeviceHandle);
                    }
                    this.Close();
                    break;
            }
        }

        private void CaptureImg(Image img,int tag)
        {
            //图片途径         
            String filePath = ".\\IdCardImg";
            if (!Directory.Exists(filePath)) 
            {
                Directory.CreateDirectory(filePath);
            }
            string imgPath = filePath + "\\" + tag + ".jpg";
            //存储图片，判断是否成功
            _cameraInvoke.func_captureImage(_mDeviceHandle, imgPath);
            _cameraInvoke.func_startPreview(_mDeviceHandle);
            //加载图片
            if (File.Exists(imgPath))
            {
                var imageSource = new BitmapImage();
                var fileStream = new FileStream(imgPath, FileMode.Open, FileAccess.Read);
                imageSource.BeginInit();
                imageSource.StreamSource = fileStream;
                imageSource.EndInit();
                img.Source = imageSource;
                //上传
                CaseHelper.AddAttachment(_personId, filePath, AttachmentType.身份证, fileStream, UploadAction);                
            }

        }
        private Action<AttachmentContent, UploadAttachmentResult> UploadAction = (attachInfo, result) =>
        {
            if (!result.Success)
            {
                MessageBox.Show("上传失败，请重新上传", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                try
                {               
                    attachInfo.AttachmentContentData.Close();
                    attachInfo.AttachmentContentData.Dispose();
                    if (File.Exists(attachInfo.FileName))
                    {
                        File.Delete(attachInfo.FileName);
                    }
                }
                catch
                {

                }
            }
        };
        #endregion

        #region 页面离开
        private void CaptureIdCard_OnUnloaded(object sender, RoutedEventArgs e)
        {
            this.KeyDown -= CaptureIdCard_KeyDown;
        }
        #endregion

        #region 图片保存
        /// <summary>
        /// 图片保存
        /// </summary>     
        private void SaveBtn_OnClick(object sender, RoutedEventArgs e)
        {
            if (_mDeviceHandle != 0)
            {
                _cameraInvoke.func_CloseDevice(_mDeviceHandle);
            }
        }
        #endregion
    }
}
