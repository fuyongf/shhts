﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Purchase
{
    /// <summary>
    /// PurchaseResultDialog.xaml 的交互逻辑
    /// </summary>
    public partial class PurchaseResultDialog : Window
    {
        /// <summary>
        /// 限购结果
        /// </summary>
        /// <param name="houseNum">可购买</param>
        /// <param name="current">当前可买套数</param>
        
        public PurchaseResultDialog(int houseNum, int current)
        {
            InitializeComponent();
            txt_current.Text = Convert.ToString(current);
            txt_house.Text = Convert.ToString(houseNum);
        }
    }
}
