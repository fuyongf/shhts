﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Purchase.Ctrl
{

    /// <summary>
    /// PurchaseCtrl.xaml 的交互逻辑
    /// </summary>
    public partial class PurchaseCtrl : UserControl
    {
        public delegate void ReturnErrorResultHandle(string message);
        public delegate void ReturnPurchaseResultHandle(int houseNum, int? currentNum=null);
        /// <summary>
        /// 返回限购结果
        /// </summary>
        public event ReturnPurchaseResultHandle ReturnPurchaseResultEvent;
        public event ReturnErrorResultHandle ReturnErrorResultEvent;

        /// <summary>
        /// 触发事件
        /// </summary>
        /// <param name="houseNum"></param>
        protected virtual void ReturnPurchaseResult(int houseNum, int? currentNum = null)
        {
            if (this.ReturnPurchaseResultEvent != null)
                this.ReturnPurchaseResultEvent(houseNum, currentNum);
        }

        /// <summary>
        /// 查询限购有错误
        /// </summary>
        /// <param name="message"></param>
        protected virtual void ReturnErrorResult(string message)
        {
            if (this.ReturnPurchaseResultEvent != null)
                this.ReturnErrorResultEvent(message);
        }

        /// <summary>
        /// 获取查询结果
        /// </summary>
        public virtual void GetPurchaseResult()
        {

        }
    }
}
