﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Purchase.Ctrl
{
    /// <summary>
    /// ShangHaiCtrl.xaml 的交互逻辑
    /// </summary>
    public partial class ShangHaiCtrl : PurchaseCtrl
    {
        /// <summary>
        ///  是否已婚
        /// </summary>
        private bool m_IsMarryed = true;

        /// <summary>
        /// 构造函数
        /// </summary>
        public ShangHaiCtrl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 获取限购结果
        /// </summary>
        public override void GetPurchaseResult()
        {
            if (m_IsMarryed)
            {
                GetResultForMarryed();
            }
            else
            {
                GetResultForUnMarryed();
            }
        }

        /// <summary>
        /// 已婚未婚
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var button = sender as Button;
                var tag = Convert.ToInt32(button.Tag);
                row_2.IsEnabled = true;
                row_3.IsEnabled = true;
                switch (tag)
                {
                    case 1:
                        txtshangHai.Text = "本人,配偶,未成年子女拥有住房";
                        btnMarried.Visibility = Visibility.Visible;
                        btnMarried1.Visibility = Visibility.Collapsed;
                        btnUnmarried.Visibility = Visibility.Collapsed;
                        btnUnmarried1.Visibility = Visibility.Visible;
                        m_IsMarryed = true;
                        txtNum0.Text = string.Empty;
                        txtNum0.Focus();
                        txtNum1.Text = string.Empty;
                        txtNum2.Text = string.Empty;
                        txtNum3.Text = string.Empty;
                        txtNum4.Text = string.Empty;
                        break;
                    case 0: //未婚
                        txtshangHai.Text = "本人,未成年子女拥有住房";
                        btnMarried.Visibility=Visibility.Collapsed;
                        btnMarried1.Visibility = Visibility.Visible;
                        btnUnmarried.Visibility = Visibility.Visible;
                        btnUnmarried1.Visibility = Visibility.Collapsed;
                        row_2.IsEnabled = false ;
                        row_3.IsEnabled = false;
                        txtNum0.Text = string.Empty;
                        txtNum0.Focus();
                        txtNum1.Text = string.Empty;
                        txtNum2.Text = string.Empty;
                        txtNum3.Text = string.Empty;
                        txtNum4.Text = string.Empty;
                        m_IsMarryed = false;
                        break;
                }
            }
            catch (Exception)
            {

            }
        }



        /// <summary>
        /// 获取未婚限购结果
        /// </summary>
        /// <param name="tag"></param>
        private void GetResultForUnMarryed()
        {
            try
            {
                var currentNum = 0;
                var num = 0;
                //本人2011年1月28日前与父母拥有住房
                num = 0;
                int.TryParse(txtNum0.Text.Trim(), out num);
                num=num - 2 < 0 ? 0 : num - 2;
                currentNum += num;
                //本人2011年1月28日后与父母拥有住房
                num = 0;
                int.TryParse(txtNum1.Text.Trim(), out num);
                currentNum += num;
                //配偶2011年1月28日前与父母拥有住房
                num = 0;
                int.TryParse(txtNum2.Text.Trim(), out num);
                num = num - 2 < 0 ? 0 : num - 2;
                currentNum += num;
                //配偶2011年1月28日后与父母拥有住房
                num = 0;
                int.TryParse(txtNum3.Text.Trim(), out num);
                currentNum += num;
                //本人,配偶,未成年子女佣有住房
                num = 0;
                int.TryParse(txtNum4.Text.Trim(), out num);
                currentNum += num;
                ReturnPurchaseResult(1-currentNum>0?1-currentNum:0,currentNum);
            }
            catch (Exception)
            {
                ReturnPurchaseResult(0);
            }
        }

      
        /// <summary>
        /// 获取已婚限购结果
        /// </summary>
        /// <param name="tag"></param>
        private void GetResultForMarryed()
        {
            try
            {
                var currentNum = 0;
                var num = 0;
                //本人2011年1月28日前与父母拥有住房
                num = 0;
                int.TryParse(txtNum0.Text.Trim(), out num);
                num = num - 2 < 0 ? 0 : num - 2;
                currentNum += num;
                //本人2011年1月28日后与父母拥有住房
                num = 0;
                int.TryParse(txtNum1.Text.Trim(), out num);
                currentNum += num;
                //配偶2011年1月28日前与父母拥有住房
                num = 0;
                int.TryParse(txtNum2.Text.Trim(), out num);
                num = num - 2 < 0 ? 0 : num - 2;
                currentNum += num;
                //配偶2011年1月28日后与父母拥有住房
                num = 0;
                int.TryParse(txtNum3.Text.Trim(), out num);
                currentNum += num;
                //本人,配偶,未成年子女佣有住房
                num = 0;
                int.TryParse( txtNum4.Text.Trim(),out num);
                currentNum += num;
                ReturnPurchaseResult(2 - currentNum > 0 ? 2 - currentNum : 0, currentNum);
               
            }
            catch (Exception)
            {
                ReturnPurchaseResult(0);
            }
        }

        
    }
}
