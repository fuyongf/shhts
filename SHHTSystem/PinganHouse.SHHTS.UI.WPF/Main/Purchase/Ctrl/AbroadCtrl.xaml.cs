﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Purchase.Ctrl
{
    /// <summary>
    /// Abroad.xaml 的交互逻辑
    /// </summary>
    public partial class AbroadCtrl : PurchaseCtrl
    {
        public AbroadCtrl()
        {
            InitializeComponent();
        }

        /// <summary>
        ///  查询限购结构
        /// </summary>
        public override void GetPurchaseResult()
        {
            try
            {
                //现有住房套数
                var current = 0;
                int.TryParse(houseNum.Text.Trim(), out current);

                //住房套数
                if (string.IsNullOrEmpty(houseNum.Text.Trim()))
                {
                    ReturnErrorResult("请填写现在拥有住房套数.");
                    return;
                }
                //确定是否有税单
                if (!Convert.ToBoolean(radioHave.IsChecked) && !Convert.ToBoolean(radioUnHave.IsChecked))
                {
                    ReturnErrorResult("请确认是否有税单.");
                    return;
                }
                //构造结果
                var dicResult = new Dictionary<string, int>();
                dicResult.Add("1-无", 0);
                dicResult.Add("0-否", 0);
                dicResult.Add("0-是", 1);
                //确定结果
                var result = new StringBuilder();
                result.Append(houseNum.Text.Trim());
                result.Append("-");
                var haveTax = Convert.ToBoolean(radioHave.IsChecked);
                result.Append(haveTax ? "是" : "否");
                var key = result.ToString().TrimEnd('-');
                if (dicResult.ContainsKey(key))
                {
                    ReturnPurchaseResult(dicResult[key], current);
                    return;
                }
                else
                {
                    ReturnPurchaseResult(0,current);
                    return;
                }  
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
