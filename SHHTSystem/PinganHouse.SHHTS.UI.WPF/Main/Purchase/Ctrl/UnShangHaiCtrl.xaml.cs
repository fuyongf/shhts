﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Purchase.Ctrl
{
    /// <summary>
    /// UnShangHaiCtrl.xaml 的交互逻辑
    /// </summary>
    public partial class UnShangHaiCtrl : PurchaseCtrl
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public UnShangHaiCtrl()
        {
            InitializeComponent();
        }

        /// <summary>
        ///  查询限购结构
        /// </summary>
        public override void GetPurchaseResult()
        {
            //现有住房套数
            var current = 0;
            int.TryParse(houseNum.Text.Trim(),out current);
            var noMarryResult = Convert.ToBoolean(noMarry.IsChecked);
            var marryResult = Convert.ToBoolean(marry.IsChecked);

            //没有选择
            if (!noMarryResult && 
                !marryResult)
            {
                ReturnErrorResult("请选择婚姻状况.");
                return;
            }

            //如果双非未婚直接无法购买
            if (noMarryResult)
            {
                ReturnPurchaseResult(0, current);
            }
           
            if (marryResult)
            {
                //住房套数
                if (string.IsNullOrEmpty(houseNum.Text.Trim()))
                {
                    ReturnErrorResult("请填写现在拥有住房套数.");
                    return;
                }
                //确定是否有税单
                if (!Convert.ToBoolean(radioHave.IsChecked) && !Convert.ToBoolean(radioUnHave.IsChecked))
                {
                    ReturnErrorResult("请确认是否有税单.");
                    return;
                }
                //构造结果
                var dicResult = new Dictionary<string, int>();
                dicResult.Add("1-无", 0);
                dicResult.Add("0-否", 0);
                dicResult.Add("0-是", 1);
                //确定结果
                var result = new StringBuilder();
                result.Append(houseNum.Text.Trim());
                result.Append("-");
                var haveTax = Convert.ToBoolean(radioHave.IsChecked);
                result.Append(haveTax ? "是" : "否");
                var key = result.ToString().TrimEnd('-');
                if (dicResult.ContainsKey(key))
                {
                    ReturnPurchaseResult(dicResult[key], current);
                    return;
                }
                else { 
                    ReturnPurchaseResult(0,current);
                    return;
                }               
            }
           
        }

        /// <summary>
        /// 选择已婚
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Marry_Checked(object sender, RoutedEventArgs e)
        {
            var checkCtrl = sender as CheckBox;
            if (checkCtrl.IsChecked == null) return;
            if (Convert.ToBoolean(checkCtrl.IsChecked))
            {
                noMarry.IsChecked = false;
                houseNum.IsEnabled = true;
                radioHave.IsEnabled = true;
                radioUnHave.IsEnabled = true;
            }
        }

        /// <summary>
        /// 选择未婚
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UnMarry_Checked(object sender, RoutedEventArgs e)
        {
            var checkCtrl = sender as CheckBox;
            if (checkCtrl.IsChecked == null) return;
            if (Convert.ToBoolean(checkCtrl.IsChecked))
            {
                marry.IsChecked = false;
                houseNum.IsEnabled = false;
                radioHave.IsEnabled = false;
                radioUnHave.IsEnabled = false;
            }
        }

        /// <summary>
        /// 已婚不选
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Marry_Unchecked(object sender, RoutedEventArgs e)
        {
            houseNum.Text = string.Empty;
            radioHave.IsChecked = false;
            radioUnHave.IsChecked = false;
        }
    }
}
