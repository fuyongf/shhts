﻿using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Main.Purchase.Ctrl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Purchase
{
    /// <summary>
    /// PurchaseQueryPage.xaml 的交互逻辑
    /// </summary>
    public partial class PurchaseQueryPage : Page
    {
        /// <summary>
        /// 限购标致位置
        /// </summary>
        private PurchaseCtrl m_PurchaseCtrl;



        /// <summary>
        /// 构造函数
        /// </summary>
        public PurchaseQueryPage()
        {
            InitializeComponent();
            UnShangHai.IsChecked = true;
        }

        /// <summary>
        /// 确定选择项目
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                var radio = sender as RadioButton;
                var tag = Convert.ToString(radio.Tag);
                switch (tag)
                {
                    case "Abroad":
                        m_PurchaseCtrl = new AbroadCtrl();
                        break;
                    case "ShangHai":
                        m_PurchaseCtrl = new ShangHaiCtrl();
                        break;
                    case "UnShangHai":
                        m_PurchaseCtrl = new UnShangHaiCtrl();                       
                        break;
                }
                m_PurchaseCtrl.ReturnPurchaseResultEvent += PurchaseCtrl_ReturnErrorResultEvent;
                m_PurchaseCtrl.ReturnErrorResultEvent+=PurchaseCtrl_ReturnErrorResultEvent;
                if (m_PurchaseCtrl != null)
                {
                    mainPanel.Children.Clear();
                    mainPanel.Children.Add(m_PurchaseCtrl);
                }
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// 返回错误
        /// </summary>
        /// <param name="message"></param>
        private void PurchaseCtrl_ReturnErrorResultEvent(string message)
        {
            if (string.IsNullOrEmpty(message)) return;
            MessageBox.Show(message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        /// <summary>
        /// 显示限购结果
        /// </summary>
        /// <param name="houseNum"></param>
        private void PurchaseCtrl_ReturnErrorResultEvent(int houseNum,int? currentNum = null)
        {
            new PurchaseResultDialog(houseNum, currentNum == null ? 0 : Convert.ToInt32(currentNum)).ShowDialog();
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (m_PurchaseCtrl == null) { return; }
                m_PurchaseCtrl.GetPurchaseResult();
            }
            catch (Exception)
            {
                MessageBox.Show("很抱歉,系统查询发生异常.","系统异常",MessageBoxButton.OK,MessageBoxImage.Error);
            }
        }
    }
}
