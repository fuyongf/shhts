﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Model;

namespace PinganHouse.SHHTS.UI.WPF.Main.Host
{
    /// <summary>
    /// FollowUpDialog.xaml 的交互逻辑
    /// </summary>
    public partial class FollowUpDialog : Window
    {
        private CaseManagementListPage.CaseManagementModel cm = null;
        public FollowUpDialog()
        {
            InitializeComponent();
        }

        public FollowUpDialog(CaseManagementListPage.CaseManagementModel cm)
            : this()
        {
            this.cm = cm;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ProccessType type = ProccessType.AverageDelay;
            if(comb1.IsChecked==true)type = ProccessType.Normal;
            if (comb2.IsChecked == true) type = ProccessType.AverageDelay;
            if (comb3.IsChecked == true) type = ProccessType.SeriousDelay;
            string re = remarks.Text;
            OperationResult result = CaseProxyService.GetInstanse()
                .SetCaseTrail(cm.Case.CaseId, type, re, LoginHelper.CurrentUser.SysNo);
            if (result.Success)
            {
                MessageBox.Show("操作成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                this.DialogResult = true;
            }
            else
            {
                MessageBox.Show("操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            this.Close();
               
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }


        private void FollowUpDialog_OnLoaded(object sender, RoutedEventArgs e)
        {
            this.addressLabel.Content = cm.Case.TenementAddress;
            this.caseIdLabel.Content = cm.Case.CaseId +"       买家："+cm.BuyerName;
        }
    }
}
