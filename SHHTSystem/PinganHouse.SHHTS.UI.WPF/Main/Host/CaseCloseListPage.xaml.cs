﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Main.Dialog;
using PinganHouse.SHHTS.UI.WPF.Main.Nuclear;
using PinganHouse.SHHTS.UI.WPF.Main.PreCheck;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Host
{
    /// <summary>
    /// CaseCloseListPage.xaml 的交互逻辑
    /// </summary>
    public partial class CaseCloseListPage : Page
    {

        CaseStatus status = CaseStatus.ZB8 | CaseStatus.ZB13;

        private int PageSize = 15;
        /// <summary>
        /// 构造函数
        /// </summary>
        public CaseCloseListPage()
        {
            InitializeComponent();
            this.GridPager.PageNumChanged = this.GridPager_PagerIndexChanged;
            //获取案件状态
            var list = EnumHelper.InitCaseStatusToCombobox(status);
            caseStatusCb.ItemsSource = list;
            caseStatusCb.DisplayMemberPath = "DisplayMember";
            caseStatusCb.SelectedValuePath = "ValueMember";
            caseStatusCb.SelectedValue = 0;
            
        }
        /// <summary>
        /// 加载数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            GridPager.PageIndex = 1;
        }

        /// <summary>
        /// 搜索案件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchBtn_Click(object sender, RoutedEventArgs e)
        {
            this.GridPager.PageIndex = 1;
        }
        /// <summary>
        /// 分页事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridPager_PagerIndexChanged(int pageIndex)
        {
            PageHelper.PageActiveLoadingControl(this.loading, "正在加载数据,请稍后...");
            this.GetDataToBindCtrl();
        }

        #region 页面数据取得方法
        /// <summary>
        /// 异步获取系统数据
        /// </summary>
        private async void GetDataToBindCtrl()
        {
            try
            {
                //案件编号
                var caseId = string.IsNullOrEmpty(this.txtCaseId.Text.Trim()) ? null : this.txtCaseId.Text.Trim();
                //案件地址
                var address = string.IsNullOrEmpty(this.txtAddress.Text.Trim()) ? null : this.txtAddress.Text.Trim();
                //卖家
                var buyerName = string.IsNullOrEmpty(this.txtBuyer.Text.Trim()) ? null : this.txtBuyer.Text.Trim();
                //经纪人
                var agencyName = string.IsNullOrEmpty(this.txtAgency.Text.Trim()) ? null : this.txtAgency.Text.Trim();
                //创建人
                var createrName = string.IsNullOrEmpty(this.txtCreater.Text.Trim()) ? null : this.txtCreater.Text.Trim();
                //开始时间
                DateTime startTime = DateTime.MinValue;
                if (!string.IsNullOrEmpty(dateStart.Text))
                {
                    DateTime.TryParse(dateStart.Text, out startTime);
                }
                //结束时间
                DateTime endTime = DateTime.MinValue;
                if (!string.IsNullOrEmpty(dateEnd.Text))
                {
                    DateTime.TryParse(dateEnd.Text, out endTime);
                }
                //案件状态选择
                var caseStatus = (CaseStatus)Enum.Parse(typeof(CaseStatus), Convert.ToString(caseStatusCb.SelectedValue));
                CaseStatus? selectStatus = caseStatus;
                if (caseStatus == CaseStatus.Unkown) { selectStatus = null; }
                var tupleModel = await GetDataGridData(caseId, address, buyerName, agencyName, startTime, endTime, selectStatus, createrName);
                this.dataGrid.DataContext = tupleModel.Item2;
                GridPager.Visibility = Visibility.Visible;
                GridPager.PageCount = tupleModel.Item1 / PageSize;
                if (tupleModel.Item1 % PageSize != 0) GridPager.PageCount++;
                this.loading.IsBusy = false;
            }
            catch (Exception)
            {
                this.loading.IsBusy = false;
                MessageBox.Show("加载数据发生系统异常.", "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        /// <summary>
        /// 获取页面数据
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="size"></param>
        /// <param name="caseId"></param>
        /// <param name="address"></param>
        /// <param name="buyerName"></param>
        /// <param name="agencyName"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        private Task<Tuple<int, List<CastModel>>> GetDataGridData(string caseId, string address, string buyerName, string agencyName,
            DateTime startTime, DateTime endTime, CaseStatus? selectStatus, string createrName)
        {
            return Task.Run(() =>
            {
                int cnt = 0;
                var caseModelList = new List<CastModel>();
                if (startTime == DateTime.MinValue && endTime == DateTime.MinValue)
                {
                    var caseList = CaseProxyService.GetInstanse().GetPaginatedList(GridPager.PageIndex, PageSize, out cnt
                        , ConfigHelper.GetCurrentReceptionCenter(), status, selectStatus
                        , caseId, address, buyerName, agencyName, operationStatus: CaseStatus.HA1, creatorName: createrName);
                    if (caseList != null)
                    {
                        caseModelList = CastModel.InitData(caseList, CaseStatus.HA1);
                    }
                }
                else if (startTime != DateTime.MinValue && endTime != DateTime.MinValue)
                {
                    var caseList = CaseProxyService.GetInstanse().GetPaginatedList(GridPager.PageIndex, PageSize, out cnt
                        , ConfigHelper.GetCurrentReceptionCenter(), status, selectStatus
                        , caseId, address, buyerName, agencyName, startTime, endTime,
                        operationStatus: CaseStatus.HA1, creatorName: createrName);
                    if (caseList != null)
                    {
                        caseModelList = CastModel.InitData(caseList, CaseStatus.HA1);
                    }
                }
                else if (startTime != DateTime.MinValue && endTime == DateTime.MinValue)
                {
                    var caseList = CaseProxyService.GetInstanse().GetPaginatedList(GridPager.PageIndex, PageSize, out cnt
                        , ConfigHelper.GetCurrentReceptionCenter(), status, selectStatus
                        , caseId, address, buyerName, agencyName, startTime, operationStatus: CaseStatus.HA1, creatorName: createrName);
                    if (caseList != null)
                    {
                        caseModelList = CastModel.InitData(caseList, CaseStatus.HA1);
                    }
                }
                else if (startTime == DateTime.MinValue && endTime != DateTime.MinValue)
                {
                    var caseList = CaseProxyService.GetInstanse().GetPaginatedList(GridPager.PageIndex, PageSize, out cnt
                        , ConfigHelper.GetCurrentReceptionCenter(), status, selectStatus
                        , caseId, address, buyerName, agencyName, null, endTime, operationStatus: CaseStatus.HA1, creatorName: createrName);
                    if (caseList != null)
                    {
                        caseModelList = CastModel.InitData(caseList, CaseStatus.HA1);
                    }
                }
                var dicResult = new Tuple<int, List<CastModel>>(cnt, caseModelList);
                return dicResult;
            });
        }
        #endregion


        #region  页面切换
        private void SwitchButtons_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, new HostListPage(int.Parse(((Button)sender).Tag.ToString())));
        }
        /// <summary>
        /// 案件管理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCaseManage_OnClick(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, new CaseManagementListPage());
        }

        /// <summary>
        /// 上传附件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AttachmentBtn_Click(object sender, RoutedEventArgs e)
        {
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            var attachments = new UploadAttachment(caseId);
            PageHelper.PageNavigateHelper(this, attachments);
        }
        #endregion

        /// <summary>
        /// 结案操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void CloseCase_Click(object sender, RoutedEventArgs e)
        {
            var msgResult = MessageBox.Show("确认结案？", "系统询问", MessageBoxButton.YesNo, MessageBoxImage.Question);
            try
            {
                if (msgResult == MessageBoxResult.Yes)
                {
                    var caseId = Convert.ToString(((Button)sender).CommandParameter);
                    //结案前检查
                    PageHelper.PageActiveLoadingControl(this.loading, "正在检查结案数据,请稍后...");
                    var checkResult = await CheckCaseResult(caseId);
                    if (!checkResult.Success)
                    {
                        MessageBox.Show("结案操作失败：" + checkResult.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    PageHelper.PageActiveLoadingControl(this.loading, "正在处理结案,请稍后...");
                    var result = await CloseCaseResult(caseId);
                    if (result.Success)
                    {
                        MessageBox.Show("结案操作成功.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                        GridPager_PagerIndexChanged(GridPager.PageIndex);
                    }
                    else
                    {
                        MessageBox.Show("结案操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("操作发生系统异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }finally{
                this.loading.IsBusy =false;
            }
        }

        /// <summary>
        /// 检查结案数据
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        private Task<OperationResult> CheckCaseResult(string caseId)
        {
            OperationResult result = new OperationResult(0, string.Empty);
            return Task.Run(() =>
            {
               #region 实收实付验证
                Decimal seller_IncomeAmount = 0;
                Decimal seller_ExpenditureAmount = 0;
                Decimal buyer_IncomeAmount = 0;
                Decimal buyer_ExpenditureAmount = 0;
                //验证实收实付
                var tradeAmountData = AccountServiceProxy.GetCaseTradeAmount(caseId);
                if (tradeAmountData != null)
                {
                    //卖方
                    var sellerAccount = tradeAmountData.Item1;
                    //买方
                    var buyerAccount = tradeAmountData.Item2;

                    //卖方实收
                    seller_IncomeAmount = sellerAccount[OrderType.Collection];
                    //卖方实付
                    seller_ExpenditureAmount = sellerAccount[OrderType.Payment];
                    //买方实收
                    buyer_IncomeAmount = buyerAccount[OrderType.Collection];
                    //买方实付
                    buyer_ExpenditureAmount = buyerAccount[OrderType.Payment];
                }
                #endregion

               #region 应收应付验证
               var customerFundFlowList = CustomerFundFlowServiceProxy.GetFlows(caseId);
               Decimal incomeAmount = 0;
               Decimal expenditureAmount = 0;
               //卖方应收
               Decimal incomeAmount_Seller = 0;
               //卖方应付
               Decimal expenditureAmount_Seller = 0;

               //买方应收
               Decimal incomeAmount_Buyer = 0;
               //买方应付
               Decimal expenditureAmount_Buyer = 0;
               foreach (var flow in customerFundFlowList)
               {
                   if (flow.Amount > 0)
                   {
                       incomeAmount += flow.Amount;
                       if (flow.CustomerType == CustomerType.Seller)
                           incomeAmount_Seller += flow.Amount;
                       else if (flow.CustomerType == CustomerType.Buyer)
                           incomeAmount_Buyer += flow.Amount;
                   }
                   else
                   {
                       expenditureAmount += flow.Amount;
                       if (flow.CustomerType == CustomerType.Seller)
                           expenditureAmount_Seller += flow.Amount;
                       else if (flow.CustomerType == CustomerType.Buyer)
                           expenditureAmount_Buyer += flow.Amount;
                   }
               }
                #endregion

               #region 验证金额是否相等
              //买方应收和买方实收 相等
               if (incomeAmount_Buyer != buyer_IncomeAmount)
               {
                   result = new OperationResult(1, "买方应收和买方实收不相等,无法结案.");
               }
               //买方应付和买方实收 相等
               if (expenditureAmount_Buyer != buyer_ExpenditureAmount)
               {
                   result = new OperationResult(1, "买方应付和买方实付不相等,无法结案.");
               }
               //卖方应收和卖方实收 相等
               if (seller_IncomeAmount != incomeAmount_Seller)
               {
                   result = new OperationResult(1, "卖方应收和卖方实收不相等,无法结案.");
               }
               //卖方应付和卖方实付 相等
               if (expenditureAmount_Seller != seller_ExpenditureAmount)
               {
                   result = new OperationResult(1, "卖方应付和卖方实付不相等,无法结案.");
               }
               #endregion

               #region 验证账户余额
               var buyerAccountBalance = AccountServiceProxy.GetBalance(AccountType.Buyer, caseId);
                var sellerAccountBalance = AccountServiceProxy.GetBalance(AccountType.Seller, caseId);
                if (buyerAccountBalance > 0 && sellerAccountBalance > 0)
                {
                    result = new OperationResult(1, "买卖方钱款均未结清,无法结案.");
                }
                else if (buyerAccountBalance > 0)
                {
                    result = new OperationResult(1, "买方钱款未结清,无法结案.");
                }
                else if (sellerAccountBalance > 0)
                {
                    result = new OperationResult(1, "卖方钱款未结清,无法结案.");
                }
                #endregion
              
                return result;
            });
        } 

        /// <summary>
        /// 结案操作
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        private Task<OperationResult> CloseCaseResult(string caseId)
        {
            OperationResult result = new OperationResult(1,string.Empty);
            return Task.Run(() =>
            {
                result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.ZB13, LoginHelper.CurrentUser.SysNo);
                return result;
            });
        }

        /// <summary>
        /// 案件明细
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RequestNavigate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var link = e.Source as Hyperlink;
                if (null != link)
                {
                    var caseId = Convert.ToString(link.NavigateUri);
                    var caseInfo = new CaseDetailInfo(caseId);
                    PageHelper.PageNavigateHelper(this, caseInfo);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("显示明细发生错误", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            e.Handled = true;
        }
    }
}
