﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF
{
	/// <summary>
	/// LoanChangesDialog.xaml 的交互逻辑
	/// </summary>
	public partial class LoanChangesDialog : Window
	{
        CastModel cm;
        BackgroundWorker getOtherDataBackWorker = new BackgroundWorker();
        CaseEvent caseEvent;
		public LoanChangesDialog()
		{
			this.InitializeComponent();
            getOtherDataBackWorker.WorkerSupportsCancellation = true;
            getOtherDataBackWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.backWorkerCompleted);
            getOtherDataBackWorker.DoWork += new DoWorkEventHandler(this.backWorkerDo);
        }

        private void backWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!e.Cancelled && caseEvent != null)
            {
                if(caseEvent.OtherDatas.ContainsKey("agent"))  this.TextBox1.Text = "从 "+caseEvent.OtherDatas["agent"]+" 变更为：";
            }
        }
        private void backWorkerDo(object sender, DoWorkEventArgs e)
        {
            var caseEvents = CaseProxyService.GetInstanse().GetCaseEvents(cm.CaseId, cm.CaseStatus, null);
            if (caseEvents.Count > 0) caseEvent = caseEvents[0];
        }

        public LoanChangesDialog(CastModel cm)
            : this()
        {
            this.cm = cm;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LabelLine1.Content = "案件编号：" + cm.CaseId + "      买方：" + cm.BuyerName2;
            LabelLine2.Content = "物业地址：" + cm.TenementAddress;
            List<String> comboxList = new List<string> { "请选择","平安银行", "专员", "第三方", "客户" };
            combox.ItemsSource = comboxList;
            if (cm.CaseStatus == CaseStatus.ZB9)//发起贷款申请
            {
                this.Title = "发起贷款申请";
                this.TextBox1.Text = "请选择受理方";
                combox.SelectedIndex = 0;
            }
            else
            {
                combox.SelectedIndex = 0;
                this.Title = "变更贷款申请";
                this.TextBox1.Text = "受理人变更为：";
                getOtherDataBackWorker.RunWorkerAsync();
            }
        }
           //取消
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }

        //确认
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(combox.Text))
            {
                MessageBox.Show("请选择受理方", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (combox.Text=="请选择")
            {
                MessageBox.Show("请选择受理方", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("agent", combox.Text);
            dic.Add("remarks",remarks.Text);
            OperationResult result;

            if (cm.CaseStatus == CaseStatus.ZB9 || cm.CaseStatus == CaseStatus.DK9)
            {
                result = CaseProxyService.GetInstanse().UpdateCaseStatus(cm.CaseId, CaseStatus.DK1, LoginHelper.CurrentUser.SysNo, dic);

            }
            else
            {
                result = CaseProxyService.GetInstanse().UpdateCaseEvent(caseEvent.SysNo, dic, LoginHelper.CurrentUser.SysNo);
            }

            if (result.Success)
            {
                MessageBox.Show("操作成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                this.DialogResult = true;
            }
            else
            {
                MessageBox.Show("操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            this.Hide();
        }
	}
}