﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Host
{
    /// <summary>
    /// BookedDialog.xaml 的交互逻辑
    /// </summary>
    public partial class BookedDialog : Window
    {

        BackgroundWorker getOtherDataBackWorker = new BackgroundWorker();
        CaseEvent caseEvent = null;
        
        /// <summary>
        /// true:第一次预约时间
        /// flase:表示修改预约时间
        /// </summary>
        bool firstSetBookTime = false;
        public BookedDialog()
        {
            InitializeComponent();
            getOtherDataBackWorker.WorkerSupportsCancellation = true;
            getOtherDataBackWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.backWorkerCompleted);
            getOtherDataBackWorker.DoWork += new DoWorkEventHandler(this.backWorkerDo);
        }

        private void backWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            LabelBookTime.Content = "预约时间：立即        更改为：";
            if (!e.Cancelled && caseEvent!=null)
            {
                if(caseEvent.OtherDatas.ContainsKey("booktime")) LabelBookTime.Content = "预约时间："+caseEvent.OtherDatas["booktime"]+"    更改为：";
                if (caseEvent.OtherDatas.ContainsKey("remarks")) remarks.Text = caseEvent.OtherDatas["remarks"].ToString();
            }
        }
        private void backWorkerDo(object sender, DoWorkEventArgs e)
        {
            var caseEvents = CaseProxyService.GetInstanse().GetCaseEvents(cm.CaseId, cm.CaseStatus, null);
            if (caseEvents.Count > 0) caseEvent = caseEvents[0];
        }


        CastModel cm;
        public BookedDialog(CastModel cm,bool firstSetBookTime):
            this()
        {
            this.firstSetBookTime = firstSetBookTime;
            this.cm = cm;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LabelLine1.Content = "案件编号：" + cm.CaseId + "      买方：" + cm.BuyerName2;
            LabelLine2.Content = "物业地址：" + cm.TenementAddress;
            if (firstSetBookTime)
            {
                LabelBookTime.Content = "预约时间：";
            }
            else
            {
                this.Title = "更改预约";
                LabelBookTime.Content = "预约时间：获取中...   更改为：";
                getOtherDataBackWorker.RunWorkerAsync();
            }
        }

        
        //取消
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }

        //确认
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("booktime", datePicker.Text);
            dic.Add("remarks", remarks.Text);


            if (firstSetBookTime||caseEvent==null)
            {
                CaseStatus currentStatus = cm.CaseStatus;
                CaseStatus nextStatus = CaseStatus.Unkown;
                if (currentStatus == CaseStatus.ZB4||currentStatus == CaseStatus.ZB1) nextStatus = CaseStatus.ZB1;
                else if (currentStatus == CaseStatus.JY6 || currentStatus == CaseStatus.JY8) nextStatus = CaseStatus.JY8;
              
                if (String.IsNullOrEmpty(datePicker.Text))
                {
                    MessageBox.Show("请选择预约时间", "警告", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                var result = CaseProxyService.GetInstanse().UpdateCaseStatus(cm.CaseId, nextStatus, LoginHelper.CurrentUser.SysNo, dic);
                if (result.Success)
                {
                    MessageBox.Show("操作成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                this.Hide();
            }
            else
            {
                var result = CaseProxyService.GetInstanse().UpdateCaseEvent(caseEvent.SysNo, dic, LoginHelper.CurrentUser.SysNo);
                if (result.Success)
                {
                    MessageBox.Show("操作成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                this.Hide();
            }
          
        }

    }
}
