﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using PinganHouse.SHHTS.UI.WPF.Main.PreCheck;
using PinganHouse.SHHTS.UI.WPF.Main.Dialog;
using System.Windows.Documents;
using PinganHouse.SHHTS.UI.WPF.Main.Nuclear;
using PinganHouse.SHHTS.AccessControl.Proxy;

namespace PinganHouse.SHHTS.UI.WPF.Main.Host
{
    /// <summary>
    /// HostListPage.xaml 的交互逻辑
    /// </summary>
    [SecurityAuthorize]
    public partial class HostListPage : Page
    {

        private int PageSize = 10;
        List<CastModel> caseModelList = null;
        public delegate void LoadDataCompleted(string errorMsg, List<CastModel> caseModelList, int pageCount);
        private LoadDataCompleted loadDataCompletedDelegate;

        public int currentSeletedBtnIndex = 0;
        Button currentSelectedSwitchButton = null;
        CaseStatus CheckTaxApplyStatus = CaseStatus.ZB4 | CaseStatus.ZB1 | CaseStatus.JY1 | CaseStatus.JY6 | CaseStatus.JY8 | CaseStatus.JY11 | CaseStatus.ZB7 | CaseStatus.JY11A1;
        CaseStatus RepayLoanApplyStatus = CaseStatus.ZB10 | CaseStatus.ZB3 | CaseStatus.HD1 | CaseStatus.ZB12 | CaseStatus.ZB7;
        CaseStatus LoadApplyStatus = CaseStatus.ZB9 | CaseStatus.DK1 | CaseStatus.DK2 | CaseStatus.DK9 | CaseStatus.ZB11 | CaseStatus.ZB7;
        CaseStatus CancelContractStatus = CaseStatus.ZB2 | CaseStatus.ZB5 | CaseStatus.ZB7;
        CaseStatus CaseEndStatus = (CaseStatus)(EnumHelper.getEnumListSum(typeof(CaseStatus)) ^ (long)CaseStatus.ZB2);
        CaseStatus LeaveHouseConfirmStatus = CaseStatus.ZB8 | CaseStatus.ZB9 | CaseStatus.DK8 | CaseStatus.ZB7;

        CaseStatus currentStatus = CaseStatus.Unkown;
        CaseStatus currenthostOperationNameStatus = CaseStatus.Unkown;
        CaseStatus currenthostOperationNameStatus2 = CaseStatus.Unkown;
        CaseStatus[] statusList = null;
        CaseStatus[] hostOperationNameStatusList = null;//主办
        private CaseStatus hostNameSearch = CaseStatus.Unkown;
        private CaseStatus[] hostNameSearchList = null;
        CaseStatus[] hostOperationNameStatusList2 = null;//发起人，受理人
        public HostListPage()
        {
            InitializeComponent();
            loadDataCompletedDelegate = this.DataLoadCompleted;
            statusList = new CaseStatus[6] { CheckTaxApplyStatus, RepayLoanApplyStatus, LoadApplyStatus, CancelContractStatus, CaseEndStatus, LeaveHouseConfirmStatus };
            hostOperationNameStatusList = new CaseStatus[6] { CaseStatus.ZB1, CaseStatus.ZB3, CaseStatus.DK1, CaseStatus.ZB2, CaseStatus.ZB7, CaseStatus.ZB8 };
            hostOperationNameStatusList2 = new CaseStatus[6] { CaseStatus.JY1, CaseStatus.HD1, CaseStatus.DK2, CaseStatus.ZB5, CaseStatus.ZB6, CaseStatus.JY2 };
        }

        public HostListPage(int switchButtonIndex)
            : this()
        {
            this.currentSeletedBtnIndex = switchButtonIndex;
        }

        /// <summary>
        /// 搜索数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchBtn_Click(object sender, RoutedEventArgs e)
        {
            this.pageControl.PageIndex = 1;
        }



        private void StartLoadDataAsyn()
        {
            caseModelList = null;

            //获取搜索参数：
            string caseId = string.IsNullOrEmpty(this.txtCaseId.Text.Trim()) ? null : this.txtCaseId.Text.Trim();
            string address = string.IsNullOrEmpty(this.txtAddress.Text.Trim()) ? null : this.txtAddress.Text.Trim();
            string buyerName = string.IsNullOrEmpty(this.txtBuyer.Text.Trim()) ? null : this.txtBuyer.Text.Trim();
            string agencyName = string.IsNullOrEmpty(this.txtAgency.Text.Trim()) ? null : this.txtAgency.Text.Trim();
            string createName = string.IsNullOrEmpty(this.txtCreateName.Text.Trim()) ? null : this.txtCreateName.Text.Trim();
            DateTime startTime = DateTime.MinValue;
            if (!string.IsNullOrEmpty(dateStart.Text)) DateTime.TryParse(dateStart.Text, out startTime);
            DateTime endTime = DateTime.Now;
            if (!string.IsNullOrEmpty(dateEnd.Text)) DateTime.TryParse(dateEnd.Text, out endTime);
            CaseStatus? caseStatus = null;
            if (caseStatusCb.SelectedValue != null) caseStatus = (CaseStatus)Enum.Parse(typeof(CaseStatus), Convert.ToString(caseStatusCb.SelectedValue));
            if (caseStatus == CaseStatus.Unkown) caseStatus = null;
            int pageIndex = pageControl.PageIndex;

            var thread = new Thread(delegate()
            {
                GetDataGridData(caseId, address, buyerName, agencyName, createName, startTime, endTime, selectStatus: caseStatus, pageIndex: pageIndex);
            });
            thread.IsBackground = true;

            PageHelper.PageActiveLoadingControl(this.loading, "正在加载数据,请稍后...");
            thread.Start();
        }
        private void GetDataGridData(string caseId, string address, string buyerName, string agencyName, string createName, DateTime startTime, DateTime endTime, CaseStatus? selectStatus, int pageIndex)
        {

            int pageCount = 0;
            try
            {
                int totalCount = 0;
                IList<CaseDto> caseList = CaseProxyService.GetInstanse().GetPaginatedList(pageIndex, PageSize, out totalCount, ConfigHelper.GetCurrentReceptionCenter(),
                    currentStatus, caseStatus: selectStatus, caseId: caseId, tenementAddress: address, buyerName: buyerName, agencyName: agencyName,
                    operationStatus: currentStatus | currenthostOperationNameStatus | currenthostOperationNameStatus2 | CaseStatus.HA2, finished: false,
                    topStatus: hostNameSearch, creatorName: createName);
                pageCount = totalCount / PageSize;
                if (totalCount % PageSize != 0) pageCount++;
                if (caseList != null)
                {
                    caseModelList = CastModel.InitData(caseList, currenthostOperationNameStatus, currenthostOperationNameStatus2);
                    //交房确认的时候：ZB9（无贷款）的时候，如果JY12（过户完成）不存在；DK8（银行已经放款）的时候，如果JY12（过户完成)不存在
                    if (currentStatus == LeaveHouseConfirmStatus)
                    {
                        for (int i = 0; i < caseModelList.Count; i++)
                        {
                            CastModel cModel = caseModelList[i];
                            if (cModel.CaseStatus == CaseStatus.ZB7 || cModel.CaseStatus == CaseStatus.ZB8) continue;
                            //查看是否过户完成
                            int pageSize = 0;
                            var cl = CaseProxyService.GetInstanse().GetPaginatedList(1, PageSize, out pageSize, ConfigHelper.GetCurrentReceptionCenter(), CaseStatus.JY12, null, cModel.CaseId, null, null, null, null, finished: false);
                            if (cl != null && caseList.Count > 0)
                            {
                                cModel.CaseStatusKey = 3;
                                cModel.CaseStatusValue = "确认交房完成";
                            }
                            else
                            {
                                cModel.CaseStatusKey = 5;
                                cModel.CaseStatusValue += "，但未完成过户";
                            }
                        }
                    }
                    //案件中止：
                    if (currentStatus == CaseEndStatus)
                        foreach (CastModel caseDto in caseModelList)
                        {
                            caseDto.CaseEndOperation = 1;
                        }
                }

            }
            catch (Exception ex)
            {
                this.Dispatcher.BeginInvoke(loadDataCompletedDelegate, new object[] { ex.ToString(), this.caseModelList, 0 });
            }
            this.Dispatcher.BeginInvoke(loadDataCompletedDelegate, new object[] { null, this.caseModelList, pageCount });

        }
        private void DataLoadCompleted(string errorMsg, List<CastModel> caseModelList, int pageCount)
        {
            loading.IsBusy = false;
            if (errorMsg != null)
            {
                MessageBox.Show("数据获取错误：" + errorMsg, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
                this.dataGrid.DataContext = null;
                pageControl.PageCount = pageCount;
                return;
            }
            this.dataGrid.DataContext = caseModelList;
            pageControl.PageCount = pageCount;
        }


        private void pageControl_PagerIndexChanged(int pageIndex)
        {
            StartLoadDataAsyn();
        }

        #region 页面操作按钮事件：

        /// <summary>
        /// 添加附件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AttachmentBtn_Click(object sender, RoutedEventArgs e)
        {
            //案件编号
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            var attachments = new UploadAttachment(caseId);
            PageHelper.PageNavigateHelper(this, attachments);
        }
        private void btnBooked_Click(object sender, RoutedEventArgs e)
        {
            string caseId = Convert.ToString(((Button)sender).CommandParameter);
            CastModel cm = getCastModelById(caseId);
            if (new BookedDialog(cm, true).ShowDialog() == true) StartLoadDataAsyn();
        }

        private void btnChangeBook_Click(object sender, RoutedEventArgs e)
        {
            string caseId = Convert.ToString(((Button)sender).CommandParameter);
            CastModel cm = getCastModelById(caseId);
            if (new BookedDialog(cm, false).ShowDialog() == true) StartLoadDataAsyn();
        }

        private void btnLanuchLoanApply_Click(object sender, RoutedEventArgs e)
        {
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            CastModel cm = getCastModelById(caseId);
            if (new LoanChangesDialog(cm).ShowDialog() == true) StartLoadDataAsyn();
        }

        private void btnChangeLoanApply_Click(object sender, RoutedEventArgs e)
        {
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            CastModel cm = getCastModelById(caseId);
            if (new LoanChangesDialog(cm).ShowDialog() == true) StartLoadDataAsyn();
        }

        private void btnLanuchRepayLoadApply_Click(object sender, RoutedEventArgs e)
        {
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            CastModel cm = getCastModelById(caseId);
            if (new LanuchRepayLoadApplyDialog(cm).ShowDialog() == true) StartLoadDataAsyn();
        }

        private void btnCancelConctact_Click(object sender, RoutedEventArgs e)
        {
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            CastModel cm = getCastModelById(caseId);
            if (new CancelConctactDialog(cm).ShowDialog() == true) StartLoadDataAsyn();
        }
        private void btnSendOrderAgain_Click(object sender, RoutedEventArgs e)
        {
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            CastModel cm = getCastModelById(caseId);
            if (new SendOrderAgainDialog(cm).ShowDialog() == true) StartLoadDataAsyn();
        }
        private void btnConfirmLeaveHouse_Click(object sender, RoutedEventArgs e)
        {
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            CastModel cm = getCastModelById(caseId);
            if (new ConfirmLeaveHouseDialog(cm).ShowDialog() == true) StartLoadDataAsyn();
        }

        private void btnCancelApply_Click(object sender, RoutedEventArgs e)
        {
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            CastModel cm = getCastModelById(caseId);
            if (new CancelConctactDialog(cm).ShowDialog() == true) StartLoadDataAsyn();
        }
        private void btnConfirmCaseEnd_Click(object sender, RoutedEventArgs e)
        {
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            CastModel cm = getCastModelById(caseId);
            if (new CancelConctactDialog(cm).ShowDialog() == true) StartLoadDataAsyn();
        }
        private void btnCaseEnd_Click(object sender, RoutedEventArgs e)
        {
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            CastModel cm = getCastModelById(caseId);
            if (new CancelConctactDialog(cm).ShowDialog() == true) StartLoadDataAsyn();
        }
        private void btnChangeRepayLoadApply_Click(object sender, RoutedEventArgs e)
        {
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            CastModel cm = getCastModelById(caseId);
            if (new LanuchRepayLoadApplyDialog(cm).ShowDialog() == true) StartLoadDataAsyn();
        }
        private void btnCancelEnd_Click(object sender, RoutedEventArgs e)
        {
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            CastModel cm = getCastModelById(caseId);
            if (new CancelConctactDialog(cm, 1).ShowDialog() == true) StartLoadDataAsyn();
        }
        private void BtnGetNewCertificate_OnClick(object sender, RoutedEventArgs e)
        {
            string caseId = Convert.ToString(((Button)sender).CommandParameter);
            CastModel cm = getCastModelById(caseId);
            if (new CancelConctactDialog(cm).ShowDialog() == true) StartLoadDataAsyn();
        }

        #endregion

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            //初始化分页插件
            this.pageControl.PageNumChanged = this.pageControl_PagerIndexChanged;
            //默认选中第一个
            currentSelectedSwitchButton = (Button)switchPanel.Children[currentSeletedBtnIndex + 1];
            SwitchButtons_Click(currentSelectedSwitchButton, null);
        }
 

        private void SwitchButtons_Click(object sender, RoutedEventArgs e)
        {

            if (currentSelectedSwitchButton != null && sender != null)
            {
                currentSelectedSwitchButton.Style = (Style)FindResource("ShhtsBtGray");
                currentSelectedSwitchButton = (Button)sender;
                currentSelectedSwitchButton.Style = (Style)FindResource("ShhtsBtBlue");
                int tag = int.Parse(currentSelectedSwitchButton.Tag.ToString());
                currentStatus = statusList[tag];
                hostNameSearch = hostOperationNameStatusList2[tag];
                currenthostOperationNameStatus = hostOperationNameStatusList[tag];
                currenthostOperationNameStatus2 = hostOperationNameStatusList2[tag];
            }

            if (!CheckPermission(currentSelectedSwitchButton.Tag.ToString()))
            {
                MessageBox.Show("您不具备此操作权限.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            if (sender != null)//不是搜索按钮
            {
                //获取案件状态
                List<EnumHelper> list = EnumHelper.InitCaseStatusToCombobox(currentStatus == CheckTaxApplyStatus ? currentStatus ^ CaseStatus.JY11 : currentStatus);
                if (currentStatus == LeaveHouseConfirmStatus) list = new List<EnumHelper>() { new EnumHelper { DisplayMember = "交房确认", ValueMember = 0 } };
                caseStatusCb.ItemsSource = list;
                caseStatusCb.DisplayMemberPath = "DisplayMember";
                caseStatusCb.SelectedValuePath = "ValueMember";
                caseStatusCb.SelectedValue = 0;
                int tag = int.Parse(currentSelectedSwitchButton.Tag.ToString());
                //   卖方/买方
                if (tag == 1)
                {
                    column4.Header = "卖方";
                    Binding binding = new Binding();
                    binding.Path = new PropertyPath("SellerName2");
                    column4.Binding = binding;
                }
                else
                {
                    column4.Header = "买方";
                    Binding binding = new Binding();
                    binding.Path = new PropertyPath("BuyerName2");
                    column4.Binding = binding;
                }
                //  受理人/发起人/核案
                if (tag == 4 || tag == 3)
                {
                    column9.Header = "发起人";
                }
                else if (tag == 5)
                {
                    column9.Header = "核案";
                }
                else
                {
                    column9.Header = "受理人";
                }

                if (tag == 3)
                {
                    column6.Visibility = Visibility.Collapsed;
                }
            }
            pageControl.PageIndex = 1;
        }

        #region 权限

        private bool CheckPermission(string tag)
        {
            return PermissionProxyService.GetInstance().Authorize(GetButtonPermissionNo(tag));
        }

        private string GetButtonPermissionNo(string tag)
        {
            if (string.IsNullOrEmpty(tag))
                throw new Exception("权限标签未设置");
            switch (tag)
            {
                case "0": return "10014";
                case "1": return "10015";
                case "2": return "10016";
                case "3": return "10017";
                case "4": return "10018";
                case "5": return "10019";
                case "7": return "10020";
                default: throw new Exception("权限标签未设置");
            }
        }

        #endregion

        private void BtnCaseManage_OnClick(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, new CaseManagementListPage());
        }
        public CastModel getCastModelById(string caseId)
        {
            foreach (CastModel cm in caseModelList)
            {
                if (cm.CaseId == caseId) return cm;
            }
            return null;
        }

        /// <summary>
        /// 结案业务
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [Permission("10020")]
        private void BtnCloseCase_OnClick(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, new CaseCloseListPage());
        }

        /// <summary>
        /// 案件明细
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RequestNavigate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var link = e.Source as Hyperlink;
                if (null != link)
                {
                    var caseId = Convert.ToString(link.NavigateUri);
                    var caseInfo = new CaseDetailInfo(caseId);
                    PageHelper.PageNavigateHelper(this, caseInfo);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("显示明细发生错误", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            e.Handled = true;
        }
    }
}
