﻿using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Host
{
    /// <summary>
    /// CancelConctactDialog.xaml 的交互逻辑
    /// </summary>
    public partial class CancelConctactDialog : Window
    {
        CastModel cm = null;
        int LeftOrRightButtonClicked = 0;
        public CancelConctactDialog()
        {
            InitializeComponent();
        }

        public CancelConctactDialog(CastModel cm) : this()
        {
            this.cm = cm;
        }
        public CancelConctactDialog(CastModel cm, int whichButtonClicked):this()
        {
            this.cm = cm;
            this.LeftOrRightButtonClicked = whichButtonClicked;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (cm.CaseStatus == CaseStatus.DK2 || cm.CaseStatus == CaseStatus.DK9)
            {
                this.Title ="取消贷款申请";
            }
            else if(cm.CaseStatus==CaseStatus.ZB10||cm.CaseStatus==CaseStatus.HD1)
            {
                this.Title ="取消还贷申请";
            }
            else if (cm.CaseStatus == CaseStatus.ZB5)
            {
                this.Title = "确认取消签约";
            }
            else if (cm.CaseStatus == CaseStatus.JY11A1)
            {
                this.Title = "确认领新产证";
            }
            //最后判断是不是案件中止操作
            if (cm.CaseEndOperation == 1)
            {
                this.Title = "中止案件";
            }
            if (cm.CaseStatus == CaseStatus.ZB6)//确认案件中止
            {
                if (LeftOrRightButtonClicked == 0) this.Title = "确认案件中止";
                else this.Title = "取消中止";
            }
            


            LableLine1.Content = "案件编号：" + cm.CaseId + "      买家：" + cm.BuyerName2;
            if (cm.CaseStatus == CaseStatus.ZB10 || cm.CaseStatus == CaseStatus.HD1)
            {
                LableLine1.Content = "案件编号：" + cm.CaseId + "      卖方：" + cm.SellerName2;
            }
            LableLine2.Content = "物业地址：" + cm.TenementAddress;
        }
        //取消
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }
        //确定
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            CaseStatus nextStatus = CaseStatus.Unkown;
            if (cm.CaseEndOperation != 1)
            {
                if (cm.CaseStatus == CaseStatus.ZB10 || cm.CaseStatus == CaseStatus.HD1) 
                    nextStatus = CaseStatus.ZB12;
                if (cm.CaseStatus == CaseStatus.DK2 || cm.CaseStatus == CaseStatus.DK9)
                {
                    nextStatus = CaseStatus.ZB11;
                }
                if (cm.CaseStatus == CaseStatus.ZB5) nextStatus = CaseStatus.ZB2;
                if (cm.CaseStatus == CaseStatus.JY11A1) nextStatus = CaseStatus.JY12;
            }
            else
            {
                if (cm.CaseStatus == CaseStatus.ZB6)
                    if (LeftOrRightButtonClicked == 0) nextStatus = CaseStatus.ZB7;
                    else nextStatus = CaseStatus.JY9;
                else nextStatus = CaseStatus.ZB7;
            }
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("remarks", remarks.Text);
            var result = CaseProxyService.GetInstanse().UpdateCaseStatus(cm.CaseId, nextStatus, LoginHelper.CurrentUser.SysNo, dic);
            if (result.Success)
            {
                MessageBox.Show("操作成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                this.DialogResult = true;
            }
            else
            {
                MessageBox.Show("操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            this.Hide();
        }


    }
}
