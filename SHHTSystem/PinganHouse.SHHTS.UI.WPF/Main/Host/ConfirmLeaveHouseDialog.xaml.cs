﻿using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Host
{
    /// <summary>
    /// ConfirmLeaveHouseDialog.xaml 的交互逻辑
    /// </summary>
    public partial class ConfirmLeaveHouseDialog : Window
    {

        CastModel cm = null;
        public ConfirmLeaveHouseDialog()
        {
            InitializeComponent();
        }


        public ConfirmLeaveHouseDialog(CastModel cm)
            : this()
        {
            this.cm = cm;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LableLine1.Text = "案件编号：" + cm.CaseId + "      买方：" + cm.BuyerName2;
            LableLine2.Text = "物业地址：" + cm.TenementAddress;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("AgentComment", getCommentFromRadioGroupPanel(RadioGroupPanel1));
            dic.Add("TradeComment", getCommentFromRadioGroupPanel(RadioGroupPanel2));
            dic.Add("LoadComment", getCommentFromRadioGroupPanel(RadioGroupPanel3));
            dic.Add("TotalComment", getCommentFromRadioGroupPanel(RadioGroupPanel5));
            var result = CaseProxyService.GetInstanse().UpdateCaseStatus(cm.CaseId, CaseStatus.ZB8, LoginHelper.CurrentUser.SysNo, dic);
            if (result.Success)
            {
                MessageBox.Show("操作成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                this.DialogResult = true;
            }
            else
            {
                MessageBox.Show("操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            this.Hide();
        }

        private string getCommentFromRadioGroupPanel(StackPanel panel)
        {
            foreach (var r in panel.Children)
            {
                if (r is RadioButton)
                {
                    RadioButton rb = (RadioButton)r;
                    return rb.Content.ToString();
                }
            }
            return "";
        }

    }
}
