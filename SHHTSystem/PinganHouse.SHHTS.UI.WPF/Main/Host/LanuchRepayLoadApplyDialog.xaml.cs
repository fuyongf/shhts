﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Host
{
    /// <summary>
    /// LanuchRepayLoadApplyDialog.xaml 的交互逻辑
    /// </summary>
    public partial class LanuchRepayLoadApplyDialog : Window
    {

        BackgroundWorker getOtherDataBackWorker = new BackgroundWorker();
        CaseEvent caseEvent = null;

        private CastModel cm = null;
        public LanuchRepayLoadApplyDialog()
        {
            InitializeComponent();
            getOtherDataBackWorker.WorkerSupportsCancellation = true;
            getOtherDataBackWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.backWorkerCompleted);
            getOtherDataBackWorker.DoWork += new DoWorkEventHandler(this.backWorkerDo);
        }
        //默认还贷申请
        public LanuchRepayLoadApplyDialog(CastModel cm)
            : this()
        {
            this.cm = cm;
        }
        private void backWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!e.Cancelled && caseEvent != null)
            {
                if (caseEvent.OtherDatas.ContainsKey("agent"))
                {
                    label1.Content = "受理人从" + caseEvent.OtherDatas["agent"] + "变更为：";
                }
            }
        }
        private void backWorkerDo(object sender, DoWorkEventArgs e)
        {
            var caseEvents = CaseProxyService.GetInstanse().GetCaseEvents(cm.CaseId, cm.CaseStatus, null);
            if (caseEvents.Count > 0) caseEvent = caseEvents[0];
        }

        //取消
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }
        //确定
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(comboBox.Text))
            {
                MessageBox.Show("请选择受理方", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (comboBox.Text == "请选择")
            {
                MessageBox.Show("请选择受理方", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("agent", comboBox.Text);
            dic.Add("remarks", remarks.Text);

            OperationResult result = null;
            if (cm.CaseStatus == CaseStatus.ZB10)
            {
                result = CaseProxyService.GetInstanse().UpdateCaseStatus(cm.CaseId, CaseStatus.ZB3, LoginHelper.CurrentUser.SysNo, dic);
            }
            else
            {
                result = CaseProxyService.GetInstanse().UpdateCaseEvent(caseEvent.SysNo, dic, LoginHelper.CurrentUser.SysNo);
            }
            
            if (result.Success)
            {
                MessageBox.Show("操作成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                this.DialogResult = true;
            }
            else
            {
                MessageBox.Show("操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            this.Hide();


        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
            LabelLine1.Content = "案件编号：" + cm.CaseId + "      卖方：" + cm.SellerName2;
            LabelLine2.Content = "物业地址：" + cm.TenementAddress;
            List<String> comboxList = new List<string> { "请选择","平安银行", "专员", "第三方", "客户" };
            comboBox.ItemsSource = comboxList;
            comboBox.SelectedIndex = 0;
            if (cm.CaseStatus == CaseStatus.ZB10)
            {
                this.Title = "发起还贷申请";
            }
            else
            {
                this.Title = "变更还贷申请";
                getOtherDataBackWorker.RunWorkerAsync();
            }
           
        }

    }
}
