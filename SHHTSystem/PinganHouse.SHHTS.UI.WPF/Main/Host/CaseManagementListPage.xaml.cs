﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Main.PreCheck;
using PinganHouse.SHHTS.UI.WPF.Model;
using System.Windows.Documents;
using PinganHouse.SHHTS.UI.WPF.Main.Dialog;
using PinganHouse.SHHTS.UI.WPF.Main.Nuclear;

namespace PinganHouse.SHHTS.UI.WPF.Main.Host
{
    /// <summary>
    /// CaseManagementListPage.xaml 的交互逻辑
    /// </summary>
    public partial class CaseManagementListPage : Page
    {

        private IList<CaseManagementModel> data;
        private readonly int PageSize = 5;
        private enum LastUpdatedTime
        {
            [Description("1天内")]
            OneDay = 1,
            [Description("2天内")]
            TwoDay = 2,
            [Description("3天内")]
            ThreeDay = 3,
            [Description("1周内")]
            OneWeek = 7
        }


        public static IList<CaseStatus> categories = null;


        public CaseManagementListPage()
        {
            InitializeComponent();
            this.pageControl.PageNumChanged = this.pageIndexChanged;
            categories = new List<CaseStatus>();

            categories.Add(CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.HA3
                | CaseStatus.HA4 | CaseStatus.HA5 | CaseStatus.HA6
                | CaseStatus.ZB2 | CaseStatus.ZB5 | CaseStatus.ZB7);

            categories.Add(CaseStatus.ZB3 | CaseStatus.HD1 | CaseStatus.HD2 | CaseStatus.HD3
            | CaseStatus.HD4 | CaseStatus.HD5 | CaseStatus.HD6 | CaseStatus.ZB12 | CaseStatus.ZB7);

            categories.Add(CaseStatus.ZB1 |
                                    CaseStatus.JY1 |
                                    CaseStatus.JY2 |
                                    CaseStatus.JY3 |
                                    CaseStatus.JY4 |
                                    CaseStatus.JY6 |
                                    CaseStatus.JY7 |
                                    CaseStatus.JY8 |
                                    CaseStatus.JY9 |
                                    CaseStatus.JY10 |
                                    CaseStatus.JY11 |
                                    CaseStatus.JY12 |
                                    CaseStatus.JY11A1 |
                                    CaseStatus.ZB6 |
                                    CaseStatus.ZB7);

            categories.Add(CaseStatus.DK1 | CaseStatus.DK2 | CaseStatus.DK3 | CaseStatus.DK4 | CaseStatus.ZB11
            | CaseStatus.DK5 | CaseStatus.DK6 | CaseStatus.DK7 | CaseStatus.DK8 | CaseStatus.DK9 | CaseStatus.DK10
            | CaseStatus.JY5 | CaseStatus.ZB7);

        }

        private void CaseManagementListPage_OnLoaded(object sender, RoutedEventArgs e)
        {

            List<EnumHelper> list = EnumHelper.EnumToEnumHelperList(typeof(ProccessType));
            EnumHelper eh = new EnumHelper();
            eh.DisplayMember = "全部进度";
            eh.ValueMember = -1;
            list.Insert(0, eh);
            processComboBox.ItemsSource = list;
            processComboBox.DisplayMemberPath = "DisplayMember";
            processComboBox.SelectedValuePath = "ValueMember";
            processComboBox.SelectedIndex = 0;

            List<EnumHelper> list1 = EnumHelper.EnumToEnumHelperList(typeof(LastUpdatedTime));
            EnumHelper eh1 = new EnumHelper();
            eh1.DisplayMember = "全部时间";
            eh1.ValueMember = -1;
            list1.Insert(0, eh1);
            lastUpdatedTimeComboBox.ItemsSource = list1;
            lastUpdatedTimeComboBox.DisplayMemberPath = "DisplayMember";
            lastUpdatedTimeComboBox.SelectedValuePath = "ValueMember";
            lastUpdatedTimeComboBox.SelectedIndex = 0;

            this.pageControl.PageIndex = 1;
        }

        private void SearchBtn_OnClick(object sender, RoutedEventArgs e)
        {
            this.pageControl.PageIndex = 1;
        }

        private void pageIndexChanged(int pageIndex)
        {
            startLoadDataAnsy(pageIndex);
        }

        private void startLoadDataAnsy(int pageIndex)
        {
            ProccessType? type = null;
            int? day = null;
            string stuff = null;
            int temp = -1;
            int.TryParse(processComboBox.SelectedValue + "", out temp);
            if (temp != -1)
                type = (ProccessType)temp;
            temp = -1;
            int.TryParse(lastUpdatedTimeComboBox.SelectedValue + "", out temp);
            if (temp != -1) day = temp;
            if (!string.IsNullOrEmpty(lastFollowStuffTextBox.Text)) stuff = lastFollowStuffTextBox.Text;

            Thread thread = new Thread(delegate()
            {
                loadData(pageIndex, type, day, stuff);
            });
            thread.IsBackground = true;
            PageHelper.PageActiveLoadingControl(this.loading, "正在加载数据,请稍后...");
            thread.Start();

        }

        private void loadData(int pageIndex, ProccessType? type, int? day, string stuff)
        {
            string errorMsg = null;
            int pageCount = 0;
            data = new List<CaseManagementModel>();
            try
            {
                var cases = CaseProxyService.GetInstanse()
                    .GetCaseTrailPaged(pageIndex, PageSize, out pageCount, ConfigHelper.GetCurrentReceptionCenter(),
                        CaseStatus.HA2, categories, type, day, stuff, null);
                if (cases != null)
                    foreach (var v in cases)
                    {
                        data.Add(new CaseManagementModel(v));
                    }
            }
            catch (Exception ex)
            {
                errorMsg = ex.ToString();
            }
            //主线程更新数据
            this.Dispatcher.Invoke(new Action(delegate()
                {
                    loading.IsBusy = false;
                    if (errorMsg != null)
                    {
                        MessageBox.Show("数据获取错误：" + errorMsg, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
                        this.dataGrid.DataContext = null;
                        this.pageControl.PageCount = 0;
                    }
                    else
                    {
                        int totalCount = pageCount;
                        pageCount = totalCount / PageSize;
                        if (totalCount % PageSize != 0) pageCount++;
                        this.dataGrid.DataContext = this.data;
                        this.pageControl.PageCount = pageCount;
                    }
                }));
        }

        public class CaseManagementModel
        {
            private string[] strs = new string[] { "核案签约：", "卖方还贷：", "审税过户：", "贷款办理：" };
            private string[] strs1 = new string[] { "核案签约：--", "卖方还贷：无抵押", "审税过户：等待预约审税", "贷款办理：无贷款" };

            public CaseTrailPaged Case { set; get; }
            public CaseManagementModel(CaseTrailPaged ctp)
            {
                this.Case = ctp;
            }

            public string BuyerName
            {
                get
                {
                    if (Case != null && Case.BuyerNameDic != null && Case.BuyerNameDic.Count > 0)
                    {
                        return Case.BuyerName.Split(',')[0];
                    }
                    return "--";
                }
            }

            public string Status
            {
                get
                {
                    StringBuilder sb = new StringBuilder();
                    //bool first = true;
                    int index = -1;
                    bool caseEnd = false;
                    bool showHuanDai = false;
                    foreach (CaseStatus cs in CaseManagementListPage.categories)
                    {
                        index++;

                        //if (first)
                        //    first = false;
                        //else
                        //sb.Append("\n");
                        if (Case.LastStatusDic.ContainsKey(cs))
                        {
                            if (Case.LastStatusDic[cs] == CaseStatus.ZB2 || Case.LastStatusDic[cs] == CaseStatus.ZB5 ||
                                Case.LastStatusDic[cs] == CaseStatus.YJ1 || Case.LastStatusDic[cs] == CaseStatus.HA1)
                                return "\n" + strs[index] + EnumHelper.GetEnumDesc(Case.LastStatusDic[cs]);

                            if (Case.LastStatusDic[cs] == CaseStatus.ZB7)
                            {
                                caseEnd = true;
                                break;
                            }

                            if (Case.LastStatusDic[cs] == CaseStatus.HA5)
                            {
                                showHuanDai = true;
                            }
                            if (index != 1)
                            {
                                sb.Append("\n" + strs[index] + EnumHelper.GetEnumDesc(Case.LastStatusDic[cs]));    
                            }
                            else if(showHuanDai)
                            {
                                sb.Append("\n" + strs[1] + EnumHelper.GetEnumDesc(Case.LastStatusDic[cs]));    
                            }
                            
                        }
                        else
                        {
                            if (index != 1)
                            {
                                sb.Append("\n" + strs1[index]);
                            }
                            else if (showHuanDai)
                            {
                                sb.Append("\n" + strs1[1]);
                            }
                        }
                    }
                    if (caseEnd)
                        return "案件已终止";
                    return sb.ToString().Substring(1);
                }

            }

        }


        #region  页面切换
        private void SwitchButtons_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, new HostListPage(int.Parse(((Button)sender).Tag.ToString())));
        }

        private void BtnCaseManage_OnClick(object sender, RoutedEventArgs e)
        {
            // do nothing
        }
        #endregion

        private void BtnWriteFollow_OnClick(object sender, RoutedEventArgs e)
        {
            string caseId = Convert.ToString(((Button)sender).CommandParameter);
            if (new FollowUpDialog(getModelByID(caseId)).ShowDialog() == true) startLoadDataAnsy(this.pageControl.PageIndex);
        }

        private CaseManagementModel getModelByID(string caseId)
        {
            foreach (var v in data)
            {
                if (v.Case.CaseId == caseId) return v;
            }
            return null;
        }


        private void BtnOpenReceipt_OnClick(object sender, RoutedEventArgs e)
        {
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            var attachments = new UploadAttachment(caseId);
            PageHelper.PageNavigateHelper(this, attachments);
        }

        /// <summary>
        /// 结案
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCloseCase_OnClick(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, new CaseCloseListPage());
        }

        /// <summary>
        /// 案件明细
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RequestNavigate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var link = e.Source as Hyperlink;
                if (null != link)
                {
                    var caseId = Convert.ToString(link.NavigateUri);
                    var caseInfo = new CaseDetailInfo(caseId);
                    PageHelper.PageNavigateHelper(this, caseInfo);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("显示明细发生错误", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            e.Handled = true;
        }
    }
}
