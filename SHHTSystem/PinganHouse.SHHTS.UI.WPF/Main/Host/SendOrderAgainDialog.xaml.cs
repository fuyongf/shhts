﻿using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Host
{
    /// <summary>
    /// SendOrderAgainDialog.xaml 的交互逻辑
    /// </summary>
    public partial class SendOrderAgainDialog : Window
    {        CastModel cm = null;
        public SendOrderAgainDialog()
        {
            InitializeComponent();
        }

        public SendOrderAgainDialog(CastModel cm)
            : this()
        {
            this.cm = cm;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LableLine1.Content = "案件编号：" + cm.CaseId + "      买方：" + cm.BuyerName2;
            LableLine2.Content = "物业地址：" + cm.TenementAddress;
        }
        //取消
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }
        //确定
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            CaseStatus nextStatus = CaseStatus.Unkown;
            if (cm.CaseStatus==CaseStatus.JY1)
            {
                nextStatus = CaseStatus.ZB1;
            }
            else if (cm.CaseStatus == CaseStatus.ZB5)
            {
                nextStatus = CaseStatus.YJ1;
            }
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("remarks", remarks.Text);
            var result = CaseProxyService.GetInstanse().UpdateCaseStatus(cm.CaseId, nextStatus, LoginHelper.CurrentUser.SysNo, dic);
            if (result.Success)
            {
                MessageBox.Show("操作成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                this.DialogResult = true;
            }
            else
            {
                MessageBox.Show("操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            this.Hide();
        }


    }
}
