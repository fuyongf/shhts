﻿using PinganHouse.SHHTS.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Host
{
    /// <summary>
    /// ChangeBookTimeDialog.xaml 的交互逻辑
    /// </summary>
    public partial class ChangeBookTimeDialog : Window
    {
        CaseDto caseDto = null;
        public ChangeBookTimeDialog()
        {
            InitializeComponent();
        }
        public ChangeBookTimeDialog(CaseDto caseDto):this()
        {
            this.caseDto = caseDto;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.Content.ToString() == "取消")
            {
                this.Hide();
            }
            else if (btn.Content.ToString() == "确认")
            {
                //dosomething


                this.Hide();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (caseDto != null)
            {
                LabelLine1.Content = "案件编号："+caseDto.CaseId;
                LabelLine2.Content = "物业地址：" + caseDto.TenementAddress;
                LabelContractTime.Content = caseDto.CreateDate.ToString();
 
            }
        }
    }
}
