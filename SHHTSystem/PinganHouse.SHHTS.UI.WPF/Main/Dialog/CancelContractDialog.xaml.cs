﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Dialog
{
    /// <summary>
    /// CancelContractDialog.xaml 的交互逻辑
    /// </summary>
    public partial class CancelContractDialog : Window
    {
        /// <summary>
        /// 取消签约原因
        /// </summary>
        public string Reason;

        public CancelContractDialog(string caseId)
        {
            InitializeComponent();
        }

        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        /// <summary>
        /// 确定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            Reason = txtReason.Text.Trim();
            if (string.IsNullOrEmpty(Reason)) {
                MessageBox.Show("请填写取消原因.","系统提示",MessageBoxButton.OK,MessageBoxImage.Warning);
                return;
            }
            else
            {
                this.DialogResult = true;
            }     
        }
    }
}
