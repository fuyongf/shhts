﻿using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Dialog
{
    /// <summary>
    /// BanksDialog.xaml 的交互逻辑
    /// </summary>
    public partial class BanksDialog : Window
    {
        public delegate void SelectBankHandle(string bankCode,string bankName,string tag);
        //确定预约事件
        public event SelectBankHandle SelectBankEvent;

        public delegate void ClearBankHandle(string tag);
        //清空银行信息事件
        public event ClearBankHandle ClearBankEvent;

        /// <summary>
        /// 选择标志
        /// </summary>
        private string m_Tag;

        List<Bank> m_Banks=new List<Bank>();

        public BanksDialog(string tag)
        {
            InitializeComponent();
            m_Tag = tag;
        }
        
        /// <summary>
        /// 页面初始化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                PageHelper.PageActiveLoadingControl(this.ctrlLoading,"正在获取,请稍候...");
                //获取银行
                GetBank();
            }
            catch (Exception)
            {
                ctrlLoading.IsBusy = false;
                MessageBox.Show("加载银行信息发生系统异常.","系统异常",MessageBoxButton.OK,MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 获取银行
        /// </summary>
        private async void GetBank()
        {
            var banks = await GetBanks();
            foreach (var item in banks)
            {
                var bank = new Bank();
                bank.BankCode = Convert.ToString(((System.Collections.DictionaryEntry)(item)).Key);
                bank.BankName = Convert.ToString(((System.Collections.DictionaryEntry)(item)).Value);
                m_Banks.Add(bank);
            }
            dataGrid.DataContext = m_Banks;
            ctrlLoading.IsBusy = false;
        }

        /// <summary>
        /// 获取银行数据
        /// </summary>
        /// <returns></returns>
        private Task<IDictionary> GetBanks()
        {
            return Task.Run(() =>
            {
                var banks = ConfigServiceProxy.GetBanks();
                return banks;
            });
            
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ClearBankEvent != null)
                {
                    ClearBankEvent(m_Tag);
                    this.DialogResult = true;
                }
                
            }
            catch (Exception)
            {
                //MessageBox.Show("选择银行操作发生系统异常", "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 确定选择银行
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var item = dataGrid.SelectedItem as Bank;
                if (item == null)
                {
                    MessageBox.Show("请选择银行","系统提示",MessageBoxButton.OK,MessageBoxImage.Warning);
                }
                else
                {
                    if (SelectBankEvent != null)
                    {
                        SelectBankEvent(item.BankCode,item.BankName,m_Tag);
                        this.DialogResult = true;
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("选择银行操作发生系统异常", "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 取消选择
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancle_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        /// <summary>
        /// 双击选中
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var item = dataGrid.SelectedItem as Bank;
                if (item == null)
                {
                    return;
                }
                if (SelectBankEvent != null)
                {
                    SelectBankEvent(item.BankCode, item.BankName, m_Tag);
                    this.DialogResult = true;
                }

            }
            catch (Exception)
            {
                MessageBox.Show("选择银行操作发生系统异常", "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 搜索银行
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Search_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var new_Banks = from bank in m_Banks
                         where CheckName(bank)
                         select bank;
                dataGrid.DataContext = null;
                dataGrid.DataContext = new_Banks;
            }
            catch (Exception)
            {

            }
        }

        private bool CheckName(Bank bank)
        {
            var key = txt_BankName.Text.Trim();
            if (string.IsNullOrEmpty(key)) return true;
            if (bank.BankName.Contains(key))
            {
                return true;
            }
            return false;
        }
        private void txt_BankName_TextChanged(object sender, TextChangedEventArgs e)
        {
            Search_Click(sender, e);
        }

        private void dataGrid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                btnOk_Click(sender, e);
            }
        }
    }

    public class Bank
    {
        public string BankCode { get; set; }

        /// <summary>
        /// 开户银行
        /// </summary>
        public string BankName { get; set; }

        /// <summary>
        /// 支行名称
        /// </summary>
        public string SubBankName { get; set; }
    }
}
