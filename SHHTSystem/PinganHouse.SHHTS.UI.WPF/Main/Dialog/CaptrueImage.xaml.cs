﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;

namespace PinganHouse.SHHTS.UI.WPF.Main.Dialog
{
    /// <summary>
    /// CaptrueImage.xaml 的交互逻辑
    /// </summary>
    public partial class CaptrueImage : Window
    {
        public delegate void AllUploadedHandle(Dictionary<string, Stream> files, string filePath, CaptrueImage captrueImage, CameraInterfaceInvoke cameraInvoke, UInt32 mDeviceHandle);
        public event AllUploadedHandle AllUploadedEvent;


        #region 公共变量
        /// <summary>
        /// 图片存储途径
        /// </summary>
        StringBuilder _filePath;
        //打开弹框时的途径
        String _pathImage;
        CameraInterfaceInvoke _cameraInvoke;
        private UInt32 _mDeviceHandle = 0;
        private Int32 _mPreviewHWnd;
        /// <summary>
        /// 案件编号
        /// </summary>
        private readonly string _id;
        /// <summary>
        /// 附件类型
        /// </summary>
        private readonly AttachmentType _type;
        /// <summary>
        /// 照片集合
        /// </summary>
        List<UploadFile> _captureImgs = new List<UploadFile>();
        /// <summary>
        /// 选择的照片集合
        /// </summary>
        List<Stream> fileStreamList = new List<Stream>();
        /// <summary>
        /// 选择的照片名字集合
        /// </summary>
        List<string> fileNameList=new List<string>();

        ///// <summary>
        ///// 图片路径
        ///// </summary>
        private static string _path;
        static List<string> imageIds;

        /// <summary>
        /// 图片类型
        /// </summary>
        private AttachmentType _attachmentType = AttachmentType.未知;

        private const int GWL_STYLE = -16;
        private const int WS_SYSMENU = 0x80000;
        [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        #endregion

        public CaptrueImage(AttachmentType type)
        {
            InitializeComponent();
            _attachmentType = type;
            FindGaopaiyi();
        }
           public void FindGaopaiyi()
        {

               try
               {
                   //再重新连接高拍仪还会报错
                   _cameraInvoke = new CameraInterfaceInvoke("./CameraInterface.dll");
                   _mDeviceHandle = _cameraInvoke.func_InitDevice(0);
                   //string errorMessage = Marshal.PtrToStringAnsi(_cameraInvoke.func_getLastErrorMessage());
                   if (_mDeviceHandle == 0xFFFFFFFF || _mDeviceHandle == 0)
                   {
                       MessageBox.Show("高拍仪未连接或者初始化摄像头失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                       return;
                   }
                   else
                   {
                       this.Show();
                       CaptrueWindow.WindowState = WindowState.Maximized;
                   }
               }
               catch (Exception e)
               {
                   MessageBox.Show(e.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
               }

        }
 
        #region 创建文件夹
        /// <summary>
        /// 创建文件夹
        /// </summary>
        private void CreateFile()
        {
            _filePath = new StringBuilder();
            
            _filePath.AppendFormat(".\\CaptrueImages");
            if (!Directory.Exists(_filePath.ToString()))
            {
                Directory.CreateDirectory(_filePath.ToString());
            }
            //创建文件夹
            _filePath.AppendFormat("\\{0}", _attachmentType);
            if (!Directory.Exists(_filePath.ToString()))
            {
                Directory.CreateDirectory(_filePath.ToString());
            }
            _pathImage = _filePath.ToString();
        }
        #endregion

        #region 拍照
        /// <summary>
        /// 拍照
        /// </summary>     
        private void captureBtn_Click(object sender, RoutedEventArgs e)
        {
            //图片途径     
            //string imagePath = DateTime.Now.Ticks + ".jpg";
            _path = _pathImage + "\\" + DateTime.Now.Ticks + ".jpg";
           
            //存储图片，判断是否成功
            _cameraInvoke.func_captureImage(_mDeviceHandle, _path);
            _cameraInvoke.func_startPreview(_mDeviceHandle);
            //预览页面  
            Double Stride = (ImgSp.Orientation == Orientation.Horizontal) ? ImgSp.Height : ImgSp.Width;
            // 设置边框                             
            Border border = new Border();
            border.Margin = new Thickness(2);             // 边框外边距  
            border.BorderThickness = new Thickness(1);   // 边框厚度  
            border.BorderBrush = new SolidColorBrush(Colors.Red);  // 边框颜色            
            border.Width = 100;
            border.Height = 100;
            // 设置图像控件  
            Image image = new Image();
            image.Stretch = Stretch.Uniform;
            var uri = string.Format("{0}{1}", AppDomain.CurrentDomain.BaseDirectory, _path.Substring(2));
            if (File.Exists(uri))
            {
                var imageSource = new BitmapImage();
                var fileStream = new FileStream(uri, FileMode.Open, FileAccess.Read);
                imageSource.BeginInit();
                imageSource.StreamSource = fileStream;
                imageSource.EndInit();
                image.Source = imageSource;
                image.Tag = uri;
                // 图片点击事件              
                image.MouseDown += image_MouseDown;
                _captureImgs.Add(new UploadFile
                {
                    FileName = uri,
                    FileStream = fileStream,
                    Selected = true
                });

                // 设置控件布局  
                border.Child = image;
                ImgSp.Children.Add(border);
                uploadBtn.IsEnabled = true;
            }
        }
  
        /// <summary>
        /// 图片点击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Image imgSelect = sender as Image;
            if (imgSelect != null)
            {
                string imgName = imgSelect.Tag.ToString();
                Border imgParent = (Border)VisualTreeHelper.GetParent(imgSelect);
                var file = _captureImgs.Where(f => f.FileName == imgName).SingleOrDefault();
                if (file != null)
                {
                    if (file.FileStream.Position != 0)
                        file.FileStream.Seek(0, SeekOrigin.Begin);
                    file.Selected = !file.Selected;
                    if (file.Selected)
                    {
                        imgParent.BorderBrush = new SolidColorBrush(Colors.Red);
                    }
                    else
                    {
                        imgParent.BorderBrush = new SolidColorBrush(Colors.Gainsboro);
                    }
                }               
            }
        }

        #endregion

        #region 上传事件
        /// <summary>
        /// 上传
        /// </summary>    
        private  void uploadBtn_Click(object sender, RoutedEventArgs e)
        {
            var fileList = new Dictionary<string, Stream>();
            foreach (var captureImg in _captureImgs)
            {
                try
                {
                    if (captureImg.Selected)
                    {
                        fileList.Add(captureImg.FileName, captureImg.FileStream);
                    }             
                    else
                    {
                        captureImg.FileStream.Dispose();
                        File.Delete(captureImg.FileName);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                    continue;
                }              
            }
            if (AllUploadedEvent != null)
            {
                string path = string.Format("{0}{1}", AppDomain.CurrentDomain.BaseDirectory, _pathImage.Substring(2));
                AllUploadedEvent(fileList, path, this, _cameraInvoke, _mDeviceHandle);             
            }              
        }
  
        #endregion

        #region 关闭事件
         
        /// <summary>
        /// 关闭
        /// </summary>     
        private void colseBtn_Click(object sender, RoutedEventArgs e)
        {
           
            foreach (var captureImg in _captureImgs)
            {
                captureImg.FileStream.Dispose();
                File.Delete(captureImg.FileName);
            }
            if (_mDeviceHandle != 0)
            {
                _cameraInvoke.func_CloseDevice(_mDeviceHandle);
            }
            this.Close();
        }

        #endregion

        #region 窗体的打开、关闭
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {         
            var hwnd = new System.Windows.Interop.WindowInteropHelper(this).Handle;
            SetWindowLong(hwnd, GWL_STYLE, GetWindowLong(hwnd, GWL_STYLE) & ~WS_SYSMENU);

            CreateFile();
            if (_cameraInvoke.strErrorMessage != string.Empty)
            {
                MessageBox.Show(_cameraInvoke.strErrorMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            this.deviceNameCB.Items.Clear();
            int i = 0;
            while (true)
            {
                string tmp1;
                string tmp2;
                int iWidth = -1;
                int iHeight = -1;
                if (_cameraInvoke.func_getFormat(_mDeviceHandle, i, ref iWidth, ref iHeight) == 0) break;
                else
                {
                    tmp1 = iWidth.ToString();
                    tmp2 = iHeight.ToString();
                    this.deviceNameCB.Items.Add(tmp1 + " * " + tmp2);
                }
                i++;
            }
            HwndSource hwndSource = PresentationSource.FromVisual(pictrueBox) as HwndSource;
            //_mPreviewHWnd = this.pictureBox.Handle.ToInt32();
            _mPreviewHWnd = hwndSource.Handle.ToInt32();          
            _cameraInvoke.func_setPreviewWindow(_mDeviceHandle, _mPreviewHWnd);
            deviceNameCB.SelectedIndex = 7;
            _cameraInvoke.func_setAutoCrop(_mDeviceHandle, 1);

            if (_cameraInvoke.strErrorMessage != string.Empty)
            {
                MessageBox.Show(_cameraInvoke.strErrorMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                _cameraInvoke.func_startPreview(_mDeviceHandle);
                captureBtn.IsEnabled = true;
            }

        }
        /// <summary>
        /// 关闭窗体时需要处理
        /// </summary> 
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {          
            if (_mDeviceHandle != 0)
            {
                _cameraInvoke.func_CloseDevice(_mDeviceHandle);
            }

        }
        #endregion

        #region 分辨率窗体
        private void deviceNameCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _cameraInvoke.func_setCaptureSizeByIndex(_mDeviceHandle, 7);
        }
        #endregion

    }

    internal class UploadFile
    {
        public string FileName { get; set; }

        public Stream FileStream { get; set; }

        public bool Selected { get; set; }
    }
 }

