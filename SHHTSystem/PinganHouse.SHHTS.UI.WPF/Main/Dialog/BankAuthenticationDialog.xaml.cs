﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Dialog
{
    /// <summary>
    /// BankAuthenticationDialog.xaml 的交互逻辑
    /// </summary>
    public partial class BankAuthenticationDialog : Window
    {
        #region 变量

        ////案件编号
        //private string _CaseId = "";
        ////姓名
        //private string _Name = "";
        //银行名称
        private string _BankName = "";
        ////银行卡号
        //private string _BankNo = "";

        //身份证号
        private string iDCardNo = "";
        //银行预留手机号
        private string mobilePhone = "";

        int totalCount = 0;
        #endregion

        #region 事件
        /// <summary>
        /// 委托
        /// </summary>
        public delegate void CompletedEventHandler(Object sender, AuthenticationEventArgs e);

        /// <summary>
        /// 事件
        /// </summary>
        public event CompletedEventHandler OnCompleted;
       
        public class AuthenticationEventArgs : EventArgs
        {
            private OperationResult _OperationResult;

            /// <summary>
            /// 鉴权成功返回参数
            /// </summary>
            /// <param name="operationResul"></param>
            public AuthenticationEventArgs(OperationResult operationResul)
            {
                this._OperationResult = operationResul;
            }

            public OperationResult OperationResult
            {
                get
                {
                    return _OperationResult;
                }
            }
        }

        protected virtual void Completed(AuthenticationEventArgs e)
        {
            if (OnCompleted != null)
                OnCompleted(this, e);
        }

        #endregion

        public BankAuthenticationDialog(string caseId,string name, string bankName, string bankNo)
        {
            InitializeComponent();
            _BankName = bankName;

            TbIDCardNo.Focus();

            TbCaseId.Text = caseId;
            TbName.Text = name;
            TbBankNo.Text = bankNo;

        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            bool isExist = false;
            IDictionary iDict = ConfigServiceProxy.GetIdentifyBanks();

            if (iDict.Count > 0)
            {
                foreach (string iDic in iDict.Keys)
                {
                    if (!string.IsNullOrEmpty(_BankName))
                    {
                        if (iDic.Contains(_BankName))
                        {
                            isExist = true;
                        }
                    }
                }
            }
            if (isExist == false)
            {
                MessageBox.Show("无法进行鉴权！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            CboBankName.ItemsSource = iDict;
            CboBankName.DisplayMemberPath = "Value";
            CboBankName.SelectedValuePath = "Key";

            CboBankName.SelectedValue = _BankName;
        }

        /// <summary>
        /// 鉴权
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAuthentication_Click(object sender, RoutedEventArgs e)
        {
            //身份证号
            iDCardNo = TbIDCardNo.Text.ToString().Trim();
            //银行预留手机号
            mobilePhone = TbMobilePhone.Text.ToString().Trim();

            #region 验证空
            if (CboBankName.SelectedValue == null)
            {
                return;
            }
            if (string.IsNullOrEmpty(iDCardNo))
            {
                MessageBox.Show("身份证号不能为空！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (string.IsNullOrEmpty(mobilePhone))
            {
                MessageBox.Show("银行预留手机号不能为空！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            #endregion
            OperationResult result = AccountServiceProxy.AuthenticateBankCard(TbCaseId.Text.Trim(), CboBankName.SelectedValue.ToString(), TbBankNo.Text.Trim().Replace(" ", "").ToString(),
                TbName.Text.Trim(), iDCardNo, mobilePhone, LoginHelper.CurrentUser.SysNo);
            if (result.Success)
            {
                Completed(new AuthenticationEventArgs(result));
                MessageBox.Show("添加银行卡鉴权成功！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();
            }
            else
            {
                MessageBox.Show("添加银行卡鉴权失败！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

        }

        /// <summary>
        /// 窗体关闭事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closed(object sender, EventArgs e)
        {
            this.Close();
        }





    }
}
