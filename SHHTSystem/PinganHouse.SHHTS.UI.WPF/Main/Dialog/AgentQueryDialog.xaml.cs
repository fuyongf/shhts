﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.UI.WPF.Main;

namespace PinganHouse.SHHTS.UI.WPF
{
    /// <summary>
    /// AgentQueryDialog.xaml 的交互逻辑
    /// </summary>
    public partial class AgentQueryDialog : Window
    {
        private AgentStaff _agentStaff;

        public AgentQueryDialog()
        {
            InitializeComponent();
        }

        public AgentQueryDialog(AgentStaff agentStaff)
        {
            InitializeComponent();

            if (agentStaff == null)
                throw new ArgumentNullException("AgentStaff");

            _agentStaff = agentStaff;
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSelect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                IList<AgentStaff> agentStaffList = new List<AgentStaff>();
                int totalCount = 0;
                //查询条件
                var condition = string.IsNullOrEmpty(TbCondition.Text.Trim()) ? null : TbCondition.Text.Trim();

                agentStaffList = AgentServiceProxy.GetAgentStaffs(1, 10000, out totalCount, null, null, condition);

                if (agentStaffList != null && agentStaffList.Count > 0)
                {
                    DgAgentList.DataContext = agentStaffList;
                }
                else
                {
                    DgAgentList.DataContext = null;
                }
                //var agentList = AgentServiceProxy.GetAgentStaffs(1, 10000, out totalCount, null, null, condition);
                //if (agentList != null)
                //{
                //    agentStaffList = InitData(agentList);
                //}

                //DgAgentList.DataContext = agentStaffList;

            }
            catch (Exception ex)
            {
                MessageBox.Show("加载数据发生错误：" + ex.ToString());
            }
        }

        public static List<AgentStaff> InitData(IList<AgentStaff> agentStaffList)
        {
            var agentList = new List<AgentStaff>();

            foreach (var agentStaffDto in agentStaffList)
            {
                var agentStaff = new AgentStaff();
                agentStaff.RealName = agentStaffDto.RealName;
                agentStaff.Mobile = agentStaffDto.Mobile;
                agentList.Add(agentStaff);
            }
            return agentList;
        }


        /// <summary>
        /// 确认
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnConfirm_Click(object sender, RoutedEventArgs e)
        {
            if (DgAgentList.SelectedItems.Count > 0)
            {
                _agentStaff.AgentCompanySysNo = ((AgentStaff)DgAgentList.SelectedValue).AgentCompanySysNo;
                _agentStaff.SysNo = ((AgentStaff) DgAgentList.SelectedValue).SysNo;
                _agentStaff.RealName = ((AgentStaff)DgAgentList.SelectedValue).RealName;
                
                Close();
            }
            else
            {
                MessageBox.Show("请选择一行数据");
                return;
            }
        }
        
        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// 双击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DgAgentList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (DgAgentList.SelectedItems.Count > 0)
            {
                _agentStaff.AgentCompanySysNo = ((AgentStaff)DgAgentList.SelectedValue).AgentCompanySysNo;
                _agentStaff.SysNo = ((AgentStaff)DgAgentList.SelectedValue).SysNo;
                _agentStaff.RealName = ((AgentStaff)DgAgentList.SelectedValue).RealName;

                Close();
            }
            else
            {
                //MessageBox.Show("请选择一行数据");
                return;
            }
        }
    }
}
