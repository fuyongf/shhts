﻿using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Main.Nuclear;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Dialog
{
    /// <summary>
    /// CaseDetailsDialog.xaml 的交互逻辑
    /// </summary>
    public partial class CaseDetailsDialog : Window
    {

        /// <summary>
        /// 页面状态 -- 核案过来的状态
        /// </summary>
        const CaseStatus m_Status = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.HA3
                | CaseStatus.HA4 | CaseStatus.HA5 | CaseStatus.HA6 | CaseStatus.ZB2;

        /// <summary>
        /// 案件编号
        /// </summary>
        private string m_CaseId;

        public CaseDetailsDialog(string caseId)
        {
            InitializeComponent();
            m_CaseId = caseId;
            ctrl_BasicInfo.NavClickEvent += NavButton_Click;
            ctrl_CustomerInfo.NavClickEvent += NavButton_Click;
            ctrl_TransactionInfo.m_CaseId = caseId;
            ctrl_TransactionInfo.NavClickEvent += NavButton_Click;
        }

        /// <summary>
        /// 页面加载数据事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void CaseDetails_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                PageHelper.PageActiveLoadingControl(this.ctrlLoading, "加载数据,请稍候...");
                //案件对象
                var caseDto = await NuclearHelper.Instance.GetCaseDto(m_CaseId, m_Status);
                //案件附属对象
                var caseRemarkDto = await NuclearHelper.Instance.GetCaseRemarkDto(caseDto.SysNo);
                //中介信息
                var agencyDto = await NuclearHelper.Instance.GetAgentStaff(caseDto.AgencySysNo);
                //中介公司信息
                var agentCompany = await NuclearHelper.Instance.GetAgentCompany(caseDto.CompanySysNo);
                //银行卡信息
                var bankAccounts = await NuclearHelper.Instance.GetCustomerBankAccounts(caseDto.SysNo);
                //案件基础信息
                ctrl_BasicInfo.InitCtrlData(caseDto, caseRemarkDto, bankAccounts, agentCompany, agencyDto);
                //获取上下家信息
                var customers = await NuclearHelper.Instance.GetCustomers(m_CaseId);
                //上下家信息
                ctrl_CustomerInfo.InitCtrlData(customers.Item1, customers.Item2);
                //案件完成时间
                var dicCaseStatus = await NuclearHelper.Instance.GetCaseProcess(m_CaseId, m_Status);
                //计算时间
                ctrlProcessCtrl.BindingDataToProcess(dicCaseStatus);
            }
            catch (Exception ex)
            {
                MessageBox.Show("加载案件信息发生系统异常：" + ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                this.ctrlLoading.IsBusy = false;
            }
        }

        /// <summary>
        /// Tab切换
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavButton_Click(object sender, EventArgs e)
        {
            var lab = sender as Button;
            switch (lab.Tag.ToString())
            {
                case "1":
                    tab_BasicInfo.IsSelected = true;
                    break;
                case "2":
                    tab_CustomerInfo.IsSelected = true;
                    break;
                case "3":
                    tab_TransactionInfo.IsSelected = true;
                    break;
            }
        }
    
    }
}
