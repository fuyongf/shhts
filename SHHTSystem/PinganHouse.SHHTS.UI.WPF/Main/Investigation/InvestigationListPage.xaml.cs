﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PinganHouse.SHHTS.UI.WPF.Main.PreCheck;
using PinganHouse.SHHTS.AccessControl.Proxy;

namespace PinganHouse.SHHTS.UI.WPF.Main.Investigation
{
    /// <summary>
    /// InvestigationListPage.xaml 的交互逻辑
    /// </summary>
    [SecurityAuthorize]
    public partial class InvestigationListPage : Page
    {
        #region 公共变量
        private int PageSize = 15;
        private ScanBarCodeDialog scanBarCodeDialog = new ScanBarCodeDialog();
        #endregion

        public InvestigationListPage()
        {
            InitializeComponent();
            //注册分页
            this.GridPager.PageNumChanged = GridPager_PagerIndexChanged;
        }

        /// <summary>
        /// 查询搜索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchBtn_Click(object sender, RoutedEventArgs e)
        {
            GridPager.PageIndex = 1;
        }

        /// <summary>
        /// 分页事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridPager_PagerIndexChanged(int pageIndex)
        {
            PageHelper.PageActiveLoadingControl(this.loading, "正在加载数据,请稍后...");
            this.GetDataToBindCtrl();
        }

        /// <summary>
        /// 添加附件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AttachmentBtn_Click(object sender, RoutedEventArgs e)
        {
            //案件编号
            var item = this.dataGrid.CurrentItem;
            if (item != null)
            {
                var caseId = ((PropertySurveyModel)item).CaseId;
                var attachments = new UploadAttachment(caseId);
                PageHelper.PageNavigateHelper(this, attachments);
            }
        }

        /// <summary>
        /// Load 加载数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Body_Loaded(object sender, RoutedEventArgs e)
        {
            GridPager.PageIndex = 1;
        }

        /// <summary>
        /// 双击Grid事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var item = this.dataGrid.CurrentItem;
                e.Handled = true;
            }
            catch (Exception)
            {
                MessageBox.Show("显示明细发生错误", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #region 页面数据取得方法
        private async void GetDataToBindCtrl()
        {
            try
            {
                //产权证号
                var contract = string.IsNullOrEmpty(this.txtContract.Text.Trim()) ? null : this.txtContract.Text.Trim();

                //案件地址
                var address = string.IsNullOrEmpty(this.txtAddress.Text.Trim()) ? null : this.txtAddress.Text.Trim();

                //开始时间
                DateTime startTime = DateTime.MinValue;
                if (!string.IsNullOrEmpty(dateStart.Text))
                {
                    DateTime.TryParse(dateStart.Text, out startTime);
                }
                //结束时间
                DateTime endTime = DateTime.MinValue;
                if (!string.IsNullOrEmpty(dateEnd.Text))
                {
                    DateTime.TryParse(dateEnd.Text, out endTime);
                }

                var tupleModel = await GetDataGridData(this.GridPager.PageIndex, PageSize, contract, address, startTime, endTime, null);
                this.dataGrid.DataContext = tupleModel.Item2;
                GridPager.Visibility = Visibility.Visible;
                if (tupleModel.Item1 % PageSize != 0)
                {
                    GridPager.PageCount = tupleModel.Item1 / PageSize + 1;
                }
                else { GridPager.PageCount = tupleModel.Item1 / PageSize; }
                loading.IsBusy = false;

            }
            catch (Exception ex)
            {
                loading.IsBusy = false;
                MessageBox.Show("加载案件数据发生错误：" + ex.ToString(), "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        /// <summary>
        /// 获取页面数据
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="size"></param>
        /// <param name="caseId"></param>
        /// <param name="address"></param>
        /// <param name="buyerName"></param>
        /// <param name="agencyName"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        private Task<Tuple<int, List<PropertySurveyModel>>>GetDataGridData(int pageIndex, int pageSize, string tenementContract = null,
                string tenementAddress = null, DateTime? startDate = null, DateTime? endDate = null, IDictionary<string, object> data = null)
        {
            return Task.Run(() =>
            {
                var cnt = 0;
                var propertySurveyModelList = new List<PropertySurveyModel>();
                if (startDate == DateTime.MinValue && endDate == DateTime.MinValue)
                {
                    var propertySurvey = PropertySurveyProxyService.GetInstanse().GetPaginatedList(GridPager.PageIndex, PageSize, out cnt, tenementContract, tenementAddress, null, null);
                    if (propertySurvey != null)
                    {
                        propertySurveyModelList = PropertySurveyModel.InitListModel(propertySurvey);
                    }
                }
                else if (startDate != DateTime.MinValue && endDate != DateTime.MinValue)
                {
                    var propertySurveyList = PropertySurveyProxyService.GetInstanse().GetPaginatedList(GridPager.PageIndex, PageSize, out cnt, tenementContract, tenementAddress, startDate, endDate);
                    if (propertySurveyList != null)
                    {
                        propertySurveyModelList = PropertySurveyModel.InitListModel(propertySurveyList);
                    }
                }
                else if (startDate != DateTime.MinValue && endDate == DateTime.MinValue)
                {
                    var propertySurveyList = PropertySurveyProxyService.GetInstanse().GetPaginatedList(GridPager.PageIndex, PageSize, out cnt, tenementContract, tenementAddress, null, startDate);
                    if (propertySurveyList != null)
                    {
                        propertySurveyModelList = PropertySurveyModel.InitListModel(propertySurveyList);
                    }
                }
                else if (startDate == DateTime.MinValue && endDate != DateTime.MinValue)
                {
                    var propertySurveyList = PropertySurveyProxyService.GetInstanse().GetPaginatedList(GridPager.PageIndex, PageSize, out cnt, tenementContract, tenementAddress, null, endDate);
                    if (propertySurveyList != null)
                    {
                        propertySurveyModelList = PropertySurveyModel.InitListModel(propertySurveyList);
                    }
                }

                var dicResult = new Tuple<int, List<PropertySurveyModel>>(cnt, propertySurveyModelList);
                return dicResult;
                });
        }
        #endregion

        /// <summary>
        /// 追加产调
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddInvesContinue_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var item = this.dataGrid.CurrentItem as PropertySurveyModel;
                if (item != null)
                {
                    var addIns = new AddInvesContinue(item.TenementContract);
                    PageHelper.PageNavigateHelper(this, addIns);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("操作发生系统异常:"+ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 新增产调
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [Permission("10010")]
        private void AddInves_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var item = this.dataGrid.CurrentItem;
                PageHelper.PageNavigateHelper(this, new AddInves());
            }
            catch (Exception ex)
            {
                MessageBox.Show("操作发生系统异常:" + ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 产调记录
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SurveyNum_Click(object sender, RoutedEventArgs e)
        {
            var tenementContract = Convert.ToString(((Button)sender).CommandParameter);
            if (!string.IsNullOrEmpty(tenementContract))
            {
                new PropertyList(tenementContract).ShowDialog();
            }
        }
    }
}
