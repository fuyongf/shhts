﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PinganHouse.SHHTS.UI.WPF.ReadCard;
using PinganHouse.SHHTS.AccessControl.Proxy;

namespace PinganHouse.SHHTS.UI.WPF.Main.Investigation
{
    /// <summary>
    /// AddInves.xaml 的交互逻辑
    /// </summary>
    [SecurityAuthorize]
    public partial class AddInves : Page
    {
        //产权证号
        private string m_PropertyNo;

        /// <summary>
        /// 构造函数
        /// </summary>
        public AddInves()
        {
            InitializeComponent();
        }


        /// <summary>
        /// 上传附件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUpload_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                m_PropertyNo = txt_TenementContract_0.Text.Trim() + "-" + txt_TenementContract.Text.Trim();
                if (!string.IsNullOrEmpty(m_PropertyNo.Replace("-", "")))
                {
                    var upload = new UploadFileHelper();
                    var fileNum = 0;
                    upload.UploadFile(m_PropertyNo, AttachmentType.产调证, out fileNum);
                }
                else
                {
                    MessageBox.Show("请先填写产证号.", "系统异常", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
             
            }
            catch (Exception ex){
                MessageBox.Show("上传附件操作发生系统异常:"+ex.Message,"系统异常",MessageBoxButton.OK,MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 高拍仪上传
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCamera_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var captruePage = new CaptureImgeDialog(m_PropertyNo, AttachmentType.产调证);
                captruePage.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("上传文件操作发生异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 取消产调
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [Permission("10011")]
        private void btnCancle_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, new InvestigationListPage());
        }

        /// <summary>
        /// 确定产调
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CheckValidityData())
                {
                    var propertySurvey = new PropertySurveyDto();
                    propertySurvey.TenementContract = txt_TenementContract_0.Text.Trim() + "-" + txt_TenementContract.Text.Trim();
                    propertySurvey.SurveyDate = dp_SurveyDate.SelectedDate;
                    if (m_AgentStaff != null)
                    {
                        propertySurvey.ProposerSysNo = m_AgentStaff.SysNo;
                    }
                    //有无抵押
                    if ((bool)rb_IsGuaranty_1.IsChecked)
                    {
                        propertySurvey.IsGuaranty = true;
                    }
                    else if ((bool)rb_IsGuaranty_0.IsChecked)
                    {
                        propertySurvey.IsGuaranty = false;
                    }
                    else
                    {
                        propertySurvey.IsGuaranty = null;
                    }
                    //有无查封
                    if ((bool)rb_IsAttachment_1.IsChecked)
                    {
                        propertySurvey.IsAttachment = true;
                    }
                    else if ((bool)rb_IsAttachment_0.IsChecked)
                    {
                        propertySurvey.IsAttachment = false;
                    }
                    else
                    {
                        propertySurvey.IsAttachment = null;
                    }
                    //有无租赁
                    if ((bool)rb_IsLeasing_1.IsChecked)
                    {
                        propertySurvey.IsLeasing = true;
                    }
                    else if ((bool)rb_IsLeasing_0.IsChecked)
                    {
                        propertySurvey.IsLeasing = false;
                    }
                    else
                    {
                        propertySurvey.IsLeasing = null;
                    }
                    propertySurvey.CreateUserSysNo = LoginHelper.CurrentUser.SysNo;
                    var result = PropertySurveyProxyService.GetInstanse().AddPropertySurvey(propertySurvey,txt_TenementAddress.Text.Trim(),null);
                    if (!result.Success) {
                        MessageBox.Show("保存产调数据发生错误：" + result.ResultMessage, "系统异常", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    else
                    {
                        btnCancle_Click(sender, e);
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show("操作系统发生异常："+ex.Message,"系统异常",MessageBoxButton.OK,MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 数据合法性检查
        /// </summary>
        /// <returns></returns>
        private bool CheckValidityData()
        {
            if (string.IsNullOrEmpty(txt_TenementContract.Text.Trim())) 
            {
                MessageBox.Show("产权号不能为空.","系统提示",MessageBoxButton.OK,MessageBoxImage.Information);
                return false;
            }
            if (string.IsNullOrEmpty(txt_TenementAddress.Text.Trim()))
            {
                MessageBox.Show("物业地址不能为空.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }
            //验证申请人
            if (string.IsNullOrEmpty(m_AgentStaff.RealName))
            {
                MessageBox.Show("请选择产调申请人.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }
            //产调结果
            if (!(bool)rb_IsAttachment_0.IsChecked && !(bool)rb_IsAttachment_1.IsChecked
                && !(bool)rb_IsGuaranty_0.IsChecked && !(bool)rb_IsGuaranty_1.IsChecked
                && !(bool)rb_IsLeasing_0.IsChecked && !(bool)rb_IsLeasing_1.IsChecked)
            {
                MessageBox.Show("请选择产调结果.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }
            //产调时间
            if (dp_SurveyDate.SelectedDate==null)
            {
                MessageBox.Show("请选择产调时间.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }
            return true;
        }

        /// <summary>
        /// 页面Key_Down
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Body_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                btnCancle_Click(sender,e);
            }
        }

        /// <summary>
        /// 页面加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Body_Loaded(object sender, RoutedEventArgs e)
        {
            txt_TenementContract_0.Focus();
        }

        /// <summary>
        /// 证件
        /// </summary>
        UserIdentityModel m_IdentityInfo = new UserIdentityModel();

        /// <summary>
        /// 读取经纪人
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAgent_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var idCard = new IdCard(m_IdentityInfo,3);
                //验证是否有设备
                if (idCard.DeviceNotExists)
                    return;

                idCard.Closing += idCard_Closing;
                idCard.ShowDialog();
            }
            catch (Exception) { }
        }

        /// <summary>
        /// 读取身份证
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void idCard_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try {
                int totalCount = 0;
                IList<AgentStaff> agentStaffList = new List<AgentStaff>();
                agentStaffList = AgentServiceProxy.GetAgentStaffs(1, 10000, out totalCount, null, null, null, m_IdentityInfo.IdentityNo);
                if (agentStaffList.Count > 0)
                {
                    for (int i = 0; i < agentStaffList.Count; i++)
                    {
                        AgentCompany agentCompany = AgentServiceProxy.GetAgentCompany(agentStaffList[i].AgentCompanySysNo);
                        if (agentCompany != null)
                        {
                            txt_ShowInfo.Text = "姓名：" + agentStaffList[i].RealName + "                 所属门店：" +
                                               agentCompany.CompanyName;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("没有符合该身份证号的经纪人！","系统提示",MessageBoxButton.OK,MessageBoxImage.Information);
                }           
            }
            catch (Exception ex)
            {
                MessageBox.Show("系统读取证件发生异常："+ex.Message,"系统提示",MessageBoxButton.OK,MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 经纪人--申请人
        /// </summary>
        private AgentStaff m_AgentStaff = new AgentStaff();
        /// <summary>
        /// 模糊查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnQuery_Click(object sender, RoutedEventArgs e)
        {
            AgentQueryDialog agentQueryDialog = new AgentQueryDialog(m_AgentStaff);
            agentQueryDialog.Closing += agentQueryDialog_Closing;
            agentQueryDialog.ShowDialog();
        }

        /// <summary>
        /// 读取经纪人
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void agentQueryDialog_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try {
                var agentCompany = AgentServiceProxy.GetAgentCompany(m_AgentStaff.AgentCompanySysNo);
                if (agentCompany != null)
                {
                    txt_ShowInfo.Text = "姓名：" + m_AgentStaff.RealName + "                 所属门店：" + agentCompany.CompanyName;
                }
            }
            catch (Exception) { }
        }
    }
}
