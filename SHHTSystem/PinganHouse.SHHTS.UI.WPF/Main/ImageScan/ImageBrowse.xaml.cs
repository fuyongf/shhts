﻿using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.ImageScan
{
    /// <summary>
    /// ImageBrowse.xaml 的交互逻辑
    /// </summary>
    public partial class ImageBrowse : Window
    {
        #region 公共变量

        private double rotateAngle;

        private BitmapImage bitmapImage;

        private List<string> imageId = new List<string>();
        
        private Dictionary<string, Stream> imgstream = new Dictionary<string, Stream>();
        //private List<Stream> imgstream=new List<Stream>();

        private int count = 0;

        #endregion

        public ImageBrowse(List<string> imageIds)
        {
            InitializeComponent();
            imageId = imageIds;
        }
 
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (imageId == null)
                MessageBox.Show("页面初始化错误", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            if (imageId.Count==1)
            {
                UpBtn.IsEnabled = false;
                DownBtn.IsEnabled = false;
            }
            else
            {
                UpBtn.IsEnabled = false;               
            }
            count = 1;          
            ShowPicture(imageId[count-1]);


        }
        /// <summary>
        /// 展示图片
        /// </summary>
        /// <param name="fileName"></param>
        private async void ShowPicture(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return;
            if (count > 1)
                UpBtn.IsEnabled = true;
            if (count < imageId.Count)
                DownBtn.IsEnabled = true;
         
            #region 下载图片
            Loading_Ctrl.IsBusy = true;
            Loading_Ctrl.Text = "正在下载...";

            Stream fileStream = await Task.Run(() =>
            {
                return DownImage(id);
            });

            Loading_Ctrl.IsBusy = false;
            #endregion

            if (fileStream == null)
            {
                MessageBox.Show("图片下载失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            fileStream.Seek(0, SeekOrigin.Begin);
            bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.StreamSource = fileStream;

            if (rotateAngle == 0)
            {
                bitmapImage.Rotation = Rotation.Rotate0;
            }
            else if (rotateAngle == 90 || rotateAngle == -270)
            {
                bitmapImage.Rotation = Rotation.Rotate90;
            }
            else if (rotateAngle == 180 || rotateAngle == -180)
            {
                bitmapImage.Rotation = Rotation.Rotate180;
            }
            else if (rotateAngle == 270||rotateAngle == -90)
            {
                bitmapImage.Rotation = Rotation.Rotate270;
            }      
            bitmapImage.EndInit();
            ResizePicture();
        }

        private void ResizePicture()
        {
            this.ImgShow.Height = bitmapImage.Height;
            this.ImgShow.Width = bitmapImage.Width;
            this.ImgBorder.Height = this.ImgShow.Height;
            this.ImgBorder.Width = this.ImgShow.Width;
            this.ImgShow.Source = bitmapImage;
        }

        private void UpBtn_Click(object sender, RoutedEventArgs e)
        {
            count--;
            if (count == 1)
            {
                UpBtn.IsEnabled = false;
            }       
            ShowPicture(imageId[count-1]);
        }

        private void DownBtn_Click(object sender, RoutedEventArgs e)
        {
            count++;
            if (count== imageId.Count )
            {
                DownBtn.IsEnabled = false;
            }          
            ShowPicture(imageId[count-1]);
        }

        private void LeftBtn_Click(object sender, RoutedEventArgs e)
        {
            rotateAngle -= 90;
            rotateAngle = rotateAngle % 360;
            doRotate();
        }

        private void rightBtn_Click(object sender, RoutedEventArgs e)
        {          
            rotateAngle += 90;
            rotateAngle = rotateAngle % 360;
            doRotate();
        }

        private void doRotate()
        {
            ShowPicture(imageId[count-1]);
        }

        private async Task<Stream> DownImage(string fileId)
        {
            if (imgstream.ContainsKey(fileId))
                return imgstream[fileId];
            Stream imagStream = null;
            var file = CaseHelper.DownloadAttachment(fileId);
            using (file.FileData)
            {
                imagStream = new MemoryStream();
                file.FileData.CopyTo(imagStream);
            }
            imgstream.Add(fileId, imagStream);
            return imagStream;
        }

        private void img_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            Point centerPoint = e.GetPosition(ImgBorder);
            this.sfr.CenterX = centerPoint.X;
            this.sfr.CenterY = centerPoint.Y;
            if (sfr.ScaleX < 0.3 && sfr.ScaleY < 0.3 && e.Delta < 0)
            {
                return;
            }
            sfr.ScaleX += (double)e.Delta / 3500;
            sfr.ScaleY += (double)e.Delta / 3500;
        } 
    }
}
