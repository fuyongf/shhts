﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.EHouseMoney
{
    /// <summary>
    /// EHouseMoneyPrintInfo.xaml 的交互逻辑
    /// </summary>
    public partial class EHouseMoneyPrintInfo : Page
    {
        IList<PrintTemplate> _ListName;

        Dictionary<string, PrintTemplateValue> _DicPrintTemValue = new Dictionary<string, PrintTemplateValue>();

        private readonly PrintDocument _printDocument = new PrintDocument();

        /// <summary>
        /// 打印机列表
        /// </summary>
        private List<PrintModel> _ListPrint = new List<PrintModel>();

        /// <summary>
        /// 打印机名字
        /// </summary>
        private string _ReceiptPrintName;

        public EHouseMoneyPrintInfo()
        {
            InitializeComponent();

            #region 获得本机所有打印机
            var printers = PrinterSettings.InstalledPrinters;//获取本机上的所有打印机
            //PrintModel pm1 = new PrintModel();
            //pm1.PrintNo = -1;
            //pm1.PrintName = "请选择打印机";
            //_ListPrint.Add(pm1);
            PrintModel pm = new PrintModel();
            pm.PrintNo = 0;
            pm.PrintName = "未分配";
            _ListPrint.Add(pm);

            for (int i = 0; i < printers.Count; i++)
            {
                PrintModel print = new PrintModel();
                print.PrintNo = i + 1;
                print.PrintName = printers[i].ToString();
                _ListPrint.Add(print);
            }
            #endregion
            //从注册表加载 E房钱打印机信息          
            _ReceiptPrintName = PrivateProfileUtils.GetContentValue("ContractPrint", "PrintName"); //打印机名

        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            _ListName = ConfigServiceProxy.GetEFangqianPrintTemplates();
            if (_ListName != null)
            {
                CboName.ItemsSource = _ListName;
                CboName.DisplayMemberPath = "TemplateName";
                CboName.SelectedValuePath = "TemplateId";

                //CboPageNum.ItemsSource = _ListName[0].Pages;
            }

        }

        private void CboName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CboName.SelectedValue == null)
            {
                return;
            }
            string pageNum = CboName.SelectedValue.ToString();
            if (_ListName != null)
            {
                for (int i = 0; i < _ListName.Count; i++)
                {
                    if (_ListName[i].TemplateId == pageNum)
                    {
                        CboPageNum.ItemsSource = _ListName[i].Pages;
                    }
                }
            }
        }


        /// <summary>
        /// 生成
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCreate_Click(object sender, RoutedEventArgs e)
        {
            Grid_InputBox.Children.Clear();
            if (CboName.SelectedValue != null && CboPageNum.SelectedValue != null)
            {
                _DicPrintTemValue = ConfigServiceProxy.GetPrintTemplate(CboName.SelectedValue.ToString(), CboPageNum.SelectedValue.ToString());

                if (_DicPrintTemValue == null) { return; }
                if (_DicPrintTemValue.Count > 0)
                {
                    foreach (var dic in _DicPrintTemValue.Keys)
                    {
                        Label lbl = new Label();
                        lbl.Margin = new Thickness(0, 5, 0, 0);

                        PrintTemplateValue t = _DicPrintTemValue[dic];
                        lbl.Content = t.FieldName;
                        Grid_BoxName.Children.Add(lbl);

                        TextBox tb = new TextBox();
                        tb.Name = dic;
                        tb.Width = 150;
                        tb.Margin = new Thickness(5, 5, 5, 5);
                        Grid_InputBox.Children.Add(tb);

                    }
                }
            }
        }

        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnPrint_Click(object sender, RoutedEventArgs e)
        {
            #region E房钱打印机信息
            int printCount = 0;

            if (!string.IsNullOrWhiteSpace(_ReceiptPrintName))
            {
                for (int j = 0; j < _ListPrint.Count; j++)
                {
                    if (_ReceiptPrintName == _ListPrint[j].PrintName)
                    {
                        printCount++;
                    }
                }
            }
            else //为空，则 E房钱打印机未分配
            {
                MessageBox.Show("该E房钱打印机未设置，无法进行打印操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (printCount == 0)
            {
                MessageBox.Show("该E房钱打印机未设置，无法进行打印操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (_ReceiptPrintName.Contains("未分配"))
            {
                MessageBox.Show("该E房钱打印机未设置，无法进行打印操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            #endregion

            var controler = new StandardPrintController();

            try
            {
                //打印机名
                _printDocument.PrinterSettings.PrinterName = _ReceiptPrintName;

                _printDocument.PrintPage += PrintBarCodeNumber;
                _printDocument.PrintController = controler;
                _printDocument.DefaultPageSettings.PaperSize = new PaperSize("Custum", 237, 119); //119  169
                //_printDocument.PrinterSettings.LandscapeAngle = 180;
                //var pDialog = new PrintPreviewDialog { Document = _printDocument };
                //pDialog.ShowDialog();
                _printDocument.Print();
            }
            catch (Exception err)
            {
                throw;
            }
            finally
            {
                _printDocument.Dispose();
            }

        }



        private void PrintBarCodeNumber(Object sender, PrintPageEventArgs av)
        {
            //TextBox tb;
            for (int i = 0; i < Grid_InputBox.Children.Count; i++)
            {
                TextBox tb = Grid_InputBox.Children[i] as TextBox;
                if (tb.Name != null)
                {
                    //Test t = myDictionary[tb.Name];
                    //av.Graphics.DrawString(tb.Text, Print_Font, MyBrush, new PointF(float.Parse(t.sX), float.Parse(t.sY)));
                }
            }

            //av.Graphics.DrawString("上", Print_Font, MyBrush, Location);

            //av.Graphics.DrawString("上", Print_Font, MyBrush, new PointF(10, 20));
            //av.Graphics.DrawString("下", Print_Font, MyBrush, new PointF(15, 20));
            //av.Graphics.DrawString("左", Print_Font, MyBrush, new PointF(20, 20));
            //av.Graphics.DrawString("右", Print_Font, MyBrush, new PointF(25, 20));

            av.HasMorePages = false;
        }


    }
}
