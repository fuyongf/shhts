﻿using PinganHouse.SHHTS.RemoteServiceProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Setting
{
    /// <summary>
    /// AboutPage.xaml 的交互逻辑
    /// </summary>
    public partial class AboutPage : Window
    {
        public AboutPage()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                LblVersion.Content = "版本号："+Assembly.GetExecutingAssembly().GetName().Version;

                TbInfo.Text = ConfigServiceProxy.GetSHHTMSAbooutInfo();
            }
            catch
            {

            }
        }

        private void BtnConfirm_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
