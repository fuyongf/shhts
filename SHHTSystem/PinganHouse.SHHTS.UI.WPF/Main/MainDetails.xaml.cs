﻿using PinganHouse.SHHTS.AccessControl.Proxy;
using PinganHouse.SHHTS.UI.WPF.Main.Funds;
using PinganHouse.SHHTS.UI.WPF.Main.Host;
using PinganHouse.SHHTS.UI.WPF.Main.Investigation;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace PinganHouse.SHHTS.UI.WPF.Main
{
    /// <summary>
    /// MainDetails.xaml 的交互逻辑
    /// </summary>
    [SecurityAuthorize]
    public partial class MainDetails : Window
    {
        #region 公共变量

        /// <summary>
        /// 当前页面
        /// </summary>
        public static MainDetails Current;

        #endregion

        /// <summary>
        /// 页面类型
        /// </summary>
        public MainDetails(string pageType)
        {
            InitializeComponent();
            Current = this;

            userName.Text = LoginHelper.CurrentUser.RealName;
            switch (pageType)
            {
                case "preview":
                    txt_WorkTag.Text = "（预检工作台）";
                    MainFrame.Navigate(new Uri("Main/PreCheck/AddCasePage.xaml", UriKind.RelativeOrAbsolute));
                    break;
                case "searchCase":
                    txt_WorkTag.Text = "（预检工作台）";
                    MainFrame.Navigate(new Uri("Main/PreCheck/PreCheckSearchListPage.xaml", UriKind.RelativeOrAbsolute));
                    break;
                case "sign":
                    txt_WorkTag.Text = "（核案工作台）";
                    MainFrame.Navigate(new Uri("Main/Nuclear/CheckCaseSearchPage.xaml", UriKind.RelativeOrAbsolute));
                    break;
                case "returnLoan":
                    txt_WorkTag.Text = "（卖方还贷工作台）";
                    MainFrame.Navigate(new Uri("Main/Credit/ApplyForCreditListPage.xaml", UriKind.RelativeOrAbsolute));
                    break;
                case "changeName":
                    txt_WorkTag.Text = "（审税过户工作台）";
                    MainFrame.Navigate(new Uri("Main/AuditTax/ChangeNameListPage.xaml", UriKind.RelativeOrAbsolute));
                    break;
                case "hostTable":
                    txt_WorkTag.Text = "（主办工作台）";
                    MainFrame.Navigate(new Uri("Main/Host/CaseManagementListPage.xaml", UriKind.RelativeOrAbsolute));
                    break;
                case "hostTable0":
                case "hostTable1":
                case "hostTable2":
                case "hostTable3":
                case "hostTable4":
                case "hostTable5":
                    txt_WorkTag.Text = "（主办工作台）";
                    MainFrame.Navigate(new HostListPage(Convert.ToInt32(pageType.Substring(9))), UriKind.RelativeOrAbsolute);
                    break;
                case "chanDiao":
                    txt_WorkTag.Text = "（产调工作台）";
                    MainFrame.Navigate(new Uri("Main/Investigation/InvestigationListPage.xaml", UriKind.RelativeOrAbsolute));
                    break;
                case "addNewChanDiao":
                    txt_WorkTag.Text = "（产调工作台）";
                    MainFrame.Navigate(new Uri("Main/Investigation/AddInves.xaml", UriKind.RelativeOrAbsolute));    
                    break;
                case "loanAgent":
                    txt_WorkTag.Text = "（贷款办理工作台）";
                    MainFrame.Navigate(new Uri("Main/Credit/CreditListPage.xaml", UriKind.RelativeOrAbsolute));
                    break;
                case "endBusibess":
                    txt_WorkTag.Text = "（结案工作台）";
                    MainFrame.Navigate(new Uri("Main/AddCasePage.xaml", UriKind.RelativeOrAbsolute));
                    break;
                case "ehousemoney":
                    txt_WorkTag.Text = "（E房钱）";
                    MainFrame.Navigate(new Uri("Main/EHouseMoney/ApplicationInfo.xaml", UriKind.RelativeOrAbsolute));
                    break;
                case "moneysettle": //资金信息
                    txt_WorkTag.Text = "（资金结算工作台）";
                    //MainFrame.Navigate(new Uri("Main/Funds/FundSettlementListPage.xaml", UriKind.RelativeOrAbsolute));
                    MainFrame.Navigate(new FundSettlementListPage(), UriKind.RelativeOrAbsolute);
                    break;
                case "moneysettle1":
                case "moneysettle2":
                case "moneysettle3":
                case "moneysettle4":
                    txt_WorkTag.Text = "（资金结算工作台）";
                    MainFrame.Navigate(new FundSettlementListPage(Convert.ToInt32(pageType.Substring(11))), UriKind.RelativeOrAbsolute);
                    break;
                case "taxCalculator":
                    txt_WorkTag.Text = "（税费计算器）";
                    MainFrame.Navigate(new Uri("Main/Calculation/TaxCalculation.xaml", UriKind.RelativeOrAbsolute));
                    break;
                case "limitCheck":
                    txt_WorkTag.Text = "（限购查询）";
                    MainFrame.Navigate(new Uri("Main/Purchase/PurchaseQueryPage.xaml", UriKind.RelativeOrAbsolute));
                    break;
                case "BankAuthentication":
                    txt_WorkTag.Text = "（代扣授权）";
                    MainFrame.Navigate(new Uri("Main/BankAuthentication/BankAuthenticationSearch.xaml", UriKind.RelativeOrAbsolute));
                    break;
            }

        }
        /// <summary>
        /// 首页按钮跳转
        /// </summary>    
        private void BtnHome_Click(object sender, RoutedEventArgs e)
        {
            //new MainWindow().Show();
            this.Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            new MainWindow(true).Show();
        }

        private bool _allowDirectNavigation = false;
        private NavigatingCancelEventArgs _navArgs = null;
        /// <summary>
        /// 页面效果处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainFrame_Navigating(object sender, System.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            //if (Content != null && !_allowDirectNavigation)
            //{
            //    e.Cancel = true;
            //    _navArgs = e;
            //    this.IsHitTestVisible = false;
            //    DoubleAnimation da = new DoubleAnimation(0.3d, new Duration(TimeSpan.FromMilliseconds(1)));
            //    da.Completed += FadeOutCompleted;
            //    this.BeginAnimation(OpacityProperty, da);
            //}
            //_allowDirectNavigation = false;
        }

        private void FadeOutCompleted(object sender, EventArgs e)
        {
            try
            {
                (sender as AnimationClock).Completed -= FadeOutCompleted;

                this.IsHitTestVisible = true;

                _allowDirectNavigation = true;
                switch (_navArgs.NavigationMode)
                {
                    case NavigationMode.New:
                        if (_navArgs.Uri == null)
                        {
                            MainFrame.NavigationService.Navigate(_navArgs.Content);
                        }
                        else
                        {
                            MainFrame.NavigationService.Navigate(_navArgs.Uri);
                        }
                        break;
                    case NavigationMode.Back:
                        MainFrame.NavigationService.GoBack();
                        break;
                    case NavigationMode.Forward:
                        MainFrame.NavigationService.GoForward();
                        break;
                    case NavigationMode.Refresh:
                        MainFrame.NavigationService.Refresh();
                        break;
                }
                Dispatcher.BeginInvoke(DispatcherPriority.Loaded,
                    (ThreadStart)delegate()
                {
                    DoubleAnimation da = new DoubleAnimation(1.0d, new Duration(TimeSpan.FromMilliseconds(100)));
                    this.BeginAnimation(OpacityProperty, da);
                });
            }
            catch (Exception ex) {
                MessageBox.Show("前往指定的页面发生系统异常："+ex.Message ,"系统提示",MessageBoxButton.OK,MessageBoxImage.Error);
            }
        }
    }
}
