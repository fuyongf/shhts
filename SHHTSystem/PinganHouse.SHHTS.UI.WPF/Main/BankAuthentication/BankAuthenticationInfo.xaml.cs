﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.BankAuthentication
{
    /// <summary>
    /// BankAuthenticationInfo.xaml 的交互逻辑
    /// </summary>
    public partial class BankAuthenticationInfo : Page
    {
        /// <summary>
        /// 案件信息
        /// </summary>
        private CaseWithCustomer _CaseInfo;

        /// <summary>
        /// 银行信息
        /// </summary>
        private AuthedBankCard _BankInfo;

        /// <summary>
        /// 操作按钮状态：1,取消；2，确认；3，更改；4，注销
        /// </summary>
        private int _OperateButtonStatus;


        public BankAuthenticationInfo(CaseWithCustomer caseInfo, AuthedBankCard bankInfo)
        {
            InitializeComponent();

            _CaseInfo = caseInfo;
            _BankInfo = bankInfo;

            //TbBankNo.Foreground = Brushes.Gray;
            //TbBankNo.Text = "银行卡号";

            CboName.Focus();
            BtnModify.IsEnabled = false;
            BtnModifyBank.IsEnabled = false;
        }

        public BankAuthenticationInfo()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 窗体加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            IDictionary iDict = ConfigServiceProxy.GetIdentifyBanks();
            if (iDict.Count > 0)
            {
                CboBankName.ItemsSource = iDict;
                CboBankName.DisplayMemberPath = "Value";
                CboBankName.SelectedValuePath = "Key";
            }
            #region 案件信息
            if (_CaseInfo != null)
            {
                TbCaseId.Text = _CaseInfo.CaseId;
                //产权证号
                if (_CaseInfo.TenementContract != null)
                {
                    TbTenementContract.Text = _CaseInfo.TenementContract.Replace('-', ' ');
                }
                //房屋坐落
                if (_CaseInfo.TenementAddress != null)
                    TbTenementAddress.Text = _CaseInfo.TenementAddress;

                if (_CaseInfo.Buyers.Count > 0)
                {
                    //IList<Customer> listCustomer = _CaseInfo.Buyers.Where(customer => customer.CustomerType == CustomerType.Buyer).ToList();

                    CboName.ItemsSource = _CaseInfo.Buyers;
                    CboName.DisplayMemberPath = "RealName";
                    CboName.SelectedValuePath = "UserSysNo";
                }
            }
            #endregion

            #region  银行信息

            if (_BankInfo != null)
            {
                BtnModify.IsEnabled = true;
                BtnModifyBank.IsEnabled = true;
                OperateControl(false);
                //姓名
                if (!string.IsNullOrEmpty(_BankInfo.AccountName))
                {
                    CboName.Text = _BankInfo.AccountName;
                }
                //身份证
                if (!string.IsNullOrEmpty(_BankInfo.IdentityNo))
                {
                    TbIDCardNo.Text = _BankInfo.IdentityNo;
                }
                //银行名称
                if(!string.IsNullOrEmpty(_BankInfo.BankCode))
                {
                    CboBankName.SelectedValue = _BankInfo.BankCode;
                }
                //银行卡号
                if (!string.IsNullOrEmpty(_BankInfo.CardNo))
                {
                    TbBankNo.Text = _BankInfo.CardNo;
                }
                //银行预留手机号
                if (!string.IsNullOrEmpty(_BankInfo.Mobile))
                {
                    TbMobilePhone.Text = _BankInfo.Mobile;
                }
            }

            #endregion
        }

        #region

        /// <summary>
        /// 控制输入框是否可用
        /// </summary>
        /// <param name="blTorF"></param>
        private void OperateControl(bool blTorF)
        {
            CboName.IsEnabled = blTorF;
            CboBankName.IsEnabled = blTorF;
            TbBankNo.IsEnabled = blTorF;
            TbMobilePhone.IsEnabled = blTorF;
        }

        #endregion

        #region 按钮事件

        /// <summary>
        /// 更改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnModify_Click(object sender, RoutedEventArgs e)
        {
            if (_BankInfo != null)
            {
                OperateControl(true);
            }
        }

        /// <summary>
        /// 注销
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnLogout_Click(object sender, RoutedEventArgs e)
        {
            if (_BankInfo == null)
            {
                MessageBox.Show("该案件无授权信息！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            System.Windows.Forms.DialogResult dialogResult = (System.Windows.Forms.DialogResult)MessageBox.Show("您真的要注销本次授权吗？", "提示", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (dialogResult == System.Windows.Forms.DialogResult.Yes)
            {
                OperationResult result = AccountServiceProxy.DeleteAuthedBankCard(_BankInfo.SysNo, LoginHelper.CurrentUser.SysNo);
                if (result.Success)
                {
                    MessageBox.Show("注销本次授权成功！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    PageHelper.PageNavigateHelper(this, new BankAuthenticationSearch());
                }
                else
                {
                    MessageBox.Show("注销本次授权失败！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
            }
            else
            {
                return;
            }
        }

        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, new BankAuthenticationSearch());
        }

        /// <summary>
        /// 确认
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnConfirm_Click(object sender, RoutedEventArgs e)
        {
            //姓名
            string name = CboName.Text.ToString().Trim();
            //银行名称
            string bankName = CboBankName.SelectedValue.ToString();
            //银行卡号
            string bankNo = TbBankNo.Text.ToString().Trim();
            //银行预留手机号
            string mobilePhone = TbMobilePhone.Text.ToString().Trim();

            #region 验证空
            if (CboBankName.SelectedValue == null)
            {
                return;
            }
            if (string.IsNullOrEmpty(name))
            {
                MessageBox.Show("请选择买家姓名", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (string.IsNullOrEmpty(bankName))
            {
                MessageBox.Show("请选择银行名称", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (string.IsNullOrEmpty(bankNo))
            {
                MessageBox.Show("请输入银行卡号", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (string.IsNullOrEmpty(mobilePhone))
            {
                MessageBox.Show("请输入银行预留手机号", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            #endregion
            OperationResult result = AccountServiceProxy.AuthenticateBankCard(TbCaseId.Text.Trim(), bankName, bankNo.Replace(" ", "").ToString(),
                name, TbIDCardNo.Text, mobilePhone, LoginHelper.CurrentUser.SysNo);
            if (result.Success)
            {
                MessageBox.Show("添加银行卡授权成功！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                PageHelper.PageNavigateHelper(this, new BankAuthenticationSearch());
            }
            else
            {
                MessageBox.Show("添加银行卡授权失败！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        #endregion

        #region 按钮操作

        /// <summary>
        /// 取消按钮操作
        /// </summary>
        private void OperateCancel()
        {
            PageHelper.PageNavigateHelper(this, new BankAuthenticationSearch());
        }

        /// <summary>
        /// 确认按钮操作
        /// </summary>
        private void OperateConfirm()
        {
            //银行预留手机号
            string mobilePhone = TbMobilePhone.Text.ToString().Trim();

            #region 验证空
            if (CboBankName.SelectedValue == null)
            {
                return;
            }
            if (string.IsNullOrEmpty(mobilePhone))
            {
                MessageBox.Show("银行预留手机号不能为空！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            #endregion
            OperationResult result = AccountServiceProxy.AuthenticateBankCard(TbCaseId.Text.Trim(), CboBankName.SelectedValue.ToString(), TbBankNo.Text.Trim().Replace(" ", "").ToString(),
                CboName.SelectedValue.ToString(), TbIDCardNo.Text, mobilePhone, LoginHelper.CurrentUser.SysNo);
            if (result.Success)
            {
                MessageBox.Show("添加银行卡鉴权成功！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("添加银行卡鉴权失败！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        /// <summary>
        /// 注销按钮操作
        /// </summary>
        private void OperateLogout()
        {
            OperationResult result = AccountServiceProxy.DeleteAuthedBankCard(0, LoginHelper.CurrentUser.SysNo);
            if (result.Success)
            {
                MessageBox.Show("注销银行卡成功！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("注销银行卡失败！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        /// <summary>
        /// 更改按钮操纵
        /// </summary>
        private void OperateModify()
        {
            OperateControl(true);
        }

        #endregion

        /// <summary>
        /// 姓名下拉框事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CboName.SelectedValue == null)
            {
                return;
            }
            long userNo = long.Parse(CboName.SelectedValue.ToString());
            if (_CaseInfo != null)
            {
                for (int i = 0; i < _CaseInfo.Buyers.Count; i++)
                {
                    if (_CaseInfo.Buyers[i].UserSysNo == userNo)
                    {
                        TbIDCardNo.Text = _CaseInfo.Buyers[i].IdentityNo.ToString();
                    }
                }
            }
        }

        /// <summary>
        /// 银行卡号文本框字体颜色设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TbBankNo_GotFocus(object sender, RoutedEventArgs e)
        {
            //TbBankNo.Text = "";
            //TbBankNo.Foreground = Brushes.Black;
        }

       

    }
}
