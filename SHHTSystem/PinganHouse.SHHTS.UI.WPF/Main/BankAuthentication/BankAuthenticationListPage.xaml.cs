﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Model;
using System.Collections;
using PinganHouse.SHHTS.UI.WPF.Common;

namespace PinganHouse.SHHTS.UI.WPF.Main.BankAuthentication
{
    /// <summary>
    /// BankAuthenticationListPage.xaml 的交互逻辑
    /// </summary>
    public partial class BankAuthenticationListPage : Page
    {
        #region 变量

        //案件编号
        string caseId = "";
        //姓名
        string name = "";
        //身份证号
        string iDCardNo = "";
        //银行名称
        string bankName = "";
        //银行卡号
        string bankNo = "";
        //银行预留手机号
        string mobilePhone = "";

        int totalCount = 0;
        #endregion

        public BankAuthenticationListPage()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            IDictionary iDict = ConfigServiceProxy.GetIdentifyBanks();

            CboBankName.ItemsSource = iDict;
            CboBankName.DisplayMemberPath = "Value";
            CboBankName.SelectedValuePath = "Key";
        }

        /// <summary>
        /// 鉴权
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAuthentication_Click(object sender, RoutedEventArgs e)
        {
            //案件编号
            caseId = TbCaseId.Text.ToString().Trim();
            //姓名
            name = TbName.Text.ToString().Trim();
            //身份证号
            iDCardNo = TbIDCardNo.Text.ToString().Trim();
            //银行名称
            bankName = CboBankName.SelectedValue.ToString().Trim();
            //银行卡号
            bankNo = TbBankNo.Text.ToString().Trim();
            //银行预留手机号
            mobilePhone = TbMobilePhone.Text.ToString().Trim();

            #region 验证空
            if (string.IsNullOrEmpty(caseId))
            {
                MessageBox.Show("案件编号不能为空！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (string.IsNullOrEmpty(name))
            {
                MessageBox.Show("姓名不能为空！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (string.IsNullOrEmpty(iDCardNo))
            {
                MessageBox.Show("身份证号不能为空！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (string.IsNullOrEmpty(bankNo) && string.IsNullOrEmpty(bankName))
            {
                MessageBox.Show("银行信息不能为空！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (string.IsNullOrEmpty(mobilePhone))
            {
                MessageBox.Show("银行预留手机号不能为空！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            #endregion
            OperationResult result = AccountServiceProxy.AuthenticateBankCard(caseId, bankName, bankNo, name, iDCardNo, mobilePhone, LoginHelper.CurrentUser.SysNo);
            if (result.Success)
            {
                MessageBox.Show("添加银行卡鉴权成功！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
                //AuthenticationResultShow aResultShow = new AuthenticationResultShow();
                //aResultShow.Closing += aResultShow_Closing;
                //aResultShow.ShowDialog();
            }

        }

        void aResultShow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //IList<AuthedBankCard> _listBank = AccountServiceProxy.GetAuthedBankCards(1, 9999, out totalCount, caseId, bankNo, bankName, iDCardNo, mobilePhone);
            //this.dataGrid.DataContext = _listBank;
        }

        private void TbCaseId_TextChanged(object sender, TextChangedEventArgs e)
        {
            //LbCaseId.Items.Clear(); //清空一下这个控件的值
            //int totalCount = 0;
            //IList<string> listCaseId = CaseProxyService.GetInstanse().GetCaseIds(9999, 10, out totalCount, ConfigHelper.GetCurrentReceptionCenter(), TbCaseId.Text.Trim());
            //for (int i = 0; i < listCaseId.Count; i++)//循环所有行数
            //{
            //    LbCaseId.Items.Add(listCaseId[i].ToString());//每行的名称值给listBox
            //}
        }

    }
}
