﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.BankAuthentication
{
    /// <summary>
    /// BankAuthenticationSearch.xaml 的交互逻辑
    /// </summary>
    public partial class BankAuthenticationSearch : Page
    {
        public BankAuthenticationSearch()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 搜索按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSearch_Click(object sender, RoutedEventArgs e)
        {
            CaseWithCustomer caseInfo = CaseProxyService.GetInstanse().GetCaseWithCustomer(TbCaseId.Text.Trim(), ConfigHelper.GetCurrentReceptionCenter());

            AuthedBankCard bankInfo = AccountServiceProxy.GetAuthedBankCardByCase(TbCaseId.Text.Trim());

            //IList<Customer> listCustomer = CustomerServiceProxy.GetCustomerByCase(TbCaseId.Text.Trim());
            if (caseInfo != null)
            {
                LblErrorInfo.Content = "";
                PageHelper.PageNavigateHelper(this, new BankAuthenticationInfo(caseInfo, bankInfo));
            }
            else
            {
                LblErrorInfo.Content = "该案件编号不存在，请重新输入";
                TbCaseId.Focus();
                return;
            }
        }

    }
}
