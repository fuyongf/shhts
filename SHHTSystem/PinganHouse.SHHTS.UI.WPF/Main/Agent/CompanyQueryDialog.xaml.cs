﻿using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Agent
{
    /// <summary>
    /// CompanyQueryDialog.xaml 的交互逻辑
    /// </summary>
    public partial class CompanyQueryDialog : Window
    {
        private PinganHouse.SHHTS.DataTransferObjects.AgentCompany _AgentCompany;

        public CompanyQueryDialog(PinganHouse.SHHTS.DataTransferObjects.AgentCompany agentCompany)
        {
            InitializeComponent();

            if (agentCompany == null)
                throw new ArgumentNullException("AgentCompany");

            _AgentCompany = agentCompany;
        }


        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                IList<PinganHouse.SHHTS.DataTransferObjects.AgentCompany> agentCompanyList = new List<PinganHouse.SHHTS.DataTransferObjects.AgentCompany>();
                int totalCount = 0;
                //查询条件
                var condition = string.IsNullOrEmpty(TbCondition.Text.Trim()) ? null : TbCondition.Text.Trim();

                agentCompanyList = AgentServiceProxy.GetAgentCompanyList(condition, AgentJointStatus.Jointing, 1, 1000000, out totalCount);

                if (agentCompanyList != null && agentCompanyList.Count > 0)
                {
                    DgCompanyList.DataContext = agentCompanyList;
                }
                else
                {
                    DgCompanyList.DataContext = null;
                }
                //var agentList = AgentServiceProxy.GetAgentStaffs(1, 10000, out totalCount, null, null, condition);
                //if (agentList != null)
                //{
                //    agentStaffList = InitData(agentList);
                //}

                //DgAgentList.DataContext = agentStaffList;

            }
            catch (Exception ex)
            {
                MessageBox.Show("加载数据发生错误：" + ex.ToString());
            }
        }

        /// <summary>
        /// 确认
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnConfirm_Click(object sender, RoutedEventArgs e)
        {
            if (DgCompanyList.SelectedItems.Count > 0)
            {
                _AgentCompany.SysNo = ((DataTransferObjects.AgentCompany)DgCompanyList.SelectedValue).SysNo;
                _AgentCompany.CompanyName = ((PinganHouse.SHHTS.DataTransferObjects.AgentCompany)DgCompanyList.SelectedValue).CompanyName;
                _AgentCompany.Address = ((PinganHouse.SHHTS.DataTransferObjects.AgentCompany)DgCompanyList.SelectedValue).Address;

                Close();
            }
            else
            {
                MessageBox.Show("请选择一行数据");
                return;
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// 双击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DgCompanyList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (DgCompanyList.SelectedItems.Count > 0)
            {
                _AgentCompany.SysNo = ((DataTransferObjects.AgentCompany)DgCompanyList.SelectedValue).SysNo;
                _AgentCompany.CompanyName = ((PinganHouse.SHHTS.DataTransferObjects.AgentCompany)DgCompanyList.SelectedValue).CompanyName;
                _AgentCompany.Address = ((PinganHouse.SHHTS.DataTransferObjects.AgentCompany)DgCompanyList.SelectedValue).Address;

                Close();
            }
            else
            {
                return;
            }
        }

    }
}
