﻿using PinganHouse.SHHTS.RemoteServiceProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using PinganHouse.SHHTS.UI.WPF.Common;

namespace PinganHouse.SHHTS.UI.WPF.Main.Agent
{
    /// <summary>
    /// AgentStaff.xaml 的交互逻辑
    /// </summary>
    public partial class AgentStaff : Window
    {
        private PinganHouse.SHHTS.DataTransferObjects.AgentCompany _AgentCompany = new DataTransferObjects.AgentCompany();

        public AgentStaff()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //int totalCount = 0;
            //var company = AgentServiceProxy.GetAgentCompanyList(null, null, 1, 100000, out totalCount);
            //if (company == null)
            //    return;
            //cbCompany.ItemsSource = company;
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            #region 判空
            //姓名验证
            if (string.IsNullOrEmpty(txtName.Text.Trim()))
            {
                MessageBox.Show("姓名不能为空！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                txtName.Focus();
                return;
            }
            //手机号验证
            if (string.IsNullOrEmpty(txtMobile.Text.Trim()) && string.IsNullOrEmpty(txtMobile1.Text.Trim()))
            {
                MessageBox.Show("手机号不能为空！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                txtMobile.Focus();
                return;
            }
            //所属公司验证
            if (_AgentCompany.SysNo == 0)
            {
                MessageBox.Show("所属公司不能为空！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                BtnSearch.Focus();
                return;
            }
            //身份证号验证
            if (!string.IsNullOrEmpty(txtIdNo.Text) && !ValidationCommon.IsIdMatch(txtIdNo.Text))
            {
                MessageBox.Show("身份证号不正确！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                txtIdNo.Focus();
                return;
            }
            #endregion
            //if (_AgentCompany.SysNo == 0) { return; }
            var result = AgentServiceProxy.CreateAgentStaff(new AgentStaffModel
            {
                Name = txtName.Text.Trim(),
                Mobile = txtMobile.Text.Trim(),
                Mobile1 = txtMobile1.Text.Trim(),
                IdentityNo = txtIdNo.Text.Trim(),
                AgentCompanySysNo = _AgentCompany.SysNo,
                LoginName = txtLoginName.Text.Trim(),
                Password = txtPassword.Text.Trim(),
                IsManager = (bool)cbIsManager.IsChecked
            }, 0);
            if (result.Success)
            {
                MessageBox.Show("添加成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                txtName.Text = string.Empty;
                txtMobile.Text = string.Empty;
                txtIdNo.Text = string.Empty;
                txtMobile1.Text = string.Empty;
                txtLoginName.Text = string.Empty;
                txtPassword.Text = string.Empty;
                tbCompanyName.Text = string.Empty;
                _AgentCompany.SysNo = 0;
            }
            else
                MessageBox.Show(result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 搜索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSearch_Click(object sender, RoutedEventArgs e)
        {
            CompanyQueryDialog companyQueryDialog = new CompanyQueryDialog(_AgentCompany);
            companyQueryDialog.Closing += companyQueryDialog_Closing;
            companyQueryDialog.ShowDialog();
        }

        void companyQueryDialog_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            tbCompanyName.Text = _AgentCompany.CompanyName;
        }

    }
}
