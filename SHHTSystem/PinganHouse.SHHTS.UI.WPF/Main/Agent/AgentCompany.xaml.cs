﻿using PinganHouse.SHHTS.RemoteServiceProxy;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.Agent
{
    /// <summary>
    /// AgentCompany.xaml 的交互逻辑
    /// </summary>
    public partial class AgentCompany : Window
    {
        public AgentCompany()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 窗体加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IDictionary iDict = ConfigServiceProxy.GetBanks();
            if (iDict.Count > 0)
            {
                cbReceiveBank.ItemsSource = iDict;
                cbReceiveBank.DisplayMemberPath = "Value";
                cbReceiveBank.SelectedValuePath = "Key";
            }
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            #region 判空
            //中介公司名称验证
            if (string.IsNullOrEmpty(txtCompanyName.Text.Trim()))
            {
                MessageBox.Show("中介公司名称不能为空！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                txtCompanyName.Focus();
                return;
            }
            //负责人验证
            if (string.IsNullOrEmpty(txtPrincipal.Text.Trim()))
            {
                MessageBox.Show("负责人不能为空！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                txtPrincipal.Focus();
                return;
            }
            //联系方式验证
            if (string.IsNullOrEmpty(txtContactInfo.Text.Trim()))
            {
                MessageBox.Show("联系方式不能为空！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                txtContactInfo.Focus();
                return;
            }
            //地址验证
            if (string.IsNullOrEmpty(txtAddress.Text.Trim()))
            {
                MessageBox.Show("地址不能为空！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                txtAddress.Focus();
                return;
            }
            #endregion
            //if (cbReceiveBank.SelectedValue == null) { return; } 
            var reusult = AgentServiceProxy.AgentJoint(new AgentCompanyModel
            {
                CompanyName = txtCompanyName.Text.Trim(),
                Principal = txtPrincipal.Text.Trim(),
                Address = txtAddress.Text.Trim(),
                ContactInfo = txtContactInfo.Text.Trim(),
                JointContractNo = txtContractNo.Text.Trim(),
                ReceiveBank = cbReceiveBank.SelectedValue.ToString(),
                ReceiveSubBank = txtReceiveSubBank.Text.Trim(),
                ReceiveName = txtReceiveName.Text.Trim(),
                ReceiveAccount = txtReceiveAccount.Text.Trim()
            }, 0);
            if (reusult.Success)
            {
                MessageBox.Show("添加成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                txtCompanyName.Text = string.Empty;
                txtPrincipal.Text = string.Empty;
                txtAddress.Text = string.Empty;
                txtContractNo.Text = string.Empty;
                txtContactInfo.Text = string.Empty;
                cbReceiveBank.SelectedValue = string.Empty;
                txtReceiveSubBank.Text = string.Empty;
                txtReceiveName.Text = string.Empty;
                txtReceiveAccount.Text = string.Empty;

            }
            else
                MessageBox.Show(reusult.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

    }
}
