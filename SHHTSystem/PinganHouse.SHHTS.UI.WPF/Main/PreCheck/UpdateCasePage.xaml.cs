﻿using System.IO;
using System.Linq;
using System.Windows.Forms.VisualStyles;
using System.Windows.Media;
using FSLib.App.SimpleUpdater.Defination;
using HTB.DevFx.Data;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using PinganHouse.SHHTS.UI.WPF.ReadCard;
using PinganHouse.SHHTS.UI.WPF.Control;

namespace PinganHouse.SHHTS.UI.WPF.Main.PreCheck
{
    /// <summary>
    /// UpdateCasePage.xaml 的交互逻辑
    /// </summary>
    public partial class UpdateCasePage : Page
    {
        private const string sCaseInfo =
            "案件编号：{0}  |  立案日期：{1}  |  预检：{2}  |  接单时间：{3}  |  核案：{4}  | 签约完成时间：{5}  |   最后修改时间：{6}  |  修改人：{7}";

        #region 全局变量
        /// <summary>
        /// 页面案件对象
        /// </summary>
        private CaseDto _CaseDto;
        /// <summary>
        /// 案件编号
        /// </summary>
        private string _caseId;
        /// <summary>
        /// 预检台状态
        /// </summary>
        CaseStatus _status = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.ZB2;
        #endregion

        #region
        /// <summary>
        /// 买方姓名集合
        /// </summary>
        private Dictionary<long, string> _buyerNameDic = new Dictionary<long, string>();


        /// <summary>
        /// 卖家姓名集合
        /// </summary>
        private Dictionary<long, string> _sellerNameDic = new Dictionary<long, string>();


        private IDictionary<long, string> _buyerIdentityNoIDic = new Dictionary<long, string>();
        private IDictionary<long, string> _sellerIdentityNoIDic = new Dictionary<long, string>();

        #endregion

        #region 声明


        private Dictionary<long, UserIdentity> icardInfoDic = new Dictionary<long, UserIdentity>();

        /// <summary>
        /// 买方操作状态 false:添加 true:修改
        /// </summary>
        private bool _operationStatusBuyer;

        /// <summary>
        /// 卖家操作状态 false:添加 true:修改
        /// </summary>
        private bool _operationStatusSeller;

        /// <summary>
        /// 买方编号
        /// </summary>
        private long _buyerUserSysNo;
        /// <summary>
        /// 卖家编号
        /// </summary>
        private long _sellerUserSysNo;

        #endregion

        private AgentStaff _agentStaff = new AgentStaff();

        #region 身份证信息

        /// <summary>
        /// 身份证信息对象
        /// </summary>
        private UserIdentityModel IdentityInfo = new UserIdentityModel();
        /// <summary>
        /// 读取身份证人的类型
        /// </summary>
        private enum IdCardRead
        {
            /// <summary>
            /// 未知
            /// </summary>
            Unkonw,
            /// <summary>
            /// 买方
            /// </summary>
            Buyer,
            /// <summary>
            /// 卖家
            /// </summary>
            Seller,
            /// <summary>
            /// 经纪人
            /// </summary>
            Agent
        }
        ///<summary>
        /// 读取身份证人
        /// </summary>
        private int _idCardReadType = 0;

        #endregion

        public UpdateCasePage(string caseId)
        {
            InitializeComponent();

            BtnBuyerRevoke.Visibility = Visibility.Collapsed;
            BtnSellerRevoke.Visibility = Visibility.Collapsed;

            _caseId = caseId;

            #region 绑定证件类别

            List<EnumHelper> listIdentity = EnumHelper.InitCertificateTypeToCombobox();
            CboBuyerIdentity.ItemsSource = listIdentity;
            CboBuyerIdentity.DisplayMemberPath = "DisplayMember";
            CboBuyerIdentity.SelectedValuePath = "ValueMember";
            CboBuyerIdentity.SelectedValue = 0;

            CboSellerIdentity.ItemsSource = listIdentity;
            CboSellerIdentity.DisplayMemberPath = "DisplayMember";
            CboSellerIdentity.SelectedValuePath = "ValueMember";
            CboSellerIdentity.SelectedValue = 0;

            #endregion

            #region 绑定民族

            List<EnumHelper> listNation = EnumHelper.InitNationToCombobox();
            CertificateOfOfficersBuyer.TbNation.ItemsSource = listNation;
            CertificateOfOfficersBuyer.TbNation.DisplayMemberPath = "DisplayMember";
            CertificateOfOfficersBuyer.TbNation.SelectedValuePath = "ValueMember";

            CertificateOfOfficersSeller.TbNation.ItemsSource = listNation;
            CertificateOfOfficersSeller.TbNation.DisplayMemberPath = "DisplayMember";
            CertificateOfOfficersSeller.TbNation.SelectedValuePath = "ValueMember";

            ResidenceBookletBuyer.TbNation.ItemsSource = listNation;
            ResidenceBookletBuyer.TbNation.DisplayMemberPath = "DisplayMember";
            ResidenceBookletBuyer.TbNation.SelectedValuePath = "ValueMember";

            ResidenceBookletSeller.TbNation.ItemsSource = listNation;
            ResidenceBookletSeller.TbNation.DisplayMemberPath = "DisplayMember";
            ResidenceBookletSeller.TbNation.SelectedValuePath = "ValueMember";

            #endregion

            CboBuyerIdentity.SelectionChanged += CboBuyerIdentity_SelectionChanged;
            CboSellerIdentity.SelectionChanged += CboSellerIdentity_SelectionChanged;

            ctrl_Navigation.OperationEvent += ctrl_Navigation_OperationEvent;
            ctrl_Navigation.UploadEvent += ctrl_Navigation_UploadEvent;
            ctrl_Navigation.AttachmentEvent += ctrl_Navigation_AttachmentEvent;
        }

        #region 自定义导航事件
        /// <summary>
        /// 操作信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ctrl_Navigation_OperationEvent(object sender, EventArgs e)
        {
            var status = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.ZB2;
            var attachments = new OperationInfos(_caseId, status, 2); //detailsFlag：0，核案；1，查看预检；2，修改预检。
            PageHelper.PageNavigateHelper(this, attachments);
        }
        /// <summary>
        /// 上传页面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ctrl_Navigation_UploadEvent(object sender, EventArgs e)
        {
            var attachments = new UploadAttachment(_caseId, 2); //detailsFlag：0，核案；1，查看预检；2，修改预检。
            PageHelper.PageNavigateHelper(this, attachments);
        }
        void ctrl_Navigation_AttachmentEvent(object sender, EventArgs e)
        {
            ctrl_Navigation.btnAttachment.Background = new SolidColorBrush(Color.FromArgb(100, 10, 10, 250));
            //var attachments = new CaseAttachmentsInfo(_caseId, 2); //detailsFlag：0，核案；1，查看预检；2，修改预检。
            //PageHelper.PageNavigateHelper(this, attachments);
        }
        #endregion


        /// <summary>
        /// 窗体加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(_caseId))
                {
                    var status = CaseStatus.HA1 | CaseStatus.HA2;
                    var dicCaseStatus = new Dictionary<CaseStatus, Tuple<string, DateTime>>();
                    _CaseDto = CaseProxyService.GetInstanse().GetCaseAndProcess(_caseId, status, ref dicCaseStatus);
                    //_CaseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(_caseId);
                    if (_CaseDto != null)
                    {
                        string checkCase = ""; //核案
                        DateTime? checkCaseDate = null; //接单时间
                        DateTime? signingDate = null; //签约完成时间
                        //背景色 橙色
                        Color backgroundColor = (Color)ColorConverter.ConvertFromString("#B5F05A22");

                        #region 进度显示

                        if (dicCaseStatus != null && dicCaseStatus.Count > 0)
                        {
                            if (dicCaseStatus.ContainsKey(CaseStatus.HA1))
                            {
                                checkCase = dicCaseStatus[CaseStatus.HA1].Item1;
                                checkCaseDate = dicCaseStatus[CaseStatus.HA1].Item2;

                                //设置进度显示背景色为橙色
                                BtnCheckCase.Background = new SolidColorBrush(backgroundColor);
                                //设置进度显示图片为可见
                                ImgCheckCaseTime.Visibility = Visibility.Visible;
                                //核案接单用时
                                TimeSpan ts = (TimeSpan)(checkCaseDate - _CaseDto.CreateDate);
                                TblCheckCaseTime.Text = ts.Days.ToString() + "天" + ts.Hours.ToString() + "时" +
                                                        ts.Minutes.ToString() + "分" + ts.Seconds.ToString() + "秒";
                                //TblCheckCaseTime.Text = (checkCaseDate - _CaseDto.CreateDate).ToString();//"yyyy时MM分dd秒"
                            }
                            if (dicCaseStatus.ContainsKey(CaseStatus.HA2))
                            {
                                signingDate = dicCaseStatus[CaseStatus.HA2].Item2;

                                //设置进度显示背景色为橙色
                                BtnSigned.Background = new SolidColorBrush(backgroundColor);
                                //设置进度显示图片为可见
                                ImgSigningTime.Visibility = Visibility.Visible;
                                //签约完成用时
                                TimeSpan ts = (TimeSpan)(signingDate - checkCaseDate);
                                TblSigningTime.Text = ts.Days.ToString() + "天" + ts.Hours.ToString() + "时" +
                                                      ts.Minutes.ToString() + "分" + ts.Seconds.ToString() + "秒";
                                //TblSigningTime.Text = (signingDate - checkCaseDate).ToString();

                                #region 保存按钮不可用

                                BtnBuyerSave.IsEnabled = false;
                                BtnSellerSave.IsEnabled = false;
                                BtnSaveUpdate.IsEnabled = false;

                                #endregion
                            }

                        }

                        #endregion

                        //TblCaseInfo.Text = string.Format(sCaseInfo, _CaseDto.CaseId, _CaseDto.CreateDate,
                        //    _CaseDto.CreatorName, checkCaseDate, checkCase, signingDate, _CaseDto.ModifyDate,
                        //    _CaseDto.ModifyUserName);
                        //TblCaseInfo.Text = "案件编号："+_CaseDto.CaseId;  

                        TblCaseInfo.Text = string.Format(sCaseInfo, _CaseDto.CaseId, _CaseDto.CreateDate,
                            string.IsNullOrWhiteSpace(_CaseDto.CreatorName) ? "---" : _CaseDto.CreatorName,
                            checkCaseDate != null ? checkCaseDate.Value.ToString() : "00:00:00",
                            string.IsNullOrWhiteSpace(checkCase) ? "---" : checkCase,
                            signingDate != null ? signingDate.Value.ToString() : "00:00:00",
                            _CaseDto.ModifyDate != null ? _CaseDto.ModifyDate.Value.ToString() : "00:00:00",
                            string.IsNullOrWhiteSpace(_CaseDto.ModifyUserName) ? "---" : _CaseDto.ModifyUserName);
                        TblCaseSource.Text = string.Format("案件来源：{0}", EnumHelper.GetEnumDesc(_CaseDto.SourceType));
  
                        InitData();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("加载信息出错：" + ex.Message);
            }
        }

        #region 初始时需加载的数据
        /// <summary>
        /// 加载案件基础数据
        /// </summary>
        private void InitData()
        {
            try
            {
                #region 案件
                //产权证号
                if (_CaseDto.TenementContract != null)
                {
                    var temps = _CaseDto.TenementContract.Split('-');
                    if (temps.Length > 1)
                    {
                        TbTenementContractWord.Text = temps[0];
                        TbTenementContract.Text = temps[1];
                    }
                    else
                    {
                        TbTenementContract.Text = temps[0];
                    }
                }
                //房屋坐落
                if (_CaseDto.TenementAddress != null)
                    TbTenementAddress.Text = _CaseDto.TenementAddress;
                #endregion

                #region 经纪人
                _agentStaff = AgentServiceProxy.GetAgentSatff(_CaseDto.AgencySysNo);
                TblShowInfo.Text = "姓名：" + _CaseDto.AgencyName + "                 所属门店：" + _CaseDto.CompanyName;

                #endregion

                #region 买方
                _buyerNameDic = _CaseDto.BuyerNameDic;
                _buyerIdentityNoIDic = CustomerServiceProxy.GetUserIdentityNoByCustomerSysNos(_CaseDto.Buyers.ToArray());
                BindData(LbBuyerList, _buyerNameDic);
                #endregion

                #region 卖家
                _sellerNameDic = _CaseDto.SellerNameDic;
                _sellerIdentityNoIDic = CustomerServiceProxy.GetUserIdentityNoByCustomerSysNos(_CaseDto.Sellers.ToArray());
                BindData(LbSellerList, _sellerNameDic);
                #endregion

                //Todo:绑定列表

            }
            catch (Exception ex)
            {
                MessageBox.Show("加载数据出错：" + ex.Message);
            }
        }

        #endregion


        #region 调用方法

        /// <summary>
        /// 保存买卖家信息
        /// </summary>     
        /// <param name="customerType">类型</param>        
        private OperationResult AddUserIdentity(UserIdentityModel userIdentity, CustomerType customerType)
        {
            OperationResult result = null;
            try
            {
                switch (customerType)
                {
                    case CustomerType.Buyer:
                        result = CustomerServiceProxy.CreateBuyer(userIdentity, LoginHelper.CurrentUser.SysNo);
                        break;
                    case CustomerType.Seller:
                        result = CustomerServiceProxy.CreateSeller(userIdentity, LoginHelper.CurrentUser.SysNo);
                        break;
                }
                return result;
            }
            catch (Exception ex)
            {
                result.ResultMessage = ex.Message;
                return result;
            }
        }

        /// <summary>
        /// 增加新的案件
        /// </summary>
        /// <param name="caseDto"></param>
        /// <returns></returns>
        private OperationResult AddNewCase(CaseDto caseDto)
        {
            OperationResult result = null;
            try
            {
                caseDto.ReceptionCenter = ConfigHelper.GetCurrentReceptionCenter();
                result = CaseProxyService.GetInstanse().GenerateCase(caseDto);
                return result;
            }
            catch (Exception ex)
            {
                result.ResultMessage = ex.Message;
                return result;
            }
        }

        #endregion

        #region 清空

        /// <summary>
        /// 清空各证件类型数据
        /// </summary>
        private void ClearBuyerIDType()
        {
            ClearIdCard(IdCardBuyer);
            ClearPassport(PassportBuyer);
            ClearHKMacaoIdCard(HKMacaoIdCardBuyer);
            ClearBirthCertificate(BirthCertificateBuyer);
            ClearMacaoPermit(MacaoPermitBuyer);
            ClearTaiwanPermit(TaiwanPermitBuyer);
            ClearCertificateOfOfficers(CertificateOfOfficersBuyer);
            ClearResidenceBooklet(ResidenceBookletBuyer);
        }

        /// <summary>
        /// 清空各证件类型数据
        /// </summary>
        private void ClearSellerIDType()
        {
            ClearIdCard(IdCardSeller);
            ClearPassport(PassportSeller);
            ClearHKMacaoIdCard(HKMacaoIdCardSeller);
            ClearBirthCertificate(BirthCertificateSeller);
            ClearMacaoPermit(MacaoPermitSeller);
            ClearTaiwanPermit(TaiwanPermitSeller);
            ClearCertificateOfOfficers(CertificateOfOfficersSeller);
            ClearResidenceBooklet(ResidenceBookletSeller);
        }

        /// <summary>
        /// 清空身份证信息
        /// </summary>
        /// <param name="idCard"></param>
        private void ClearIdCard(IdCardControl idCard)
        {
            try
            {
                idCard.TblName.Text = string.Empty;
                idCard.TblGender.Text = string.Empty;
                idCard.TblNation.Text = string.Empty;
                idCard.TblBirthday.Text = string.Empty;
                idCard.TblAddress.Text = string.Empty;
                idCard.TblIdentityNo.Text = string.Empty;
                idCard.TblIdentityNo.Tag = null;//一定要清空 记录每个身份证件的唯一标识
                idCard.TblVisaAgency.Text = string.Empty;
                idCard.TblEffectiveDate.Text = string.Empty;
                idCard.TblExpiryDate.Text = string.Empty;
                idCard.TblStr.Text = string.Empty;
                idCard.ImgPhoto.Source = null;
            }
            catch (Exception e)
            {
                MessageBox.Show("重置数据发生异常：" + e.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 清空护照信息
        /// </summary>
        /// <param name="passport"></param>
        private void ClearPassport(PassportControl passport)
        {
            try
            {
                passport.TbName.Text = string.Empty;
                passport.RdobtnMale.IsChecked = false;
                passport.RdobtnFemale.IsChecked = false;
                passport.DateBirthday.Text = string.Empty;
                passport.CboNationality.SelectedValue = -1;
                passport.TbIdentityNo.Text = string.Empty;
                passport.TbIdentityNo.Tag = null;//一定要清空 记录每个身份证件的唯一标识
            }
            catch (Exception e)
            {
                MessageBox.Show("重置数据发生异常：" + e.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 清空港澳居民身份证信息
        /// </summary>
        /// <param name="hKMacaoIdCard"></param>
        private void ClearHKMacaoIdCard(HKMacaoIdCardControl hKMacaoIdCard)
        {
            try
            {
                hKMacaoIdCard.TbName.Text = string.Empty;
                hKMacaoIdCard.RdobtnMale.IsChecked = false;
                hKMacaoIdCard.RdobtnFemale.IsChecked = false;
                hKMacaoIdCard.DateBirthday.Text = string.Empty;
                hKMacaoIdCard.TbIdentityNo.Text = string.Empty;
                hKMacaoIdCard.TbIdentityNo.Tag = null;//一定要清空 记录每个身份证件的唯一标识
                hKMacaoIdCard.DateVisaAgency.Text = string.Empty;
            }
            catch (Exception e)
            {
                MessageBox.Show("重置数据发生异常：" + e.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 清空出生证信息
        /// </summary>
        /// <param name="birthCertificate"></param>
        private void ClearBirthCertificate(BirthCertificateControl birthCertificate)
        {
            try
            {
                birthCertificate.TbName.Text = string.Empty;
                birthCertificate.RdobtnMale.IsChecked = false;
                birthCertificate.RdobtnFemale.IsChecked = false;
                birthCertificate.DateBirthday.Text = string.Empty;
                birthCertificate.TbIdentityNo.Text = string.Empty;
                birthCertificate.TbIdentityNo.Tag = null;//一定要清空 记录每个身份证件的唯一标识
                birthCertificate.TbVisaAgency.Text = string.Empty;
            }
            catch (Exception e)
            {
                MessageBox.Show("重置数据发生异常：" + e.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 清空港澳居民往来大陆通行证信息
        /// </summary>
        /// <param name="macaoPermit"></param>
        private void ClearMacaoPermit(MacaoPermitControl macaoPermit)
        {
            try
            {
                macaoPermit.TbName.Text = string.Empty;
                macaoPermit.RdobtnMale.IsChecked = false;
                macaoPermit.RdobtnFemale.IsChecked = false;
                macaoPermit.DateBirthday.Text = string.Empty;
                macaoPermit.TbIdentityNo.Text = string.Empty;
                macaoPermit.TbIdentityNo.Tag = null;//一定要清空 记录每个身份证件的唯一标识
                macaoPermit.TbVisaAgency.Text = string.Empty;
                macaoPermit.TbEffectiveDate.Text = string.Empty;
                macaoPermit.TbExpiryDate.Text = string.Empty;
            }
            catch (Exception e)
            {
                MessageBox.Show("重置数据发生异常：" + e.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 清空台湾居民往来大陆通行证信息
        /// </summary>
        /// <param name="taiwanPermit"></param>
        private void ClearTaiwanPermit(TaiwanPermitControl taiwanPermit)
        {
            try
            {
                taiwanPermit.TbName.Text = string.Empty;
                taiwanPermit.RdobtnMale.IsChecked = false;
                taiwanPermit.RdobtnFemale.IsChecked = false;
                taiwanPermit.DateBirthday.Text = string.Empty;
                taiwanPermit.TbIdentityNo.Text = string.Empty;
                taiwanPermit.TbIdentityNo.Tag = null;//一定要清空 记录每个身份证件的唯一标识
                taiwanPermit.TbVisaAgency.Text = string.Empty;
                taiwanPermit.TbEffectiveDate.Text = string.Empty;
                taiwanPermit.TbExpiryDate.Text = string.Empty;
            }
            catch (Exception e)
            {
                MessageBox.Show("重置数据发生异常：" + e.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 清空军官证信息
        /// </summary>
        /// <param name="certificateOfOfficers"></param>
        private void ClearCertificateOfOfficers(CertificateOfOfficersControl certificateOfOfficers)
        {
            try
            {
                certificateOfOfficers.TbName.Text = string.Empty;
                certificateOfOfficers.RdobtnMale.IsChecked = false;
                certificateOfOfficers.RdobtnFemale.IsChecked = false;
                certificateOfOfficers.DateBirthday.Text = string.Empty;
                certificateOfOfficers.TbNation.Text = string.Empty;
                certificateOfOfficers.TbDepartmental.Text = string.Empty;
                certificateOfOfficers.TbIdentityNo.Text = string.Empty;
                certificateOfOfficers.TbIdentityNo.Tag = null;//一定要清空 记录每个身份证件的唯一标识
                certificateOfOfficers.TbVisaAgency.Text = string.Empty;
                certificateOfOfficers.TbEffectiveDate.Text = string.Empty;
                certificateOfOfficers.TbExpiryDate.Text = string.Empty;
            }
            catch (Exception e)
            {
                MessageBox.Show("重置数据发生异常：" + e.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 清空户口簿
        /// </summary>
        /// <param name="residenceBooklet"></param>
        private void ClearResidenceBooklet(ResidenceBookletControl residenceBooklet)
        {
            try
            {
                residenceBooklet.TbName.Text = string.Empty;
                residenceBooklet.RdobtnMale.IsChecked = false;
                residenceBooklet.RdobtnFemale.IsChecked = false;
                residenceBooklet.DateBirthday.Text = string.Empty;
                residenceBooklet.TbNation.Text = string.Empty;
                residenceBooklet.TbIdentityNo.Text = string.Empty;
                residenceBooklet.TbIdentityNo.Tag = null;//一定要清空 记录每个身份证件的唯一标识
                residenceBooklet.TbAddress.Text = string.Empty;
            }
            catch (Exception e)
            {
                MessageBox.Show("重置数据发生异常：" + e.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #endregion

        #region 清空Old

        /// <summary>
        /// 清空买方身份证信息
        /// </summary>
        private void WipeDataBuyerIdCard()
        {
            TblBuyerName.Text = "";
            TblBuyerGender.Text = "";
            TblBuyerNation.Text = "";
            TblBuyerBirthday.Text = "";
            TblBuyerAddress.Text = "";
            TblBuyerIdentityNo.Text = "";
            TblBuyerVisaAgency.Text = "";
            TblBuyerEffectiveDate.Text = "";
            TblBuyerExpiryDate.Text = "";
            TblStr.Text = "";
            ImgBuyerPhoto.Source = null;
        }
        /// <summary>
        /// 清空买方护照信息
        /// </summary>
        private void WipeDataBuyerPassport()
        {
            TbBuyerName.Text = "";
            RdoBtnBuyerGenderMale.IsChecked = false;
            RdoBtnBuyerGenderFemale.IsChecked = false;
            TbBuyerBirthday.Text = "";
            CboBuyerNationality.SelectedValue = -1;
            TbBuyerIdentityNo.Text = "";
        }

        /// <summary>
        /// 清空卖家身份证信息
        /// </summary>
        private void WipeDataSellerIdCard()
        {
            TblSellerName.Text = "";
            TblSellerGender.Text = "";
            TblSellerNation.Text = "";
            TblSellerBirthday.Text = "";
            TblSellerAddress.Text = "";
            TblSellerIdentityNo.Text = "";
            TblSellerVisaAgency.Text = "";
            TblSellerEffectiveDate.Text = "";
            TblSellerExpiryDate.Text = "";
            TblSellerStr.Text = "";
            ImgSellerPhoto.Source = null;
        }
        /// <summary>
        /// 清空卖家护照信息
        /// </summary>
        private void WipeDataSellerPassport()
        {
            TbSellerName.Text = "";
            RdoBtnSellerGenderMale.IsChecked = false;
            RdoBtnSellerGenderFemale.IsChecked = false;
            TbSellerBirthday.Text = "";
            CboSellerNationality.SelectedValue = -1;
            TbSellerIdentityNo.Text = "";
        }

        #endregion

        #region 证件类型隐藏

        private void BuyerIDTypeCollapsed()
        {
            IdCardBuyer.Visibility = Visibility.Collapsed;
            PassportBuyer.Visibility = Visibility.Collapsed;
            HKMacaoIdCardBuyer.Visibility = Visibility.Collapsed;
            BirthCertificateBuyer.Visibility = Visibility.Collapsed;
            MacaoPermitBuyer.Visibility = Visibility.Collapsed;
            TaiwanPermitBuyer.Visibility = Visibility.Collapsed;
            CertificateOfOfficersBuyer.Visibility = Visibility.Collapsed;
            ResidenceBookletBuyer.Visibility = Visibility.Collapsed;
        }

        private void SellerIDTypeCollapsed()
        {
            IdCardSeller.Visibility = Visibility.Collapsed;
            PassportSeller.Visibility = Visibility.Collapsed;
            HKMacaoIdCardSeller.Visibility = Visibility.Collapsed;
            BirthCertificateSeller.Visibility = Visibility.Collapsed;
            MacaoPermitSeller.Visibility = Visibility.Collapsed;
            TaiwanPermitSeller.Visibility = Visibility.Collapsed;
            CertificateOfOfficersSeller.Visibility = Visibility.Collapsed;
            ResidenceBookletSeller.Visibility = Visibility.Collapsed;
        }

        #endregion

        /// <summary>
        /// 绑定数据源
        /// </summary>
        /// <param name="listBox"></param>
        /// <param name="source"></param>
        private void BindData(ListBox listBox, Dictionary<long, string> source)
        {
            listBox.ItemsSource = null;
            listBox.ItemsSource = source;
            listBox.DisplayMemberPath = "Value";
            listBox.SelectedValuePath = "Key";
            //listBox.SelectedIndex = 0;
        }

        #region 证件类型添加

        private UserIdentityModel AddIdCard(UserIdentityModel user, IdCardControl idCard)
        {
            #region 身份证
            user.CertificateType = CertificateType.IdCard;
            user.Name = idCard.TblName.Text; //姓名
            //性别
            if (!string.IsNullOrEmpty(idCard.TblGender.Text))
            {
                switch (idCard.TblGender.Text.Trim())
                {
                    case "男":
                        user.Gender = Gender.Male;
                        break;
                    case "女":
                        user.Gender = Gender.Female;
                        break;
                    default:
                        user.Gender = Gender.Unknown;
                        break;
                }
            }
            //民族
            if (IdCardBuyer.TblNation.Text != null && idCard.TblNation.Text != "")
            {
                user.Nation = (Nation)Nation.Parse(typeof(Nation), idCard.TblNation.Text); //字符串转枚举
            }
            else
            {
                user.Nation = Nation.未知;
            }
            //出生
            if (!string.IsNullOrEmpty(idCard.TblBirthday.Text))
            {
                user.Birthday = Convert.ToDateTime(idCard.TblBirthday.Text);
            }
            //地址
            if (!string.IsNullOrEmpty(idCard.TblAddress.Text))
            {
                user.Address = idCard.TblAddress.Text;
            }
            //证件号
            if (!string.IsNullOrEmpty(idCard.TblIdentityNo.Text))
            {
                user.IdentityNo = idCard.TblIdentityNo.Text;
            }
            //签发机关
            if (!string.IsNullOrEmpty(idCard.TblVisaAgency.Text))
            {
                user.VisaAgency = idCard.TblVisaAgency.Text;
            }
            //有效期-生效
            if (!string.IsNullOrEmpty(idCard.TblEffectiveDate.Text))
            {
                user.EffectiveDate = Convert.ToDateTime(idCard.TblEffectiveDate.Text);
            }

            //有效期-失效
            if (!string.IsNullOrEmpty(idCard.TblExpiryDate.Text))
            {
                if (idCard.TblExpiryDate.Text != "长期")
                {
                    user.ExpiryDate = Convert.ToDateTime(idCard.TblExpiryDate.Text);
                }
                else
                {
                    user.ExpiryDate = DateTime.MaxValue;
                }
            }
            else
            {
                user.ExpiryDate = null;
            }
            //照片
            if (IdentityInfo.Photo != null && IdentityInfo.Photo.Length != 0)
            {
                user.Photo = IdentityInfo.Photo;
            }
            user.IsTrusted = IdentityInfo.IsTrusted;


            #endregion

            return user;
        }

        private UserIdentityModel AddPassport(UserIdentityModel user, PassportControl passport)
        {
            #region 护照
            user.CertificateType = CertificateType.Passport;
            user.Name = passport.TbName.Text; //姓名
            //性别
            if (passport.RdobtnMale.IsChecked == true)
            {
                user.Gender = Gender.Male;
            }
            else if (passport.RdobtnFemale.IsChecked == true)
            {
                user.Gender = Gender.Female;
            }
            else
            {
                user.Gender = Gender.Unknown;
            }
            //出生
            if (!string.IsNullOrEmpty(passport.DateBirthday.Text))
            {
                user.Birthday = Convert.ToDateTime(passport.DateBirthday.Text);
            }
            user.Nationality = passport.CboNationality.Text.Trim(); //国籍
            //证件号
            if (!string.IsNullOrEmpty(passport.TbIdentityNo.Text))
            {
                user.IdentityNo = passport.TbIdentityNo.Text;
            }
            #endregion

            return user;
        }

        private UserIdentityModel AddHKMacaoIdCard(UserIdentityModel user, HKMacaoIdCardControl hKMacaoIdCard)
        {
            #region 港澳居民身份证
            user.CertificateType = CertificateType.HKMacaoIdCard;
            user.Name = hKMacaoIdCard.TbName.Text; //姓名
            //性别
            if (hKMacaoIdCard.RdobtnMale.IsChecked == true)
            {
                user.Gender = Gender.Male;
            }
            else if (hKMacaoIdCard.RdobtnFemale.IsChecked == true)
            {
                user.Gender = Gender.Female;
            }
            else
            {
                user.Gender = Gender.Unknown;
            }
            //出生
            if (!string.IsNullOrEmpty(hKMacaoIdCard.DateBirthday.Text))
            {
                user.Birthday = Convert.ToDateTime(hKMacaoIdCard.DateBirthday.Text);
            }
            //证件号
            if (!string.IsNullOrEmpty(hKMacaoIdCard.TbIdentityNo.Text))
            {
                user.IdentityNo = hKMacaoIdCard.TbIdentityNo.Text;
            }
            user.EffectiveDate = Convert.ToDateTime(hKMacaoIdCard.DateVisaAgency.Text.Trim()); //签发日期

            #endregion

            return user;
        }

        private UserIdentityModel AddBirthCertificate(UserIdentityModel user, BirthCertificateControl birthCertificate)
        {
            #region 出生证
            user.CertificateType = CertificateType.BirthCertificate;
            user.Name = birthCertificate.TbName.Text; //姓名
            //性别
            if (birthCertificate.RdobtnMale.IsChecked == true)
            {
                user.Gender = Gender.Male;
            }
            else if (birthCertificate.RdobtnFemale.IsChecked == true)
            {
                user.Gender = Gender.Female;
            }
            else
            {
                user.Gender = Gender.Unknown;
            }
            //出生
            if (!string.IsNullOrEmpty(birthCertificate.DateBirthday.Text))
            {
                user.Birthday = Convert.ToDateTime(birthCertificate.DateBirthday.Text);
            }
            //证件号
            if (!string.IsNullOrEmpty(birthCertificate.TbIdentityNo.Text))
            {
                user.IdentityNo = birthCertificate.TbIdentityNo.Text;
            }
            user.VisaAgency = birthCertificate.TbVisaAgency.Text.Trim(); //接生机构

            #endregion

            return user;
        }

        private UserIdentityModel AddMacaoPermit(UserIdentityModel user, MacaoPermitControl macaoPermit)
        {
            #region 港澳居民往来大陆通行证
            user.CertificateType = CertificateType.MacaoPermit;
            user.Name = macaoPermit.TbName.Text; //姓名
            //性别
            if (macaoPermit.RdobtnMale.IsChecked == true)
            {
                user.Gender = Gender.Male;
            }
            else if (macaoPermit.RdobtnFemale.IsChecked == true)
            {
                user.Gender = Gender.Female;
            }
            else
            {
                user.Gender = Gender.Unknown;
            }
            //出生
            if (!string.IsNullOrEmpty(macaoPermit.DateBirthday.Text))
            {
                user.Birthday = Convert.ToDateTime(macaoPermit.DateBirthday.Text);
            }
            //证件号
            if (!string.IsNullOrEmpty(macaoPermit.TbIdentityNo.Text))
            {
                user.IdentityNo = macaoPermit.TbIdentityNo.Text;
            }
            user.VisaAgency = macaoPermit.TbVisaAgency.Text.Trim(); //签发机构
            //有效期-生效
            if (!string.IsNullOrEmpty(macaoPermit.TbEffectiveDate.Text))
            {
                DateTime dTime = new DateTime(int.Parse(macaoPermit.TbEffectiveDate.Text), 1, 1);

                user.EffectiveDate = dTime;
            }
            //有效期-失效
            if (!string.IsNullOrEmpty(macaoPermit.TbExpiryDate.Text))
            {
                DateTime dTime = new DateTime(int.Parse(macaoPermit.TbExpiryDate.Text), 12, 31);

                user.ExpiryDate = dTime;
            }

            #endregion

            return user;
        }

        private UserIdentityModel AddTaiwanPermit(UserIdentityModel user, TaiwanPermitControl taiwanPermit)
        {
            #region 台湾居民往来大陆通行证
            user.CertificateType = CertificateType.TaiwanPermit;
            user.Name = taiwanPermit.TbName.Text; //姓名
            //性别
            if (taiwanPermit.RdobtnMale.IsChecked == true)
            {
                user.Gender = Gender.Male;
            }
            else if (taiwanPermit.RdobtnFemale.IsChecked == true)
            {
                user.Gender = Gender.Female;
            }
            else
            {
                user.Gender = Gender.Unknown;
            }
            //出生
            if (!string.IsNullOrEmpty(taiwanPermit.DateBirthday.Text))
            {
                user.Birthday = Convert.ToDateTime(taiwanPermit.DateBirthday.Text);
            }
            //证件号
            if (!string.IsNullOrEmpty(taiwanPermit.TbIdentityNo.Text))
            {
                user.IdentityNo = taiwanPermit.TbIdentityNo.Text;
            }
            user.VisaAgency = taiwanPermit.TbVisaAgency.Text.Trim(); //签发机构
            //有效期-生效
            if (!string.IsNullOrEmpty(taiwanPermit.TbEffectiveDate.Text))
            {
                DateTime dTime = new DateTime(int.Parse(taiwanPermit.TbEffectiveDate.Text), 1, 1);

                user.EffectiveDate = dTime;
            }
            //有效期-失效
            if (!string.IsNullOrEmpty(taiwanPermit.TbExpiryDate.Text))
            {
                DateTime dTime = new DateTime(int.Parse(taiwanPermit.TbExpiryDate.Text), 12, 31);

                user.ExpiryDate = dTime;
            }

            #endregion

            return user;
        }

        private UserIdentityModel AddCertificateOfOfficers(UserIdentityModel user, CertificateOfOfficersControl certificateOfOfficers)
        {
            #region 军官证
            user.CertificateType = CertificateType.CertificateOfOfficers;
            user.Name = certificateOfOfficers.TbName.Text; //姓名
            //性别
            if (certificateOfOfficers.RdobtnMale.IsChecked == true)
            {
                user.Gender = Gender.Male;
            }
            else if (certificateOfOfficers.RdobtnFemale.IsChecked == true)
            {
                user.Gender = Gender.Female;
            }
            else
            {
                user.Gender = Gender.Unknown;
            }
            //出生
            if (!string.IsNullOrEmpty(certificateOfOfficers.DateBirthday.Text))
            {
                user.Birthday = Convert.ToDateTime(certificateOfOfficers.DateBirthday.Text);
            }
            //民族
            if (IdCardBuyer.TblNation.Text != null && certificateOfOfficers.TbNation.Text != "")
            {
                user.Nation = (Nation)Nation.Parse(typeof(Nation), certificateOfOfficers.TbNation.Text); //字符串转枚举
            }
            else
            {
                user.Nation = Nation.未知;
            }
            //部别
            if (!string.IsNullOrEmpty(certificateOfOfficers.TbDepartmental.Text))
            {
                user.Address = certificateOfOfficers.TbDepartmental.Text;
            }
            //证件号
            if (!string.IsNullOrEmpty(certificateOfOfficers.TbIdentityNo.Text))
            {
                user.IdentityNo = certificateOfOfficers.TbIdentityNo.Text;
            }
            //发证机关
            if (!string.IsNullOrEmpty(certificateOfOfficers.TbVisaAgency.Text))
            {
                user.VisaAgency = certificateOfOfficers.TbVisaAgency.Text;
            }
            //有效期限-生效
            if (!string.IsNullOrEmpty(certificateOfOfficers.TbEffectiveDate.Text))
            {
                DateTime dTime = new DateTime(int.Parse(certificateOfOfficers.TbEffectiveDate.Text), 1, 1);

                user.EffectiveDate = dTime;
            }
            //有效期限-失效
            if (!string.IsNullOrEmpty(certificateOfOfficers.TbExpiryDate.Text))
            {
                DateTime dTime = new DateTime(int.Parse(certificateOfOfficers.TbExpiryDate.Text), 12, 31);

                user.ExpiryDate = dTime;
            }

            #endregion

            return user;
        }

        private UserIdentityModel AddResidenceBooklet(UserIdentityModel user, ResidenceBookletControl residenceBooklet)
        {
            #region 户口簿
            user.CertificateType = CertificateType.ResidenceBooklet;
            user.Name = residenceBooklet.TbName.Text; //姓名
            //性别
            if (residenceBooklet.RdobtnMale.IsChecked == true)
            {
                user.Gender = Gender.Male;
            }
            else if (residenceBooklet.RdobtnFemale.IsChecked == true)
            {
                user.Gender = Gender.Female;
            }
            else
            {
                user.Gender = Gender.Unknown;
            }
            //出生
            if (!string.IsNullOrEmpty(residenceBooklet.DateBirthday.Text))
            {
                user.Birthday = Convert.ToDateTime(residenceBooklet.DateBirthday.Text);
            }
            //民族
            if (residenceBooklet.TbNation.Text != null && residenceBooklet.TbNation.Text != "")
            {
                user.Nation = (Nation)Nation.Parse(typeof(Nation), residenceBooklet.TbNation.Text); //字符串转枚举
            }
            else
            {
                user.Nation = Nation.未知;
            }
            //证件号
            if (!string.IsNullOrEmpty(residenceBooklet.TbIdentityNo.Text))
            {
                user.IdentityNo = residenceBooklet.TbIdentityNo.Text;
            }
            //地址
            if (!string.IsNullOrEmpty(residenceBooklet.TbAddress.Text))
            {
                user.Address = residenceBooklet.TbAddress.Text;
            }

            #endregion

            return user;
        }

        #endregion

        #region 保存证件信息

        /// <summary>
        /// 保存买方证件信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnBuyerSave_Click(object sender, RoutedEventArgs e)
        {
            var checkDataResult = false;
            var user = new UserIdentityModel();
            var userIdentity = new UserIdentityModel(); //赋过值的User

            #region 判别证件类型
            //var certificateType = CertificateType.IdCard;
            var certificateType = (CertificateType)Enum.Parse(typeof(CertificateType), Convert.ToString(CboBuyerIdentity.SelectedValue));
            switch (certificateType)
            {
                case CertificateType.IdCard:
                    #region 身份证
                    //合法性检查
                    //if (string.IsNullOrEmpty(TblBuyerName.Text) ||
                    //    string.IsNullOrEmpty(TblBuyerGender.Text) ||
                    //    string.IsNullOrEmpty(TblBuyerBirthday.Text) ||
                    //    string.IsNullOrEmpty(TblBuyerNation.Text) ||
                    //    string.IsNullOrEmpty(TblBuyerAddress.Text) ||
                    //    string.IsNullOrEmpty(TblBuyerIdentityNo.Text) ||
                    //    string.IsNullOrEmpty(TblBuyerVisaAgency.Text) ||
                    //    string.IsNullOrEmpty(TblBuyerEffectiveDate.Text) ||
                    //    string.IsNullOrEmpty(TblBuyerExpiryDate.Text))
                    //{
                    //    MessageBox.Show("信息资料不合法，保存失败");
                    //    return;
                    //}
                    if (_buyerIdentityNoIDic.Count > 0)
                    {
                        foreach (string iDic in _buyerIdentityNoIDic.Values)
                        {
                            //if (iDic == TblBuyerIdentityNo.Text)
                            if (iDic == IdCardBuyer.TblIdentityNo.Text)
                            {
                                MessageBox.Show("该证件号买方已存在！");
                                return;
                            }
                        }
                    }
                    //赋值
                    userIdentity = AddIdCard(user, IdCardBuyer);

                    #region 判断是添加操作还是修改操作

                    if (_operationStatusBuyer == false) //添加
                    {

                        OperationResult result = AddUserIdentity(userIdentity, CustomerType.Buyer);

                        if (result.OtherData.Count == 0)
                        {
                            MessageBox.Show("用户保存失败");
                            return;
                        }

                        //long sUserSysNo
                        _buyerUserSysNo = result.OtherData["UserSysNo"] == null
                            ? -1
                            : Convert.ToInt64(result.OtherData["UserSysNo"]);

                        if (_buyerUserSysNo <= 0)
                        {
                            throw new ArgumentException("用户保存失败");
                        }
                        _buyerIdentityNoIDic.Add(_buyerUserSysNo, userIdentity.IdentityNo);

                    }
                    else //修改
                    {
                        _buyerUserSysNo = long.Parse(LbBuyerList.SelectedValue.ToString());
                        OperationResult result = UserServiceProxy.BindUserIdentity(_buyerUserSysNo, UserType.Customer, userIdentity);

                        icardInfoDic.Remove(_buyerUserSysNo);
                        _buyerIdentityNoIDic.Remove(_buyerUserSysNo);
                        _buyerIdentityNoIDic.Add(_buyerUserSysNo, userIdentity.IdentityNo);
                    }

                    #endregion

                    #endregion

                    break;
                case CertificateType.Passport:
                    #region 护照
                    //001-2检查数据合法性
                    checkDataResult = CheckBuyerDataNew(CertificateType.Passport);
                    if (!checkDataResult) { return; }

                    //赋值
                    userIdentity = AddPassport(user, PassportBuyer);

                    #region 判断是添加操作还是修改操作

                    if (_operationStatusBuyer == false) //添加
                    {
                        OperationResult result = AddUserIdentity(userIdentity, CustomerType.Buyer);
                        if (result.OtherData.Count == 0)
                        {
                            MessageBox.Show("用户保存失败");
                            return;
                        }

                        //long sUserSysNo
                        _buyerUserSysNo = result.OtherData["UserSysNo"] == null
                            ? -1
                            : Convert.ToInt64(result.OtherData["UserSysNo"]);

                        if (_buyerUserSysNo <= 0)
                        {
                            throw new ArgumentException("用户保存失败");
                        }

                        if (_buyerIdentityNoIDic.Count > 0)
                        {
                            foreach (string iDic in _buyerIdentityNoIDic.Values)
                            {
                                if (iDic == userIdentity.IdentityNo)
                                {
                                    MessageBox.Show("该证件号买方已存在！");
                                    return;
                                }
                            }
                        }
                        _buyerIdentityNoIDic.Add(_buyerUserSysNo, userIdentity.IdentityNo);
                    }
                    else //修改
                    {
                        _buyerUserSysNo = long.Parse(LbBuyerList.SelectedValue.ToString());
                        OperationResult result = UserServiceProxy.BindUserIdentity(_buyerUserSysNo, UserType.Customer, userIdentity);

                        icardInfoDic.Remove(_buyerUserSysNo);
                        _buyerIdentityNoIDic.Remove(_buyerUserSysNo);
                        _buyerIdentityNoIDic.Add(_buyerUserSysNo, userIdentity.IdentityNo);
                        LbBuyerList.SelectedItem = null;

                        BtnBuyerRevoke_Click(sender, e);
                    }

                    #endregion

                    #endregion
                    break;
                case CertificateType.HKMacaoIdCard:
                    #region 港澳居民身份证
                    //001-2检查数据合法性
                    checkDataResult = CheckBuyerDataNew(CertificateType.HKMacaoIdCard);
                    if (!checkDataResult) { return; }

                    //赋值
                    userIdentity = AddHKMacaoIdCard(user, HKMacaoIdCardBuyer);

                    #region 判断是添加操作还是修改操作

                    if (_operationStatusBuyer == false) //添加
                    {
                        OperationResult result = AddUserIdentity(userIdentity, CustomerType.Buyer);
                        if (result.OtherData.Count == 0)
                        {
                            MessageBox.Show("用户保存失败");
                            return;
                        }

                        //long sUserSysNo
                        _buyerUserSysNo = result.OtherData["UserSysNo"] == null
                            ? -1
                            : Convert.ToInt64(result.OtherData["UserSysNo"]);

                        if (_buyerUserSysNo <= 0)
                        {
                            throw new ArgumentException("用户保存失败");
                        }

                        if (_buyerIdentityNoIDic.Count > 0)
                        {
                            foreach (string iDic in _buyerIdentityNoIDic.Values)
                            {
                                if (iDic == userIdentity.IdentityNo)
                                {
                                    MessageBox.Show("该证件号买方已存在！");
                                    return;
                                }
                            }
                        }
                        _buyerIdentityNoIDic.Add(_buyerUserSysNo, userIdentity.IdentityNo);
                    }
                    else //修改
                    {
                        _buyerUserSysNo = long.Parse(LbBuyerList.SelectedValue.ToString());
                        OperationResult result = UserServiceProxy.BindUserIdentity(_buyerUserSysNo, UserType.Customer, userIdentity);

                        icardInfoDic.Remove(_buyerUserSysNo);
                        _buyerIdentityNoIDic.Remove(_buyerUserSysNo);
                        _buyerIdentityNoIDic.Add(_buyerUserSysNo, userIdentity.IdentityNo);
                        LbBuyerList.SelectedItem = null;

                        BtnBuyerRevoke_Click(sender, e);
                    }

                    #endregion

                    #endregion
                    break;
                case CertificateType.BirthCertificate:
                    #region 出生证
                    //001-2检查数据合法性
                    checkDataResult = CheckBuyerDataNew(CertificateType.BirthCertificate);
                    if (!checkDataResult) { return; }

                    //赋值
                    userIdentity = AddBirthCertificate(user, BirthCertificateBuyer);

                    #region 判断是添加操作还是修改操作

                    if (_operationStatusBuyer == false) //添加
                    {
                        OperationResult result = AddUserIdentity(userIdentity, CustomerType.Buyer);
                        if (result.OtherData.Count == 0)
                        {
                            MessageBox.Show("用户保存失败");
                            return;
                        }

                        //long sUserSysNo
                        _buyerUserSysNo = result.OtherData["UserSysNo"] == null
                            ? -1
                            : Convert.ToInt64(result.OtherData["UserSysNo"]);

                        if (_buyerUserSysNo <= 0)
                        {
                            throw new ArgumentException("用户保存失败");
                        }

                        if (_buyerIdentityNoIDic.Count > 0)
                        {
                            foreach (string iDic in _buyerIdentityNoIDic.Values)
                            {
                                if (iDic == userIdentity.IdentityNo)
                                {
                                    MessageBox.Show("该证件号买方已存在！");
                                    return;
                                }
                            }
                        }
                        _buyerIdentityNoIDic.Add(_buyerUserSysNo, userIdentity.IdentityNo);
                    }
                    else //修改
                    {
                        _buyerUserSysNo = long.Parse(LbBuyerList.SelectedValue.ToString());
                        OperationResult result = UserServiceProxy.BindUserIdentity(_buyerUserSysNo, UserType.Customer, userIdentity);

                        icardInfoDic.Remove(_buyerUserSysNo);
                        _buyerIdentityNoIDic.Remove(_buyerUserSysNo);
                        _buyerIdentityNoIDic.Add(_buyerUserSysNo, userIdentity.IdentityNo);
                        LbBuyerList.SelectedItem = null;

                        BtnBuyerRevoke_Click(sender, e);
                    }

                    #endregion

                    #endregion
                    break;
                case CertificateType.MacaoPermit:
                    #region 港澳居民往来大陆通行证
                    //001-2检查数据合法性
                    checkDataResult = CheckBuyerDataNew(CertificateType.MacaoPermit);
                    if (!checkDataResult) { return; }

                    //赋值
                    userIdentity = AddMacaoPermit(user, MacaoPermitBuyer);

                    #region 判断是添加操作还是修改操作

                    if (_operationStatusBuyer == false) //添加
                    {
                        OperationResult result = AddUserIdentity(userIdentity, CustomerType.Buyer);
                        if (result.OtherData.Count == 0)
                        {
                            MessageBox.Show("用户保存失败");
                            return;
                        }

                        //long sUserSysNo
                        _buyerUserSysNo = result.OtherData["UserSysNo"] == null
                            ? -1
                            : Convert.ToInt64(result.OtherData["UserSysNo"]);

                        if (_buyerUserSysNo <= 0)
                        {
                            throw new ArgumentException("用户保存失败");
                        }

                        if (_buyerIdentityNoIDic.Count > 0)
                        {
                            foreach (string iDic in _buyerIdentityNoIDic.Values)
                            {
                                if (iDic == userIdentity.IdentityNo)
                                {
                                    MessageBox.Show("该证件号买方已存在！");
                                    return;
                                }
                            }
                        }
                        _buyerIdentityNoIDic.Add(_buyerUserSysNo, userIdentity.IdentityNo);
                    }
                    else //修改
                    {
                        _buyerUserSysNo = long.Parse(LbBuyerList.SelectedValue.ToString());
                        OperationResult result = UserServiceProxy.BindUserIdentity(_buyerUserSysNo, UserType.Customer, userIdentity);

                        icardInfoDic.Remove(_buyerUserSysNo);
                        _buyerIdentityNoIDic.Remove(_buyerUserSysNo);
                        _buyerIdentityNoIDic.Add(_buyerUserSysNo, userIdentity.IdentityNo);
                        LbBuyerList.SelectedItem = null;

                        BtnBuyerRevoke_Click(sender, e);
                    }

                    #endregion

                    #endregion
                    break;
                case CertificateType.TaiwanPermit:
                    #region 台湾居民往来大陆通行证
                    //001-2检查数据合法性
                    checkDataResult = CheckBuyerDataNew(CertificateType.TaiwanPermit);
                    if (!checkDataResult) { return; }

                    //赋值
                    userIdentity = AddTaiwanPermit(user, TaiwanPermitBuyer);

                    #region 判断是添加操作还是修改操作

                    if (_operationStatusBuyer == false) //添加
                    {
                        OperationResult result = AddUserIdentity(userIdentity, CustomerType.Buyer);
                        if (result.OtherData.Count == 0)
                        {
                            MessageBox.Show("用户保存失败");
                            return;
                        }

                        //long sUserSysNo
                        _buyerUserSysNo = result.OtherData["UserSysNo"] == null
                            ? -1
                            : Convert.ToInt64(result.OtherData["UserSysNo"]);

                        if (_buyerUserSysNo <= 0)
                        {
                            throw new ArgumentException("用户保存失败");
                        }

                        if (_buyerIdentityNoIDic.Count > 0)
                        {
                            foreach (string iDic in _buyerIdentityNoIDic.Values)
                            {
                                if (iDic == userIdentity.IdentityNo)
                                {
                                    MessageBox.Show("该证件号买方已存在！");
                                    return;
                                }
                            }
                        }
                        _buyerIdentityNoIDic.Add(_buyerUserSysNo, userIdentity.IdentityNo);
                    }
                    else //修改
                    {
                        _buyerUserSysNo = long.Parse(LbBuyerList.SelectedValue.ToString());
                        OperationResult result = UserServiceProxy.BindUserIdentity(_buyerUserSysNo, UserType.Customer, userIdentity);

                        icardInfoDic.Remove(_buyerUserSysNo);
                        _buyerIdentityNoIDic.Remove(_buyerUserSysNo);
                        _buyerIdentityNoIDic.Add(_buyerUserSysNo, userIdentity.IdentityNo);
                        LbBuyerList.SelectedItem = null;

                        BtnBuyerRevoke_Click(sender, e);
                    }

                    #endregion

                    #endregion
                    break;
                case CertificateType.CertificateOfOfficers:
                    #region 军官证
                    //001-2检查数据合法性
                    checkDataResult = CheckBuyerDataNew(CertificateType.CertificateOfOfficers);
                    if (!checkDataResult) { return; }

                    //赋值
                    userIdentity = AddCertificateOfOfficers(user, CertificateOfOfficersBuyer);

                    #region 判断是添加操作还是修改操作

                    if (_operationStatusBuyer == false) //添加
                    {
                        OperationResult result = AddUserIdentity(userIdentity, CustomerType.Buyer);
                        if (result.OtherData.Count == 0)
                        {
                            MessageBox.Show("用户保存失败");
                            return;
                        }

                        //long sUserSysNo
                        _buyerUserSysNo = result.OtherData["UserSysNo"] == null
                            ? -1
                            : Convert.ToInt64(result.OtherData["UserSysNo"]);

                        if (_buyerUserSysNo <= 0)
                        {
                            throw new ArgumentException("用户保存失败");
                        }

                        if (_buyerIdentityNoIDic.Count > 0)
                        {
                            foreach (string iDic in _buyerIdentityNoIDic.Values)
                            {
                                if (iDic == userIdentity.IdentityNo)
                                {
                                    MessageBox.Show("该证件号买方已存在！");
                                    return;
                                }
                            }
                        }
                        _buyerIdentityNoIDic.Add(_buyerUserSysNo, userIdentity.IdentityNo);
                    }
                    else //修改
                    {
                        _buyerUserSysNo = long.Parse(LbBuyerList.SelectedValue.ToString());
                        OperationResult result = UserServiceProxy.BindUserIdentity(_buyerUserSysNo, UserType.Customer, userIdentity);

                        icardInfoDic.Remove(_buyerUserSysNo);
                        _buyerIdentityNoIDic.Remove(_buyerUserSysNo);
                        _buyerIdentityNoIDic.Add(_buyerUserSysNo, userIdentity.IdentityNo);
                        LbBuyerList.SelectedItem = null;

                        BtnBuyerRevoke_Click(sender, e);
                    }

                    #endregion

                    #endregion
                    break;
                case CertificateType.ResidenceBooklet:
                    #region 户口簿
                    //001-2检查数据合法性
                    checkDataResult = CheckBuyerDataNew(CertificateType.ResidenceBooklet);
                    if (!checkDataResult) { return; }

                    //赋值
                    userIdentity = AddResidenceBooklet(user, ResidenceBookletBuyer);

                    #region 判断是添加操作还是修改操作

                    if (_operationStatusBuyer == false) //添加
                    {
                        OperationResult result = AddUserIdentity(userIdentity, CustomerType.Buyer);
                        if (result.OtherData.Count == 0)
                        {
                            MessageBox.Show("用户保存失败");
                            return;
                        }

                        //long sUserSysNo
                        _buyerUserSysNo = result.OtherData["UserSysNo"] == null
                            ? -1
                            : Convert.ToInt64(result.OtherData["UserSysNo"]);

                        if (_buyerUserSysNo <= 0)
                        {
                            throw new ArgumentException("用户保存失败");
                        }

                        if (_buyerIdentityNoIDic.Count > 0)
                        {
                            foreach (string iDic in _buyerIdentityNoIDic.Values)
                            {
                                if (iDic == userIdentity.IdentityNo)
                                {
                                    MessageBox.Show("该证件号买方已存在！");
                                    return;
                                }
                            }
                        }
                        _buyerIdentityNoIDic.Add(_buyerUserSysNo, userIdentity.IdentityNo);
                    }
                    else //修改
                    {
                        _buyerUserSysNo = long.Parse(LbBuyerList.SelectedValue.ToString());
                        OperationResult result = UserServiceProxy.BindUserIdentity(_buyerUserSysNo, UserType.Customer, userIdentity);

                        icardInfoDic.Remove(_buyerUserSysNo);
                        _buyerIdentityNoIDic.Remove(_buyerUserSysNo);
                        _buyerIdentityNoIDic.Add(_buyerUserSysNo, userIdentity.IdentityNo);
                        LbBuyerList.SelectedItem = null;

                        BtnBuyerRevoke_Click(sender, e);
                    }

                    #endregion

                    #endregion
                    break;
            }
            #region 清空数据
            //ClearIdCard(IdCardBuyer);
            //ClearPassport(PassportBuyer);
            //ClearHKMacaoIdCard(HKMacaoIdCardBuyer);
            //ClearBirthCertificate(BirthCertificateBuyer);
            //ClearMacaoPermit(MacaoPermitBuyer);
            //ClearCertificateOfOfficers(CertificateOfOfficersBuyer);
            //ClearResidenceBooklet(ResidenceBookletBuyer);
            ClearBuyerIDType();
            #endregion

            _buyerNameDic[_buyerUserSysNo] = user.Name;
            BindData(LbBuyerList, _buyerNameDic);

            #endregion

        }

        private void BtnBuyerSave_ClickOld(object sender, RoutedEventArgs e)
        {
            var checkDataResult = false;
            var user = new UserIdentityModel();
            #region 判别证件类型

            if (CboBuyerIdentity.SelectedValue.ToString() == "0")
            {
                #region 身份证

                //合法性检查
                if (string.IsNullOrEmpty(TblBuyerName.Text) ||
                    string.IsNullOrEmpty(TblBuyerGender.Text) ||
                    string.IsNullOrEmpty(TblBuyerBirthday.Text) ||
                    string.IsNullOrEmpty(TblBuyerNation.Text) ||
                    string.IsNullOrEmpty(TblBuyerAddress.Text) ||
                    string.IsNullOrEmpty(TblBuyerIdentityNo.Text) ||
                    string.IsNullOrEmpty(TblBuyerVisaAgency.Text) ||
                    string.IsNullOrEmpty(TblBuyerEffectiveDate.Text) ||
                    string.IsNullOrEmpty(TblBuyerExpiryDate.Text))
                {
                    MessageBox.Show("信息资料不合法，保存失败");
                    return;
                }
                if (_buyerIdentityNoIDic.Count > 0)
                {
                    foreach (string iDic in _buyerIdentityNoIDic.Values)
                    {
                        if (iDic == TblBuyerIdentityNo.Text)
                        {
                            MessageBox.Show("该证件号买方已存在！");
                            return;
                        }
                    }
                }
                //001-2检查数据合法性
                checkDataResult = CheckBuyerData(CertificateType.IdCard);
                if (!checkDataResult) { return; }

                #region 赋值

                user.CertificateType = CertificateType.IdCard;
                user.Name = TblBuyerName.Text;
                if (TblBuyerGender.Text == "男")
                {
                    user.Gender = Gender.Male;
                }
                else if (TblBuyerGender.Text == "女")
                {
                    user.Gender = Gender.Female;
                }
                else
                {
                    user.Gender = Gender.Unknown;
                }
                if (TblBuyerNation.Text != null)
                    user.Nation = (Nation)Nation.Parse(typeof(Nation), TblBuyerNation.Text); //字符串转枚举
                user.Birthday = Convert.ToDateTime(TblBuyerBirthday.Text);
                user.Address = TblBuyerAddress.Text;
                user.IdentityNo = TblBuyerIdentityNo.Text;
                user.VisaAgency = TblBuyerVisaAgency.Text;
                user.EffectiveDate = Convert.ToDateTime(TblBuyerEffectiveDate.Text);
                if (TblBuyerExpiryDate.Text != "长期")
                {
                    user.ExpiryDate = Convert.ToDateTime(TblBuyerExpiryDate.Text);
                }
                else
                {
                    user.ExpiryDate = null;
                }

                #endregion

                #region 判断是添加操作还是修改操作

                if (_operationStatusBuyer == false) //添加
                {

                    OperationResult result = AddUserIdentity(user, CustomerType.Buyer);

                    if (result.OtherData.Count == 0)
                    {
                        MessageBox.Show("用户保存失败");
                        return;
                    }

                    //long sUserSysNo
                    _buyerUserSysNo = result.OtherData["UserSysNo"] == null
                        ? -1
                        : Convert.ToInt64(result.OtherData["UserSysNo"]);

                    if (_buyerUserSysNo <= 0)
                    {
                        throw new ArgumentException("用户保存失败");
                    }
                    _buyerIdentityNoIDic.Add(_buyerUserSysNo, user.IdentityNo);

                }
                else //修改
                {
                    _buyerUserSysNo = long.Parse(LbBuyerList.SelectedValue.ToString());
                    OperationResult result = UserServiceProxy.BindUserIdentity(_buyerUserSysNo, UserType.Customer, user);

                    icardInfoDic.Remove(_buyerUserSysNo);
                    _buyerIdentityNoIDic.Remove(_buyerUserSysNo);
                    _buyerIdentityNoIDic.Add(_buyerUserSysNo, user.IdentityNo);
                }

                #endregion

                WipeDataBuyerIdCard();
                WipeDataBuyerPassport();

                #endregion
            }
            else
            {
                #region 其他

                //if (string.IsNullOrEmpty(TbBuyerName.Text))
                //{
                //    MessageBox.Show("用户名不能为空！");
                //    return;
                //}

                //if (RdoBtnBuyerGenderMale.IsChecked == null ||
                //    RdoBtnBuyerGenderFemale.IsChecked == null ||
                //    (RdoBtnBuyerGenderFemale.IsChecked == false && RdoBtnBuyerGenderMale.IsChecked == false))
                //{
                //    MessageBox.Show("请选择性别！");
                //    return;
                //}

                //if (string.IsNullOrEmpty(TbBuyerIdentityNo.Text))
                //{
                //    MessageBox.Show("请填写证件编号！");
                //    return;
                //}

                //001-2检查数据合法性
                checkDataResult = CheckBuyerData(CertificateType.Passport);
                if (!checkDataResult) { return; }

                #region 赋值
                user.CertificateType = CertificateType.Passport;
                user.Name = TbBuyerName.Text;
                if (RdoBtnBuyerGenderMale.IsChecked == true)
                {
                    user.Gender = Gender.Male;
                }
                else if (RdoBtnBuyerGenderFemale.IsChecked == true)
                {
                    user.Gender = Gender.Female;
                }
                else
                {
                    user.Gender = Gender.Unknown;
                }
                user.Birthday = Convert.ToDateTime(TbBuyerBirthday.Text.Trim());
                user.Nationality = CboBuyerNationality.Text.Trim();
                user.IdentityNo = TbBuyerIdentityNo.Text.Trim();
                #endregion

                #region 判断是添加操作还是修改操作

                if (_operationStatusBuyer == false) //添加
                {
                    OperationResult result = AddUserIdentity(user, CustomerType.Buyer);
                    if (result.OtherData.Count == 0)
                    {
                        MessageBox.Show("用户保存失败");
                        return;
                    }

                    //long sUserSysNo
                    _buyerUserSysNo = result.OtherData["UserSysNo"] == null
                        ? -1
                        : Convert.ToInt64(result.OtherData["UserSysNo"]);

                    if (_buyerUserSysNo <= 0)
                    {
                        throw new ArgumentException("用户保存失败");
                    }

                    if (_buyerIdentityNoIDic.Count > 0)
                    {
                        foreach (string iDic in _buyerIdentityNoIDic.Values)
                        {
                            if (iDic == user.IdentityNo)
                            {
                                MessageBox.Show("该证件号买方已存在！");
                                return;
                            }
                        }
                    }
                    _buyerIdentityNoIDic.Add(_buyerUserSysNo, user.IdentityNo);
                }
                else //修改
                {
                    _buyerUserSysNo = long.Parse(LbBuyerList.SelectedValue.ToString());
                    OperationResult result = UserServiceProxy.BindUserIdentity(_buyerUserSysNo, UserType.Customer, user);

                    icardInfoDic.Remove(_buyerUserSysNo);
                    _buyerIdentityNoIDic.Remove(_buyerUserSysNo);
                    _buyerIdentityNoIDic.Add(_buyerUserSysNo, user.IdentityNo);
                    LbBuyerList.SelectedItem = null;

                    BtnBuyerRevoke_Click(sender, e);
                }

                #endregion

                WipeDataBuyerIdCard();
                WipeDataBuyerPassport();

                #endregion
            }
            //_buyerNameDic.Add(_buyerUserSysNo, user.Name);
            _buyerNameDic[_buyerUserSysNo] = user.Name;
            BindData(LbBuyerList, _buyerNameDic);


            #endregion


        }


        /// <summary>
        /// 保存卖家证件信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSellerSave_Click(object sender, RoutedEventArgs e)
        {
            var checkDataResult = false;
            UserIdentityModel user = new UserIdentityModel();
            var userIdentity = new UserIdentityModel(); //赋过值的User

            #region 判别证件类型
            //var certificateType = CertificateType.IdCard;
            var certificateType = (CertificateType)Enum.Parse(typeof(CertificateType), Convert.ToString(CboSellerIdentity.SelectedValue));
            switch (certificateType)
            {
                case CertificateType.IdCard:
                    #region 身份证
                    //合法性检查
                    //if (string.IsNullOrEmpty(TblSellerName.Text) ||
                    //    string.IsNullOrEmpty(TblSellerGender.Text) ||
                    //    string.IsNullOrEmpty(TblSellerBirthday.Text) ||
                    //    string.IsNullOrEmpty(TblSellerNation.Text) ||
                    //    string.IsNullOrEmpty(TblSellerAddress.Text) ||
                    //    string.IsNullOrEmpty(TblSellerIdentityNo.Text) ||
                    //    string.IsNullOrEmpty(TblSellerVisaAgency.Text) ||
                    //    string.IsNullOrEmpty(TblSellerEffectiveDate.Text) ||
                    //    string.IsNullOrEmpty(TblSellerExpiryDate.Text))
                    //{
                    //    MessageBox.Show("信息资料不合法，保存失败");
                    //    return;
                    //}
                    if (_sellerIdentityNoIDic.Count > 0)
                    {
                        foreach (string iDic in _sellerIdentityNoIDic.Values)
                        {
                            if (iDic == IdCardSeller.TblIdentityNo.Text)
                            {
                                MessageBox.Show("该证件号卖方已存在！");
                                return;
                            }
                        }
                    }
                    //赋值
                    userIdentity = AddIdCard(user, IdCardSeller);

                    #region 判断是添加操作还是修改操作

                    if (_operationStatusSeller == false) //添加
                    {

                        OperationResult result = AddUserIdentity(userIdentity, CustomerType.Seller);

                        if (result.OtherData.Count == 0)
                        {
                            MessageBox.Show("用户保存失败");
                            return;
                        }

                        //long sUserSysNo
                        _sellerUserSysNo = result.OtherData["UserSysNo"] == null
                            ? -1
                            : Convert.ToInt64(result.OtherData["UserSysNo"]);

                        if (_sellerUserSysNo <= 0)
                        {
                            throw new ArgumentException("用户保存失败");
                        }
                        _sellerIdentityNoIDic.Add(_sellerUserSysNo, userIdentity.IdentityNo);

                    }
                    else //修改
                    {
                        _sellerUserSysNo = long.Parse(LbSellerList.SelectedValue.ToString());
                        OperationResult result = UserServiceProxy.BindUserIdentity(_sellerUserSysNo, UserType.Customer, userIdentity);

                        icardInfoDic.Remove(_sellerUserSysNo);
                        _sellerIdentityNoIDic.Remove(_sellerUserSysNo);
                        _sellerIdentityNoIDic.Add(_sellerUserSysNo, userIdentity.IdentityNo);
                    }

                    #endregion

                    #endregion
                    break;
                case CertificateType.Passport:
                    #region 护照
                    //001-2检查数据合法性
                    checkDataResult = CheckSellerDataNew(CertificateType.Passport);
                    if (!checkDataResult) { return; }

                    //赋值
                    userIdentity = AddPassport(user, PassportSeller);

                    #region 判断是添加操作还是修改操作

                    if (_operationStatusSeller == false) //添加
                    {
                        OperationResult result = AddUserIdentity(userIdentity, CustomerType.Seller);
                        if (result.OtherData.Count == 0)
                        {
                            MessageBox.Show("用户保存失败");
                            return;
                        }

                        //long sUserSysNo
                        _sellerUserSysNo = result.OtherData["UserSysNo"] == null
                            ? -1
                            : Convert.ToInt64(result.OtherData["UserSysNo"]);

                        if (_sellerUserSysNo <= 0)
                        {
                            throw new ArgumentException("用户保存失败");
                        }

                        if (_sellerIdentityNoIDic.Count > 0)
                        {
                            foreach (string iDic in _sellerIdentityNoIDic.Values)
                            {
                                if (iDic == userIdentity.IdentityNo)
                                {
                                    MessageBox.Show("该证件号买方已存在！");
                                    return;
                                }
                            }
                        }
                        _sellerIdentityNoIDic.Add(_sellerUserSysNo, userIdentity.IdentityNo);
                    }
                    else //修改
                    {
                        _sellerUserSysNo = long.Parse(LbSellerList.SelectedValue.ToString());
                        OperationResult result = UserServiceProxy.BindUserIdentity(_sellerUserSysNo, UserType.Customer, userIdentity);

                        icardInfoDic.Remove(_sellerUserSysNo);
                        _sellerIdentityNoIDic.Remove(_sellerUserSysNo);
                        _sellerIdentityNoIDic.Add(_sellerUserSysNo, userIdentity.IdentityNo);
                        LbSellerList.SelectedItem = null;

                        BtnSellerRevoke_Click(sender, e);
                    }

                    #endregion

                    #endregion
                    break;
                case CertificateType.HKMacaoIdCard:
                    #region 港澳居民身份证
                    //001-2检查数据合法性
                    checkDataResult = CheckSellerDataNew(CertificateType.HKMacaoIdCard);
                    if (!checkDataResult) { return; }

                    //赋值
                    userIdentity = AddHKMacaoIdCard(user, HKMacaoIdCardSeller);

                    #region 判断是添加操作还是修改操作

                    if (_operationStatusSeller == false) //添加
                    {
                        OperationResult result = AddUserIdentity(userIdentity, CustomerType.Seller);
                        if (result.OtherData.Count == 0)
                        {
                            MessageBox.Show("用户保存失败");
                            return;
                        }

                        //long sUserSysNo
                        _sellerUserSysNo = result.OtherData["UserSysNo"] == null
                            ? -1
                            : Convert.ToInt64(result.OtherData["UserSysNo"]);

                        if (_sellerUserSysNo <= 0)
                        {
                            throw new ArgumentException("用户保存失败");
                        }

                        if (_sellerIdentityNoIDic.Count > 0)
                        {
                            foreach (string iDic in _sellerIdentityNoIDic.Values)
                            {
                                if (iDic == userIdentity.IdentityNo)
                                {
                                    MessageBox.Show("该证件号买方已存在！");
                                    return;
                                }
                            }
                        }
                        _sellerIdentityNoIDic.Add(_sellerUserSysNo, userIdentity.IdentityNo);
                    }
                    else //修改
                    {
                        _sellerUserSysNo = long.Parse(LbSellerList.SelectedValue.ToString());
                        OperationResult result = UserServiceProxy.BindUserIdentity(_sellerUserSysNo, UserType.Customer, userIdentity);

                        icardInfoDic.Remove(_sellerUserSysNo);
                        _sellerIdentityNoIDic.Remove(_sellerUserSysNo);
                        _sellerIdentityNoIDic.Add(_sellerUserSysNo, userIdentity.IdentityNo);
                        LbSellerList.SelectedItem = null;

                        BtnSellerRevoke_Click(sender, e);
                    }

                    #endregion

                    #endregion
                    break;
                case CertificateType.BirthCertificate:
                    #region 出生证
                    //001-2检查数据合法性
                    checkDataResult = CheckSellerDataNew(CertificateType.BirthCertificate);
                    if (!checkDataResult) { return; }

                    //赋值
                    userIdentity = AddBirthCertificate(user, BirthCertificateSeller);

                    #region 判断是添加操作还是修改操作

                    if (_operationStatusSeller == false) //添加
                    {
                        OperationResult result = AddUserIdentity(userIdentity, CustomerType.Seller);
                        if (result.OtherData.Count == 0)
                        {
                            MessageBox.Show("用户保存失败");
                            return;
                        }

                        //long sUserSysNo
                        _sellerUserSysNo = result.OtherData["UserSysNo"] == null
                            ? -1
                            : Convert.ToInt64(result.OtherData["UserSysNo"]);

                        if (_sellerUserSysNo <= 0)
                        {
                            throw new ArgumentException("用户保存失败");
                        }

                        if (_sellerIdentityNoIDic.Count > 0)
                        {
                            foreach (string iDic in _sellerIdentityNoIDic.Values)
                            {
                                if (iDic == userIdentity.IdentityNo)
                                {
                                    MessageBox.Show("该证件号买方已存在！");
                                    return;
                                }
                            }
                        }
                        _sellerIdentityNoIDic.Add(_sellerUserSysNo, userIdentity.IdentityNo);
                    }
                    else //修改
                    {
                        _sellerUserSysNo = long.Parse(LbSellerList.SelectedValue.ToString());
                        OperationResult result = UserServiceProxy.BindUserIdentity(_sellerUserSysNo, UserType.Customer, userIdentity);

                        icardInfoDic.Remove(_sellerUserSysNo);
                        _sellerIdentityNoIDic.Remove(_sellerUserSysNo);
                        _sellerIdentityNoIDic.Add(_sellerUserSysNo, userIdentity.IdentityNo);
                        LbSellerList.SelectedItem = null;

                        BtnSellerRevoke_Click(sender, e);
                    }

                    #endregion

                    #endregion
                    break;
                case CertificateType.MacaoPermit:
                    #region 港澳居民往来大陆通行证
                    //001-2检查数据合法性
                    checkDataResult = CheckSellerDataNew(CertificateType.MacaoPermit);
                    if (!checkDataResult) { return; }

                    //赋值
                    userIdentity = AddMacaoPermit(user, MacaoPermitSeller);

                    #region 判断是添加操作还是修改操作

                    if (_operationStatusSeller == false) //添加
                    {
                        OperationResult result = AddUserIdentity(userIdentity, CustomerType.Seller);
                        if (result.OtherData.Count == 0)
                        {
                            MessageBox.Show("用户保存失败");
                            return;
                        }

                        //long sUserSysNo
                        _sellerUserSysNo = result.OtherData["UserSysNo"] == null
                            ? -1
                            : Convert.ToInt64(result.OtherData["UserSysNo"]);

                        if (_sellerUserSysNo <= 0)
                        {
                            throw new ArgumentException("用户保存失败");
                        }

                        if (_sellerIdentityNoIDic.Count > 0)
                        {
                            foreach (string iDic in _sellerIdentityNoIDic.Values)
                            {
                                if (iDic == userIdentity.IdentityNo)
                                {
                                    MessageBox.Show("该证件号买方已存在！");
                                    return;
                                }
                            }
                        }
                        _sellerIdentityNoIDic.Add(_sellerUserSysNo, userIdentity.IdentityNo);
                    }
                    else //修改
                    {
                        _sellerUserSysNo = long.Parse(LbSellerList.SelectedValue.ToString());
                        OperationResult result = UserServiceProxy.BindUserIdentity(_sellerUserSysNo, UserType.Customer, userIdentity);

                        icardInfoDic.Remove(_sellerUserSysNo);
                        _sellerIdentityNoIDic.Remove(_sellerUserSysNo);
                        _sellerIdentityNoIDic.Add(_sellerUserSysNo, userIdentity.IdentityNo);
                        LbSellerList.SelectedItem = null;

                        BtnSellerRevoke_Click(sender, e);
                    }

                    #endregion

                    #endregion
                    break;
                case CertificateType.TaiwanPermit:
                    #region 台湾居民往来大陆通行证
                    //001-2检查数据合法性
                    checkDataResult = CheckSellerDataNew(CertificateType.TaiwanPermit);
                    if (!checkDataResult) { return; }

                    //赋值
                    userIdentity = AddTaiwanPermit(user, TaiwanPermitSeller);

                    #region 判断是添加操作还是修改操作

                    if (_operationStatusSeller == false) //添加
                    {
                        OperationResult result = AddUserIdentity(userIdentity, CustomerType.Seller);
                        if (result.OtherData.Count == 0)
                        {
                            MessageBox.Show("用户保存失败");
                            return;
                        }

                        //long sUserSysNo
                        _sellerUserSysNo = result.OtherData["UserSysNo"] == null
                            ? -1
                            : Convert.ToInt64(result.OtherData["UserSysNo"]);

                        if (_sellerUserSysNo <= 0)
                        {
                            throw new ArgumentException("用户保存失败");
                        }

                        if (_sellerIdentityNoIDic.Count > 0)
                        {
                            foreach (string iDic in _sellerIdentityNoIDic.Values)
                            {
                                if (iDic == userIdentity.IdentityNo)
                                {
                                    MessageBox.Show("该证件号买方已存在！");
                                    return;
                                }
                            }
                        }
                        _sellerIdentityNoIDic.Add(_sellerUserSysNo, userIdentity.IdentityNo);
                    }
                    else //修改
                    {
                        _sellerUserSysNo = long.Parse(LbSellerList.SelectedValue.ToString());
                        OperationResult result = UserServiceProxy.BindUserIdentity(_sellerUserSysNo, UserType.Customer, userIdentity);

                        icardInfoDic.Remove(_sellerUserSysNo);
                        _sellerIdentityNoIDic.Remove(_sellerUserSysNo);
                        _sellerIdentityNoIDic.Add(_sellerUserSysNo, userIdentity.IdentityNo);
                        LbSellerList.SelectedItem = null;

                        BtnSellerRevoke_Click(sender, e);
                    }

                    #endregion

                    #endregion
                    break;
                case CertificateType.CertificateOfOfficers:
                    #region 军官证
                    //001-2检查数据合法性
                    checkDataResult = CheckSellerDataNew(CertificateType.CertificateOfOfficers);
                    if (!checkDataResult) { return; }

                    //赋值
                    userIdentity = AddCertificateOfOfficers(user, CertificateOfOfficersSeller);

                    #region 判断是添加操作还是修改操作

                    if (_operationStatusSeller == false) //添加
                    {
                        OperationResult result = AddUserIdentity(userIdentity, CustomerType.Seller);
                        if (result.OtherData.Count == 0)
                        {
                            MessageBox.Show("用户保存失败");
                            return;
                        }

                        //long sUserSysNo
                        _sellerUserSysNo = result.OtherData["UserSysNo"] == null
                            ? -1
                            : Convert.ToInt64(result.OtherData["UserSysNo"]);

                        if (_sellerUserSysNo <= 0)
                        {
                            throw new ArgumentException("用户保存失败");
                        }

                        if (_sellerIdentityNoIDic.Count > 0)
                        {
                            foreach (string iDic in _sellerIdentityNoIDic.Values)
                            {
                                if (iDic == userIdentity.IdentityNo)
                                {
                                    MessageBox.Show("该证件号买方已存在！");
                                    return;
                                }
                            }
                        }
                        _sellerIdentityNoIDic.Add(_sellerUserSysNo, userIdentity.IdentityNo);
                    }
                    else //修改
                    {
                        _sellerUserSysNo = long.Parse(LbSellerList.SelectedValue.ToString());
                        OperationResult result = UserServiceProxy.BindUserIdentity(_sellerUserSysNo, UserType.Customer, userIdentity);

                        icardInfoDic.Remove(_sellerUserSysNo);
                        _sellerIdentityNoIDic.Remove(_sellerUserSysNo);
                        _sellerIdentityNoIDic.Add(_sellerUserSysNo, userIdentity.IdentityNo);
                        LbSellerList.SelectedItem = null;

                        BtnSellerRevoke_Click(sender, e);
                    }

                    #endregion

                    #endregion
                    break;
                case CertificateType.ResidenceBooklet:
                    #region 护照
                    //001-2检查数据合法性
                    checkDataResult = CheckSellerDataNew(CertificateType.ResidenceBooklet);
                    if (!checkDataResult) { return; }

                    //赋值
                    userIdentity = AddResidenceBooklet(user, ResidenceBookletSeller);

                    #region 判断是添加操作还是修改操作

                    if (_operationStatusSeller == false) //添加
                    {
                        OperationResult result = AddUserIdentity(userIdentity, CustomerType.Seller);
                        if (result.OtherData.Count == 0)
                        {
                            MessageBox.Show("用户保存失败");
                            return;
                        }

                        //long sUserSysNo
                        _sellerUserSysNo = result.OtherData["UserSysNo"] == null
                            ? -1
                            : Convert.ToInt64(result.OtherData["UserSysNo"]);

                        if (_sellerUserSysNo <= 0)
                        {
                            throw new ArgumentException("用户保存失败");
                        }

                        if (_sellerIdentityNoIDic.Count > 0)
                        {
                            foreach (string iDic in _sellerIdentityNoIDic.Values)
                            {
                                if (iDic == userIdentity.IdentityNo)
                                {
                                    MessageBox.Show("该证件号买方已存在！");
                                    return;
                                }
                            }
                        }
                        _sellerIdentityNoIDic.Add(_sellerUserSysNo, userIdentity.IdentityNo);
                    }
                    else //修改
                    {
                        _sellerUserSysNo = long.Parse(LbSellerList.SelectedValue.ToString());
                        OperationResult result = UserServiceProxy.BindUserIdentity(_sellerUserSysNo, UserType.Customer, userIdentity);

                        icardInfoDic.Remove(_sellerUserSysNo);
                        _sellerIdentityNoIDic.Remove(_sellerUserSysNo);
                        _sellerIdentityNoIDic.Add(_sellerUserSysNo, userIdentity.IdentityNo);
                        LbSellerList.SelectedItem = null;

                        BtnSellerRevoke_Click(sender, e);
                    }

                    #endregion

                    #endregion
                    break;
            }
            #region 清空数据
            //ClearIdCard(IdCardSeller);
            //ClearPassport(PassportSeller);
            //ClearHKMacaoIdCard(HKMacaoIdCardSeller);
            //ClearBirthCertificate(BirthCertificateSeller);
            //ClearMacaoPermit(MacaoPermitSeller);
            //ClearCertificateOfOfficers(CertificateOfOfficersSeller);
            //ClearResidenceBooklet(ResidenceBookletSeller);
            ClearSellerIDType();
            #endregion

            _sellerNameDic[_sellerUserSysNo] = user.Name;
            BindData(LbSellerList, _sellerNameDic);

            #endregion
        }

        private void BtnSellerSave_ClickOld(object sender, RoutedEventArgs e)
        {
            var checkDataResult = false;
            UserIdentityModel user = new UserIdentityModel();

            #region 判别证件类型

            if (CboSellerIdentity.SelectedValue.ToString() == "0")
            {
                #region 身份证

                //合法性检查
                if (string.IsNullOrEmpty(TblSellerName.Text) ||
                    string.IsNullOrEmpty(TblSellerGender.Text) ||
                    string.IsNullOrEmpty(TblSellerBirthday.Text) ||
                    string.IsNullOrEmpty(TblSellerNation.Text) ||
                    string.IsNullOrEmpty(TblSellerAddress.Text) ||
                    string.IsNullOrEmpty(TblSellerIdentityNo.Text) ||
                    string.IsNullOrEmpty(TblSellerVisaAgency.Text) ||
                    string.IsNullOrEmpty(TblSellerEffectiveDate.Text) ||
                    string.IsNullOrEmpty(TblSellerExpiryDate.Text))
                {
                    MessageBox.Show("信息资料不合法，保存失败");
                    return;
                }

                //001-2检查数据合法性
                checkDataResult = CheckSellerData(CertificateType.IdCard);
                if (!checkDataResult) { return; }

                #region 赋值

                user.CertificateType = CertificateType.IdCard;
                user.Name = TblSellerName.Text;
                if (TblSellerGender.Text == "男")
                {
                    user.Gender = Gender.Male;
                }
                else if (TblSellerGender.Text == "女")
                {
                    user.Gender = Gender.Female;
                }
                else
                {
                    user.Gender = Gender.Unknown;
                }

                if (TblSellerNation.Text != null)
                    user.Nation = (Nation)Nation.Parse(typeof(Nation), TblSellerNation.Text); //字符串转枚举
                user.Birthday = Convert.ToDateTime(TblSellerBirthday.Text);
                user.Address = TblSellerAddress.Text;
                user.IdentityNo = TblSellerIdentityNo.Text;
                user.VisaAgency = TblSellerVisaAgency.Text;
                user.EffectiveDate = Convert.ToDateTime(TblSellerEffectiveDate.Text);
                if (TblSellerExpiryDate.Text != "长期")
                {
                    user.ExpiryDate = Convert.ToDateTime(TblSellerExpiryDate.Text);
                }
                else
                {
                    user.ExpiryDate = null;
                }

                #endregion

                #region 判断是添加操作还是修改操作

                if (_operationStatusSeller == false) //添加
                {
                    OperationResult result = AddUserIdentity(user, CustomerType.Seller);

                    if (result.OtherData.Count == 0)
                    {
                        MessageBox.Show("用户保存失败");
                        return;
                    }

                    //long sUserSysNo
                    _sellerUserSysNo = result.OtherData["UserSysNo"] == null
                        ? -1
                        : Convert.ToInt64(result.OtherData["UserSysNo"]);

                    if (_sellerUserSysNo <= 0)
                    {
                        throw new ArgumentException("用户保存失败");
                    }

                    if (_sellerIdentityNoIDic.Count > 0)
                    {
                        foreach (string iDic in _sellerIdentityNoIDic.Values)
                        {
                            if (iDic == user.IdentityNo)
                            {
                                MessageBox.Show("该证件号卖方已存在！");
                                return;
                            }
                        }
                    }
                    _sellerIdentityNoIDic.Add(_sellerUserSysNo, user.IdentityNo);

                }
                else //修改
                {
                    _sellerUserSysNo = long.Parse(LbSellerList.SelectedValue.ToString());
                    OperationResult result = UserServiceProxy.BindUserIdentity(_sellerUserSysNo, UserType.Customer, user);

                    //_sellerDictionary.Remove(LbSellerList.SelectedValue.ToString());
                    //_sellerDictionary.Add(user.IdentityNo, _sellerUserSysNo);
                    icardInfoDic.Remove(_sellerUserSysNo);
                    _sellerIdentityNoIDic.Remove(_sellerUserSysNo);
                    _sellerIdentityNoIDic.Add(_sellerUserSysNo, user.IdentityNo);

                    LbSellerList.SelectedItem = null;
                }

                #endregion

                WipeDataSellerIdCard();
                WipeDataSellerPassport();

                #endregion
            }
            else
            {
                #region 其他

                //if (string.IsNullOrEmpty(TbSellerName.Text))
                //{
                //    MessageBox.Show("用户名不能为空！");
                //    return;
                //}

                //if (RdoBtnSellerGenderMale.IsChecked == null ||
                //    RdoBtnSellerGenderFemale.IsChecked == null ||
                //    (RdoBtnSellerGenderFemale.IsChecked == false && RdoBtnSellerGenderMale.IsChecked == false))
                //{
                //    MessageBox.Show("请选择性别！");
                //    return;
                //}

                //if (string.IsNullOrEmpty(TbSellerIdentityNo.Text))
                //{
                //    MessageBox.Show("请填写证件编号！");
                //    return;
                //}

                //001-2检查数据合法性
                checkDataResult = CheckSellerData(CertificateType.Passport);
                if (!checkDataResult) { return; }

                #region 赋值

                user.CertificateType = CertificateType.Passport;
                user.Name = TbSellerName.Text;
                if (RdoBtnSellerGenderMale.IsChecked == true)
                {
                    user.Gender = Gender.Male;
                }
                else if (RdoBtnSellerGenderFemale.IsChecked == true)
                {
                    user.Gender = Gender.Female;
                }
                else
                {
                    user.Gender = Gender.Unknown;
                }
                user.Birthday = Convert.ToDateTime(TbSellerBirthday.Text.Trim());
                user.Nationality = CboSellerNationality.Text.Trim();
                user.IdentityNo = TbSellerIdentityNo.Text.Trim();

                #endregion

                #region 判断是添加操作还是修改操作

                if (_operationStatusSeller == false) //添加
                {
                    OperationResult result = AddUserIdentity(user, CustomerType.Seller);
                    if (result.OtherData.Count == 0)
                    {
                        MessageBox.Show("用户保存失败");
                        return;
                    }

                    //long sUserSysNo
                    _sellerUserSysNo = result.OtherData["UserSysNo"] == null
                        ? -1
                        : Convert.ToInt64(result.OtherData["UserSysNo"]);

                    if (_sellerUserSysNo <= 0)
                    {
                        throw new ArgumentException("用户保存失败");
                    }

                    if (_sellerIdentityNoIDic.Count > 0)
                    {
                        foreach (string iDic in _sellerIdentityNoIDic.Values)
                        {
                            if (iDic == user.IdentityNo)
                            {
                                MessageBox.Show("该证件号卖方已存在！");
                                return;
                            }
                        }
                    }
                    _sellerIdentityNoIDic.Add(_sellerUserSysNo, user.IdentityNo);

                }
                else //修改
                {
                    _sellerUserSysNo = long.Parse(LbSellerList.SelectedValue.ToString());
                    OperationResult result = UserServiceProxy.BindUserIdentity(_sellerUserSysNo, UserType.Customer, user);
                    icardInfoDic.Remove(_sellerUserSysNo);
                    _sellerIdentityNoIDic.Remove(_sellerUserSysNo);
                    _sellerIdentityNoIDic.Add(_sellerUserSysNo, user.IdentityNo);

                    LbSellerList.SelectedItem = null;

                    BtnSellerRevoke_Click(sender, e);
                }

                #endregion

                WipeDataSellerIdCard();
                WipeDataSellerPassport();

                #endregion
            }

            //_sellerNameDic.Add(_sellerUserSysNo, user.Name);
            _sellerNameDic[_sellerUserSysNo] = user.Name;
            BindData(LbSellerList, _sellerNameDic);

            #endregion

        }

        #endregion

        #region 判空

        /// <summary>
        /// 身份证判空
        /// </summary>
        /// <param name="idCard"></param>
        /// <returns></returns>
        private bool JudgeEmptyIdCard(IdCardControl idCard)
        {
            if (string.IsNullOrEmpty(TblBuyerName.Text) ||
                            string.IsNullOrEmpty(TblBuyerGender.Text) ||
                            string.IsNullOrEmpty(TblBuyerBirthday.Text) ||
                            string.IsNullOrEmpty(TblBuyerNation.Text) ||
                            string.IsNullOrEmpty(TblBuyerAddress.Text) ||
                            string.IsNullOrEmpty(TblBuyerIdentityNo.Text) ||
                            string.IsNullOrEmpty(TblBuyerVisaAgency.Text) ||
                            string.IsNullOrEmpty(TblBuyerEffectiveDate.Text) ||
                            string.IsNullOrEmpty(TblBuyerExpiryDate.Text))
            {
                MessageBox.Show("身份证信息资料不合法，无法进行保存操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }

        /// <summary>
        /// 护照判空
        /// </summary>
        /// <param name="passport"></param>
        /// <returns></returns>
        private bool JudgeEmptyPassport(PassportControl passport)
        {
            //姓名验证
            if (string.IsNullOrEmpty(passport.TbName.Text))
            {
                MessageBox.Show("姓名不能为空，无法进行保存操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            ////身份性别验证
            //if (passport.RdobtnMale.IsChecked == null ||
            //    passport.RdobtnFemale.IsChecked == null ||
            //    (passport.RdobtnFemale.IsChecked == false && passport.RdobtnMale.IsChecked == false))
            //{
            //    MessageBox.Show("请选择性别.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////出生日期验证
            //if (string.IsNullOrEmpty(passport.DateBirthday.Text))
            //{
            //    MessageBox.Show("请选择出生日期.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////国籍验证
            //if (string.IsNullOrEmpty(passport.CboNationality.Text))
            //{
            //    MessageBox.Show("请选择国籍.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            //证件号码验证
            if (string.IsNullOrEmpty(passport.TbIdentityNo.Text))
            {
                MessageBox.Show("请填写证件号码.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }

        /// <summary>
        /// 港澳居民身份证判空
        /// </summary>
        /// <param name="hKMacaoIdCard"></param>
        /// <returns></returns>
        private bool JudgeEmptyHKMacaoIdCard(HKMacaoIdCardControl hKMacaoIdCard)
        {
            //姓名验证
            if (string.IsNullOrEmpty(hKMacaoIdCard.TbName.Text))
            {
                MessageBox.Show("姓名不能为空，无法进行保存操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            ////身份性别验证
            //if (hKMacaoIdCard.RdobtnMale.IsChecked == null ||
            //    hKMacaoIdCard.RdobtnFemale.IsChecked == null ||
            //    (hKMacaoIdCard.RdobtnFemale.IsChecked == false && hKMacaoIdCard.RdobtnMale.IsChecked == false))
            //{
            //    MessageBox.Show("请选择性别.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////出生日期验证
            //if (string.IsNullOrEmpty(hKMacaoIdCard.DateBirthday.Text))
            //{
            //    MessageBox.Show("请选择出生日期.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            //证件号码验证
            if (string.IsNullOrEmpty(hKMacaoIdCard.TbIdentityNo.Text))
            {
                MessageBox.Show("请填写身份证号.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            ////签发日期验证
            //if (string.IsNullOrEmpty(hKMacaoIdCard.DateVisaAgency.Text))
            //{
            //    MessageBox.Show("请选择签发日期.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            return true;
        }

        /// <summary>
        /// 出生证判空
        /// </summary>
        /// <param name="birthCertificate"></param>
        /// <returns></returns>
        private bool JudgeEmptyBirthCertificate(BirthCertificateControl birthCertificate)
        {
            //姓名验证
            if (string.IsNullOrEmpty(birthCertificate.TbName.Text))
            {
                MessageBox.Show("姓名不能为空，无法进行保存操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            //身份性别验证
            //if (birthCertificate.RdobtnMale.IsChecked == null ||
            //    birthCertificate.RdobtnFemale.IsChecked == null ||
            //    (birthCertificate.RdobtnFemale.IsChecked == false && birthCertificate.RdobtnMale.IsChecked == false))
            //{
            //    MessageBox.Show("请选择性别.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////出生日期验证
            //if (string.IsNullOrEmpty(birthCertificate.DateBirthday.Text))
            //{
            //    MessageBox.Show("请选择出生日期.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            //出生证编号验证
            if (string.IsNullOrEmpty(birthCertificate.TbIdentityNo.Text))
            {
                MessageBox.Show("请填写出生证编号.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            ////接生机构验证
            //if (string.IsNullOrEmpty(birthCertificate.TbVisaAgency.Text))
            //{
            //    MessageBox.Show("请填写接生机构.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            return true;
        }

        /// <summary>
        /// 港澳居民往来大陆通行证判空
        /// </summary>
        /// <param name="macaoPermit"></param>
        /// <returns></returns>
        private bool JudgeEmptyMacaoPermit(MacaoPermitControl macaoPermit)
        {
            //姓名验证
            if (string.IsNullOrEmpty(macaoPermit.TbName.Text))
            {
                MessageBox.Show("姓名不能为空，无法进行保存操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            ////身份性别验证
            //if (macaoPermit.RdobtnMale.IsChecked == null ||
            //    macaoPermit.RdobtnFemale.IsChecked == null ||
            //    (macaoPermit.RdobtnFemale.IsChecked == false && macaoPermit.RdobtnMale.IsChecked == false))
            //{
            //    MessageBox.Show("请选择性别.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////出生日期验证
            //if (string.IsNullOrEmpty(macaoPermit.DateBirthday.Text))
            //{
            //    MessageBox.Show("请选择出生日期.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            //证件号码验证
            if (string.IsNullOrEmpty(macaoPermit.TbIdentityNo.Text))
            {
                MessageBox.Show("请填写证件号码.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            ////签发机构验证
            //if (string.IsNullOrEmpty(macaoPermit.TbVisaAgency.Text))
            //{
            //    MessageBox.Show("请填写签发机构.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////有效期限验证
            //if (string.IsNullOrEmpty(macaoPermit.TbEffectiveDate.Text) && string.IsNullOrEmpty(macaoPermit.TbExpiryDate.Text))
            //{
            //    MessageBox.Show("请填写有效期限.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}

            //有效期限验证
            if (macaoPermit.TbEffectiveDate.Text != "")
            {
                try
                {
                    if (int.Parse(macaoPermit.TbEffectiveDate.Text.Trim()) < 1900)
                    {
                        MessageBox.Show("有效期限输入不正确.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return false;
                    }
                    //if (int.Parse(macaoPermit.TbEffectiveDate.Text.Trim()) > DateTime.Now.Year)
                    //{
                    //    MessageBox.Show("有效期限输入不正确.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    //    return false;
                    //}
                }
                catch (Exception ex)
                {
                    MessageBox.Show("有效期限只能是数字.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
            }

            if (macaoPermit.TbExpiryDate.Text != "")
            {
                try
                {
                    if (int.Parse(macaoPermit.TbExpiryDate.Text.Trim()) < 1900)
                    {
                        MessageBox.Show("有效期限输入不正确.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return false;
                    }
                    //if (int.Parse(macaoPermit.TbExpiryDate.Text.Trim()) > DateTime.Now.Year)
                    //{
                    //    MessageBox.Show("有效期限输入不正确.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    //    return false;
                    //}
                }
                catch (Exception ex)
                {
                    MessageBox.Show("有效期限只能是数字.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 台湾居民往来大陆通行证判空
        /// </summary>
        /// <param name="taiwanPermit"></param>
        /// <returns></returns>
        private bool JudgeEmptyTaiwanPermit(TaiwanPermitControl taiwanPermit)
        {
            //姓名验证
            if (string.IsNullOrEmpty(taiwanPermit.TbName.Text))
            {
                MessageBox.Show("姓名不能为空，无法进行保存操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            ////身份性别验证
            //if (taiwanPermit.RdobtnMale.IsChecked == null ||
            //    taiwanPermit.RdobtnFemale.IsChecked == null ||
            //    (taiwanPermit.RdobtnFemale.IsChecked == false && taiwanPermit.RdobtnMale.IsChecked == false))
            //{
            //    MessageBox.Show("请选择性别.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////出生日期验证
            //if (string.IsNullOrEmpty(taiwanPermit.DateBirthday.Text))
            //{
            //    MessageBox.Show("请选择出生日期.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            //证件号码验证
            if (string.IsNullOrEmpty(taiwanPermit.TbIdentityNo.Text))
            {
                MessageBox.Show("请填写证件号码.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            ////签发机构验证
            //if (string.IsNullOrEmpty(taiwanPermit.TbVisaAgency.Text))
            //{
            //    MessageBox.Show("请填写签发机构.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////有效期限验证
            //if (string.IsNullOrEmpty(taiwanPermit.TbEffectiveDate.Text) && string.IsNullOrEmpty(taiwanPermit.TbExpiryDate.Text))
            //{
            //    MessageBox.Show("请填写有效期限.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}

            //有效期限验证
            if (taiwanPermit.TbEffectiveDate.Text != "")
            {
                try
                {
                    if (int.Parse(taiwanPermit.TbEffectiveDate.Text.Trim()) < 1900)
                    {
                        MessageBox.Show("有效期限输入不正确.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return false;
                    }
                    //if (int.Parse(taiwanPermit.TbEffectiveDate.Text.Trim()) > DateTime.Now.Year)
                    //{
                    //    MessageBox.Show("有效期限输入不正确.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    //    return false;
                    //}
                }
                catch (Exception ex)
                {
                    MessageBox.Show("有效期限只能是数字.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
            }

            if (taiwanPermit.TbExpiryDate.Text != "")
            {
                try
                {
                    if (int.Parse(taiwanPermit.TbExpiryDate.Text.Trim()) < 1900)
                    {
                        MessageBox.Show("有效期限输入不正确.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return false;
                    }
                    //if (int.Parse(taiwanPermit.TbExpiryDate.Text.Trim()) > DateTime.Now.Year)
                    //{
                    //    MessageBox.Show("有效期限输入不正确.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    //    return false;
                    //}
                }
                catch (Exception ex)
                {
                    MessageBox.Show("有效期限只能是数字.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 军官证判空
        /// </summary>
        /// <param name="certificateOfOfficers"></param>
        /// <returns></returns>
        private bool JudgeEmptyCertificateOfOfficers(CertificateOfOfficersControl certificateOfOfficers)
        {
            //姓名验证
            if (string.IsNullOrEmpty(certificateOfOfficers.TbName.Text))
            {
                MessageBox.Show("姓名不能为空，无法进行保存操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            ////身份性别验证
            //if (certificateOfOfficers.RdobtnMale.IsChecked == null ||
            //    certificateOfOfficers.RdobtnFemale.IsChecked == null ||
            //    (certificateOfOfficers.RdobtnFemale.IsChecked == false && certificateOfOfficers.RdobtnMale.IsChecked == false))
            //{
            //    MessageBox.Show("请选择性别.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////出生日期验证
            //if (string.IsNullOrEmpty(certificateOfOfficers.DateBirthday.Text))
            //{
            //    MessageBox.Show("请选择出生日期.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////民族验证
            //if (string.IsNullOrEmpty(certificateOfOfficers.TbNation.Text))
            //{
            //    MessageBox.Show("请填写民族.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////部别验证
            //if (string.IsNullOrEmpty(certificateOfOfficers.TbDepartmental.Text))
            //{
            //    MessageBox.Show("请填写部别.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            //身份证号验证
            if (string.IsNullOrEmpty(certificateOfOfficers.TbIdentityNo.Text))
            {
                MessageBox.Show("请填写身份证号.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (!ValidationCommon.IsIdMatch(certificateOfOfficers.TbIdentityNo.Text))
            {
                MessageBox.Show("身份证号不合法，请重新填写.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            ////发证机关验证
            //if (string.IsNullOrEmpty(certificateOfOfficers.TbVisaAgency.Text))
            //{
            //    MessageBox.Show("请填写发证机关.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////有效期限验证
            //if (string.IsNullOrEmpty(certificateOfOfficers.TbEffectiveDate.Text) && string.IsNullOrEmpty(certificateOfOfficers.TbExpiryDate.Text))
            //{
            //    MessageBox.Show("请填写有效期限.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}

            //有效期限验证
            if (certificateOfOfficers.TbEffectiveDate.Text != "")
            {
                try
                {
                    if (int.Parse(certificateOfOfficers.TbEffectiveDate.Text.Trim()) < 1900)
                    {
                        MessageBox.Show("有效期限输入不正确.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return false;
                    }
                    //if (int.Parse(certificateOfOfficers.TbEffectiveDate.Text.Trim()) > DateTime.Now.Year)
                    //{
                    //    MessageBox.Show("有效期限输入不正确.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    //    return false;
                    //}
                }
                catch (Exception ex)
                {
                    MessageBox.Show("有效期限只能是数字.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
            }

            if (certificateOfOfficers.TbExpiryDate.Text != "")
            {
                try
                {
                    if (int.Parse(certificateOfOfficers.TbExpiryDate.Text.Trim()) < 1900)
                    {
                        MessageBox.Show("有效期限输入不正确.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return false;
                    }
                    //if (int.Parse(certificateOfOfficers.TbExpiryDate.Text.Trim()) > DateTime.Now.Year)
                    //{
                    //    MessageBox.Show("有效期限输入不正确.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    //    return false;
                    //}
                }
                catch (Exception ex)
                {
                    MessageBox.Show("有效期限只能是数字.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 户口簿判空
        /// </summary>
        /// <param name="residenceBooklet"></param>
        /// <returns></returns>
        private bool JudgeEmptyResidenceBooklet(ResidenceBookletControl residenceBooklet)
        {
            //姓名验证
            if (string.IsNullOrEmpty(residenceBooklet.TbName.Text))
            {
                MessageBox.Show("姓名不能为空，无法进行保存操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            //身份性别验证
            //if (residenceBooklet.RdobtnMale.IsChecked == null ||
            //    residenceBooklet.RdobtnFemale.IsChecked == null ||
            //    (residenceBooklet.RdobtnFemale.IsChecked == false && residenceBooklet.RdobtnMale.IsChecked == false))
            //{
            //    MessageBox.Show("请选择性别.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////出生日期验证
            //if (string.IsNullOrEmpty(residenceBooklet.DateBirthday.Text))
            //{
            //    MessageBox.Show("请选择出生日期.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            //身份证号验证
            if (string.IsNullOrEmpty(residenceBooklet.TbIdentityNo.Text))
            {
                MessageBox.Show("请填写身份证号.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (!ValidationCommon.IsIdMatch(residenceBooklet.TbIdentityNo.Text))
            {
                MessageBox.Show("身份证号不合法，请重新填写.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            ////民族验证
            //if (string.IsNullOrEmpty(residenceBooklet.TbNation.Text))
            //{
            //    MessageBox.Show("请填写民族.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////住址验证
            //if (string.IsNullOrEmpty(residenceBooklet.TbAddress.Text))
            //{
            //    MessageBox.Show("请填写住址.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            return true;
        }

        #endregion

        private bool CheckBuyerDataNew(CertificateType certificateType)
        {
            try
            {
                switch (certificateType)
                {
                    case CertificateType.Passport:
                        return JudgeEmptyPassport(PassportBuyer);
                        break;
                    case CertificateType.HKMacaoIdCard:
                        return JudgeEmptyHKMacaoIdCard(HKMacaoIdCardBuyer);
                        break;
                    case CertificateType.BirthCertificate:
                        return JudgeEmptyBirthCertificate(BirthCertificateBuyer);
                        break;
                    case CertificateType.MacaoPermit:
                        return JudgeEmptyMacaoPermit(MacaoPermitBuyer);
                        break;
                    case CertificateType.CertificateOfOfficers:
                        return JudgeEmptyCertificateOfOfficers(CertificateOfOfficersBuyer);
                        break;
                    case CertificateType.ResidenceBooklet:
                        return JudgeEmptyResidenceBooklet(ResidenceBookletBuyer);
                        break;
                }
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show("数据合法性检查发生异常：" + e.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return false;
        }

        private bool CheckSellerDataNew(CertificateType certificateType)
        {
            try
            {
                switch (certificateType)
                {
                    case CertificateType.Passport:
                        return JudgeEmptyPassport(PassportSeller);
                        break;
                    case CertificateType.HKMacaoIdCard:
                        return JudgeEmptyHKMacaoIdCard(HKMacaoIdCardSeller);
                        break;
                    case CertificateType.BirthCertificate:
                        return JudgeEmptyBirthCertificate(BirthCertificateSeller);
                        break;
                    case CertificateType.MacaoPermit:
                        return JudgeEmptyMacaoPermit(MacaoPermitSeller);
                        break;
                    case CertificateType.CertificateOfOfficers:
                        return JudgeEmptyCertificateOfOfficers(CertificateOfOfficersSeller);
                        break;
                    case CertificateType.ResidenceBooklet:
                        return JudgeEmptyResidenceBooklet(ResidenceBookletSeller);
                        break;
                }
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show("数据合法性检查发生异常：" + e.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return false;
        }

        /// <summary>
        /// 买方数据合法性检查
        /// </summary>
        /// <param name="certificateType">证件类型</param>
        /// <returns></returns>
        private bool CheckBuyerData(CertificateType certificateType)
        {
            try
            {
                switch (certificateType)
                {
                    case CertificateType.IdCard:
                        if (string.IsNullOrEmpty(TblBuyerName.Text) ||
                            string.IsNullOrEmpty(TblBuyerGender.Text) ||
                            string.IsNullOrEmpty(TblBuyerBirthday.Text) ||
                            string.IsNullOrEmpty(TblBuyerNation.Text) ||
                            string.IsNullOrEmpty(TblBuyerAddress.Text) ||
                            string.IsNullOrEmpty(TblBuyerIdentityNo.Text) ||
                            string.IsNullOrEmpty(TblBuyerVisaAgency.Text) ||
                            string.IsNullOrEmpty(TblBuyerEffectiveDate.Text) ||
                            string.IsNullOrEmpty(TblBuyerExpiryDate.Text))
                        {
                            MessageBox.Show("身份证信息资料不合法，无法进行保存操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return false;
                        }
                        break;
                    case CertificateType.Passport:
                        //姓名验证
                        if (string.IsNullOrEmpty(TbBuyerName.Text))
                        {
                            MessageBox.Show("用户名不能为空，无法进行保存操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return false;
                        }
                        //身份性别验证
                        if (RdoBtnBuyerGenderMale.IsChecked == null ||
                            RdoBtnBuyerGenderFemale.IsChecked == null ||
                            (RdoBtnBuyerGenderFemale.IsChecked == false && RdoBtnBuyerGenderMale.IsChecked == false))
                        {
                            MessageBox.Show("请选择性别.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return false;
                        }
                        //出生日期验证
                        if (string.IsNullOrEmpty(TbBuyerBirthday.Text))
                        {
                            MessageBox.Show("请选择出生日期.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return false;
                        }
                        //国籍验证
                        if (string.IsNullOrEmpty(CboBuyerNationality.Text))
                        {
                            MessageBox.Show("请选择国籍.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return false;
                        }
                        //证件号码验证
                        if (string.IsNullOrEmpty(TbBuyerIdentityNo.Text))
                        {
                            MessageBox.Show("请填写证件编号.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return false;
                        }
                        break;
                }
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show("数据合法性检查发生异常：" + e.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return false;
        }

        /// <summary>
        /// 卖家数据合法性检查
        /// </summary>
        /// <param name="certificateType"></param>
        /// <returns></returns>
        private bool CheckSellerData(CertificateType certificateType)
        {
            try
            {
                switch (certificateType)
                {
                    case CertificateType.IdCard:
                        if (string.IsNullOrEmpty(TblSellerName.Text) ||
                            string.IsNullOrEmpty(TblSellerGender.Text) ||
                            string.IsNullOrEmpty(TblSellerBirthday.Text) ||
                            string.IsNullOrEmpty(TblSellerNation.Text) ||
                            string.IsNullOrEmpty(TblSellerAddress.Text) ||
                            string.IsNullOrEmpty(TblSellerIdentityNo.Text) ||
                            string.IsNullOrEmpty(TblSellerVisaAgency.Text) ||
                            string.IsNullOrEmpty(TblSellerEffectiveDate.Text) ||
                            string.IsNullOrEmpty(TblSellerExpiryDate.Text))
                        {
                            MessageBox.Show("身份证信息资料不合法，无法进行保存操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return false;
                        }
                        break;
                    case CertificateType.Passport:
                        //姓名验证
                        if (string.IsNullOrEmpty(TbSellerName.Text))
                        {
                            MessageBox.Show("用户名不能为空，无法进行保存操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return false;
                        }
                        //身份性别验证
                        if (RdoBtnSellerGenderMale.IsChecked == null ||
                            RdoBtnSellerGenderFemale.IsChecked == null ||
                            (RdoBtnSellerGenderFemale.IsChecked == false && RdoBtnSellerGenderMale.IsChecked == false))
                        {
                            MessageBox.Show("请选择性别.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return false;
                        }
                        //出生日期验证
                        if (string.IsNullOrEmpty(TbSellerBirthday.Text))
                        {
                            MessageBox.Show("请选择出生日期.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return false;
                        }
                        //国籍验证
                        if (string.IsNullOrEmpty(CboSellerNationality.Text))
                        {
                            MessageBox.Show("请选择国籍.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return false;
                        }
                        //证件号码验证
                        if (string.IsNullOrEmpty(TbSellerIdentityNo.Text))
                        {
                            MessageBox.Show("请填写证件编号.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return false;
                        }
                        break;
                }
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show("数据合法性检查发生异常：" + e.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return false;
        }



        #region 删除

        /// <summary>
        /// 删除买方信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnBuyerDelete_Click(object sender, RoutedEventArgs e)
        {
            if (LbBuyerList.Items.Count == 0 || LbBuyerList.SelectedValue == null)
            {
                MessageBox.Show("请选择要删除的数据");
                return;
            }
            MessageBoxResult confirmToDel = MessageBox.Show("确认要删除所选行吗？", "提示", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (confirmToDel == MessageBoxResult.Yes)
            {
                var sysno = long.Parse(LbBuyerList.SelectedValue.ToString());
                _buyerIdentityNoIDic.Remove(sysno);
                //_buyerDictionary.Remove(LbBuyerList.SelectedValue.ToString());
                _buyerNameDic.Remove(sysno);
                BindData(LbBuyerList, _buyerNameDic);
                //LbBuyerList.Items.Remove(LbBuyerList.SelectedItem);
                if (_buyerNameDic.Count > 0)
                {
                    //WipeDataBuyerIdCard();
                    //WipeDataBuyerPassport();

                    //New
                    //ClearIdCard(IdCardBuyer);
                    //ClearPassport(PassportBuyer);
                    //ClearHKMacaoIdCard(HKMacaoIdCardBuyer);
                    //ClearBirthCertificate(BirthCertificateBuyer);
                    //ClearMacaoPermit(MacaoPermitBuyer);
                    //ClearCertificateOfOfficers(CertificateOfOfficersBuyer);
                    //ClearResidenceBooklet(ResidenceBookletBuyer);
                    ClearBuyerIDType();
                }
                else
                {
                    LbBuyerList.ItemsSource = null;
                }
                BtnBuyerRevoke_Click(sender, e);
            }
            else
            {
                return;
            }

        }

        /// <summary>
        /// 删除卖家信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSellerDelete_Click(object sender, RoutedEventArgs e)
        {
            if (LbSellerList.Items.Count == 0 || LbSellerList.SelectedValue == null)
            {
                MessageBox.Show("请选择要删除的数据");
                return;
            }
            MessageBoxResult confirmToDel = MessageBox.Show("确认要删除所选行吗？", "提示", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (confirmToDel == MessageBoxResult.Yes)
            {
                var sysno = long.Parse(LbSellerList.SelectedValue.ToString());
                _sellerIdentityNoIDic.Remove(sysno);
                //_buyerDictionary.Remove(LbBuyerList.SelectedValue.ToString());
                _sellerNameDic.Remove(sysno);
                BindData(LbSellerList, _sellerNameDic);
                if (_sellerNameDic.Count > 0)
                {
                    //LbSellerList.Items.Remove(LbBuyerList.SelectedItem);

                    //WipeDataSellerIdCard();
                    //WipeDataSellerPassport(); 
                    
                    //New
                    //ClearIdCard(IdCardSeller);
                    //ClearPassport(PassportSeller);
                    //ClearHKMacaoIdCard(HKMacaoIdCardSeller);
                    //ClearBirthCertificate(BirthCertificateSeller);
                    //ClearMacaoPermit(MacaoPermitSeller);
                    //ClearCertificateOfOfficers(CertificateOfOfficersSeller);
                    //ClearResidenceBooklet(ResidenceBookletSeller);
                    ClearSellerIDType();
                }
                else
                {
                    LbSellerList.ItemsSource = null;
                }
                BtnSellerRevoke_Click(sender, e);
            }
        }

        #endregion

        #region 证件类型

        /// <summary>
        /// 买方证件类型切换
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboBuyerIdentity_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (CboBuyerIdentity.Text != "" && CboBuyerIdentity.SelectedValue.ToString() == "0")
            //{
            //    BtnBuyerRead.Visibility = Visibility.Visible;
            //    Grid_BuyerIdCard.Visibility = Visibility.Visible;
            //    Grid_BuyerPassport.Visibility = Visibility.Collapsed;
            //}
            //else
            //{
            //    BtnBuyerRead.Visibility = Visibility.Collapsed;
            //    Grid_BuyerIdCard.Visibility = Visibility.Collapsed;
            //    Grid_BuyerPassport.Visibility = Visibility.Visible;
            //}
            switch ((CertificateType)(int.Parse(CboBuyerIdentity.SelectedValue.ToString())))
            {
                case CertificateType.IdCard:
                    BuyerIDTypeCollapsed();
                    IdCardBuyer.Visibility = Visibility.Visible;
                    BtnBuyerRead.Visibility = Visibility.Visible;
                    ClearBuyerIDType();
                    break;
                case CertificateType.Passport:
                    BuyerIDTypeCollapsed();
                    PassportBuyer.Visibility = Visibility.Visible;
                    BtnBuyerRead.Visibility = Visibility.Collapsed;
                    ClearBuyerIDType();
                    break;
                case CertificateType.HKMacaoIdCard:
                    BuyerIDTypeCollapsed();
                    HKMacaoIdCardBuyer.Visibility = Visibility.Visible;
                    BtnBuyerRead.Visibility = Visibility.Collapsed;
                    ClearBuyerIDType();
                    break;
                case CertificateType.BirthCertificate:
                    BuyerIDTypeCollapsed();
                    BirthCertificateBuyer.Visibility = Visibility.Visible;
                    BtnBuyerRead.Visibility = Visibility.Collapsed;
                    ClearBuyerIDType();
                    break;
                case CertificateType.MacaoPermit:
                    BuyerIDTypeCollapsed();
                    MacaoPermitBuyer.Visibility = Visibility.Visible;
                    BtnBuyerRead.Visibility = Visibility.Collapsed;
                    ClearBuyerIDType();
                    break;
                case CertificateType.TaiwanPermit:
                    BuyerIDTypeCollapsed();
                    TaiwanPermitBuyer.Visibility = Visibility.Visible;
                    BtnBuyerRead.Visibility = Visibility.Collapsed;
                    ClearBuyerIDType();
                    break;
                case CertificateType.CertificateOfOfficers:
                    BuyerIDTypeCollapsed();
                    CertificateOfOfficersBuyer.Visibility = Visibility.Visible;
                    BtnBuyerRead.Visibility = Visibility.Collapsed;
                    ClearBuyerIDType();
                    break;
                case CertificateType.ResidenceBooklet:
                    BuyerIDTypeCollapsed();
                    ResidenceBookletBuyer.Visibility = Visibility.Visible;
                    BtnBuyerRead.Visibility = Visibility.Collapsed;
                    ClearBuyerIDType();
                    break;
            }
        }

        /// <summary>
        /// 卖家证件类型切换
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboSellerIdentity_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (CboSellerIdentity.Text != "" && CboSellerIdentity.SelectedValue.ToString() == "0")
            //{
            //    BtnSellerRead.Visibility = Visibility.Visible;
            //    Grid_SellerIdCard.Visibility = Visibility.Visible;
            //    Grid_SellerPassport.Visibility = Visibility.Collapsed;
            //}
            //else
            //{
            //    BtnSellerRead.Visibility = Visibility.Collapsed;
            //    Grid_SellerIdCard.Visibility = Visibility.Collapsed;
            //    Grid_SellerPassport.Visibility = Visibility.Visible;
            //}

            switch ((CertificateType)(int.Parse(CboSellerIdentity.SelectedValue.ToString())))
            {
                case CertificateType.IdCard:
                    SellerIDTypeCollapsed();
                    IdCardSeller.Visibility = Visibility.Visible;
                    BtnSellerRead.Visibility = Visibility.Visible;
                    ClearSellerIDType();
                    break;
                case CertificateType.Passport:
                    SellerIDTypeCollapsed();
                    PassportSeller.Visibility = Visibility.Visible;
                    BtnSellerRead.Visibility = Visibility.Collapsed;
                    ClearSellerIDType();
                    break;
                case CertificateType.HKMacaoIdCard:
                    SellerIDTypeCollapsed();
                    HKMacaoIdCardSeller.Visibility = Visibility.Visible;
                    BtnSellerRead.Visibility = Visibility.Collapsed;
                    ClearSellerIDType();
                    break;
                case CertificateType.BirthCertificate:
                    SellerIDTypeCollapsed();
                    BirthCertificateSeller.Visibility = Visibility.Visible;
                    BtnSellerRead.Visibility = Visibility.Collapsed;
                    ClearSellerIDType();
                    break;
                case CertificateType.MacaoPermit:
                    SellerIDTypeCollapsed();
                    MacaoPermitSeller.Visibility = Visibility.Visible;
                    BtnSellerRead.Visibility = Visibility.Collapsed;
                    ClearSellerIDType();
                    break;
                case CertificateType.TaiwanPermit:
                    SellerIDTypeCollapsed();
                    TaiwanPermitSeller.Visibility = Visibility.Visible;
                    BtnSellerRead.Visibility = Visibility.Collapsed;
                    ClearSellerIDType();
                    break;
                case CertificateType.CertificateOfOfficers:
                    SellerIDTypeCollapsed();
                    CertificateOfOfficersSeller.Visibility = Visibility.Visible;
                    BtnSellerRead.Visibility = Visibility.Collapsed;
                    ClearSellerIDType();
                    break;
                case CertificateType.ResidenceBooklet:
                    SellerIDTypeCollapsed();
                    ResidenceBookletSeller.Visibility = Visibility.Visible;
                    BtnSellerRead.Visibility = Visibility.Collapsed;
                    ClearSellerIDType();
                    break;
            }
        }

        #endregion

        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, "Main/PreCheck/PreCheckSearchListPage.xaml");
        }

        #region 买方、卖家、经纪人读取身份证信息
        /// <summary>
        /// 经纪人读取身份证信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAgent_Click(object sender, RoutedEventArgs e)
        {
            _idCardReadType = (int)IdCardRead.Agent;
            CreatIdCard();
        }

        /// <summary>
        /// 买方读取身份证信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnBuyerRead_Click(object sender, RoutedEventArgs e)
        {
            _idCardReadType = (int)IdCardRead.Buyer;
            CreatIdCard();
        }

        /// <summary>
        /// 卖家读取身份证信息
        /// </summary>
        private void BtnSellerRead_Click(object sender, RoutedEventArgs e)
        {
            _idCardReadType = (int)IdCardRead.Seller;
            CreatIdCard();
        }

        private void CreatIdCard()
        {
            IdCard idCard = new IdCard(IdentityInfo, _idCardReadType);
            //if (idCard.DeviceNotExists)
            //{
            //    MessageBox.Show("设备打开失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            //    return; 
            //}
              
            idCard.Closing += idCard_Closing;
            idCard.ShowDialog();
        }

        #endregion

        #region 读取身份证页面关闭事件

        /// <summary>
        ///读取身份证页面关闭
        /// </summary>      
        private void idCard_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //根据不同的类型赋值
            switch (_idCardReadType)
            {
                case 1:
                    //GetBuyerValue();
                    GetBuyerValueNew();
                    break;
                case 2:
                    //GetSellerValue();
                    GetSellerValueNew();
                    break;
                case 3:
                    GetAgentValue();
                    break;
                default:
                    MessageBox.Show("系统错误", "提示");
                    break;
            }
        }


        private void GetBuyerValueNew()
        {
            if (IdentityInfo.IdentityNo == null)
                return;

            if (!IdentityInfo.IsTrusted)
            {
                IdCardBuyer.TblName.Text = IdentityInfo.Name;
                IdCardBuyer.TblIdentityNo.Text = IdentityInfo.IdentityNo;
            }
            else
            {
                IdCardBuyer.TblName.Text = IdentityInfo.Name;
                switch (IdentityInfo.Gender)
                {
                    case Gender.Male:
                        IdCardBuyer.TblGender.Text = "男";
                        break;
                    case Gender.Female:
                        IdCardBuyer.TblGender.Text = "女";
                        break;
                    default:
                        IdCardBuyer.TblGender.Text = "";
                        break;
                }
                IdCardBuyer.TblNation.Text = IdentityInfo.Nation.ToString();
                if (IdentityInfo.Birthday != null)
                    IdCardBuyer.TblBirthday.Text = ((DateTime)IdentityInfo.Birthday).ToString("yyyy.MM.dd");
                IdCardBuyer.TblAddress.Text = IdentityInfo.Address;
                IdCardBuyer.TblIdentityNo.Text = IdentityInfo.IdentityNo;
                IdCardBuyer.TblVisaAgency.Text = IdentityInfo.VisaAgency;
                if (IdentityInfo.EffectiveDate != null)
                    IdCardBuyer.TblEffectiveDate.Text = ((DateTime)IdentityInfo.EffectiveDate).ToString("yyyy.MM.dd");
                IdCardBuyer.TblStr.Text = "至";
                if (IdentityInfo.ExpiryDate != null)
                {
                    IdCardBuyer.TblExpiryDate.Text = ((DateTime)IdentityInfo.ExpiryDate).ToString("yyyy.MM.dd");
                }
                else
                {
                    IdCardBuyer.TblExpiryDate.Text = "长期";
                }
                //照片显示
                try
                {
                    BitmapImage bitmap = new BitmapImage();
                    MemoryStream ms1 = new MemoryStream(IdentityInfo.Photo);

                    bitmap.BeginInit();
                    bitmap.StreamSource = ms1;
                    bitmap.EndInit();

                    IdCardBuyer.ImgPhoto.Source = bitmap;
                }
                catch (Exception)
                {
                    //throw new ArgumentException("照片读取失败");
                    MessageBox.Show("照片读取失败!", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
            }

        }

        private void GetSellerValueNew()
        {
            if (IdentityInfo.IdentityNo == null)
                return;

            if (!IdentityInfo.IsTrusted)
            {
                IdCardSeller.TblName.Text = IdentityInfo.Name;
                IdCardSeller.TblIdentityNo.Text = IdentityInfo.IdentityNo;
            }
            else
            {
                IdCardSeller.TblName.Text = IdentityInfo.Name;
                switch (IdentityInfo.Gender)
                {
                    case Gender.Male:
                        IdCardSeller.TblGender.Text = "男";
                        break;
                    case Gender.Female:
                        IdCardSeller.TblGender.Text = "女";
                        break;
                    default:
                        IdCardSeller.TblGender.Text = "";
                        break;
                }
                IdCardSeller.TblNation.Text = IdentityInfo.Nation.ToString();
                if (IdentityInfo.Birthday != null)
                    IdCardSeller.TblBirthday.Text = ((DateTime)IdentityInfo.Birthday).ToString("yyyy.MM.dd");
                IdCardSeller.TblAddress.Text = IdentityInfo.Address;
                IdCardSeller.TblIdentityNo.Text = IdentityInfo.IdentityNo;
                IdCardSeller.TblVisaAgency.Text = IdentityInfo.VisaAgency;
                if (IdentityInfo.EffectiveDate != null)
                    IdCardSeller.TblEffectiveDate.Text = ((DateTime)IdentityInfo.EffectiveDate).ToString("yyyy.MM.dd");
                IdCardSeller.TblStr.Text = "至";
                if (IdentityInfo.ExpiryDate != null)
                {
                    IdCardSeller.TblExpiryDate.Text = ((DateTime)IdentityInfo.ExpiryDate).ToString("yyyy.MM.dd");
                }
                else
                {
                    IdCardSeller.TblExpiryDate.Text = "长期";
                }
                //照片显示
                try
                {
                    BitmapImage bitmap = new BitmapImage();
                    MemoryStream ms1 = new MemoryStream(IdentityInfo.Photo);

                    bitmap.BeginInit();
                    bitmap.StreamSource = ms1;
                    bitmap.EndInit();

                    IdCardSeller.ImgPhoto.Source = bitmap;
                }
                catch (Exception)
                {
                    //throw new ArgumentException("照片读取失败");
                    MessageBox.Show("照片读取失败!", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
            }

        }


        private void GetBuyerValue()
        {
            if (IdentityInfo.IdentityNo == null)
                return;
            TblBuyerName.Text = IdentityInfo.Name;
            switch (IdentityInfo.Gender)
            {
                case Gender.Male:
                    TblBuyerGender.Text = "男";
                    break;
                case Gender.Female:
                    TblBuyerGender.Text = "女";
                    break;
                default:
                    TblBuyerGender.Text = "";
                    break;
            }
            TblBuyerNation.Text = IdentityInfo.Nation.ToString();
            if (IdentityInfo.Birthday != null)
                TblBuyerBirthday.Text = ((DateTime)IdentityInfo.Birthday).ToString("yyyy.MM.dd");
            TblBuyerAddress.Text = IdentityInfo.Address;
            TblBuyerIdentityNo.Text = IdentityInfo.IdentityNo;
            TblBuyerVisaAgency.Text = IdentityInfo.VisaAgency;
            if (IdentityInfo.EffectiveDate != null)
                TblBuyerEffectiveDate.Text = ((DateTime)IdentityInfo.EffectiveDate).ToString("yyyy.MM.dd");
            TblStr.Text = "至";
            if (IdentityInfo.ExpiryDate != null)
            {
                TblBuyerExpiryDate.Text = ((DateTime)IdentityInfo.ExpiryDate).ToString("yyyy.MM.dd");
            }
            else
            {
                TblBuyerExpiryDate.Text = "长期";
            }
            //照片显示
            try
            {
                BitmapImage bitmap = new BitmapImage();
                MemoryStream ms1 = new MemoryStream(IdentityInfo.Photo);

                bitmap.BeginInit();
                bitmap.StreamSource = ms1;
                bitmap.EndInit();

                ImgBuyerPhoto.Source = bitmap;
            }
            catch (Exception)
            {
                //throw new ArgumentException("照片读取失败");
                MessageBox.Show("照片读取失败!", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }

        private void GetSellerValue()
        {
            if (IdentityInfo.IdentityNo == null)
                return;
            TblSellerName.Text = IdentityInfo.Name;
            switch (IdentityInfo.Gender)
            {
                case Gender.Male:
                    TblSellerGender.Text = "男";
                    break;
                case Gender.Female:
                    TblSellerGender.Text = "女";
                    break;
                default:
                    TblSellerGender.Text = "";
                    break;
            }
            TblSellerNation.Text = IdentityInfo.Nation.ToString();
            if (IdentityInfo.Birthday != null)
                TblSellerBirthday.Text = ((DateTime)IdentityInfo.Birthday).ToString("yyyy.MM.dd");
            TblSellerAddress.Text = IdentityInfo.Address;
            TblSellerIdentityNo.Text = IdentityInfo.IdentityNo;
            TblSellerVisaAgency.Text = IdentityInfo.VisaAgency;
            if (IdentityInfo.EffectiveDate != null)
                TblSellerEffectiveDate.Text = ((DateTime)IdentityInfo.EffectiveDate).ToString("yyyy.MM.dd");
            TblStr.Text = "至";
            if (IdentityInfo.ExpiryDate != null)
            {
                TblSellerExpiryDate.Text = ((DateTime)IdentityInfo.ExpiryDate).ToString("yyyy.MM.dd");
            }
            else
            {
                TblSellerExpiryDate.Text = "长期";
            }
            //照片显示
            try
            {
                BitmapImage bitmap = new BitmapImage();
                MemoryStream ms1 = new MemoryStream(IdentityInfo.Photo);

                bitmap.BeginInit();
                bitmap.StreamSource = ms1;
                bitmap.EndInit();

                ImgSellerPhoto.Source = bitmap;
            }
            catch (Exception)
            {
                throw new ArgumentException("照片读取失败");
            }
        }

        private void GetAgentValue()
        {
            if (IdentityInfo.IdentityNo == null)
                return;
            int totalCount;
            var idNo = IdentityInfo.IdentityNo;
            //IList<AgentStaff> agentStaffs = AgentServiceProxy.GetAgentStaffs
            //    (1, 1, out totalCount, null, null, idNo);
            ////Todo：数据为什么绑定不上
            ////Test agent = new Test("张三", "地址");
            //Sp.DataContext = agentStaffs[0];

            IList<AgentStaff> agentStaffList = new List<AgentStaff>();

            agentStaffList = AgentServiceProxy.GetAgentStaffs(1, 10000, out totalCount, null, null, null, idNo);
            if (agentStaffList.Count > 0)
            {
                for (int i = 0; i < agentStaffList.Count; i++)
                {
                    AgentCompany agentCompany = AgentServiceProxy.GetAgentCompany(agentStaffList[i].AgentCompanySysNo);
                    if (agentCompany != null)
                    {
                        TblShowInfo.Text = "姓名：" + agentStaffList[i].RealName + "                 所属门店：" +
                                           agentCompany.CompanyName;
                    }

                }
            }
            else
            {
                MessageBox.Show("没有符合该身份证号的经纪人！");
                return;
            }

        }

        #endregion

        /// <summary>
        /// 搜索案件事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSearch_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, "Main/PreCheck/PreCheckSearchListPage.xaml");
        }

        #region 撤销

        /// <summary>
        /// 撤销买方证件信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnBuyerRevoke_Click(object sender, RoutedEventArgs e)
        {
            _operationStatusBuyer = false;

            CboBuyerIdentity.SelectedValue = (int)CertificateType.IdCard;

            CboBuyerIdentity.IsEnabled = true;
            CboBuyerIdentity.Visibility = Visibility.Visible;
            BtnBuyerRead.Visibility = Visibility.Visible;
            BtnBuyerRevoke.Visibility = Visibility.Collapsed;

            //Grid_BuyerPassport.Visibility = Visibility.Collapsed;
            //Grid_BuyerIdCard.Visibility = Visibility.Visible;

            //New
            BuyerIDTypeCollapsed();
            IdCardBuyer.Visibility = Visibility.Visible;

            LbBuyerList.SelectedItem = null;
            //WipeDataBuyerIdCard();
            //WipeDataBuyerPassport();

            //New
            //ClearIdCard(IdCardBuyer);
            //ClearPassport(PassportBuyer);
            //ClearHKMacaoIdCard(HKMacaoIdCardBuyer);
            //ClearBirthCertificate(BirthCertificateBuyer);
            //ClearMacaoPermit(MacaoPermitBuyer);
            //ClearCertificateOfOfficers(CertificateOfOfficersBuyer);
            //ClearResidenceBooklet(ResidenceBookletBuyer);
            ClearBuyerIDType();
        }

        /// <summary>
        /// 撤销卖家证件信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSellerRevoke_Click(object sender, RoutedEventArgs e)
        {
            _operationStatusSeller = false;

            CboSellerIdentity.SelectedValue = (int)CertificateType.IdCard;

            CboSellerIdentity.IsEnabled = true;
            CboSellerIdentity.Visibility = Visibility.Visible;
            BtnSellerRead.Visibility = Visibility.Visible;
            BtnSellerRevoke.Visibility = Visibility.Collapsed;

            //Grid_SellerPassport.Visibility = Visibility.Collapsed;
            //Grid_SellerIdCard.Visibility = Visibility.Visible;

            //New
            SellerIDTypeCollapsed();
            IdCardSeller.Visibility = Visibility.Visible;

            LbSellerList.SelectedItem = null;
            //WipeDataSellerIdCard();
            //WipeDataSellerPassport();

            //New
            //ClearIdCard(IdCardSeller);
            //ClearPassport(PassportSeller);
            //ClearHKMacaoIdCard(HKMacaoIdCardSeller);
            //ClearBirthCertificate(BirthCertificateSeller);
            //ClearMacaoPermit(MacaoPermitSeller);
            //ClearCertificateOfOfficers(CertificateOfOfficersSeller);
            //ClearResidenceBooklet(ResidenceBookletSeller);
            ClearSellerIDType();
        }

        #endregion


        #region 列表改变事件

        /// <summary>
        /// 身份证
        /// </summary>
        /// <param name="idCard"></param>
        /// <param name="userIdentity"></param>
        private void ChangeIdCard(IdCardControl idCard, UserIdentity userIdentity)
        {
            #region 身份证
            idCard.TblName.Text = userIdentity.Name; //姓名
            //性别
            if (userIdentity.Gender != null)
            {
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        idCard.TblGender.Text = "男";
                        break;
                    case Gender.Female:
                        idCard.TblGender.Text = "女";
                        break;
                    default:
                        idCard.TblGender.Text = "";
                        break;
                }
            }
            else
            {
                idCard.TblGender.Text = "";
            }
            //民族
            if (userIdentity.Nation != null)
            {
                idCard.TblNation.Text = userIdentity.Nation.ToString();
            }
            //出生
            if (userIdentity.Birthday != null)
                idCard.TblBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
            //地址
            if (!string.IsNullOrEmpty(userIdentity.Address))
            {
                idCard.TblAddress.Text = userIdentity.Address;
            }
            //身份证号
            if (!string.IsNullOrEmpty(userIdentity.Id))
            {
                idCard.TblIdentityNo.Text = userIdentity.Id;
            }
            //签发机关
            if (!string.IsNullOrEmpty(userIdentity.VisaAgency))
            {
                idCard.TblVisaAgency.Text = userIdentity.VisaAgency;
            }
            //有效期限-生效
            if (userIdentity.EffectiveDate != null)
            {
                idCard.TblEffectiveDate.Text = ((DateTime)userIdentity.EffectiveDate).ToString("yyyy.MM.dd");
                idCard.TblStr.Text = "至";
            }
            //有效期限-失效
            if (userIdentity.ExpiryDate != null)
            {
                idCard.TblExpiryDate.Text = ((DateTime)userIdentity.ExpiryDate).ToString("yyyy.MM.dd");
                if (userIdentity.ExpiryDate == DateTime.MaxValue)
                {
                    idCard.TblExpiryDate.Text = "长期";
                }
            }
            else
            {
                idCard.TblExpiryDate.Text = "";
            }

            //照片显示
            try
            {

                if (userIdentity.Photo != null && userIdentity.Photo.Length != 0)
                {
                    BitmapImage bitmap = new BitmapImage();
                    MemoryStream ms1 = new MemoryStream(userIdentity.Photo);

                    bitmap.BeginInit();
                    bitmap.StreamSource = ms1;
                    bitmap.EndInit();

                    idCard.ImgPhoto.Source = bitmap;
                }
            }
            catch (Exception)
            {
                //throw new ArgumentException("照片读取失败");
                //MessageBox.Show("照片读取失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            #endregion
        }

        /// <summary>
        /// 护照
        /// </summary>
        /// <param name="passport"></param>
        /// <param name="userIdentity"></param>
        private void ChangePassport(PassportControl passport, UserIdentity userIdentity)
        {
            #region 护照
            passport.TbName.Text = userIdentity.Name; //姓名
            //性别
            switch (userIdentity.Gender)
            {
                case Gender.Male:
                    passport.RdobtnMale.IsChecked = true;
                    break;
                case Gender.Female:
                    passport.RdobtnFemale.IsChecked = true;
                    break;
            }
            //出生
            if (userIdentity.Birthday != null)
                passport.DateBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
            //国家
            passport.CboNationality.Text = userIdentity.Nationality;
            //身份证号
            if (!string.IsNullOrEmpty(userIdentity.Id))
            {
                passport.TbIdentityNo.Text = userIdentity.Id;
            }
            #endregion
        }

        /// <summary>
        /// 港澳居民身份证
        /// </summary>
        /// <param name="hKMacaoIdCard"></param>
        /// <param name="userIdentity"></param>
        private void ChangeHKMacaoIdCard(HKMacaoIdCardControl hKMacaoIdCard, UserIdentity userIdentity)
        {
            #region 港澳居民身份证
            hKMacaoIdCard.TbName.Text = userIdentity.Name; //姓名
            //性别
            if (userIdentity.Gender != null)
            {
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        hKMacaoIdCard.RdobtnMale.IsChecked = true;
                        break;
                    case Gender.Female:
                        hKMacaoIdCard.RdobtnFemale.IsChecked = true;
                        break;
                }
            }
            else
            {
                hKMacaoIdCard.RdobtnMale.IsChecked = false;
                hKMacaoIdCard.RdobtnFemale.IsChecked = false;
            }
            //出生
            if (userIdentity.Birthday != null)
                hKMacaoIdCard.DateBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
            //身份证号
            if (!string.IsNullOrEmpty(userIdentity.Id))
            {
                hKMacaoIdCard.TbIdentityNo.Text = userIdentity.Id;
            }
            //签发日期
            if (userIdentity.EffectiveDate != null)
            {
                hKMacaoIdCard.DateVisaAgency.Text = ((DateTime)userIdentity.EffectiveDate).ToString("yyyy.MM.dd");
            }
            #endregion
        }

        /// <summary>
        /// 出生
        /// </summary>
        /// <param name="birthCertificate"></param>
        /// <param name="userIdentity"></param>
        private void ChangeBirthCertificate(BirthCertificateControl birthCertificate, UserIdentity userIdentity)
        {
            #region 出生证
            birthCertificate.TbName.Text = userIdentity.Name; //姓名
            //性别
            if (userIdentity.Gender != null)
            {
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        birthCertificate.RdobtnMale.IsChecked = true;
                        break;
                    case Gender.Female:
                        birthCertificate.RdobtnFemale.IsChecked = true;
                        break;
                }
            }
            else
            {
                birthCertificate.RdobtnMale.IsChecked = false;
                birthCertificate.RdobtnFemale.IsChecked = false;
            }
            //出生
            if (userIdentity.Birthday != null)
                birthCertificate.DateBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
            //身份证号
            if (!string.IsNullOrEmpty(userIdentity.Id))
            {
                birthCertificate.TbIdentityNo.Text = userIdentity.Id;
            }
            //接生机构
            if (userIdentity.VisaAgency != null)
            {
                birthCertificate.TbVisaAgency.Text = userIdentity.VisaAgency;
            }
            #endregion
        }

        /// <summary>
        /// 港澳居民往来大陆通行证
        /// </summary>
        /// <param name="macaoPermit"></param>
        /// <param name="userIdentity"></param>
        private void ChangeMacaoPermit(MacaoPermitControl macaoPermit, UserIdentity userIdentity)
        {
            #region 港澳居民往来大陆通行证
            macaoPermit.TbName.Text = userIdentity.Name; //姓名
            //性别
            if (userIdentity.Gender != null)
            {
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        macaoPermit.RdobtnMale.IsChecked = true;
                        break;
                    case Gender.Female:
                        macaoPermit.RdobtnFemale.IsChecked = true;
                        break;
                }
            }
            else
            {
                macaoPermit.RdobtnMale.IsChecked = false;
                macaoPermit.RdobtnFemale.IsChecked = false;
            }
            //出生
            if (userIdentity.Birthday != null)
                macaoPermit.DateBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
            //证件号码
            if (!string.IsNullOrEmpty(userIdentity.Id))
            {
                macaoPermit.TbIdentityNo.Text = userIdentity.Id;
            }
            //签发机构
            if (!string.IsNullOrEmpty(userIdentity.VisaAgency))
            {
                macaoPermit.TbVisaAgency.Text = userIdentity.VisaAgency;
            }
            //有效期限-生效
            if (userIdentity.EffectiveDate != null)
            {
                macaoPermit.TbEffectiveDate.Text = ((DateTime)userIdentity.EffectiveDate).Year.ToString();
            }
            //有效期限-失效
            if (userIdentity.ExpiryDate != null)
            {
                macaoPermit.TbExpiryDate.Text = ((DateTime)userIdentity.ExpiryDate).Year.ToString();
            }
            #endregion
        }

        /// <summary>
        /// 台湾居民往来大陆通行证
        /// </summary>
        /// <param name="taiwanPermit"></param>
        /// <param name="userIdentity"></param>
        private void ChangeTaiwanPermit(TaiwanPermitControl taiwanPermit, UserIdentity userIdentity)
        {
            #region 台湾居民往来大陆通行证
            taiwanPermit.TbName.Text = userIdentity.Name; //姓名
            //性别
            if (userIdentity.Gender != null)
            {
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        taiwanPermit.RdobtnMale.IsChecked = true;
                        break;
                    case Gender.Female:
                        taiwanPermit.RdobtnFemale.IsChecked = true;
                        break;
                }
            }
            else
            {
                taiwanPermit.RdobtnMale.IsChecked = false;
                taiwanPermit.RdobtnFemale.IsChecked = false;
            }
            //出生
            if (userIdentity.Birthday != null)
                taiwanPermit.DateBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
            //证件号码
            if (!string.IsNullOrEmpty(userIdentity.Id))
            {
                taiwanPermit.TbIdentityNo.Text = userIdentity.Id;
            }
            //签发机构
            if (!string.IsNullOrEmpty(userIdentity.VisaAgency))
            {
                taiwanPermit.TbVisaAgency.Text = userIdentity.VisaAgency;
            }
            //有效期限-生效
            if (userIdentity.EffectiveDate != null)
            {
                taiwanPermit.TbEffectiveDate.Text = ((DateTime)userIdentity.EffectiveDate).Year.ToString();
            }
            //有效期限-失效
            if (userIdentity.ExpiryDate != null)
            {
                taiwanPermit.TbExpiryDate.Text = ((DateTime)userIdentity.ExpiryDate).Year.ToString();
            }
            #endregion
        }

        /// <summary>
        /// 军官证
        /// </summary>
        /// <param name="certificateOfOfficers"></param>
        /// <param name="userIdentity"></param>
        private void ChangeCertificateOfOfficers(CertificateOfOfficersControl certificateOfOfficers, UserIdentity userIdentity)
        {
            #region 军官证
            certificateOfOfficers.TbName.Text = userIdentity.Name; //姓名
            //性别
            if (userIdentity.Gender != null)
            {
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        certificateOfOfficers.RdobtnMale.IsChecked = true;
                        break;
                    case Gender.Female:
                        certificateOfOfficers.RdobtnFemale.IsChecked = true;
                        break;
                }
            }
            else
            {
                certificateOfOfficers.RdobtnMale.IsChecked = false;
                certificateOfOfficers.RdobtnFemale.IsChecked = false;
            }
            //出生
            if (userIdentity.Birthday != null)
                certificateOfOfficers.DateBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
            //民族
            if (userIdentity.Nation != null)
            {
                certificateOfOfficers.TbNation.Text = userIdentity.Nation.ToString();
            }
            //部别
            if (!string.IsNullOrEmpty(userIdentity.Address))
            {
                certificateOfOfficers.TbDepartmental.Text = userIdentity.Address;
            }
            //证件号码
            if (!string.IsNullOrEmpty(userIdentity.Id))
            {
                certificateOfOfficers.TbIdentityNo.Text = userIdentity.Id;
            }
            //发证机关
            if (!string.IsNullOrEmpty(userIdentity.VisaAgency))
            {
                certificateOfOfficers.TbVisaAgency.Text = userIdentity.VisaAgency;
            }
            //有效期限-生效
            if (userIdentity.EffectiveDate != null)
            {
                certificateOfOfficers.TbEffectiveDate.Text = ((DateTime)userIdentity.EffectiveDate).Year.ToString();
            }
            //有效期限-失效
            if (userIdentity.ExpiryDate != null)
            {
                certificateOfOfficers.TbExpiryDate.Text = ((DateTime)userIdentity.ExpiryDate).Year.ToString();
            }
            #endregion
        }

        /// <summary>
        /// 户口簿
        /// </summary>
        /// <param name="residenceBooklet"></param>
        /// <param name="userIdentity"></param>
        private void ChangeResidenceBooklet(ResidenceBookletControl residenceBooklet, UserIdentity userIdentity)
        {
            #region 户口簿
            residenceBooklet.TbName.Text = userIdentity.Name; //姓名
            //性别
            if (userIdentity.Gender != null)
            {
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        residenceBooklet.RdobtnMale.IsChecked = true;
                        break;
                    case Gender.Female:
                        residenceBooklet.RdobtnFemale.IsChecked = true;
                        break;
                }
            }
            else
            {
                residenceBooklet.RdobtnMale.IsChecked = false;
                residenceBooklet.RdobtnFemale.IsChecked = false;
            }
            //出生
            if (userIdentity.Birthday != null)
                residenceBooklet.DateBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
            //民族
            if (userIdentity.Nation != null)
            {
                residenceBooklet.TbNation.Text = userIdentity.Nation.ToString();
            }
            //身份证号
            if (!string.IsNullOrEmpty(userIdentity.Id))
            {
                residenceBooklet.TbIdentityNo.Text = userIdentity.Id;
            }
            //住址
            if (!string.IsNullOrEmpty(userIdentity.Address))
            {
                residenceBooklet.TbAddress.Text = userIdentity.Address;
            }
            #endregion
        }

        #endregion


        #region 列表事件

        /// <summary>
        /// 买方列表切换事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LbBuyerList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var userIdentity = GetICardInfo(LbBuyerList);
            if (userIdentity == null) { return; }
            _operationStatusBuyer = true;

            switch (userIdentity.CertificateType)
            {
                #region 身份证
                case CertificateType.IdCard:

                    CboBuyerIdentity.SelectionChanged += CboBuyerIdentity_SelectionChanged;
                    CboBuyerIdentity.SelectedValue = (int)CertificateType.IdCard;
                    CboBuyerIdentity.IsEnabled = false;

                    ChangeIdCard(IdCardBuyer, userIdentity);

                    BuyerIDTypeCollapsed();
                    IdCardBuyer.Visibility = Visibility.Visible;

                    //身份证不需要撤销按钮
                    //IdCardOperate(CboBuyerIdentity, BtnBuyerRead, BtnBuyerRevoke);
                    break;

                #endregion

                #region 护照
                case CertificateType.Passport:
                    CboBuyerIdentity.SelectionChanged += CboBuyerIdentity_SelectionChanged;
                    CboBuyerIdentity.SelectedValue = (int)CertificateType.Passport;
                    CboBuyerIdentity.IsEnabled = false;

                    ChangePassport(PassportBuyer, userIdentity);

                    BuyerIDTypeCollapsed();
                    PassportBuyer.Visibility = Visibility.Visible;

                    //护照只需要撤销按钮
                    //OtherTypeOperate(CboBuyerIdentity, BtnBuyerRead, BtnBuyerRevoke);
                    break;
                #endregion

                #region 港澳居民身份证
                case CertificateType.HKMacaoIdCard:
                    CboBuyerIdentity.SelectionChanged += CboBuyerIdentity_SelectionChanged;
                    CboBuyerIdentity.SelectedValue = (int)CertificateType.HKMacaoIdCard;
                    CboBuyerIdentity.IsEnabled = false;

                    ChangeHKMacaoIdCard(HKMacaoIdCardBuyer, userIdentity);

                    BuyerIDTypeCollapsed();
                    HKMacaoIdCardBuyer.Visibility = Visibility.Visible;

                    //港澳居民身份证只需要撤销按钮
                    //OtherTypeOperate(CboBuyerIdentity, BtnBuyerRead, BtnBuyerRevoke);
                    break;
                #endregion

                #region 出生证
                case CertificateType.BirthCertificate:
                    CboBuyerIdentity.SelectionChanged += CboBuyerIdentity_SelectionChanged;
                    CboBuyerIdentity.SelectedValue = (int)CertificateType.BirthCertificate;
                    CboBuyerIdentity.IsEnabled = false;

                    ChangeBirthCertificate(BirthCertificateBuyer, userIdentity);

                    BuyerIDTypeCollapsed();
                    BirthCertificateBuyer.Visibility = Visibility.Visible;

                    //出生证只需要撤销按钮
                    //OtherTypeOperate(CboBuyerIdentity, BtnBuyerRead, BtnBuyerRevoke);
                    break;
                #endregion

                #region 港澳居民往来大陆通行证
                case CertificateType.MacaoPermit:
                    CboBuyerIdentity.SelectionChanged += CboBuyerIdentity_SelectionChanged;
                    CboBuyerIdentity.SelectedValue = (int)CertificateType.MacaoPermit;
                    CboBuyerIdentity.IsEnabled = false;

                    ChangeMacaoPermit(MacaoPermitBuyer, userIdentity);

                    BuyerIDTypeCollapsed();
                    MacaoPermitBuyer.Visibility = Visibility.Visible;

                    //港澳通行证只需要撤销按钮
                    //OtherTypeOperate(CboBuyerIdentity, BtnBuyerRead, BtnBuyerRevoke);
                    break;
                #endregion

                #region 台湾居民往来大陆通行证
                case CertificateType.TaiwanPermit:
                    CboBuyerIdentity.SelectionChanged += CboBuyerIdentity_SelectionChanged;
                    CboBuyerIdentity.SelectedValue = (int)CertificateType.TaiwanPermit;
                    CboBuyerIdentity.IsEnabled = false;

                    ChangeTaiwanPermit(TaiwanPermitBuyer, userIdentity);

                    BuyerIDTypeCollapsed();
                    TaiwanPermitBuyer.Visibility = Visibility.Visible;

                    //港澳通行证只需要撤销按钮
                    //OtherTypeOperate(CboBuyerIdentity, BtnBuyerRead, BtnBuyerRevoke);
                    break;
                #endregion

                #region 军官证
                case CertificateType.CertificateOfOfficers:
                    CboBuyerIdentity.SelectionChanged += CboBuyerIdentity_SelectionChanged;
                    CboBuyerIdentity.SelectedValue = (int)CertificateType.CertificateOfOfficers;
                    CboBuyerIdentity.IsEnabled = false;

                    ChangeCertificateOfOfficers(CertificateOfOfficersBuyer, userIdentity);

                    BuyerIDTypeCollapsed();
                    CertificateOfOfficersBuyer.Visibility = Visibility.Visible;

                    //军官证只需要撤销按钮
                    //OtherTypeOperate(CboBuyerIdentity, BtnBuyerRead, BtnBuyerRevoke);
                    break;
                #endregion

                #region 户口簿
                case CertificateType.ResidenceBooklet:
                    CboBuyerIdentity.SelectionChanged += CboBuyerIdentity_SelectionChanged;
                    CboBuyerIdentity.SelectedValue = (int)CertificateType.ResidenceBooklet;
                    CboBuyerIdentity.IsEnabled = false;

                    ChangeResidenceBooklet(ResidenceBookletBuyer, userIdentity);

                    BuyerIDTypeCollapsed();
                    ResidenceBookletBuyer.Visibility = Visibility.Visible;

                    //户口簿只需要撤销按钮
                    //OtherTypeOperate(CboBuyerIdentity, BtnBuyerRead, BtnBuyerRevoke);
                    break;
                #endregion
            }

            IdTypeOperateNew(CboBuyerIdentity, BtnBuyerRead, BtnBuyerRevoke);
        }


        private void LbBuyerList_SelectionChangedOld(object sender, SelectionChangedEventArgs e)
        {
            var userIdentity = GetICardInfo(LbBuyerList);
            if (userIdentity == null) { return; }
            _operationStatusBuyer = true;
            if (userIdentity.CertificateType == CertificateType.IdCard)
            {
                //CboBuyerIdentity.SelectedValue = 0;
                CboBuyerIdentity.SelectionChanged += CboBuyerIdentity_SelectionChanged;
                CboBuyerIdentity.SelectedValue = (int)CertificateType.IdCard;
                CboBuyerIdentity.IsEnabled = false;

                #region 身份证

                TblBuyerName.Text = userIdentity.Name;
                //switch (userIdentity.Gender)
                //{
                //    case Gender.Male:
                //        TblBuyerGender.Text = "男";
                //        break;
                //    case Gender.Female:
                //        TblBuyerGender.Text = "女";
                //        break;
                //    default:
                //        TblBuyerGender.Text = "";
                //        break;
                //}
                //TblBuyerNation.Text = userIdentity.Nation.ToString();
                //if (userIdentity.Birthday != null)
                //    TblBuyerBirthday.Text = ((DateTime) userIdentity.Birthday).ToString("yyyy.MM.dd");
                //TblBuyerAddress.Text = userIdentity.Address;
                //TblBuyerIdentityNo.Text = userIdentity.Id;
                //TblBuyerVisaAgency.Text = userIdentity.VisaAgency;
                //if (userIdentity.EffectiveDate != null)
                //    TblBuyerEffectiveDate.Text = ((DateTime) userIdentity.EffectiveDate).ToString("yyyy.MM.dd");
                //TblStr.Text = "至";
                //if (userIdentity.ExpiryDate != null)
                //{
                //    TblBuyerExpiryDate.Text = ((DateTime) userIdentity.ExpiryDate).ToString("yyyy.MM.dd");
                //}
                //else
                //{
                //    TblBuyerExpiryDate.Text = "长期";
                //}
                if (userIdentity.Gender != null)
                {
                    switch (userIdentity.Gender)
                    {
                        case Gender.Male:
                            TblBuyerGender.Text = "男";
                            break;
                        case Gender.Female:
                            TblBuyerGender.Text = "女";
                            break;
                        default:
                            TblBuyerGender.Text = "";
                            break;
                    }
                }
                else
                {
                    TblBuyerGender.Text = "";
                }
                if (userIdentity.Nation != null)
                {
                    TblBuyerNation.Text = userIdentity.Nation.ToString();
                }
                if (userIdentity.Birthday != null)
                    TblBuyerBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
                if (!string.IsNullOrEmpty(userIdentity.Address))
                {
                    TblBuyerAddress.Text = userIdentity.Address;
                }
                if (!string.IsNullOrEmpty(userIdentity.Id))
                {
                    TblBuyerIdentityNo.Text = userIdentity.Id;
                }

                if (!string.IsNullOrEmpty(userIdentity.VisaAgency))
                {
                    TblBuyerVisaAgency.Text = userIdentity.VisaAgency;
                }
                if (userIdentity.EffectiveDate != null)
                {
                    TblBuyerEffectiveDate.Text = ((DateTime)userIdentity.EffectiveDate).ToString("yyyy.MM.dd");
                    TblStr.Text = "至";
                }

                if (userIdentity.ExpiryDate != null)
                {
                    TblBuyerExpiryDate.Text = ((DateTime)userIdentity.ExpiryDate).ToString("yyyy.MM.dd");
                    if (userIdentity.ExpiryDate == DateTime.MaxValue)
                    {
                        TblBuyerExpiryDate.Text = "长期";
                    }
                }
                else
                {
                    TblBuyerExpiryDate.Text = "";
                }
                //照片显示
                try
                {
                    if (userIdentity.Photo != null && userIdentity.Photo.Length != 0)
                    {
                        BitmapImage bitmap = new BitmapImage();
                        MemoryStream ms1 = new MemoryStream(userIdentity.Photo);

                        bitmap.BeginInit();
                        bitmap.StreamSource = ms1;
                        bitmap.EndInit();

                        ImgBuyerPhoto.Source = bitmap;
                    }
                }
                catch (Exception)
                {
                    //throw new ArgumentException("照片读取失败");
                    MessageBox.Show("照片读取失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                #endregion

                Grid_BuyerIdCard.Visibility = Visibility.Visible;
                Grid_BuyerPassport.Visibility = Visibility.Collapsed;

                //身份证不需要撤销按钮
                IdCardOperate(CboBuyerIdentity, BtnBuyerRead, BtnBuyerRevoke);
            }
            else
            {
                //CboBuyerIdentity.SelectedValue = 1;
                CboBuyerIdentity.SelectionChanged += CboBuyerIdentity_SelectionChanged;
                CboBuyerIdentity.SelectedValue = (int)CertificateType.Passport;
                CboBuyerIdentity.IsEnabled = false;

                #region 护照

                TbBuyerName.Text = userIdentity.Name;
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        RdoBtnBuyerGenderMale.IsChecked = true;
                        break;
                    case Gender.Female:
                        RdoBtnBuyerGenderFemale.IsChecked = true;
                        break;
                }
                if (userIdentity.Birthday != null)
                    TbBuyerBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
                CboBuyerNationality.Text = userIdentity.Nationality;
                TbBuyerIdentityNo.Text = userIdentity.Id;

                #endregion

                Grid_BuyerPassport.Visibility = Visibility.Visible;
                Grid_BuyerIdCard.Visibility = Visibility.Collapsed;

                //护照只需要撤销按钮
                PassportOperate(CboBuyerIdentity, BtnBuyerRead, BtnBuyerRevoke);
            }

            //CboBuyerIdentity.Visibility = Visibility.Collapsed;
            //BtnBuyerRead.Visibility = Visibility.Collapsed;
            //BtnBuyerRevoke.Visibility = Visibility.Visible;

        }


        #region 身份证、护照等其他证件类型读取撤销切换

        /// <summary>
        /// 类型下拉框可见 读取按钮不可见 撤销按钮可见
        /// </summary>
        /// <param name="cboType"></param>
        /// <param name="btnRead"></param>
        /// <param name="btnRevoke"></param>
        private void IdTypeOperateNew(ComboBox cboType, Button btnRead, Button btnRevoke)
        {
            cboType.Visibility = Visibility.Visible;
            btnRead.Visibility = Visibility.Collapsed;
            btnRevoke.Visibility = Visibility.Visible;
        }

        private void IdCardOperate(ComboBox cboType, Button btnRead, Button btnRevoke)
        {
            cboType.Visibility = Visibility.Visible;
            btnRead.Visibility = Visibility.Visible;
            btnRevoke.Visibility = Visibility.Collapsed;
        }

        private void PassportOperate(ComboBox cboType, Button btnRead, Button btnRevoke)
        {
            cboType.Visibility = Visibility.Collapsed;
            btnRead.Visibility = Visibility.Collapsed;
            btnRevoke.Visibility = Visibility.Visible;
        }
        private void OtherTypeOperate(ComboBox cboType, Button btnRead, Button btnRevoke)
        {
            cboType.Visibility = Visibility.Collapsed;
            btnRead.Visibility = Visibility.Collapsed;
            btnRevoke.Visibility = Visibility.Visible;
        }

        #endregion

        /// <summary>
        /// 卖家列表切换事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LbSellerList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var userIdentity = GetICardInfo(LbSellerList);
            if (userIdentity == null)
            {
                return;
            }

            _operationStatusSeller = true;
            switch (userIdentity.CertificateType)
            {
                #region 身份证
                case CertificateType.IdCard:

                    CboSellerIdentity.SelectionChanged += CboSellerIdentity_SelectionChanged;
                    CboSellerIdentity.SelectedValue = (int)CertificateType.IdCard;
                    CboSellerIdentity.IsEnabled = false;

                    ChangeIdCard(IdCardSeller, userIdentity);

                    SellerIDTypeCollapsed();
                    IdCardSeller.Visibility = Visibility.Visible;

                    //身份证不需要撤销按钮
                    //IdCardOperate(CboSellerIdentity, BtnSellerRead, BtnSellerRevoke);
                    break;

                #endregion

                #region 护照
                case CertificateType.Passport:
                    CboSellerIdentity.SelectionChanged += CboSellerIdentity_SelectionChanged;
                    CboSellerIdentity.SelectedValue = (int)CertificateType.Passport;
                    CboSellerIdentity.IsEnabled = false;

                    ChangePassport(PassportSeller, userIdentity);

                    SellerIDTypeCollapsed();
                    PassportSeller.Visibility = Visibility.Visible;

                    //护照只需要撤销按钮
                    //OtherTypeOperate(CboSellerIdentity, BtnSellerRead, BtnSellerRevoke);
                    break;
                #endregion

                #region 港澳居民身份证
                case CertificateType.HKMacaoIdCard:
                    CboSellerIdentity.SelectionChanged += CboSellerIdentity_SelectionChanged;
                    CboSellerIdentity.SelectedValue = (int)CertificateType.HKMacaoIdCard;
                    CboSellerIdentity.IsEnabled = false;

                    ChangeHKMacaoIdCard(HKMacaoIdCardSeller, userIdentity);

                    SellerIDTypeCollapsed();
                    HKMacaoIdCardSeller.Visibility = Visibility.Visible;

                    //港澳居民身份证只需要撤销按钮
                    //OtherTypeOperate(CboSellerIdentity, BtnSellerRead, BtnSellerRevoke);
                    break;
                #endregion

                #region 出生证
                case CertificateType.BirthCertificate:
                    CboSellerIdentity.SelectionChanged += CboSellerIdentity_SelectionChanged;
                    CboSellerIdentity.SelectedValue = (int)CertificateType.BirthCertificate;
                    CboSellerIdentity.IsEnabled = false;

                    ChangeBirthCertificate(BirthCertificateSeller, userIdentity);

                    SellerIDTypeCollapsed();
                    BirthCertificateSeller.Visibility = Visibility.Visible;

                    //出生证只需要撤销按钮
                    //OtherTypeOperate(CboSellerIdentity, BtnSellerRead, BtnSellerRevoke);
                    break;
                #endregion

                #region 港澳居民往来通行证
                case CertificateType.MacaoPermit:
                    CboSellerIdentity.SelectionChanged += CboSellerIdentity_SelectionChanged;
                    CboSellerIdentity.SelectedValue = (int)CertificateType.MacaoPermit;
                    CboSellerIdentity.IsEnabled = false;

                    ChangeMacaoPermit(MacaoPermitSeller, userIdentity);

                    SellerIDTypeCollapsed();
                    MacaoPermitSeller.Visibility = Visibility.Visible;

                    //港澳通行证只需要撤销按钮
                    //OtherTypeOperate(CboSellerIdentity, BtnSellerRead, BtnSellerRevoke);
                    break;
                #endregion

                #region 台湾居民往来通行证
                case CertificateType.TaiwanPermit:
                    CboSellerIdentity.SelectionChanged += CboSellerIdentity_SelectionChanged;
                    CboSellerIdentity.SelectedValue = (int)CertificateType.TaiwanPermit;
                    CboSellerIdentity.IsEnabled = false;

                    ChangeTaiwanPermit(TaiwanPermitSeller, userIdentity);

                    SellerIDTypeCollapsed();
                    TaiwanPermitSeller.Visibility = Visibility.Visible;

                    //港澳通行证只需要撤销按钮
                    //OtherTypeOperate(CboSellerIdentity, BtnSellerRead, BtnSellerRevoke);
                    break;
                #endregion

                #region 军官证
                case CertificateType.CertificateOfOfficers:
                    CboSellerIdentity.SelectionChanged += CboSellerIdentity_SelectionChanged;
                    CboSellerIdentity.SelectedValue = (int)CertificateType.CertificateOfOfficers;
                    CboSellerIdentity.IsEnabled = false;

                    ChangeCertificateOfOfficers(CertificateOfOfficersSeller, userIdentity);

                    SellerIDTypeCollapsed();
                    CertificateOfOfficersSeller.Visibility = Visibility.Visible;

                    //军官证只需要撤销按钮
                    //OtherTypeOperate(CboSellerIdentity, BtnSellerRead, BtnSellerRevoke);
                    break;
                #endregion

                #region 户口簿
                case CertificateType.ResidenceBooklet:
                    CboSellerIdentity.SelectionChanged += CboSellerIdentity_SelectionChanged;
                    CboSellerIdentity.SelectedValue = (int)CertificateType.ResidenceBooklet;
                    CboSellerIdentity.IsEnabled = false;

                    ChangeResidenceBooklet(ResidenceBookletSeller, userIdentity);

                    SellerIDTypeCollapsed();
                    ResidenceBookletSeller.Visibility = Visibility.Visible;

                    //户口簿只需要撤销按钮
                    //OtherTypeOperate(CboSellerIdentity, BtnSellerRead, BtnSellerRevoke);
                    break;
                #endregion
            }

            IdTypeOperateNew(CboSellerIdentity, BtnSellerRead, BtnSellerRevoke);
        }


        private void LbSellerList_SelectionChangedOld(object sender, SelectionChangedEventArgs e)
        {
            var userIdentity = GetICardInfo(LbSellerList);
            if (userIdentity == null)
            {
                return;
            }

            _operationStatusSeller = true;
            if (userIdentity.CertificateType == CertificateType.IdCard)
            {
                //CboSellerIdentity.SelectedValue = 0;
                CboSellerIdentity.SelectionChanged += CboSellerIdentity_SelectionChanged;
                CboSellerIdentity.SelectedValue = (int)CertificateType.IdCard;
                CboSellerIdentity.IsEnabled = false;

                #region 身份证

                TblSellerName.Text = userIdentity.Name;
                //switch (userIdentity.Gender)
                //{
                //    case Gender.Male:
                //        TblSellerGender.Text = "男";
                //        break;
                //    case Gender.Female:
                //        TblSellerGender.Text = "女";
                //        break;
                //    default:
                //        TblSellerGender.Text = "";
                //        break;
                //}
                //TblSellerNation.Text = userIdentity.Nation.ToString();
                //if (userIdentity.Birthday != null)
                //    TblSellerBirthday.Text = ((DateTime) userIdentity.Birthday).ToString("yyyy.MM.dd");
                //TblSellerAddress.Text = userIdentity.Address;
                //TblSellerIdentityNo.Text = userIdentity.Id;
                //TblSellerVisaAgency.Text = userIdentity.VisaAgency;
                //if (userIdentity.EffectiveDate != null)
                //    TblSellerEffectiveDate.Text = ((DateTime) userIdentity.EffectiveDate).ToString("yyyy.MM.dd");
                //TblStr.Text = "至";
                //if (userIdentity.ExpiryDate != null)
                //{
                //    TblSellerExpiryDate.Text = ((DateTime) userIdentity.ExpiryDate).ToString("yyyy.MM.dd");
                //}
                //else
                //{
                //    TblSellerExpiryDate.Text = "长期";
                //}
                if (userIdentity.Gender != null)
                {
                    switch (userIdentity.Gender)
                    {
                        case Gender.Male:
                            TblSellerGender.Text = "男";
                            break;
                        case Gender.Female:
                            TblSellerGender.Text = "女";
                            break;
                        default:
                            TblSellerGender.Text = "";
                            break;
                    }
                }
                else
                {
                    TblBuyerGender.Text = "";
                }
                if (userIdentity.Nation != null)
                {
                    TblSellerNation.Text = userIdentity.Nation.ToString();
                }
                if (userIdentity.Birthday != null)
                    TblSellerBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
                if (!string.IsNullOrEmpty(userIdentity.Address))
                {
                    TblSellerAddress.Text = userIdentity.Address;
                }
                if (!string.IsNullOrEmpty(userIdentity.Id))
                {
                    TblSellerIdentityNo.Text = userIdentity.Id;
                }

                if (!string.IsNullOrEmpty(userIdentity.VisaAgency))
                {
                    TblSellerVisaAgency.Text = userIdentity.VisaAgency;
                }
                if (userIdentity.EffectiveDate != null)
                {
                    TblSellerEffectiveDate.Text = ((DateTime)userIdentity.EffectiveDate).ToString("yyyy.MM.dd");
                    TblStr.Text = "至";
                }

                if (userIdentity.ExpiryDate != null)
                {
                    TblSellerExpiryDate.Text = ((DateTime)userIdentity.ExpiryDate).ToString("yyyy.MM.dd");
                    if (userIdentity.ExpiryDate == DateTime.MaxValue)
                    {
                        TblSellerExpiryDate.Text = "长期";
                    }
                }
                else
                {
                    TblSellerExpiryDate.Text = "";
                }
                //照片显示
                try
                {
                    if (userIdentity.Photo != null && userIdentity.Photo.Length != 0)
                    {
                        BitmapImage bitmap = new BitmapImage();
                        MemoryStream ms1 = new MemoryStream(userIdentity.Photo);

                        bitmap.BeginInit();
                        bitmap.StreamSource = ms1;
                        bitmap.EndInit();

                        ImgSellerPhoto.Source = bitmap;
                    }
                }
                catch (Exception)
                {
                    //throw new ArgumentException("照片读取失败");
                    MessageBox.Show("照片读取失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                #endregion

                Grid_SellerIdCard.Visibility = Visibility.Visible;
                Grid_SellerPassport.Visibility = Visibility.Collapsed;

                //身份证不需要撤销按钮
                IdCardOperate(CboSellerIdentity, BtnSellerRead, BtnSellerRevoke);
            }
            else
            {
                //CboSellerIdentity.SelectedValue = 1;
                CboSellerIdentity.SelectionChanged += CboSellerIdentity_SelectionChanged;
                CboSellerIdentity.SelectedValue = (int)CertificateType.Passport;
                CboSellerIdentity.IsEnabled = false;

                #region 护照

                TbSellerName.Text = userIdentity.Name;
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        RdoBtnSellerGenderMale.IsChecked = true;
                        break;
                    case Gender.Female:
                        RdoBtnSellerGenderFemale.IsChecked = true;
                        break;
                }
                if (userIdentity.Birthday != null)
                    TbSellerBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
                CboSellerNationality.Text = userIdentity.Nationality;
                //CboSellerNationality.SelectedValue = 0;
                TbSellerIdentityNo.Text = userIdentity.Id;

                #endregion

                Grid_SellerPassport.Visibility = Visibility.Visible;
                Grid_SellerIdCard.Visibility = Visibility.Collapsed;

                //护照只需要撤销按钮
                PassportOperate(CboSellerIdentity, BtnSellerRead, BtnSellerRevoke);
            }

            //CboSellerIdentity.Visibility = Visibility.Collapsed;
            //BtnSellerRead.Visibility = Visibility.Collapsed;
            //BtnSellerRevoke.Visibility = Visibility.Visible;

        }


        private UserIdentity GetICardInfo(ListBox listBox)
        {
            if (listBox.SelectedItem == null)
            {
                return null;
            }
            var source = (KeyValuePair<long, string>)listBox.SelectedItem;
            UserIdentity userIdentity;
            if (icardInfoDic == null || icardInfoDic.Count == 0 || !icardInfoDic.ContainsKey(source.Key))
            {
                userIdentity = UserServiceProxy.GetUserIdentityInfo(source.Key,UserType.Customer);
                if (userIdentity != null)
                {
                    userIdentity.SysNo = source.Key;
                    if (icardInfoDic != null) icardInfoDic[source.Key] = userIdentity;
                }
            }
            else
            {
                userIdentity = icardInfoDic[source.Key];
            }
            return userIdentity;
        }

        #endregion

        /// <summary>
        /// 模糊查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnFuzzyQuery_Click(object sender, RoutedEventArgs e)
        {
            AgentQueryDialog agentQueryDialog = new AgentQueryDialog(_agentStaff);
            agentQueryDialog.Closing += agentQueryDialog_Closing;

            agentQueryDialog.ShowDialog();
        }

        void agentQueryDialog_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            AgentCompany agentCompany = AgentServiceProxy.GetAgentCompany(_agentStaff.AgentCompanySysNo);
            if (agentCompany != null)
            {
                TblShowInfo.Text = "姓名：" + _agentStaff.RealName + "                 所属门店：" + agentCompany.CompanyName;
            }

        }

        #region 必填项验证
        private void SaveValidate()
        {
            //买方
            var listBuyersUserSysNo = new List<long>();
            var enumerableBuyers = LbBuyerList.ItemsSource;
            if (enumerableBuyers == null)
            {
                MessageBox.Show("至少有一个买方.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            foreach (var item in enumerableBuyers)
            {
                var user = item as UserIdentityModel;
                if (user == null) continue;
                long userSysNo;
                long.TryParse(Convert.ToString(user.Tag), out userSysNo);
                listBuyersUserSysNo.Add(userSysNo);
            }

            //卖家
            var listSellersUserSysNo = new List<long>();
            var enumerableSellers = LbSellerList.ItemsSource;
            if (enumerableSellers == null)
            {
                MessageBox.Show("至少有一个卖方.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            foreach (var item in enumerableSellers)
            {
                var user = item as UserIdentityModel;
                if (user == null) continue;
                long userSysNo;
                long.TryParse(Convert.ToString(user.Tag), out userSysNo);
                listSellersUserSysNo.Add(userSysNo);
            }

            if (string.IsNullOrEmpty(TbTenementContract.Text))
            {
                MessageBox.Show("请填写产权证号", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            if (string.IsNullOrEmpty(TbTenementAddress.Text))
            {
                MessageBox.Show("请填写房屋坐落", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }


        }
        #endregion
        /// <summary>
        /// 保存案件
        /// </summary>
        /// <returns></returns>
        private OperationResult SaveUpdateCase()
        {
            //买方
            var listBuyersUserSysNo = new List<long>();
            var enumerableBuyers = LbBuyerList.ItemsSource;
            if (enumerableBuyers == null)
            {
                MessageBox.Show("至少有一个买方.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return null;
            }
            foreach (var item in enumerableBuyers)
            {
                var user = item as UserIdentityModel;
                if (user == null) continue;
                long userSysNo;
                long.TryParse(Convert.ToString(user.Tag), out userSysNo);
                listBuyersUserSysNo.Add(userSysNo);
            }

            //卖家
            var listSellersUserSysNo = new List<long>();
            var enumerableSellers = LbSellerList.ItemsSource;
            if (enumerableSellers == null)
            {
                MessageBox.Show("至少有一个卖方.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return null;
            }
            foreach (var item in enumerableSellers)
            {
                var user = item as UserIdentityModel;
                if (user == null) continue;
                long userSysNo;
                long.TryParse(Convert.ToString(user.Tag), out userSysNo);
                listSellersUserSysNo.Add(userSysNo);
            }

            //if (string.IsNullOrEmpty(TbTenementContractWord.Text) ||
            //    string.IsNullOrEmpty(TbTenementContract.Text))
            //{
            //    MessageBox.Show("请填写产权证号", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return null;
            //}
            if (string.IsNullOrEmpty(TbTenementContract.Text))
            {
                MessageBox.Show("请填写产权证号", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return null;
            }

            if (string.IsNullOrEmpty(TbTenementAddress.Text))
            {
                MessageBox.Show("请填写房屋坐落", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return null;
            }

            OperationResult result = CaseProxyService.GetInstanse()
                   .Update(_caseId, _buyerNameDic.Keys.ToList(), _sellerNameDic.Keys.ToList(), _agentStaff.SysNo,
                       LoginHelper.CurrentUser.SysNo, TbTenementAddress.Text,
                       TbTenementContractWord.Text + "-" + TbTenementContract.Text);
            if (result.Success)
            {
                MessageBox.Show("保存成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                PageHelper.PageNavigateHelper(this, "Main/PreCheck/PreCheckSearchListPage.xaml");
            }
            else
            {
                MessageBox.Show("保存失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
            }


            return result;
        }


        /// <summary>
        /// 保存修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSaveUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SaveUpdateCase();

                #region 作废

                ////验证
                //SaveValidate();
                ////再次查询案件信息--可能其他用户修改了该案件编号的案件信息
                ////CaseDto cDto = CaseProxyService.GetInstanse().GetCaseByCaseId(_caseId);
                ////if (cDto == null)
                ////{
                ////    return;
                ////}
                ////cDto.Buyers.AddRange(_buyerNameDic.Keys);
                ////cDto.Sellers.AddRange(_sellerNameDic.Keys);
                //OperationResult result = CaseProxyService.GetInstanse()
                //    .Update(_caseId, _buyerNameDic.Keys.ToList(), _sellerNameDic.Keys.ToList(), _agentStaff.SysNo,
                //        LoginHelper.CurrentUser.SysNo, TbTenementAddress.Text,
                //        TbTenementContractWord.Text + "-" + TbTenementContract.Text);
                //if (result.Success)
                //{
                //    MessageBox.Show("保存成功");
                //    PageHelper.PageNavigateHelper(this, "Main/PreCheck/PreCheckSearchListPage.xaml");
                //    return;
                //}
                //if (result.OtherData.Count == 0)
                //{
                //    MessageBox.Show("保存失败");
                //    return;
                //}

                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show("系统发生异常：" + ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error); ;
            }
        }


        /// <summary>
        /// 产权证号 光标移走时检查是否重复
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TbTenementContract_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                //产权证号
                string sTenementContract = TbTenementContractWord.Text.Trim() + "-" + TbTenementContract.Text.Trim();
                if (string.IsNullOrEmpty(TbTenementContract.Text))
                {
                    MessageBox.Show("请填写产权证号", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                bool blE = CaseProxyService.GetInstanse().ExistTenementContract(sTenementContract);

                if (sTenementContract != _CaseDto.TenementContract)
                {
                    if (blE)
                    {
                        //MessageBox.Show("该产权证号已存在，请重新输入", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        //TbTenementContractWord.Focus();
                        ImgWarning.Visibility = Visibility.Visible;
                        TblWarning.Text = "已存在";
                        Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Render,
                            new Action(() => TbTenementContract.Focus()));
                    }
                    else
                    {
                        ImgWarning.Visibility = Visibility.Collapsed;
                        TblWarning.Text = "";
                    }
                }

            }
            catch (Exception)
            {
                MessageBox.Show("未获取到产权证号", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }


    }
}
