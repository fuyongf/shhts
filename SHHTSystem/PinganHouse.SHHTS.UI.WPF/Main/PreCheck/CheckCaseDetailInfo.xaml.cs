﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;
using PinganHouse.SHHTS.UI.WPF.Control;

namespace PinganHouse.SHHTS.UI.WPF.Main.PreCheck
{
    /// <summary>
    /// CaseDetailInfo.xaml 的交互逻辑
    /// </summary>
    public partial class CheckCaseDetailInfo : Page
    {
        #region 全局变量
        /// <summary>
        /// 页面案件对象
        /// </summary>
        private CaseDto _CaseDto;
        /// <summary>
        /// 案件编号
        /// </summary>
        private string _caseId;
        /// <summary>
        /// 预检台状态
        /// </summary>
        CaseStatus _status = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.ZB2;

        /// <summary>
        /// 买方姓名集合
        /// </summary>
        private Dictionary<long, string> _buyerNameDic = new Dictionary<long, string>();

        /// <summary>
        /// 卖家姓名集合
        /// </summary>
        private Dictionary<long, string> _sellerNameDic = new Dictionary<long, string>();


        private IDictionary<long, string> _buyerIdentityNoIDic = new Dictionary<long, string>();
        private IDictionary<long, string> _sellerIdentityNoIDic = new Dictionary<long, string>();

        private Dictionary<long, UserIdentity> icardInfoDic = new Dictionary<long, UserIdentity>();

        #endregion

        private const string sCaseInfo =
           "案件编号：{0}  |  立案日期：{1}  |  预检：{2}  |  接单时间：{3}  |  核案：{4}  | 签约完成时间：{5}  |   最后修改时间：{6}  |  修改人：{7}";

        public CheckCaseDetailInfo(string caseId)
        {
            InitializeComponent();

            _caseId = caseId;

            ctrl_Navigation.OperationEvent += ctrl_Navigation_OperationEvent;
            ctrl_Navigation.UploadEvent += ctrl_Navigation_UploadEvent;
            ctrl_Navigation.AttachmentEvent += ctrl_Navigation_AttachmentEvent;
        }

        #region 自定义导航事件
        /// <summary>
        /// 操作信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ctrl_Navigation_OperationEvent(object sender, EventArgs e)
        {
            var status = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.ZB2;
            var attachments = new OperationInfos(_caseId, status, 1); //detailsFlag：0，核案；1，查看预检；2，修改预检。
            PageHelper.PageNavigateHelper(this, attachments);
        }
        /// <summary>
        /// 上传页面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ctrl_Navigation_UploadEvent(object sender, EventArgs e)
        {
            var attachments = new UploadAttachment(_caseId, 1); //detailsFlag：0，核案；1，查看预检；2，修改预检。
            PageHelper.PageNavigateHelper(this, attachments);
        }
        void ctrl_Navigation_AttachmentEvent(object sender, EventArgs e)
        {
            var attachments = new CaseAttachmentsInfo(_caseId, 1); //detailsFlag：0，核案；1，查看预检；2，修改预检。
            PageHelper.PageNavigateHelper(this, attachments);
        }
        #endregion

        /// <summary>
        /// 页面加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(_caseId))
                {
                    var status = CaseStatus.HA1 | CaseStatus.HA2;
                    var dicCaseStatus = new Dictionary<CaseStatus, Tuple<string, DateTime>>();
                    _CaseDto = CaseProxyService.GetInstanse().GetCaseAndProcess(_caseId, status, ref dicCaseStatus);
                    if (_CaseDto != null)
                    {
                        string checkCase = ""; //核案
                        DateTime? checkCaseDate = null; //接单时间
                        DateTime? signingDate = null; //签约完成时间
                        //背景色 橙色
                        Color backgroundColor = (Color)ColorConverter.ConvertFromString("#B5F05A22");

                        #region 进度显示

                        if (dicCaseStatus != null && dicCaseStatus.Count > 0)
                        {
                            if (dicCaseStatus.ContainsKey(CaseStatus.HA1))
                            {
                                checkCase = dicCaseStatus[CaseStatus.HA1].Item1;
                                checkCaseDate = dicCaseStatus[CaseStatus.HA1].Item2;

                                //设置进度显示背景色为橙色
                                BtnCheckCase.Background = new SolidColorBrush(backgroundColor);
                                //设置进度显示图片为可见
                                ImgCheckCaseTime.Visibility = Visibility.Visible;
                                //核案接单用时
                                TimeSpan ts = (TimeSpan)(checkCaseDate - _CaseDto.CreateDate);
                                TblCheckCaseTime.Text = ts.Days.ToString() + "天" + ts.Hours.ToString() + "时" +
                                                        ts.Minutes.ToString() + "分" + ts.Seconds.ToString() + "秒";
                            }
                            if (dicCaseStatus.ContainsKey(CaseStatus.HA2))
                            {
                                signingDate = dicCaseStatus[CaseStatus.HA2].Item2;

                                //设置进度显示背景色为橙色
                                BtnSigned.Background = new SolidColorBrush(backgroundColor);
                                //设置进度显示图片为可见
                                ImgSigningTime.Visibility = Visibility.Visible;
                                //签约完成用时
                                TimeSpan ts = (TimeSpan)(signingDate - checkCaseDate);
                                TblSigningTime.Text = ts.Days.ToString() + "天" + ts.Hours.ToString() + "时" +
                                                      ts.Minutes.ToString() + "分" + ts.Seconds.ToString() + "秒";
                            }

                        }

                        #endregion

                        TblCaseInfo.Text = string.Format(sCaseInfo, _CaseDto.CaseId, _CaseDto.CreateDate,
                            string.IsNullOrWhiteSpace(_CaseDto.CreatorName) ? "---" : _CaseDto.CreatorName,
                            checkCaseDate != null ? checkCaseDate.Value.ToString() : "00:00:00",
                            string.IsNullOrWhiteSpace(checkCase) ? "---" : checkCase,
                            signingDate != null ? signingDate.Value.ToString() : "00:00:00",
                            _CaseDto.ModifyDate != null ? _CaseDto.ModifyDate.Value.ToString() : "00:00:00",
                            string.IsNullOrWhiteSpace(_CaseDto.ModifyUserName) ? "---" : _CaseDto.ModifyUserName);
                        TblCaseSource.Text = string.Format("案件来源：{0}", EnumHelper.GetEnumDesc(_CaseDto.SourceType));
                     
                        InitData();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("加载信息出错：" + ex.Message);
            }
        }

        #region 初始时需加载的数据
        /// <summary>
        /// 加载案件基础数据
        /// </summary>
        private void InitData()
        {
            try
            {
                #region 案件
                //产权证号
                if (_CaseDto.TenementContract != null)
                {
                    var temps = _CaseDto.TenementContract.Split('-');
                    if (temps.Length > 1)
                    {
                        TblTenementContractWord.Text = temps[0];
                        TblTenementContract.Text = temps[1];
                    }
                    else
                    {
                        TblTenementContract.Text = temps[0];
                    }
                }
                //房屋坐落
                if (_CaseDto.TenementAddress != null)
                    TblTenementAddress.Text = _CaseDto.TenementAddress;
                #endregion

                #region 经纪人
                TblAgent.Text = _CaseDto.AgencyName.Trim();
                TblCompany.Text = _CaseDto.CompanyName.Trim();

                #endregion

                #region 买方
                _buyerNameDic = _CaseDto.BuyerNameDic;
                _buyerIdentityNoIDic = CustomerServiceProxy.GetUserIdentityNoByCustomerSysNos(_CaseDto.Buyers.ToArray());
                BindData(LbBuyerList, _buyerNameDic);
                #endregion

                #region 卖家
                _sellerNameDic = _CaseDto.SellerNameDic;
                _sellerIdentityNoIDic = CustomerServiceProxy.GetUserIdentityNoByCustomerSysNos(_CaseDto.Sellers.ToArray());
                BindData(LbSellerList, _sellerNameDic);
                #endregion

            }
            catch (Exception ex)
            {
                MessageBox.Show("加载数据出错：" + ex.Message);
            }
        }

        #endregion

        /// <summary>
        /// 绑定数据源
        /// </summary>
        /// <param name="listBox"></param>
        /// <param name="source"></param>
        private void BindData(ListBox listBox, Dictionary<long, string> source)
        {
            listBox.ItemsSource = null;
            listBox.ItemsSource = source;
            listBox.DisplayMemberPath = "Value";
            listBox.SelectedValuePath = "Key";
        }

        #region 证件类型隐藏

        private void BuyerIDTypeCollapsed()
        {
            IdCardBuyer.Visibility = Visibility.Collapsed;
            PassportBuyer.Visibility = Visibility.Collapsed;
            HKMacaoIdCardBuyer.Visibility = Visibility.Collapsed;
            BirthCertificateBuyer.Visibility = Visibility.Collapsed;
            MacaoPermitBuyer.Visibility = Visibility.Collapsed;
            TaiwanPermitBuyer.Visibility = Visibility.Collapsed;
            CertificateOfOfficersBuyer.Visibility = Visibility.Collapsed;
            ResidenceBookletBuyer.Visibility = Visibility.Collapsed;
        }

        private void SellerIDTypeCollapsed()
        {
            IdCardSeller.Visibility = Visibility.Collapsed;
            PassportSeller.Visibility = Visibility.Collapsed;
            HKMacaoIdCardSeller.Visibility = Visibility.Collapsed;
            BirthCertificateSeller.Visibility = Visibility.Collapsed;
            MacaoPermitSeller.Visibility = Visibility.Collapsed;
            TaiwanPermitSeller.Visibility = Visibility.Collapsed;
            CertificateOfOfficersSeller.Visibility = Visibility.Collapsed;
            ResidenceBookletSeller.Visibility = Visibility.Collapsed;
        }

        #endregion

        #region 列表改变事件

        /// <summary>
        /// 身份证
        /// </summary>
        /// <param name="idCard"></param>
        /// <param name="userIdentity"></param>
        private void ChangeIdCard(IdCardControl idCard, UserIdentity userIdentity)
        {
            #region 身份证
            idCard.TblName.Text = userIdentity.Name; //姓名
            //性别
            if (userIdentity.Gender != null)
            {
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        idCard.TblGender.Text = "男";
                        break;
                    case Gender.Female:
                        idCard.TblGender.Text = "女";
                        break;
                    default:
                        idCard.TblGender.Text = "";
                        break;
                }
            }
            else
            {
                idCard.TblGender.Text = "";
            }
            //民族
            if (userIdentity.Nation != null)
            {
                idCard.TblNation.Text = userIdentity.Nation.ToString();
            }
            //出生
            if (userIdentity.Birthday != null)
                idCard.TblBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
            //地址
            if (!string.IsNullOrEmpty(userIdentity.Address))
            {
                idCard.TblAddress.Text = userIdentity.Address;
            }
            //身份证号
            if (!string.IsNullOrEmpty(userIdentity.Id))
            {
                idCard.TblIdentityNo.Text = userIdentity.Id;
            }
            //签发机关
            if (!string.IsNullOrEmpty(userIdentity.VisaAgency))
            {
                idCard.TblVisaAgency.Text = userIdentity.VisaAgency;
            }
            //有效期限-生效
            if (userIdentity.EffectiveDate != null)
            {
                idCard.TblEffectiveDate.Text = ((DateTime)userIdentity.EffectiveDate).ToString("yyyy.MM.dd");
                idCard.TblStr.Text = "至";
            }
            //有效期限-失效
            if (userIdentity.ExpiryDate != null)
            {
                idCard.TblExpiryDate.Text = ((DateTime)userIdentity.ExpiryDate).ToString("yyyy.MM.dd");
                if (userIdentity.ExpiryDate == DateTime.MaxValue)
                {
                    idCard.TblExpiryDate.Text = "长期";
                }
            }
            else
            {
                idCard.TblExpiryDate.Text = "";
            }

            //照片显示
            try
            {

                if (userIdentity.Photo != null && userIdentity.Photo.Length != 0)
                {
                    BitmapImage bitmap = new BitmapImage();
                    MemoryStream ms1 = new MemoryStream(userIdentity.Photo);

                    bitmap.BeginInit();
                    bitmap.StreamSource = ms1;
                    bitmap.EndInit();

                    idCard.ImgPhoto.Source = bitmap;
                }
            }
            catch (Exception)
            {
                //throw new ArgumentException("照片读取失败");
                //MessageBox.Show("照片读取失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            #endregion
        }

        /// <summary>
        /// 护照
        /// </summary>
        /// <param name="passport"></param>
        /// <param name="userIdentity"></param>
        private void ChangePassport(PassportControl passport, UserIdentity userIdentity)
        {
            #region 护照
            passport.TbName.Text = userIdentity.Name; //姓名
            //性别
            switch (userIdentity.Gender)
            {
                case Gender.Male:
                    passport.RdobtnMale.IsChecked = true;
                    break;
                case Gender.Female:
                    passport.RdobtnFemale.IsChecked = true;
                    break;
            }
            //出生
            if (userIdentity.Birthday != null)
                passport.DateBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
            //国家
            passport.CboNationality.Text = userIdentity.Nationality;
            //身份证号
            if (!string.IsNullOrEmpty(userIdentity.Id))
            {
                passport.TbIdentityNo.Text = userIdentity.Id;
            }
            #endregion
        }

        /// <summary>
        /// 港澳居民身份证
        /// </summary>
        /// <param name="hKMacaoIdCard"></param>
        /// <param name="userIdentity"></param>
        private void ChangeHKMacaoIdCard(HKMacaoIdCardControl hKMacaoIdCard, UserIdentity userIdentity)
        {
            #region 港澳居民身份证
            hKMacaoIdCard.TbName.Text = userIdentity.Name; //姓名
            //性别
            if (userIdentity.Gender != null)
            {
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        hKMacaoIdCard.RdobtnMale.IsChecked = true;
                        break;
                    case Gender.Female:
                        hKMacaoIdCard.RdobtnFemale.IsChecked = true;
                        break;
                }
            }
            else
            {
                hKMacaoIdCard.RdobtnMale.IsChecked = false;
                hKMacaoIdCard.RdobtnFemale.IsChecked = false;
            }
            //出生
            if (userIdentity.Birthday != null)
                hKMacaoIdCard.DateBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
            //身份证号
            if (!string.IsNullOrEmpty(userIdentity.Id))
            {
                hKMacaoIdCard.TbIdentityNo.Text = userIdentity.Id;
            }
            //签发日期
            if (userIdentity.EffectiveDate != null)
            {
                hKMacaoIdCard.DateVisaAgency.Text = ((DateTime)userIdentity.EffectiveDate).ToString("yyyy.MM.dd");
            }
            #endregion
        }

        /// <summary>
        /// 出生
        /// </summary>
        /// <param name="birthCertificate"></param>
        /// <param name="userIdentity"></param>
        private void ChangeBirthCertificate(BirthCertificateControl birthCertificate, UserIdentity userIdentity)
        {
            #region 出生证
            birthCertificate.TbName.Text = userIdentity.Name; //姓名
            //性别
            if (userIdentity.Gender != null)
            {
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        birthCertificate.RdobtnMale.IsChecked = true;
                        break;
                    case Gender.Female:
                        birthCertificate.RdobtnFemale.IsChecked = true;
                        break;
                }
            }
            else
            {
                birthCertificate.RdobtnMale.IsChecked = false;
                birthCertificate.RdobtnFemale.IsChecked = false;
            }
            //出生
            if (userIdentity.Birthday != null)
                birthCertificate.DateBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
            //身份证号
            if (!string.IsNullOrEmpty(userIdentity.Id))
            {
                birthCertificate.TbIdentityNo.Text = userIdentity.Id;
            }
            //接生机构
            if (userIdentity.VisaAgency != null)
            {
                birthCertificate.TbVisaAgency.Text = userIdentity.VisaAgency;
            }
            #endregion
        }

        /// <summary>
        /// 港澳居民往来大陆通行证
        /// </summary>
        /// <param name="macaoPermit"></param>
        /// <param name="userIdentity"></param>
        private void ChangeMacaoPermit(MacaoPermitControl macaoPermit, UserIdentity userIdentity)
        {
            #region 港澳居民往来大陆通行证
            macaoPermit.TbName.Text = userIdentity.Name; //姓名
            //性别
            if (userIdentity.Gender != null)
            {
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        macaoPermit.RdobtnMale.IsChecked = true;
                        break;
                    case Gender.Female:
                        macaoPermit.RdobtnFemale.IsChecked = true;
                        break;
                }
            }
            else
            {
                macaoPermit.RdobtnMale.IsChecked = false;
                macaoPermit.RdobtnFemale.IsChecked = false;
            }
            //出生
            if (userIdentity.Birthday != null)
                macaoPermit.DateBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
            //证件号码
            if (!string.IsNullOrEmpty(userIdentity.Id))
            {
                macaoPermit.TbIdentityNo.Text = userIdentity.Id;
            }
            //签发机构
            if (!string.IsNullOrEmpty(userIdentity.VisaAgency))
            {
                macaoPermit.TbVisaAgency.Text = userIdentity.VisaAgency;
            }
            //有效期限-生效
            if (userIdentity.EffectiveDate != null)
            {
                macaoPermit.TbEffectiveDate.Text = ((DateTime)userIdentity.EffectiveDate).Year.ToString();
            }
            //有效期限-失效
            if (userIdentity.ExpiryDate != null)
            {
                macaoPermit.TbExpiryDate.Text = ((DateTime)userIdentity.ExpiryDate).Year.ToString();
            }
            #endregion
        }

        /// <summary>
        /// 台湾居民往来大陆通行证
        /// </summary>
        /// <param name="taiwanPermit"></param>
        /// <param name="userIdentity"></param>
        private void ChangeTaiwanPermit(TaiwanPermitControl taiwanPermit, UserIdentity userIdentity)
        {
            #region 台湾居民往来大陆通行证
            taiwanPermit.TbName.Text = userIdentity.Name; //姓名
            //性别
            if (userIdentity.Gender != null)
            {
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        taiwanPermit.RdobtnMale.IsChecked = true;
                        break;
                    case Gender.Female:
                        taiwanPermit.RdobtnFemale.IsChecked = true;
                        break;
                }
            }
            else
            {
                taiwanPermit.RdobtnMale.IsChecked = false;
                taiwanPermit.RdobtnFemale.IsChecked = false;
            }
            //出生
            if (userIdentity.Birthday != null)
                taiwanPermit.DateBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
            //证件号码
            if (!string.IsNullOrEmpty(userIdentity.Id))
            {
                taiwanPermit.TbIdentityNo.Text = userIdentity.Id;
            }
            //签发机构
            if (!string.IsNullOrEmpty(userIdentity.VisaAgency))
            {
                taiwanPermit.TbVisaAgency.Text = userIdentity.VisaAgency;
            }
            //有效期限-生效
            if (userIdentity.EffectiveDate != null)
            {
                taiwanPermit.TbEffectiveDate.Text = ((DateTime)userIdentity.EffectiveDate).Year.ToString();
            }
            //有效期限-失效
            if (userIdentity.ExpiryDate != null)
            {
                taiwanPermit.TbExpiryDate.Text = ((DateTime)userIdentity.ExpiryDate).Year.ToString();
            }
            #endregion
        }

        /// <summary>
        /// 军官证
        /// </summary>
        /// <param name="certificateOfOfficers"></param>
        /// <param name="userIdentity"></param>
        private void ChangeCertificateOfOfficers(CertificateOfOfficersControl certificateOfOfficers, UserIdentity userIdentity)
        {
            #region 军官证
            certificateOfOfficers.TbName.Text = userIdentity.Name; //姓名
            //性别
            if (userIdentity.Gender != null)
            {
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        certificateOfOfficers.RdobtnMale.IsChecked = true;
                        break;
                    case Gender.Female:
                        certificateOfOfficers.RdobtnFemale.IsChecked = true;
                        break;
                }
            }
            else
            {
                certificateOfOfficers.RdobtnMale.IsChecked = false;
                certificateOfOfficers.RdobtnFemale.IsChecked = false;
            }
            //出生
            if (userIdentity.Birthday != null)
                certificateOfOfficers.DateBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
            //民族
            if (userIdentity.Nation != null)
            {
                certificateOfOfficers.TbNation.Text = userIdentity.Nation.ToString();
            }
            //部别
            if (!string.IsNullOrEmpty(userIdentity.Address))
            {
                certificateOfOfficers.TbDepartmental.Text = userIdentity.Address;
            }
            //证件号码
            if (!string.IsNullOrEmpty(userIdentity.Id))
            {
                certificateOfOfficers.TbIdentityNo.Text = userIdentity.Id;
            }
            //发证机关
            if (!string.IsNullOrEmpty(userIdentity.VisaAgency))
            {
                certificateOfOfficers.TbVisaAgency.Text = userIdentity.VisaAgency;
            }
            //有效期限-生效
            if (userIdentity.EffectiveDate != null)
            {
                certificateOfOfficers.TbEffectiveDate.Text = ((DateTime)userIdentity.EffectiveDate).Year.ToString();
            }
            //有效期限-失效
            if (userIdentity.ExpiryDate != null)
            {
                certificateOfOfficers.TbExpiryDate.Text = ((DateTime)userIdentity.ExpiryDate).Year.ToString();
            }
            #endregion
        }

        /// <summary>
        /// 户口簿
        /// </summary>
        /// <param name="residenceBooklet"></param>
        /// <param name="userIdentity"></param>
        private void ChangeResidenceBooklet(ResidenceBookletControl residenceBooklet, UserIdentity userIdentity)
        {
            #region 户口簿
            residenceBooklet.TbName.Text = userIdentity.Name; //姓名
            //性别
            if (userIdentity.Gender != null)
            {
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        residenceBooklet.RdobtnMale.IsChecked = true;
                        break;
                    case Gender.Female:
                        residenceBooklet.RdobtnFemale.IsChecked = true;
                        break;
                }
            }
            else
            {
                residenceBooklet.RdobtnMale.IsChecked = false;
                residenceBooklet.RdobtnFemale.IsChecked = false;
            }
            //出生
            if (userIdentity.Birthday != null)
                residenceBooklet.DateBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
            //民族
            if (userIdentity.Nation != null)
            {
                residenceBooklet.TbNation.Text = userIdentity.Nation.ToString();
            }
            //身份证号
            if (!string.IsNullOrEmpty(userIdentity.Id))
            {
                residenceBooklet.TbIdentityNo.Text = userIdentity.Id;
            }
            //住址
            if (!string.IsNullOrEmpty(userIdentity.Address))
            {
                residenceBooklet.TbAddress.Text = userIdentity.Address;
            }
            #endregion
        }

        #endregion


        private void LbBuyerList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var userIdentity = GetICardInfo(LbBuyerList);
            if (userIdentity == null)
            {
                MessageBox.Show("获取信息失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            switch (userIdentity.CertificateType)
            {
                #region 身份证
                case CertificateType.IdCard:

                    RunBuyerIdentityType.Text = "身份证";
                    ChangeIdCard(IdCardBuyer, userIdentity);

                    BuyerIDTypeCollapsed();
                    IdCardBuyer.Visibility = Visibility.Visible;

                    break;

                #endregion

                #region 护照
                case CertificateType.Passport:

                    RunBuyerIdentityType.Text = "护照";
                    ChangePassport(PassportBuyer, userIdentity);

                    BuyerIDTypeCollapsed();
                    PassportBuyer.Visibility = Visibility.Visible;

                    break;
                #endregion

                #region 港澳居民身份证
                case CertificateType.HKMacaoIdCard:

                    RunBuyerIdentityType.Text = "港澳居民身份证";
                    ChangeHKMacaoIdCard(HKMacaoIdCardBuyer, userIdentity);

                    BuyerIDTypeCollapsed();
                    HKMacaoIdCardBuyer.Visibility = Visibility.Visible;

                    break;
                #endregion

                #region 出生证
                case CertificateType.BirthCertificate:

                    RunBuyerIdentityType.Text = "出生证";
                    ChangeBirthCertificate(BirthCertificateBuyer, userIdentity);

                    BuyerIDTypeCollapsed();
                    BirthCertificateBuyer.Visibility = Visibility.Visible;

                    break;
                #endregion

                #region 港澳居民往来大陆通行证
                case CertificateType.MacaoPermit:

                    RunBuyerIdentityType.Text = "港澳居民往来大陆通行证";
                    ChangeMacaoPermit(MacaoPermitBuyer, userIdentity);

                    BuyerIDTypeCollapsed();
                    MacaoPermitBuyer.Visibility = Visibility.Visible;

                    break;
                #endregion

                #region 台湾居民往来大陆通行证
                case CertificateType.TaiwanPermit:

                    RunBuyerIdentityType.Text = "台湾居民往来大陆通行证";
                    ChangeTaiwanPermit(TaiwanPermitBuyer, userIdentity);

                    BuyerIDTypeCollapsed();
                    TaiwanPermitBuyer.Visibility = Visibility.Visible;

                    break;
                #endregion

                #region 军官证
                case CertificateType.CertificateOfOfficers:

                    RunBuyerIdentityType.Text = "军官证";
                    ChangeCertificateOfOfficers(CertificateOfOfficersBuyer, userIdentity);

                    BuyerIDTypeCollapsed();
                    CertificateOfOfficersBuyer.Visibility = Visibility.Visible;

                    break;
                #endregion

                #region 户口簿
                case CertificateType.ResidenceBooklet:

                    RunBuyerIdentityType.Text = "户口簿";
                    ChangeResidenceBooklet(ResidenceBookletBuyer, userIdentity);

                    BuyerIDTypeCollapsed();
                    ResidenceBookletBuyer.Visibility = Visibility.Visible;

                    break;
                #endregion
            }

        }

        private void LbBuyerList_SelectionChangedOld(object sender, SelectionChangedEventArgs e)
        {
            var userIdentity = GetICardInfo(LbBuyerList);
            if (userIdentity == null) { return; }
            if (userIdentity.CertificateType == CertificateType.IdCard)
            {
                #region 身份证

                RunBuyerIdentityType.Text = "身份证";
                TblBuyerName.Text = userIdentity.Name;
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        TblBuyerGender.Text = "男";
                        break;
                    case Gender.Female:
                        TblBuyerGender.Text = "女";
                        break;
                    default:
                        TblBuyerGender.Text = "";
                        break;
                }
                TblBuyerNation.Text = userIdentity.Nation.ToString();
                if (userIdentity.Birthday != null)
                    TblBuyerBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
                TblBuyerAddress.Text = userIdentity.Address;
                TblBuyerIdentityNo.Text = userIdentity.Id;
                TblBuyerVisaAgency.Text = userIdentity.VisaAgency;
                if (userIdentity.EffectiveDate != null)
                    TblBuyerEffectiveDate.Text = ((DateTime)userIdentity.EffectiveDate).ToString("yyyy.MM.dd");
                TblStr.Text = "至";
                if (userIdentity.ExpiryDate != null)
                {
                    TblBuyerExpiryDate.Text = ((DateTime)userIdentity.ExpiryDate).ToString("yyyy.MM.dd");
                }
                else
                {
                    TblBuyerExpiryDate.Text = "长期";
                }
                //照片显示
                if (userIdentity.IsTrusted)
                {
                    try
                    {
                        BitmapImage bitmap = new BitmapImage();
                        MemoryStream ms1 = new MemoryStream(userIdentity.Photo);

                        bitmap.BeginInit();
                        bitmap.StreamSource = ms1;
                        bitmap.EndInit();

                        ImgBuyerPhoto.Source = bitmap;
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("照片读取失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }

                #endregion

                Grid_BuyerIdCard.Visibility = Visibility.Visible;
                Grid_BuyerPassport.Visibility = Visibility.Collapsed;

            }
            else
            {
                #region 护照

                RunBuyerIdentityType.Text = "护照";
                TbBuyerName.Text = userIdentity.Name;
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        RdoBtnBuyerGenderMale.IsChecked = true;
                        break;
                    case Gender.Female:
                        RdoBtnBuyerGenderFemale.IsChecked = true;
                        break;
                }
                if (userIdentity.Birthday != null)
                    TbBuyerBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
                CboBuyerNationality.Text = userIdentity.Nationality;
                TbBuyerIdentityNo.Text = userIdentity.Id;

                #endregion

                Grid_BuyerPassport.Visibility = Visibility.Visible;
                Grid_BuyerIdCard.Visibility = Visibility.Collapsed;

            }
        }

        private UserIdentity GetICardInfo(ListBox listBox)
        {
            if (listBox.SelectedItem == null)
            {
                return null;
            }
            var source = (KeyValuePair<long, string>)listBox.SelectedItem;
            UserIdentity userIdentity;
            if (icardInfoDic == null || icardInfoDic.Count == 0 || !icardInfoDic.ContainsKey(source.Key))
            {
                userIdentity = UserServiceProxy.GetUserIdentityInfo(source.Key, UserType.Customer);
                if (userIdentity != null)
                {
                    userIdentity.SysNo = source.Key;
                    if (icardInfoDic != null) icardInfoDic[source.Key] = userIdentity;
                }
            }
            else
            {
                userIdentity = icardInfoDic[source.Key];
            }
            return userIdentity;
        }

        private void LbSellerList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var userIdentity = GetICardInfo(LbSellerList);
            if (userIdentity == null)
            {
                MessageBox.Show("获取信息失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            switch (userIdentity.CertificateType)
            {
                #region 身份证
                case CertificateType.IdCard:

                    RunSellerIdentityType.Text = "身份证";
                    ChangeIdCard(IdCardSeller, userIdentity);

                    SellerIDTypeCollapsed();
                    IdCardSeller.Visibility = Visibility.Visible;

                    break;

                #endregion

                #region 护照
                case CertificateType.Passport:

                    RunSellerIdentityType.Text = "护照";
                    ChangePassport(PassportSeller, userIdentity);

                    SellerIDTypeCollapsed();
                    PassportSeller.Visibility = Visibility.Visible;

                    break;
                #endregion

                #region 港澳居民身份证
                case CertificateType.HKMacaoIdCard:

                    RunSellerIdentityType.Text = "港澳居民身份证";
                    ChangeHKMacaoIdCard(HKMacaoIdCardSeller, userIdentity);

                    SellerIDTypeCollapsed();
                    HKMacaoIdCardSeller.Visibility = Visibility.Visible;

                    break;
                #endregion

                #region 出生证
                case CertificateType.BirthCertificate:

                    RunSellerIdentityType.Text = "出生证";
                    ChangeBirthCertificate(BirthCertificateSeller, userIdentity);

                    SellerIDTypeCollapsed();
                    BirthCertificateSeller.Visibility = Visibility.Visible;

                    break;
                #endregion

                #region 港澳居民往来大陆通行证
                case CertificateType.MacaoPermit:

                    RunSellerIdentityType.Text = "港澳居民往来大陆通行证";
                    ChangeMacaoPermit(MacaoPermitSeller, userIdentity);

                    SellerIDTypeCollapsed();
                    MacaoPermitSeller.Visibility = Visibility.Visible;

                    break;
                #endregion

                #region 台湾居民往来大陆通行证
                case CertificateType.TaiwanPermit:

                    RunSellerIdentityType.Text = "台湾居民往来大陆通行证";
                    ChangeTaiwanPermit(TaiwanPermitSeller, userIdentity);

                    SellerIDTypeCollapsed();
                    TaiwanPermitSeller.Visibility = Visibility.Visible;

                    break;
                #endregion

                #region 军官证
                case CertificateType.CertificateOfOfficers:

                    RunSellerIdentityType.Text = "军官证";
                    ChangeCertificateOfOfficers(CertificateOfOfficersSeller, userIdentity);

                    SellerIDTypeCollapsed();
                    CertificateOfOfficersSeller.Visibility = Visibility.Visible;

                    break;
                #endregion

                #region 户口簿
                case CertificateType.ResidenceBooklet:

                    RunSellerIdentityType.Text = "户口簿";
                    ChangeResidenceBooklet(ResidenceBookletSeller, userIdentity);

                    SellerIDTypeCollapsed();
                    ResidenceBookletSeller.Visibility = Visibility.Visible;

                    break;
                #endregion
            }
        }

        private void LbSellerList_SelectionChangedOld(object sender, SelectionChangedEventArgs e)
        {
            var userIdentity = GetICardInfo(LbSellerList);
            if (userIdentity == null)
            {
                MessageBox.Show("获取信息失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (userIdentity.CertificateType == CertificateType.IdCard)
            {
                #region 身份证

                RunSellerIdentityType.Text = "身份证";
                TblSellerName.Text = userIdentity.Name;
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        TblSellerGender.Text = "男";
                        break;
                    case Gender.Female:
                        TblSellerGender.Text = "女";
                        break;
                    default:
                        TblSellerGender.Text = "";
                        break;
                }
                TblSellerNation.Text = userIdentity.Nation.ToString();
                if (userIdentity.Birthday != null)
                    TblSellerBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
                TblSellerAddress.Text = userIdentity.Address;
                TblSellerIdentityNo.Text = userIdentity.Id;
                TblSellerVisaAgency.Text = userIdentity.VisaAgency;
                if (userIdentity.EffectiveDate != null)
                    TblSellerEffectiveDate.Text = ((DateTime)userIdentity.EffectiveDate).ToString("yyyy.MM.dd");
                TblStr.Text = "至";
                if (userIdentity.ExpiryDate != null)
                {
                    TblSellerExpiryDate.Text = ((DateTime)userIdentity.ExpiryDate).ToString("yyyy.MM.dd");
                }
                else
                {
                    TblSellerExpiryDate.Text = "长期";
                }
                //照片显示
                try
                {
                    BitmapImage bitmap = new BitmapImage();
                    MemoryStream ms1 = new MemoryStream(userIdentity.Photo);

                    bitmap.BeginInit();
                    bitmap.StreamSource = ms1;
                    bitmap.EndInit();

                    ImgSellerPhoto.Source = bitmap;
                }
                catch (Exception)
                {
                    MessageBox.Show("照片读取失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                #endregion

                Grid_SellerIdCard.Visibility = Visibility.Visible;
                Grid_SellerPassport.Visibility = Visibility.Collapsed;

            }
            else
            {
                #region 护照

                RunSellerIdentityType.Text = "护照";
                TbSellerName.Text = userIdentity.Name;
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        RdoBtnSellerGenderMale.IsChecked = true;
                        break;
                    case Gender.Female:
                        RdoBtnSellerGenderFemale.IsChecked = true;
                        break;
                }
                if (userIdentity.Birthday != null)
                    TbSellerBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
                CboSellerNationality.Text = userIdentity.Nationality;
                TbSellerIdentityNo.Text = userIdentity.Id;

                #endregion

                Grid_SellerPassport.Visibility = Visibility.Visible;
                Grid_SellerIdCard.Visibility = Visibility.Collapsed;

            }
        }

    }
}
