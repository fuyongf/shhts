﻿using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Main.Nuclear;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main.PreCheck
{
    /// <summary>
    /// CaseAttachmentsInfo.xaml 的交互逻辑
    /// </summary>
    public partial class CaseAttachmentsInfo : Page
    {
        #region 全局变量
        /// <summary>
        /// 案件编号
        /// </summary>
        private string _caseId;
        /// <summary>
        /// 预检台状态
        /// </summary>
        CaseStatus _status = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.ZB2;

        /// <summary>
        /// detailsFlag：0，核案；1，查看预检；2，修改预检。
        /// </summary>
        private int _detailsFlag = 0;
        #endregion

        public CaseAttachmentsInfo(string caseId, int detailsFlag = 0)
        {
            InitializeComponent();
            _caseId = caseId;
            _detailsFlag = detailsFlag;

            ctrl_Navigation.BasicInfoEvent += ctrl_Navigation_BasicInfoEvent;
            ctrl_Navigation.OperationEvent += ctrl_Navigation_OperationEvent;
            ctrl_Navigation.UploadEvent += ctrl_Navigation_UploadEvent;
            ctrl_Navigation.AttachmentEvent += ctrl_Navigation_AttachmentEvent;
            ctrl_Navigation.DetailsFlag = detailsFlag;
        }
        /// <summary>
        /// 页面加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            InitData(_caseId);
        }

        #region 自定义导航事件
        /// <summary>
        /// 基本信息跳转
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ctrl_Navigation_BasicInfoEvent(object sender, EventArgs e)
        {
            try
            {
                switch (_detailsFlag)
                {
                    case 1:
                        PageHelper.PageNavigateHelper(this, new CheckCaseDetailInfo(_caseId));
                        break;
                    case 2:
                        PageHelper.PageNavigateHelper(this, new UpdateCasePage(_caseId));
                        break;
                    default:
                        PageHelper.PageNavigateHelper(this, new CaseDetailInfo(_caseId));
                        break;
                }

                #region 作废
                //var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseIdAndCaseStatus(_caseId,CaseStatus.HA1|CaseStatus.HA2|CaseStatus.HA3|CaseStatus.HA4);
                //if (caseDto != null)
                //{
                //    switch (caseDto.CaseStatus) { 
                //        case CaseStatus.HA1:
                //            PageHelper.PageNavigateHelper(this, new UpdateCasePage(_caseId));
                //            break;
                //        case CaseStatus.HA2:
                //            PageHelper.PageNavigateHelper(this, new CheckCaseDetailInfo(_caseId));
                //            break;
                //        case CaseStatus.HA3:
                //            PageHelper.PageNavigateHelper(this, new CaseDetailInfo(_caseId));
                //            break;
                //        default:
                //            PageHelper.PageNavigateHelper(this, new CaseDetailInfo(_caseId));
                //            break;
                //    }
                //}
                #endregion
            }
            catch (Exception)
            {
                MessageBox.Show("页面跳转发生系统异常.","系统异常",MessageBoxButton.OK,MessageBoxImage.Error);
            }
        }
        /// <summary>
        /// 操作信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ctrl_Navigation_OperationEvent(object sender, EventArgs e)
        {
            var status = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.ZB2;
            var attachments = new OperationInfos(_caseId, status, _detailsFlag);
            PageHelper.PageNavigateHelper(this, attachments);
        }
        /// <summary>
        /// 上传页面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ctrl_Navigation_UploadEvent(object sender, EventArgs e)
        {
            var attachments = new UploadAttachment(_caseId, _detailsFlag);
            PageHelper.PageNavigateHelper(this, attachments);
        }
        void ctrl_Navigation_AttachmentEvent(object sender, EventArgs e)
        {
            var attachments = new CaseAttachmentsInfo(_caseId, _detailsFlag);
            PageHelper.PageNavigateHelper(this, attachments);
        }
        #endregion

        private void InitData(string caseId){
            try{
                this.txtCaseId.Text = caseId;
                var caseAttachmentDto = CaseHelper.GetCaseAttachmentDto(caseId);
                if (caseAttachmentDto == null) {
                    throw new Exception("未查找到任何数据...");
                }
                //买方
                var buyerAttachments = caseAttachmentDto.BuyerAttachments;
                //卖家
                var sellerAttachments = caseAttachmentDto.SellerAttachments;
                //案件及其他
                var otherAttachments = caseAttachmentDto.OtherAttachments;

                #region 买方
                //买方
                var buyerList = new List<CaseCustoemrAttachmentInfo>();
                foreach (var buyer in buyerAttachments) {
                    var info = new CaseCustoemrAttachmentInfo();
                    info.SysNo = buyer.Item1;
                    info.CustomerName = buyer.Item2;
                    CheckAttachmentsType(info, buyer.Item3);
                    buyerList.Add(info);
                }
                buyerListDataGrid.DataContext = buyerList;
                #endregion

                #region 卖家
                //卖家
                var sellerList = new List<CaseCustoemrAttachmentInfo>();
                foreach (var seller in sellerAttachments)
                {
                    var info = new CaseCustoemrAttachmentInfo();
                    info.SysNo = seller.Item1;
                    info.CustomerName = seller.Item2;
                    CheckAttachmentsType(info, seller.Item3);
                    sellerList.Add(info);
                }
                sellerListDataGrid.DataContext = sellerList;
                #endregion

                #region 其他
                CheckOtherAttachmentsType(otherAttachments);               
                #endregion
            }
            catch(Exception ex){
                MessageBox.Show("读取数据错误："+ex.Message,"系统错误",MessageBoxButton.OK,MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 判断其它价格
        /// </summary>
        /// <param name="otherAttachments"></param>
        private void CheckOtherAttachmentsType(List<DataTransferObjects.AttachmentDto> otherAttachments)
        {
            foreach (var attachment in otherAttachments) {
                switch (attachment.AttachmentType)
                {
                    case AttachmentType.产证:
                        SetBackground(lab_CZ);
                        break;
                    case AttachmentType.他证:
                        SetBackground(lab_TZ);
                        break;
                    case AttachmentType.购房发票:
                        SetBackground(lab_GFFP);
                        break;
                    case AttachmentType.原购房发票:
                        SetBackground(lab_YGFFP);
                        break;
                    case AttachmentType.原购房合同:
                        SetBackground(lab_YGFHT);
                        break;
                    case AttachmentType.原契税发票:
                        SetBackground(lab_YQSFP);
                        break;
                    case AttachmentType.预告登记证:
                        SetBackground(lab_YGDJZ);
                        break;
                    case AttachmentType.法院判决书:
                        SetBackground(lab_FYPJS);
                        break;
                    case AttachmentType.拍卖确认书:
                        SetBackground(lab_PMQRS);
                        break;
                    case AttachmentType.权利凭证其他:
                        SetBackground(lab_QT_01);
                        break;
                    case AttachmentType.产调证:
                        SetBackground(lab_CDD);
                        break;
                    case AttachmentType.核价单:
                        SetBackground(lab_HJD);
                        break;
                    case AttachmentType.收件收据房屋状况查询:
                        SetBackground(lab_SJSJ_FWZKDC);
                        break;
                    case AttachmentType.限购查询结果:
                        SetBackground(lab_XGCXJG);
                        break;
                    case AttachmentType.税收受理回执:
                        SetBackground(lab_SCSLHZ);
                        break;
                    case AttachmentType.契税完税凭证:
                        SetBackground(lab_QSWSPZ);
                        break;
                    case AttachmentType.个人所得税单:
                        SetBackground(lab_GRSDSSD);
                        break;
                    case AttachmentType.房产税认定书:
                        SetBackground(lab_FCSRDS);
                        break;
                    case AttachmentType.收件收据注销抵押:
                        SetBackground(lab_SJSJ_ZXDY);
                        break;
                    case AttachmentType.收件收据产权过户:
                        SetBackground(lab_SJSJ_CQGH);
                        break;
                    case AttachmentType.住房情况申请表:
                        SetBackground(lab_ZFQKSQB);
                        break;
                    case AttachmentType.网签合同:
                        SetBackground(lab_WQHT);
                        break;
                    case AttachmentType.资金托管协议:
                        SetBackground(lab_ZJTGXY);
                        break;
                    case AttachmentType.佣金托管协议:
                        SetBackground(lab_YJTGXY);
                        break;
                    case AttachmentType.装修补偿协议书:
                        SetBackground(lab_ZXBCXYS);
                        break;
                    case AttachmentType.房屋交接书:
                        SetBackground(lab_FWJJS);
                        break;
                    case AttachmentType.解约协议书:
                        SetBackground(lab_JYXYS);
                        break;
                    case AttachmentType.优先购买切结书:
                        SetBackground(lab_YXGMQJS);
                        break;
                    case AttachmentType.限购政策告知书:
                        SetBackground(lab_XGZCGZS);
                        break;
                    case AttachmentType.中介定金收据:
                        SetBackground(lab_ZJDJSJ);
                        break;
                    case AttachmentType.房款收据:
                        SetBackground(lab_FKSJ);
                        break;
                    case AttachmentType.佣金收据:
                        SetBackground(lab_YJSJ);
                        break;
                    case AttachmentType.税费发票:
                        SetBackground(lab_SFFP);
                        break;
                    case AttachmentType.服务费发票:
                        SetBackground(lab_FWFFP);
                        break;
                    case AttachmentType.上家提前还款结算凭证:
                        SetBackground(lab_SJTQHKJQPZ);
                        break;
                    //case AttachmentType.变更账号确认书:
                    //    SetBackground(lab_ZHQRS);
                    //    break;
                    //case AttachmentType.放款同意书:
                    //    SetBackground(lab_FKTYS);
                    //    break;
                    case AttachmentType.退款申请书:
                        SetBackground(lab_TKSQS);
                        break;
                    case AttachmentType.收据遗失证明:
                        SetBackground(lab_SJYSZM);
                        break;
                    case AttachmentType.交易材料其他:
                        SetBackground(lab_QT_02);
                        break;
                    case AttachmentType.合同协议其他:
                        SetBackground(lab_QT_03);
                        break;
                    case AttachmentType.钱款收据其他:
                        SetBackground(lab_QT_04);
                        break;
                }
            }

        }

        /// <summary>
        /// 确定上下家附件类型
        /// </summary>
        /// <param name="info"></param>
        /// <param name="enumerable"></param>
        private void CheckAttachmentsType(CaseCustoemrAttachmentInfo info, IEnumerable<DataTransferObjects.AttachmentDto> AttachmentDtoList)
        {
            foreach (var attachment in AttachmentDtoList)
            {
                switch (attachment.AttachmentType)
                {
                    case AttachmentType.身份证:
                        info.IsHashSFZ = true;
                        break;
                    case AttachmentType.户口本:
                        info.IsHashHKB = true;
                        break;
                    case AttachmentType.出生证明:
                        info.IsHashCSZM = true;
                        break;
                    case AttachmentType.结婚证:
                        info.IsHashJHZ = true;
                        break;
                    case AttachmentType.离婚证:
                        info.IsHashLHZ = true;
                        break;
                    case AttachmentType.单身证明:
                        info.IsHashDSZM = true;
                        break;
                    case AttachmentType.授权委托书:
                        info.IsHashSQWTS = true;
                        break;
                    case AttachmentType.代理人身份证:
                        info.IsHashDLRSFZ = true;
                        break;
                    case AttachmentType.军官证:
                        info.IsHashJGZ = true;
                        break;
                    case AttachmentType.法人类:
                        info.IsHashFRDB = true;
                        break;
                    case AttachmentType.外籍人士:
                        info.IsHashWJRS = true;
                        break;
                    case AttachmentType.社会保险:
                        info.IsHashSHBX = true;
                        break;
                    case AttachmentType.上家其他:
                    case AttachmentType.下家其他:
                        info.IsHashOther = true;
                        break;
                }
            }
        }

        /// <summary>
        /// 设置按钮的背景
        /// </summary>
        /// <param name="lable"></param>
        public void SetBackground(Label lable)
        {
            ImageBrush berriesBrush = new ImageBrush();
            berriesBrush.ImageSource =
                new BitmapImage(
                    new Uri("Images/right.jpg", UriKind.Relative)
                );

            // Use the brush to paint the button's background.
            lable.Background = berriesBrush;
        }
     
        /// <summary>
        /// 案件附件基本信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InfoBtn_Click(object sender, RoutedEventArgs e)
        {
            var attachments = new UpdateCasePage(_caseId);
            PageHelper.PageNavigateHelper(this, attachments);
        }
        /// <summary>
        /// 上传附件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UploadBtn_Click(object sender, RoutedEventArgs e)
        {
            var attachments = new UploadAttachment(_caseId);
            PageHelper.PageNavigateHelper(this, attachments);
        }
        /// <summary>
        /// 操作信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OperationBtn_Click(object sender, RoutedEventArgs e)
        {
            var attachments = new OperationInfos(_caseId, _status);
            PageHelper.PageNavigateHelper(this, attachments);
        }
    }

    /// <summary>
    /// 页面附件展示帮助类
    /// </summary>
    public class CaseCustoemrAttachmentInfo{
        public long SysNo { get; set; }
        public string CustomerName { get; set; }
        /// <summary>
        /// 是否含有身份证
        /// </summary>
        public bool IsHashSFZ { get; set; }

        /// <summary>
        /// 是否含有户口本
        /// </summary>
        public bool IsHashHKB { get; set; }

        /// <summary>
        /// 是否含有出生证明
        /// </summary>
        public bool IsHashCSZM { get; set; }

        /// <summary>
        /// 是否含有结婚证明
        /// </summary>
        public bool IsHashJHZ { get; set; }

        /// <summary>
        /// 是否含有离婚证明
        /// </summary>
        public bool IsHashLHZ { get; set; }

        /// <summary>
        /// 是否含有单身证明
        /// </summary>
        public bool IsHashDSZM { get; set; }

        /// <summary>
        /// 是否含有授权委托书
        /// </summary>
        public bool IsHashSQWTS { get; set; }

        /// <summary>
        /// 是否含有代理人身份证
        /// </summary>
        public bool IsHashDLRSFZ { get; set; }

        /// <summary>
        /// 是否含有军官证
        /// </summary>
        public bool IsHashJGZ { get; set; }

        /// <summary>
        /// 是否含有法人类
        /// </summary>
        public bool IsHashFRDB { get; set; }

        /// <summary>
        /// 是否外籍人士
        /// </summary>
        public bool IsHashWJRS{ get; set; }
        /// <summary>
        /// 是否外有社会保险
        /// </summary>
        public bool IsHashSHBX{ get; set; }

        /// <summary>
        /// 是否其它
        /// </summary>
        public bool IsHashOther{ get; set; }
    }
}
