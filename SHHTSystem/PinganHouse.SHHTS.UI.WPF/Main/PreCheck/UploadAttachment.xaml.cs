﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Main.ImageScan;
using PinganHouse.SHHTS.UI.WPF.Main.Nuclear;
using PinganHouse.SHHTS.UI.WPF.Model;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Button = System.Windows.Controls.Button;
using MessageBox = System.Windows.MessageBox;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;

namespace PinganHouse.SHHTS.UI.WPF.Main.PreCheck
{
    /// <summary>
    /// UploadAttachment.xaml 的交互逻辑
    /// </summary>
    public partial class UploadAttachment : Page
    {
        #region 公共变量

        #region 附件类型集合
        private List<AttachmentType> sellerAttachment = new List<AttachmentType>()
        {
            AttachmentType.身份证,
            AttachmentType.户口本,
            AttachmentType.出生证明,
            AttachmentType.结婚证,
            AttachmentType.离婚证,
            AttachmentType.单身证明,
            AttachmentType.授权委托书,
            AttachmentType.代理人身份证,
            AttachmentType.军官证,
            AttachmentType.法人身份证,
            AttachmentType.税务登记证,
            AttachmentType.组织机构代码证,
            AttachmentType.公司营业执照,
            AttachmentType.劳动合同,
            AttachmentType.在职证明,
            AttachmentType.境外人士就业证,
            AttachmentType.台胞证,
            AttachmentType.回乡证,
            AttachmentType.港澳身份证,
            AttachmentType.户籍藤本,
            AttachmentType.银行卡,
            AttachmentType.上家其他,
            AttachmentType.离婚判决书,
            AttachmentType.离婚协议书
        };
        private List<AttachmentType> buyerAttachment = new List<AttachmentType>()
        {
            AttachmentType.身份证,
            AttachmentType.户口本,
            AttachmentType.出生证明,
            AttachmentType.结婚证,
            AttachmentType.离婚证,
            AttachmentType.单身证明,
            AttachmentType.授权委托书,
            AttachmentType.代理人身份证,
            AttachmentType.军官证,
            AttachmentType.法人身份证,
            AttachmentType.税务登记证,
            AttachmentType.组织机构代码证,
            AttachmentType.公司营业执照,
            AttachmentType.劳动合同,
            AttachmentType.在职证明,
            AttachmentType.境外人士就业证,
            AttachmentType.台胞证,
            AttachmentType.回乡证,
            AttachmentType.港澳身份证,
            AttachmentType.社保,
            AttachmentType.个保,
            AttachmentType.户籍藤本,
            AttachmentType.银行卡,
            AttachmentType.收入证明,
            AttachmentType.下家其他,
            AttachmentType.离婚判决书,
            AttachmentType.离婚协议书
        };
        private List<AttachmentType> VoucherAttachment = new List<AttachmentType>()
        {     
            AttachmentType.他证,
            AttachmentType.购房发票,
            AttachmentType.原购房发票,
            AttachmentType.原购房合同,
            AttachmentType.原契税发票,
            AttachmentType.预告登记证,
            AttachmentType.法院判决书,
            AttachmentType.拍卖确认书,
            AttachmentType.产证密码条,
            AttachmentType .权利凭证其他,         
        };
        private List<AttachmentType> TradeAttachment = new List<AttachmentType>()
        {
            AttachmentType.产调证,
            AttachmentType.核价单,
            AttachmentType.限购查询结果,
            AttachmentType.税收受理回执,
            AttachmentType.契税完税凭证,
            AttachmentType.个人所得税单,
            AttachmentType.营业税发票,
            AttachmentType.房产税认定书,
            AttachmentType.收件收据注销抵押,
            AttachmentType.收件收据产权过户,
            AttachmentType .经济人证书,   
             AttachmentType .居间公司营业执照复印件,    
             AttachmentType .居间公司委托书,    
             AttachmentType .交易材料其他,
             AttachmentType.住房情况申请表,
             AttachmentType.收件收据房屋状况查询

        };
        private List<AttachmentType> ContractAttachment = new List<AttachmentType>()
        {
            AttachmentType.网签合同,
            AttachmentType.资金托管协议,
            AttachmentType.佣金托管协议,
            AttachmentType.装修补偿协议书,
            AttachmentType.房屋交接书,
            AttachmentType.解约协议书,
            AttachmentType.优先购买切结书,
            AttachmentType.限购政策告知书,
            AttachmentType.租赁合同复印件,
            AttachmentType.权利凭证其他转租约协议,
            AttachmentType .佣金确认书,   
             AttachmentType .合同协议其他,    
         
        };
        private List<AttachmentType> MoneyAttachment = new List<AttachmentType>()
        {
            AttachmentType.中介定金收据,
            AttachmentType.房款收据,
            AttachmentType.佣金收据,
            AttachmentType.税费发票,
            AttachmentType.服务费发票,
            AttachmentType.上家提前还款结算凭证,
            AttachmentType.退款申请书,
            AttachmentType.收据遗失证明,          
            AttachmentType.转账凭条,          
            AttachmentType .钱款收据其他,            
        };
        #endregion

        /// <summary>
        /// 案件编号
        /// </summary>
        private string _caseId;

        /// <summary>
        /// 上下家编号 
        /// </summary>
        private string _buyerOrSellerId;

        /// <summary>
        /// 图片存储途径
        /// </summary>
        private StringBuilder _filePath;

        /// <summary>
        /// 预检台状态
        /// </summary>
        private CaseStatus _status = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.ZB2;

        /// <summary>
        /// detailsFlag：0，核案；1，查看预检；2，修改预检。
        /// </summary>
        private int _detailsFlag = 0;

        /// <summary>
        /// 产证编号
        /// </summary>
        private string _houseNum;

        #endregion

        public UploadAttachment(string caseId, int detailsFlag = 0)
        {
            InitializeComponent();
            _caseId = caseId;
            _detailsFlag = detailsFlag;

            InitData(caseId);
            //接着上传未上传的附件
            UploadBegin();
            ctrl_Navigation.BasicInfoEvent += ctrl_Navigation_BasicInfoEvent;
            ctrl_Navigation.OperationEvent += ctrl_Navigation_OperationEvent;
            ctrl_Navigation.UploadEvent += ctrl_Navigation_UploadEvent;
            ctrl_Navigation.AttachmentEvent += ctrl_Navigation_AttachmentEvent;
        }

        #region 自定义导航事件

        /// <summary>
        /// 基本信息跳转
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ctrl_Navigation_BasicInfoEvent(object sender, EventArgs e)
        {
            try
            {
                switch (_detailsFlag)
                {
                    case 1:
                        PageHelper.PageNavigateHelper(this, new CheckCaseDetailInfo(_caseId));
                        break;
                    case 2:
                        PageHelper.PageNavigateHelper(this, new UpdateCasePage(_caseId));
                        break;
                    default:
                        PageHelper.PageNavigateHelper(this, new CaseDetailInfo(_caseId));
                        break;
                }

                #region 作废
                //var caseDto = CaseProxyService.GetInstanse()
                //    .GetCaseByCaseIdAndCaseStatus(_caseId,
                //        CaseStatus.YJ1 | CaseStatus.ZB2 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.HA3 | CaseStatus.HA4);
                //if (caseDto != null)
                //{
                //    switch (caseDto.CaseStatus)
                //    {
                //        case CaseStatus.HA1:
                //            PageHelper.PageNavigateHelper(this, new UpdateCasePage(_caseId));
                //            break;
                //        case CaseStatus.HA2:
                //            PageHelper.PageNavigateHelper(this, new CheckCaseDetailInfo(_caseId));
                //            break;
                //        case CaseStatus.HA3:
                //            PageHelper.PageNavigateHelper(this, new CaseDetailInfo(_caseId));
                //            break;
                //        case CaseStatus.YJ1:
                //            PageHelper.PageNavigateHelper(this, new UpdateCasePage(_caseId));
                //            break;
                //        case CaseStatus.ZB2:
                //            PageHelper.PageNavigateHelper(this, new CheckCaseDetailInfo(_caseId));
                //            break;
                //        default:
                //            PageHelper.PageNavigateHelper(this, new CaseDetailInfo(_caseId));
                //            break;
                //    }
                //}
                #endregion
            }
            catch (Exception)
            {
                MessageBox.Show("页面跳转发生系统异常.", "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 操作信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ctrl_Navigation_OperationEvent(object sender, EventArgs e)
        {
            var status = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.ZB2;
            var attachments = new OperationInfos(_caseId, status, _detailsFlag);
            PageHelper.PageNavigateHelper(this, attachments);
            //UploadBegin();
        }
        /// <summary>
        /// 上传页面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ctrl_Navigation_UploadEvent(object sender, EventArgs e)
        {
            var attachments = new UploadAttachment(_caseId, _detailsFlag);
            PageHelper.PageNavigateHelper(this, attachments);
        }

        private void ctrl_Navigation_AttachmentEvent(object sender, EventArgs e)
        {
            var attachments = new CaseAttachmentsInfo(_caseId, _detailsFlag);
            PageHelper.PageNavigateHelper(this, attachments);
        }

        #endregion

        #region 页面数据初始化

        /// <summary>
        /// 页面数据初始化
        /// </summary>
        /// <param name="caseID"></param>
        private void InitData(string caseID)
        {
            var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(caseID);
            //绑定页面数据
            TbCaseId.DataContext = _caseId;
            if (caseDto != null)
            {
                _houseNum = caseDto.TenementContract;
                CbBuyers.ItemsSource = caseDto.BuyerNameDic;
                CbBuyers.SelectedValuePath = "Key";
                CbBuyers.DisplayMemberPath = "Value";
                CbBuyers.SelectedValue = 0;

                CbSellers.ItemsSource = caseDto.SellerNameDic;
                CbSellers.SelectedValuePath = "Key";
                CbSellers.DisplayMemberPath = "Value";
                CbSellers.SelectedValue = 0;
            }

        }

        #endregion

        #region 重新上传

        /// <summary>
        /// 重新上传
        /// </summary>
        private void UploadBegin()
        {
            try
            {
                string path = string.Format("{0}{1}", AppDomain.CurrentDomain.BaseDirectory, Files.FileParh.Substring(2));
                if (!Directory.Exists(path))
                    return;

                DirectoryInfo floders = new DirectoryInfo(path);

                DirectoryInfo[] iFloders = floders.GetDirectories();
                if (iFloders.Any())
                {
                    foreach (var iFloder in iFloders)
                    {
                        string caseId = iFloder.Name;
                        DirectoryInfo[] iiFloders = iFloder.GetDirectories();
                        if (iiFloders.Any())
                        {
                            foreach (var iiFloder in iiFloders)
                            {
                                AttachmentType fileType =
                                    (AttachmentType)Enum.Parse(typeof(AttachmentType), iiFloder.Name);
                                FileInfo[] attachments = iiFloder.GetFiles();
                                if (attachments.Any())
                                {
                                    foreach (var attachment in attachments)
                                    {
                                        if (attachment != null)
                                        {
                                            CaseHelper.AddAttachment(caseId, attachment.FullName, fileType,
                                                attachment.Open(FileMode.Open, FileAccess.Read)
                                                //(attachInfo, result) =>
                                                //{
                                                //    if (!result.Success)
                                                //    {
                                                //        MessageBox.Show(result.ResultMessage, "系统提示",
                                                //            MessageBoxButton.OK,
                                                //            MessageBoxImage.Error);
                                                //    }
                                                //    else
                                                //    {
                                                //        attachInfo.AttachmentContentData.Close();
                                                //        attachInfo.AttachmentContentData.Dispose();
                                                //    }
                                                //}
                                                //UploadAction
                                                );

                                        }
                                    }
                                }
                                else
                                {
                                    if (Directory.Exists(iiFloder.FullName))
                                        Directory.Delete(iiFloder.FullName);
                                }
                            }
                        }
                        else
                        {
                            if (Directory.Exists(iFloder.FullName))
                                Directory.Delete(iFloder.FullName);
                        }
                    }
                }
                else
                {
                    if (Directory.Exists(path))
                        Directory.Delete(path);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #endregion

        #region 本地上传

        /// <summary>
        /// 卖方本地上传
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UploadSellerBtn_Click(object sender, RoutedEventArgs e)
        {
            var attachmenttype = ((Button)sender).Tag as string;
            if (CbSellers.SelectedItem == null)
            {
                MessageBox.Show("请选择所属人", "系统提示", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }
            var selectItem = (KeyValuePair<long, string>)CbSellers.SelectedItem;
            var sellerId = Convert.ToString(selectItem.Key);
            if (attachmenttype != null)
            {
                UpLaodAttachment(sellerId, attachmenttype);
            }
        }

        /// <summary>
        /// 买方本地上传
        /// </summary>
        private void UploadBuyerBtn_Click(object sender, RoutedEventArgs e)
        {
            var attachmenttype = ((Button)sender).Tag as string;
            if (CbBuyers.SelectedItem == null)
            {
                MessageBox.Show("请选择所属人", "系统提示", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }
            var selectItem = (KeyValuePair<long, string>)CbBuyers.SelectedItem;
            var buyerId = Convert.ToString(selectItem.Key);
            if (attachmenttype != null)
            {
                UpLaodAttachment(buyerId, attachmenttype);
                //}
                //if (attachmenttype.Equals("10"))
                //{
                //    if (_CbLegal2Type == null)
                //    {
                //        MessageBox.Show("请选择法人类的附件类型", "系统提示", MessageBoxButton.OK, MessageBoxImage.Stop);
                //        return;
                //    }
                //    UpLaodAttachment(buyerId, _CbLegal2Type);
                //}
                //else if (attachmenttype.Equals("11"))
                //{
                //    if (_CbForeign2Type == null)
                //    {
                //        MessageBox.Show("请选择外籍人士的附件类型", "系统提示", MessageBoxButton.OK, MessageBoxImage.Stop);
                //        return;
                //    }
                //    UpLaodAttachment(buyerId, _CbForeign2Type);
                //}
                //else if (attachmenttype.Equals("12"))
                //{
                //    if (_CbSecureType == null)
                //    {
                //        MessageBox.Show("请选择社保的附件类型", "系统提示", MessageBoxButton.OK, MessageBoxImage.Stop);
                //        return;
                //    }
                //    UpLaodAttachment(buyerId, _CbSecureType);
                //}
                //else
                //{                   
            }
        }

        /// <summary>
        /// 本地上传附件(案件)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UploadIdBtn_Click(object sender, RoutedEventArgs e)
        {
            var attachmenttype = ((Button)sender).Tag as string;
            if (attachmenttype != null)
            {
                if (attachmenttype.Equals("22") || attachmenttype.Equals("13"))
                {
                    UpLaodAttachment(_houseNum, attachmenttype);
                }
                else
                {
                    UpLaodAttachment(_caseId, attachmenttype);
                }
            }

        }

        private void UpLaodAttachment(string id, string attachmentType)
        {
            try
            {
                var type = AttachmentType.未知;

                if (attachmentType != null)
                {
                    type = (AttachmentType)Enum.Parse(typeof(AttachmentType), attachmentType);
                }
                var openFiles = new OpenFileDialog
                {
                    Filter = "图像文件(*.jpg,*.bmp,*.png)|*.jpg;*.bmp;*.png",
                    Multiselect = true
                };

                var dialogResult = openFiles.ShowDialog();

                var files = openFiles.OpenFiles();

                if (dialogResult == true)
                {
                    foreach (FileStream file in files)
                    {
                        if (file != null)
                        {
                            //把本地附件放到指定的目录
                            CreateFile(id, type);
                            FileStream AttachFile;
                            using (
                                AttachFile =
                                    new FileStream(_filePath + "\\" + DateTime.Now.Ticks + ".bmp", FileMode.OpenOrCreate,
                                        FileAccess.Write))
                            {
                                byte[] fileByte = new byte[file.Length];
                                file.Read(fileByte, 0, fileByte.Length);

                                AttachFile.Write(fileByte, 0, fileByte.Length);
                            }
                            CaseHelper.AddAttachment(id, AttachFile.Name, type, file, Comlpate);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("本地上传失败原因：" + ex.Message);
            }
        }

        public static bool isShow = false;

        private void Comlpate(AttachmentContent attachInfo, UploadAttachmentResult result)
        {
            if (!result.Success)
            {
                MessageBox.Show("上传失败，请重新上传", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                try
                {
                    attachInfo.AttachmentContentData.Dispose();

                    if (File.Exists(attachInfo.FileName))
                    {
                        File.Delete(attachInfo.FileName);
                    }
                    Application.Current.Dispatcher.Invoke(() =>
                    {       
                    //上传之后重新刷新              
                    int index = TabControls.SelectedIndex;
                    switch (index)
                    {
                        case 0:
                            SellerValue(sellerId);
                            break;
                        case 1:
                            BuyerValue(buyerId);
                            break;
                        case 2:
                            TabItem_Loaded();
                            break;
                        case 3:
                            TabItem_Loaded_1();
                            break;
                        case 4:
                            TabItem_Loaded_2();
                            break;
                        case 5:
                            TabItem_Loaded_3();
                            break;
                    }
                    });
                }
                catch
                {

                }
                finally
                {
                    attachInfo.AttachmentContentData.Close();
                    attachInfo.AttachmentContentData.Dispose();
                }
            }
        }

        #region 创建文件夹

        /// <summary>
        /// 创建文件夹
        /// </summary>
        private void CreateFile(string id, AttachmentType type)
        {
            _filePath = new StringBuilder();
            //创建文件夹
            _filePath.AppendFormat(Files.FileParh);
            if (!Directory.Exists(_filePath.ToString()))
            {
                Directory.CreateDirectory(_filePath.ToString());
            }
            _filePath.AppendFormat("\\{0}", id);
            if (!Directory.Exists(_filePath.ToString()))
            {
                Directory.CreateDirectory(_filePath.ToString());
            }
            _filePath.AppendFormat("\\{0}", type);
            if (!Directory.Exists(_filePath.ToString()))
            {
                Directory.CreateDirectory(_filePath.ToString());
            }
            //_pathImage = _filePath.ToString();
        }

        #endregion

        #endregion

        #region 高拍仪上传

        /// <summary>
        /// 高拍仪卖方上传
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CameraSellerBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var type = AttachmentType.未知;
                var attachmenttype = ((Button)sender).Tag as string;
                if (attachmenttype != null)
                {
                    type = (AttachmentType)Enum.Parse(typeof(AttachmentType), attachmenttype);
                }
                if (CbSellers.SelectedItem == null)
                {
                    MessageBox.Show("请选择所属人", "系统提示", MessageBoxButton.OK, MessageBoxImage.Stop);
                    return;
                }
                var selectItem = (KeyValuePair<long, string>)CbSellers.SelectedItem;
                var sellerId = Convert.ToString(selectItem.Key);
                if (attachmenttype != null)
                {
                    new CaptureImgeDialog(sellerId, type);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("高拍上传失败原因：" + ex.Message);
            }
        }

        /// <summary>
        /// 高拍仪卖方上传
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CameraBuyerBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var type = AttachmentType.未知;
                var attachmenttype = ((Button)sender).Tag as string;
                if (attachmenttype != null)
                {
                    type = (AttachmentType)Enum.Parse(typeof(AttachmentType), attachmenttype);
                }
                if (CbBuyers.SelectedItem == null)
                {
                    MessageBox.Show("请选择所属人", "系统提示", MessageBoxButton.OK, MessageBoxImage.Stop);
                    return;
                }
                var selectItem = (KeyValuePair<long, string>)CbBuyers.SelectedItem;
                var sellerId = Convert.ToString(selectItem.Key);
                if (attachmenttype != null)
                {
                    new CaptureImgeDialog(sellerId, type);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("高拍上传失败原因：" + ex.Message);
            }
        }

        private void CameraIdBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var type = AttachmentType.未知;
                var attachmenttype = ((Button)sender).Tag as string;
                if (attachmenttype != null)
                {
                    type = (AttachmentType)Enum.Parse(typeof(AttachmentType), attachmenttype);
                }
                if (type.ToString().Equals("产证") || type.ToString().Equals("产调证"))
                {
                    new CaptureImgeDialog(_houseNum, type);
                }
                else
                {
                    new CaptureImgeDialog(_caseId, type);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("高拍上传失败原因：" + ex.Message);
            }

        }

        #endregion

        #region 选择上下家

        private string sellerId;
        /// <summary>
        /// 选择上家
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CbSellers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CbSellers.IsEditable = false;

            var selectItem = (KeyValuePair<long, string>)CbSellers.SelectedItem;

            sellerId = Convert.ToString(selectItem.Key);
            SellerValue(sellerId);
        }

        private void SellerValue(string idStr)
        {
            if (string.IsNullOrWhiteSpace(idStr))
                MessageBox.Show("请选择卖方", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            var results = CaseHelper.GetAttachments(idStr, sellerAttachment);

            if (results == null)
                return;

            SellerClass sellers = new SellerClass();

            if (results.ContainsKey(AttachmentType.身份证))
            {
                sellers.SellerId = results[AttachmentType.身份证].Count;
                sellers.SellerIdlist = results[AttachmentType.身份证] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.户口本))
            {
                sellers.SellerBook = results[AttachmentType.户口本].Count;
                sellers.SellerBooklist = results[AttachmentType.户口本] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.出生证明))
            {
                sellers.SellerBirth = results[AttachmentType.出生证明].Count;
                sellers.SellerBirthlist = results[AttachmentType.出生证明] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.结婚证))
            {
                sellers.SellerMarry = results[AttachmentType.结婚证].Count;
                sellers.SellerMarrylist = results[AttachmentType.结婚证] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.离婚证))
            {
                sellers.SellerDiv = results[AttachmentType.离婚证].Count;
                sellers.SelleDivlist = results[AttachmentType.离婚证] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.离婚协议书))
            {
                sellers.SellerDivPro = results[AttachmentType.离婚协议书].Count;
                sellers.SellerDivProlist = results[AttachmentType.离婚协议书] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.离婚判决书))
            {
                sellers.SellerDivJud = results[AttachmentType.离婚判决书].Count;
                sellers.SellerDivJudlist = results[AttachmentType.离婚判决书] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.单身证明))
            {
                sellers.SellerSingle = results[AttachmentType.单身证明].Count;
                sellers.SellerSinglelist = results[AttachmentType.单身证明] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.授权委托书))
            {
                sellers.SellerAuth = results[AttachmentType.授权委托书].Count;
                sellers.SellerAuthlist = results[AttachmentType.授权委托书] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.军官证))
            {
                sellers.SellerArmy = results[AttachmentType.军官证].Count;
                sellers.SellerArmylist = results[AttachmentType.军官证] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.代理人身份证))
            {
                sellers.SellerIdAgent = results[AttachmentType.代理人身份证].Count;
                sellers.SellerIdAgentlist = results[AttachmentType.代理人身份证] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.法人身份证))
            {
                sellers.SellerIdLegal = results[AttachmentType.法人身份证].Count;
                sellers.SellerIdLegallist = results[AttachmentType.法人身份证] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.组织机构代码证))
            {
                sellers.SellerAgent = results[AttachmentType.组织机构代码证].Count;
                sellers.SellerAgentlist = results[AttachmentType.组织机构代码证] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.税务登记证))
            {
                sellers.SellerTax = results[AttachmentType.税务登记证].Count;
                sellers.SellerTaxlist = results[AttachmentType.税务登记证] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.公司营业执照))
            {
                sellers.SellerCom = results[AttachmentType.公司营业执照].Count;
                sellers.SellerComlist = results[AttachmentType.公司营业执照] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.劳动合同))
            {
                sellers.SellerContract = results[AttachmentType.劳动合同].Count;
                sellers.SellerContractlist = results[AttachmentType.劳动合同] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.在职证明))
            {
                sellers.SellerWork = results[AttachmentType.在职证明].Count;
                sellers.SellerWorklist = results[AttachmentType.在职证明] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.境外人士就业证))
            {
                sellers.SellerOut = results[AttachmentType.境外人士就业证].Count;
                sellers.SellerOutlist = results[AttachmentType.境外人士就业证] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.台胞证))
            {
                sellers.SellerTaiwan = results[AttachmentType.台胞证].Count;
                sellers.SellerTaiwanlist = results[AttachmentType.台胞证] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.回乡证))
            {
                sellers.SellerRet = results[AttachmentType.回乡证].Count;
                sellers.SellerRetlist = results[AttachmentType.回乡证] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.港澳身份证))
            {
                sellers.SellerIdOut = results[AttachmentType.港澳身份证].Count;
                sellers.SellerIdOutlist = results[AttachmentType.港澳身份证] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.户籍藤本))
            {
                sellers.SellerBookOut = results[AttachmentType.户籍藤本].Count;
                sellers.SellerBookOutlist = results[AttachmentType.户籍藤本] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.银行卡))
            {
                sellers.SellerBookOut = results[AttachmentType.银行卡].Count;
                sellers.SellerBookOutlist = results[AttachmentType.银行卡] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.上家其他))
            {
                sellers.SellerOther = results[AttachmentType.上家其他].Count;
                sellers.SellerOtherlist = results[AttachmentType.上家其他] as List<string>;
            }

            SellerGrid.DataContext = sellers;
        }

        private string buyerId;
        /// <summary>
        /// 选择下家
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CbBuyers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           var selectItem = (KeyValuePair<long, string>)CbBuyers.SelectedItem;
          buyerId = Convert.ToString(selectItem.Key);
          BuyerValue(buyerId);
        }

        private void BuyerValue(string idStr)
        {
            if (string.IsNullOrWhiteSpace(idStr))
                MessageBox.Show("请选择买方", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);

            var results = CaseHelper.GetAttachments(idStr, buyerAttachment);

            if (results == null)
                return;
            CbBuyers.IsEditable = false;

            BuyerClass buyers = new BuyerClass();

            if (results.ContainsKey(AttachmentType.身份证))
            {
                buyers.BuyerId = results[AttachmentType.身份证].Count;
                buyers.BuyerIdlist = results[AttachmentType.身份证] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.户口本))
            {
                buyers.BuyerBook = results[AttachmentType.户口本].Count;
                buyers.BuyerBooklist = results[AttachmentType.户口本] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.出生证明))
            {
                buyers.BuyerBirth = results[AttachmentType.出生证明].Count;
                buyers.BuyerBirthlist = results[AttachmentType.出生证明] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.结婚证))
            {
                buyers.BuyerMarry = results[AttachmentType.结婚证].Count;
                buyers.BuyerMarrylist = results[AttachmentType.结婚证] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.离婚证))
            {
                buyers.BuyerDiv = results[AttachmentType.离婚证].Count;
                buyers.BuyerDivlist = results[AttachmentType.离婚证] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.离婚协议书))
            {
                buyers.BuyerDivPro = results[AttachmentType.离婚协议书].Count;
                buyers.BuyerDivProlist = results[AttachmentType.离婚协议书] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.离婚判决书))
            {
                buyers.BuyerDivJud = results[AttachmentType.离婚判决书].Count;
                buyers.BuyerDivJudlist = results[AttachmentType.离婚判决书] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.单身证明))
            {
                buyers.BuyerSingle = results[AttachmentType.单身证明].Count;
                buyers.BuyerSinglelist = results[AttachmentType.单身证明] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.授权委托书))
            {
                buyers.BuyerAuth = results[AttachmentType.授权委托书].Count;
                buyers.BuyerAuthlist = results[AttachmentType.授权委托书] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.军官证))
            {
                buyers.BuyerArmy = results[AttachmentType.军官证].Count;
                buyers.BuyerArmylist = results[AttachmentType.军官证] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.代理人身份证))
            {
                buyers.BuyerIdAgent = results[AttachmentType.代理人身份证].Count;
                buyers.BuyerIdAgentlist = results[AttachmentType.代理人身份证] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.法人身份证))
            {
                buyers.BuyerIdLegal = results[AttachmentType.法人身份证].Count;
                buyers.BuyerIdLegallist = results[AttachmentType.法人身份证] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.组织机构代码证))
            {
                buyers.BuyerAgent = results[AttachmentType.组织机构代码证].Count;
                buyers.BuyerAgentlist = results[AttachmentType.组织机构代码证] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.税务登记证))
            {
                buyers.BuyerTax = results[AttachmentType.税务登记证].Count;
                buyers.BuyerTaxlist = results[AttachmentType.税务登记证] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.公司营业执照))
            {
                buyers.BuyerCom = results[AttachmentType.公司营业执照].Count;
                buyers.BuyerComlist = results[AttachmentType.公司营业执照] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.劳动合同))
            {
                buyers.BuyerContract = results[AttachmentType.劳动合同].Count;
                buyers.BuyerContractlist = results[AttachmentType.劳动合同] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.在职证明))
            {
                buyers.BuyerWork = results[AttachmentType.在职证明].Count;
                buyers.BuyerWorklist = results[AttachmentType.在职证明] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.境外人士就业证))
            {
                buyers.BuyerOut = results[AttachmentType.境外人士就业证].Count;
                buyers.BuyerOutlist = results[AttachmentType.境外人士就业证] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.台胞证))
            {
                buyers.BuyerTaiwan = results[AttachmentType.台胞证].Count;
                buyers.BuyerTaiwanlist = results[AttachmentType.台胞证] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.回乡证))
            {
                buyers.BuyerRet = results[AttachmentType.回乡证].Count;
                buyers.BuyerRetlist = results[AttachmentType.回乡证] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.港澳身份证))
            {
                buyers.BuyerIdOut = results[AttachmentType.港澳身份证].Count;
                buyers.BuyerIdOutlist = results[AttachmentType.港澳身份证] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.户籍藤本))
            {
                buyers.BuyerBookOut = results[AttachmentType.户籍藤本].Count;
                buyers.BuyerBookOutlist = results[AttachmentType.户籍藤本] as List<string>;
            } 
            if (results.ContainsKey(AttachmentType.银行卡))
            {
                buyers.BuyerBookOut = results[AttachmentType.银行卡].Count;
                buyers.BuyerBookOutlist = results[AttachmentType.银行卡] as List<string>;
            } 
            if (results.ContainsKey(AttachmentType.收入证明))
            {           
                buyers.BuyerBookOut = results[AttachmentType.收入证明].Count;
                buyers.BuyerBookOutlist = results[AttachmentType.收入证明] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.社保))
            {
                buyers.BuyerInsure = results[AttachmentType.社保].Count;
                buyers.BuyerInsurelist = results[AttachmentType.社保] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.个保))
            {
                buyers.BuyerInsurePre = results[AttachmentType.个保].Count;
                buyers.BuyerInsurePrelist = results[AttachmentType.个保] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.上家其他))
            {
                buyers.BuyerOther = results[AttachmentType.上家其他].Count;
                buyers.BuyerOtherlist = results[AttachmentType.上家其他] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.下家其他))
            {
                buyers.BuyerOther = results[AttachmentType.下家其他].Count;
                buyers.BuyerOtherlist = results[AttachmentType.下家其他] as List<string>;
            }

            BuyerGrid.DataContext = buyers;

        }
        #endregion

        #region 页面辅助类
        public class SellerClass
        {
            public int SellerId { get; set; }
            public List<string> SellerIdlist { get; set; }
            public int SellerBook { get; set; }
            public List<string> SellerBooklist { get; set; }
            public int SellerBirth { get; set; }
            public List<string> SellerBirthlist { get; set; }
            public int SellerMarry { get; set; }
            public List<string> SellerMarrylist { get; set; }
            public int SellerDiv { get; set; }
            public List<string> SelleDivlist { get; set; }
            public int SellerDivPro { get; set; }
            public List<string> SellerDivProlist { get; set; }
            public int SellerDivJud { get; set; }
            public List<string> SellerDivJudlist { get; set; }
            public int SellerSingle { get; set; }
            public List<string> SellerSinglelist { get; set; }
            public int SellerAuth { get; set; }
            public List<string> SellerAuthlist { get; set; }
            public int SellerArmy { get; set; }
            public List<string> SellerArmylist { get; set; }
            public int SellerIdAgent { get; set; }
            public List<string> SellerIdAgentlist { get; set; }
            public int SellerIdLegal { get; set; }
            public List<string> SellerIdLegallist { get; set; }
            public int SellerAgent { get; set; }
            public List<string> SellerAgentlist { get; set; }
            public int SellerTax { get; set; }
            public List<string> SellerTaxlist { get; set; }
            public int SellerCom { get; set; }
            public List<string> SellerComlist { get; set; }
            public int SellerContract { get; set; }
            public List<string> SellerContractlist { get; set; }
            public int SellerWork { get; set; }
            public List<string> SellerWorklist { get; set; }
            public int SellerOut { get; set; }
            public List<string> SellerOutlist { get; set; }
            public int SellerTaiwan { get; set; }
            public List<string> SellerTaiwanlist { get; set; }
            public int SellerRet { get; set; }
            public List<string> SellerRetlist { get; set; }
            public int SellerIdOut { get; set; }
            public List<string> SellerIdOutlist { get; set; }
            public int SellerBookOut { get; set; }
            public List<string> SellerBookOutlist { get; set; }
            public int SellerOther { get; set; }
            public List<string> SellerOtherlist { get; set; }
        }
        public class BuyerClass
        {
            public int BuyerId { get; set; }
            public List<string> BuyerIdlist { get; set; }
            public int BuyerBook { get; set; }
            public List<string> BuyerBooklist { get; set; }
            public int BuyerBirth { get; set; }
            public List<string> BuyerBirthlist { get; set; }
            public int BuyerMarry { get; set; }
            public List<string> BuyerMarrylist { get; set; }
            public int BuyerDiv { get; set; }
            public List<string> BuyerDivlist { get; set; }
            public int BuyerDivPro { get; set; }
            public List<string> BuyerDivProlist { get; set; }
            public int BuyerDivJud { get; set; }
            public List<string> BuyerDivJudlist { get; set; }
            public int BuyerSingle { get; set; }
            public List<string> BuyerSinglelist { get; set; }
            public int BuyerAuth { get; set; }
            public List<string> BuyerAuthlist { get; set; }
            public int BuyerArmy { get; set; }
            public List<string> BuyerArmylist { get; set; }
            public int BuyerIdAgent { get; set; }
            public List<string> BuyerIdAgentlist { get; set; }
            public int BuyerIdLegal { get; set; }
            public List<string> BuyerIdLegallist { get; set; }
            public int BuyerAgent { get; set; }
            public List<string> BuyerAgentlist { get; set; }
            public int BuyerTax { get; set; }
            public List<string> BuyerTaxlist { get; set; }
            public int BuyerCom { get; set; }
            public List<string> BuyerComlist { get; set; }
            public int BuyerContract { get; set; }
            public List<string> BuyerContractlist { get; set; }
            public int BuyerWork { get; set; }
            public List<string> BuyerWorklist { get; set; }
            public int BuyerOut { get; set; }
            public List<string> BuyerOutlist { get; set; }
            public int BuyerTaiwan { get; set; }
            public List<string> BuyerTaiwanlist { get; set; }
            public int BuyerRet { get; set; }
            public List<string> BuyerRetlist { get; set; }
            public int BuyerIdOut { get; set; }
            public List<string> BuyerIdOutlist { get; set; }
            public int BuyerBookOut { get; set; }
            public List<string> BuyerBookOutlist { get; set; }
            public int BuyerInsure { get; set; }
            public List<string> BuyerInsurelist { get; set; }
            public int BuyerInsurePre { get; set; }
            public List<string> BuyerInsurePrelist { get; set; }
            public int BuyerOther { get; set; }
            public List<string> BuyerOtherlist { get; set; }
        }
        public class VoucherClass
        {
            public int HouseCert { get; set; }
            public List<string> HouseCertlist { get; set; }
            public int HouseOther { get; set; }
            public List<string> HouseOtherlist { get; set; }
            public int HouseBill { get; set; }
            public List<string> HouseBilllist { get; set; }
            public int HouseBillOrig { get; set; }
            public List<string> HouseBillOriglist { get; set; }
            public int HouseCompactOrig { get; set; }
            public List<string> HouseCompactOriglist { get; set; }
            public int HouseContractOrig { get; set; }
            public List<string> HouseContractOriglist { get; set; }
            public int PreRegister { get; set; }
            public List<string> PreRegisterlist { get; set; }
            public int CourtJug { get; set; }
            public List<string> CourtJuglist { get; set; }
            public int AuctionSure { get; set; }
            public List<string> AuctionSurelist { get; set; }
            public int CertPassword { get; set; }
            public List<string> CertPasswordlist { get; set; }
            public int VoucherOther { get; set; }
            public List<string> VoucherOtherlist { get; set; }
        }
        public class TradeClass
        {
            public int HouseCallCert { get; set; }
            public List<string> HouseCallCertlist { get; set; }
            public int CheckPrice { get; set; }
            public List<string> CheckPricelist { get; set; }
            public int HouseConCheck { get; set; }
            public List<string> HouseConChecklist { get; set; }
            public int LimitCheck { get; set; }
            public List<string> LimitChecklist { get; set; }
            public int TaxReturn { get; set; }
            public List<string> TaxReturnlist { get; set; }
            public int TaxFinishRecipt { get; set; }
            public List<string> TaxFinishReciptlist { get; set; }
            public int PersonTax { get; set; }
            public List<string> PersonTaxlist { get; set; }
            public int BusinessTax { get; set; }
            public List<string> BusinessTaxlist { get; set; }
            public int HouseTaxCert { get; set; }
            public List<string> HouseTaxCertlist { get; set; }
            public int CancelPle { get; set; }
            public List<string> CancelPlelist { get; set; }
            public int HouseTran { get; set; }
            public List<string> HouseTranlist { get; set; }
            public int HouseApply { get; set; }
            public List<string> HouseApplylist { get; set; }
            public int AgentMan { get; set; }
            public List<string> AgentManlist { get; set; }
            public int ComLiceseCopy { get; set; }
            public List<string> ComLiceseCopylist { get; set; }
            public int ComDelegate { get; set; }
            public List<string> ComDelegatelist { get; set; }
            public int TradeOther { get; set; }
            public List<string> TradeOtherlist { get; set; }
        }
        public class ContractClass
        {
            public int OnlineContract { get; set; }
            public List<string> OnlineContractlist { get; set; }
            public int FundPro { get; set; }
            public List<string> FundProlist { get; set; }
            public int CommPro { get; set; }
            public List<string> CommProlist { get; set; }
            public int FitPro { get; set; }
            public List<string> FitProlist { get; set; }
            public int HouseConnect { get; set; }
            public List<string> HouseConnectlist { get; set; }
            public int CancelCon { get; set; }
            public List<string> CancelConlist { get; set; }
            public int PriorBuy { get; set; }
            public List<string> PriorBuylist { get; set; }
            public int LimitPolicy { get; set; }
            public List<string> LimitPolicylist { get; set; }
            public int RentCon { get; set; }
            public List<string> RentConlist { get; set; }
            public int ConvertPro { get; set; }
            public List<string> ConvertProlist { get; set; }
            public int CommConfrim { get; set; }
            public List<string> CommConfrimlist { get; set; }
            public int ContractOther { get; set; }
            public List<string> ContractOtherlist { get; set; }

        }
        public class MoneyClass
        {
            public int MediumMoney { get; set; }
            public List<string> MediumMoneylist { get; set; }
            public int HouseRecipt { get; set; }
            public List<string> HouseReciptlist { get; set; }
            public int CommRecipt { get; set; }
            public List<string> CommReciptlist { get; set; }
            public int TaxRecipt { get; set; }
            public List<string> TaxReciptlist { get; set; }
            public int ServiceRecipt { get; set; }
            public List<string> ServiceReciptlist { get; set; }
            public int ColseRecipt { get; set; }
            public List<string> ColseReciptlist { get; set; }
            public int RetApply { get; set; }
            public List<string> RetApplylist { get; set; }
            public int ReciptLose { get; set; }
            public List<string> ReciptLoselist { get; set; }
            public int MoneyOther { get; set; }
            public List<string> MoneyOtherlist { get; set; }
        }
        #endregion

        #region 查看附件详情
        /// <summary>
        /// 查看附件详情
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var fileIds = ((Button)sender).Tag as List<string>;
                if (fileIds != null && fileIds.Any())
                {
                    //打开图片浏览
                    new ImageBrowse(fileIds).ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("查看附加失败：" + ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }


        #endregion

        #region TabControl控件切换
        private void TabItem_Loaded()
        {
            VoucherClass Voucher = new VoucherClass();

            var houseresult = CaseHelper.GetAttachments(_houseNum, new List<AttachmentType>() { AttachmentType.产证 });
            if (houseresult == null) return;
            if (houseresult.ContainsKey(AttachmentType.产证))
            {
                Voucher.HouseCert = houseresult[AttachmentType.产证].Count;
                Voucher.HouseCertlist = houseresult[AttachmentType.产证] as List<string>;
            }

            var results = CaseHelper.GetAttachments(_caseId, VoucherAttachment);

            if (results == null)
                return;
            if (results.ContainsKey(AttachmentType.他证))
            {
                Voucher.HouseOther = results[AttachmentType.他证].Count;
                Voucher.HouseOtherlist = results[AttachmentType.他证] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.购房发票))
            {
                Voucher.HouseBill = results[AttachmentType.购房发票].Count;
                Voucher.HouseBilllist = results[AttachmentType.购房发票] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.原购房发票))
            {
                Voucher.HouseBillOrig = results[AttachmentType.原购房发票].Count;
                Voucher.HouseBillOriglist = results[AttachmentType.原购房发票] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.原购房合同))
            {
                Voucher.HouseCompactOrig = results[AttachmentType.原购房合同].Count;
                Voucher.HouseCompactOriglist = results[AttachmentType.原购房合同] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.原契税发票))
            {
                Voucher.HouseContractOrig = results[AttachmentType.原契税发票].Count;
                Voucher.HouseContractOriglist = results[AttachmentType.原契税发票] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.预告登记证))
            {
                Voucher.PreRegister = results[AttachmentType.预告登记证].Count;
                Voucher.PreRegisterlist = results[AttachmentType.预告登记证] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.法院判决书))
            {
                Voucher.CourtJug = results[AttachmentType.法院判决书].Count;
                Voucher.CourtJuglist = results[AttachmentType.法院判决书] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.拍卖确认书))
            {
                Voucher.AuctionSure = results[AttachmentType.拍卖确认书].Count;
                Voucher.AuctionSurelist = results[AttachmentType.拍卖确认书] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.产证密码条))
            {
                Voucher.CertPassword = results[AttachmentType.产证密码条].Count;
                Voucher.CertPasswordlist = results[AttachmentType.产证密码条] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.权利凭证其他))
            {
                Voucher.VoucherOther = results[AttachmentType.权利凭证其他].Count;
                Voucher.VoucherOtherlist = results[AttachmentType.权利凭证其他] as List<string>;
            }

            VoucherGrid.DataContext = Voucher;
        }

        private void TabItem_Loaded_1()
        {
            TradeClass Trade = new TradeClass();

            var houseresult = CaseHelper.GetAttachments(_houseNum, new List<AttachmentType>() { AttachmentType.产调证 });
            if (houseresult == null) return;
            if (houseresult.ContainsKey(AttachmentType.产调证))
            {
                Trade.HouseCallCert = houseresult[AttachmentType.产调证].Count;
                Trade.HouseCallCertlist = houseresult[AttachmentType.产调证] as List<string>;
            }

            var results = CaseHelper.GetAttachments(_caseId, TradeAttachment);

            if (results == null)
                return;
            if (results.ContainsKey(AttachmentType.核价单))
            {
                Trade.CheckPrice = results[AttachmentType.核价单].Count;
                Trade.CheckPricelist = results[AttachmentType.核价单] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.收件收据房屋状况查询))
            {
                Trade.HouseConCheck = results[AttachmentType.收件收据房屋状况查询].Count;
                Trade.HouseConChecklist = results[AttachmentType.收件收据房屋状况查询] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.限购查询结果))
            {
                Trade.LimitCheck = results[AttachmentType.限购查询结果].Count;
                Trade.LimitChecklist = results[AttachmentType.限购查询结果] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.税收受理回执))
            {
                Trade.TaxReturn = results[AttachmentType.税收受理回执].Count;
                Trade.TaxReturnlist = results[AttachmentType.税收受理回执] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.契税完税凭证))
            {
                Trade.TaxFinishRecipt = results[AttachmentType.契税完税凭证].Count;
                Trade.TaxFinishReciptlist = results[AttachmentType.契税完税凭证] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.个人所得税单))
            {
                Trade.PersonTax = results[AttachmentType.个人所得税单].Count;
                Trade.PersonTaxlist = results[AttachmentType.个人所得税单] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.营业税发票))
            {
                Trade.BusinessTax = results[AttachmentType.营业税发票].Count;
                Trade.BusinessTaxlist = results[AttachmentType.营业税发票] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.房产税认定书))
            {
                Trade.HouseTaxCert = results[AttachmentType.房产税认定书].Count;
                Trade.HouseTaxCertlist = results[AttachmentType.房产税认定书] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.收件收据注销抵押))
            {
                Trade.CancelPle = results[AttachmentType.收件收据注销抵押].Count;
                Trade.CancelPlelist = results[AttachmentType.收件收据注销抵押] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.收件收据产权过户))
            {
                Trade.HouseTran = results[AttachmentType.收件收据产权过户].Count;
                Trade.HouseTranlist = results[AttachmentType.收件收据产权过户] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.住房情况申请表))
            {
                Trade.HouseApply = results[AttachmentType.住房情况申请表].Count;
                Trade.HouseApplylist = results[AttachmentType.住房情况申请表] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.经济人证书))
            {
                Trade.AgentMan = results[AttachmentType.经济人证书].Count;
                Trade.AgentManlist = results[AttachmentType.经济人证书] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.居间公司营业执照复印件))
            {
                Trade.ComLiceseCopy = results[AttachmentType.居间公司营业执照复印件].Count;
                Trade.ComLiceseCopylist = results[AttachmentType.居间公司营业执照复印件] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.居间公司委托书))
            {
                Trade.ComDelegate = results[AttachmentType.居间公司委托书].Count;
                Trade.ComDelegatelist = results[AttachmentType.居间公司委托书] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.交易材料其他))
            {
                Trade.TradeOther = results[AttachmentType.交易材料其他].Count;
                Trade.TradeOtherlist = results[AttachmentType.交易材料其他] as List<string>;
            }

            TradeGrid.DataContext = Trade;
        }

        private void TabItem_Loaded_2()
        {
            var results = CaseHelper.GetAttachments(_caseId, ContractAttachment);

            if (results == null)
                return;

            ContractClass Contract = new ContractClass();

            if (results.ContainsKey(AttachmentType.网签合同))
            {
                Contract.OnlineContract = results[AttachmentType.网签合同].Count;
                Contract.OnlineContractlist = results[AttachmentType.网签合同] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.资金托管协议))
            {
                Contract.FundPro = results[AttachmentType.资金托管协议].Count;
                Contract.FundProlist = results[AttachmentType.资金托管协议] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.佣金托管协议))
            {
                Contract.CommPro = results[AttachmentType.佣金托管协议].Count;
                Contract.CommProlist = results[AttachmentType.佣金托管协议] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.装修补偿协议书))
            {
                Contract.FitPro = results[AttachmentType.装修补偿协议书].Count;
                Contract.FitProlist = results[AttachmentType.装修补偿协议书] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.房屋交接书))
            {
                Contract.HouseConnect = results[AttachmentType.房屋交接书].Count;
                Contract.HouseConnectlist = results[AttachmentType.房屋交接书] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.解约协议书))
            {
                Contract.CancelCon = results[AttachmentType.解约协议书].Count;
                Contract.CancelConlist = results[AttachmentType.解约协议书] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.优先购买切结书))
            {
                Contract.PriorBuy = results[AttachmentType.优先购买切结书].Count;
                Contract.PriorBuylist = results[AttachmentType.优先购买切结书] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.限购政策告知书))
            {
                Contract.LimitPolicy = results[AttachmentType.限购政策告知书].Count;
                Contract.LimitPolicylist = results[AttachmentType.限购政策告知书] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.租赁合同复印件))
            {
                Contract.RentCon = results[AttachmentType.租赁合同复印件].Count;
                Contract.RentConlist = results[AttachmentType.租赁合同复印件] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.权利凭证其他转租约协议))
            {
                Contract.ConvertPro = results[AttachmentType.权利凭证其他转租约协议].Count;
                Contract.ConvertProlist = results[AttachmentType.权利凭证其他转租约协议] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.佣金确认书))
            {
                Contract.CommConfrim = results[AttachmentType.佣金确认书].Count;
                Contract.CommConfrimlist = results[AttachmentType.佣金确认书] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.合同协议其他))
            {
                Contract.ContractOther = results[AttachmentType.合同协议其他].Count;
                Contract.ContractOtherlist = results[AttachmentType.合同协议其他] as List<string>;
            }
            ContractGird.DataContext = Contract;
        }

        private void TabItem_Loaded_3()
        {
            var results = CaseHelper.GetAttachments(_caseId, MoneyAttachment);

            if (results == null)
                return;

            MoneyClass Money = new MoneyClass();

            if (results.ContainsKey(AttachmentType.中介定金收据))
            {
                Money.MediumMoney = results[AttachmentType.中介定金收据].Count;
                Money.MediumMoneylist = results[AttachmentType.中介定金收据] as List<string>;
            }

            if (results.ContainsKey(AttachmentType.房款收据))
            {
                Money.HouseRecipt = results[AttachmentType.房款收据].Count;
                Money.HouseReciptlist = results[AttachmentType.房款收据] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.佣金收据))
            {
                Money.CommRecipt = results[AttachmentType.佣金收据].Count;
                Money.CommReciptlist = results[AttachmentType.佣金收据] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.税费发票))
            {
                Money.TaxRecipt = results[AttachmentType.税费发票].Count;
                Money.TaxReciptlist = results[AttachmentType.税费发票] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.服务费发票))
            {
                Money.ServiceRecipt = results[AttachmentType.服务费发票].Count;
                Money.ServiceReciptlist = results[AttachmentType.服务费发票] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.上家提前还款结算凭证))
            {
                Money.ColseRecipt = results[AttachmentType.上家提前还款结算凭证].Count;
                Money.ColseReciptlist = results[AttachmentType.上家提前还款结算凭证] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.退款申请书))
            {
                Money.RetApply = results[AttachmentType.退款申请书].Count;
                Money.RetApplylist = results[AttachmentType.退款申请书] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.收据遗失证明))
            {
                Money.ReciptLose = results[AttachmentType.收据遗失证明].Count;
                Money.ReciptLoselist = results[AttachmentType.收据遗失证明] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.转账凭条))
            {
                Money.ReciptLose = results[AttachmentType.转账凭条].Count;
                Money.ReciptLoselist = results[AttachmentType.转账凭条] as List<string>;
            }
            if (results.ContainsKey(AttachmentType.钱款收据其他))
            {
                Money.MoneyOther = results[AttachmentType.钱款收据其他].Count;
                Money.MoneyOtherlist = results[AttachmentType.钱款收据其他] as List<string>;
            }
            MoneyGrid.DataContext = Money;
        }

   
        /// <summary>
        /// TabControl控件切换事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TabControls_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var tabcontrol = (TabControl)sender;
            int index = tabcontrol.SelectedIndex;
            switch (index)
            {
                case 2:
                    TabItem_Loaded();
                    break;
                case 3:
                    TabItem_Loaded_1();
                    break;
                case 4:
                    TabItem_Loaded_2();
                    break;
                case 5:
                    TabItem_Loaded_3();
                    break;
            }
        }
        #endregion

    }
}
