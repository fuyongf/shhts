﻿using System.IO;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Collections;
using Button = System.Windows.Controls.Button;
using ComboBox = System.Windows.Controls.ComboBox;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using MessageBox = System.Windows.MessageBox;
using System.Drawing.Printing;
using System.Windows.Documents;
using PinganHouse.SHHTS.UI.WPF.ReadCard;
using PinganHouse.SHHTS.UI.WPF.Control;
using PinganHouse.SHHTS.AccessControl.Proxy;

namespace PinganHouse.SHHTS.UI.WPF.Main.PreCheck
{
    /// <summary>
    /// AddCasePage.xaml 的交互逻辑
    /// </summary>
    [SecurityAuthorize]
    public partial class AddCasePage : Page
    {

        /// <summary>
        /// 打印机列表
        /// </summary>
        private List<PrintModel> _ListPrint = new List<PrintModel>();

        /// <summary>
        /// 打印机名字
        /// </summary>
        private string _ReceiptPrintName;

        /// <summary>
        /// 生成条形码数
        /// </summary>
        private int _BarCodeNum;

        /// <summary>
        /// 记录经纪人
        /// </summary>
        private AgentStaff _AgentStaff = new AgentStaff();

        /// <summary>
        /// 记录经纪公司
        /// </summary>
        private AgentCompany _AgentCompany = new AgentCompany();

        #region 身份证信息

        /// <summary>
        /// 身份证信息对象
        /// </summary>
        private UserIdentityModel IdentityInfo = new UserIdentityModel();
        /// <summary>
        /// 读取身份证人的类型
        /// </summary>
        private enum IdCardRead
        {
            /// <summary>
            /// 未知
            /// </summary>
            Unkonw,
            /// <summary>
            /// 买方
            /// </summary>
            Buyer,
            /// <summary>
            /// 卖家
            /// </summary>
            Seller,
            /// <summary>
            /// 经纪人
            /// </summary>
            Agent
        }
        ///<summary>
        /// 读取身份证人
        /// </summary>
        private int _idCardReadType = 0;
        /// <summary>
        /// 案件来源类型
        /// </summary>
        private SourceType certificateType;

        #endregion

        /// <summary>
        /// 构造函数
        /// </summary>
        public AddCasePage()
        {
            InitializeComponent();

            this.TbTenementContractWord.Focusable = true;

            BtnBuyerRevoke.Visibility = Visibility.Collapsed;
            BtnSellerRevoke.Visibility = Visibility.Collapsed;

            #region 绑定证件类别

            List<EnumHelper> listIdentity = EnumHelper.InitCertificateTypeToCombobox();
            CboBuyerIdentity.ItemsSource = listIdentity;
            CboBuyerIdentity.DisplayMemberPath = "DisplayMember";
            CboBuyerIdentity.SelectedValuePath = "ValueMember";
            CboBuyerIdentity.SelectedValue = 0;

            CboSellerIdentity.ItemsSource = listIdentity;
            CboSellerIdentity.DisplayMemberPath = "DisplayMember";
            CboSellerIdentity.SelectedValuePath = "ValueMember";
            CboSellerIdentity.SelectedValue = 0;

            #endregion

            #region 绑定民族

            List<EnumHelper> listNation = EnumHelper.InitNationToCombobox();
            CertificateOfOfficersBuyer.TbNation.ItemsSource = listNation;
            CertificateOfOfficersBuyer.TbNation.DisplayMemberPath = "DisplayMember";
            CertificateOfOfficersBuyer.TbNation.SelectedValuePath = "ValueMember";

            CertificateOfOfficersSeller.TbNation.ItemsSource = listNation;
            CertificateOfOfficersSeller.TbNation.DisplayMemberPath = "DisplayMember";
            CertificateOfOfficersSeller.TbNation.SelectedValuePath = "ValueMember";

            ResidenceBookletBuyer.TbNation.ItemsSource = listNation;
            ResidenceBookletBuyer.TbNation.DisplayMemberPath = "DisplayMember";
            ResidenceBookletBuyer.TbNation.SelectedValuePath = "ValueMember";

            ResidenceBookletSeller.TbNation.ItemsSource = listNation;
            ResidenceBookletSeller.TbNation.DisplayMemberPath = "DisplayMember";
            ResidenceBookletSeller.TbNation.SelectedValuePath = "ValueMember";

            #endregion

            #region 获得本机所有打印机
            var printers = PrinterSettings.InstalledPrinters;//获取本机上的所有打印机
            //PrintModel pm1 = new PrintModel();
            //pm1.PrintNo = -1;
            //pm1.PrintName = "请选择打印机";
            //_ListPrint.Add(pm1);
            PrintModel pm = new PrintModel();
            pm.PrintNo = 0;
            pm.PrintName = "未分配";
            _ListPrint.Add(pm);

            for (int i = 0; i < printers.Count; i++)
            {
                PrintModel print = new PrintModel();
                print.PrintNo = i + 1;
                print.PrintName = printers[i].ToString();
                _ListPrint.Add(print);
            }
            #endregion
            //从注册表加载 条形码打印机信息          
            _ReceiptPrintName = PrivateProfileUtils.GetContentValue("BarCodePrint", "PrintName"); //打印机名

            CboBuyerIdentity.SelectionChanged += CboBuyerIdentity_SelectionChanged;
            CboSellerIdentity.SelectionChanged += CboSellerIdentity_SelectionChanged;


            //快捷键触发事件
            KeyDown += AddCasePage_KeyDown;
            CaseSourceCb.ItemsSource = EnumHelper.InitSourceTypeCombobox();
            CaseSourceCb.DisplayMemberPath = "DisplayMember";
            CaseSourceCb.SelectedValue = "ValueMember";
            CaseSourceCb.SelectedIndex = 0;

        }

        /// <summary>
        /// 页面加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //从注册表加载 生成条形码数         
                string sBarCodeNum = PrivateProfileUtils.GetContentValue("BarCodeNum", "Num"); //生成条形码数  
                if (sBarCodeNum != null && sBarCodeNum != "")
                {
                    _BarCodeNum = int.Parse(PrivateProfileUtils.GetContentValue("BarCodeNum", "Num"));
                }
                else
                {
                    _BarCodeNum = 1;
                }

            }
            catch (Exception ex)
            {

            }
        }


        #region

        private void ReadOperate(Button btnRead, Button btnRevoke)
        {
            btnRead.Visibility = Visibility.Collapsed;
            btnRevoke.Visibility = Visibility.Visible;
        }

        private void RevokeOperate(Button btnRead, Button btnRevoke)
        {
            btnRead.Visibility = Visibility.Visible;
            btnRevoke.Visibility = Visibility.Collapsed;
        }

        //private void SaveOperate(Button btnRead, Button btnRevoke)
        //{
        //    btnRead.Visibility = Visibility.Visible;
        //    btnRevoke.Visibility = Visibility.Collapsed;
        //}

        #endregion


        /// <summary>
        /// 保存买方证件信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuyerSave_Click(object sender, RoutedEventArgs e)
        {
            //000 获取证件类型
            var certificateType = CertificateType.IdCard;
            certificateType = (CertificateType)Enum.Parse(typeof(CertificateType), Convert.ToString(CboBuyerIdentity.SelectedValue));
            long userSysNo;
            var checkDataResult = false;
            var checkIdCardNoInItemSourceResult = false;
            OperationResult saveUserResult = null;
            switch (certificateType)
            {
                case CertificateType.IdCard:
                    #region 身份证
                    //000 检查是否修改数据
                    //if (TblBuyerIdentityNo.Tag != null)
                    if (IdCardBuyer.TblIdentityNo.Tag != null)
                    {
                        long.TryParse(Convert.ToString(IdCardBuyer.TblIdentityNo.Tag), out userSysNo);
                        //001-1修改数据
                        if (userSysNo > 0)
                        {
                            saveUserResult = ModifyBuyerUserIdentity(CertificateType.IdCard, userSysNo);
                        }
                    }
                    else
                    {
                        //001-2检查数据合法性
                        checkDataResult = CheckBuyerData(CertificateType.IdCard);
                        if (!checkDataResult) { return; }
                        //002检查证件是否已经存在
                        checkIdCardNoInItemSourceResult = CheckIdCardNoInItemSource(LbBuyerList.ItemsSource, IdCardBuyer.TblIdentityNo.Text.Trim());
                        if (!checkIdCardNoInItemSourceResult) { return; }
                        //string idNum = TblSellerIdentityNo.Text;
                        //003 保存数据
                        //saveUserResult = AddBuyerUserIdentity(CertificateType.IdCard);
                        saveUserResult = AddBuyerUserIdentityNew(CertificateType.IdCard);
                        if (saveUserResult.Success)
                        {
                            string idSym = saveUserResult.OtherData["UserSysNo"].ToString();
                            MessageBoxResult result = MessageBox.Show("是否上传身份证页面", "系统提示", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                            if (result == MessageBoxResult.OK)
                            {
                                new CaptureIdCard(idSym);
                            }
                        }


                    }
                    #endregion
                    break;
                case CertificateType.Passport:
                    #region 护照
                    //000 检查是否修改数据
                    if (PassportBuyer.TbIdentityNo.Tag != null)
                    {
                        long.TryParse(Convert.ToString(PassportBuyer.TbIdentityNo.Tag), out userSysNo);
                        //修改数据
                        if (userSysNo > 0)
                        {
                            //saveUserResult = ModifyBuyerUserIdentity(CertificateType.Passport, userSysNo);
                            saveUserResult = ModifyBuyerUserIdentityNew(CertificateType.Passport, userSysNo);
                            BtnBuyerRevoke_Click(sender, e);
                            return;
                        }
                    }
                    //001检查数据合法性
                    //checkDataResult = CheckBuyerData(CertificateType.Passport);
                    checkDataResult = CheckBuyerDataNew(CertificateType.Passport);
                    if (!checkDataResult) { return; }
                    //002检查证件是否已经存在
                    checkIdCardNoInItemSourceResult = CheckIdCardNoInItemSource(LbBuyerList.ItemsSource, PassportBuyer.TbIdentityNo.Text.Trim());
                    if (!checkIdCardNoInItemSourceResult) { return; }
                    //003 保存数据
                    //saveUserResult = AddBuyerUserIdentity(CertificateType.Passport);
                    saveUserResult = AddBuyerUserIdentityNew(CertificateType.Passport);
                    #endregion
                    break;
                case CertificateType.HKMacaoIdCard:
                    #region 港澳居民身份证
                    //000 检查是否修改数据
                    if (HKMacaoIdCardBuyer.TbIdentityNo.Tag != null)
                    {
                        long.TryParse(Convert.ToString(HKMacaoIdCardBuyer.TbIdentityNo.Tag), out userSysNo);
                        //修改数据
                        if (userSysNo > 0)
                        {
                            saveUserResult = ModifyBuyerUserIdentityNew(CertificateType.HKMacaoIdCard, userSysNo);
                            BtnBuyerRevoke_Click(sender, e);
                            return;
                        }
                    }
                    //001检查数据合法性
                    checkDataResult = CheckBuyerDataNew(CertificateType.HKMacaoIdCard);
                    if (!checkDataResult) { return; }
                    //002检查证件是否已经存在
                    checkIdCardNoInItemSourceResult = CheckIdCardNoInItemSource(LbBuyerList.ItemsSource, HKMacaoIdCardBuyer.TbIdentityNo.Text.Trim());
                    if (!checkIdCardNoInItemSourceResult) { return; }
                    //003 保存数据
                    saveUserResult = AddBuyerUserIdentityNew(CertificateType.HKMacaoIdCard);
                    #endregion
                    break;
                case CertificateType.BirthCertificate:
                    #region 出生证
                    //000 检查是否修改数据
                    if (BirthCertificateBuyer.TbIdentityNo.Tag != null)
                    {
                        long.TryParse(Convert.ToString(BirthCertificateBuyer.TbIdentityNo.Tag), out userSysNo);
                        //修改数据
                        if (userSysNo > 0)
                        {
                            saveUserResult = ModifyBuyerUserIdentityNew(CertificateType.BirthCertificate, userSysNo);
                            BtnBuyerRevoke_Click(sender, e);
                            return;
                        }
                    }
                    //001检查数据合法性
                    checkDataResult = CheckBuyerDataNew(CertificateType.BirthCertificate);
                    if (!checkDataResult) { return; }
                    //002检查证件是否已经存在
                    checkIdCardNoInItemSourceResult = CheckIdCardNoInItemSource(LbBuyerList.ItemsSource, BirthCertificateBuyer.TbIdentityNo.Text.Trim());
                    if (!checkIdCardNoInItemSourceResult) { return; }
                    //003 保存数据
                    saveUserResult = AddBuyerUserIdentityNew(CertificateType.BirthCertificate);
                    #endregion
                    break;
                case CertificateType.MacaoPermit:
                    #region 港澳居民往来大陆通行证
                    //000 检查是否修改数据
                    if (MacaoPermitBuyer.TbIdentityNo.Tag != null)
                    {
                        long.TryParse(Convert.ToString(MacaoPermitBuyer.TbIdentityNo.Tag), out userSysNo);
                        //修改数据
                        if (userSysNo > 0)
                        {
                            saveUserResult = ModifyBuyerUserIdentityNew(CertificateType.MacaoPermit, userSysNo);
                            BtnBuyerRevoke_Click(sender, e);
                            return;
                        }
                    }
                    //001检查数据合法性
                    checkDataResult = CheckBuyerDataNew(CertificateType.MacaoPermit);
                    if (!checkDataResult) { return; }
                    //002检查证件是否已经存在
                    checkIdCardNoInItemSourceResult = CheckIdCardNoInItemSource(LbBuyerList.ItemsSource, MacaoPermitBuyer.TbIdentityNo.Text.Trim());
                    if (!checkIdCardNoInItemSourceResult) { return; }
                    //003 保存数据
                    saveUserResult = AddBuyerUserIdentityNew(CertificateType.MacaoPermit);
                    #endregion
                    break;
                case CertificateType.TaiwanPermit:
                    #region 台湾居民往来大陆通行证
                    //000 检查是否修改数据
                    if (TaiwanPermitBuyer.TbIdentityNo.Tag != null)
                    {
                        long.TryParse(Convert.ToString(TaiwanPermitBuyer.TbIdentityNo.Tag), out userSysNo);
                        //修改数据
                        if (userSysNo > 0)
                        {
                            saveUserResult = ModifyBuyerUserIdentityNew(CertificateType.TaiwanPermit, userSysNo);
                            BtnBuyerRevoke_Click(sender, e);
                            return;
                        }
                    }
                    //001检查数据合法性
                    checkDataResult = CheckBuyerDataNew(CertificateType.TaiwanPermit);
                    if (!checkDataResult) { return; }
                    //002检查证件是否已经存在
                    checkIdCardNoInItemSourceResult = CheckIdCardNoInItemSource(LbBuyerList.ItemsSource, TaiwanPermitBuyer.TbIdentityNo.Text.Trim());
                    if (!checkIdCardNoInItemSourceResult) { return; }
                    //003 保存数据
                    saveUserResult = AddBuyerUserIdentityNew(CertificateType.TaiwanPermit);
                    #endregion
                    break;
                case CertificateType.CertificateOfOfficers:
                    #region 军官证
                    //000 检查是否修改数据
                    if (CertificateOfOfficersBuyer.TbIdentityNo.Tag != null)
                    {
                        long.TryParse(Convert.ToString(CertificateOfOfficersBuyer.TbIdentityNo.Tag), out userSysNo);
                        //修改数据
                        if (userSysNo > 0)
                        {
                            saveUserResult = ModifyBuyerUserIdentityNew(CertificateType.CertificateOfOfficers, userSysNo);
                            BtnBuyerRevoke_Click(sender, e);
                            return;
                        }
                    }
                    //001检查数据合法性
                    checkDataResult = CheckBuyerDataNew(CertificateType.CertificateOfOfficers);
                    if (!checkDataResult) { return; }
                    //002检查证件是否已经存在
                    checkIdCardNoInItemSourceResult = CheckIdCardNoInItemSource(LbBuyerList.ItemsSource, CertificateOfOfficersBuyer.TbIdentityNo.Text.Trim());
                    if (!checkIdCardNoInItemSourceResult) { return; }
                    //003 保存数据
                    saveUserResult = AddBuyerUserIdentityNew(CertificateType.CertificateOfOfficers);
                    #endregion
                    break;
                case CertificateType.ResidenceBooklet:
                    #region 户口簿
                    //000 检查是否修改数据
                    if (ResidenceBookletBuyer.TbIdentityNo.Tag != null)
                    {
                        long.TryParse(Convert.ToString(ResidenceBookletBuyer.TbIdentityNo.Tag), out userSysNo);
                        //修改数据
                        if (userSysNo > 0)
                        {
                            saveUserResult = ModifyBuyerUserIdentityNew(CertificateType.ResidenceBooklet, userSysNo);
                            BtnBuyerRevoke_Click(sender, e);
                            return;
                        }
                    }
                    //001检查数据合法性
                    checkDataResult = CheckBuyerDataNew(CertificateType.ResidenceBooklet);
                    if (!checkDataResult) { return; }
                    //002检查证件是否已经存在
                    checkIdCardNoInItemSourceResult = CheckIdCardNoInItemSource(LbBuyerList.ItemsSource, ResidenceBookletBuyer.TbIdentityNo.Text.Trim());
                    if (!checkIdCardNoInItemSourceResult) { return; }
                    //003 保存数据
                    saveUserResult = AddBuyerUserIdentityNew(CertificateType.ResidenceBooklet);
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// 保存卖家证件信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSellerSave_Click(object sender, RoutedEventArgs e)
        {
            //000 获取证件类型
            var certificateType = CertificateType.IdCard;
            certificateType = (CertificateType)Enum.Parse(typeof(CertificateType), Convert.ToString(CboSellerIdentity.SelectedValue));
            long userSysNo;
            var checkDataResult = false;
            var checkIdCardNoInItemSourceResult = false;
            OperationResult saveUserResult = null;
            switch (certificateType)
            {
                case CertificateType.IdCard:
                    #region 身份证
                    //000 检查是否修改数据
                    if (IdCardSeller.TblIdentityNo.Tag != null)
                    {
                        long.TryParse(Convert.ToString(IdCardSeller.TblIdentityNo.Tag), out userSysNo);
                        //001-1修改数据
                        if (userSysNo > 0)
                        {
                            //saveUserResult = ModifySellerUserIdentity(CertificateType.IdCard, userSysNo);
                            saveUserResult = ModifySellerUserIdentityNew(CertificateType.IdCard, userSysNo);
                        }
                    }
                    else
                    {
                        //001-2检查数据合法性
                        //checkDataResult = CheckSellerData(CertificateType.IdCard);
                        checkDataResult = CheckSellerDataNew(CertificateType.IdCard);
                        if (!checkDataResult) { return; }
                        //002检查证件是否已经存在
                        //checkIdCardNoInItemSourceResult = CheckIdCardNoInItemSource(LbSellerList.ItemsSource, IdCardSeller.TblIdentityNo.Text.Trim());
                        checkIdCardNoInItemSourceResult = CheckSellerIdCardNoInItemSource(LbSellerList.ItemsSource, IdCardSeller.TblIdentityNo.Text.Trim());
                        if (!checkIdCardNoInItemSourceResult) { return; }
                        //003 保存数据
                        //saveUserResult = AddSellerUserIdentity(CertificateType.IdCard);
                        saveUserResult = AddSellerUserIdentityNew(CertificateType.IdCard);
                        if (saveUserResult.Success)
                        {
                            string idSym = saveUserResult.OtherData["UserSysNo"].ToString();
                            MessageBoxResult result = MessageBox.Show("是否上传身份证页面", "系统提示", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                            if (result == MessageBoxResult.OK)
                            {
                                new CaptureIdCard(idSym);
                            }
                        }
                    }
                    #endregion
                    break;
                case CertificateType.Passport:
                    #region 护照
                    //000 检查是否修改数据
                    if (PassportSeller.TbIdentityNo.Tag != null)
                    {
                        long.TryParse(Convert.ToString(PassportSeller.TbIdentityNo.Tag), out userSysNo);
                        //修改数据
                        if (userSysNo > 0)
                        {
                            //saveUserResult = ModifySellerUserIdentity(CertificateType.Passport, userSysNo);
                            saveUserResult = ModifySellerUserIdentityNew(CertificateType.Passport, userSysNo);
                            BtnSellerRevoke_Click(sender, e);
                            return;
                        }
                    }
                    //001检查数据合法性
                    //checkDataResult = CheckSellerData(CertificateType.Passport);
                    checkDataResult = CheckSellerDataNew(CertificateType.Passport);
                    if (!checkDataResult) { return; }
                    //002检查证件是否已经存在
                    //checkIdCardNoInItemSourceResult = CheckIdCardNoInItemSource(LbSellerList.ItemsSource, TbSellerIdentityNo.Text.Trim());
                    checkIdCardNoInItemSourceResult = CheckSellerIdCardNoInItemSource(LbSellerList.ItemsSource, PassportSeller.TbIdentityNo.Text.Trim());
                    if (!checkIdCardNoInItemSourceResult) { return; }
                    //003 保存数据
                    //saveUserResult = AddSellerUserIdentity(CertificateType.Passport);
                    saveUserResult = AddSellerUserIdentityNew(CertificateType.Passport);
                    #endregion
                    break;
                case CertificateType.HKMacaoIdCard:
                    #region 港澳居民身份证
                    //000 检查是否修改数据
                    if (HKMacaoIdCardSeller.TbIdentityNo.Tag != null)
                    {
                        long.TryParse(Convert.ToString(HKMacaoIdCardSeller.TbIdentityNo.Tag), out userSysNo);
                        //修改数据
                        if (userSysNo > 0)
                        {
                            saveUserResult = ModifySellerUserIdentityNew(CertificateType.HKMacaoIdCard, userSysNo);
                            BtnSellerRevoke_Click(sender, e);
                            return;
                        }
                    }
                    //001检查数据合法性
                    checkDataResult = CheckSellerDataNew(CertificateType.HKMacaoIdCard);
                    if (!checkDataResult) { return; }
                    //002检查证件是否已经存在
                    checkIdCardNoInItemSourceResult = CheckSellerIdCardNoInItemSource(LbSellerList.ItemsSource, HKMacaoIdCardSeller.TbIdentityNo.Text.Trim());
                    if (!checkIdCardNoInItemSourceResult) { return; }
                    //003 保存数据
                    saveUserResult = AddSellerUserIdentityNew(CertificateType.HKMacaoIdCard);
                    #endregion
                    break;
                case CertificateType.BirthCertificate:
                    #region 出生证
                    //000 检查是否修改数据
                    if (BirthCertificateSeller.TbIdentityNo.Tag != null)
                    {
                        long.TryParse(Convert.ToString(BirthCertificateSeller.TbIdentityNo.Tag), out userSysNo);
                        //修改数据
                        if (userSysNo > 0)
                        {
                            saveUserResult = ModifySellerUserIdentityNew(CertificateType.BirthCertificate, userSysNo);
                            BtnSellerRevoke_Click(sender, e);
                            return;
                        }
                    }
                    //001检查数据合法性
                    checkDataResult = CheckSellerDataNew(CertificateType.BirthCertificate);
                    if (!checkDataResult) { return; }
                    //002检查证件是否已经存在
                    checkIdCardNoInItemSourceResult = CheckSellerIdCardNoInItemSource(LbSellerList.ItemsSource, BirthCertificateSeller.TbIdentityNo.Text.Trim());
                    if (!checkIdCardNoInItemSourceResult) { return; }
                    //003 保存数据
                    saveUserResult = AddSellerUserIdentityNew(CertificateType.BirthCertificate);
                    #endregion
                    break;
                case CertificateType.MacaoPermit:
                    #region 港澳居民往来大陆通行证
                    //000 检查是否修改数据
                    if (MacaoPermitSeller.TbIdentityNo.Tag != null)
                    {
                        long.TryParse(Convert.ToString(MacaoPermitSeller.TbIdentityNo.Tag), out userSysNo);
                        //修改数据
                        if (userSysNo > 0)
                        {
                            saveUserResult = ModifySellerUserIdentityNew(CertificateType.MacaoPermit, userSysNo);
                            BtnSellerRevoke_Click(sender, e);
                            return;
                        }
                    }
                    //001检查数据合法性
                    checkDataResult = CheckSellerDataNew(CertificateType.MacaoPermit);
                    if (!checkDataResult) { return; }
                    //002检查证件是否已经存在
                    checkIdCardNoInItemSourceResult = CheckSellerIdCardNoInItemSource(LbSellerList.ItemsSource, MacaoPermitSeller.TbIdentityNo.Text.Trim());
                    if (!checkIdCardNoInItemSourceResult) { return; }
                    //003 保存数据
                    saveUserResult = AddSellerUserIdentityNew(CertificateType.MacaoPermit);
                    #endregion
                    break;
                case CertificateType.TaiwanPermit:
                    #region 台湾居民往来大陆通行证
                    //000 检查是否修改数据
                    if (TaiwanPermitSeller.TbIdentityNo.Tag != null)
                    {
                        long.TryParse(Convert.ToString(TaiwanPermitSeller.TbIdentityNo.Tag), out userSysNo);
                        //修改数据
                        if (userSysNo > 0)
                        {
                            saveUserResult = ModifySellerUserIdentityNew(CertificateType.TaiwanPermit, userSysNo);
                            BtnSellerRevoke_Click(sender, e);
                            return;
                        }
                    }
                    //001检查数据合法性
                    checkDataResult = CheckSellerDataNew(CertificateType.TaiwanPermit);
                    if (!checkDataResult) { return; }
                    //002检查证件是否已经存在
                    checkIdCardNoInItemSourceResult = CheckSellerIdCardNoInItemSource(LbSellerList.ItemsSource, TaiwanPermitSeller.TbIdentityNo.Text.Trim());
                    if (!checkIdCardNoInItemSourceResult) { return; }
                    //003 保存数据
                    saveUserResult = AddSellerUserIdentityNew(CertificateType.TaiwanPermit);
                    #endregion
                    break;
                case CertificateType.CertificateOfOfficers:
                    #region 军官证
                    //000 检查是否修改数据
                    if (CertificateOfOfficersSeller.TbIdentityNo.Tag != null)
                    {
                        long.TryParse(Convert.ToString(CertificateOfOfficersSeller.TbIdentityNo.Tag), out userSysNo);
                        //修改数据
                        if (userSysNo > 0)
                        {
                            saveUserResult = ModifySellerUserIdentityNew(CertificateType.CertificateOfOfficers, userSysNo);
                            BtnSellerRevoke_Click(sender, e);
                            return;
                        }
                    }
                    //001检查数据合法性
                    checkDataResult = CheckSellerDataNew(CertificateType.CertificateOfOfficers);
                    if (!checkDataResult) { return; }
                    //002检查证件是否已经存在
                    checkIdCardNoInItemSourceResult = CheckSellerIdCardNoInItemSource(LbSellerList.ItemsSource, CertificateOfOfficersSeller.TbIdentityNo.Text.Trim());
                    if (!checkIdCardNoInItemSourceResult) { return; }
                    //003 保存数据
                    saveUserResult = AddSellerUserIdentityNew(CertificateType.CertificateOfOfficers);
                    #endregion
                    break;
                case CertificateType.ResidenceBooklet:
                    #region 户口簿
                    //000 检查是否修改数据
                    if (ResidenceBookletSeller.TbIdentityNo.Tag != null)
                    {
                        long.TryParse(Convert.ToString(ResidenceBookletSeller.TbIdentityNo.Tag), out userSysNo);
                        //修改数据
                        if (userSysNo > 0)
                        {
                            saveUserResult = ModifySellerUserIdentityNew(CertificateType.ResidenceBooklet, userSysNo);
                            BtnSellerRevoke_Click(sender, e);
                            return;
                        }
                    }
                    //001检查数据合法性
                    checkDataResult = CheckSellerDataNew(CertificateType.ResidenceBooklet);
                    if (!checkDataResult) { return; }
                    //002检查证件是否已经存在
                    checkIdCardNoInItemSourceResult = CheckSellerIdCardNoInItemSource(LbSellerList.ItemsSource, ResidenceBookletSeller.TbIdentityNo.Text.Trim());
                    if (!checkIdCardNoInItemSourceResult) { return; }
                    //003 保存数据
                    saveUserResult = AddSellerUserIdentityNew(CertificateType.ResidenceBooklet);
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// 删除买方信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnBuyerDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var listBuyer = (List<UserIdentityModel>)LbBuyerList.ItemsSource;
                if (listBuyer == null || listBuyer.Count == 0)
                {
                    MessageBox.Show("请选择要删除的数据.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                MessageBoxResult confirmToDel = MessageBox.Show("确认要删除所选行吗？", "提示", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (confirmToDel == MessageBoxResult.Yes)
                {
                    listBuyer.Remove((UserIdentityModel)LbBuyerList.SelectedItem);
                    LbBuyerList.ItemsSource = null;
                    LbBuyerList.ItemsSource = listBuyer;
                    LbBuyerList.DisplayMemberPath = "Name";
                    LbBuyerList.SelectedValuePath = "IdentityNo";

                    //清除页面数据
                    //ClearBuyerIdCardData();
                    //ClearBuyerPassportData();
                    
                    //New
                    //ClearIdCard(IdCardBuyer);
                    //ClearPassport(PassportBuyer);
                    //ClearHKMacaoIdCard(HKMacaoIdCardBuyer);
                    //ClearBirthCertificate(BirthCertificateBuyer);
                    //ClearMacaoPermit(MacaoPermitBuyer);
                    //ClearCertificateOfOfficers(CertificateOfOfficersBuyer);
                    //ClearResidenceBooklet(ResidenceBookletBuyer);
                    ClearBuyerIDType();
                }
                else
                {
                    return;
                }
                BtnBuyerRevoke_Click(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show("删除数据发生异常：" + ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 删除卖家信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSellerDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var listSeller = (List<UserIdentityModel>)LbSellerList.ItemsSource;
                if (listSeller == null || listSeller.Count == 0)
                {
                    MessageBox.Show("请选择要删除的数据.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                MessageBoxResult confirmToDel = MessageBox.Show("确认要删除所选行吗？", "提示", MessageBoxButton.YesNo,
                    MessageBoxImage.Question);
                if (confirmToDel == MessageBoxResult.Yes)
                {
                    listSeller.Remove((UserIdentityModel)LbSellerList.SelectedItem);
                    LbSellerList.ItemsSource = null;
                    LbSellerList.ItemsSource = listSeller;
                    LbSellerList.DisplayMemberPath = "Name";
                    LbSellerList.SelectedValuePath = "IdentityNo";

                    //清除页面数据
                    //ClearSellerIdCardData();
                    //ClearSellerPassportData();

                    //New
                    //ClearIdCard(IdCardSeller);
                    //ClearPassport(PassportSeller);
                    //ClearHKMacaoIdCard(HKMacaoIdCardSeller);
                    //ClearBirthCertificate(BirthCertificateSeller);
                    //ClearMacaoPermit(MacaoPermitSeller);
                    //ClearCertificateOfOfficers(CertificateOfOfficersSeller);
                    //ClearResidenceBooklet(ResidenceBookletSeller);
                    ClearSellerIDType();
                }
                else
                {
                    return;
                }
                BtnSellerRevoke_Click(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show("删除数据发生异常：" + ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #region 证件类型隐藏

        private void BuyerIDTypeCollapsed()
        {
            IdCardBuyer.Visibility = Visibility.Collapsed;
            PassportBuyer.Visibility = Visibility.Collapsed;
            HKMacaoIdCardBuyer.Visibility = Visibility.Collapsed;
            BirthCertificateBuyer.Visibility = Visibility.Collapsed;
            MacaoPermitBuyer.Visibility = Visibility.Collapsed;
            TaiwanPermitBuyer.Visibility = Visibility.Collapsed;
            CertificateOfOfficersBuyer.Visibility = Visibility.Collapsed;
            ResidenceBookletBuyer.Visibility = Visibility.Collapsed;
        }

        private void SellerIDTypeCollapsed()
        {
            IdCardSeller.Visibility = Visibility.Collapsed;
            PassportSeller.Visibility = Visibility.Collapsed;
            HKMacaoIdCardSeller.Visibility = Visibility.Collapsed;
            BirthCertificateSeller.Visibility = Visibility.Collapsed;
            MacaoPermitSeller.Visibility = Visibility.Collapsed;
            TaiwanPermitSeller.Visibility = Visibility.Collapsed;
            CertificateOfOfficersSeller.Visibility = Visibility.Collapsed;
            ResidenceBookletSeller.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// 买方证件类型切换
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboBuyerIdentity_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (CboBuyerIdentity.Text != "" && CboBuyerIdentity.SelectedValue.ToString() == "0")
            //{
            //    BtnBuyerRead.Visibility = Visibility.Visible;
            //    Grid_BuyerIdCard.Visibility = Visibility.Visible;
            //    Grid_BuyerPassport.Visibility = Visibility.Collapsed;
            //}
            //else
            //{
            //    BtnBuyerRead.Visibility = Visibility.Collapsed;
            //    Grid_BuyerIdCard.Visibility = Visibility.Collapsed;
            //    Grid_BuyerPassport.Visibility = Visibility.Visible;
            //}
            switch ((CertificateType)(int.Parse(CboBuyerIdentity.SelectedValue.ToString())))
            {
                case CertificateType.IdCard:
                    BuyerIDTypeCollapsed();
                    IdCardBuyer.Visibility = Visibility.Visible;
                    BtnBuyerRead.Visibility = Visibility.Visible;
                    ClearBuyerIDType();
                    break;
                case CertificateType.Passport:
                    BuyerIDTypeCollapsed();
                    PassportBuyer.Visibility = Visibility.Visible;
                    BtnBuyerRead.Visibility = Visibility.Collapsed;
                    ClearBuyerIDType();
                    break;
                case CertificateType.HKMacaoIdCard:
                    BuyerIDTypeCollapsed();
                    HKMacaoIdCardBuyer.Visibility = Visibility.Visible;
                    BtnBuyerRead.Visibility = Visibility.Collapsed;
                    ClearBuyerIDType();
                    break;
                case CertificateType.BirthCertificate:
                    BuyerIDTypeCollapsed();
                    BirthCertificateBuyer.Visibility = Visibility.Visible;
                    BtnBuyerRead.Visibility = Visibility.Collapsed;
                    ClearBuyerIDType();
                    break;
                case CertificateType.MacaoPermit:
                    BuyerIDTypeCollapsed();
                    MacaoPermitBuyer.Visibility = Visibility.Visible;
                    BtnBuyerRead.Visibility = Visibility.Collapsed;
                    ClearBuyerIDType();
                    break;
                case CertificateType.TaiwanPermit:
                    BuyerIDTypeCollapsed();
                    TaiwanPermitBuyer.Visibility = Visibility.Visible;
                    BtnBuyerRead.Visibility = Visibility.Collapsed;
                    ClearBuyerIDType();
                    break;
                case CertificateType.CertificateOfOfficers:
                    BuyerIDTypeCollapsed();
                    CertificateOfOfficersBuyer.Visibility = Visibility.Visible;
                    BtnBuyerRead.Visibility = Visibility.Collapsed;
                    ClearBuyerIDType();
                    break;
                case CertificateType.ResidenceBooklet:
                    BuyerIDTypeCollapsed();
                    ResidenceBookletBuyer.Visibility = Visibility.Visible;
                    BtnBuyerRead.Visibility = Visibility.Collapsed;
                    ClearBuyerIDType();
                    break;
            }
        }

        /// <summary>
        /// 卖家证件类型切换
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboSellerIdentity_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (CboSellerIdentity.Text != "" && CboSellerIdentity.SelectedValue.ToString() == "0")
            //{
            //    BtnSellerRead.Visibility = Visibility.Visible;
            //    Grid_SellerIdCard.Visibility = Visibility.Visible;
            //    Grid_SellerPassport.Visibility = Visibility.Collapsed;
            //}
            //else
            //{
            //    BtnSellerRead.Visibility = Visibility.Collapsed;
            //    Grid_SellerIdCard.Visibility = Visibility.Collapsed;
            //    Grid_SellerPassport.Visibility = Visibility.Visible;
            //}
            switch ((CertificateType)(int.Parse(CboSellerIdentity.SelectedValue.ToString())))
            {
                case CertificateType.IdCard:
                    SellerIDTypeCollapsed();
                    IdCardSeller.Visibility = Visibility.Visible;
                    BtnSellerRead.Visibility = Visibility.Visible;
                    ClearSellerIDType();
                    break;
                case CertificateType.Passport:
                    SellerIDTypeCollapsed();
                    PassportSeller.Visibility = Visibility.Visible;
                    BtnSellerRead.Visibility = Visibility.Collapsed;
                    ClearSellerIDType();
                    break;
                case CertificateType.HKMacaoIdCard:
                    SellerIDTypeCollapsed();
                    HKMacaoIdCardSeller.Visibility = Visibility.Visible;
                    BtnSellerRead.Visibility = Visibility.Collapsed;
                    ClearSellerIDType();
                    break;
                case CertificateType.BirthCertificate:
                    SellerIDTypeCollapsed();
                    BirthCertificateSeller.Visibility = Visibility.Visible;
                    BtnSellerRead.Visibility = Visibility.Collapsed;
                    ClearSellerIDType();
                    break;
                case CertificateType.MacaoPermit:
                    SellerIDTypeCollapsed();
                    MacaoPermitSeller.Visibility = Visibility.Visible;
                    BtnSellerRead.Visibility = Visibility.Collapsed;
                    ClearSellerIDType();
                    break;
                case CertificateType.TaiwanPermit:
                    SellerIDTypeCollapsed();
                    TaiwanPermitSeller.Visibility = Visibility.Visible;
                    BtnSellerRead.Visibility = Visibility.Collapsed;
                    ClearSellerIDType();
                    break;
                case CertificateType.CertificateOfOfficers:
                    SellerIDTypeCollapsed();
                    CertificateOfOfficersSeller.Visibility = Visibility.Visible;
                    BtnSellerRead.Visibility = Visibility.Collapsed;
                    ClearSellerIDType();
                    break;
                case CertificateType.ResidenceBooklet:
                    SellerIDTypeCollapsed();
                    ResidenceBookletSeller.Visibility = Visibility.Visible;
                    BtnSellerRead.Visibility = Visibility.Collapsed;
                    ClearSellerIDType();
                    break;
            }
        }

        #endregion

        #region 生成案件

        /// <summary>
        /// 生成案件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCreateCase_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SaveCase();
            }
            catch (Exception ex)
            {
                MessageBox.Show("生成案件系统发生异常：" + ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 保存案件
        /// </summary>
        /// <returns></returns>
        private OperationResult SaveCase()
        {
            //买方
            var listBuyersUserSysNo = new List<long>();
            var enumerableBuyers = LbBuyerList.ItemsSource;
            if (enumerableBuyers == null)
            {
                MessageBox.Show("至少有一个买方.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return null;
            }
            foreach (var item in enumerableBuyers)
            {
                var user = item as UserIdentityModel;
                if (user == null) continue;
                long userSysNo;
                long.TryParse(Convert.ToString(user.Tag), out userSysNo);
                listBuyersUserSysNo.Add(userSysNo);
            }

            //卖家
            var listSellersUserSysNo = new List<long>();
            var enumerableSellers = LbSellerList.ItemsSource;
            if (enumerableSellers == null)
            {
                MessageBox.Show("至少有一个卖方.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return null;
            }
            foreach (var item in enumerableSellers)
            {
                var user = item as UserIdentityModel;
                if (user == null) continue;
                long userSysNo;
                long.TryParse(Convert.ToString(user.Tag), out userSysNo);
                listSellersUserSysNo.Add(userSysNo);
            }

            //if (string.IsNullOrEmpty(TbTenementContractWord.Text) ||
            //    string.IsNullOrEmpty(TbTenementContract.Text))
            //{
            //    MessageBox.Show("请填写产权证号", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return null;
            //}
            if (string.IsNullOrEmpty(TbTenementContract.Text))
            {
                MessageBox.Show("请填写产权证号", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return null;
            }

            if (string.IsNullOrEmpty(TbTenementAddress.Text))
            {
                MessageBox.Show("请填写房屋坐落", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return null;
            }

            if (_AgentStaff.SysNo == 0 && _AgentCompany.SysNo == 0)
            {
                MessageBox.Show("请选择经纪人", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return null;
            }


            var caseDto = new CaseDto()
            {
                Buyers = listBuyersUserSysNo,
                Sellers = listSellersUserSysNo,
                TenementContract = TbTenementContractWord.Text.Trim() + "-" + TbTenementContract.Text.Trim(), //产权证号
                TenementAddress = TbTenementAddress.Text.Trim(), //房屋座落
                OperatorSysNo = LoginHelper.CurrentUser.SysNo, //受理人
                AgencySysNo = _AgentStaff.SysNo,
                CompanySysNo = _AgentCompany.SysNo,
                CreateUserSysNo = LoginHelper.CurrentUser.SysNo,
                SourceType = certificateType
            };
            OperationResult result = AddNewCase(caseDto);

            if (result.Success)
            {
                string sMessage = "添加案件成功\n案件编号：" + result.ResultMessage; //案件编号
                MessageBox.Show(sMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                PageHelper.PageNavigateHelper(this, "Main/PreCheck/PreCheckSearchListPage.xaml");
            }
            else
            {
                MessageBox.Show("添加案件失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
            }


            return result;
        }

        /// <summary>
        /// 生成案件并打印条形码
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCreateCaseAndPrintBarcode_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                #region 条形码打印机信息
                int printCount = 0;

                if (!string.IsNullOrWhiteSpace(_ReceiptPrintName))
                {
                    for (int j = 0; j < _ListPrint.Count; j++)
                    {
                        if (_ReceiptPrintName == _ListPrint[j].PrintName)
                        {
                            printCount++;
                        }
                    }
                }
                else //为空，则 收据打印机未分配
                {
                    MessageBox.Show("该条形码打印机未设置，无法进行打印操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (printCount == 0)
                {
                    MessageBox.Show("该条形码打印机未设置，无法进行打印操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (_ReceiptPrintName.Contains("未分配"))
                {
                    MessageBox.Show("该条形码打印机未设置，无法进行打印操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                #endregion

                var result = SaveCase();
                if (result != null && result.ResultMessage != null)
                {
                    for (int i = 0; i < _BarCodeNum; i++)
                    {
                        var icp = new CaseCodePrint();
                        icp.PrintCaseBarCode(result.ResultMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("生成案件系统发生异常：" + ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #endregion

        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, "Main/PreCheck/PreCheckSearchListPage.xaml");
        }

        #region 买方、卖家、经纪人读取身份证信息
        /// <summary>
        /// 经纪人读取身份证信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAgent_Click(object sender, RoutedEventArgs e)
        {
            _idCardReadType = (int)IdCardRead.Agent;
            CreatIdCard();
        }

        /// <summary>
        /// 买方读取身份证信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnBuyerRead_Click(object sender, RoutedEventArgs e)
        {
            btn_BuyerSave.Focus();
            //ClearBuyerIdCardData();
            ClearIdCard(IdCardBuyer); //New
            _idCardReadType = (int)IdCardRead.Buyer;
            CreatIdCard();
        }

        /// <summary>
        /// 卖家读取身份证信息
        /// </summary>
        private void BtnSellerRead_Click(object sender, RoutedEventArgs e)
        {
            BtnSellerSave.Focus();
            //ClearSellerIdCardData();
            ClearIdCard(IdCardSeller); //New
            _idCardReadType = (int)IdCardRead.Seller;
            CreatIdCard();
        }

        private IdCard _idCard;

        private void CreatIdCard()
        {
            IdentityInfo = new UserIdentityModel();
            _idCard = new IdCard(IdentityInfo, _idCardReadType);

            //if (_idCard.DeviceNotExists)
            //    return;                    
            _idCard.Closed += _idCard_Closed;
            _idCard.ShowDialog();
        }

        void _idCard_Closed(object sender, EventArgs e)
        {

            //根据不同的类型赋值
            switch (_idCardReadType)
            {
                case 1:
                    //GetBuyerValue();
                    GetBuyerValueNew();
                    break;
                case 2:
                    //GetSellerValue();
                    GetSellerValueNew();
                    break;
                case 3:
                    GetAgentValue();
                    break;
                default:
                    MessageBox.Show("系统错误", "提示");
                    break;
            }
        }

        #endregion

        #region 读取身份证页面关闭事件

        private void GetBuyerValueNew()
        {
            if (IdentityInfo.IdentityNo == null)
                return;

            if (!IdentityInfo.IsTrusted)
            {
                IdCardBuyer.TblName.Text = IdentityInfo.Name;
                IdCardBuyer.TblIdentityNo.Text = IdentityInfo.IdentityNo;
            }
            else
            {
                IdCardBuyer.TblName.Text = IdentityInfo.Name;
                switch (IdentityInfo.Gender)
                {
                    case Gender.Male:
                        IdCardBuyer.TblGender.Text = "男";
                        break;
                    case Gender.Female:
                        IdCardBuyer.TblGender.Text = "女";
                        break;
                    default:
                        IdCardBuyer.TblGender.Text = "";
                        break;
                }
                IdCardBuyer.TblNation.Text = IdentityInfo.Nation.ToString();
                if (IdentityInfo.Birthday != null)
                    IdCardBuyer.TblBirthday.Text = ((DateTime)IdentityInfo.Birthday).ToString("yyyy.MM.dd");
                IdCardBuyer.TblAddress.Text = IdentityInfo.Address;
                IdCardBuyer.TblIdentityNo.Text = IdentityInfo.IdentityNo;
                IdCardBuyer.TblVisaAgency.Text = IdentityInfo.VisaAgency;
                if (IdentityInfo.EffectiveDate != null)
                    IdCardBuyer.TblEffectiveDate.Text = ((DateTime)IdentityInfo.EffectiveDate).ToString("yyyy.MM.dd");
                IdCardBuyer.TblStr.Text = "至";
                if (IdentityInfo.ExpiryDate != null)
                {
                    IdCardBuyer.TblExpiryDate.Text = ((DateTime)IdentityInfo.ExpiryDate).ToString("yyyy.MM.dd");
                }
                else
                {
                    IdCardBuyer.TblExpiryDate.Text = "长期";
                }
                //照片显示
                try
                {
                    BitmapImage bitmap = new BitmapImage();
                    MemoryStream ms1 = new MemoryStream(IdentityInfo.Photo);

                    bitmap.BeginInit();
                    bitmap.StreamSource = ms1;
                    bitmap.EndInit();

                    IdCardBuyer.ImgPhoto.Source = bitmap;
                }
                catch (Exception)
                {
                    //throw new ArgumentException("照片读取失败");
                    MessageBox.Show("照片读取失败!", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
            }

        }

        private void GetSellerValueNew()
        {
            if (IdentityInfo.IdentityNo == null)
                return;

            if (!IdentityInfo.IsTrusted)
            {
                IdCardSeller.TblName.Text = IdentityInfo.Name;
                IdCardSeller.TblIdentityNo.Text = IdentityInfo.IdentityNo;
            }
            else
            {
                IdCardSeller.TblName.Text = IdentityInfo.Name;
                switch (IdentityInfo.Gender)
                {
                    case Gender.Male:
                        IdCardSeller.TblGender.Text = "男";
                        break;
                    case Gender.Female:
                        IdCardSeller.TblGender.Text = "女";
                        break;
                    default:
                        IdCardSeller.TblGender.Text = "";
                        break;
                }
                IdCardSeller.TblNation.Text = IdentityInfo.Nation.ToString();
                if (IdentityInfo.Birthday != null)
                    IdCardSeller.TblBirthday.Text = ((DateTime)IdentityInfo.Birthday).ToString("yyyy.MM.dd");
                IdCardSeller.TblAddress.Text = IdentityInfo.Address;
                IdCardSeller.TblIdentityNo.Text = IdentityInfo.IdentityNo;
                IdCardSeller.TblVisaAgency.Text = IdentityInfo.VisaAgency;
                if (IdentityInfo.EffectiveDate != null)
                    IdCardSeller.TblEffectiveDate.Text = ((DateTime)IdentityInfo.EffectiveDate).ToString("yyyy.MM.dd");
                IdCardSeller.TblStr.Text = "至";
                if (IdentityInfo.ExpiryDate != null)
                {
                    IdCardSeller.TblExpiryDate.Text = ((DateTime)IdentityInfo.ExpiryDate).ToString("yyyy.MM.dd");
                }
                else
                {
                    IdCardSeller.TblExpiryDate.Text = "长期";
                }
                //照片显示
                try
                {
                    BitmapImage bitmap = new BitmapImage();
                    MemoryStream ms1 = new MemoryStream(IdentityInfo.Photo);

                    bitmap.BeginInit();
                    bitmap.StreamSource = ms1;
                    bitmap.EndInit();

                    IdCardSeller.ImgPhoto.Source = bitmap;
                }
                catch (Exception)
                {
                    //throw new ArgumentException("照片读取失败");
                    MessageBox.Show("照片读取失败!", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
            }

        }

        /// <summary>
        /// 读取买方信息
        /// </summary>
        private void GetBuyerValue()
        {
            if (IdentityInfo.IdentityNo == null)
                return;

            if (!IdentityInfo.IsTrusted)
            {
                TblBuyerName.Text = IdentityInfo.Name;
                TblBuyerIdentityNo.Text = IdentityInfo.IdentityNo;
            }
            else
            {
                TblBuyerName.Text = IdentityInfo.Name;
                switch (IdentityInfo.Gender)
                {
                    case Gender.Male:
                        TblBuyerGender.Text = "男";
                        break;
                    case Gender.Female:
                        TblBuyerGender.Text = "女";
                        break;
                    default:
                        TblBuyerGender.Text = "";
                        break;
                }
                TblBuyerNation.Text = IdentityInfo.Nation.ToString();
                if (IdentityInfo.Birthday != null)
                    TblBuyerBirthday.Text = ((DateTime)IdentityInfo.Birthday).ToString("yyyy.MM.dd");
                TblBuyerAddress.Text = IdentityInfo.Address;
                TblBuyerIdentityNo.Text = IdentityInfo.IdentityNo;
                TblBuyerVisaAgency.Text = IdentityInfo.VisaAgency;
                if (IdentityInfo.EffectiveDate != null)
                    TblBuyerEffectiveDate.Text = ((DateTime)IdentityInfo.EffectiveDate).ToString("yyyy.MM.dd");
                TblStr.Text = "至";
                if (IdentityInfo.ExpiryDate != null)
                {
                    TblBuyerExpiryDate.Text = ((DateTime)IdentityInfo.ExpiryDate).ToString("yyyy.MM.dd");
                }
                else
                {
                    TblBuyerExpiryDate.Text = "长期";
                }
                //照片显示
                try
                {
                    BitmapImage bitmap = new BitmapImage();
                    MemoryStream ms1 = new MemoryStream(IdentityInfo.Photo);

                    bitmap.BeginInit();
                    bitmap.StreamSource = ms1;
                    bitmap.EndInit();

                    ImgBuyerPhoto.Source = bitmap;
                }
                catch (Exception)
                {
                    //throw new ArgumentException("照片读取失败");
                    MessageBox.Show("照片读取失败!", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
            }

        }

        /// <summary>
        /// 读取卖家信息
        /// </summary>
        private void GetSellerValue()
        {
            if (IdentityInfo.IdentityNo == null)
                return;
            if (!IdentityInfo.IsTrusted)
            {
                TblSellerName.Text = IdentityInfo.Name;
                TblSellerIdentityNo.Text = IdentityInfo.IdentityNo;
            }
            else
            {
                TblSellerName.Text = IdentityInfo.Name;
                switch (IdentityInfo.Gender)
                {
                    case Gender.Male:
                        TblSellerGender.Text = "男";
                        break;
                    case Gender.Female:
                        TblSellerGender.Text = "女";
                        break;
                    default:
                        TblSellerGender.Text = "";
                        break;
                }
                TblSellerNation.Text = IdentityInfo.Nation.ToString();
                if (IdentityInfo.Birthday != null)
                    TblSellerBirthday.Text = ((DateTime)IdentityInfo.Birthday).ToString("yyyy.MM.dd");
                TblSellerAddress.Text = IdentityInfo.Address;
                TblSellerIdentityNo.Text = IdentityInfo.IdentityNo;
                TblSellerVisaAgency.Text = IdentityInfo.VisaAgency;
                if (IdentityInfo.EffectiveDate != null)
                    TblSellerEffectiveDate.Text = ((DateTime)IdentityInfo.EffectiveDate).ToString("yyyy.MM.dd");
                TblStr.Text = "至";
                if (IdentityInfo.ExpiryDate != null)
                {
                    TblSellerExpiryDate.Text = ((DateTime)IdentityInfo.ExpiryDate).ToString("yyyy.MM.dd");
                }
                else
                {
                    TblSellerExpiryDate.Text = "长期";
                }
                //照片显示
                try
                {
                    BitmapImage bitmap = new BitmapImage();
                    MemoryStream ms1 = new MemoryStream(IdentityInfo.Photo);

                    bitmap.BeginInit();
                    bitmap.StreamSource = ms1;
                    bitmap.EndInit();

                    ImgSellerPhoto.Source = bitmap;
                }
                catch (Exception)
                {
                    throw new ArgumentException("照片读取失败");
                }
            }
        }

        private void GetAgentValue()
        {
            int totalCount = 0;
            var idNo = IdentityInfo.IdentityNo;

            IList<AgentStaff> agentStaffList = new List<AgentStaff>();
            if (idNo == null)
            {
                return;
            }
            agentStaffList = AgentServiceProxy.GetAgentStaffs(1, 10000, out totalCount, null, null, null, idNo);
            if (agentStaffList.Count > 0)
            {
                for (int i = 0; i < agentStaffList.Count; i++)
                {
                    AgentCompany agentCompany = AgentServiceProxy.GetAgentCompany(agentStaffList[i].AgentCompanySysNo);
                    if (agentCompany != null)
                    {
                        TblShowInfo.Text = "姓名：" + agentStaffList[i].RealName + "                 所属门店：" +
                                           agentCompany.CompanyName;
                    }
                }
            }
            else
            {
                MessageBox.Show("没有符合该身份证号的经纪人！");
                return;
            }

        }

        #endregion

        /// <summary>
        /// 搜索案件事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [Permission("10003")]
        private void BtnSearch_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, "Main/PreCheck/PreCheckSearchListPage.xaml");
        }

        /// <summary>
        /// 撤销买方证件信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnBuyerRevoke_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CboBuyerIdentity.SelectedValue = (int)CertificateType.IdCard;

                CboBuyerIdentity.IsEnabled = true;
                CboBuyerIdentity.Visibility = Visibility.Visible;
                BtnBuyerRead.Visibility = Visibility.Visible;
                BtnBuyerRevoke.Visibility = Visibility.Collapsed;

                //Grid_BuyerPassport.Visibility = Visibility.Collapsed;
                //Grid_BuyerIdCard.Visibility = Visibility.Visible;

                //New
                BuyerIDTypeCollapsed();
                IdCardBuyer.Visibility = Visibility.Visible;

                LbBuyerList.SelectedItem = null;
                //ClearBuyerIdCardData();
                //ClearBuyerPassportData();

                //New
                //ClearIdCard(IdCardBuyer);
                //ClearPassport(PassportBuyer);
                //ClearHKMacaoIdCard(HKMacaoIdCardBuyer);
                //ClearBirthCertificate(BirthCertificateBuyer);
                //ClearMacaoPermit(MacaoPermitBuyer);
                //ClearCertificateOfOfficers(CertificateOfOfficersBuyer);
                //ClearResidenceBooklet(ResidenceBookletBuyer);
                ClearBuyerIDType();
            }
            catch (Exception ex)
            {
                MessageBox.Show("撤销发生系统异常：" + ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 撤销卖家证件信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSellerRevoke_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CboSellerIdentity.IsEnabled = true;

                CboSellerIdentity.SelectedValue = (int)CertificateType.IdCard;

                CboSellerIdentity.Visibility = Visibility.Visible;
                BtnSellerRead.Visibility = Visibility.Visible;
                BtnSellerRevoke.Visibility = Visibility.Collapsed;

                //Grid_SellerPassport.Visibility = Visibility.Collapsed;
                //Grid_SellerIdCard.Visibility = Visibility.Visible;

                //New
                SellerIDTypeCollapsed();
                IdCardSeller.Visibility = Visibility.Visible;

                LbSellerList.SelectedItem = null;
                //ClearSellerIdCardData();
                //ClearSellerPassportData();

                //New
                //ClearIdCard(IdCardSeller);
                //ClearPassport(PassportSeller);
                //ClearHKMacaoIdCard(HKMacaoIdCardSeller);
                //ClearBirthCertificate(BirthCertificateSeller);
                //ClearMacaoPermit(MacaoPermitSeller);
                //ClearCertificateOfOfficers(CertificateOfOfficersSeller);
                //ClearResidenceBooklet(ResidenceBookletSeller);
                ClearSellerIDType();
            }
            catch (Exception ex)
            {
                MessageBox.Show("撤销发生系统异常：" + ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #region 列表改变事件

        /// <summary>
        /// 身份证
        /// </summary>
        /// <param name="idCard"></param>
        /// <param name="userIdentity"></param>
        private void ChangeIdCard(IdCardControl idCard, UserIdentityModel userIdentity)
        {
            #region 身份证
            idCard.TblName.Text = userIdentity.Name; //姓名
            //性别
            if (userIdentity.Gender != null)
            {
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        idCard.TblGender.Text = "男";
                        break;
                    case Gender.Female:
                        idCard.TblGender.Text = "女";
                        break;
                    default:
                        idCard.TblGender.Text = "";
                        break;
                }
            }
            else
            {
                idCard.TblGender.Text = "";
            }
            //民族
            if (userIdentity.Nation != null)
            {
                idCard.TblNation.Text = userIdentity.Nation.ToString();
            }
            //出生
            if (userIdentity.Birthday != null)
                idCard.TblBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
            //地址
            if (!string.IsNullOrEmpty(userIdentity.Address))
            {
                idCard.TblAddress.Text = userIdentity.Address;
            }
            //身份证号
            if (!string.IsNullOrEmpty(userIdentity.IdentityNo))
            {
                idCard.TblIdentityNo.Text = userIdentity.IdentityNo;
                idCard.TblIdentityNo.Tag = userIdentity.Tag;
            }
            //签发机关
            if (!string.IsNullOrEmpty(userIdentity.VisaAgency))
            {
                idCard.TblVisaAgency.Text = userIdentity.VisaAgency;
            }
            //有效期限-生效
            if (userIdentity.EffectiveDate != null)
            {
                idCard.TblEffectiveDate.Text = ((DateTime)userIdentity.EffectiveDate).ToString("yyyy.MM.dd");
                idCard.TblStr.Text = "至";
            }
            //有效期限-失效
            if (userIdentity.ExpiryDate != null)
            {
                idCard.TblExpiryDate.Text = ((DateTime)userIdentity.ExpiryDate).ToString("yyyy.MM.dd");
                if (userIdentity.ExpiryDate == DateTime.MaxValue)
                {
                    idCard.TblExpiryDate.Text = "长期";
                }
            }
            else
            {
                idCard.TblExpiryDate.Text = "";
            }

            //照片显示
            try
            {

                if (userIdentity.Photo != null && userIdentity.Photo.Length != 0)
                {
                    BitmapImage bitmap = new BitmapImage();
                    MemoryStream ms1 = new MemoryStream(userIdentity.Photo);

                    bitmap.BeginInit();
                    bitmap.StreamSource = ms1;
                    bitmap.EndInit();

                    idCard.ImgPhoto.Source = bitmap;
                }
            }
            catch (Exception)
            {
                //throw new ArgumentException("照片读取失败");
                //MessageBox.Show("照片读取失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            #endregion
        }

        /// <summary>
        /// 护照
        /// </summary>
        /// <param name="passport"></param>
        /// <param name="userIdentity"></param>
        private void ChangePassport(PassportControl passport, UserIdentityModel userIdentity)
        {
            #region 护照
            passport.TbName.Text = userIdentity.Name; //姓名
            //性别
            switch (userIdentity.Gender)
            {
                case Gender.Male:
                    passport.RdobtnMale.IsChecked = true;
                    break;
                case Gender.Female:
                    passport.RdobtnFemale.IsChecked = true;
                    break;
            }
            //出生
            if (userIdentity.Birthday != null)
                passport.DateBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
            //国家
            passport.CboNationality.Text = userIdentity.Nationality;
            //身份证号
            if (!string.IsNullOrEmpty(userIdentity.IdentityNo))
            {
                passport.TbIdentityNo.Text = userIdentity.IdentityNo;
                passport.TbIdentityNo.Tag = userIdentity.Tag;//记录sysNo
            }
            #endregion
        }

        /// <summary>
        /// 港澳居民身份证
        /// </summary>
        /// <param name="hKMacaoIdCard"></param>
        /// <param name="userIdentity"></param>
        private void ChangeHKMacaoIdCard(HKMacaoIdCardControl hKMacaoIdCard, UserIdentityModel userIdentity)
        {
            #region 港澳居民身份证
            hKMacaoIdCard.TbName.Text = userIdentity.Name; //姓名
            //性别
            if (userIdentity.Gender != null)
            {
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        hKMacaoIdCard.RdobtnMale.IsChecked = true;
                        break;
                    case Gender.Female:
                        hKMacaoIdCard.RdobtnFemale.IsChecked = true;
                        break;
                }
            }
            else
            {
                hKMacaoIdCard.RdobtnMale.IsChecked = false;
                hKMacaoIdCard.RdobtnFemale.IsChecked = false;
            }
            //出生
            if (userIdentity.Birthday != null)
                hKMacaoIdCard.DateBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
            //身份证号
            if (!string.IsNullOrEmpty(userIdentity.IdentityNo))
            {
                hKMacaoIdCard.TbIdentityNo.Text = userIdentity.IdentityNo;
                hKMacaoIdCard.TbIdentityNo.Tag = userIdentity.Tag;
            }
            //签发日期
            if (userIdentity.EffectiveDate != null)
            {
                hKMacaoIdCard.DateVisaAgency.Text = ((DateTime)userIdentity.EffectiveDate).ToString("yyyy.MM.dd");
            }
            #endregion
        }

        /// <summary>
        /// 出生
        /// </summary>
        /// <param name="birthCertificate"></param>
        /// <param name="userIdentity"></param>
        private void ChangeBirthCertificate(BirthCertificateControl birthCertificate, UserIdentityModel userIdentity)
        {
            #region 出生证
            birthCertificate.TbName.Text = userIdentity.Name; //姓名
            //性别
            if (userIdentity.Gender != null)
            {
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        birthCertificate.RdobtnMale.IsChecked = true;
                        break;
                    case Gender.Female:
                        birthCertificate.RdobtnFemale.IsChecked = true;
                        break;
                }
            }
            else
            {
                birthCertificate.RdobtnMale.IsChecked = false;
                birthCertificate.RdobtnFemale.IsChecked = false;
            }
            //出生
            if (userIdentity.Birthday != null)
                birthCertificate.DateBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
            //身份证号
            if (!string.IsNullOrEmpty(userIdentity.IdentityNo))
            {
                birthCertificate.TbIdentityNo.Text = userIdentity.IdentityNo;
                birthCertificate.TbIdentityNo.Tag = userIdentity.Tag;
            }
            //接生机构
            if (userIdentity.VisaAgency != null)
            {
                birthCertificate.TbVisaAgency.Text = userIdentity.VisaAgency;
            }
            #endregion
        }

        /// <summary>
        /// 港澳居民往来大陆通行证
        /// </summary>
        /// <param name="macaoPermit"></param>
        /// <param name="userIdentity"></param>
        private void ChangeMacaoPermit(MacaoPermitControl macaoPermit, UserIdentityModel userIdentity)
        {
            #region 港澳居民往来大陆通行证
            macaoPermit.TbName.Text = userIdentity.Name; //姓名
            //性别
            if (userIdentity.Gender != null)
            {
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        macaoPermit.RdobtnMale.IsChecked = true;
                        break;
                    case Gender.Female:
                        macaoPermit.RdobtnFemale.IsChecked = true;
                        break;
                }
            }
            else
            {
                macaoPermit.RdobtnMale.IsChecked = false;
                macaoPermit.RdobtnFemale.IsChecked = false;
            }
            //出生
            if (userIdentity.Birthday != null)
                macaoPermit.DateBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
            //证件号码
            if (!string.IsNullOrEmpty(userIdentity.IdentityNo))
            {
                macaoPermit.TbIdentityNo.Text = userIdentity.IdentityNo;
                macaoPermit.TbIdentityNo.Tag = userIdentity.Tag;
            }
            //签发机构
            if (!string.IsNullOrEmpty(userIdentity.VisaAgency))
            {
                macaoPermit.TbVisaAgency.Text = userIdentity.VisaAgency;
            }
            //有效期限-生效
            if (userIdentity.EffectiveDate != null)
            {
                macaoPermit.TbEffectiveDate.Text = ((DateTime)userIdentity.EffectiveDate).Year.ToString();
            }
            //有效期限-失效
            if (userIdentity.ExpiryDate != null)
            {
                macaoPermit.TbExpiryDate.Text = ((DateTime)userIdentity.ExpiryDate).Year.ToString();
            }
            #endregion
        }

        /// <summary>
        /// 台湾居民往来大陆通行证
        /// </summary>
        /// <param name="macaoPermit"></param>
        /// <param name="userIdentity"></param>
        private void ChangeTaiwanPermit(TaiwanPermitControl taiwanPermit, UserIdentityModel userIdentity)
        {
            #region 台湾居民往来大陆通行证
            taiwanPermit.TbName.Text = userIdentity.Name; //姓名
            //性别
            if (userIdentity.Gender != null)
            {
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        taiwanPermit.RdobtnMale.IsChecked = true;
                        break;
                    case Gender.Female:
                        taiwanPermit.RdobtnFemale.IsChecked = true;
                        break;
                }
            }
            else
            {
                taiwanPermit.RdobtnMale.IsChecked = false;
                taiwanPermit.RdobtnFemale.IsChecked = false;
            }
            //出生
            if (userIdentity.Birthday != null)
                taiwanPermit.DateBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
            //证件号码
            if (!string.IsNullOrEmpty(userIdentity.IdentityNo))
            {
                taiwanPermit.TbIdentityNo.Text = userIdentity.IdentityNo;
                taiwanPermit.TbIdentityNo.Tag = userIdentity.Tag;
            }
            //签发机构
            if (!string.IsNullOrEmpty(userIdentity.VisaAgency))
            {
                taiwanPermit.TbVisaAgency.Text = userIdentity.VisaAgency;
            }
            //有效期限-生效
            if (userIdentity.EffectiveDate != null)
            {
                taiwanPermit.TbEffectiveDate.Text = ((DateTime)userIdentity.EffectiveDate).Year.ToString();
            }
            //有效期限-失效
            if (userIdentity.ExpiryDate != null)
            {
                taiwanPermit.TbExpiryDate.Text = ((DateTime)userIdentity.ExpiryDate).Year.ToString();
            }
            #endregion
        }

        /// <summary>
        /// 军官证
        /// </summary>
        /// <param name="certificateOfOfficers"></param>
        /// <param name="userIdentity"></param>
        private void ChangeCertificateOfOfficers(CertificateOfOfficersControl certificateOfOfficers, UserIdentityModel userIdentity)
        {
            #region 军官证
            certificateOfOfficers.TbName.Text = userIdentity.Name; //姓名
            //性别
            if (userIdentity.Gender != null)
            {
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        certificateOfOfficers.RdobtnMale.IsChecked = true;
                        break;
                    case Gender.Female:
                        certificateOfOfficers.RdobtnFemale.IsChecked = true;
                        break;
                }
            }
            else
            {
                certificateOfOfficers.RdobtnMale.IsChecked = false;
                certificateOfOfficers.RdobtnFemale.IsChecked = false;
            }
            //出生
            if (userIdentity.Birthday != null)
                certificateOfOfficers.DateBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
            //民族
            if (userIdentity.Nation != null)
            {
                certificateOfOfficers.TbNation.Text = userIdentity.Nation.ToString();
            }
            //部别
            if (!string.IsNullOrEmpty(userIdentity.Address))
            {
                certificateOfOfficers.TbDepartmental.Text = userIdentity.Address;
            }
            //证件号码
            if (!string.IsNullOrEmpty(userIdentity.IdentityNo))
            {
                certificateOfOfficers.TbIdentityNo.Text = userIdentity.IdentityNo;
                certificateOfOfficers.TbIdentityNo.Tag = userIdentity.Tag;
            }
            //发证机关
            if (!string.IsNullOrEmpty(userIdentity.VisaAgency))
            {
                certificateOfOfficers.TbVisaAgency.Text = userIdentity.VisaAgency;
            }
            //有效期限-生效
            if (userIdentity.EffectiveDate != null)
            {
                certificateOfOfficers.TbEffectiveDate.Text = ((DateTime)userIdentity.EffectiveDate).Year.ToString();
            }
            //有效期限-失效
            if (userIdentity.ExpiryDate != null)
            {
                certificateOfOfficers.TbExpiryDate.Text = ((DateTime)userIdentity.ExpiryDate).Year.ToString();
            }
            #endregion
        }

        /// <summary>
        /// 户口簿
        /// </summary>
        /// <param name="residenceBooklet"></param>
        /// <param name="userIdentity"></param>
        private void ChangeResidenceBooklet(ResidenceBookletControl residenceBooklet, UserIdentityModel userIdentity)
        {
            #region 户口簿
            residenceBooklet.TbName.Text = userIdentity.Name; //姓名
            //性别
            if (userIdentity.Gender != null)
            {
                switch (userIdentity.Gender)
                {
                    case Gender.Male:
                        residenceBooklet.RdobtnMale.IsChecked = true;
                        break;
                    case Gender.Female:
                        residenceBooklet.RdobtnFemale.IsChecked = true;
                        break;
                }
            }
            else
            {
                residenceBooklet.RdobtnMale.IsChecked = false;
                residenceBooklet.RdobtnFemale.IsChecked = false;
            }
            //出生
            if (userIdentity.Birthday != null)
                residenceBooklet.DateBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
            //民族
            if (userIdentity.Nation != null)
            {
                residenceBooklet.TbNation.Text = userIdentity.Nation.ToString();
            }
            //身份证号
            if (!string.IsNullOrEmpty(userIdentity.IdentityNo))
            {
                residenceBooklet.TbIdentityNo.Text = userIdentity.IdentityNo;
                residenceBooklet.TbIdentityNo.Tag = userIdentity.Tag;
            }
            //住址
            if (!string.IsNullOrEmpty(userIdentity.Address))
            {
                residenceBooklet.TbAddress.Text = userIdentity.Address;
            }
            #endregion
        }

        #endregion

        /// <summary>
        /// 买方列表切换事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LbBuyerList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (LbBuyerList.SelectedItem == null)
                {
                    return;
                }
                var userIdentity = (UserIdentityModel)LbBuyerList.SelectedItem;
                if (userIdentity != null)
                {
                    switch (userIdentity.CertificateType)
                    {
                        #region 身份证
                        case CertificateType.IdCard:

                            CboBuyerIdentity.SelectionChanged += CboBuyerIdentity_SelectionChanged;
                            CboBuyerIdentity.SelectedValue = (int)CertificateType.IdCard;
                            CboBuyerIdentity.IsEnabled = false;

                            ChangeIdCard(IdCardBuyer, userIdentity);

                            BuyerIDTypeCollapsed();
                            IdCardBuyer.Visibility = Visibility.Visible;

                            //身份证不需要撤销按钮
                            //IdCardOperate(CboBuyerIdentity, BtnBuyerRead, BtnBuyerRevoke);
                            break;

                        #endregion

                        #region 护照
                        case CertificateType.Passport:
                            CboBuyerIdentity.SelectionChanged += CboBuyerIdentity_SelectionChanged;
                            CboBuyerIdentity.SelectedValue = (int)CertificateType.Passport;
                            CboBuyerIdentity.IsEnabled = false;

                            ChangePassport(PassportBuyer, userIdentity);

                            BuyerIDTypeCollapsed();
                            PassportBuyer.Visibility = Visibility.Visible;

                            //护照只需要撤销按钮
                            //OtherTypeOperate(CboBuyerIdentity, BtnBuyerRead, BtnBuyerRevoke);
                            break;
                        #endregion

                        #region 港澳居民身份证
                        case CertificateType.HKMacaoIdCard:
                            CboBuyerIdentity.SelectionChanged += CboBuyerIdentity_SelectionChanged;
                            CboBuyerIdentity.SelectedValue = (int)CertificateType.HKMacaoIdCard;
                            CboBuyerIdentity.IsEnabled = false;

                            ChangeHKMacaoIdCard(HKMacaoIdCardBuyer, userIdentity);

                            BuyerIDTypeCollapsed();
                            HKMacaoIdCardBuyer.Visibility = Visibility.Visible;

                            //港澳居民身份证只需要撤销按钮
                            //OtherTypeOperate(CboBuyerIdentity, BtnBuyerRead, BtnBuyerRevoke);
                            break;
                        #endregion

                        #region 出生证
                        case CertificateType.BirthCertificate:
                            CboBuyerIdentity.SelectionChanged += CboBuyerIdentity_SelectionChanged;
                            CboBuyerIdentity.SelectedValue = (int)CertificateType.BirthCertificate;
                            CboBuyerIdentity.IsEnabled = false;

                            ChangeBirthCertificate(BirthCertificateBuyer, userIdentity);

                            BuyerIDTypeCollapsed();
                            BirthCertificateBuyer.Visibility = Visibility.Visible;

                            //出生证只需要撤销按钮
                            //OtherTypeOperate(CboBuyerIdentity, BtnBuyerRead, BtnBuyerRevoke);
                            break;
                        #endregion

                        #region 港澳居民往来大陆通行证
                        case CertificateType.MacaoPermit:
                            CboBuyerIdentity.SelectionChanged += CboBuyerIdentity_SelectionChanged;
                            CboBuyerIdentity.SelectedValue = (int)CertificateType.MacaoPermit;
                            CboBuyerIdentity.IsEnabled = false;

                            ChangeMacaoPermit(MacaoPermitBuyer, userIdentity);

                            BuyerIDTypeCollapsed();
                            MacaoPermitBuyer.Visibility = Visibility.Visible;

                            //港澳通行证只需要撤销按钮
                            //OtherTypeOperate(CboBuyerIdentity, BtnBuyerRead, BtnBuyerRevoke);
                            break;
                        #endregion

                        #region 台湾居民往来大陆通行证
                        case CertificateType.TaiwanPermit:
                            CboBuyerIdentity.SelectionChanged += CboBuyerIdentity_SelectionChanged;
                            CboBuyerIdentity.SelectedValue = (int)CertificateType.TaiwanPermit;
                            CboBuyerIdentity.IsEnabled = false;

                            ChangeTaiwanPermit(TaiwanPermitBuyer, userIdentity);

                            BuyerIDTypeCollapsed();
                            TaiwanPermitBuyer.Visibility = Visibility.Visible;

                            //港澳通行证只需要撤销按钮
                            //OtherTypeOperate(CboBuyerIdentity, BtnBuyerRead, BtnBuyerRevoke);
                            break;
                        #endregion

                        #region 军官证
                        case CertificateType.CertificateOfOfficers:
                            CboBuyerIdentity.SelectionChanged += CboBuyerIdentity_SelectionChanged;
                            CboBuyerIdentity.SelectedValue = (int)CertificateType.CertificateOfOfficers;
                            CboBuyerIdentity.IsEnabled = false;

                            ChangeCertificateOfOfficers(CertificateOfOfficersBuyer, userIdentity);

                            BuyerIDTypeCollapsed();
                            CertificateOfOfficersBuyer.Visibility = Visibility.Visible;

                            //军官证只需要撤销按钮
                            //OtherTypeOperate(CboBuyerIdentity, BtnBuyerRead, BtnBuyerRevoke);
                            break;
                        #endregion

                        #region 户口簿
                        case CertificateType.ResidenceBooklet:
                            CboBuyerIdentity.SelectionChanged += CboBuyerIdentity_SelectionChanged;
                            CboBuyerIdentity.SelectedValue = (int)CertificateType.ResidenceBooklet;
                            CboBuyerIdentity.IsEnabled = false;

                            ChangeResidenceBooklet(ResidenceBookletBuyer, userIdentity);

                            BuyerIDTypeCollapsed();
                            ResidenceBookletBuyer.Visibility = Visibility.Visible;

                            //户口簿只需要撤销按钮
                            //OtherTypeOperate(CboBuyerIdentity, BtnBuyerRead, BtnBuyerRevoke);
                            break;
                        #endregion
                    }

                    IdTypeOperateNew(CboBuyerIdentity, BtnBuyerRead, BtnBuyerRevoke);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("证件显示数据发生异常：" + ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void LbBuyerList_SelectionChangedOld(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (LbBuyerList.SelectedItem == null)
                {
                    return;
                }
                var userIdentity = (UserIdentityModel)LbBuyerList.SelectedItem;
                if (userIdentity != null)
                {
                    if (userIdentity.CertificateType == CertificateType.IdCard)
                    {
                        CboBuyerIdentity.SelectionChanged += CboBuyerIdentity_SelectionChanged;
                        CboBuyerIdentity.SelectedValue = (int)CertificateType.IdCard;
                        CboBuyerIdentity.IsEnabled = false;
                        #region 身份证
                        TblBuyerName.Text = userIdentity.Name;
                        if (userIdentity.Gender != null)
                        {
                            switch (userIdentity.Gender)
                            {
                                case Gender.Male:
                                    TblBuyerGender.Text = "男";
                                    break;
                                case Gender.Female:
                                    TblBuyerGender.Text = "女";
                                    break;
                                default:
                                    TblBuyerGender.Text = "";
                                    break;
                            }
                        }
                        else
                        {
                            TblBuyerGender.Text = "";
                        }
                        if (userIdentity.Nation != null)
                        {
                            TblBuyerNation.Text = userIdentity.Nation.ToString();
                        }
                        if (userIdentity.Birthday != null)
                            TblBuyerBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
                        if (!string.IsNullOrEmpty(userIdentity.Address))
                        {
                            TblBuyerAddress.Text = userIdentity.Address;
                        }
                        if (!string.IsNullOrEmpty(userIdentity.IdentityNo))
                        {
                            TblBuyerIdentityNo.Text = userIdentity.IdentityNo;
                            TblBuyerIdentityNo.Tag = userIdentity.Tag;
                        }

                        if (!string.IsNullOrEmpty(userIdentity.VisaAgency))
                        {
                            TblBuyerVisaAgency.Text = userIdentity.VisaAgency;
                        }
                        if (userIdentity.EffectiveDate != null)
                        {
                            TblBuyerEffectiveDate.Text = ((DateTime)userIdentity.EffectiveDate).ToString("yyyy.MM.dd");
                            TblStr.Text = "至";
                        }

                        if (userIdentity.ExpiryDate != null)
                        {
                            TblBuyerExpiryDate.Text = ((DateTime)userIdentity.ExpiryDate).ToString("yyyy.MM.dd");
                            if (userIdentity.ExpiryDate == DateTime.MaxValue)
                            {
                                TblBuyerExpiryDate.Text = "长期";
                            }
                        }
                        else
                        {
                            TblBuyerExpiryDate.Text = "";
                        }

                        //照片显示
                        try
                        {

                            if (userIdentity.Photo != null && userIdentity.Photo.Length != 0)
                            {
                                BitmapImage bitmap = new BitmapImage();
                                MemoryStream ms1 = new MemoryStream(userIdentity.Photo);

                                bitmap.BeginInit();
                                bitmap.StreamSource = ms1;
                                bitmap.EndInit();

                                ImgBuyerPhoto.Source = bitmap;
                            }
                        }
                        catch (Exception)
                        {
                            //throw new ArgumentException("照片读取失败");
                            MessageBox.Show("照片读取失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return;
                        }
                        #endregion

                        //Grid_BuyerIdCard.Visibility = Visibility.Visible;
                        //Grid_BuyerPassport.Visibility = Visibility.Collapsed;
                        BuyerIDTypeCollapsed();
                        IdCardBuyer.Visibility = Visibility.Visible;

                        //身份证不需要撤销按钮
                        IdCardOperate(CboBuyerIdentity, BtnBuyerRead, BtnBuyerRevoke);
                    }
                    else
                    {
                        CboBuyerIdentity.SelectionChanged += CboBuyerIdentity_SelectionChanged;
                        CboBuyerIdentity.SelectedValue = (int)CertificateType.Passport;
                        CboBuyerIdentity.IsEnabled = false;

                        #region 护照
                        TbBuyerName.Text = userIdentity.Name;
                        switch (userIdentity.Gender)
                        {
                            case Gender.Male:
                                RdoBtnBuyerGenderMale.IsChecked = true;
                                break;
                            case Gender.Female:
                                RdoBtnBuyerGenderFemale.IsChecked = true;
                                break;
                        }
                        if (userIdentity.Birthday != null)
                            TbBuyerBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
                        CboBuyerNationality.Text = userIdentity.Nationality;
                        TbBuyerIdentityNo.Text = userIdentity.IdentityNo;
                        TbBuyerIdentityNo.Tag = userIdentity.Tag;//记录sysNo
                        #endregion

                        //Grid_BuyerPassport.Visibility = Visibility.Visible;
                        //Grid_BuyerIdCard.Visibility = Visibility.Collapsed;
                        BuyerIDTypeCollapsed();
                        PassportBuyer.Visibility = Visibility.Visible;

                        //护照只需要撤销按钮
                        PassportOperate(CboBuyerIdentity, BtnBuyerRead, BtnBuyerRevoke);
                    }
                }
                //CboBuyerIdentity.Visibility = Visibility.Collapsed;
                //BtnBuyerRead.Visibility = Visibility.Collapsed;
                //BtnBuyerRevoke.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                MessageBox.Show("证件显示数据发生异常：" + ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #region 身份证、护照等其他证件类型读取撤销切换
        
        /// <summary>
        /// 类型下拉框可见 读取按钮不可见 撤销按钮可见
        /// </summary>
        /// <param name="cboType"></param>
        /// <param name="btnRead"></param>
        /// <param name="btnRevoke"></param>
        private void IdTypeOperateNew(ComboBox cboType, Button btnRead, Button btnRevoke)
        {
            cboType.Visibility = Visibility.Visible;
            btnRead.Visibility = Visibility.Collapsed;
            btnRevoke.Visibility = Visibility.Visible;
        }


        private void IdCardOperate(ComboBox cboType, Button btnRead, Button btnRevoke)
        {
            cboType.Visibility = Visibility.Visible;
            btnRead.Visibility = Visibility.Visible;
            btnRevoke.Visibility = Visibility.Collapsed;
        }

        private void PassportOperate(ComboBox cboType, Button btnRead, Button btnRevoke)
        {
            cboType.Visibility = Visibility.Collapsed;
            btnRead.Visibility = Visibility.Collapsed;
            btnRevoke.Visibility = Visibility.Visible;
        }

        private void OtherTypeOperate(ComboBox cboType, Button btnRead, Button btnRevoke)
        {
            cboType.Visibility = Visibility.Collapsed;
            btnRead.Visibility = Visibility.Collapsed;
            btnRevoke.Visibility = Visibility.Visible;
        }

        #endregion

        /// <summary>
        /// 卖家列表切换事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LbSellerList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (LbSellerList.SelectedItem == null)
            {
                return;
            }
            UserIdentityModel userIdentity = (UserIdentityModel)LbSellerList.SelectedItem;
            if (userIdentity != null)
            {
                switch (userIdentity.CertificateType)
                {
                    #region 身份证
                    case CertificateType.IdCard:

                        CboSellerIdentity.SelectionChanged += CboSellerIdentity_SelectionChanged;
                        CboSellerIdentity.SelectedValue = (int)CertificateType.IdCard;
                        CboSellerIdentity.IsEnabled = false;

                        ChangeIdCard(IdCardSeller, userIdentity);

                        SellerIDTypeCollapsed();
                        IdCardSeller.Visibility = Visibility.Visible;

                        //身份证不需要撤销按钮
                        //IdCardOperate(CboSellerIdentity, BtnSellerRead, BtnSellerRevoke);
                        break;

                    #endregion

                    #region 护照
                    case CertificateType.Passport:
                        CboSellerIdentity.SelectionChanged += CboSellerIdentity_SelectionChanged;
                        CboSellerIdentity.SelectedValue = (int)CertificateType.Passport;
                        CboSellerIdentity.IsEnabled = false;

                        ChangePassport(PassportSeller, userIdentity);

                        SellerIDTypeCollapsed();
                        PassportSeller.Visibility = Visibility.Visible;

                        //护照只需要撤销按钮
                        //OtherTypeOperate(CboSellerIdentity, BtnSellerRead, BtnSellerRevoke);
                        break;
                    #endregion

                    #region 港澳居民身份证
                    case CertificateType.HKMacaoIdCard:
                        CboSellerIdentity.SelectionChanged += CboSellerIdentity_SelectionChanged;
                        CboSellerIdentity.SelectedValue = (int)CertificateType.HKMacaoIdCard;
                        CboSellerIdentity.IsEnabled = false;

                        ChangeHKMacaoIdCard(HKMacaoIdCardSeller, userIdentity);

                        SellerIDTypeCollapsed();
                        HKMacaoIdCardSeller.Visibility = Visibility.Visible;

                        //港澳居民身份证只需要撤销按钮
                        //OtherTypeOperate(CboSellerIdentity, BtnSellerRead, BtnSellerRevoke);
                        break;
                    #endregion

                    #region 出生证
                    case CertificateType.BirthCertificate:
                        CboSellerIdentity.SelectionChanged += CboSellerIdentity_SelectionChanged;
                        CboSellerIdentity.SelectedValue = (int)CertificateType.BirthCertificate;
                        CboSellerIdentity.IsEnabled = false;

                        ChangeBirthCertificate(BirthCertificateSeller, userIdentity);

                        SellerIDTypeCollapsed();
                        BirthCertificateSeller.Visibility = Visibility.Visible;

                        //出生证只需要撤销按钮
                        //OtherTypeOperate(CboSellerIdentity, BtnSellerRead, BtnSellerRevoke);
                        break;
                    #endregion

                    #region 港澳居民往来大陆通行证
                    case CertificateType.MacaoPermit:
                        CboSellerIdentity.SelectionChanged += CboSellerIdentity_SelectionChanged;
                        CboSellerIdentity.SelectedValue = (int)CertificateType.MacaoPermit;
                        CboSellerIdentity.IsEnabled = false;

                        ChangeMacaoPermit(MacaoPermitSeller, userIdentity);

                        SellerIDTypeCollapsed();
                        MacaoPermitSeller.Visibility = Visibility.Visible;

                        //港澳通行证只需要撤销按钮
                        //OtherTypeOperate(CboSellerIdentity, BtnSellerRead, BtnSellerRevoke);
                        break;
                    #endregion

                    #region 台湾居民往来大陆通行证
                    case CertificateType.TaiwanPermit:
                        CboSellerIdentity.SelectionChanged += CboSellerIdentity_SelectionChanged;
                        CboSellerIdentity.SelectedValue = (int)CertificateType.TaiwanPermit;
                        CboSellerIdentity.IsEnabled = false;

                        ChangeTaiwanPermit(TaiwanPermitSeller, userIdentity);

                        SellerIDTypeCollapsed();
                        TaiwanPermitSeller.Visibility = Visibility.Visible;

                        //港澳通行证只需要撤销按钮
                        //OtherTypeOperate(CboSellerIdentity, BtnSellerRead, BtnSellerRevoke);
                        break;
                    #endregion

                    #region 军官证
                    case CertificateType.CertificateOfOfficers:
                        CboSellerIdentity.SelectionChanged += CboSellerIdentity_SelectionChanged;
                        CboSellerIdentity.SelectedValue = (int)CertificateType.CertificateOfOfficers;
                        CboSellerIdentity.IsEnabled = false;

                        ChangeCertificateOfOfficers(CertificateOfOfficersSeller, userIdentity);

                        SellerIDTypeCollapsed();
                        CertificateOfOfficersSeller.Visibility = Visibility.Visible;

                        //军官证只需要撤销按钮
                        //OtherTypeOperate(CboSellerIdentity, BtnSellerRead, BtnSellerRevoke);
                        break;
                    #endregion

                    #region 户口簿
                    case CertificateType.ResidenceBooklet:
                        CboSellerIdentity.SelectionChanged += CboSellerIdentity_SelectionChanged;
                        CboSellerIdentity.SelectedValue = (int)CertificateType.ResidenceBooklet;
                        CboSellerIdentity.IsEnabled = false;

                        ChangeResidenceBooklet(ResidenceBookletSeller, userIdentity);

                        SellerIDTypeCollapsed();
                        ResidenceBookletSeller.Visibility = Visibility.Visible;

                        //户口簿只需要撤销按钮
                        //OtherTypeOperate(CboSellerIdentity, BtnSellerRead, BtnSellerRevoke);
                        break;
                    #endregion
                }

                IdTypeOperateNew(CboSellerIdentity, BtnSellerRead, BtnSellerRevoke);
            }

        }

        private void LbSellerList_SelectionChangedOld(object sender, SelectionChangedEventArgs e)
        {
            if (LbSellerList.SelectedItem == null)
            {
                return;
            }
            UserIdentityModel userIdentity = (UserIdentityModel)LbSellerList.SelectedItem;
            if (userIdentity != null)
            {
                if (userIdentity.CertificateType == CertificateType.IdCard)
                {
                    CboSellerIdentity.SelectionChanged += CboSellerIdentity_SelectionChanged;
                    CboSellerIdentity.SelectedValue = (int)CertificateType.IdCard;
                    CboSellerIdentity.IsEnabled = false;

                    #region 身份证

                    TblSellerName.Text = userIdentity.Name;
                    if (userIdentity.Gender != null)
                    {
                        switch (userIdentity.Gender)
                        {
                            case Gender.Male:
                                TblSellerGender.Text = "男";
                                break;
                            case Gender.Female:
                                TblSellerGender.Text = "女";
                                break;
                            default:
                                TblSellerGender.Text = "";
                                break;
                        }
                    }
                    else
                    {
                        TblBuyerGender.Text = "";
                    }
                    if (userIdentity.Nation != null)
                    {
                        TblSellerNation.Text = userIdentity.Nation.ToString();
                    }
                    if (userIdentity.Birthday != null)
                        TblSellerBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
                    //TblSellerAddress.Text = userIdentity.Address;
                    //TblSellerIdentityNo.Text = userIdentity.IdentityNo;
                    //TblSellerVisaAgency.Text = userIdentity.VisaAgency;
                    //if (userIdentity.EffectiveDate != null)
                    //    TblSellerEffectiveDate.Text = ((DateTime) userIdentity.EffectiveDate).ToString("yyyy.MM.dd");
                    //TblStr.Text = "至";
                    //if (userIdentity.ExpiryDate != null)
                    //{
                    //    TblSellerExpiryDate.Text = ((DateTime) userIdentity.ExpiryDate).ToString("yyyy.MM.dd");
                    //}
                    //else
                    //{
                    //    TblSellerExpiryDate.Text = "长期";
                    //}
                    if (!string.IsNullOrEmpty(userIdentity.Address))
                    {
                        TblSellerAddress.Text = userIdentity.Address;
                    }
                    if (!string.IsNullOrEmpty(userIdentity.IdentityNo))
                    {
                        TblSellerIdentityNo.Text = userIdentity.IdentityNo;
                    }

                    if (!string.IsNullOrEmpty(userIdentity.VisaAgency))
                    {
                        TblSellerVisaAgency.Text = userIdentity.VisaAgency;
                    }
                    if (userIdentity.EffectiveDate != null)
                    {
                        TblSellerEffectiveDate.Text = ((DateTime)userIdentity.EffectiveDate).ToString("yyyy.MM.dd");
                        TblStr.Text = "至";
                    }

                    if (userIdentity.ExpiryDate != null)
                    {
                        TblSellerExpiryDate.Text = ((DateTime)userIdentity.ExpiryDate).ToString("yyyy.MM.dd");
                        if (userIdentity.ExpiryDate == DateTime.MaxValue)
                        {
                            TblSellerExpiryDate.Text = "长期";
                        }
                    }
                    else
                    {
                        TblSellerExpiryDate.Text = "";
                    }
                    //照片显示
                    try
                    {
                        if (userIdentity.Photo != null && userIdentity.Photo.Length != 0)
                        {
                            BitmapImage bitmap = new BitmapImage();
                            MemoryStream ms1 = new MemoryStream(userIdentity.Photo);

                            bitmap.BeginInit();
                            bitmap.StreamSource = ms1;
                            bitmap.EndInit();

                            ImgSellerPhoto.Source = bitmap;
                        }
                    }
                    catch (Exception)
                    {
                        //throw new ArgumentException("照片读取失败");
                        MessageBox.Show("照片读取失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }

                    #endregion

                    Grid_SellerIdCard.Visibility = Visibility.Visible;
                    Grid_SellerPassport.Visibility = Visibility.Collapsed;

                    //身份证不需要撤销按钮
                    IdCardOperate(CboSellerIdentity, BtnSellerRead, BtnSellerRevoke);
                }
                else
                {
                    CboSellerIdentity.SelectionChanged += CboSellerIdentity_SelectionChanged;
                    CboSellerIdentity.SelectedValue = (int)CertificateType.Passport;
                    CboSellerIdentity.IsEnabled = false;

                    #region 护照

                    TbSellerName.Text = userIdentity.Name;
                    switch (userIdentity.Gender)
                    {
                        case Gender.Male:
                            RdoBtnSellerGenderMale.IsChecked = true;
                            break;
                        case Gender.Female:
                            RdoBtnSellerGenderFemale.IsChecked = true;
                            break;
                    }
                    if (userIdentity.Birthday != null)
                        TbSellerBirthday.Text = ((DateTime)userIdentity.Birthday).ToString("yyyy.MM.dd");
                    //CboSellerNationality.SelectedValue = 0;
                    CboSellerNationality.Text = userIdentity.Nationality;
                    TbSellerIdentityNo.Text = userIdentity.IdentityNo;
                    TbSellerIdentityNo.Tag = userIdentity.Tag; //记录sysNo

                    #endregion

                    Grid_SellerPassport.Visibility = Visibility.Visible;
                    Grid_SellerIdCard.Visibility = Visibility.Collapsed;

                    //护照只需要撤销按钮
                    PassportOperate(CboSellerIdentity, BtnSellerRead, BtnSellerRevoke);
                }
            }
            //CboSellerIdentity.Visibility = Visibility.Collapsed;
            //BtnSellerRead.Visibility = Visibility.Collapsed;
            //BtnSellerRevoke.Visibility = Visibility.Visible;

        }

        /// <summary>
        /// 模糊查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnFuzzyQuery_Click(object sender, RoutedEventArgs e)
        {
            AgentQueryDialog agentQueryDialog = new AgentQueryDialog(_AgentStaff);
            agentQueryDialog.Closing += agentQueryDialog_Closing;
            agentQueryDialog.ShowDialog();
        }

        /// <summary>
        /// 获取经纪人信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void agentQueryDialog_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _AgentCompany = AgentServiceProxy.GetAgentCompany(_AgentStaff.AgentCompanySysNo);
            if (_AgentCompany != null)
            {
                TblShowInfo.Text = "姓名：" + _AgentStaff.RealName + "                 所属门店：" + _AgentCompany.CompanyName;
            }
        }

        #region 判空

        /// <summary>
        /// 护照判空
        /// </summary>
        /// <param name="passport"></param>
        /// <returns></returns>
        private bool JudgeEmptyPassport(PassportControl passport)
        {
            //姓名验证
            if (string.IsNullOrEmpty(passport.TbName.Text))
            {
                MessageBox.Show("姓名不能为空，无法进行保存操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            ////身份性别验证
            //if (passport.RdobtnMale.IsChecked == null ||
            //    passport.RdobtnFemale.IsChecked == null ||
            //    (passport.RdobtnFemale.IsChecked == false && passport.RdobtnMale.IsChecked == false))
            //{
            //    MessageBox.Show("请选择性别.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////出生日期验证
            //if (string.IsNullOrEmpty(passport.DateBirthday.Text))
            //{
            //    MessageBox.Show("请选择出生日期.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////国籍验证
            //if (string.IsNullOrEmpty(passport.CboNationality.Text))
            //{
            //    MessageBox.Show("请选择国籍.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            //证件号码验证
            if (string.IsNullOrEmpty(passport.TbIdentityNo.Text))
            {
                MessageBox.Show("请填写证件号码.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }

        /// <summary>
        /// 港澳居民身份证判空
        /// </summary>
        /// <param name="hKMacaoIdCard"></param>
        /// <returns></returns>
        private bool JudgeEmptyHKMacaoIdCard(HKMacaoIdCardControl hKMacaoIdCard)
        {
            //姓名验证
            if (string.IsNullOrEmpty(hKMacaoIdCard.TbName.Text))
            {
                MessageBox.Show("姓名不能为空，无法进行保存操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            ////身份性别验证
            //if (hKMacaoIdCard.RdobtnMale.IsChecked == null ||
            //    hKMacaoIdCard.RdobtnFemale.IsChecked == null ||
            //    (hKMacaoIdCard.RdobtnFemale.IsChecked == false && hKMacaoIdCard.RdobtnMale.IsChecked == false))
            //{
            //    MessageBox.Show("请选择性别.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////出生日期验证
            //if (string.IsNullOrEmpty(hKMacaoIdCard.DateBirthday.Text))
            //{
            //    MessageBox.Show("请选择出生日期.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            //证件号码验证
            if (string.IsNullOrEmpty(hKMacaoIdCard.TbIdentityNo.Text))
            {
                MessageBox.Show("请填写身份证号.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            ////签发日期验证
            //if (string.IsNullOrEmpty(hKMacaoIdCard.DateVisaAgency.Text))
            //{
            //    MessageBox.Show("请选择签发日期.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            return true;
        }

        /// <summary>
        /// 出生证判空
        /// </summary>
        /// <param name="birthCertificate"></param>
        /// <returns></returns>
        private bool JudgeEmptyBirthCertificate(BirthCertificateControl birthCertificate)
        {
            //姓名验证
            if (string.IsNullOrEmpty(birthCertificate.TbName.Text))
            {
                MessageBox.Show("姓名不能为空，无法进行保存操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            //身份性别验证
            //if (birthCertificate.RdobtnMale.IsChecked == null ||
            //    birthCertificate.RdobtnFemale.IsChecked == null ||
            //    (birthCertificate.RdobtnFemale.IsChecked == false && birthCertificate.RdobtnMale.IsChecked == false))
            //{
            //    MessageBox.Show("请选择性别.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////出生日期验证
            //if (string.IsNullOrEmpty(birthCertificate.DateBirthday.Text))
            //{
            //    MessageBox.Show("请选择出生日期.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            //出生证编号验证
            if (string.IsNullOrEmpty(birthCertificate.TbIdentityNo.Text))
            {
                MessageBox.Show("请填写出生证编号.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            ////接生机构验证
            //if (string.IsNullOrEmpty(birthCertificate.TbVisaAgency.Text))
            //{
            //    MessageBox.Show("请填写接生机构.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            return true;
        }

        /// <summary>
        /// 港澳居民往来大陆通行证判空
        /// </summary>
        /// <param name="macaoPermit"></param>
        /// <returns></returns>
        private bool JudgeEmptyMacaoPermit(MacaoPermitControl macaoPermit)
        {
            //姓名验证
            if (string.IsNullOrEmpty(macaoPermit.TbName.Text))
            {
                MessageBox.Show("姓名不能为空，无法进行保存操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            ////身份性别验证
            //if (macaoPermit.RdobtnMale.IsChecked == null ||
            //    macaoPermit.RdobtnFemale.IsChecked == null ||
            //    (macaoPermit.RdobtnFemale.IsChecked == false && macaoPermit.RdobtnMale.IsChecked == false))
            //{
            //    MessageBox.Show("请选择性别.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////出生日期验证
            //if (string.IsNullOrEmpty(macaoPermit.DateBirthday.Text))
            //{
            //    MessageBox.Show("请选择出生日期.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            //证件号码验证
            if (string.IsNullOrEmpty(macaoPermit.TbIdentityNo.Text))
            {
                MessageBox.Show("请填写证件号码.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            ////签发机构验证
            //if (string.IsNullOrEmpty(macaoPermit.TbVisaAgency.Text))
            //{
            //    MessageBox.Show("请填写签发机构.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////有效期限验证
            //if (string.IsNullOrEmpty(macaoPermit.TbEffectiveDate.Text) && string.IsNullOrEmpty(macaoPermit.TbExpiryDate.Text))
            //{
            //    MessageBox.Show("请填写有效期限.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}

            //有效期限验证
            if (macaoPermit.TbEffectiveDate.Text != "")
            {
                try
                {
                    if (int.Parse(macaoPermit.TbEffectiveDate.Text.Trim()) < 1900)
                    {
                        MessageBox.Show("有效期限输入不正确.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return false;
                    }
                    //if (int.Parse(macaoPermit.TbEffectiveDate.Text.Trim()) > DateTime.Now.Year)
                    //{
                    //    MessageBox.Show("有效期限输入不正确.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    //    return false;
                    //}
                }
                catch (Exception ex)
                {
                    MessageBox.Show("有效期限只能是数字.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
            }

            if (macaoPermit.TbExpiryDate.Text != "")
            {
                try
                {
                    if (int.Parse(macaoPermit.TbExpiryDate.Text.Trim()) < 1900)
                    {
                        MessageBox.Show("有效期限输入不正确.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return false;
                    }
                    //if (int.Parse(macaoPermit.TbExpiryDate.Text.Trim()) > DateTime.Now.Year)
                    //{
                    //    MessageBox.Show("有效期限输入不正确.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    //    return false;
                    //}
                }
                catch (Exception ex)
                {
                    MessageBox.Show("有效期限只能是数字.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
            }

            return true;
        }


        /// <summary>
        /// 台湾居民往来大陆通行证判空
        /// </summary>
        /// <param name="taiwanPermit"></param>
        /// <returns></returns>
        private bool JudgeEmptyTaiwanPermit(TaiwanPermitControl taiwanPermit)
        {
            //姓名验证
            if (string.IsNullOrEmpty(taiwanPermit.TbName.Text))
            {
                MessageBox.Show("姓名不能为空，无法进行保存操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            ////身份性别验证
            //if (taiwanPermit.RdobtnMale.IsChecked == null ||
            //    taiwanPermit.RdobtnFemale.IsChecked == null ||
            //    (taiwanPermit.RdobtnFemale.IsChecked == false && taiwanPermit.RdobtnMale.IsChecked == false))
            //{
            //    MessageBox.Show("请选择性别.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////出生日期验证
            //if (string.IsNullOrEmpty(taiwanPermit.DateBirthday.Text))
            //{
            //    MessageBox.Show("请选择出生日期.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            //证件号码验证
            if (string.IsNullOrEmpty(taiwanPermit.TbIdentityNo.Text))
            {
                MessageBox.Show("请填写证件号码.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            ////签发机构验证
            //if (string.IsNullOrEmpty(taiwanPermit.TbVisaAgency.Text))
            //{
            //    MessageBox.Show("请填写签发机构.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////有效期限验证
            //if (string.IsNullOrEmpty(taiwanPermit.TbEffectiveDate.Text) && string.IsNullOrEmpty(taiwanPermit.TbExpiryDate.Text))
            //{
            //    MessageBox.Show("请填写有效期限.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}

            //有效期限验证
            if (taiwanPermit.TbEffectiveDate.Text != "")
            {
                try
                {
                    if (int.Parse(taiwanPermit.TbEffectiveDate.Text.Trim()) < 1900)
                    {
                        MessageBox.Show("有效期限输入不正确.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return false;
                    }
                    //if (int.Parse(taiwanPermit.TbEffectiveDate.Text.Trim()) > DateTime.Now.Year)
                    //{
                    //    MessageBox.Show("有效期限输入不正确.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    //    return false;
                    //}
                }
                catch (Exception ex)
                {
                    MessageBox.Show("有效期限只能是数字.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
            }

            if (taiwanPermit.TbExpiryDate.Text != "")
            {
                try
                {
                    if (int.Parse(taiwanPermit.TbExpiryDate.Text.Trim()) < 1900)
                    {
                        MessageBox.Show("有效期限输入不正确.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return false;
                    }
                    //if (int.Parse(macaoPermit.TbExpiryDate.Text.Trim()) > DateTime.Now.Year)
                    //{
                    //    MessageBox.Show("有效期限输入不正确.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    //    return false;
                    //}
                }
                catch (Exception ex)
                {
                    MessageBox.Show("有效期限只能是数字.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 军官证判空
        /// </summary>
        /// <param name="certificateOfOfficers"></param>
        /// <returns></returns>
        private bool JudgeEmptyCertificateOfOfficers(CertificateOfOfficersControl certificateOfOfficers)
        {
            //姓名验证
            if (string.IsNullOrEmpty(certificateOfOfficers.TbName.Text))
            {
                MessageBox.Show("姓名不能为空，无法进行保存操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            ////身份性别验证
            //if (certificateOfOfficers.RdobtnMale.IsChecked == null ||
            //    certificateOfOfficers.RdobtnFemale.IsChecked == null ||
            //    (certificateOfOfficers.RdobtnFemale.IsChecked == false && certificateOfOfficers.RdobtnMale.IsChecked == false))
            //{
            //    MessageBox.Show("请选择性别.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////出生日期验证
            //if (string.IsNullOrEmpty(certificateOfOfficers.DateBirthday.Text))
            //{
            //    MessageBox.Show("请选择出生日期.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////民族验证
            //if (string.IsNullOrEmpty(certificateOfOfficers.TbNation.Text))
            //{
            //    MessageBox.Show("请填写民族.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////部别验证
            //if (string.IsNullOrEmpty(certificateOfOfficers.TbDepartmental.Text))
            //{
            //    MessageBox.Show("请填写部别.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            //身份证号验证
            if (string.IsNullOrEmpty(certificateOfOfficers.TbIdentityNo.Text))
            {
                MessageBox.Show("请填写身份证号.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (!ValidationCommon.IsIdMatch(certificateOfOfficers.TbIdentityNo.Text))
            {
                MessageBox.Show("身份证号不合法，请重新填写.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            ////发证机关验证
            //if (string.IsNullOrEmpty(certificateOfOfficers.TbVisaAgency.Text))
            //{
            //    MessageBox.Show("请填写发证机关.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////有效期限验证
            //if (string.IsNullOrEmpty(certificateOfOfficers.TbEffectiveDate.Text) && string.IsNullOrEmpty(certificateOfOfficers.TbExpiryDate.Text))
            //{
            //    MessageBox.Show("请填写有效期限.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}

            //有效期限验证
            if (certificateOfOfficers.TbEffectiveDate.Text != "")
            {
                try
                {
                    if (int.Parse(certificateOfOfficers.TbEffectiveDate.Text.Trim()) < 1900)
                    {
                        MessageBox.Show("有效期限输入不正确.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return false;
                    }
                    //if (int.Parse(certificateOfOfficers.TbEffectiveDate.Text.Trim()) > DateTime.Now.Year)
                    //{
                    //    MessageBox.Show("有效期限输入不正确.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    //    return false;
                    //}
                }
                catch (Exception ex)
                {
                    MessageBox.Show("有效期限只能是数字.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
            }

            if (certificateOfOfficers.TbExpiryDate.Text != "")
            {
                try
                {
                    if (int.Parse(certificateOfOfficers.TbExpiryDate.Text.Trim()) < 1900)
                    {
                        MessageBox.Show("有效期限输入不正确.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return false;
                    }
                    //if (int.Parse(certificateOfOfficers.TbExpiryDate.Text.Trim()) > DateTime.Now.Year)
                    //{
                    //    MessageBox.Show("有效期限输入不正确.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    //    return false;
                    //}
                }
                catch (Exception ex)
                {
                    MessageBox.Show("有效期限只能是数字.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 户口簿判空
        /// </summary>
        /// <param name="residenceBooklet"></param>
        /// <returns></returns>
        private bool JudgeEmptyResidenceBooklet(ResidenceBookletControl residenceBooklet)
        {
            //姓名验证
            if (string.IsNullOrEmpty(residenceBooklet.TbName.Text))
            {
                MessageBox.Show("姓名不能为空，无法进行保存操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            //身份性别验证
            //if (residenceBooklet.RdobtnMale.IsChecked == null ||
            //    residenceBooklet.RdobtnFemale.IsChecked == null ||
            //    (residenceBooklet.RdobtnFemale.IsChecked == false && residenceBooklet.RdobtnMale.IsChecked == false))
            //{
            //    MessageBox.Show("请选择性别.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////出生日期验证
            //if (string.IsNullOrEmpty(residenceBooklet.DateBirthday.Text))
            //{
            //    MessageBox.Show("请选择出生日期.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            //身份证号验证
            if (string.IsNullOrEmpty(residenceBooklet.TbIdentityNo.Text))
            {
                MessageBox.Show("请填写身份证号.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (!ValidationCommon.IsIdMatch(residenceBooklet.TbIdentityNo.Text))
            {
                MessageBox.Show("身份证号不合法，请重新填写.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            ////民族验证
            //if (string.IsNullOrEmpty(residenceBooklet.TbNation.Text))
            //{
            //    MessageBox.Show("请填写民族.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            ////住址验证
            //if (string.IsNullOrEmpty(residenceBooklet.TbAddress.Text))
            //{
            //    MessageBox.Show("请填写住址.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            return true;
        }

        #endregion

        private bool CheckBuyerDataNew(CertificateType certificateType)
        {
            try
            {
                switch (certificateType)
                {
                    case CertificateType.Passport:
                        return JudgeEmptyPassport(PassportBuyer);
                        break;
                    case CertificateType.HKMacaoIdCard:
                        return JudgeEmptyHKMacaoIdCard(HKMacaoIdCardBuyer);
                        break;
                    case CertificateType.BirthCertificate:
                        return JudgeEmptyBirthCertificate(BirthCertificateBuyer);
                        break;
                    case CertificateType.MacaoPermit:
                        return JudgeEmptyMacaoPermit(MacaoPermitBuyer);
                        break;
                    case CertificateType.TaiwanPermit:
                        return JudgeEmptyTaiwanPermit(TaiwanPermitBuyer);
                        break;
                    case CertificateType.CertificateOfOfficers:
                        return JudgeEmptyCertificateOfOfficers(CertificateOfOfficersBuyer);
                        break;
                    case CertificateType.ResidenceBooklet:
                        return JudgeEmptyResidenceBooklet(ResidenceBookletBuyer);
                        break;
                }
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show("数据合法性检查发生异常：" + e.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return false;
        }

        private bool CheckSellerDataNew(CertificateType certificateType)
        {
            try
            {
                switch (certificateType)
                {
                    case CertificateType.Passport:
                        return JudgeEmptyPassport(PassportSeller);
                        break;
                    case CertificateType.HKMacaoIdCard:
                        return JudgeEmptyHKMacaoIdCard(HKMacaoIdCardSeller);
                        break;
                    case CertificateType.BirthCertificate:
                        return JudgeEmptyBirthCertificate(BirthCertificateSeller);
                        break;
                    case CertificateType.MacaoPermit:
                        return JudgeEmptyMacaoPermit(MacaoPermitSeller);
                        break;
                    case CertificateType.TaiwanPermit:
                        return JudgeEmptyTaiwanPermit(TaiwanPermitSeller);
                        break;
                    case CertificateType.CertificateOfOfficers:
                        return JudgeEmptyCertificateOfOfficers(CertificateOfOfficersSeller);
                        break;
                    case CertificateType.ResidenceBooklet:
                        return JudgeEmptyResidenceBooklet(ResidenceBookletSeller);
                        break;
                }
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show("数据合法性检查发生异常：" + e.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return false;
        }

        /// <summary>
        /// 买方数据合法性检查
        /// </summary>
        /// <param name="certificateType">证件类型</param>
        /// <returns></returns>
        private bool CheckBuyerData(CertificateType certificateType)
        {
            try
            {
                switch (certificateType)
                {
                    //case CertificateType.IdCard:
                    //    if (string.IsNullOrEmpty(TblBuyerName.Text) ||
                    //        string.IsNullOrEmpty(TblBuyerGender.Text) ||
                    //        string.IsNullOrEmpty(TblBuyerBirthday.Text) ||
                    //        string.IsNullOrEmpty(TblBuyerNation.Text) ||
                    //        string.IsNullOrEmpty(TblBuyerAddress.Text) ||
                    //        string.IsNullOrEmpty(TblBuyerIdentityNo.Text) ||
                    //        string.IsNullOrEmpty(TblBuyerVisaAgency.Text) ||
                    //        string.IsNullOrEmpty(TblBuyerEffectiveDate.Text) ||
                    //        string.IsNullOrEmpty(TblBuyerExpiryDate.Text))
                    //    {
                    //        MessageBox.Show("身份证信息资料不合法，无法进行保存操作.","系统提示",MessageBoxButton.OK,MessageBoxImage.Warning);
                    //        return false;
                    //    }
                    //    break;
                    case CertificateType.Passport:
                        #region 作废
                        ////姓名验证
                        //if (string.IsNullOrEmpty(TbBuyerName.Text))
                        //{
                        //    MessageBox.Show("用户名不能为空，无法进行保存操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        //    return false;
                        //}
                        ////身份性别验证
                        //if (RdoBtnBuyerGenderMale.IsChecked == null ||
                        //    RdoBtnBuyerGenderFemale.IsChecked == null ||
                        //    (RdoBtnBuyerGenderFemale.IsChecked == false && RdoBtnBuyerGenderMale.IsChecked == false))
                        //{
                        //    MessageBox.Show("请选择性别.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        //    return false;
                        //}
                        ////出生日期验证
                        //if (string.IsNullOrEmpty(TbBuyerBirthday.Text))
                        //{
                        //    MessageBox.Show("请选择出生日期.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        //    return false;
                        //}
                        ////国籍验证
                        //if (string.IsNullOrEmpty(CboBuyerNationality.Text))
                        //{
                        //    MessageBox.Show("请选择国籍.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        //    return false;
                        //}
                        ////证件号码验证
                        //if (string.IsNullOrEmpty(TbBuyerIdentityNo.Text))
                        //{
                        //    MessageBox.Show("请填写证件编号.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        //    return false;
                        //}
                        #endregion

                        #region New
                        //姓名验证
                        if (string.IsNullOrEmpty(TbBuyerName.Text))
                        {
                            MessageBox.Show("用户名不能为空，无法进行保存操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return false;
                        }
                        //身份性别验证
                        if (RdoBtnBuyerGenderMale.IsChecked == null ||
                            RdoBtnBuyerGenderFemale.IsChecked == null ||
                            (RdoBtnBuyerGenderFemale.IsChecked == false && RdoBtnBuyerGenderMale.IsChecked == false))
                        {
                            MessageBox.Show("请选择性别.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return false;
                        }
                        //出生日期验证
                        if (string.IsNullOrEmpty(TbBuyerBirthday.Text))
                        {
                            MessageBox.Show("请选择出生日期.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return false;
                        }
                        //国籍验证
                        if (string.IsNullOrEmpty(CboBuyerNationality.Text))
                        {
                            MessageBox.Show("请选择国籍.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning); 
                            return false;
                        }
                        //证件号码验证
                        if (string.IsNullOrEmpty(TbBuyerIdentityNo.Text))
                        {
                            MessageBox.Show("请填写证件编号.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return false;
                        }
                        #endregion

                        break;
                }
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show("数据合法性检查发生异常：" + e.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return false;
        }

        /// <summary>
        /// 卖家数据合法性检查
        /// </summary>
        /// <param name="certificateType"></param>
        /// <returns></returns>
        private bool CheckSellerData(CertificateType certificateType)
        {
            try
            {
                switch (certificateType)
                {
                    //case CertificateType.IdCard:
                    //    if (string.IsNullOrEmpty(TblSellerName.Text) ||
                    //        string.IsNullOrEmpty(TblSellerGender.Text) ||
                    //        string.IsNullOrEmpty(TblSellerBirthday.Text) ||
                    //        string.IsNullOrEmpty(TblSellerNation.Text) ||
                    //        string.IsNullOrEmpty(TblSellerAddress.Text) ||
                    //        string.IsNullOrEmpty(TblSellerIdentityNo.Text) ||
                    //        string.IsNullOrEmpty(TblSellerVisaAgency.Text) ||
                    //        string.IsNullOrEmpty(TblSellerEffectiveDate.Text) ||
                    //        string.IsNullOrEmpty(TblSellerExpiryDate.Text))
                    //    {
                    //        MessageBox.Show("身份证信息资料不合法，无法进行保存操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    //        return false;
                    //    }
                    //    break;
                    case CertificateType.Passport:
                        //姓名验证
                        if (string.IsNullOrEmpty(TbSellerName.Text))
                        {
                            MessageBox.Show("用户名不能为空，无法进行保存操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return false;
                        }
                        //身份性别验证
                        if (RdoBtnSellerGenderMale.IsChecked == null ||
                            RdoBtnSellerGenderFemale.IsChecked == null ||
                            (RdoBtnSellerGenderFemale.IsChecked == false && RdoBtnSellerGenderMale.IsChecked == false))
                        {
                            MessageBox.Show("请选择性别.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return false;
                        }
                        //出生日期验证
                        if (string.IsNullOrEmpty(TbSellerBirthday.Text))
                        {
                            MessageBox.Show("请选择出生日期.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return false;
                        }
                        //国籍验证
                        if (string.IsNullOrEmpty(CboSellerNationality.Text))
                        {
                            MessageBox.Show("请选择国籍.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return false;
                        }
                        //证件号码验证
                        if (string.IsNullOrEmpty(TbSellerIdentityNo.Text))
                        {
                            MessageBox.Show("请填写证件编号.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return false;
                        }
                        break;
                }
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show("数据合法性检查发生异常：" + e.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return false;
        }

        /// <summary>
        /// 检查证据信息是否存在右边列表
        /// </summary>
        /// <param name="enumerable">数据源对象</param>
        /// <param name="newCardNo">新的证件号码</param>
        /// <returns></returns>
        private bool CheckIdCardNoInItemSource(IEnumerable enumerable, string newCardNo)
        {
            try
            {
                //无对象说明没有任何值
                if (enumerable == null) return true;
                foreach (var item in enumerable)
                {
                    var user = item as UserIdentityModel;
                    if (user == null) continue;
                    if (user.IdentityNo.Equals(newCardNo))
                    {
                        MessageBox.Show("当前证件信息已经存在.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        //ClearBuyerIdCardData();//相应数据清除
                        //ClearBuyerPassportData();//相应的数据清除

                        //New
                        //ClearIdCard(IdCardBuyer);
                        //ClearPassport(PassportBuyer);
                        //ClearHKMacaoIdCard(HKMacaoIdCardBuyer);
                        //ClearBirthCertificate(BirthCertificateBuyer);
                        //ClearMacaoPermit(MacaoPermitBuyer);
                        //ClearTaiwanPermit(TaiwanPermitBuyer);
                        //ClearCertificateOfOfficers(CertificateOfOfficersBuyer);
                        //ClearResidenceBooklet(ResidenceBookletBuyer);
                        ClearBuyerIDType();
                        return false;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show("检查证件号是否存在发生异常：" + e.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return false;
        }

        private bool CheckSellerIdCardNoInItemSource(IEnumerable enumerable, string newCardNo)
        {
            try
            {
                //无对象说明没有任何值
                if (enumerable == null) return true;
                foreach (var item in enumerable)
                {
                    var user = item as UserIdentityModel;
                    if (user == null) continue;
                    if (user.IdentityNo.Equals(newCardNo))
                    {
                        MessageBox.Show("当前证件信息已经存在.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        //ClearBuyerIdCardData();//相应数据清除
                        //ClearBuyerPassportData();//相应的数据清除

                        //New
                        //ClearIdCard(IdCardSeller);
                        //ClearPassport(PassportSeller);
                        //ClearHKMacaoIdCard(HKMacaoIdCardSeller);
                        //ClearBirthCertificate(BirthCertificateSeller);
                        //ClearMacaoPermit(MacaoPermitSeller);
                        //ClearCertificateOfOfficers(CertificateOfOfficersSeller);
                        //ClearResidenceBooklet(ResidenceBookletSeller);
                        ClearSellerIDType();
                        return false;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show("检查证件号是否存在发生异常：" + e.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return false;
        }

        #region 证件类型添加

        private UserIdentityModel AddIdCard(UserIdentityModel user, IdCardControl idCard)
        {
            #region 身份证
            user.CertificateType = CertificateType.IdCard;
            user.Name = idCard.TblName.Text; //姓名
            //性别
            if (!string.IsNullOrEmpty(idCard.TblGender.Text))
            {
                switch (idCard.TblGender.Text.Trim())
                {
                    case "男":
                        user.Gender = Gender.Male;
                        break;
                    case "女":
                        user.Gender = Gender.Female;
                        break;
                    default:
                        user.Gender = Gender.Unknown;
                        break;
                }
            }
            //民族
            if (IdCardBuyer.TblNation.Text != null && idCard.TblNation.Text != "")
            {
                user.Nation = (Nation)Nation.Parse(typeof(Nation), idCard.TblNation.Text); //字符串转枚举
            }
            else
            {
                user.Nation = Nation.未知;
            }
            //出生
            if (!string.IsNullOrEmpty(idCard.TblBirthday.Text))
            {
                user.Birthday = Convert.ToDateTime(idCard.TblBirthday.Text);
            }
            //地址
            if (!string.IsNullOrEmpty(idCard.TblAddress.Text))
            {
                user.Address = idCard.TblAddress.Text;
            }
            //证件号
            if (!string.IsNullOrEmpty(idCard.TblIdentityNo.Text))
            {
                user.IdentityNo = idCard.TblIdentityNo.Text;
            }
            //签发机关
            if (!string.IsNullOrEmpty(idCard.TblVisaAgency.Text))
            {
                user.VisaAgency = idCard.TblVisaAgency.Text;
            }
            //有效期-生效
            if (!string.IsNullOrEmpty(idCard.TblEffectiveDate.Text))
            {
                user.EffectiveDate = Convert.ToDateTime(idCard.TblEffectiveDate.Text);
            }

            //有效期-失效
            if (!string.IsNullOrEmpty(idCard.TblExpiryDate.Text))
            {
                if (idCard.TblExpiryDate.Text != "长期")
                {
                    user.ExpiryDate = Convert.ToDateTime(idCard.TblExpiryDate.Text);
                }
                else
                {
                    user.ExpiryDate = DateTime.MaxValue;
                }
            }
            else
            {
                user.ExpiryDate = null;
            }
            //照片
            if (IdentityInfo.Photo != null && IdentityInfo.Photo.Length != 0)
            {
                user.Photo = IdentityInfo.Photo;
            }
            user.IsTrusted = IdentityInfo.IsTrusted;


            #endregion

            return user;
        }

        private UserIdentityModel AddPassport(UserIdentityModel user, PassportControl passport)
        {
            #region 护照
            user.CertificateType = CertificateType.Passport;
            user.Name = passport.TbName.Text; //姓名
            //性别
            if (passport.RdobtnMale.IsChecked == true)
            {
                user.Gender = Gender.Male;
            }
            else if (passport.RdobtnFemale.IsChecked == true)
            {
                user.Gender = Gender.Female;
            }
            else
            {
                user.Gender = Gender.Unknown;
            }
            //出生
            if (!string.IsNullOrEmpty(passport.DateBirthday.Text))
            {
                user.Birthday = Convert.ToDateTime(passport.DateBirthday.Text);
            }
            user.Nationality = passport.CboNationality.Text.Trim(); //国籍
            //证件号
            if (!string.IsNullOrEmpty(passport.TbIdentityNo.Text))
            {
                user.IdentityNo = passport.TbIdentityNo.Text;
            }
            #endregion

            return user;
        }

        private UserIdentityModel AddHKMacaoIdCard(UserIdentityModel user, HKMacaoIdCardControl hKMacaoIdCard)
        {
            #region 港澳居民身份证
            user.CertificateType = CertificateType.HKMacaoIdCard;
            user.Name = hKMacaoIdCard.TbName.Text; //姓名
            //性别
            if (hKMacaoIdCard.RdobtnMale.IsChecked == true)
            {
                user.Gender = Gender.Male;
            }
            else if (hKMacaoIdCard.RdobtnFemale.IsChecked == true)
            {
                user.Gender = Gender.Female;
            }
            else
            {
                user.Gender = Gender.Unknown;
            }
            //出生
            if (!string.IsNullOrEmpty(hKMacaoIdCard.DateBirthday.Text))
            {
                user.Birthday = Convert.ToDateTime(hKMacaoIdCard.DateBirthday.Text);
            }
            //证件号
            if (!string.IsNullOrEmpty(hKMacaoIdCard.TbIdentityNo.Text))
            {
                user.IdentityNo = hKMacaoIdCard.TbIdentityNo.Text;
            }
            user.EffectiveDate = Convert.ToDateTime(hKMacaoIdCard.DateVisaAgency.Text.Trim()); //签发日期

            #endregion

            return user;
        }

        private UserIdentityModel AddBirthCertificate(UserIdentityModel user, BirthCertificateControl birthCertificate)
        {
            #region 出生证
            user.CertificateType = CertificateType.BirthCertificate;
            user.Name = birthCertificate.TbName.Text; //姓名
            //性别
            if (birthCertificate.RdobtnMale.IsChecked == true)
            {
                user.Gender = Gender.Male;
            }
            else if (birthCertificate.RdobtnFemale.IsChecked == true)
            {
                user.Gender = Gender.Female;
            }
            else
            {
                user.Gender = Gender.Unknown;
            }
            //出生
            if (!string.IsNullOrEmpty(birthCertificate.DateBirthday.Text))
            {
                user.Birthday = Convert.ToDateTime(birthCertificate.DateBirthday.Text);
            }
            //证件号
            if (!string.IsNullOrEmpty(birthCertificate.TbIdentityNo.Text))
            {
                user.IdentityNo = birthCertificate.TbIdentityNo.Text;
            }
            user.VisaAgency = birthCertificate.TbVisaAgency.Text.Trim(); //接生机构

            #endregion

            return user;
        }

        private UserIdentityModel AddMacaoPermit(UserIdentityModel user, MacaoPermitControl macaoPermit)
        {
            #region 港澳居民往来大陆通行证
            user.CertificateType = CertificateType.MacaoPermit;
            user.Name = macaoPermit.TbName.Text; //姓名
            //性别
            if (macaoPermit.RdobtnMale.IsChecked == true)
            {
                user.Gender = Gender.Male;
            }
            else if (macaoPermit.RdobtnFemale.IsChecked == true)
            {
                user.Gender = Gender.Female;
            }
            else
            {
                user.Gender = Gender.Unknown;
            }
            //出生
            if (!string.IsNullOrEmpty(macaoPermit.DateBirthday.Text))
            {
                user.Birthday = Convert.ToDateTime(macaoPermit.DateBirthday.Text);
            }
            //证件号
            if (!string.IsNullOrEmpty(macaoPermit.TbIdentityNo.Text))
            {
                user.IdentityNo = macaoPermit.TbIdentityNo.Text;
            }
            user.VisaAgency = macaoPermit.TbVisaAgency.Text.Trim(); //签发机构
            //有效期-生效
            if (!string.IsNullOrEmpty(macaoPermit.TbEffectiveDate.Text))
            {
                DateTime dTime = new DateTime(int.Parse(macaoPermit.TbEffectiveDate.Text), 1, 1);

                user.EffectiveDate = dTime;
            }
            //有效期-失效
            if (!string.IsNullOrEmpty(macaoPermit.TbExpiryDate.Text))
            {
                DateTime dTime = new DateTime(int.Parse(macaoPermit.TbExpiryDate.Text), 12, 31);

                user.ExpiryDate = dTime;
            }

            #endregion

            return user;
        }

        /// <summary>
        /// 台湾居民往来大陆通行证
        /// </summary>
        /// <param name="user"></param>
        /// <param name="taiwanPermit"></param>
        /// <returns></returns>
        private UserIdentityModel AddTaiwanPermit(UserIdentityModel user, TaiwanPermitControl taiwanPermit)
        {
            #region 台湾居民往来大陆通行证
            user.CertificateType = CertificateType.TaiwanPermit;
            user.Name = taiwanPermit.TbName.Text; //姓名
            //性别
            if (taiwanPermit.RdobtnMale.IsChecked == true)
            {
                user.Gender = Gender.Male;
            }
            else if (taiwanPermit.RdobtnFemale.IsChecked == true)
            {
                user.Gender = Gender.Female;
            }
            else
            {
                user.Gender = Gender.Unknown;
            }
            //出生
            if (!string.IsNullOrEmpty(taiwanPermit.DateBirthday.Text))
            {
                user.Birthday = Convert.ToDateTime(taiwanPermit.DateBirthday.Text);
            }
            //证件号
            if (!string.IsNullOrEmpty(taiwanPermit.TbIdentityNo.Text))
            {
                user.IdentityNo = taiwanPermit.TbIdentityNo.Text;
            }
            user.VisaAgency = taiwanPermit.TbVisaAgency.Text.Trim(); //签发机构
            //有效期-生效
            if (!string.IsNullOrEmpty(taiwanPermit.TbEffectiveDate.Text))
            {
                DateTime dTime = new DateTime(int.Parse(taiwanPermit.TbEffectiveDate.Text), 1, 1);

                user.EffectiveDate = dTime;
            }
            //有效期-失效
            if (!string.IsNullOrEmpty(taiwanPermit.TbExpiryDate.Text))
            {
                DateTime dTime = new DateTime(int.Parse(taiwanPermit.TbExpiryDate.Text), 12, 31);

                user.ExpiryDate = dTime;
            }

            #endregion

            return user;
        }

        private UserIdentityModel AddCertificateOfOfficers(UserIdentityModel user, CertificateOfOfficersControl certificateOfOfficers)
        {
            #region 军官证
            user.CertificateType = CertificateType.CertificateOfOfficers;
            user.Name = certificateOfOfficers.TbName.Text; //姓名
            //性别
            if (certificateOfOfficers.RdobtnMale.IsChecked == true)
            {
                user.Gender = Gender.Male;
            }
            else if (certificateOfOfficers.RdobtnFemale.IsChecked == true)
            {
                user.Gender = Gender.Female;
            }
            else
            {
                user.Gender = Gender.Unknown;
            }
            //出生
            if (!string.IsNullOrEmpty(certificateOfOfficers.DateBirthday.Text))
            {
                user.Birthday = Convert.ToDateTime(certificateOfOfficers.DateBirthday.Text);
            }
            //民族
            if (IdCardBuyer.TblNation.Text != null && certificateOfOfficers.TbNation.Text != "")
            {
                user.Nation = (Nation)Nation.Parse(typeof(Nation), certificateOfOfficers.TbNation.Text); //字符串转枚举
            }
            else
            {
                user.Nation = Nation.未知;
            }
            //部别
            if (!string.IsNullOrEmpty(certificateOfOfficers.TbDepartmental.Text))
            {
                user.Address = certificateOfOfficers.TbDepartmental.Text;
            }
            //证件号
            if (!string.IsNullOrEmpty(certificateOfOfficers.TbIdentityNo.Text))
            {
                user.IdentityNo = certificateOfOfficers.TbIdentityNo.Text;
            }
            //发证机关
            if (!string.IsNullOrEmpty(certificateOfOfficers.TbVisaAgency.Text))
            {
                user.VisaAgency = certificateOfOfficers.TbVisaAgency.Text;
            }
            //有效期限-生效
            if (!string.IsNullOrEmpty(certificateOfOfficers.TbEffectiveDate.Text))
            {
                DateTime dTime = new DateTime(int.Parse(certificateOfOfficers.TbEffectiveDate.Text), 1, 1);

                user.EffectiveDate = dTime;
            }
            //有效期限-失效
            if (!string.IsNullOrEmpty(certificateOfOfficers.TbExpiryDate.Text))
            {
                DateTime dTime = new DateTime(int.Parse(certificateOfOfficers.TbExpiryDate.Text), 12, 31);

                user.ExpiryDate = dTime;
            }

            #endregion

            return user;
        }

        private UserIdentityModel AddResidenceBooklet(UserIdentityModel user, ResidenceBookletControl residenceBooklet)
        {
            #region 户口簿
                    user.CertificateType = CertificateType.ResidenceBooklet;
                    user.Name = residenceBooklet.TbName.Text; //姓名
                    //性别
                    if (residenceBooklet.RdobtnMale.IsChecked == true)
                    {
                        user.Gender = Gender.Male;
                    }
                    else if (residenceBooklet.RdobtnFemale.IsChecked == true)
                    {
                        user.Gender = Gender.Female;
                    }
                    else
                    {
                        user.Gender = Gender.Unknown;
                    }
                    //出生
                    if (!string.IsNullOrEmpty(residenceBooklet.DateBirthday.Text))
                    {
                        user.Birthday = Convert.ToDateTime(residenceBooklet.DateBirthday.Text);
                    }
                    //民族
                    if (residenceBooklet.TbNation.Text != null && residenceBooklet.TbNation.Text != "")
                    {
                        user.Nation = (Nation)Nation.Parse(typeof(Nation), residenceBooklet.TbNation.Text); //字符串转枚举
                    }
                    else
                    {
                        user.Nation = Nation.未知;
                    }
                    //证件号
                    if (!string.IsNullOrEmpty(residenceBooklet.TbIdentityNo.Text))
                    {
                        user.IdentityNo = residenceBooklet.TbIdentityNo.Text;
                    }
                    //地址
                    if (!string.IsNullOrEmpty(residenceBooklet.TbAddress.Text))
                    {
                        user.Address = residenceBooklet.TbAddress.Text;
                    }
                    
                #endregion

            return user;
        }

        #endregion



        private OperationResult AddBuyerUserIdentityNew(CertificateType certificateType)
        {
            var user = new UserIdentityModel();
            switch (certificateType)
            {
                #region 身份证
                case CertificateType.IdCard:
                    AddIdCard(user, IdCardBuyer);
                    break;

                #endregion

                #region 护照
                case CertificateType.Passport:
                    AddPassport(user, PassportBuyer);
                    break;
                #endregion

                #region 港澳居民身份证
                case CertificateType.HKMacaoIdCard:
                    AddHKMacaoIdCard(user, HKMacaoIdCardBuyer);
                    break;
                #endregion

                #region 出生证
                case CertificateType.BirthCertificate:
                    AddBirthCertificate(user, BirthCertificateBuyer);
                    break;
                #endregion

                #region 港澳居民往来大陆通行证
                case CertificateType.MacaoPermit:
                    AddMacaoPermit(user, MacaoPermitBuyer);
                    break;
                #endregion

                #region 台湾居民往来大陆通行证
                case CertificateType.TaiwanPermit:
                    AddTaiwanPermit(user, TaiwanPermitBuyer);
                    break;
                #endregion

                #region 军官证
                case CertificateType.CertificateOfOfficers:
                    AddCertificateOfOfficers(user, CertificateOfOfficersBuyer);
                    break;
                #endregion

                #region 户口簿
                case CertificateType.ResidenceBooklet:
                    AddResidenceBooklet(user, ResidenceBookletBuyer);
                    break;
                #endregion
            }
            var result = AddUserIdentity(user, CustomerType.Buyer);
            if (result != null && result.Success)
            {
                //004 显示添加成功的数据
                var newItemSource = LbBuyerList.ItemsSource as List<UserIdentityModel>;
                if (newItemSource == null) newItemSource = new List<UserIdentityModel>();
                user.Tag = result.OtherData["UserSysNo"];
                newItemSource.Add(user);
                //清空数据源再次绑定
                LbBuyerList.ItemsSource = null;
                LbBuyerList.ItemsSource = newItemSource;
                LbBuyerList.DisplayMemberPath = "Name";
                LbBuyerList.SelectedValuePath = "IdentityNo";
            }
            //ClearBuyerIdCardData();
            //ClearBuyerPassportData();

            //New
            //ClearIdCard(IdCardBuyer);
            //ClearPassport(PassportBuyer);
            //ClearHKMacaoIdCard(HKMacaoIdCardBuyer);
            //ClearBirthCertificate(BirthCertificateBuyer);
            //ClearMacaoPermit(MacaoPermitBuyer);
            //ClearCertificateOfOfficers(CertificateOfOfficersBuyer);
            //ClearResidenceBooklet(ResidenceBookletBuyer);
            ClearBuyerIDType();
            return result;
        }


        private OperationResult ModifyBuyerUserIdentityNew(CertificateType certificateType, long userSysNo)
        {
            var user = (UserIdentityModel)LbBuyerList.SelectedItem;
            switch (certificateType)
            {
                #region 身份证
                case CertificateType.IdCard:
                    AddIdCard(user, IdCardBuyer);
                    break;

                #endregion

                #region 护照
                case CertificateType.Passport:
                    AddPassport(user, PassportBuyer);
                    break;
                #endregion

                #region 港澳居民身份证
                case CertificateType.HKMacaoIdCard:
                    AddHKMacaoIdCard(user, HKMacaoIdCardBuyer);
                    break;
                #endregion

                #region 出生证
                case CertificateType.BirthCertificate:
                    AddBirthCertificate(user, BirthCertificateBuyer);
                    break;
                #endregion

                #region 港澳居民往来大陆通行证
                case CertificateType.MacaoPermit:
                    AddMacaoPermit(user, MacaoPermitBuyer);
                    break;
                #endregion

                #region 台湾居民往来大陆通行证
                case CertificateType.TaiwanPermit:
                    AddTaiwanPermit(user, TaiwanPermitBuyer);
                    break;
                #endregion

                #region 军官证
                case CertificateType.CertificateOfOfficers:
                    AddCertificateOfOfficers(user, CertificateOfOfficersBuyer);
                    break;
                #endregion

                #region 户口簿
                case CertificateType.ResidenceBooklet:
                    AddResidenceBooklet(user, ResidenceBookletBuyer);
                    break;
                #endregion
            }
            var result = UserServiceProxy.BindUserIdentity(userSysNo, UserType.Customer, user);
            if (result != null && result.Success)
            {
                //004 显示添加成功的数据
                var newItemSource = LbBuyerList.ItemsSource as List<UserIdentityModel>;
                if (newItemSource == null) newItemSource = new List<UserIdentityModel>();
                //清空数据源再次绑定
                LbBuyerList.ItemsSource = null;
                LbBuyerList.ItemsSource = newItemSource;
                LbBuyerList.DisplayMemberPath = "Name";
                LbBuyerList.SelectedValuePath = "IdentityNo";

                //ClearBuyerIdCardData();
                //ClearBuyerPassportData();

                //New
                //ClearIdCard(IdCardBuyer);
                //ClearPassport(PassportBuyer);
                //ClearHKMacaoIdCard(HKMacaoIdCardBuyer);
                //ClearBirthCertificate(BirthCertificateBuyer);
                //ClearMacaoPermit(MacaoPermitBuyer);
                //ClearCertificateOfOfficers(CertificateOfOfficersBuyer);
                //ClearResidenceBooklet(ResidenceBookletBuyer);
                ClearBuyerIDType();
            }
            else
            {
                MessageBox.Show(result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            return result;
        }


        private OperationResult AddSellerUserIdentityNew(CertificateType certificateType)
        {
            var user = new UserIdentityModel();
            switch (certificateType)
            {
                #region 身份证
                case CertificateType.IdCard:
                    AddIdCard(user, IdCardSeller);
                    break;

                #endregion

                #region 护照
                case CertificateType.Passport:
                    AddPassport(user, PassportSeller);
                    break;
                #endregion

                #region 港澳居民身份证
                case CertificateType.HKMacaoIdCard:
                    AddHKMacaoIdCard(user, HKMacaoIdCardSeller);
                    break;
                #endregion

                #region 出生证
                case CertificateType.BirthCertificate:
                    AddBirthCertificate(user, BirthCertificateSeller);
                    break;
                #endregion

                #region 港澳居民往来大陆通行证
                case CertificateType.MacaoPermit:
                    AddMacaoPermit(user, MacaoPermitSeller);
                    break;
                #endregion

                #region 台湾居民往来大陆通行证
                case CertificateType.TaiwanPermit:
                    AddTaiwanPermit(user, TaiwanPermitSeller);
                    break;
                #endregion

                #region 军官证
                case CertificateType.CertificateOfOfficers:
                    AddCertificateOfOfficers(user, CertificateOfOfficersSeller);
                    break;
                #endregion

                #region 户口簿
                case CertificateType.ResidenceBooklet:
                    AddResidenceBooklet(user, ResidenceBookletSeller);
                    break;
                #endregion
            }
            var result = AddUserIdentity(user, CustomerType.Seller);
            if (result != null && result.Success)
            {
                //004 显示添加成功的数据
                var newItemSource = LbSellerList.ItemsSource as List<UserIdentityModel>;
                if (newItemSource == null) newItemSource = new List<UserIdentityModel>();
                user.Tag = result.OtherData["UserSysNo"];
                newItemSource.Add(user);
                //清空数据源再次绑定
                LbSellerList.ItemsSource = null;
                LbSellerList.ItemsSource = newItemSource;
                LbSellerList.DisplayMemberPath = "Name";
                LbSellerList.SelectedValuePath = "IdentityNo";
            }
            //ClearSellerIdCardData();
            //ClearSellerPassportData();

            //New
            //ClearIdCard(IdCardSeller);
            //ClearPassport(PassportSeller);
            //ClearHKMacaoIdCard(HKMacaoIdCardSeller);
            //ClearBirthCertificate(BirthCertificateSeller);
            //ClearMacaoPermit(MacaoPermitSeller);
            //ClearCertificateOfOfficers(CertificateOfOfficersSeller);
            //ClearResidenceBooklet(ResidenceBookletSeller);
            ClearSellerIDType();
            return result;
        }


        private OperationResult ModifySellerUserIdentityNew(CertificateType certificateType, long userSysNo)
        {
            var user = (UserIdentityModel)LbSellerList.SelectedItem;
            switch (certificateType)
            {
                #region 身份证
                case CertificateType.IdCard:
                    AddIdCard(user, IdCardSeller);
                    break;

                #endregion

                #region 护照
                case CertificateType.Passport:
                    AddPassport(user, PassportSeller);
                    break;
                #endregion

                #region 港澳居民身份证
                case CertificateType.HKMacaoIdCard:
                    AddHKMacaoIdCard(user, HKMacaoIdCardSeller);
                    break;
                #endregion

                #region 出生证
                case CertificateType.BirthCertificate:
                    AddBirthCertificate(user, BirthCertificateSeller);
                    break;
                #endregion

                #region 港澳居民往来大陆通行证
                case CertificateType.MacaoPermit:
                    AddMacaoPermit(user, MacaoPermitSeller);
                    break;
                #endregion

                #region 台湾居民往来大陆通行证
                case CertificateType.TaiwanPermit:
                    AddTaiwanPermit(user, TaiwanPermitSeller);
                    break;
                #endregion

                #region 军官证
                case CertificateType.CertificateOfOfficers:
                    AddCertificateOfOfficers(user, CertificateOfOfficersSeller);
                    break;
                #endregion

                #region 户口簿
                case CertificateType.ResidenceBooklet:
                    AddResidenceBooklet(user, ResidenceBookletSeller);
                    break;
                #endregion
            }
            var result = UserServiceProxy.BindUserIdentity(userSysNo, UserType.Customer, user);
            if (result != null && result.Success)
            {
                //004 显示添加成功的数据
                var newItemSource = LbSellerList.ItemsSource as List<UserIdentityModel>;
                if (newItemSource == null) newItemSource = new List<UserIdentityModel>();
                //清空数据源再次绑定
                LbSellerList.ItemsSource = null;
                LbSellerList.ItemsSource = newItemSource;
                LbSellerList.DisplayMemberPath = "Name";
                LbSellerList.SelectedValuePath = "IdentityNo";
                //ClearSellerIdCardData();
                //ClearSellerPassportData();

                //New
                //ClearIdCard(IdCardSeller);
                //ClearPassport(PassportSeller);
                //ClearHKMacaoIdCard(HKMacaoIdCardSeller);
                //ClearBirthCertificate(BirthCertificateSeller);
                //ClearMacaoPermit(MacaoPermitSeller);
                //ClearCertificateOfOfficers(CertificateOfOfficersSeller);
                //ClearResidenceBooklet(ResidenceBookletSeller);
                ClearSellerIDType();
            }
            else
            {
                MessageBox.Show(result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return result;
        }


        /// <summary>
        /// 添加买方数据
        /// </summary>
        /// <returns></returns>
        private OperationResult AddBuyerUserIdentity(CertificateType certificateType)
        {
            var user = new UserIdentityModel();
            switch (certificateType)
            {
                case CertificateType.IdCard:
                    user.CertificateType = CertificateType.IdCard;
                    user.Name = TblBuyerName.Text;
                    if (!string.IsNullOrEmpty(TblBuyerGender.Text))
                    {
                        switch (TblBuyerGender.Text.Trim())
                        {
                            case "男":
                                user.Gender = Gender.Male;
                                break;
                            case "女":
                                user.Gender = Gender.Female;
                                break;
                            default:
                                user.Gender = Gender.Unknown;
                                break;
                        }
                    }
                    if (TblBuyerNation.Text != null && TblBuyerNation.Text != "")
                    {
                        user.Nation = (Nation)Nation.Parse(typeof(Nation), TblBuyerNation.Text); //字符串转枚举
                    }
                    else
                    {
                        user.Nation = Nation.未知;
                    }
                    if (!string.IsNullOrEmpty(TblBuyerBirthday.Text))
                    {
                        user.Birthday = Convert.ToDateTime(TblBuyerBirthday.Text);
                    }
                    if (!string.IsNullOrEmpty(TblBuyerAddress.Text))
                    {
                        user.Address = TblBuyerAddress.Text;
                    }
                    if (!string.IsNullOrEmpty(TblBuyerIdentityNo.Text))
                    {
                        user.IdentityNo = TblBuyerIdentityNo.Text;
                    }
                    if (!string.IsNullOrEmpty(TblBuyerVisaAgency.Text))
                    {
                        user.VisaAgency = TblBuyerVisaAgency.Text;
                    }
                    if (!string.IsNullOrEmpty(TblBuyerEffectiveDate.Text))
                    {
                        user.EffectiveDate = Convert.ToDateTime(TblBuyerEffectiveDate.Text);
                    }

                    if (!string.IsNullOrEmpty(TblBuyerExpiryDate.Text))
                    {
                        if (TblBuyerExpiryDate.Text != "长期")
                        {
                            user.ExpiryDate = Convert.ToDateTime(TblBuyerExpiryDate.Text);
                        }
                        else
                        {
                            user.ExpiryDate = DateTime.MaxValue;
                        }
                    }
                    else
                    {
                        user.ExpiryDate = null;
                    }
                    if (IdentityInfo.Photo != null && IdentityInfo.Photo.Length != 0)
                    {
                        user.Photo = IdentityInfo.Photo;
                    }
                    user.IsTrusted = IdentityInfo.IsTrusted;
                    break;
                case CertificateType.Passport:
                    user.CertificateType = CertificateType.Passport;
                    user.Name = TbBuyerName.Text;
                    if (RdoBtnBuyerGenderMale.IsChecked == true)
                    {
                        user.Gender = Gender.Male;
                    }
                    else if (RdoBtnBuyerGenderFemale.IsChecked == true)
                    {
                        user.Gender = Gender.Female;
                    }
                    else
                    {
                        user.Gender = Gender.Unknown;
                    }
                    user.Birthday = Convert.ToDateTime(TbBuyerBirthday.Text.Trim());
                    user.Nationality = CboBuyerNationality.Text.Trim();
                    user.IdentityNo = TbBuyerIdentityNo.Text.Trim();
                    break;
            }
            var result = AddUserIdentity(user, CustomerType.Buyer);
            if (result != null && result.Success)
            {
                //004 显示添加成功的数据
                var newItemSource = LbBuyerList.ItemsSource as List<UserIdentityModel>;
                if (newItemSource == null) newItemSource = new List<UserIdentityModel>();
                user.Tag = result.OtherData["UserSysNo"];
                newItemSource.Add(user);
                //清空数据源再次绑定
                LbBuyerList.ItemsSource = null;
                LbBuyerList.ItemsSource = newItemSource;
                LbBuyerList.DisplayMemberPath = "Name";
                LbBuyerList.SelectedValuePath = "IdentityNo";
            }
            ClearBuyerIdCardData();
            ClearBuyerPassportData();
            return result;
        }

        private OperationResult AddBuyerUserIdentity1(CertificateType certificateType)
        {
            var user = new UserIdentityModel();
            switch (certificateType)
            {
                #region 身份证
                case CertificateType.IdCard:
                    user.CertificateType = CertificateType.IdCard;
                    user.Name = IdCardBuyer.TblName.Text; //姓名
                    //性别
                    if (!string.IsNullOrEmpty(IdCardBuyer.TblGender.Text))
                    {
                        switch (IdCardBuyer.TblGender.Text.Trim())
                        {
                            case "男":
                                user.Gender = Gender.Male;
                                break;
                            case "女":
                                user.Gender = Gender.Female;
                                break;
                            default:
                                user.Gender = Gender.Unknown;
                                break;
                        }
                    }
                    //民族
                    if (IdCardBuyer.TblNation.Text != null && IdCardBuyer.TblNation.Text != "")
                    {
                        user.Nation = (Nation)Nation.Parse(typeof(Nation), IdCardBuyer.TblNation.Text); //字符串转枚举
                    }
                    else
                    {
                        user.Nation = Nation.未知;
                    }
                    //出生
                    if (!string.IsNullOrEmpty(IdCardBuyer.TblBirthday.Text))
                    {
                        user.Birthday = Convert.ToDateTime(IdCardBuyer.TblBirthday.Text);
                    }
                    //地址
                    if (!string.IsNullOrEmpty(IdCardBuyer.TblAddress.Text))
                    {
                        user.Address = IdCardBuyer.TblAddress.Text;
                    }
                    //证件号
                    if (!string.IsNullOrEmpty(IdCardBuyer.TblIdentityNo.Text))
                    {
                        user.IdentityNo = IdCardBuyer.TblIdentityNo.Text;
                    }
                    //签发机关
                    if (!string.IsNullOrEmpty(IdCardBuyer.TblVisaAgency.Text))
                    {
                        user.VisaAgency = IdCardBuyer.TblVisaAgency.Text;
                    }
                    //有效期-生效
                    if (!string.IsNullOrEmpty(IdCardBuyer.TblEffectiveDate.Text))
                    {
                        user.EffectiveDate = Convert.ToDateTime(IdCardBuyer.TblEffectiveDate.Text);
                    }

                    //有效期-失效
                    if (!string.IsNullOrEmpty(IdCardBuyer.TblExpiryDate.Text))
                    {
                        if (IdCardBuyer.TblExpiryDate.Text != "长期")
                        {
                            user.ExpiryDate = Convert.ToDateTime(IdCardBuyer.TblExpiryDate.Text);
                        }
                        else
                        {
                            user.ExpiryDate = DateTime.MaxValue;
                        }
                    }
                    else
                    {
                        user.ExpiryDate = null;
                    }
                    //照片
                    if (IdentityInfo.Photo != null && IdentityInfo.Photo.Length != 0)
                    {
                        user.Photo = IdentityInfo.Photo;
                    }
                    user.IsTrusted = IdentityInfo.IsTrusted;
                    break;

                #endregion

                #region 护照
                case CertificateType.Passport:
                    user.CertificateType = CertificateType.Passport;
                    user.Name = PassportBuyer.TbName.Text; //姓名
                    //性别
                    if (PassportBuyer.RdobtnMale.IsChecked == true)
                    {
                        user.Gender = Gender.Male;
                    }
                    else if (PassportBuyer.RdobtnFemale.IsChecked == true)
                    {
                        user.Gender = Gender.Female;
                    }
                    else
                    {
                        user.Gender = Gender.Unknown;
                    }
                    //出生
                    if (!string.IsNullOrEmpty(PassportBuyer.DateBirthday.Text))
                    {
                        user.Birthday = Convert.ToDateTime(PassportBuyer.DateBirthday.Text);
                    }
                    user.Nationality = PassportBuyer.CboNationality.Text.Trim(); //国籍
                    //证件号
                    if (!string.IsNullOrEmpty(PassportBuyer.TbIdentityNo.Text))
                    {
                        user.IdentityNo = PassportBuyer.TbIdentityNo.Text;
                    }
                    break;
                #endregion

                #region 港澳居民身份证
                case CertificateType.HKMacaoIdCard:
                    user.CertificateType = CertificateType.HKMacaoIdCard;
                    user.Name = HKMacaoIdCardBuyer.TbName.Text; //姓名
                    //性别
                    if (HKMacaoIdCardBuyer.RdobtnMale.IsChecked == true)
                    {
                        user.Gender = Gender.Male;
                    }
                    else if (HKMacaoIdCardBuyer.RdobtnFemale.IsChecked == true)
                    {
                        user.Gender = Gender.Female;
                    }
                    else
                    {
                        user.Gender = Gender.Unknown;
                    }
                    //出生
                    if (!string.IsNullOrEmpty(HKMacaoIdCardBuyer.DateBirthday.Text))
                    {
                        user.Birthday = Convert.ToDateTime(HKMacaoIdCardBuyer.DateBirthday.Text);
                    }
                    //证件号
                    if (!string.IsNullOrEmpty(HKMacaoIdCardBuyer.TbIdentityNo.Text))
                    {
                        user.IdentityNo = HKMacaoIdCardBuyer.TbIdentityNo.Text;
                    }
                    user.EffectiveDate = Convert.ToDateTime(HKMacaoIdCardBuyer.DateVisaAgency.Text.Trim()); //签发日期
                    break;
                #endregion

                #region 出生证
                case CertificateType.BirthCertificate:
                    user.CertificateType = CertificateType.BirthCertificate;
                    user.Name = BirthCertificateBuyer.TbName.Text; //姓名
                    //性别
                    if (BirthCertificateBuyer.RdobtnMale.IsChecked == true)
                    {
                        user.Gender = Gender.Male;
                    }
                    else if (BirthCertificateBuyer.RdobtnFemale.IsChecked == true)
                    {
                        user.Gender = Gender.Female;
                    }
                    else
                    {
                        user.Gender = Gender.Unknown;
                    }
                    //出生
                    if (!string.IsNullOrEmpty(BirthCertificateBuyer.DateBirthday.Text))
                    {
                        user.Birthday = Convert.ToDateTime(BirthCertificateBuyer.DateBirthday.Text);
                    }
                    //证件号
                    if (!string.IsNullOrEmpty(BirthCertificateBuyer.TbIdentityNo.Text))
                    {
                        user.IdentityNo = BirthCertificateBuyer.TbIdentityNo.Text;
                    }
                    user.VisaAgency = BirthCertificateBuyer.TbVisaAgency.Text.Trim(); //接生机构
                    break;
                #endregion

                #region 港澳通行证
                case CertificateType.MacaoPermit:
                    user.CertificateType = CertificateType.MacaoPermit;
                    user.Name = MacaoPermitBuyer.TbName.Text; //姓名
                    //性别
                    if (MacaoPermitBuyer.RdobtnMale.IsChecked == true)
                    {
                        user.Gender = Gender.Male;
                    }
                    else if (MacaoPermitBuyer.RdobtnFemale.IsChecked == true)
                    {
                        user.Gender = Gender.Female;
                    }
                    else
                    {
                        user.Gender = Gender.Unknown;
                    }
                    //出生
                    if (!string.IsNullOrEmpty(MacaoPermitBuyer.DateBirthday.Text))
                    {
                        user.Birthday = Convert.ToDateTime(MacaoPermitBuyer.DateBirthday.Text);
                    }
                    //证件号
                    if (!string.IsNullOrEmpty(MacaoPermitBuyer.TbIdentityNo.Text))
                    {
                        user.IdentityNo = MacaoPermitBuyer.TbIdentityNo.Text;
                    }
                    user.VisaAgency = MacaoPermitBuyer.TbVisaAgency.Text.Trim(); //签发机构
                    //有效期-生效
                    if (!string.IsNullOrEmpty(MacaoPermitBuyer.TbEffectiveDate.Text))
                    {
                        DateTime dTime = new DateTime(int.Parse(MacaoPermitBuyer.TbEffectiveDate.Text), 1, 1);

                        user.EffectiveDate = dTime;
                    }
                    //有效期-失效
                    if (!string.IsNullOrEmpty(MacaoPermitBuyer.TbExpiryDate.Text))
                    {
                        DateTime dTime = new DateTime(int.Parse(MacaoPermitBuyer.TbExpiryDate.Text), 12, 31);

                        user.ExpiryDate = dTime;
                    }
                    break;
                #endregion

                #region 军官证
                case CertificateType.CertificateOfOfficers:
                    user.CertificateType = CertificateType.CertificateOfOfficers;
                    user.Name = CertificateOfOfficersBuyer.TbName.Text; //姓名
                    //性别
                    if (CertificateOfOfficersBuyer.RdobtnMale.IsChecked == true)
                    {
                        user.Gender = Gender.Male;
                    }
                    else if (CertificateOfOfficersBuyer.RdobtnFemale.IsChecked == true)
                    {
                        user.Gender = Gender.Female;
                    }
                    else
                    {
                        user.Gender = Gender.Unknown;
                    }
                    //出生
                    if (!string.IsNullOrEmpty(CertificateOfOfficersBuyer.DateBirthday.Text))
                    {
                        user.Birthday = Convert.ToDateTime(CertificateOfOfficersBuyer.DateBirthday.Text);
                    }
                    //民族
                    if (IdCardBuyer.TblNation.Text != null && CertificateOfOfficersBuyer.TbNation.Text != "")
                    {
                        user.Nation = (Nation)Nation.Parse(typeof(Nation), CertificateOfOfficersBuyer.TbNation.Text); //字符串转枚举
                    }
                    else
                    {
                        user.Nation = Nation.未知;
                    }
                    //部别
                    if (!string.IsNullOrEmpty(CertificateOfOfficersBuyer.TbDepartmental.Text))
                    {
                        user.Address = CertificateOfOfficersBuyer.TbDepartmental.Text;
                    }
                    //证件号
                    if (!string.IsNullOrEmpty(CertificateOfOfficersBuyer.TbIdentityNo.Text))
                    {
                        user.IdentityNo = CertificateOfOfficersBuyer.TbIdentityNo.Text;
                    }
                    //发证机关
                    if (!string.IsNullOrEmpty(CertificateOfOfficersBuyer.TbVisaAgency.Text))
                    {
                        user.VisaAgency = CertificateOfOfficersBuyer.TbVisaAgency.Text;
                    }
                    //有效期限-生效
                    if (!string.IsNullOrEmpty(CertificateOfOfficersBuyer.TbEffectiveDate.Text))
                    {
                        DateTime dTime = new DateTime(int.Parse(CertificateOfOfficersBuyer.TbEffectiveDate.Text), 1, 1);

                        user.EffectiveDate = dTime;
                    }
                    //有效期限-失效
                    if (!string.IsNullOrEmpty(CertificateOfOfficersBuyer.TbExpiryDate.Text))
                    {
                        DateTime dTime = new DateTime(int.Parse(CertificateOfOfficersBuyer.TbExpiryDate.Text), 12, 31);

                        user.ExpiryDate = dTime;
                    }
                    break;
                #endregion

                #region 户口簿
                case CertificateType.ResidenceBooklet:
                    user.CertificateType = CertificateType.ResidenceBooklet;
                    user.Name = ResidenceBookletBuyer.TbName.Text; //姓名
                    //性别
                    if (ResidenceBookletBuyer.RdobtnMale.IsChecked == true)
                    {
                        user.Gender = Gender.Male;
                    }
                    else if (ResidenceBookletBuyer.RdobtnFemale.IsChecked == true)
                    {
                        user.Gender = Gender.Female;
                    }
                    else
                    {
                        user.Gender = Gender.Unknown;
                    }
                    //出生
                    if (!string.IsNullOrEmpty(ResidenceBookletBuyer.DateBirthday.Text))
                    {
                        user.Birthday = Convert.ToDateTime(ResidenceBookletBuyer.DateBirthday.Text);
                    }
                    //民族
                    if (ResidenceBookletBuyer.TbNation.Text != null && ResidenceBookletBuyer.TbNation.Text != "")
                    {
                        user.Nation = (Nation)Nation.Parse(typeof(Nation), ResidenceBookletBuyer.TbNation.Text); //字符串转枚举
                    }
                    else
                    {
                        user.Nation = Nation.未知;
                    }
                    //证件号
                    if (!string.IsNullOrEmpty(ResidenceBookletBuyer.TbIdentityNo.Text))
                    {
                        user.IdentityNo = ResidenceBookletBuyer.TbIdentityNo.Text;
                    }
                    //地址
                    if (!string.IsNullOrEmpty(ResidenceBookletBuyer.TbAddress.Text))
                    {
                        user.Address = ResidenceBookletBuyer.TbAddress.Text;
                    }
                    break;
                #endregion
            }
            var result = AddUserIdentity(user, CustomerType.Buyer);
            if (result != null && result.Success)
            {
                //004 显示添加成功的数据
                var newItemSource = LbBuyerList.ItemsSource as List<UserIdentityModel>;
                if (newItemSource == null) newItemSource = new List<UserIdentityModel>();
                user.Tag = result.OtherData["UserSysNo"];
                newItemSource.Add(user);
                //清空数据源再次绑定
                LbBuyerList.ItemsSource = null;
                LbBuyerList.ItemsSource = newItemSource;
                LbBuyerList.DisplayMemberPath = "Name";
                LbBuyerList.SelectedValuePath = "IdentityNo";
            }
            ClearBuyerIdCardData();
            ClearBuyerPassportData();
            return result;
        }

        /// <summary>
        /// 添加卖家数据
        /// </summary>
        /// <returns></returns>
        private OperationResult AddSellerUserIdentity(CertificateType certificateType)
        {
            var user = new UserIdentityModel();
            switch (certificateType)
            {
                case CertificateType.IdCard:
                    user.CertificateType = CertificateType.IdCard;
                    user.Name = TblSellerName.Text;
                    if (!string.IsNullOrEmpty(TblBuyerGender.Text))
                    {
                        switch (TblSellerGender.Text.Trim())
                        {
                            case "男":
                                user.Gender = Gender.Male;
                                break;
                            case "女":
                                user.Gender = Gender.Female;
                                break;
                            default:
                                user.Gender = Gender.Unknown;
                                break;
                        }
                    }
                    if (TblSellerNation.Text != null && TblSellerNation.Text != "")
                    {
                        user.Nation = (Nation)Nation.Parse(typeof(Nation), TblSellerNation.Text); //字符串转枚举
                    }
                    else
                    {
                        user.Nation = Nation.未知;
                    }
                    if (!string.IsNullOrEmpty(TblSellerBirthday.Text))
                    {
                        user.Birthday = Convert.ToDateTime(TblSellerBirthday.Text);
                    }
                    if (!string.IsNullOrEmpty(TblSellerAddress.Text))
                    {
                        user.Address = TblSellerAddress.Text;
                    }
                    if (!string.IsNullOrEmpty(TblSellerIdentityNo.Text))
                    {
                        user.IdentityNo = TblSellerIdentityNo.Text;
                    }
                    if (!string.IsNullOrEmpty(TblSellerVisaAgency.Text))
                    {
                        user.VisaAgency = TblSellerVisaAgency.Text;
                    }

                    if (!string.IsNullOrEmpty(TblSellerEffectiveDate.Text))
                    {
                        user.EffectiveDate = Convert.ToDateTime(TblSellerEffectiveDate.Text);
                    }

                    if (!string.IsNullOrEmpty(TblSellerExpiryDate.Text))
                    {
                        if (TblSellerExpiryDate.Text != "长期")
                        {
                            user.ExpiryDate = Convert.ToDateTime(TblSellerExpiryDate.Text);
                        }
                        else
                        {
                            user.ExpiryDate = DateTime.MaxValue;
                        }
                    }
                    else
                    {
                        user.ExpiryDate = null;
                    }
                    if (IdentityInfo.Photo != null && IdentityInfo.Photo.Length != 0)
                    {
                        user.Photo = IdentityInfo.Photo;
                    }
                    user.IsTrusted = IdentityInfo.IsTrusted;
                    break;
                case CertificateType.Passport:
                    user.CertificateType = CertificateType.Passport;
                    user.Name = TbSellerName.Text;
                    if (RdoBtnSellerGenderMale.IsChecked == true)
                    {
                        user.Gender = Gender.Male;
                    }
                    else if (RdoBtnSellerGenderFemale.IsChecked == true)
                    {
                        user.Gender = Gender.Female;
                    }
                    else
                    {
                        user.Gender = Gender.Unknown;
                    }
                    user.Birthday = Convert.ToDateTime(TbSellerBirthday.Text.Trim());
                    user.Nationality = CboSellerNationality.Text.Trim();
                    user.IdentityNo = TbSellerIdentityNo.Text.Trim();
                    break;
            }
            var result = AddUserIdentity(user, CustomerType.Seller);
            if (result != null && result.Success)
            {
                //004 显示添加成功的数据
                var newItemSource = LbSellerList.ItemsSource as List<UserIdentityModel>;
                if (newItemSource == null) newItemSource = new List<UserIdentityModel>();
                user.Tag = result.OtherData["UserSysNo"];
                newItemSource.Add(user);
                //清空数据源再次绑定
                LbSellerList.ItemsSource = null;
                LbSellerList.ItemsSource = newItemSource;
                LbSellerList.DisplayMemberPath = "Name";
                LbSellerList.SelectedValuePath = "IdentityNo";
            }
            ClearSellerIdCardData();
            ClearSellerPassportData();
            return result;
        }

        /// <summary>
        /// 修改买方数据
        /// </summary>
        /// <param name="certificateType"></param>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        private OperationResult ModifyBuyerUserIdentity(CertificateType certificateType, long userSysNo)
        {
            var user = (UserIdentityModel)LbBuyerList.SelectedItem;
            switch (certificateType)
            {
                case CertificateType.IdCard:
                    user.CertificateType = CertificateType.IdCard;
                    user.Name = TblBuyerName.Text;
                    switch (TblBuyerGender.Text.Trim())
                    {
                        case "男":
                            user.Gender = Gender.Male;
                            break;
                        case "女":
                            user.Gender = Gender.Female;
                            break;
                        default:
                            user.Gender = Gender.Unknown;
                            break;
                    }
                    if (TblBuyerNation.Text != null)
                    {
                        user.Nation = (Nation)Nation.Parse(typeof(Nation), TblBuyerNation.Text); //字符串转枚举
                    }
                    user.Birthday = Convert.ToDateTime(TblBuyerBirthday.Text);
                    user.Address = TblBuyerAddress.Text;
                    user.IdentityNo = TblBuyerIdentityNo.Text;
                    user.VisaAgency = TblBuyerVisaAgency.Text;
                    user.EffectiveDate = Convert.ToDateTime(TblBuyerEffectiveDate.Text);
                    if (TblBuyerExpiryDate.Text != "长期")
                    {
                        user.ExpiryDate = Convert.ToDateTime(TblBuyerExpiryDate.Text);
                    }
                    else
                    {
                        user.ExpiryDate = null;
                    }
                    user.Photo = IdentityInfo.Photo;
                    user.IsTrusted = IdentityInfo.IsTrusted;
                    break;
                case CertificateType.Passport:
                    user.CertificateType = CertificateType.Passport;
                    user.Name = TbBuyerName.Text;
                    if (RdoBtnBuyerGenderMale.IsChecked == true)
                    {
                        user.Gender = Gender.Male;
                    }
                    else if (RdoBtnBuyerGenderFemale.IsChecked == true)
                    {
                        user.Gender = Gender.Female;
                    }
                    else
                    {
                        user.Gender = Gender.Unknown;
                    }
                    user.Birthday = Convert.ToDateTime(TbBuyerBirthday.Text.Trim());
                    user.Nationality = CboBuyerNationality.Text.Trim();
                    user.IdentityNo = TbBuyerIdentityNo.Text.Trim();
                    break;
            }
            var result = UserServiceProxy.BindUserIdentity(userSysNo, UserType.Customer, user);
            if (result != null && result.Success)
            {
                //004 显示添加成功的数据
                var newItemSource = LbBuyerList.ItemsSource as List<UserIdentityModel>;
                if (newItemSource == null) newItemSource = new List<UserIdentityModel>();
                //清空数据源再次绑定
                LbBuyerList.ItemsSource = null;
                LbBuyerList.ItemsSource = newItemSource;
                LbBuyerList.DisplayMemberPath = "Name";
                LbBuyerList.SelectedValuePath = "IdentityNo";

                ClearBuyerIdCardData();
                ClearBuyerPassportData();
            }
            else
            {
                MessageBox.Show(result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            return result;
        }

        /// <summary>
        /// 修改卖家数据
        /// </summary>
        /// <param name="certificateType"></param>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        private OperationResult ModifySellerUserIdentity(CertificateType certificateType, long userSysNo)
        {
            var user = (UserIdentityModel)LbSellerList.SelectedItem;
            switch (certificateType)
            {
                case CertificateType.IdCard:
                    user.CertificateType = CertificateType.IdCard;
                    user.Name = TblSellerName.Text;
                    switch (TblSellerGender.Text.Trim())
                    {
                        case "男":
                            user.Gender = Gender.Male;
                            break;
                        case "女":
                            user.Gender = Gender.Female;
                            break;
                        default:
                            user.Gender = Gender.Unknown;
                            break;
                    }
                    if (TblSellerNation.Text != null)
                    {
                        user.Nation = (Nation)Nation.Parse(typeof(Nation), TblSellerNation.Text); //字符串转枚举
                    }
                    user.Birthday = Convert.ToDateTime(TblSellerBirthday.Text);
                    user.Address = TblSellerAddress.Text;
                    user.IdentityNo = TblSellerIdentityNo.Text;
                    user.VisaAgency = TblSellerVisaAgency.Text;
                    user.EffectiveDate = Convert.ToDateTime(TblSellerEffectiveDate.Text);
                    if (TblSellerExpiryDate.Text != "长期")
                    {
                        user.ExpiryDate = Convert.ToDateTime(TblSellerExpiryDate.Text);
                    }
                    else
                    {
                        user.ExpiryDate = null;
                    }
                    user.Photo = IdentityInfo.Photo;
                    user.IsTrusted = IdentityInfo.IsTrusted;
                    break;
                case CertificateType.Passport:
                    user.CertificateType = CertificateType.Passport;
                    user.Name = TbSellerName.Text;
                    if (RdoBtnSellerGenderMale.IsChecked == true)
                    {
                        user.Gender = Gender.Male;
                    }
                    else if (RdoBtnSellerGenderFemale.IsChecked == true)
                    {
                        user.Gender = Gender.Female;
                    }
                    else
                    {
                        user.Gender = Gender.Unknown;
                    }
                    user.Birthday = Convert.ToDateTime(TbSellerBirthday.Text.Trim());
                    user.Nationality = CboSellerNationality.Text.Trim();
                    user.IdentityNo = TbSellerIdentityNo.Text.Trim();
                    break;
            }
            var result = UserServiceProxy.BindUserIdentity(userSysNo, UserType.Customer, user);
            if (result != null && result.Success)
            {
                //004 显示添加成功的数据
                var newItemSource = LbSellerList.ItemsSource as List<UserIdentityModel>;
                if (newItemSource == null) newItemSource = new List<UserIdentityModel>();
                //清空数据源再次绑定
                LbSellerList.ItemsSource = null;
                LbSellerList.ItemsSource = newItemSource;
                LbSellerList.DisplayMemberPath = "Name";
                LbSellerList.SelectedValuePath = "IdentityNo";
                ClearSellerIdCardData();
                ClearSellerPassportData();
            }
            else
            {
                MessageBox.Show(result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return result;
        }


        /// <summary>
        /// 保存买卖家信息
        /// </summary>     
        /// <param name="customerType">类型</param>        
        private OperationResult AddUserIdentity(UserIdentityModel userIdentity, CustomerType customerType)
        {
            OperationResult result = null;
            try
            {
                switch (customerType)
                {
                    case CustomerType.Buyer:
                        result = CustomerServiceProxy.CreateBuyer(userIdentity, LoginHelper.CurrentUser.SysNo);
                        break;
                    case CustomerType.Seller:
                        result = CustomerServiceProxy.CreateSeller(userIdentity, LoginHelper.CurrentUser.SysNo);
                        break;
                }
                return result;
            }
            catch (Exception ex)
            {
                result.ResultMessage = ex.Message;
                return result;
            }
        }

        /// <summary>
        /// 增加新的案件
        /// </summary>
        /// <param name="caseDto"></param>
        /// <returns></returns>
        private OperationResult AddNewCase(CaseDto caseDto)
        {
            OperationResult result = null;
            try
            {
                caseDto.ReceptionCenter = ConfigHelper.GetCurrentReceptionCenter();
                result = CaseProxyService.GetInstanse().GenerateCase(caseDto);
            }
            catch (Exception ex)
            {
                result.ResultMessage = ex.Message;
            }
            return result;
        }

        #region 清空

        /// <summary>
        /// 清空各证件类型数据
        /// </summary>
        private void ClearBuyerIDType()
        {
            ClearIdCard(IdCardBuyer);
            ClearPassport(PassportBuyer);
            ClearHKMacaoIdCard(HKMacaoIdCardBuyer);
            ClearBirthCertificate(BirthCertificateBuyer);
            ClearMacaoPermit(MacaoPermitBuyer);
            ClearTaiwanPermit(TaiwanPermitBuyer);
            ClearCertificateOfOfficers(CertificateOfOfficersBuyer);
            ClearResidenceBooklet(ResidenceBookletBuyer);
        }

        /// <summary>
        /// 清空各证件类型数据
        /// </summary>
        private void ClearSellerIDType()
        {
            ClearIdCard(IdCardSeller);
            ClearPassport(PassportSeller);
            ClearHKMacaoIdCard(HKMacaoIdCardSeller);
            ClearBirthCertificate(BirthCertificateSeller);
            ClearMacaoPermit(MacaoPermitSeller);
            ClearTaiwanPermit(TaiwanPermitSeller);
            ClearCertificateOfOfficers(CertificateOfOfficersSeller);
            ClearResidenceBooklet(ResidenceBookletSeller);
        }

        /// <summary>
        /// 清空身份证信息
        /// </summary>
        /// <param name="idCard"></param>
        private void ClearIdCard(IdCardControl idCard)
        {
            try
            {
                idCard.TblName.Text = string.Empty;
                idCard.TblGender.Text = string.Empty;
                idCard.TblNation.Text = string.Empty;
                idCard.TblBirthday.Text = string.Empty;
                idCard.TblAddress.Text = string.Empty;
                idCard.TblIdentityNo.Text = string.Empty;
                idCard.TblIdentityNo.Tag = null;//一定要清空 记录每个身份证件的唯一标识
                idCard.TblVisaAgency.Text = string.Empty;
                idCard.TblEffectiveDate.Text = string.Empty;
                idCard.TblExpiryDate.Text = string.Empty;
                idCard.TblStr.Text = string.Empty;
                idCard.ImgPhoto.Source = null;
            }
            catch (Exception e)
            {
                MessageBox.Show("重置数据发生异常：" + e.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 清空护照信息
        /// </summary>
        /// <param name="passport"></param>
        private void ClearPassport(PassportControl passport)
        {
            try
            {
                passport.TbName.Text = string.Empty;
                passport.RdobtnMale.IsChecked = false;
                passport.RdobtnFemale.IsChecked = false;
                passport.DateBirthday.Text = string.Empty;
                passport.CboNationality.SelectedValue = -1;
                passport.TbIdentityNo.Text = string.Empty;
                passport.TbIdentityNo.Tag = null;//一定要清空 记录每个身份证件的唯一标识
            }
            catch (Exception e)
            {
                MessageBox.Show("重置数据发生异常：" + e.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 清空港澳居民身份证信息
        /// </summary>
        /// <param name="hKMacaoIdCard"></param>
        private void ClearHKMacaoIdCard(HKMacaoIdCardControl hKMacaoIdCard)
        {
            try
            {
                hKMacaoIdCard.TbName.Text = string.Empty;
                hKMacaoIdCard.RdobtnMale.IsChecked = false;
                hKMacaoIdCard.RdobtnFemale.IsChecked = false;
                hKMacaoIdCard.DateBirthday.Text = string.Empty;
                hKMacaoIdCard.TbIdentityNo.Text = string.Empty;
                hKMacaoIdCard.TbIdentityNo.Tag = null;//一定要清空 记录每个身份证件的唯一标识
                hKMacaoIdCard.DateVisaAgency.Text = string.Empty;
            }
            catch (Exception e)
            {
                MessageBox.Show("重置数据发生异常：" + e.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 清空出生证信息
        /// </summary>
        /// <param name="birthCertificate"></param>
        private void ClearBirthCertificate(BirthCertificateControl birthCertificate)
        {
            try
            {
                birthCertificate.TbName.Text = string.Empty;
                birthCertificate.RdobtnMale.IsChecked = false;
                birthCertificate.RdobtnFemale.IsChecked = false;
                birthCertificate.DateBirthday.Text = string.Empty;
                birthCertificate.TbIdentityNo.Text = string.Empty;
                birthCertificate.TbIdentityNo.Tag = null;//一定要清空 记录每个身份证件的唯一标识
                birthCertificate.TbVisaAgency.Text = string.Empty;
            }
            catch (Exception e)
            {
                MessageBox.Show("重置数据发生异常：" + e.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 清空港澳居民往来大陆通行证信息
        /// </summary>
        /// <param name="macaoPermit"></param>
        private void ClearMacaoPermit(MacaoPermitControl macaoPermit)
        {
            try
            {
                macaoPermit.TbName.Text = string.Empty;
                macaoPermit.RdobtnMale.IsChecked = false;
                macaoPermit.RdobtnFemale.IsChecked = false;
                macaoPermit.DateBirthday.Text = string.Empty;
                macaoPermit.TbIdentityNo.Text = string.Empty;
                macaoPermit.TbIdentityNo.Tag = null;//一定要清空 记录每个身份证件的唯一标识
                macaoPermit.TbVisaAgency.Text = string.Empty;
                macaoPermit.TbEffectiveDate.Text = string.Empty;
                macaoPermit.TbExpiryDate.Text = string.Empty;
            }
            catch (Exception e)
            {
                MessageBox.Show("重置数据发生异常：" + e.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 清空台湾居民往来大陆通行证信息
        /// </summary>
        /// <param name="taiwanPermit"></param>
        private void ClearTaiwanPermit(TaiwanPermitControl taiwanPermit)
        {
            try
            {
                taiwanPermit.TbName.Text = string.Empty;
                taiwanPermit.RdobtnMale.IsChecked = false;
                taiwanPermit.RdobtnFemale.IsChecked = false;
                taiwanPermit.DateBirthday.Text = string.Empty;
                taiwanPermit.TbIdentityNo.Text = string.Empty;
                taiwanPermit.TbIdentityNo.Tag = null;//一定要清空 记录每个身份证件的唯一标识
                taiwanPermit.TbVisaAgency.Text = string.Empty;
                taiwanPermit.TbEffectiveDate.Text = string.Empty;
                taiwanPermit.TbExpiryDate.Text = string.Empty;
            }
            catch (Exception e)
            {
                MessageBox.Show("重置数据发生异常：" + e.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 清空军官证信息
        /// </summary>
        /// <param name="certificateOfOfficers"></param>
        private void ClearCertificateOfOfficers(CertificateOfOfficersControl certificateOfOfficers)
        {
            try
            {
                certificateOfOfficers.TbName.Text = string.Empty;
                certificateOfOfficers.RdobtnMale.IsChecked = false;
                certificateOfOfficers.RdobtnFemale.IsChecked = false;
                certificateOfOfficers.DateBirthday.Text = string.Empty;
                certificateOfOfficers.TbNation.Text = string.Empty;
                certificateOfOfficers.TbDepartmental.Text = string.Empty;
                certificateOfOfficers.TbIdentityNo.Text = string.Empty;
                certificateOfOfficers.TbIdentityNo.Tag = null;//一定要清空 记录每个身份证件的唯一标识
                certificateOfOfficers.TbVisaAgency.Text = string.Empty;
                certificateOfOfficers.TbEffectiveDate.Text = string.Empty;
                certificateOfOfficers.TbExpiryDate.Text = string.Empty;
            }
            catch (Exception e)
            {
                MessageBox.Show("重置数据发生异常：" + e.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 清空户口簿
        /// </summary>
        /// <param name="residenceBooklet"></param>
        private void ClearResidenceBooklet(ResidenceBookletControl residenceBooklet)
        {
            try
            {
                residenceBooklet.TbName.Text = string.Empty;
                residenceBooklet.RdobtnMale.IsChecked = false;
                residenceBooklet.RdobtnFemale.IsChecked = false;
                residenceBooklet.DateBirthday.Text = string.Empty;
                residenceBooklet.TbNation.Text = string.Empty;
                residenceBooklet.TbIdentityNo.Text = string.Empty;
                residenceBooklet.TbIdentityNo.Tag = null;//一定要清空 记录每个身份证件的唯一标识
                residenceBooklet.TbAddress.Text = string.Empty;
            }
            catch (Exception e)
            {
                MessageBox.Show("重置数据发生异常：" + e.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #endregion

        #region 清空Old
        /// <summary>
        /// 清空买方身份证信息
        /// </summary>
        private void ClearBuyerIdCardData()
        {
            try
            {
                TblBuyerName.Text = string.Empty;
                TblBuyerGender.Text = string.Empty;
                TblBuyerNation.Text = string.Empty;
                TblBuyerBirthday.Text = string.Empty;
                TblBuyerAddress.Text = string.Empty;
                TblBuyerIdentityNo.Text = string.Empty;
                TblBuyerIdentityNo.Tag = null;//一定要清空 记录每个身份证件的唯一标识
                TblBuyerVisaAgency.Text = string.Empty;
                TblBuyerEffectiveDate.Text = string.Empty;
                TblBuyerExpiryDate.Text = string.Empty;
                TblStr.Text = string.Empty;
                ImgBuyerPhoto.Source = null;
            }
            catch (Exception e)
            {
                MessageBox.Show("重置数据发生异常：" + e.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        /// <summary>
        /// 清空买方护照信息
        /// </summary>
        private void ClearBuyerPassportData()
        {
            try
            {
                TbBuyerName.Text = string.Empty;
                RdoBtnBuyerGenderMale.IsChecked = false;
                RdoBtnBuyerGenderFemale.IsChecked = false;
                TbBuyerBirthday.Text = string.Empty;
                CboBuyerNationality.SelectedValue = -1;
                TbBuyerIdentityNo.Text = string.Empty;
                TbBuyerIdentityNo.Tag = null;//一定要清除,记录每个身份证件的唯一标识
            }
            catch (Exception e)
            {
                MessageBox.Show("重置数据发生异常：" + e.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 清空卖家身份证信息
        /// </summary>
        private void ClearSellerIdCardData()
        {
            try
            {
                TblSellerName.Text = string.Empty;
                TblSellerGender.Text = string.Empty;
                TblSellerNation.Text = string.Empty;
                TblSellerBirthday.Text = string.Empty;
                TblSellerAddress.Text = string.Empty;
                TblSellerIdentityNo.Text = string.Empty;
                TblSellerIdentityNo.Tag = null;//一定要清除,记录每个身份证件的唯一标识
                TblSellerVisaAgency.Text = string.Empty;
                TblSellerEffectiveDate.Text = string.Empty;
                TblSellerExpiryDate.Text = string.Empty;
                TblSellerStr.Text = string.Empty;
                ImgSellerPhoto.Source = null;
            }
            catch (Exception e)
            {
                MessageBox.Show("重置数据发生异常：" + e.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        /// <summary>
        /// 清空卖家护照信息
        /// </summary>
        private void ClearSellerPassportData()
        {
            try
            {
                TbSellerName.Text = string.Empty;
                RdoBtnSellerGenderMale.IsChecked = false;
                RdoBtnSellerGenderFemale.IsChecked = false;
                TbSellerBirthday.Text = string.Empty;
                CboSellerNationality.SelectedValue = -1;
                TbSellerIdentityNo.Text = string.Empty;
                TbSellerIdentityNo.Tag = null; //一定要清除,记录每个身份证件的唯一标识           
            }
            catch (Exception e)
            {
                MessageBox.Show("重置数据发生异常：" + e.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #endregion

        /// <summary>
        /// 快捷键
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
            }
        }

        void AddCasePage_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.F2:
                    btn_BuyerSave.Focus();
                    _idCardReadType = (int)IdCardRead.Buyer;
                    CreatIdCard();
                    break;
                case Key.F3:
                    BtnSellerSave.Focus();
                    _idCardReadType = (int)IdCardRead.Seller;
                    CreatIdCard();
                    break;
                case Key.F1:
                    _idCardReadType = (int)IdCardRead.Agent;
                    CreatIdCard();
                    break;

            }
        }

      
        /// <summary>
        /// 案件来源选择
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CaseSourceCb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
             certificateType = (SourceType)Enum.Parse(typeof(SourceType), Convert.ToString(CaseSourceCb.SelectedIndex));
        }



    }
}
