﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Main.Dialog;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing.Printing;
using PinganHouse.SHHTS.AccessControl.Proxy;

namespace PinganHouse.SHHTS.UI.WPF.Main.PreCheck
{
    /// <summary>
    /// CheckCaseSearchPage.xaml 的交互逻辑
    /// </summary>
    //[SecurityAuthorize]
    public partial class PreCheckSearchListPage : Page
    {
        /// <summary>
        /// 打印机列表
        /// </summary>
        private List<PrintModel> _ListPrint = new List<PrintModel>();

        /// <summary>
        /// 打印机名字
        /// </summary>
        private string _ReceiptPrintName;

        #region 公共变量
        private int PageSize = 10;
        /// <summary>
        /// 预检台状态
        /// </summary>
        CaseStatus status = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.ZB2
            | CaseStatus.ZB5 | CaseStatus.ZB6 | CaseStatus.ZB7 | CaseStatus.ZB13;
        /// <summary>
        /// 窗体
        /// </summary>
        private Window window = null;
        private ScanBarCodeDialog scanBarCodeDialog = new ScanBarCodeDialog();
        #endregion

        /// <summary>
        /// 默认构造
        /// </summary>
        public PreCheckSearchListPage()
        {
            InitializeComponent();
            this.GridPager.PageNumChanged = GridPager_PagerIndexChanged;
            //获取案件状态
            var list = EnumHelper.InitCaseStatusToCombobox(status);
            caseStatusCb.ItemsSource = list;
            caseStatusCb.DisplayMemberPath = "DisplayMember";
            caseStatusCb.SelectedValuePath = "ValueMember";
            caseStatusCb.SelectedValue = 0;

            #region 获得本机所有打印机
            var printers = PrinterSettings.InstalledPrinters;//获取本机上的所有打印机
            //PrintModel pm1 = new PrintModel();
            //pm1.PrintNo = -1;
            //pm1.PrintName = "请选择打印机";
            //_ListPrint.Add(pm1);
            PrintModel pm = new PrintModel();
            pm.PrintNo = 0;
            pm.PrintName = "未分配";
            _ListPrint.Add(pm);

            for (int i = 0; i < printers.Count; i++)
            {
                PrintModel print = new PrintModel();
                print.PrintNo = i + 1;
                print.PrintName = printers[i].ToString();
                _ListPrint.Add(print);
            }
            #endregion
            //从注册表加载 条形码打印机信息          
            _ReceiptPrintName = PrivateProfileUtils.GetContentValue("BarCodePrint", "PrintName"); //打印机名

        }

        #region 页面数据取得方法
        /// <summary>
        /// 异步获取系统数据
        /// </summary>
        private async void GetDataToBindCtrl()
        {
            try
            {
                //案件编号
                var caseId = string.IsNullOrEmpty(this.txtCaseId.Text.Trim()) ? null : this.txtCaseId.Text.Trim();
                //案件地址
                var address = string.IsNullOrEmpty(this.txtAddress.Text.Trim()) ? null : this.txtAddress.Text.Trim();
                //卖家
                var buyerName = string.IsNullOrEmpty(this.txtBuyer.Text.Trim()) ? null : this.txtBuyer.Text.Trim();
                //经纪人
                var agencyName = string.IsNullOrEmpty(this.txtAgency.Text.Trim()) ? null : this.txtAgency.Text.Trim();
                //创建人
                var createrName = string.IsNullOrEmpty(this.txtCreater.Text.Trim()) ? null : this.txtCreater.Text.Trim();
                //开始时间
                DateTime startTime = DateTime.MinValue;
                if (!string.IsNullOrEmpty(dateStart.Text))
                {
                    DateTime.TryParse(dateStart.Text, out startTime);
                }
                //结束时间
                DateTime endTime = DateTime.MinValue;
                if (!string.IsNullOrEmpty(dateEnd.Text))
                {
                    DateTime.TryParse(dateEnd.Text, out endTime);
                }
                //案件状态选择
                var caseStatus = (CaseStatus)Enum.Parse(typeof(CaseStatus), Convert.ToString(caseStatusCb.SelectedValue));
                CaseStatus? selectStatus = caseStatus;
                if (caseStatus == CaseStatus.Unkown) { selectStatus = null; }
                var tupleModel = await GetDataGridData(caseId, address, buyerName, agencyName, startTime, endTime, selectStatus, createrName);
                this.dataGrid.DataContext = tupleModel.Item2;
                GridPager.Visibility = Visibility.Visible;
                if (tupleModel.Item1 % PageSize != 0){
                    GridPager.PageCount = tupleModel.Item1 / PageSize+1;
                }
                else { GridPager.PageCount = tupleModel.Item1 / PageSize; }
                this.loading.IsBusy = false;
            }
            catch (Exception)
            {
                this.loading.IsBusy = false;
                MessageBox.Show("加载数据发生系统异常.", "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        /// <summary>
        /// 获取页面数据
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="size"></param>
        /// <param name="caseId"></param>
        /// <param name="address"></param>
        /// <param name="buyerName"></param>
        /// <param name="agencyName"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        private Task<Tuple<int, List<CastModel>>> GetDataGridData(string caseId, string address, string buyerName, string agencyName,
            DateTime startTime, DateTime endTime, CaseStatus? selectStatus, string createrName)
        {
            return Task.Run(() =>
            {
                int cnt = 0;
                var caseModelList = new List<CastModel>();
                if (startTime == DateTime.MinValue && endTime == DateTime.MinValue)
                {
                    var caseList = CaseProxyService.GetInstanse().GetPaginatedList(GridPager.PageIndex, PageSize, out cnt
                        , ConfigHelper.GetCurrentReceptionCenter(), status, selectStatus
                        , caseId, address, buyerName, agencyName, operationStatus: CaseStatus.HA1, creatorName: createrName);
                    if (caseList != null)
                    {
                        caseModelList = CastModel.InitData(caseList,CaseStatus.HA1);
                    }
                }
                else if (startTime != DateTime.MinValue && endTime != DateTime.MinValue)
                {
                    var caseList = CaseProxyService.GetInstanse().GetPaginatedList(GridPager.PageIndex, PageSize, out cnt
                        , ConfigHelper.GetCurrentReceptionCenter(), status, selectStatus
                        , caseId, address, buyerName, agencyName, startTime, endTime,
                        operationStatus: CaseStatus.HA1, creatorName: createrName);
                    if (caseList != null)
                    {
                        caseModelList = CastModel.InitData(caseList, CaseStatus.HA1);
                    }
                }
                else if (startTime != DateTime.MinValue && endTime == DateTime.MinValue)
                {
                    var caseList = CaseProxyService.GetInstanse().GetPaginatedList(GridPager.PageIndex, PageSize, out cnt
                        , ConfigHelper.GetCurrentReceptionCenter(), status, selectStatus
                        , caseId, address, buyerName, agencyName, startTime, operationStatus: CaseStatus.HA1, creatorName: createrName);
                    if (caseList != null)
                    {
                        caseModelList = CastModel.InitData(caseList, CaseStatus.HA1);
                    }
                }
                else if (startTime == DateTime.MinValue && endTime != DateTime.MinValue)
                {
                    var caseList = CaseProxyService.GetInstanse().GetPaginatedList(GridPager.PageIndex, PageSize, out cnt
                        , ConfigHelper.GetCurrentReceptionCenter(), status, selectStatus
                        , caseId, address, buyerName, agencyName, null, endTime, operationStatus: CaseStatus.HA1, creatorName: createrName);
                    if (caseList != null)
                    {
                        caseModelList = CastModel.InitData(caseList, CaseStatus.HA1);
                    }
                }
                var dicResult=new Tuple<int,List<CastModel>>(cnt,caseModelList);
                return dicResult;
            });
        }
        #endregion
 
        #region 页面按钮事件

        /// <summary>
        /// 新增案件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [Permission("10002")]
        private void btnAddCase_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.PageNavigateHelper(this, "Main/PreCheck/AddCasePage.xaml");
        }

        /// <summary>
        /// 查询搜索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchBtn_Click(object sender, RoutedEventArgs e)
        {
            GridPager.PageIndex = 1;
            //GridPager_PagerIndexChanged(sender,e);
        }

        /// <summary>
        /// 添加附件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AttachmentBtn_Click(object sender, RoutedEventArgs e)
        {
            //案件编号
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            //获取案件状态
            var status = CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.HA3 | CaseStatus.HA4 | CaseStatus.YJ1 | CaseStatus.ZB2;
            var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseIdAndCaseStatus(caseId, status);
            var detailFlag = 1;
            if (caseDto != null)
            {
                switch (caseDto.CaseStatus)
                {
                    case CaseStatus.HA1:
                    case CaseStatus.YJ1:
                        detailFlag = 2;
                        break;
                    default:
                        detailFlag = 1;
                        break;
                }
            }
            var attachments = new UploadAttachment(caseId, detailFlag);
            PageHelper.PageNavigateHelper(this, attachments);
        }

        /// <summary>
        /// 打印条码
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PrintCode_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                #region 条形码打印机信息
                int printCount = 0;

                if (!string.IsNullOrWhiteSpace(_ReceiptPrintName))
                {
                    for (int j = 0; j < _ListPrint.Count; j++)
                    {
                        if (_ReceiptPrintName == _ListPrint[j].PrintName)
                        {
                            printCount++;
                        }
                    }
                }
                else //为空，则 收据打印机未分配
                {
                    MessageBox.Show("该条形码打印机未设置，无法进行打印操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (printCount == 0)
                {
                    MessageBox.Show("该条形码打印机未设置，无法进行打印操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (_ReceiptPrintName.Contains("未分配"))
                {
                    MessageBox.Show("该条形码打印机未设置，无法进行打印操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                #endregion

                //案件编号
                var caseId = Convert.ToString(((Button)sender).CommandParameter);
                var icp = new CaseCodePrint();
                icp.PrintCaseBarCode(caseId);
            }
            catch (Exception ex) {
                MessageBox.Show("打印错误："+ex.Message,"消息提示",MessageBoxButton.OK,MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 取消签约
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZB2_Click(object sender, RoutedEventArgs e)
        {
            //案件编号
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            var cancelContractDialog = new CancelContractDialog(caseId);
            bool? dialogResult = cancelContractDialog.ShowDialog();
            if (dialogResult.HasValue && dialogResult.Value)
            {
                var dic = new Dictionary<string, object>();
                dic.Add("Reason", cancelContractDialog.Reason);
                var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.ZB5, LoginHelper.CurrentUser.SysNo, dic);
                if (result.Success)
                {
                    MessageBox.Show("取消签约成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    GridPager_PagerIndexChanged(this.GridPager.PageIndex);
                }
                else
                {
                    MessageBox.Show("取消签约失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        #endregion

        /// <summary>
        /// 分页事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridPager_PagerIndexChanged(int pageIndex)
        {
            PageHelper.PageActiveLoadingControl(this.loading, "正在加载数据,请稍后...");
            this.GetDataToBindCtrl();
        }

        /// <summary>
        /// 案件跳转填写明细
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var link = e.Source as Hyperlink;
                if (null != link)
                {
                    var sStatus = CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.HA3 | CaseStatus.HA4 | CaseStatus.YJ1 | CaseStatus.ZB2;
                    var caseId = Convert.ToString(link.NavigateUri);
                    //var caseInfo = new UpdateCasePage(caseId);
                    //PageHelper.PageNavigateHelper(this, caseInfo);
                    var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseIdAndCaseStatus(caseId, sStatus);
                    if (caseDto != null)
                    {
                        switch (caseDto.CaseStatus)
                        {
                            case CaseStatus.HA1:
                            case CaseStatus.YJ1:
                                PageHelper.PageNavigateHelper(this, new UpdateCasePage(caseId));
                                break;
                            default:
                                PageHelper.PageNavigateHelper(this, new CheckCaseDetailInfo(caseId));
                                break;
                        }
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("显示明细发生错误", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            e.Handled = true;
        }

        /// <summary>
        /// Load 加载数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Body_Loaded(object sender, RoutedEventArgs e)
        {
            GridPager.PageIndex = 1;
            //GridPager_PagerIndexChanged(sender, e);
            Window window = Utils.GetParentWindow(this);
            this.window = window;
            window.KeyDown += this.onKeyDown;
        }

        /// <summary>
        /// 双击Grid事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var item = this.dataGrid.CurrentItem;
                if (item != null)
                {
                    var caseId = ((CaseDto)item).CaseId;
                    var caseInfo = new UpdateCasePage(caseId);
                    PageHelper.PageNavigateHelper(this, caseInfo);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("显示明细发生错误", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        /// <summary>
        /// 快捷键
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                string keyBar = new ScanBarCodeDialog().showScanBarCodeDialog(Utils.GetParentWindow(this));
                txtCaseId.Text = keyBar;
                SearchBtn_Click(null, null);
            }
        }
        /// <summary>
        /// 页面离开触发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Body_Unloaded(object sender, RoutedEventArgs e)
        {
            if (window != null)
            {
                window.KeyDown -= this.onKeyDown;
            }
        }

        private void WeichatBtn_OnClick(object sender, RoutedEventArgs e)
        {
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            new CustomersListDialog(caseId).ShowDialog();
        }
    }
}
