﻿using System;
using System.Collections.Generic;
using PinganHouse.SHHTS.Enumerations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Main.Nuclear;
using PinganHouse.SHHTS.UI.WPF.Model;
using PinganHouse.SHHTS.UI.WPF.Main.PreCheck;
using PinganHouse.SHHTS.UI.WPF.Control;
using PinganHouse.SHHTS.DataTransferObjects;

namespace PinganHouse.SHHTS.UI.WPF.Main.PreCheck
{
    /// <summary>
    /// OperationInfos.xaml 的交互逻辑
    /// </summary>
    public partial class OperationInfos : Page
    {
        #region 全局变量
        /// <summary>
        /// 案件编号
        /// </summary>
        private string _id;

        /// <summary>
        /// 状态
        /// </summary>
        private CaseStatus _status;

        /// <summary>
        /// detailsFlag：0，核案；1，查看预检；2，修改预检。
        /// </summary>
        private int _detailsFlag = 0;

        #endregion

        public OperationInfos(string id, CaseStatus status, int detailsFlag = 0)
        {
            InitializeComponent();
            _id = id;
            _status = status;
            _detailsFlag = detailsFlag;

            ctrl_Navigation.BasicInfoEvent += ctrl_Navigation_BasicInfoEvent;
            ctrl_Navigation.OperationEvent += ctrl_Navigation_OperationEvent;
            ctrl_Navigation.UploadEvent += ctrl_Navigation_UploadEvent;
            ctrl_Navigation.AttachmentEvent += ctrl_Navigation_AttachmentEvent;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            InitInfo();
        }

        #region 自定义导航事件
        /// <summary>
        /// 基本信息跳转
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ctrl_Navigation_BasicInfoEvent(object sender, EventArgs e)
        {
            try
            {
                switch (_detailsFlag)
                {
                    case 1:
                        PageHelper.PageNavigateHelper(this, new CheckCaseDetailInfo(_id));
                        break;
                    case 2:
                        PageHelper.PageNavigateHelper(this, new UpdateCasePage(_id));
                        break;
                    default:
                        PageHelper.PageNavigateHelper(this, new CaseDetailInfo(_id));
                        break;
                }

                #region 作废
                //var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseIdAndCaseStatus(_id,
                //    CaseStatus.YJ1 | CaseStatus.ZB2 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.HA3 | CaseStatus.HA4);
                //if (caseDto != null)
                //{
                //    switch (caseDto.CaseStatus)
                //    {
                //        case CaseStatus.HA1:
                //            PageHelper.PageNavigateHelper(this, new UpdateCasePage(_id));
                //            break;
                //        case CaseStatus.HA2:
                //            PageHelper.PageNavigateHelper(this, new CheckCaseDetailInfo(_id));
                //            break;
                //        case CaseStatus.HA3:
                //            PageHelper.PageNavigateHelper(this, new CaseDetailInfo(_id));
                //            break;
                //        case CaseStatus.YJ1:
                //            PageHelper.PageNavigateHelper(this, new UpdateCasePage(_id));
                //            break;
                //        case CaseStatus.ZB2:
                //            PageHelper.PageNavigateHelper(this, new CheckCaseDetailInfo(_id));
                //            break;
                //        default:
                //            PageHelper.PageNavigateHelper(this, new CaseDetailInfo(_id));
                //            break;
                //    }
                //}
                #endregion
            }
            catch (Exception)
            {
                MessageBox.Show("页面跳转发生系统异常.", "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        /// <summary>
        /// 操作信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ctrl_Navigation_OperationEvent(object sender, EventArgs e)
        {
            var status = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.ZB2;
            var attachments = new OperationInfos(_id, status, _detailsFlag);
            PageHelper.PageNavigateHelper(this, attachments);
        }
        /// <summary>
        /// 上传页面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ctrl_Navigation_UploadEvent(object sender, EventArgs e)
        {
            var attachments = new UploadAttachment(_id, _detailsFlag);
            PageHelper.PageNavigateHelper(this, attachments);
        }
        void ctrl_Navigation_AttachmentEvent(object sender, EventArgs e)
        {
            var attachments = new CaseAttachmentsInfo(_id, _detailsFlag);
            PageHelper.PageNavigateHelper(this, attachments);
        }
        #endregion


        /// <summary>
        /// 初始化数据
        /// </summary>
        private void InitInfo()
        {
            TbCaseId.DataContext = _id;
            try
            {
                var results = CaseProxyService.GetInstanse().GetCaseEvents(_id, null, null).OrderBy(x => x.CreateDate).ToList();
                //IList<CaseEvent> tempResult = new List<CaseEvent>();
                //List<CaseStatus> statusList = new List<CaseStatus>();
                //foreach(CaseEvent ce in results)
                //{
                //    if (!statusList.Contains(ce.EventType))
                //    {
                //        statusList.Add(ce.EventType);
                //        tempResult.Add(ce);
                //    }
                //}

                //this.m_CaseShowAllControl.DataContext = new CaseStatusModel(tempResult);
                this.m_CaseShowAllControl.DataContext = new CaseStatusModel(results);
                this.m_CaseShowAllControl.AllStatus = (CaseStatusModel)this.m_CaseShowAllControl.DataContext;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        /// <summary>
        /// 转换状态类型
        /// </summary>
        /// <param name="status"></param>
        private string  GetName(CaseStatus status)
        {
            string name;
            switch (status)
                    {
                        case CaseStatus.Unkown:
                            name = null;
                            break;
                        case CaseStatus.YJ1:
                            name = "等待签约";
                            break;
                        case CaseStatus.HA1:
                            name = "签约中";
                            break;
                        case CaseStatus.HA2:
                            name = "已签约";
                            break;
                        case CaseStatus.ZB2:
                            name = "取消签约";
                            break;
                        default:
                            name = null;
                        break;
                    }
            return name;
        }

        #region 顶部按钮跳转

        private void InfoBtn_Click(object sender, RoutedEventArgs e)
        {
            var attachments = new UpdateCasePage(_id);
            PageHelper.PageNavigateHelper(this, attachments);
        }

        private void ViewBtn_Click(object sender, RoutedEventArgs e)
        {
            var attachments = new CaseAttachmentsInfo(_id);
            PageHelper.PageNavigateHelper(this, attachments);
        }

        private void UploadBtn_Click(object sender, RoutedEventArgs e)
        {
            var attachments = new UploadAttachment(_id);
            PageHelper.PageNavigateHelper(this, attachments);
        }
        #endregion

        #region 页面展示类
        /// <summary>
        /// 页面展示类
        /// </summary>
        public class Infos
        {
            public string Name { get; set; }
            public string Details { get; set; }
        }
        #endregion

    }
}
