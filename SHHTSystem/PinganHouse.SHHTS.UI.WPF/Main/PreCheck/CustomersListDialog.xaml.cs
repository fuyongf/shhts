﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Main.Dialog;
using PinganHouse.SHHTS.UI.WPF.Main.Nuclear;
using PinganHouse.SHHTS.UI.WPF.Model;

namespace PinganHouse.SHHTS.UI.WPF.Main.PreCheck
{
    /// <summary>
    /// CustomersListDialog.xaml 的交互逻辑
    /// </summary>
    public partial class CustomersListDialog : Window
    {
        private Seller seller = null;
        private Buyer buyer = null;

        public CustomersListDialog(string caseId)
        {
            InitializeComponent();
            Tuple<Seller, Buyer> sb = CaseProxyService.GetInstanse().GetCaseCustomers(caseId);
            if (sb != null) seller = sb.Item1;
            if (sb != null) buyer = sb.Item2;
        }

        private void RefreshBuyersInfo()
        {
            BuyersInfoStackPanel.Children.Clear();
            if (buyer == null)
            {
                return;
            }

            int lineNum = 0;
            foreach (Customer customer in buyer.Customers)
            {
                lineNum++;
                StackPanel sp = new StackPanel();
                sp.Margin = new Thickness(0, 2, 0, 0);
                sp.Orientation = Orientation.Horizontal;

                Button btn = new Button();
                btn.Focusable = false;
                btn.Style = (Style) FindResource("NoMouseOverButtonStyle");
                btn.Width = 21;
                btn.Height = 21;
                btn.BorderThickness = new Thickness(0);
                btn.CommandParameter = customer.UserSysNo;
                btn.Click += this.weichatBtnClicked;
                ImageBrush berriesBrush = new ImageBrush();
                berriesBrush.ImageSource =
                    new BitmapImage(
                        new Uri("Images/weichatico1.png", UriKind.Relative)
                        );
                btn.Background = berriesBrush;
                btn.Margin = new Thickness(5,0,5,0);
                sp.Children.Add(btn);

                TextBlock tb = new TextBlock();
                tb.Text = lineNum + ". " + customer.RealName + " | " +
                          EnumHelper.GetEnumDesc(customer.Gender) + " | " +
                          (EnumHelper.GetEnumDesc(customer.CertificateType)) + " | 证件号码：" +
                          customer.IdentityNo +
                          (customer.CertificateType == CertificateType.IdCard
                              ? (" | 住址：" + customer.Nationality)
                              : (" | 国籍：" + customer.Nationality)) + " | 手机："+customer.Mobile+" | 邮箱："+customer.Email;
                
                sp.Children.Add(tb);
            
                BuyersInfoStackPanel.Children.Add(sp);

            }
        }

        private void RefreshSellersInfo()
        {
            SellersInfoStackPanel.Children.Clear();
            if (seller == null)
            {
                return;
            }

            int lineNum = 0;
            foreach (Customer customer in seller.Customers)
            {
                lineNum++;
                StackPanel sp = new StackPanel();
                sp.Margin = new Thickness(0, 2, 0, 0);
                sp.Orientation = Orientation.Horizontal;

                Button btn = new Button();
                btn.Focusable = false;
                btn.Style = (Style) FindResource("NoMouseOverButtonStyle");
                btn.Width = 21;
                btn.Height = 21;
                btn.BorderThickness = new Thickness(0);
                btn.CommandParameter = customer.UserSysNo;
                btn.Click += this.weichatBtnClicked;
                ImageBrush berriesBrush = new ImageBrush();
                berriesBrush.ImageSource =
                    new BitmapImage(
                        new Uri("Images/weichatico1.png", UriKind.Relative)
                        );
                btn.Background = berriesBrush;
                btn.Margin = new Thickness(5, 0, 5, 0);
                sp.Children.Add(btn);

                TextBlock tb = new TextBlock();
                tb.Text = lineNum + ". " + customer.RealName + " | " +
                          EnumHelper.GetEnumDesc(customer.Gender) + " | " +
                          (EnumHelper.GetEnumDesc(customer.CertificateType)) + " | 证件号码：" +
                          customer.IdentityNo +
                          (customer.CertificateType == CertificateType.IdCard
                              ? (" | 住址：" + customer.Nationality)
                              : (" | 国籍：" + customer.Nationality)) + " | 手机："+customer.Mobile+" | 邮箱："+customer.Email;
                sp.Children.Add(tb);
                SellersInfoStackPanel.Children.Add(sp);
            }

        }
        private void weichatBtnClicked(object sender, RoutedEventArgs e)
        {
            string userSysno = ((Button)sender).CommandParameter.ToString();
            new ScanWeiChatDialog(userSysno).ShowDialog();
        }

        private void CustomersListDialog_OnLoaded(object sender, RoutedEventArgs e)
        {
            RefreshBuyersInfo();
            RefreshSellersInfo();
        }
    }
}
