﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
//using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main
{
    /// <summary>
    /// Page1.xaml 的交互逻辑
    /// </summary>
    public partial class Page1 : Page
    {
        private static readonly Font _Font = new Font("微软雅黑", 12, GraphicsUnit.Pixel);

        public Page1()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //// printDocument1 为 打印控件

            ////设置打印用的纸张 当设置为Custom的时候，可以自定义纸张的大小，还可以选择A4,A5等常用纸型
            //PrintDocument pd = new PrintDocument();
            //pd.DefaultPageSettings.PaperSize = new PaperSize("Custum", 500, 300);

            //pd.PrintPage += new PrintPageEventHandler(this.MyPrintDocument_PrintPage);

            ////将写好的格式给打印预览控件以便预览

            ////printPreviewDialog1.Document = printDocument1;

            ////显示打印预览

            ////DialogResult result = printPreviewDialog1.ShowDialog();

            ////if (result == DialogResult.OK)

            //pd.Print();

            PrintDialog dialog = new PrintDialog();
            if (dialog.ShowDialog() == true)
            {
                dialog.PrintVisual(grid1, "Print Test");
            }

        }

        private void MyPrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {

            /*如果需要改变自己 可以在new Font(new FontFamily("黑体"),11）中的“黑体”改成自己要的字体就行了，黑体 后面的数字代表字体的大小

             System.Drawing.Brushes.Blue , 170, 10 中的 System.Drawing.Brushes.Blue 为颜色，后面的为输出的位置 ，第一个10是左边距，第二个35是上边距*/

            e.Graphics.DrawString(t1.Text, _Font, System.Drawing.Brushes.Black, 10, 35);

            e.Graphics.DrawString(t1.Text, _Font, System.Drawing.Brushes.Black, 30, 35);

            e.Graphics.DrawString(t1.Text, _Font, System.Drawing.Brushes.Black, 50, 35);

        }

    }
}
