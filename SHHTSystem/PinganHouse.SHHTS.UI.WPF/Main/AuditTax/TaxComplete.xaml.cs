﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Main;
using PinganHouse.SHHTS.UI.WPF.Main.AuditTax;
using PinganHouse.SHHTS.UI.WPF.Model;

namespace PinganHouse.SHHTS.UI.WPF
{
	/// <summary>
	/// TaxComplete.xaml 的交互逻辑
	/// </summary>
	public partial class TaxComplete : Window
	{
        public delegate void SaveHandle();
        public event SaveHandle SaveEvent;

        private String _caseId = String.Empty;

		public TaxComplete(String caseId)
		{
			
            this.InitializeComponent();

            _caseId = caseId;
            var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(caseId);
            var caseEvent = CaseProxyService.GetInstanse().GetCaseEvents(caseId, CaseStatus.HA2, null);
            var caseRemark = CaseProxyService.GetInstanse().GetRemarkBySysNo(caseDto.SysNo);

            grdFrom.DataContext = caseDto;
            lblSigningDate.Content = caseEvent.OrderByDescending(e => e.CreateDate).First().CreateDate.ToString("yyyy-MM-dd HH:mm");

            if (caseRemark != null)
            {
                var netlabelPrice = CaseProxyService.GetInstanse().GetRemarkBySysNo(caseDto.SysNo).NetlabelPrice;
                decimal price = 0;
                if (netlabelPrice != null)
                {
                    decimal.TryParse(netlabelPrice.ToString(), out price);
                }
                lblPrice.Content = price.ToString("C");
            }

            var caseJY2Datas = CaseProxyService.GetInstanse().GetCaseEvents(caseId, CaseStatus.JY2, null).OrderByDescending(e => e.CreateDate).First().OtherDatas;
            if (caseJY2Datas["CheckPriceResult"].ToString() == "不通过")
            {
                spCheckPrice.Visibility = Visibility.Visible;
                lblCheckPrice.Content = Convert.ToDecimal(caseJY2Datas["CheckPrice"]).ToString("C");
            }
		}

        private void BtnCancel_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnSave_OnClick(object sender, RoutedEventArgs e)
        {
            OperationResult result = null;
            
            Decimal price = 0;
            if (String.IsNullOrEmpty(txtPrice.Text))
            {
                MessageBox.Show("请填写税单总额，且税费总额要大于0！", "警告", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            else if (!String.IsNullOrEmpty(txtPrice.Text))
            {
                if (!Decimal.TryParse(txtPrice.Text.Trim(), out price) || price < 0)
                {
                    MessageBox.Show("请填写税单总额，且税费总额要大于0！", "警告", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
            }


            Dictionary<String, object> dict = new Dictionary<string, object>();
            dict.Add("Price", price);


            result = CaseProxyService.GetInstanse()
                .UpdateCaseStatus(_caseId, CaseStatus.JY10, LoginHelper.CurrentUser.SysNo, dict);



            if (result.Success)
            {

                SaveEvent();
                this.Close();
            }
            else
            {
                MessageBox.Show(result.ResultMessage);
            }
        }

	    private void BtnUpload_OnClick(object sender, RoutedEventArgs e)
	    {
	        SelectAttachmentType selectAttachment = new SelectAttachmentType(_caseId, 0, "TaxComplete");
	        selectAttachment.ShowDialog();
	    }

        /// <summary>
        /// 高拍仪上传按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCamera_OnClick(object sender, RoutedEventArgs e)
        {
            SelectAttachmentType selectAttachment = new SelectAttachmentType(_caseId, 1, "TaxComplete");
            selectAttachment.ShowDialog();
        }
	}
}