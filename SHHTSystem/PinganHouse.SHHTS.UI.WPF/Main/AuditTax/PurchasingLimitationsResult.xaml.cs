﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Main;
using PinganHouse.SHHTS.UI.WPF.Model;

namespace PinganHouse.SHHTS.UI.WPF
{
	/// <summary>
	/// PurchasingLimitationsResult.xaml 的交互逻辑
	/// </summary>
	public partial class PurchasingLimitationsResult : Window
	{
        public delegate void SaveHandle();
        public event SaveHandle SaveEvent;

        private String _caseId = String.Empty;

		public PurchasingLimitationsResult(String caseId)
		{
			this.InitializeComponent();

            _caseId = caseId;
            var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(caseId);
            var caseEvent = CaseProxyService.GetInstanse().GetCaseEvents(caseId, CaseStatus.HA2, null);
            var caseRemark = CaseProxyService.GetInstanse().GetRemarkBySysNo(caseDto.SysNo);

            grdFrom.DataContext = caseDto;
            lblSigningDate.Content = caseEvent.OrderByDescending(e => e.CreateDate).First().CreateDate.ToString("yyyy-MM-dd HH:mm");

            if (caseRemark != null)
            {
                var netlabelPrice = CaseProxyService.GetInstanse().GetRemarkBySysNo(caseDto.SysNo).NetlabelPrice;
                decimal price = 0;
                if (netlabelPrice != null)
                {
                    decimal.TryParse(netlabelPrice.ToString(), out price);
                }
                lblPrice.Content = price.ToString("C");
            }

            var caseJY2Datas = CaseProxyService.GetInstanse().GetCaseEvents(caseId, CaseStatus.JY2, null).OrderByDescending(e => e.CreateDate).First().OtherDatas;
            if (caseJY2Datas["CheckPriceResult"].ToString() == "不通过")
            {
                spCheckPrice.Visibility = Visibility.Visible;
                lblCheckPrice.Content = Convert.ToDecimal(caseJY2Datas["CheckPrice"]).ToString("C");
            }
		}

        private void BtnCancel_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnSave_OnClick(object sender, RoutedEventArgs e)
        {
            OperationResult result = null;
            var remark = txtRemark.Text;
            var checkResult = String.Empty;
            Dictionary<String, object> dict = new Dictionary<string, object>();
            dict.Add("Remark", remark);
            
            

            if (rdoPass.IsChecked == true)
            {
                //不限购
                checkResult = rdoPass.Content.ToString();
                dict.Add("CheckResult", checkResult);
                result = CaseProxyService.GetInstanse()
                .UpdateCaseStatus(_caseId, CaseStatus.JY6, LoginHelper.CurrentUser.SysNo, dict);

            }
            else if (rdoFail.IsChecked == true)
            {
                //限购
                checkResult = rdoFail.Content.ToString();
                dict.Add("CheckResult", checkResult);
                result = CaseProxyService.GetInstanse()
                .UpdateCaseStatus(_caseId, CaseStatus.JY9, LoginHelper.CurrentUser.SysNo, dict);

            }

            if (String.IsNullOrEmpty(checkResult))
            {
                MessageBox.Show("请选择限购结果！", "警告", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            
            
            if (result.Success)
            {
                
                SaveEvent();
                this.Close();
            }
            else
            {
                MessageBox.Show(result.ResultMessage);
            }
        }

	    private void BtnUpload_OnClick(object sender, RoutedEventArgs e)
	    {
            try
            {
                var upload = new UploadFileHelper();
                var fileNum = 0;
                upload.UploadFile(_caseId, AttachmentType.限购查询结果, out fileNum);
            }
            catch (Exception ex)
            {
                MessageBox.Show("上传附件操作发生系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
	    }

        /// <summary>
        /// 高拍仪上传按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCamera_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var captruePage = new CaptureImgeDialog(_caseId, AttachmentType.限购查询结果);
                captruePage.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("上传文件操作发生异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
	}
}