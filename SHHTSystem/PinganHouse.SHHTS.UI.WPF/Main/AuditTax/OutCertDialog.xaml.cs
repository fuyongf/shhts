﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;

namespace PinganHouse.SHHTS.UI.WPF.Main.AuditTax
{
    /// <summary>
    /// OutCertDialog.xaml 的交互逻辑
    /// </summary>
    public partial class OutCertDialog : Window
    {
        public delegate void SaveHandle();
        public event SaveHandle SaveEvent;

        private String _caseId = String.Empty;


        public OutCertDialog(String caseId)
        {
            InitializeComponent();

            _caseId = caseId;
            var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(caseId);
            var caseEvent = CaseProxyService.GetInstanse().GetCaseEvents(caseId, CaseStatus.HA2, null);
            
            grdFrom.DataContext = caseDto;
            lblSigningDate.Content = caseEvent.OrderByDescending(e => e.CreateDate).First().CreateDate.ToString("yyyy-MM-dd HH:mm");

        }

        private void BtnCancel_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnSave_OnClick(object sender, RoutedEventArgs e)
        {
            OperationResult result = null;
            var remark = txtRemark.Text;
            Dictionary<String, object> dict = new Dictionary<string, object>();
            dict.Add("Remark", remark);
            
            result = CaseProxyService.GetInstanse()
                .UpdateCaseStatus(_caseId, CaseStatus.JY12, LoginHelper.CurrentUser.SysNo, dict);


            if (result.Success)
            {

                SaveEvent();
                this.Close();
            }
            else
            {
                MessageBox.Show(result.ResultMessage);
            }
        }

        private void BtnUpload_OnClick(object sender, RoutedEventArgs e)
        {
            SelectAttachmentType selectAttachment = new SelectAttachmentType(_caseId, 0, "OutCertDialog");
            selectAttachment.ShowDialog();
        }

        /// <summary>
        /// 高拍仪上传按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCamera_OnClick(object sender, RoutedEventArgs e)
        {
            SelectAttachmentType selectAttachment = new SelectAttachmentType(_caseId, 1, "OutCertDialog");
            selectAttachment.ShowDialog();
        }
    }
}
