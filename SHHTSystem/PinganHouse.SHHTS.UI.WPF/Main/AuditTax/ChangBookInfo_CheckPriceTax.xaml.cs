﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;

namespace PinganHouse.SHHTS.UI.WPF.Main.AuditTax
{
    /// <summary>
    /// ChangBookInfo_CheckPriceTax.xaml 的交互逻辑
    /// </summary>
    public partial class ChangBookInfo_CheckPriceTax : Window
    {
        private String _caseId = String.Empty;

        
        public ChangBookInfo_CheckPriceTax(String caseId)
        {
            InitializeComponent();

            var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(caseId);
            var caseEvent = CaseProxyService.GetInstanse().GetCaseEvents(caseId, CaseStatus.HA2, null);
            

            grdFrom.DataContext = caseDto;
            lblSigningDate.DataContext = caseEvent;


        }
    }
}
