﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Main;
using PinganHouse.SHHTS.UI.WPF.Model;

namespace PinganHouse.SHHTS.UI.WPF
{
    /// <summary>
    /// UploadReceipt.xaml 的交互逻辑
    /// </summary>
    public partial class UploadReceipt : Window
    {
        public delegate void SaveHandle();
        public event SaveHandle SaveEvent;

        private String _caseId = String.Empty;

        public UploadReceipt(String caseId)
        {
            this.InitializeComponent();

            _caseId = caseId;
            var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(caseId);
            var caseEvent = CaseProxyService.GetInstanse().GetCaseEvents(caseId, CaseStatus.HA2, null);

            grdFrom.DataContext = caseDto;
            lblSigningDate.Content = caseEvent.OrderByDescending(e => e.CreateDate).First().CreateDate.ToString("yyyy-MM-dd HH:mm");

            IList<string> fundCentreList = new List<string>();
            foreach (var centre in ConfigServiceProxy.GetProvidentFundAddress())
            {
                fundCentreList.Add(centre);
            }
            fundCentreList.Insert(0,"请选择");
            cmbFundCentre.ItemsSource = fundCentreList;
            cmbFundCentre.SelectedIndex = 0;


        }
        private void BtnCancel_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnSave_OnClick(object sender, RoutedEventArgs e)
        {
            OperationResult result = null;
            var remark = txtRemark.Text;

            var date = datePicker.Text;
            if (String.IsNullOrEmpty(date))
            {
                MessageBox.Show("请选择领产证时间！", "警告", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            Dictionary<String, object> dict = new Dictionary<string, object>();
            dict.Add("Remark", remark);
            dict.Add("ReceiveDate", date);


            if (rdoBuyer.IsChecked == false && rdoBank.IsChecked == false && rdoGjj.IsChecked == false && rdoTrader.IsChecked == false)
            {
                MessageBox.Show("请选择领证人！", "警告", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            var checkResult = new DelegationType();
            var fundCentre = "";
            if (rdoBuyer.IsChecked == true)
            {
                checkResult = DelegationType.Self;

            }
            else if (rdoBank.IsChecked == true)
            {
                checkResult = DelegationType.LoanBank;
            }
            else if (rdoGjj.IsChecked == true)
            {
                checkResult = DelegationType.Bourse;
                fundCentre = cmbFundCentre.SelectedItem.ToString();
                if (fundCentre == "请选择")
                {
                    MessageBox.Show("请选择公积金中心！", "提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
                dict.Add("FundCentre", fundCentre);
            }
            else if (rdoTrader.IsChecked == true)
            {
                checkResult = DelegationType.Trader;
            }

            dict.Add("CheckResult", checkResult.ToString());

            //发起过贷款申请，并且贷款申请没有取消
            if (CaseProxyService.GetInstanse().GetCaseEvents(_caseId, CaseStatus.DK1, null) != null && CaseProxyService.GetInstanse().GetCaseEvents(_caseId, CaseStatus.ZB11, null) == null)
            {
                if (CaseProxyService.GetInstanse().GetCaseEvents(_caseId, CaseStatus.JY5, null) == null)
                {
                    MessageBox.Show("贷款合同尚未送达！", "提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
            }

            if (rdoTrader.IsChecked == true)
            {
                result = CaseProxyService.GetInstanse()
                    .UpdateCaseStatus(_caseId, CaseStatus.JY11, LoginHelper.CurrentUser.SysNo, dict);
            }
            else
            {
                result = CaseProxyService.GetInstanse()
                    .UpdateCaseStatus(_caseId, CaseStatus.JY11A1, LoginHelper.CurrentUser.SysNo, dict);
            }


            if (result.Success)
            {

                SaveEvent();
                this.Close();
            }
            else
            {
                MessageBox.Show(result.ResultMessage);
            }
        }


        private void BtnUpload_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var upload = new UploadFileHelper();
                var fileNum = 0;
                upload.UploadFile(_caseId, AttachmentType.收件收据产权过户, out fileNum);
            }
            catch (Exception ex)
            {
                MessageBox.Show("上传附件操作发生系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 高拍仪上传按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCamera_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var captruePage = new CaptureImgeDialog(_caseId, AttachmentType.收件收据产权过户);
                captruePage.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("上传文件操作发生异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RdoGjj_OnChecked(object sender, RoutedEventArgs e)
        {
            spFundCentre.Visibility = Visibility.Visible;
        }

        private void Rdo_OnClick(object sender, RoutedEventArgs e)
        {
            spFundCentre.Visibility = Visibility.Collapsed;
        }
    }
}