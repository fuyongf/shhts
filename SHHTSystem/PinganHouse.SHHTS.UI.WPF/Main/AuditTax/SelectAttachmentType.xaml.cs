﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPF.Common;

namespace PinganHouse.SHHTS.UI.WPF.Main.AuditTax
{
    /// <summary>
    /// SelectAttachmentType.xaml 的交互逻辑
    /// </summary>
    public partial class SelectAttachmentType : Window
    {
        private string _caseId = String.Empty;
        private int _deviceType = 0;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="deviceType">1高拍仪，0其他</param>
        public SelectAttachmentType(string caseId, int deviceType, string attachmentBitType)
        {
            InitializeComponent();

            if (attachmentBitType == "TaxComplete")
            { 
                cmboxAttachmentType.Items.Add(new ComboBoxItem { Tag = 27, Content = "契税完税凭证" });
                cmboxAttachmentType.Items.Add(new ComboBoxItem { Tag = 28, Content = "个人所得税单" });
                cmboxAttachmentType.Items.Add(new ComboBoxItem { Tag = 57, Content = "营业税发票" });
                cmboxAttachmentType.Items.Add(new ComboBoxItem { Tag = 29, Content = "房产税认定书" });
            }
            else
            {
                cmboxAttachmentType.Items.Add(new ComboBoxItem { Tag = 13, Content = "产证" });
                cmboxAttachmentType.Items.Add(new ComboBoxItem { Tag = 14, Content = "他证" });
                
            }

            _caseId = caseId;
            _deviceType = deviceType;
        }

        private void BtnUpload_OnClick(object sender, RoutedEventArgs e)
        {
            string selectedTag = ((ComboBoxItem)cmboxAttachmentType.SelectedItem).Tag.ToString();
            AttachmentType attachmentType = new AttachmentType();
            if (selectedTag == "27")
            {
                attachmentType = AttachmentType.契税完税凭证;
            }
            else if (selectedTag == "28")
            {
                attachmentType = AttachmentType.个人所得税单;
            }
            else if (selectedTag == "57")
            {
                attachmentType = AttachmentType.营业税发票;
            }
            else if (selectedTag == "29")
            {
                attachmentType = AttachmentType.房产税认定书;
            }
            else if (selectedTag == "13")
            {
                attachmentType = AttachmentType.产证;
            }
            else if (selectedTag == "14")
            {
                attachmentType = AttachmentType.他证;
            }
            //AttachmentType.收件收据产权过户

            try
            {
                //高拍仪上传
                if (_deviceType == 1)
                {
                    var captruePage = new CaptureImgeDialog(_caseId, attachmentType);
                    captruePage.ShowDialog();
                }
                else
                {
                    var upload = new UploadFileHelper();
                    var fileNum = 0;
                    upload.UploadFile(_caseId, attachmentType, out fileNum);

                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("上传附件操作发生系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
