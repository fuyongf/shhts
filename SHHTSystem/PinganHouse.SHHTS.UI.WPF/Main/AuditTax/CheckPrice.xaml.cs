﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;
using MessageBox = System.Windows.MessageBox;

namespace PinganHouse.SHHTS.UI.WPF.Main.AuditTax
{
    /// <summary>
    /// CheckPrice.xaml 的交互逻辑
    /// </summary>

    public partial class CheckPrice : Window
    {
        public delegate void SaveHandle();
        public event SaveHandle SaveEvent;

        private String _caseId = String.Empty;

        public CheckPrice(String caseId)
        {
            InitializeComponent();

            _caseId = caseId;
            var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(caseId);
            var caseEvent = CaseProxyService.GetInstanse().GetCaseEvents(caseId, CaseStatus.HA2, null);
            var caseRemark = CaseProxyService.GetInstanse().GetRemarkBySysNo(caseDto.SysNo);

            grdFrom.DataContext = caseDto;
            lblSigningDate.Content = caseEvent.OrderByDescending(e => e.CreateDate).First().CreateDate.ToString("yyyy-MM-dd HH:mm");

            if (caseRemark != null)
            {
                var netlabelPrice = CaseProxyService.GetInstanse().GetRemarkBySysNo(caseDto.SysNo).NetlabelPrice;
                decimal price = 0;
                if (netlabelPrice != null)
                {
                    decimal.TryParse(netlabelPrice.ToString(), out price);
                }
                lblPrice.Content = price.ToString("C");
            }

        }



        private void BtnCancel_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnSave_OnClick(object sender, RoutedEventArgs e)
        {
            var checkPriceResult = String.Empty;
            decimal checkPrice = 0;
            if (rdoPass.IsChecked == true)
            {
                checkPriceResult = rdoPass.Content.ToString();
            }
            else if (rdoFail.IsChecked == true)
            {
                checkPriceResult = rdoFail.Content.ToString();

                if (spCheckPrice.Visibility == Visibility.Visible)
                {
                    decimal.TryParse(txtCheckPrice.Text, out checkPrice);
                    if (checkPrice == 0 )
                    {
                        MessageBox.Show("请填写核定价格！", "警告", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    else if (checkPrice < 0 || checkPrice > 1000000000)
                    {
                        MessageBox.Show("核定价格范围错误，请填写1-200000000之间的数字！", "警告", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }

                }
            }

            if (String.IsNullOrEmpty(checkPriceResult))
            {
                MessageBox.Show("请选择核价结果！", "警告", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }



            Dictionary<String, object> dict = new Dictionary<string, object>();
            dict.Add("CheckPriceResult", checkPriceResult);
            dict.Add("CheckPrice", checkPrice);

            var result = CaseProxyService.GetInstanse()
                .UpdateCaseStatus(_caseId, CaseStatus.JY2, LoginHelper.CurrentUser.SysNo, dict);

            if (result.Success)
            {
                MessageBox.Show("核价成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                SaveEvent();
                this.Close();
            }
            else
            {
                MessageBox.Show(result.ResultMessage);
            }

        }

        private void RadioButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (rdoFail.IsChecked == true)
            {
                spCheckPrice.Visibility = Visibility.Visible;
            }
            else
            {
                spCheckPrice.Visibility = Visibility.Collapsed;
            }

        }

        /// <summary>
        /// 上传附件按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnUpload_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var upload = new UploadFileHelper();
                var fileNum = 0;
                upload.UploadFile(_caseId, AttachmentType.核价单, out fileNum);
            }
            catch (Exception ex)
            {
                MessageBox.Show("上传附件操作发生系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 高拍仪上传按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCamera_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var captruePage = new CaptureImgeDialog(_caseId, AttachmentType.核价单);
                captruePage.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("上传文件操作发生异常：" + ex.Message, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }


    }
}
