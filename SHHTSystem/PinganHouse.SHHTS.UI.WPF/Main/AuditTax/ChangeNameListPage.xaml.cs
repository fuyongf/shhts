﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Main.Dialog;
using PinganHouse.SHHTS.UI.WPF.Model;
using System.Windows.Documents;

namespace PinganHouse.SHHTS.UI.WPF.Main.AuditTax
{
    /// <summary>
    /// ChangeNameListPage.xaml 的交互逻辑
    /// </summary>
    public partial class ChangeNameListPage
    {
        /// <summary>
        /// 页面状态
        /// </summary>
        private readonly CaseStatus _status = CaseStatus.ZB1 |
                                    CaseStatus.JY1 |
                                    CaseStatus.JY2 |
                                    CaseStatus.JY3 |
                                    CaseStatus.JY4 |
                                    CaseStatus.JY6 |
                                    CaseStatus.JY7 |
                                    CaseStatus.JY8 |
                                    CaseStatus.JY9 |
                                    CaseStatus.JY10 |
                                    CaseStatus.JY11 |
                                    CaseStatus.JY12 |
                                    CaseStatus.JY11A1 |
                                    CaseStatus.ZB6 |
                                    CaseStatus.ZB7 ;

        private readonly int _pageSize = 10;


        public ChangeNameListPage()
        {
            InitializeComponent();
            this.GridPager.PageNumChanged = GridPager_PagerIndexChanged;



            var list = EnumHelper.InitCaseStatusToCombobox(CaseStatus.ZB1 |
                                    CaseStatus.JY1 |
                                    CaseStatus.JY2 |
                                    CaseStatus.JY3 |
                                    CaseStatus.JY4 |
                                    CaseStatus.JY6 |
                                    CaseStatus.JY7 |
                                    CaseStatus.JY8 |
                                    CaseStatus.JY9 |
                                    CaseStatus.JY10 |
                                    CaseStatus.JY11 |
                                    CaseStatus.JY12 |
                                    CaseStatus.ZB6 |
                                    CaseStatus.ZB7);
            if (list == null)
            {
                MessageBox.Show("初始化失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            caseStatusCb.ItemsSource = list;
            caseStatusCb.DisplayMemberPath = "DisplayMember";
            caseStatusCb.SelectedValuePath = "ValueMember";
            caseStatusCb.SelectedValue = 0;


        }

        /// <summary>
        /// 搜索按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchBtn_OnClick(object sender, RoutedEventArgs e)
        {
            GridPager.PageIndex = 1;

        }

        /// <summary>
        /// 分页插件事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridPager_PagerIndexChanged(int index)
        {
            PageHelper.PageActiveLoadingControl(loading, "正在加载数据,请稍后...");
            var thread = new Thread(GetDataGridData) {IsBackground = true};
            thread.Start();

        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            GridPager.PageIndex = 1;
        }

        #region 页面数据取得方法
        /// <summary>
        /// 获取页面数据
        /// </summary>
        private void GetDataGridData()
        {
            Dispatcher.Invoke(delegate
            {
                try
                {
                    //案件编号
                    var caseId = string.IsNullOrEmpty(this.txtCaseId.Text.Trim()) ? null : this.txtCaseId.Text.Trim();
                    //案件地址
                    var address = string.IsNullOrEmpty(this.txtAddress.Text.Trim()) ? null : this.txtAddress.Text.Trim();
                     //经纪人
                    var agencyName = string.IsNullOrEmpty(this.txtAgency.Text.Trim()) ? null : this.txtAgency.Text.Trim();
                    //创建人
                    var createrName = string.IsNullOrEmpty(this.txtCreater.Text.Trim()) ? null : this.txtCreater.Text.Trim();
                
                    //案件状态选择
                    var caseStatus = (CaseStatus)Enum.Parse(typeof(CaseStatus), Convert.ToString(caseStatusCb.SelectedValue));
                    if (caseStatus == CaseStatus.JY11)
                    {
                        caseStatus = CaseStatus.JY11 | CaseStatus.JY11A1;
                    }
                    CaseStatus? selectStatus = caseStatus;
                    if (caseStatus == CaseStatus.Unkown) { selectStatus = null; }

                    int cnt;
                    var caseModelList = new List<CastModel>();

                    var caseList = CaseProxyService.GetInstanse().GetPaginatedList(GridPager.PageIndex, _pageSize, out cnt, ConfigHelper.GetCurrentReceptionCenter(), _status, selectStatus, caseId, address, agencyName, operationStatus: CaseStatus.JY1, creatorName: createrName);
                    if (caseList != null)
                    {
                        caseModelList = CastModel.InitData(caseList,CaseStatus.JY1);
                    }

                    this.dataGrid.DataContext = caseModelList;
                    
                    if (cnt % _pageSize != 0){
                        GridPager.PageCount = cnt / _pageSize+1;
                    }else{
                        GridPager.PageCount = cnt / _pageSize;
                    }
                    GridPager.Visibility = Visibility.Visible;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("加载案件数据发生错误：" + ex, "系统错误", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                finally
                {
                    loading.IsBusy = false;
                }
            });
        }
        #endregion

        /// <summary>
        /// 立即接单按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnOrder_OnClick(object sender, RoutedEventArgs e)
        {
            //案件编号
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.JY1, LoginHelper.CurrentUser.SysNo);
            if (result.Success)
            {
                MessageBox.Show("接单成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                GridPager_PagerIndexChanged(GridPager.PageIndex);
            }
            else
            {
                MessageBox.Show("接单失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 核价按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCheckPrice_OnClick(object sender, RoutedEventArgs e)
        {
            var caseId = ((Button)sender).CommandParameter.ToString();
            var checkPrice = new CheckPrice(caseId);
            checkPrice.SaveEvent += operation_SaveEvent;
            checkPrice.ShowDialog();
        }

        void operation_SaveEvent()
        {
            GridPager_PagerIndexChanged(GridPager.PageIndex);
        }

        /// <summary>
        /// 送审税按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSentToAuditTax_OnClick(object sender, RoutedEventArgs e)
        {
            var caseId = ((Button)sender).CommandParameter.ToString();
            var sentToAuditTax = new SentToAuditTax(caseId);
            sentToAuditTax.SaveEvent += operation_SaveEvent;
            sentToAuditTax.ShowDialog();
        }

        /// <summary>
        /// 送限购按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnPurchasingLimitations_OnClick(object sender, RoutedEventArgs e)
        {
            var caseId = ((Button) sender).CommandParameter.ToString();
            var purchasingLimitations = new PurchasingLimitations(caseId);
            purchasingLimitations.SaveEvent += operation_SaveEvent;
            purchasingLimitations.ShowDialog();
        }

        /// <summary>
        /// 限购结果按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnPurchasingLimitationsResult_OnClick(object sender, RoutedEventArgs e)
        {
            var caseId = ((Button)sender).CommandParameter.ToString();
            var purchasingLimitationsResult = new PurchasingLimitationsResult(caseId);
            purchasingLimitationsResult.SaveEvent += operation_SaveEvent;
            purchasingLimitationsResult.ShowDialog();
        }

        /// <summary>
        /// 案件中止按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCaseAborted_OnClick(object sender, RoutedEventArgs e)
        {
            //案件编号
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.ZB6, LoginHelper.CurrentUser.SysNo);
            if (result.Success)
            {
                GridPager_PagerIndexChanged(GridPager.PageIndex);
            }
            else
            {
                MessageBox.Show("案件终止失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 客户到场按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCustomerPresence_OnClick(object sender, RoutedEventArgs e)
        {

            var msgResult = MessageBox.Show("该操作不可恢复，是否继续？", "系统确认", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
            if (msgResult == MessageBoxResult.No) return;

            //案件编号
            var caseId = Convert.ToString(((Button)sender).CommandParameter);
            var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.JY7, LoginHelper.CurrentUser.SysNo);
            if (result.Success)
            {
                GridPager_PagerIndexChanged(GridPager.PageIndex);
            }
            else
            {
                MessageBox.Show("操作失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 完税按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnTaxComplete_OnClick(object sender, RoutedEventArgs e)
        {
            var caseId = ((Button)sender).CommandParameter.ToString();
            var taxComplete = new TaxComplete(caseId);
            taxComplete.SaveEvent += operation_SaveEvent;
            taxComplete.ShowDialog();
        }

        /// <summary>
        /// 过户按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnTransferOfMaterials_OnClick(object sender, RoutedEventArgs e)
        {
            var caseId = ((Button)sender).CommandParameter.ToString();
            var uploadReceipt = new UploadReceipt(caseId);
            uploadReceipt.SaveEvent += operation_SaveEvent;
            uploadReceipt.ShowDialog();
        }

        /// <summary>
        /// 领新产证按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnGetCertificates_OnClick(object sender, RoutedEventArgs e)
        {
            var caseId = ((Button)sender).CommandParameter.ToString();
            var outCertDialog = new OutCertDialog(caseId);
            outCertDialog.SaveEvent += operation_SaveEvent;
            outCertDialog.ShowDialog();
        }

        /// <summary>
        /// 案件跳转填写明细
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RequestNavigate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var link = e.Source as Hyperlink;
                if (null != link)
                {
                    var caseId = Convert.ToString(link.NavigateUri);
                    var frm = new CaseDetailsDialog(caseId);
                    frm.Show();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("显示明细发生错误", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            e.Handled = true;
        }
    }
}
