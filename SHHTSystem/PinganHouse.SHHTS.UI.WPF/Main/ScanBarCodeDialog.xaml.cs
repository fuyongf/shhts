﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Main
{
    /// <summary>
    /// ScanBarCodeDialog.xaml 的交互逻辑
    /// </summary>
    public partial class ScanBarCodeDialog : Window
    {
        BardCodeHooK BarCode = new BardCodeHooK();
        public ScanBarCodeDialog()
        {
            InitializeComponent();
            BarCode.BarCodeEvent += new BardCodeHooK.BardCodeDeletegate(BarCode_BarCodeEvent);
            TbFocus.Focus();
        }
        private delegate void ShowInfoDelegate(BardCodeHooK.BarCodes barCode);

        string result = "";

        void BarCode_BarCodeEvent(BardCodeHooK.BarCodes barCode)
        {
            if (barCode.IsValid)
            {
                result = barCode.BarCode;
                this.Close();
            }
           
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            //BarCode.Stop();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //BarCode.Start();
        }

        public string showScanBarCodeDialog(Window parent)
        {
            BarCode.Start();
            this.Owner = parent;
            this.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            this.ShowDialog();
            result = result.Replace("\r", "");
            return result;
        }
      
    }
}
