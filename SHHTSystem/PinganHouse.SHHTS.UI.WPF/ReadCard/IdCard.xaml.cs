﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;

namespace PinganHouse.SHHTS.UI.WPF.ReadCard
{
    /// <summary>
    /// IdCard.xaml 的交互逻辑
    /// </summary>
    public partial class IdCard : Window
    {
        private readonly UserIdentityModel _identityInfo;

        private StringBuilder _name;
        private StringBuilder _gender;
        private StringBuilder _folk;
        private StringBuilder _birthDay;
        private StringBuilder _code;
        private StringBuilder _address;
        private StringBuilder _agency;
        private StringBuilder _expireStart;
        private StringBuilder _expireEnd;
        private byte[] _photo;

        private bool _isCancel;
        private readonly bool _deviceNotExists;

        /// <summary>
        /// 获取设备存在性
        /// </summary>
        public bool DeviceNotExists
        {
            get { return _deviceNotExists; }
        }

        public IdCard(UserIdentityModel userIdentityModel,int type)
        {
            InitializeComponent();
            if (userIdentityModel == null)
            {
                MessageBox.Show("参数初始化失败", "提示");
                return;
            }                     
            string errorMessage;

            if (!IdentityCardHelper.OpenDevice(out errorMessage))
            {
                _deviceNotExists = true;            
            }
         
            _identityInfo = userIdentityModel;
            _isCancel = false;
            switch (type)
            {
                case 1:
                    TbTip.Text = "请将身份证放到身份证读卡器上,请刷买方身份证...";
                    break;
                case 2:
                    TbTip.Text = "请将身份证放到身份证读卡器上,请刷卖方身份证...";
                    break;
                case 3:
                    TbTip.Text = "请将身份证放到身份证读卡器上,请刷经纪人身份证...";                    
                    break;                   
            }

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (_deviceNotExists)
            {
                IsShowInput();
                ScanBtn.IsEnabled = false;
            }             
            else
            {
                IsShowId();
            }            
            var thread = new Thread(Read);
            thread.IsBackground = true;
            thread.Start();
        }

        private void Read()
        {
            string errorMessage;

            while (!IdentityCardHelper.FindCard(out errorMessage))
            {
                if (_isCancel)
                    break;
                Thread.Sleep(50);
            }

            if (_isCancel)
            {
                return;
            }

            bool result = IdentityCardHelper.Read(
                   out _name,
                   out _gender,
                   out _folk,
                   out _birthDay,
                   out _code,
                   out _address,
                   out _agency,
                   out _expireStart,
                   out _expireEnd,
                   out _photo,
                   out errorMessage);

            if (!result)
            {
                if (MessageBox.Show(errorMessage, "错误", MessageBoxButton.OK, MessageBoxImage.Error) == MessageBoxResult.OK)
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {

                        Close();

                    });
                }
                return;
            }

            _identityInfo.Name = _name.ToString();

            _identityInfo.Birthday = DateTime.Parse(
                _birthDay.ToString().Substring(0, 4)
                + "-" + _birthDay.ToString().Substring(4, 2)
                + "-" + _birthDay.ToString().Substring(6, 2));

            _identityInfo.Address = _address.ToString();
            _identityInfo.IdentityNo = _code.ToString();

            _identityInfo.EffectiveDate = DateTime.Parse(
                _expireStart.ToString().Substring(0, 4)
                + "-" + _expireStart.ToString().Substring(4, 2)
                + "-" + _expireStart.ToString().Substring(6, 2));

            if (_expireEnd.ToString().Trim()!= "长期")
            {
                _identityInfo.ExpiryDate = DateTime.Parse(_expireEnd.ToString().Substring(0, 4)
                                                         + "-" + _expireEnd.ToString().Substring(4, 2)
                                                         + "-" + _expireEnd.ToString().Substring(6, 2));
            }
            else
            {
                _identityInfo.ExpiryDate = null;
            }


            _identityInfo.Gender = _gender.ToString() == "男" ? Gender.Male : Gender.Female;
            _identityInfo.Nation = (Nation)Enum.Parse(typeof(Nation), _folk + "族");
            _identityInfo.Nationality = "中国";
            _identityInfo.Photo = _photo;
            _identityInfo.CertificateType = (CertificateType)Enum.Parse(typeof(CertificateType), "0");
            _identityInfo.VisaAgency = _agency.ToString();
            _identityInfo.IsTrusted = true;      
            //删除本地的身份证图片
            string photoPath=string.Format("{0}"+"\\photo.bmp",AppDomain.CurrentDomain.BaseDirectory);
            if (File.Exists(photoPath))
                File.Delete(photoPath);
            
            Application.Current.Dispatcher.Invoke(() =>
            {

                Close();

            });

        }

        private void CommitBtn_Click(object sender, RoutedEventArgs e)
        {
            bool isSuccess=true;
            string userName = TbUser.Text.Trim();
            string idnum = TbIdNum.Text.Trim();
            if (!ValidationUser(userName))
                isSuccess = false;
            if (!ValidationId(idnum))
                isSuccess = false;
            if (!isSuccess)
                return;           
            _identityInfo.Name = userName;
            _identityInfo.IdentityNo = idnum;
            _identityInfo.IsTrusted = false;
            Close();
        }

        private bool ValidationUser(string userName)
        {           
            if (ValidationCommon.IsEmpty(userName))
            {
                TbUserError.Text = "用户名不能为空";
                return false;
            }
            if (!ValidationCommon.IsUserMatch(userName))
            {
                TbUserError.Text = "用户名输入不合法";
                return false;
            }
            return true;
        }
        private bool ValidationId(string idnum)
        {           
            if (ValidationCommon.IsEmpty(idnum))
            {
                TbIdNumError.Text = "身份证号不能为空";
                return false;
            }
            if (!ValidationCommon.IsIdMatch(idnum))
            {
                TbIdNumError.Text = "身份证号输入不合法";
                return false;
            }
            return true;
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _isCancel = true;
            IdentityCardHelper.CloseDevice();
            Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
          if (!_deviceNotExists)                      
            IdentityCardHelper.CloseDevice();
        }
        /// <summary>
        /// 用户名验证
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
         //private void TbUser_KeyUp(object sender, KeyEventArgs e)
         private void TbUser_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!ValidationCommon.IsUserMatch(TbUser.Text.Trim()))
            {
                TbUserError.Text = "姓名中不能包含特殊字符";               
            }
            else
            {
                TbUserError.Text = "";
            }                      
        }
        /// <summary>
        /// 身份证号码验证
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
       private void TbIdNum_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!ValidationCommon.IsIdMatch(TbIdNum.Text.Trim()))
            {
                TbIdNumError.Text = "身份证输入不合法";              
            }
            else
            {
                TbIdNumError.Text = "";
            }
        }

       #region 引导按钮的显示隐藏
       private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            IsShowId();
        }

        private void  IsShowInput()
        {
            GridInput.Visibility = Visibility.Visible;
            GridId.Visibility = Visibility.Collapsed;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            IsShowInput();
        }
        private void  IsShowId()
        {
            if (!_deviceNotExists)          
            {
                ScanBtn.IsEnabled = true;
                GridInput.Visibility = Visibility.Collapsed;
                GridId.Visibility = Visibility.Visible;
            }
            else
            {
                ScanBtn.IsEnabled = false;
            }

        }
       #endregion        
    }
}



