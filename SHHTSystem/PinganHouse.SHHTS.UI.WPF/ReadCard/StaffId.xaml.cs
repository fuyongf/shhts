﻿using System.Text;
using System.Threading;
using System.Windows;
using PinganHouse.SHHTS.UI.WPF.Common;
using System;

namespace PinganHouse.SHHTS.UI.WPF.ReadCard
{
    /// <summary>
    /// StaffId.xaml 的交互逻辑 
    /// </summary>
    public partial class StaffId : Window
    {

        private StringBuilder _serialNum;
        private  string _staffNo;
        private bool _isCancel;
        public StaffId()
        {
            InitializeComponent();
            _isCancel = false;
        }

        private bool isError;
        /// <summary>
        /// 是否读卡失败
        /// </summary>
        public bool IsReadError { get { return this.isError; } }
        public string StaffNo
        {
            get { return _staffNo; }
        }

        #region Load
        private void StaffId_OnLoaded(object sender, RoutedEventArgs e)
        {
            string errorMessage;
            if (!IdentityCardHelper.OpenDevice(out errorMessage))
            {
                MessageBox.Show(errorMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            var thread = new Thread(Read) {IsBackground = true};
            thread.Start();
        }
       #endregion

        #region 读员工卡
   
        private void Read()
        {
            string errorMessage;
            while (!IdentityCardHelper.FindStaffCard(out errorMessage))
            {
                if (_isCancel)
                    break;
                Thread.Sleep(50);
            }

            if (_isCancel)
            {
                return;
            }                      
            bool result= IdentityCardHelper.ReadStaffCard(out _serialNum, out errorMessage);
            if (!result)
            {
                if (MessageBox.Show(errorMessage, "错误", MessageBoxButton.OK, MessageBoxImage.Error) == MessageBoxResult.OK)
                {
                    //不想修改以前的业务，这里就不使用事件。
                    isError = true;
                    Application.Current.Dispatcher.Invoke(Close);
                }
                
                return;
            }
            _staffNo = _serialNum.ToString();
           var retBeep= IdentityCardHelper.BeepLed(out errorMessage);
           //if (!retBeep)
           //{
           //    MessageBox.Show(errorMessage, "错误", MessageBoxButton.OK, MessageBoxImage.Error);
           //}
            Application.Current.Dispatcher.Invoke(Close);
        }
        #endregion

        #region 取消
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _isCancel = true;
            IdentityCardHelper.CloseDevice();
            Close();
        }
        #endregion

        private void Window_Closed(object sender, System.EventArgs e)
        {
            IdentityCardHelper.CloseDevice();
        }
    }
}
