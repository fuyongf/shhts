﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;

namespace PinganHouse.SHHTS.UI.WPF.ReadCard
{
    /// <summary>
    /// InputFinger.xaml 的交互逻辑
    /// </summary>
    public partial class InputFinger : Window
    {
        private PinganStaff pinganStaff;
        private string fingerTemplate;
        //private FingerRead fingerRead;
        private long userSysNo;
        private long operatorSysNo;
        private int  count;
        public InputFinger()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 验证UM帐号
        /// </summary>    
        private void TbUm_LostFocus(object sender, RoutedEventArgs e)
        {
            string umId = TbUm.Text.Trim();
            if (!ValidationCommon.IsUmIdMatch(umId))
            {
                TebError.Text = "UM帐号格式不正确";
            }
            else
            {
                TebError.Text = "";
            }
        }   
        private void SearchBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string umId = TbUm.Text.Trim();
                if (ValidationCommon.IsEmpty(umId))
                {
                    TebError.Text = "UM帐号不能为空";
                    return;
                }
                if (!ValidationCommon.IsUmIdMatch(umId))
                {
                    TebError.Text = "UM帐号格式不正确";
                    return;
                }
                pinganStaff = PinganStaffServiceProxy.GetByUMCode(umId);
                if (pinganStaff == null || pinganStaff.UserSysNo <= 0)
                {
                    MessageBox.Show("没有该员工的信息", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                     userSysNo = pinganStaff.UserSysNo;
                    if (userSysNo < 0 || userSysNo == 0)
                    {
                        MessageBox.Show("没有该员工的信息", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    var userFingerprints = UserServiceProxy.GetFingerByUserSysNo(userSysNo);
                    if (userFingerprints == null || userFingerprints.Count == 0)
                    {
                        SwipCardBtn.Content = "采集指纹";
                    }
                    else
                    {
                        SwipCardBtn.Content = "修改指纹";                     
                    }
                    SwipCardBtn.Visibility = Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SwipCardBtnBtn_Click(object sender, RoutedEventArgs e)
        {
              var fingerRead = new FingerRead();
            fingerRead.Closed += fingerRead_Closed;
            fingerRead.Show();
        }

        void fingerRead_Closed(object sender, EventArgs e)
        {
            fingerTemplate = ((FingerRead)sender).fingerTemplate;
            if (string.IsNullOrWhiteSpace(fingerTemplate))
                return;
            TebError.Text = "采集模版成功";
            ConfrimBtn.Visibility = Visibility.Visible;
        }

        private void ConfrimBtn_Click(object sender, RoutedEventArgs e)
        {
            ++count;
            var result = StaffHelper.CollectFingerprint(userSysNo, fingerTemplate, null, LoginHelper.CurrentUser.SysNo);
            if (!result.Success)
            {
                MessageBox.Show(result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                
            }
        }
    }
}

 
