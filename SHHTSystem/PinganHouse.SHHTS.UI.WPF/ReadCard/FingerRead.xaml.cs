﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.ReadCard
{
    /// <summary>
    /// FingerRead.xaml 的交互逻辑
    /// </summary>
    public partial class FingerRead : Window
    {
        const int iImageDataType = 1; //传出的图像类型，1—raw数据，2—bmp格式数据
        const int iLevel = 3;  //安全等级
        IntPtr ghDevicePtr = IntPtr.Zero;
        byte[] cpTZBuf = new byte[512];
        int iTZSize = 0;
        byte[] cpMBBuf = new byte[512];
        int iMBSize = 0;
        bool bGetMB = false;
        bool bGetTZ = false;
        byte[] cpGetImageData = new byte[256 * 360 + 1];
        bool bGetImageData = false;
        int giImageWidth = 0;
        int giImageHeight = 0;

        public string fingerTemplate;
        public string fingerFeture;
        public string FingerTemplate
        {
            get { return fingerTemplate; }
        }
        public string FingerFeture
        {
            get { return fingerFeture; }
        } 

        public FingerRead()
        {
            InitializeComponent();
        }
        private void FingerWindow_Loaded(object sender, RoutedEventArgs e)
        {
            ghDevicePtr = FPDll.FPIOpenDevice(0, 0);
            if (ghDevicePtr == IntPtr.Zero)
            {
                MessageBox.Show("指纹仪设备打开失败","系统提示",MessageBoxButton.OK,MessageBoxImage.Error);
            }
        }
            //判断手指干湿偏移
        private string CheckFingerOffsetDryWet(int nRetFDryWet, int nRetFOffset)
        {
            string temp1, temp2;
            switch (nRetFDryWet)
            {
            case FPDll.IMAGE_QUALITY_DRY:
                temp1 = "手指偏干";
	            break;
            case FPDll.IMAGE_QUALITY_WET:
                temp1 = "手指偏湿";
	            break;
            case FPDll.IMAGE_QUALITY_OK:
                temp1 = "手指干湿：正常";
	            break;
            case FPDll.IMAGE_QUALITY_UNKNOWN:
                temp1 = "手指干湿：未知";
	            break;
            case FPDll.IMAGE_QUALITY_NO_FINGER:
                temp1 = "图像无手指";
	            break;
            default:
                temp1 = "手指干湿：未知";
	            break;
            }
            //if (temp1 == "手指干湿：正常")
            //{
            //    return "干湿偏移正常";
            //}
            switch (nRetFOffset)
            {
            case FPDll.IMAGE_QUALITY_TOP:
                temp2 = "手指向上偏移";
	            break;
            case FPDll.IMAGE_QUALITY_BOTTOM:
                temp2 = "手指向下偏移";
	            break;
            case FPDll.IMAGE_QUALITY_LEFT:
                temp2 = "手指向左偏移";
	            break;
                case FPDll.IMAGE_QUALITY_RIGHT:
                temp2 = "手指向右偏移";
	            break;
                case FPDll.IMAGE_QUALITY_SMALL:
                temp2 = "指纹图像偏小";
	            break;
                case FPDll.IMAGE_QUALITY_TOP | FPDll.IMAGE_QUALITY_LEFT:
                temp2 = "指纹图像偏左上方";
	            break;
                case FPDll.IMAGE_QUALITY_TOP | FPDll.IMAGE_QUALITY_RIGHT:
                temp2 = "指纹图像偏右上方";
	            break;
                case FPDll.IMAGE_QUALITY_BOTTOM | FPDll.IMAGE_QUALITY_LEFT:
                temp2 = "指纹图像偏左下方";
	            break;
                case FPDll.IMAGE_QUALITY_BOTTOM | FPDll.IMAGE_QUALITY_RIGHT:
                temp2 = "指纹图像偏右下方";
	            break;
                case FPDll.IMAGE_QUALITY_OK:
                temp2 = "手指偏移：正常";
	            break;
                case FPDll.IMAGE_QUALITY_UNKNOWN:
                temp2 = "手指偏移：未知";
	            break;
                case FPDll.IMAGE_QUALITY_NO_FINGER:
                temp2 = "图像无手指";
	            break;
            default:
                temp2 = "手指偏移：未知";
	            break;
            }
            return (temp1 + "；"+ temp2);
        }
        private void GetImage(Image pictureBox, byte[] cpImageBuf, int iImageWidth, int iImageHeight)
        {
            pictureBox.Source = null;

            byte[] cpBmpData = new byte[256*360+1078];

            int iBmpLen = 0;

            int iRet = FPDll.FPIRawImageDataToGrayBitmapData(cpImageBuf,iImageWidth,iImageHeight,cpBmpData,ref iBmpLen);
            if (iRet == 0)
            {
                pictureBox.Source = GetImageSource(cpBmpData);//cpBmpData为内存数据
            }
        }
        private BitmapImage GetImageSource(byte[] imgByte)
        {
            BitmapImage imgSource = new BitmapImage();
            imgSource.BeginInit();
            imgSource.StreamSource = new MemoryStream(imgByte);
            imgSource.EndInit();
            return imgSource;
        }
        private void btnGetSDKInfo_Click(object sender, RoutedEventArgs e)
        {
            byte[] cpSDKInfo = new byte[128];
            int iRet = FPDll.FPIGetSDKVersion(cpSDKInfo);
            if (iRet == 0)
            {
                //this.txtSDKInfo.Text = System.Text.Encoding.Default.GetString(cpSDKInfo);
                this.lblInfo.Text = "获取SDK信息成功";
            }
            else
            {
                this.lblInfo.Text = "获取SDK信息失败";
                //this.txtSDKInfo.Text = "";
            }
        }

        private void btnOpenDevice_Click(object sender, RoutedEventArgs e)
        {
          
        }

        private void btnCloseDevice_Click(object sender, RoutedEventArgs e)
        {
            if (ghDevicePtr == IntPtr.Zero)
            {
                MessageBox.Show("请先打开设备");
                return;
            }
            int iRet = FPDll.FPICloseDevice(ref ghDevicePtr);
            if(iRet == 0)
            {
                this.lblInfo.Text = "关闭设备成功";
            }
            else
            {
                this.lblInfo.Text = "关闭设备失败";
            }
            Close();
        }

        private void btnGetDevVersion_Click(object sender, RoutedEventArgs e)
        {
            //byte[] cpVersion = new byte[128];
            //int iVerSize = 0;
            //int iRet = FPDll.FPIGetVersion(ghDevicePtr, cpVersion, ref iVerSize);
            //if (iRet == 0)
            //{
            //    txtDevVersion.Text = System.Text.Encoding.Default.GetString(cpVersion);
            //    this.lblInfo.Text = "获取设备版本信息成功";
            //}
            //else
            //{
            //    this.lblInfo.Text = "获取设备版本信息失败";
            //    txtDevVersion.Text = "";
            //}
        }

        private void btnGetDevSerialNo_Click(object sender, RoutedEventArgs e)
        {
            byte[] cpSerialNo = new byte[64];
            int iSize = 0;
            int iRet = FPDll.FPIGetSerialNumber(ghDevicePtr, cpSerialNo, ref iSize);
            if(iRet == 0)
            {
                //txtSerialNO.Text = System.Text.Encoding.Default.GetString(cpSerialNo);
                this.lblInfo.Text = "获取设备序列号成功";
            }
            else
            {
                this.lblInfo.Text = "获取设备序列号失败";
                //txtDevVersion.Text = "";
            }
        }

        private void btnTemplate_Click(object sender, RoutedEventArgs e)
        {
            int iTimeOut = 0;
            int iImageBufLen1= 0,iImageBufLen2=0,iImageBufLen3 = 0;
            int iImageWidth = 0, iImageHeight = 0, iMBQuality = 0;
            
            byte[] cpImageData1 = new byte[256 * 360 + 1];
            byte[] cpImageData2 = new byte[256 * 360 + 1];
            byte[] cpImageData3 = new byte[256 * 360 + 1];
            byte[] cpMBStr = new byte[1024];
            int iMBStrLen = 0;

            int iRet = FPDll.FPITemplate(ghDevicePtr, iTimeOut, iImageDataType, cpMBBuf, ref iMBSize, ref iMBQuality, ref iImageWidth, ref iImageHeight, cpImageData1, ref iImageBufLen1, cpImageData2, ref iImageBufLen2, cpImageData3, ref iImageBufLen3);
            if (iRet == 0)
            {
                bGetMB = true;
                this.lblInfo.Text = "采集模板成功,模板质量=" + iMBQuality.ToString();
                if (iImageDataType == 1)
                {
                    GetImage(pictureBox1, cpImageData1, iImageWidth, iImageHeight);
                    GetImage(pictureBox2, cpImageData2, iImageWidth, iImageHeight);
                    GetImage(pictureBox3, cpImageData3, iImageWidth, iImageHeight);
                }
                else if (iImageDataType == 2)
                {
                    pictureBox1.Source = GetImageSource(cpImageData1);
                    pictureBox2.Source = GetImageSource(cpImageData2);
                    pictureBox3.Source = GetImageSource(cpImageData3);
                }

                iRet = FPDll.FPIHexFingerDataToBase64(cpMBBuf, iMBSize, cpMBStr, ref iMBStrLen);
                if (iRet == 0)
                {
                    fingerTemplate = System.Text.Encoding.Default.GetString(cpMBStr);
                    Btn3.Visibility = Visibility.Visible;
                }
            }
            else
            {
                bGetMB = false;
                this.lblInfo.Text = "采集模板失败";
                //txtMB.Text = "";
                pictureBox1.Source = null;
                pictureBox2.Source = null;
                pictureBox3.Source = null;
            }
        }

        private void btnFeature_Click(object sender, RoutedEventArgs e)
        {
            int iImageDataLen = 0,iTZQuality = 0;
            int iImageWidth = 0, iImageHeight = 0;
            byte[] cpImageData = new byte[256 * 360 + 1];
            byte[] cpTZStr = new byte[1024];
            int iTZStrLen = 0;
            int iTimeOut = 0;

            int iRet = FPDll.FPIFeature(ghDevicePtr, iTimeOut, iImageDataType, cpTZBuf, ref iTZSize, ref iTZQuality, ref iImageWidth, ref iImageHeight, cpImageData, ref iImageDataLen);
            if (iRet == 0)
            {
                bGetTZ = true;
                this.lblInfo.Text = "采集特征成功,特征质量=" + iTZQuality.ToString();
                if (iImageDataType == 1)
                {
                    GetImage(pictureBox1, cpImageData, iImageWidth, iImageHeight);
                    pictureBox2.Source = null;
                    pictureBox3.Source = null;
                    
                }
                else if (iImageDataType == 2)
                {
                    pictureBox1.Source = GetImageSource(cpImageData);
                    pictureBox2.Source = null;
                    pictureBox3.Source = null;
                }

                iRet = FPDll.FPIHexFingerDataToBase64(cpTZBuf, iTZSize, cpTZStr, ref iTZStrLen);
                if (iRet == 0)
                {
                    fingerFeture = System.Text.Encoding.Default.GetString(cpTZStr);
                }
            }
            else
            {
                bGetTZ = false;
                this.lblInfo.Text = "采集特征失败";           
                pictureBox1.Source = null;
                pictureBox2.Source = null;
                pictureBox3.Source = null;
            }
        }

        private void btnVerify_Click(object sender, RoutedEventArgs e)
        {
            if (!bGetMB)
            {
                MessageBox.Show("请先采集模板");
                return;
            }
            if (!bGetTZ)
            {
                MessageBox.Show("请先采集特征");
                return;
            }
            int iRet = FPDll.FPIVerify(ghDevicePtr, cpMBBuf, cpTZBuf, iLevel);
            if (iRet == 0)
            {
                this.lblInfo.Text = "指纹比对成功";
            }
            else
            {
                this.lblInfo.Text = "指纹比对失败";
            }
        }

        private void btnCheckFinger_Click(object sender, RoutedEventArgs e)
        {
            int iRet = FPDll.FPICheckFinger(ghDevicePtr);
            if (iRet == 0)
            {
                this.lblInfo.Text = "手指按下";
            }
            else
            {
                this.lblInfo.Text = "手指抬起";
            }
        }

        private void btnGetImage_Click(object sender, RoutedEventArgs e)
        {
            int iImageQuality = 0;
            int iFpDryOrWet = 0;
            int iFpOffset = 0;
            int iImageDataLen = 0;
            int iTimeOut = 0;
            int iRet = -1;
            iRet = FPDll.FPIGetImageData(ghDevicePtr, iTimeOut, iImageDataType, ref giImageWidth, ref giImageHeight, cpGetImageData, ref iImageDataLen, ref iImageQuality, ref iFpDryOrWet, ref iFpOffset);
            if (iRet == 0)
            {
                bGetImageData = true;
                this.lblInfo.Text = "采集指纹图像成功，图像：" + CheckFingerOffsetDryWet(iFpDryOrWet,iFpOffset);

                if (iImageDataType == 1)
                {
                    GetImage(pictureBox1, cpGetImageData, giImageWidth, giImageHeight);
                    pictureBox2.Source = null;
                    pictureBox3.Source = null;

                }
                else if (iImageDataType == 2)
                {
                    //pictureBox1.Source = Image.FromStream(new System.IO.MemoryStream(cpGetImageData));
                    //pictureBox2.Source = null;
                    //pictureBox3.Source = null;
                }
            }
            else
            {
                bGetImageData = false;
                this.lblInfo.Text = "采集图像失败";
                pictureBox1.Source = null;
                pictureBox2.Source = null;
                pictureBox3.Source = null;
            }
        }

        private void btnGetImageImedi_Click(object sender, RoutedEventArgs e)
        {
            int iImageQuality = 0;
            int iFpDryOrWet = 0;
            int iFpOffset = 0;
            int iImageDataLen = 0;
            int iRet = -1;
            iRet = FPDll.FPIGetImageDataImmediately(ghDevicePtr, iImageDataType, ref giImageWidth, ref giImageHeight, cpGetImageData, ref iImageDataLen, ref iImageQuality, ref iFpDryOrWet, ref iFpOffset);
            if (iRet == 0)
            {
                bGetImageData = true;
                this.lblInfo.Text = "立即采集指纹图像成功，图像：" + CheckFingerOffsetDryWet(iFpDryOrWet, iFpOffset);

                if (iImageDataType == 1)
                {
                    GetImage(pictureBox1, cpGetImageData, giImageWidth, giImageHeight);
                    pictureBox2.Source = null;
                    pictureBox3.Source = null;

                }
                else if (iImageDataType == 2)
                {
                    pictureBox1.Source =GetImageSource(cpGetImageData);
                    pictureBox2.Source = null;
                    pictureBox3.Source = null;
                }
            }
            else
            {
                bGetImageData = false;
                this.lblInfo.Text = "立即采集图像失败";
                pictureBox1.Source = null;
                pictureBox2.Source = null;
                pictureBox3.Source = null;
            }
        }

        private void btnExtractImgToTZ_Click(object sender, RoutedEventArgs e)
        {
            if (!bGetImageData)
            {
                MessageBox.Show("请先采集指纹图像");
                return;
            }
            byte[] cpTZStr = new byte[1024];
            int iTZStrLen = 0;
            int iRet = FPDll.FPIExtractByImageData(ghDevicePtr, iImageDataType, cpGetImageData, giImageWidth, giImageHeight, cpTZBuf, ref iTZSize);
            if (iRet == 0)
            {
                this.lblInfo.Text = "从指纹图像抽取指纹特征成功";

                iRet = FPDll.FPIHexFingerDataToBase64(cpTZBuf, iTZSize, cpTZStr, ref iTZStrLen);
                if (iRet == 0)
                {
                    //txtTZ.Text = System.Text.Encoding.Default.GetString(cpTZStr);
                }
            }
            else
            {
                this.lblInfo.Text = "从指纹图像抽取指纹特征失败";
                //txtTZ.Text = ""; 
            }
        }

        private void btnWriteData_Click(object sender, RoutedEventArgs e)
        {
            //if ((txtWriteData.Text.Length == 0) || (txtWriteData.Text.Length > 512))
            //{
            //    MessageBox.Show("写入数据长度不能等于0大于512");
            //    return;
            //}
            //int iRet = FPDll.FPIWriteData(ghDevicePtr, 0, txtWriteData.Text, txtWriteData.Text.Length);
            //if (iRet == 0)
            //{
            //    this.lblInfo.Text = "写入用户数据成功";
            //}
            //else
            //{
            //    this.lblInfo.Text = "写入用户数据失败";
            //}
        }

        private void btnReadData_Click(object sender, RoutedEventArgs e)
        {
            //byte[] cpReadData = new byte[2048];
            //int iReadDataLen = 512;
            //int iRet = FPDll.FPIReadData(ghDevicePtr, 0, cpReadData, iReadDataLen);
            //if (iRet == 0)
            //{
            //    this.lblInfo.Text = "读取用户数据成功";
            //    this.txtReadData.Text = System.Text.Encoding.Default.GetString(cpReadData);
            //}
            //else
            //{
            //    this.lblInfo.Text = "读取用户数据失败";
            //    this.txtReadData.Text = "";
            //}
        }

    

       
            
    }
  
}
