﻿using System;
using System.Windows;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;

namespace PinganHouse.SHHTS.UI.WPF.ReadCard
{
    /// <summary>
    ///     InputStaffId.xaml 的交互逻辑
    /// </summary>
    public partial class InputStaffId : Window
    {
        private PinganStaff pinganStaff;
        private string staffId;

        public InputStaffId()
        {
            InitializeComponent();
        }

        /// <summary>
        ///     验证UM帐号
        /// </summary>
        private void TbUm_LostFocus(object sender, RoutedEventArgs e)
        {
            var umId = TbUm.Text.Trim();
            if (!ValidationCommon.IsUmIdMatch(umId))
            {
                TebError.Text = "UM帐号格式不正确";
            }
            else
            {
                TebError.Text = "";
            }
        }

        /// <summary>
        ///     搜索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var umId = TbUm.Text.Trim();
                if (ValidationCommon.IsEmpty(umId))
                {
                    TebError.Text = "UM帐号不能为空";
                    return;
                }
                if (!ValidationCommon.IsUmIdMatch(umId))
                {
                    TebError.Text = "UM帐号格式不正确";
                    return;
                }
                pinganStaff = PinganStaffServiceProxy.GetByUMCode(umId);
                if (pinganStaff == null || pinganStaff.UserSysNo <= 0)
                {
                    MessageBox.Show("没有该员工的信息", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    TbName.Text = pinganStaff.RealName;
                    TbUmAcount.Text = pinganStaff.UMCode;
                    if (pinganStaff.StaffCardNo == null)
                    {
                        TbStaffId.Text = "无";
                        SwipCardBtn.Content = "读卡";
                    }
                    else
                    {
                        TbStaffId.Text = pinganStaff.StaffCardNo;
                        SwipCardBtn.Content = "修改";
                    }
                    ShowContent();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            staffId = string.Empty;
            Close();
        }

        private void SwipCardBtn_Click(object sender, RoutedEventArgs e)
        {
            var staff = new StaffId();
            staff.Closed += staff_Closed;
            staff.ShowDialog();
        }

        private void staff_Closed(object sender, EventArgs e)
        {
            staffId = ((StaffId) sender).StaffNo;
            if (string.IsNullOrWhiteSpace(staffId))
                return;
            //{
            //    MessageBox.Show("读取员工牌卡号失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            //}           
            TbStaffId.Text = staffId;
            pinganStaff.StaffCardNo = staffId;
            ConfirmBtn.Visibility = Visibility.Visible;
        }

        private void ConfirmBtn_Click(object sender, RoutedEventArgs e)
        {
            var result = StaffHelper.ModifyPinganStaff(pinganStaff);
            if (!result.Success)
            {
                MessageBox.Show(result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            Close();
        }

        private void ReturnBtn_Click(object sender, RoutedEventArgs e)
        {
            ShowSearch();
        }

        private void ShowContent()
        {
            SearchGrid.Visibility = Visibility.Collapsed;
            ShowGrid.Visibility = Visibility.Visible;
        }

        private void ShowSearch()
        {
            SearchGrid.Visibility = Visibility.Visible;
            ShowGrid.Visibility = Visibility.Collapsed;
        }
    }
}