﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Control
{
    /// <summary>
    /// CertificateOfOfficersControl.xaml 的交互逻辑
    /// </summary>
    public partial class CertificateOfOfficersControl : UserControl
    {
        public CertificateOfOfficersControl()
        {
            InitializeComponent();
        }

        private void TbEffectiveDate_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9]+");
            //Regex re = new Regex("[1,2][9,0][0-9]{2}");

            e.Handled = re.IsMatch(e.Text);
        }

        private void TbExpiryDate_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9]+");

            e.Handled = re.IsMatch(e.Text);
        }
    }
}
