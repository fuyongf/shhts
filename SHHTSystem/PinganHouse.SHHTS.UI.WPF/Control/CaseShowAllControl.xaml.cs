﻿using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Control
{
    /// <summary>
    /// CaseShowAllControl.xaml 的交互逻辑
    /// </summary>
    public partial class CaseShowAllControl : UserControl,INotifyPropertyChanged
    {
        private readonly SolidColorBrush Default_Color = new SolidColorBrush(Color.FromArgb(255, 141, 173, 205));
        private readonly SolidColorBrush Default_Color_Black = new SolidColorBrush(Colors.Black);
        public CaseShowAllControl()
        {
            InitializeComponent();
        }

        private CaseStatusModel allStatus;

        public CaseStatusModel AllStatus
        {
            get
            {
                return allStatus;
            }
            set
            {
                if (allStatus != value)
                {
                    allStatus = value;
                    NotifyPropertyChanged("AllStatus");
                }
                if (allStatus != null)
                {
                    if(allStatus.IsZB12)
                    {
                        this.m_ZB12_1.BorderBrush = Default_Color;
                    }
                    if (allStatus.IsJY11)
                    {
                        this.m_JY11_1.BorderBrush = Default_Color;
                        this.m_JY11_2.BorderBrush = Default_Color;
                        this.m_JY11_3.BorderBrush = Default_Color;
                        this.m_JY11_4.BorderBrush = Default_Color;
                        this.m_JY11_5.BorderBrush = Default_Color;
                        this.m_JY11_6.BorderBrush = Default_Color;
                    }
                    if(allStatus.IsZB8)
                    {
                        this.m_ZB8_1.BorderBrush = Default_Color;
                    }
                    if (allStatus.IsZB13)
                    {
                        this.m_ZB13_1.BorderBrush = Default_Color;
                        
                    }
                    if (allStatus.IsZB3)
                    {
                        this.m_ZB3_1.BorderBrush = Default_Color;
                        this.m_ZB3_2.BorderBrush = Default_Color;
                    }
                    if (allStatus.IsZB1)
                    {
                        this.m_ZB1_1.BorderBrush = Default_Color;
                        this.m_ZB1_2.BorderBrush = Default_Color;
                    }
                    if (allStatus.IsDK1)
                    {
                        this.m_DK1_1.BorderBrush = Default_Color;
                        this.m_DK1_2.BorderBrush = Default_Color;
                    }
                    if (allStatus.IsZB5)
                    {
                        this.m_ZB5_1.BorderBrush = Default_Color;
                    }
                    if (allStatus.IsDK10)
                    {
                        this.m_DK10_1.Fill = Default_Color;
                        this.m_DK10_2.Fill = Default_Color;
                        this.m_DK10_3.BorderBrush = Default_Color;
                        this.m_DK10_4.BorderBrush = Default_Color;
                        this.m_DK10_5.Foreground = Default_Color_Black;
                    }
                    if (allStatus.IsZB11A)
                    {
                        this.m_ZB11_1.Fill = Default_Color;
                        this.m_ZB11_2.Foreground = Default_Color_Black;
                        this.m_ZB11_3.BorderBrush = Default_Color;
                    }

                    if (allStatus.IsDK1 && allStatus.IsDK9)
                    {
                        this.m_DK2_1.BorderBrush = Default_Color;
                        this.m_DK2_2.BorderBrush = Default_Color;
                        this.m_DK2_3.Fill = Default_Color;
                    }
                    if (allStatus.IsZB7)
                    {
                        //this.m_ZB7_1.BorderBrush = Default_Color;
                    }
                    if(allStatus.IsJY4)
                    {
                        this.m_JY4_1.BorderBrush = Default_Color;
                        this.m_JY4_2.BorderBrush = Default_Color;
                        this.m_JY4_3.Fill = Default_Color;
                    }
                    if(allStatus.IsHA5)
                    {
                        this.m_HA5_1.Foreground = Default_Color_Black;
                        this.m_HA5_2.Foreground = Default_Color_Black;
                    }

                    if (allStatus.IsHA6)
                    {
                        this.m_HA6_1.BorderBrush = Default_Color;
                        this.m_HA6_2.BorderBrush = Default_Color;
                        this.m_HA6_3.Fill = Default_Color;
                    }

                    if(allStatus.IsHouseMoneyFinished)
                    {
                        this.m_HouseMoney.Foreground = Default_Color_Black;
                    }
                    if(allStatus.IsTaxFinished)
                    {
                        this.m_Tax.Foreground = Default_Color_Black;
                    }
                    if(allStatus.IsCommissionsFinished)
                    {
                        this.m_Commissions.Foreground = Default_Color_Black;
                    }
                    
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string p)
        {
            if (PropertyChanged != null)
            {
                this.PropertyChanged(this,new PropertyChangedEventArgs(p));
            }
        }

    }
}
