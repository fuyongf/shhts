﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPF.Control
{
    /// <summary>
    /// SHHTS_PassWord.xaml 的交互逻辑
    /// </summary>
    public partial class SHHTS_PassWord : UserControl
    {
        public SHHTS_PassWord()
        {
            InitializeComponent();
            if (string.IsNullOrWhiteSpace(TipContent))
            {
                _tipContent = "请输入密码";
            }
            txtPasswordTip.Text = TipContent;
        }

        string tip = string.Empty;
        private void password_PasswordChanged(object sender, RoutedEventArgs e)
        {

            if (password.Password.Length != 0)
            {
                // this.Content = password.Password;
                this._Password = password.Password;
                if (!string.IsNullOrWhiteSpace(txtPasswordTip.Text))
                    tip = txtPasswordTip.Text;
                txtPasswordTip.Text = string.Empty;
            }
            else { txtPasswordTip.Text = tip; }
        }

        private string _tipContent = string.Empty;
        public string TipContent
        {
            get
            {
                return _tipContent;
            }
            set
            {
                if (value != _tipContent)
                {
                    _tipContent = value;
                    txtPasswordTip.Text = TipContent;
                }
            }
        }

        private string _Password = string.Empty;
        public string Password
        {
            get
            {
                return _Password;
            }
            set
            {
                if (value != _Password)
                {
                    _Password = value;

                }
                if (value == string.Empty)
                {
                    password.Password = string.Empty;
                    if (string.IsNullOrWhiteSpace(TipContent))
                    {
                        _tipContent = "请输入密码";
                    }
                    txtPasswordTip.Text = TipContent;
                    password.Focus();
                }
            }
        }

    }
}
