﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.UI.WPF.Model
{
    public class CaseStatusModel:INotifyPropertyChanged
    {
        private DateTime _HA4Time;
        private DateTime _HA5Time;
        private DateTime _HA6Time;
        private DateTime _DK1Time;
        private DateTime _DK2Time;
        private DateTime _DK3Time;
        private DateTime _DK4Time;
        private DateTime _DK5Time;
        private DateTime _DK9Time;
        private DateTime _DK10Time;
        private DateTime _JY4Time;
        private DateTime _JY9Time;

        private readonly string DETAIL_INFO_FORMAT = "时间：{0}\n受理人：{1}";
        public CaseStatusModel(IList<CaseEvent> caseEventList, bool isHouseMoney = false, bool isTax = false, bool isCommissionsFinished=false)
        {
            if (caseEventList != null)
            {
                foreach (CaseEvent caseEvent in caseEventList)
                {
                    switch (caseEvent.EventType)
                    {
                        case CaseStatus.Unkown:
                            break;
                        case CaseStatus.YJ1:
                            IsYJ1 = true;
                            if(caseEvent.SourceStatus==CaseStatus.ZB5)
                            {
                                IsHA1 = false;
                            }
                            YJ1DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.HA1:
                            IsHA1 = true;
                            HA1DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.HA2:
                            IsHA2 = true;
                            HA2DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.HA3:
                            IsHA3 = true;
                            HA3DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.HA4:
                            IsHA4 = true;
                            _HA4Time = caseEvent.CreateDate;
                            HA4DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.HA5:
                            IsHA5 = true;
                            _HA5Time = caseEvent.CreateDate;
                            HA5DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.HA6:
                            IsShowHA6 = true;
                            _HA6Time = caseEvent.CreateDate;
                            IsHA6 = true;
                            HA6DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.HD1:
                            IsHD1 = true;
                            HD1DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.HD2:
                            IsHD2 = true;
                            HD2DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.HD3:
                            IsHD3 = true;
                            HD3DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.HD4:
                            IsHD4 = true;
                            HD4DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.HD5:
                            IsHD5 = true;
                            HD5DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.HD6:
                            IsHD6 = true;
                            HD6DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.JY1:
                            IsJY1 = true;
                            JY1DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.JY2:
                            IsJY2 = true;
                            JY2DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.JY3:
                            IsJY3 = true;
                            JY3DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.JY4:
                            IsJY4 = true;
                            _JY4Time = caseEvent.CreateDate;
                            JY4DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.JY5:
                            IsJY5 = true;
                            JY5DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.JY6:
                            IsJY6 = true;
                            IsJY6DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.JY9:
                            IsJY9 = true;
                            _JY9Time = caseEvent.CreateDate;
                            JY9DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.JY10:
                            IsJY10 = true;
                            JY10DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        //JY11和JY13是根据不同领证人的过户状态
                        case CaseStatus.JY11A1:
                        case CaseStatus.JY11:
                            IsJY11 = true;
                            JY11DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.JY12:
                            IsJY12 = true;
                            JY12DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.DK1:
                            IsDK1 = true;
                            _DK1Time = caseEvent.CreateDate;
                            DK1DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            IsShowDK1 = true;
                            break;
                        case CaseStatus.DK2:
                            IsDK2 = true;
                            _DK2Time = caseEvent.CreateDate;
                            DK2DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.DK3:
                            IsDK3 = true;
                            _DK3Time = caseEvent.CreateDate;
                            DK3DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.DK4:
                            IsDK4 = true;
                            _DK4Time = caseEvent.CreateDate;
                            DK4DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.DK5:
                            IsDK5 = true;
                            _DK5Time = caseEvent.CreateDate;
                            DK5DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.DK6:
                            IsDK6 = true;
                            DK6DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.DK7:
                            IsDK7 = true;
                            DK7DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.DK8:
                            IsDK8 = true;
                            DK8DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.DK9:
                            IsDK9 = true;
                            _DK9Time = caseEvent.CreateDate;
                            DK9DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.DK10:
                            IsDK10 = true;
                            _DK10Time = caseEvent.CreateDate;
                            DK10DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.ZB1:
                            IsZB1 = true;
                            if (caseEvent.SourceStatus == CaseStatus.JY1)
                            {
                                IsJY1 = false;
                            }
                                
                            ZB1DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.ZB2:
                            if (IsFromYJ1)
                            {
                                IsZB2 = true;
                                ZB2DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            }
                            else if (IsFromHA1)
                            {
                                IsZB2A = true;
                                ZB2ADetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            }
                            break;
                        case CaseStatus.ZB3:
                            IsZB3 = true;
                            ZB3DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            IsShowZB3 = true;
                            break;
                        case CaseStatus.ZB5:
                            if (caseEvent.SourceStatus == CaseStatus.YJ1)
                            {
                                IsFromYJ1 = true;
                            }
                            else if (caseEvent.SourceStatus == CaseStatus.HA1)
                            {
                                IsFromHA1 = true;
                            }
                            break;
                        case CaseStatus.ZB7:
                            IsZB7 = true;
                            ZB7DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.ZB8:
                            IsZB8 = true;
                            ZB8DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.ZB9:
                            IsZB9 = true;
                            ZB9DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.ZB11:
                            if(caseEvent.SourceStatus==CaseStatus.DK2)
                            { 
                            IsZB11 = true;
                            ZB11DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            }
                            else if(caseEvent.SourceStatus==CaseStatus.DK9)
                            {
                                IsZB11A = true; 
                                ZB11ADetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            }
                                break;
                        case CaseStatus.ZB12:
                            IsZB12 = true;
                            ZB12DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        case CaseStatus.ZB13:
                            IsZB13 = true;
                            ZB13DetailInfo = string.Format(DETAIL_INFO_FORMAT, caseEvent.CreateDate.ToString("yyyy/MM/dd HH:mm"), UserServiceProxy.GetUserName((long)caseEvent.CreateUserSysNo));
                            break;
                        default:
                            break;
                    }
                }
                IsHouseMoneyFinished = isHouseMoney;
                IsTaxFinished = isTax;
                IsCommissionsFinished=isCommissionsFinished;
               
                if(IsZB13)
                {
                    IsHouseMoneyFinished = true;
                    IsTaxFinished = true;
                    IsCommissionsFinished = true;
                }
                if (IsHA5)
                {
                    IsHA4 = true;
                    AuditReport = true;
                    AuditReportInfo = HA5DetailInfo;
                }
                else
                {
                    if(IsHA6)
                    {
                        if (_HA4Time > _HA6Time)
                        {
                            IsHA4 = true;
                        }
                        else
                        {
                            IsHA4 = false;
                        }
                        if(_HA5Time<_HA6Time)
                        {
                            AuditReportInfo = HA6DetailInfo;
                        }
                    }
                }
                AuditResult = IsDK10 || IsDK5 || IsDK9;
                if ((_DK5Time > _DK9Time) && (_DK5Time > _DK10Time))
                {
                    AuditResultInfo = DK5DetailInfo;
                }
                else if ((_DK9Time > _DK5Time) && (_DK9Time > _DK10Time))
                {
                    AuditResultInfo = DK9DetailInfo;
                }
                else if ((_DK10Time > _DK5Time) && (_DK10Time > _DK9Time))
                {
                    AuditResultInfo = DK10DetailInfo;
                }
                if(IsDK5)
                {
                    IsDK2 = true;
                    IsDK3 = true;
                    AuditResultInfo = DK5DetailInfo;
                }
                else
                {
                    if (_DK1Time > _DK9Time)
                    {
                        IsDK1 = true;
                        if(_DK2Time>_DK1Time)
                        {
                            IsDK2 = true;
                        }
                        else
                        {
                            IsDK2 = false;
                        }

                        if (_DK3Time > _DK2Time && _DK3Time > _DK1Time)
                        {
                            IsDK3 = true;
                        }
                        else
                        {
                            IsDK3 = false;
                        } 
                        if (_DK4Time > _DK3Time && _DK4Time > _DK9Time)
                        {
                            IsDK4 = true;
                        }
                        else
                        {
                            IsDK4 = false;
                        }
                    }
                }
                if(IsDK9)
                {
                    if (_DK1Time > _DK9Time)
                    {
                        IsShowDK9 = true;
                    }
                    else
                    {
                        IsShowDK9 = false;
                    }
                }

                if(IsJY6)
                {
                    IsJY4 = true;
                    LimitsPurchases = true;
                    LimitsPurchasesInfo = IsJY6DetailInfo;
                }
                else
                {
                    if(IsJY9)
                    {
                        if (_JY4Time > _JY9Time)
                        {
                            IsJY4 = true;
                        }
                        else
                        {
                            LimitsPurchases = true;
                            LimitsPurchasesInfo = JY9DetailInfo;
                        }

                    }
                }
                if(IsJY9)
                {
                    if (_JY4Time > _JY9Time)
                    {
                        IsShowJY9 = true;
                    }    
                    else
                    {
                        IsShowJY9 = false;
                    }
                }
            }
        }

        #region Property Members
        #region YJ
        private bool isYJ1;

        public bool IsYJ1
        {
            get
            {
                return isYJ1;
            }
            set
            {
                if (isYJ1 != value)
                {
                    isYJ1 = value;
                    NotifyPropertyChanged("IsYJ1");
                }
            }
        }

        private string yj1DetailInfo;

        public string YJ1DetailInfo
        {
            get
            {
                return yj1DetailInfo;
            }
            set
            {
                if (yj1DetailInfo != value)
                {
                    yj1DetailInfo = value;
                    NotifyPropertyChanged("YJ1DetailInfo");
                }
            }
        }

        #endregion

        #region HA
        private bool isHA1;

        public bool IsHA1
        {
            get
            {
                return isHA1;
            }
            set
            {
                if (isHA1 != value)
                {
                    isHA1 = value;
                    NotifyPropertyChanged("IsHA1");
                }
            }
        }

        private string ha1DetailInfo;

        public string HA1DetailInfo
        {
            get
            {
                return ha1DetailInfo;
            }
            set
            {
                if (ha1DetailInfo != value)
                {
                    ha1DetailInfo = value;
                    NotifyPropertyChanged("HA1DetailInfo");
                }
            }
        }

        private bool isHA2;

        public bool IsHA2
        {
            get
            {
                return isHA2;
            }
            set
            {
                if (isHA2 != value)
                {
                    isHA2 = value;
                    NotifyPropertyChanged("IsHA2");
                }
            }
        }
        private string ha2DetailInfo;

        public string  HA2DetailInfo
        {
            get
            {
                return ha2DetailInfo;
            }
            set
            {
                if (ha2DetailInfo != value)
                {
                    ha2DetailInfo = value;
                    NotifyPropertyChanged("HA2DetailInfo");
                }
            }
        }

        private bool isHA3;

        public bool IsHA3
        {
            get
            {
                return isHA3;
            }
            set
            {
                if (isHA3 != value)
                {
                    isHA3 = value;
                    NotifyPropertyChanged("IsHA3");
                }
            }
        }
        private string ha3DetailInfo;

        public string HA3DetailInfo
        {
            get
            {
                return ha3DetailInfo;
            }
            set
            {
                if (ha3DetailInfo != value)
                {
                    ha3DetailInfo = value;
                    NotifyPropertyChanged("HA3DetailInfo");
                }
            }
        }

        private bool isHA4;

        public bool IsHA4
        {
            get
            {
                return isHA4;
            }
            set
            {
                if (isHA4 != value)
                {
                    isHA4 = value;
                    NotifyPropertyChanged("IsHA4");
                }
            }
        }
        private string ha4DetailInfo;

        public string HA4DetailInfo
        {
            get
            {
                return ha4DetailInfo;
            }
            set
            {
                if (ha4DetailInfo != value)
                {
                    ha4DetailInfo = value;
                    NotifyPropertyChanged("HA4DetailInfo");
                }
            }
        }

        private bool isHA5;

        public bool IsHA5
        {
            get
            {
                return isHA5;
            }
            set
            {
                if (isHA5 != value)
                {
                    isHA5 = value;
                    NotifyPropertyChanged("IsHA5");
                }
            }
        }

        private string ha5DetailInfo;

        public string HA5DetailInfo
        {
            get
            {
                return ha5DetailInfo;
            }
            set
            {
                if (ha5DetailInfo != value)
                {
                    ha5DetailInfo = value;
                    NotifyPropertyChanged("HA5DetailInfo");
                }
            }
        }

        private bool isHA6;

        public bool IsHA6
        {
            get
            {
                return isHA6;
            }
            set
            {
                if (isHA6 != value)
                {
                    isHA6 = value;
                    NotifyPropertyChanged("IsHA6");
                }
            }
        }
        private string ha6DetailInfo;

        public string HA6DetailInfo
        {
            get
            {
                return ha6DetailInfo;
            }
            set
            {
                if (ha6DetailInfo != value)
                {
                    ha6DetailInfo = value;
                    NotifyPropertyChanged("HA6DetailInfo");
                }
            }
        }


        #endregion

        #region JY
        private bool isJY1;

        public bool IsJY1
        {
            get
            {
                return isJY1;
            }
            set
            {
                if (isJY1 != value)
                {
                    isJY1 = value;
                    NotifyPropertyChanged("IsJY1");
                }
            }
        }
        private string jy1DetailInfo;

        public string JY1DetailInfo
        {
            get
            {
                return jy1DetailInfo;
            }
            set
            {
                if (jy1DetailInfo != value)
                {
                    jy1DetailInfo = value;
                    NotifyPropertyChanged("JY1DetailInfo");
                }
            }
        }

        private bool isJY2;

        public bool IsJY2
        {
            get
            {
                return isJY2;
            }
            set
            {
                if (isJY2 != value)
                {
                    isJY2 = value;
                    NotifyPropertyChanged("IsJY2");
                }
            }
        }
        private string jy2DetailInfo;

        public string JY2DetailInfo
        {
            get
            {
                return jy2DetailInfo;
            }
            set
            {
                if (jy2DetailInfo != value)
                {
                    jy2DetailInfo = value;
                    NotifyPropertyChanged("JY2DetailInfo");
                }
            }
        }

        private bool isJY3;

        public bool IsJY3
        {
            get
            {
                return isJY3;
            }
            set
            {
                if (isJY3 != value)
                {
                    isJY3 = value;
                    NotifyPropertyChanged("IsJY3");
                }
            }
        }
        private string jy3DetailInfo;

        public string JY3DetailInfo
        {
            get
            {
                return jy3DetailInfo;
            }
            set
            {
                if (jy3DetailInfo != value)
                {
                    jy3DetailInfo = value;
                    NotifyPropertyChanged("JY3DetailInfo");
                }
            }
        }

        private bool isJY4;

        public bool IsJY4
        {
            get
            {
                return isJY4;
            }
            set
            {
                if (isJY4 != value)
                {
                    isJY4 = value;
                    NotifyPropertyChanged("IsJY4");
                }
            }
        }
        private string jy4DetailInfo;

        public string JY4DetailInfo
        {
            get
            {
                return jy4DetailInfo;
            }
            set
            {
                if (jy4DetailInfo != value)
                {
                    jy4DetailInfo = value;
                    NotifyPropertyChanged("JY4DetailInfo");
                }
            }
        }
        private bool isJY5;
        public bool IsJY5
        {
            get
            {
                return isJY5;
            }
            set
            {
                if (isJY5 != value)
                {
                    isJY5 = value;
                    NotifyPropertyChanged("IsJY5");
                }
            }
        }
        private string jy5DetailInfo;
        public string JY5DetailInfo
        {
            get
            {
                return jy5DetailInfo;
            }
            set
            {
                if (jy5DetailInfo != value)
                {
                    jy5DetailInfo = value;
                    NotifyPropertyChanged("JY5DetailInfo");
                }
            }
        }


        private bool isJY6;

        public bool IsJY6
        {
            get
            {
                return isJY6;
            }
            set
            {
                if (isJY6 != value)
                {
                    isJY6 = value;
                    NotifyPropertyChanged("IsJY6");
                }
            }
        }
        private string isJY6DetailInfo;
        public string IsJY6DetailInfo
        {
            get
            {
                return isJY6DetailInfo;
            }
            set
            {
                if (isJY6DetailInfo != value)
                {
                    isJY6DetailInfo = value;
                    NotifyPropertyChanged("IsJY6DetailInfo");
                }
            }
        }


        private bool isJY9;

        public bool IsJY9
        {
            get
            {
                return isJY9;
            }
            set
            {
                if (isJY9 != value)
                {
                    isJY9 = value;
                    NotifyPropertyChanged("IsJY9");
                }
            }
        }
        private string jy9DetailInfo;

        public string JY9DetailInfo
        {
            get
            {
                return jy9DetailInfo;
            }
            set
            {
                if (jy9DetailInfo != value)
                {
                    jy9DetailInfo = value;
                    NotifyPropertyChanged("JY9DetailInfo");
                }
            }
        }

        private bool isJY10;

        public bool IsJY10
        {
            get
            {
                return isJY10;
            }
            set
            {
                if (isJY10 != value)
                {
                    isJY10 = value;
                    NotifyPropertyChanged("IsJY10");
                }
            }
        }
        private string jy10DetailInfo;

        public string JY10DetailInfo
        {
            get
            {
                return jy10DetailInfo;
            }
            set
            {
                if (jy10DetailInfo != value)
                {
                    jy10DetailInfo = value;
                    NotifyPropertyChanged("JY10DetailInfo");
                }
            }
        }

        private bool isJY11;

        public bool IsJY11
        {
            get
            {
                return isJY11;
            }
            set
            {
                if (isJY11 != value)
                {
                    isJY11 = value;
                    NotifyPropertyChanged("IsJY11");
                }
            }
        }
        private string jy11DetailInfo;

        public string JY11DetailInfo
        {
            get
            {
                return jy11DetailInfo;
            }
            set
            {
                if (jy11DetailInfo != value)
                {
                    jy11DetailInfo = value;
                    NotifyPropertyChanged("JY11DetailInfo");
                }
            }
        }

        private bool isJY12;

        public bool IsJY12
        {
            get
            {
                return isJY12;
            }
            set
            {
                if (isJY12 != value)
                {
                    isJY12 = value;
                    NotifyPropertyChanged("IsJY12");
                }
            }
        }

        private string jy12DetailInfo;

        public string JY12DetailInfo
        {
            get
            {
                return jy12DetailInfo;
            }
            set
            {
                if (jy12DetailInfo != value)
                {
                    jy12DetailInfo = value;
                    NotifyPropertyChanged("JY12DetailInfo");
                }
            }
        }

        #endregion

        #region ZB
        private bool isZB1;

        public bool IsZB1
        {
            get
            {
                return isZB1;
            }
            set
            {
                if (isZB1 != value)
                {
                    isZB1 = value;
                    NotifyPropertyChanged("IsZB1");
                }
            }
        }
        private string zb1DetailInfo;

        public string ZB1DetailInfo
        {
            get
            {
                return zb1DetailInfo;
            }
            set
            {
                if (zb1DetailInfo != value)
                {
                    zb1DetailInfo = value;
                    NotifyPropertyChanged("ZB1DetailInfo");
                }
            }
        }
        private bool isZB2;
        public bool IsZB2
        {
            get
            {
                return isZB2;
            }
            set
            {
                if (isZB2 != value)
                {
                    isZB2 = value;
                    NotifyPropertyChanged("IsZB2");
                }
            }
        }

        private string zb2DetailInfo;
        public string ZB2DetailInfo
        {
            get
            {
                return zb2DetailInfo;
            }
            set
            {
                if (zb2DetailInfo != value)
                {
                    zb2DetailInfo = value;
                    NotifyPropertyChanged("ZB2DetailInfo");
                }
            }
        }
        private bool isZB2A;
        public bool IsZB2A
        {
            get
            {
                return isZB2A;
            }
            set
            {
                if (isZB2A != value)
                {
                    isZB2A = value;
                    NotifyPropertyChanged("IsZB2A");
                }
            }
        }
        private string zb2ADetailInfo;
        public string ZB2ADetailInfo
        {
            get
            {
                return zb2ADetailInfo;
            }
            set
            {
                if (zb2ADetailInfo != value)
                {
                    zb2ADetailInfo = value;
                    NotifyPropertyChanged("ZB2ADetailInfo");
                }
            }
        }


        private bool isZB3;

        public bool IsZB3
        {
            get
            {
                return isZB3;
            }
            set
            {
                if (isZB3 != value)
                {
                    isZB3 = value;
                    NotifyPropertyChanged("IsZB3");
                }
            }
        }
        private string zb3DetailInfo;

        public string ZB3DetailInfo
        {
            get
            {
                return zb3DetailInfo;
            }
            set
            {
                if (zb3DetailInfo != value)
                {
                    zb3DetailInfo = value;
                    NotifyPropertyChanged("ZB3DetailInfo");
                }
            }
        }
        private bool isZB5;
        public bool IsZB5
        {
            get
            {
                return isZB5;
            }
            set
            {
                if (isZB5 != value)
                {
                    isZB5 = value;
                    NotifyPropertyChanged("IsZB5");
                }
            }
        }
        private string zb5DetailInfo;
        public string ZB5DetailInfo
        {
            get
            {
                return zb5DetailInfo;
            }
            set
            {
                if (zb5DetailInfo != value)
                {
                    zb5DetailInfo = value;
                    NotifyPropertyChanged("ZB5DetailInfo");
                }
            }
        }


        private bool isZB7;

        public bool IsZB7
        {
            get
            {
                return isZB7;
            }
            set
            {
                if (isZB7 != value)
                {
                    isZB7 = value;
                    NotifyPropertyChanged("IsZB7");
                }
            }
        }
        private string zb7DetailInfo;

        public string ZB7DetailInfo
        {
            get
            {
                return zb7DetailInfo;
            }
            set
            {
                if (zb7DetailInfo != value)
                {
                    zb7DetailInfo = value;
                    NotifyPropertyChanged("ZB7DetailInfo");
                }
            }
        }

        private bool isZB8;

        public bool IsZB8
        {
            get
            {
                return isZB8;
            }
            set
            {
                if (isZB8 != value)
                {
                    isZB8 = value;
                    NotifyPropertyChanged("IsZB8");
                }
            }
        }

        private string zb8DetailInfo;

        public string ZB8DetailInfo
        {
            get
            {
                return zb8DetailInfo;
            }
            set
            {
                if (zb8DetailInfo != value)
                {
                    zb8DetailInfo = value;
                    NotifyPropertyChanged("ZB8DetailInfo");
                }
            }
        }

        private bool isZB9;

        public bool IsZB9
        {
            get
            {
                return isZB9;
            }
            set
            {
                if (isZB9 != value)
                {
                    isZB9 = value;
                    NotifyPropertyChanged("IsZB9");
                }
            }
        }

        private string zb9DetailInfo;

        public string ZB9DetailInfo
        {
            get
            {
                return zb9DetailInfo;
            }
            set
            {
                if (zb9DetailInfo != value)
                {
                    zb9DetailInfo = value;
                    NotifyPropertyChanged("ZB9DetailInfo");
                }
            }
        }

        private bool isZB11;

        public bool IsZB11
        {
            get
            {
                return isZB11;
            }
            set
            {
                if (isZB11 != value)
                {
                    isZB11 = value;
                    NotifyPropertyChanged("IsZB11");
                }
            }
        }
        private string zb11DetailInfo;

        public string ZB11DetailInfo
        {
            get
            {
                return zb11DetailInfo;
            }
            set
            {
                if (zb11DetailInfo != value)
                {
                    zb11DetailInfo = value;
                    NotifyPropertyChanged("ZB11DetailInfo");
                }
            }
        }
        private bool isZB11A;
        public bool IsZB11A
        {
            get
            {
                return isZB11A;
            }
            set
            {
                if (isZB11A != value)
                {
                    isZB11A = value;
                    NotifyPropertyChanged("IsZB11A");
                }
            }
        }
        private string zb11ADetailInfo;
        public string ZB11ADetailInfo
        {
            get
            {
                return zb11ADetailInfo;
            }
            set
            {
                if (zb11ADetailInfo != value)
                {
                    zb11ADetailInfo = value;
                    NotifyPropertyChanged("ZB11ADetailInfo");
                }
            }
        }


        private bool isZB12;
        public bool IsZB12
        {
            get
            {
                return isZB12;
            }
            set
            {
                if (isZB12 != value)
                {
                    isZB12 = value;
                    NotifyPropertyChanged("IsZB12");
                }
            }
        }

        private string zb12DetailInfo;
        public string ZB12DetailInfo
        {
            get
            {
                return zb12DetailInfo;
            }
            set
            {
                if (zb12DetailInfo != value)
                {
                    zb12DetailInfo = value;
                    NotifyPropertyChanged("ZB12DetailInfo");
                }
            }
        }
        private bool isZB13;
        public bool IsZB13
        {
            get
            {
                return isZB13;
            }
            set
            {
                if (isZB13 != value)
                {
                    isZB13 = value;
                    NotifyPropertyChanged("IsZB13");
                }
            }
        }
        private string zb13DetailInfo;
        public string ZB13DetailInfo
        {
            get
            {
                return zb13DetailInfo;
            }
            set
            {
                if (zb13DetailInfo != value)
                {
                    zb13DetailInfo = value;
                    NotifyPropertyChanged("ZB13DetailInfo");
                }
            }
        }


        #endregion

        #region HD
        private bool isHD1;

        public bool IsHD1
        {
            get
            {
                return isHD1;
            }
            set
            {
                if (isHD1 != value)
                {
                    isHD1 = value;
                    NotifyPropertyChanged("IsHD1");
                }
            }
        }
        private string hd1DetailInfo;

        public string HD1DetailInfo
        {
            get
            {
                return hd1DetailInfo;
            }
            set
            {
                if (hd1DetailInfo != value)
                {
                    hd1DetailInfo = value;
                    NotifyPropertyChanged("HD1DetailInfo");
                }
            }
        }
        private bool isHD2;

        public bool IsHD2
        {
            get
            {
                return isHD2;
            }
            set
            {
                if (isHD2 != value)
                {
                    isHD2 = value;
                    NotifyPropertyChanged("IsHD2");
                }
            }
        }
        private string hd2DetailInfo;

        public string HD2DetailInfo
        {
            get
            {
                return hd2DetailInfo;
            }
            set
            {
                if (hd2DetailInfo != value)
                {
                    hd2DetailInfo = value;
                    NotifyPropertyChanged("HD2DetailInfo");
                }
            }
        }

        private bool isHD3;

        public bool IsHD3
        {
            get
            {
                return isHD3;
            }
            set
            {
                if (isHD3 != value)
                {
                    isHD3 = value;
                    NotifyPropertyChanged("IsHD3");
                }
            }
        }
        private string hd3DetailInfo;

        public string HD3DetailInfo
        {
            get
            {
                return hd3DetailInfo;
            }
            set
            {
                if (hd3DetailInfo != value)
                {
                    hd3DetailInfo = value;
                    NotifyPropertyChanged("HD3DetailInfo");
                }
            }
        }

        private bool isHD4;

        public bool IsHD4
        {
            get
            {
                return isHD4;
            }
            set
            {
                if (isHD4 != value)
                {
                    isHD4 = value;
                    NotifyPropertyChanged("IsHD4");
                }
            }
        }
        private string hd4DetailInfo;

        public string HD4DetailInfo
        {
            get
            {
                return hd4DetailInfo;
            }
            set
            {
                if (hd4DetailInfo != value)
                {
                    hd4DetailInfo = value;
                    NotifyPropertyChanged("HD4DetailInfo");
                }
            }
        }

        private string hd5DetailInfo;

        public string HD5DetailInfo
        {
            get
            {
                return hd5DetailInfo;
            }
            set
            {
                if (hd5DetailInfo != value)
                {
                    hd5DetailInfo = value;
                    NotifyPropertyChanged("HD5DetailInfo");
                }
            }
        }

        private bool isHD5;

        public bool IsHD5
        {
            get
            {
                return isHD5;
            }
            set
            {
                if (isHD5 != value)
                {
                    isHD5 = value;
                    NotifyPropertyChanged("IsHD5");
                }
            }
        }
        private bool isHD6;

        public bool IsHD6
        {
            get
            {
                return isHD6;
            }
            set
            {
                if (isHD6 != value)
                {
                    isHD6 = value;
                    NotifyPropertyChanged("IsHD6");
                }
            }
        }
        private string hd6DetailInfo;

        public string HD6DetailInfo
        {
            get
            {
                return hd6DetailInfo;
            }
            set
            {
                if (hd6DetailInfo != value)
                {
                    hd6DetailInfo = value;
                    NotifyPropertyChanged("HD6DetailInfo");
                }
            }
        }

        #endregion

        #region DK
        private bool isDK1;

        public bool IsDK1
        {
            get
            {
                return isDK1;
            }
            set
            {
                if (isDK1 != value)
                {
                    isDK1 = value;
                    NotifyPropertyChanged("IsDK1");
                }
            }
        }
        private string dk1DetailInfo;

        public string DK1DetailInfo
        {
            get
            {
                return dk1DetailInfo;
            }
            set
            {
                if (dk1DetailInfo != value)
                {
                    dk1DetailInfo = value;
                    NotifyPropertyChanged("DK1DetailInfo");
                }
            }
        }

        private bool isDK2;

        public bool IsDK2
        {
            get
            {
                return isDK2;
            }
            set
            {
                if (isDK2 != value)
                {
                    isDK2 = value;
                    NotifyPropertyChanged("IsDK2");
                }
            }
        }
        private string dk2DetailInfo;

        public string DK2DetailInfo
        {
            get
            {
                return dk2DetailInfo;
            }
            set
            {
                if (dk2DetailInfo != value)
                {
                    dk2DetailInfo = value;
                    NotifyPropertyChanged("DK2DetailInfo");
                }
            }
        }

        private bool isDK3;

        public bool IsDK3
        {
            get
            {
                return isDK3;
            }
            set
            {
                if (isDK3 != value)
                {
                    isDK3 = value;
                    NotifyPropertyChanged("IsDK3");
                }
            }
        }
        private string dk3DetailInfo;

        public string DK3DetailInfo
        {
            get
            {
                return dk3DetailInfo;
            }
            set
            {
                if (dk3DetailInfo != value)
                {
                    dk3DetailInfo = value;
                    NotifyPropertyChanged("DK3DetailInfo");
                }
            }
        }
        private bool isDK4;
        public bool IsDK4
        {
            get
            {
                return isDK4;
            }
            set
            {
                if (isDK4 != value)
                {
                    isDK4 = value;
                    NotifyPropertyChanged("IsDK4");
                }
            }
        }

        private string dk4DetailInfo;
        public string DK4DetailInfo
        {
            get
            {
                return dk4DetailInfo;
            }
            set
            {
                if (dk4DetailInfo != value)
                {
                    dk4DetailInfo = value;
                    NotifyPropertyChanged("DK4DetailInfo");
                }
            }
        }


        private bool isDK5;

        public bool IsDK5
        {
            get
            {
                return isDK5;
            }
            set
            {
                if (isDK5 != value)
                {
                    isDK5 = value;
                    NotifyPropertyChanged("IsDK5");
                }
            }
        }
        private string dk5DetailInfo;

        public string DK5DetailInfo
        {
            get
            {
                return dk5DetailInfo;
            }
            set
            {
                if (dk5DetailInfo != value)
                {
                    dk5DetailInfo = value;
                    NotifyPropertyChanged("DK5DetailInfo");
                }
            }
        }

        private bool isDK6;

        public bool IsDK6
        {
            get
            {
                return isDK6;
            }
            set
            {
                if (isDK6 != value)
                {
                    isDK6 = value;
                    NotifyPropertyChanged("IsDK6");
                }
            }
        }

        private string dk6DetailInfo;

        public string DK6DetailInfo
        {
            get
            {
                return dk6DetailInfo;
            }
            set
            {
                if (dk6DetailInfo != value)
                {
                    dk6DetailInfo = value;
                    NotifyPropertyChanged("DK6DetailInfo");
                }
            }
        }

        private bool isDK7;

        public bool IsDK7
        {
            get
            {
                return isDK7;
            }
            set
            {
                if (isDK7 != value)
                {
                    isDK7 = value;
                    NotifyPropertyChanged("IsDK7");
                }
            }
        }
        private string dk7DetailInfo;

        public string DK7DetailInfo
        {
            get
            {
                return dk7DetailInfo;
            }
            set
            {
                if (dk7DetailInfo != value)
                {
                    dk7DetailInfo = value;
                    NotifyPropertyChanged("DK7DetailInfo");
                }
            }
        }

        private bool isDK8;

        public bool IsDK8
        {
            get
            {
                return isDK8;
            }
            set
            {
                if (isDK8 != value)
                {
                    isDK8 = value;
                    NotifyPropertyChanged("IsDK8");
                }
            }
        }
        private string dk8DetailInfo;

        public string DK8DetailInfo
        {
            get
            {
                return dk8DetailInfo;
            }
            set
            {
                if (dk8DetailInfo != value)
                {
                    dk8DetailInfo = value;
                    NotifyPropertyChanged("DK8DetailInfo");
                }
            }
        }

        private bool isDK9;

        public bool IsDK9
        {
            get
            {
                return isDK9;
            }
            set
            {
                if (isDK9 != value)
                {
                    isDK9 = value;
                    NotifyPropertyChanged("IsDK9");
                }
            }
        }

        private string dk9DetailInfo;

        public string DK9DetailInfo
        {
            get
            {
                return dk9DetailInfo;
            }
            set
            {
                if (dk9DetailInfo != value)
                {
                    dk9DetailInfo = value;
                    NotifyPropertyChanged("DK9DetailInfo");
                }
            }
        }
        private bool isDK10;

        public bool IsDK10
        {
            get
            {
                return isDK10;
            }
            set
            {
                if (isDK10 != value)
                {
                    isDK10 = value;
                    NotifyPropertyChanged("IsDK10");
                }
            }
        }
        private string dk10DetailInfo;

        public string DK10DetailInfo
        {
            get
            {
                return dk10DetailInfo;
            }
            set
            {
                if (dk10DetailInfo != value)
                {
                    dk10DetailInfo = value;
                    NotifyPropertyChanged("DK10DetailInfo");
                }
            }
        }

        #endregion

        #region 审核报告、审批结果、限购结果、房款、税费、佣金

        private bool auditReport;
        public bool AuditReport
        {
            get
            {
                return auditReport;
            }
            set
            {
                if (auditReport != value)
                {
                    auditReport = value;
                    NotifyPropertyChanged("AuditReport");
                }
            }
        }

        private string auditReportInfo;
        public string AuditReportInfo
        {
            get
            {
                return auditReportInfo;
            }
            set
            {
                if (auditReportInfo != value)
                {
                    auditReportInfo = value;
                    NotifyPropertyChanged("AuditReportInfo");
                }
            }
        }
        
        private bool auditResult;
        public bool AuditResult
        {
            get
            {
                return auditResult;
            }
            set
            {
                if (auditResult != value)
                {
                    auditResult = value;
                    NotifyPropertyChanged("AuditResult");
                }
            }
        }

        private string auditResultInfo;
        public string AuditResultInfo
        {
            get
            {
                return auditResultInfo;
            }
            set
            {
                if (auditResultInfo != value)
                {
                    auditResultInfo = value;
                    NotifyPropertyChanged("AuditResultInfo");
                }
            }
        }


        private bool limitsPurchases;
        public bool LimitsPurchases
        {
            get
            {
                return limitsPurchases;
            }
            set
            {
                if (limitsPurchases != value)
                {
                    limitsPurchases = value;
                    NotifyPropertyChanged("LimitsPurchases");
                }
            }
        }

        private string limitsPurchasesInfo;
        public string LimitsPurchasesInfo
        {
            get
            {
                return limitsPurchasesInfo;
            }
            set
            {
                if (limitsPurchasesInfo != value)
                {
                    limitsPurchasesInfo = value;
                    NotifyPropertyChanged("LimitsPurchasesInfo");
                }
            }
        }
        
        private bool isHouseMoneyFinished;

        public bool IsHouseMoneyFinished
        {
            get
            {
                return isHouseMoneyFinished;
            }
            set
            {
                if (isHouseMoneyFinished != value)
                {
                    isHouseMoneyFinished = value;
                    NotifyPropertyChanged("IsHouseMoneyFinished");
                }
            }
        }
        private bool isTaxFinished;

        public bool IsTaxFinished
        {
            get
            {
                return isTaxFinished;
            }
            set
            {
                if (isTaxFinished != value)
                {
                    isTaxFinished = value;
                    NotifyPropertyChanged("IsTaxFinished");
                }
            }
        }

        private bool isCommissionsFinished;

        public bool IsCommissionsFinished
        {
            get
            {
                return isCommissionsFinished;
            }
            set
            {
                if (isCommissionsFinished != value)
                {
                    isCommissionsFinished = value;
                    NotifyPropertyChanged("IsCommissionsFinished");
                }
            }
        }

        #endregion

        #region 是否显示卖方还贷、贷款办理、审核不通过、限购、审批结果不通过、结案
        private bool isShowZB3;
        public bool IsShowZB3
        {
            get
            {
                return isShowZB3;
            }
            set
            {
                if (isShowZB3 != value)
                {
                    isShowZB3 = value;
                    NotifyPropertyChanged("IsShowZB3");
                }
            }
        }

        private bool isShowDK1;
        public bool IsShowDK1
        {
            get
            {
                return isShowDK1;
            }
            set
            {
                if (isShowDK1 != value)
                {
                    isShowDK1 = value;
                    NotifyPropertyChanged("IsShowDK1");
                }
            }
        }

        private bool isShowHA6;
        public bool IsShowHA6
        {
            get
            {
                return isShowHA6;
            }
            set
            {
                if (isShowHA6 != value)
                {
                    isShowHA6 = value;
                    NotifyPropertyChanged("IsShowHA6");
                }
            }
        }

        private bool isShowDK9;
        public bool IsShowDK9
        {
            get
            {
                return isShowDK9;
            }
            set
            {
                if (isShowDK9 != value)
                {
                    isShowDK9 = value;
                    NotifyPropertyChanged("IsShowDK9");
                }
            }
        }

        private bool isShowJY9;
        public bool IsShowJY9
        {
            get
            {
                return isShowJY9;
            }
            set
            {
                if (isShowJY9 != value)
                {
                    isShowJY9 = value;
                    NotifyPropertyChanged("IsShowJY9");
                }
            }
        }

        private bool isJA;
        public bool IsJA
        {
            get
            {
                return isJA;
            }
            set
            {
                if (isJA != value)
                {
                    isJA = value;
                    NotifyPropertyChanged("IsJA");
                }
            }
        }

        private bool isShowJY9Line;
        public bool IsShowJY9Line
        {
            get
            {
                return isShowJY9Line;
            }
            set
            {
                if (isShowJY9Line != value)
                {
                    isShowJY9Line = value;
                    NotifyPropertyChanged("IsShowJY9Line");
                }
            }
        }

        private bool isShowDK9Line;
        public bool IsShowDK9Line
        {
            get
            {
                return isShowDK9Line;
            }
            set
            {
                if (isShowDK9Line != value)
                {
                    isShowDK9Line = value;
                    NotifyPropertyChanged("IsShowDK9Line");
                }
            }
        }
        private bool isFromYJ1;
        public bool IsFromYJ1
        {
            get
            {
                return isFromYJ1;
            }
            set
            {
                if (isFromYJ1 != value)
                {
                    isFromYJ1 = value;
                    NotifyPropertyChanged("IsFromYJ1");
                }
            }
        }
        private bool isFromHA1;
        public bool IsFromHA1
        {
            get
            {
                return isFromHA1;
            }
            set
            {
                if (isFromHA1 != value)
                {
                    isFromHA1 = value;
                    NotifyPropertyChanged("IsFromHA1");
                }
            }
        }

        #endregion
        #endregion


        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
