﻿using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.UI.WPF.Model
{
    /// <summary>
    /// 枚举帮助类
    /// </summary>
    public class EnumHelper
    {
        public string DisplayMember { get; set; }

        public long ValueMember { get; set; }


        public static List<EnumHelper> EnumToEnumHelperList(Type t)
        {
            var array = System.Enum.GetValues(t);    // 获取枚举的所有值
            var enumList = new List<EnumHelper>();
            foreach (var arr in array)
            {
                var text = GetEnumDesc(arr);
                var value = (int)arr;
                enumList.Add(new EnumHelper { DisplayMember = text, ValueMember = value });
            }
            return enumList;
        }


        public static string GetEnumDesc(object e)
        {
            var enumInfo = e.GetType().GetField(e.ToString());
            var enumAttributes = (DescriptionAttribute[])enumInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return enumAttributes.Length > 0 ? enumAttributes[0].Description : e.ToString();
        }

        /// <summary>
        /// 获取民族
        /// </summary>
        /// <returns></returns>
        public static List<EnumHelper> InitNationToCombobox()
        {
            var array = System.Enum.GetValues(typeof(Nation));    // 获取枚举的所有值
            var enumList = new List<EnumHelper>();
            foreach (var arr in array)
            {
                var text = GetEnumDesc((Nation)arr);
                var value = (int)arr;
                enumList.Add(new EnumHelper { DisplayMember = text, ValueMember = value });
            }
            return enumList;
        }

        /// <summary>
        /// 获取证件类型
        /// </summary>
        /// 0
        public static List<EnumHelper> InitCertificateTypeToCombobox()
        {
            var array = System.Enum.GetValues(typeof(CertificateType));    // 获取枚举的所有值
            var enumList = new List<EnumHelper>();
            foreach (var arr in array)
            {
                var text = GetEnumDesc((CertificateType)arr);
                var value = (int)arr;
                enumList.Add(new EnumHelper { DisplayMember = text, ValueMember = value });
            }
            return enumList;
        }
        public static long getEnumListSum(Type e)
        {
            var array = System.Enum.GetValues(e);    // 获取枚举的所有值
            long result = 0x0;
            foreach (var arr in array)
            {
                var value = (long)arr;
                result |= value;
            }
            return result;
        }
        /// <summary>
        /// 获取国家类型
        /// </summary>
        public static List<EnumHelper> InitCountryToCombobox()
        {
            var array = System.Enum.GetValues(typeof(CertificateType));    // 获取枚举的所有值
            var enumList = new List<EnumHelper>();
            foreach (var arr in array)
            {
                var text = GetEnumDesc((CertificateType)arr);
                var value = (int)arr;
                enumList.Add(new EnumHelper { DisplayMember = text, ValueMember = value });
            }
            return enumList;
        }


        /// <summary>
        /// 取案件签约状态
        /// </summary>
        /// <returns></returns>
        public static List<EnumHelper> InitCaseStatusToCombobox()
        {
            var enumList = new List<EnumHelper>();
            var array = System.Enum.GetValues(typeof(CaseStatus));    // 获取枚举的所有值
            foreach (var arr in array)
            {
                var text = GetEnumDesc((CaseStatus)arr);
                var value = (long)arr;
                enumList.Add(new EnumHelper { DisplayMember = text, ValueMember = value });
            }
            return enumList;
        }

        /// <summary>
        /// 取案件签约状态
        /// </summary>
        /// <returns></returns>
        public static List<EnumHelper> InitCaseStatusToCombobox(CaseStatus caseStatus)
        {
            var enumList = new List<EnumHelper>();
            var array = System.Enum.GetValues(typeof(CaseStatus));    // 获取枚举的所有值
            foreach (var arr in array)
            {                
                if (caseStatus.HasFlag((CaseStatus)arr))
                {
                    var text = GetEnumDesc((CaseStatus)arr);
                    var value = (long)arr;
                    if (value == 0) text = "全部案件";
                    enumList.Add(new EnumHelper { DisplayMember = text, ValueMember = value });
                }
            }
            return enumList;
        }

        /// <summary>
        /// 贷款类型
        /// </summary>
        /// <returns></returns>
        public static List<EnumHelper> InitCreditTypeToCombobox()
        {
            var enumList = new List<EnumHelper>();
            var array = System.Enum.GetValues(typeof(CreditType));    // 获取枚举的所有值
            foreach (var arr in array)
            {
                var text = GetEnumDesc((CreditType)arr);
                var value = (int)arr;
                if (value == 0) continue;
                enumList.Add(new EnumHelper { DisplayMember = text, ValueMember = value });
            }
            return enumList;        
        }

        /// <summary>
        /// 是否枚举
        /// </summary>
        /// <returns></returns>
        public static List<EnumHelper> InitAllegeToCombobox()
        {
            var enumList = new List<EnumHelper>();
            var array = System.Enum.GetValues(typeof(Allege));    // 获取枚举的所有值
            foreach (var arr in array)
            {
                var text = GetEnumDesc((Allege)arr);
                var value = (int)arr;
                enumList.Add(new EnumHelper { DisplayMember = text, ValueMember = value });
            }
            return enumList;        
        }

        /// <summary>
        /// 有无枚举
        /// </summary>
        /// <returns></returns>
        public static List<EnumHelper> InitBooleToCombobox()
        {
            var enumList = new List<EnumHelper>();
            var array = System.Enum.GetValues(typeof(Boole)); // 获取枚举的所有值
            foreach (var arr in array)
            {
                var text = GetEnumDesc((Boole)arr);
                var value = (int)arr;
                enumList.Add(new EnumHelper { DisplayMember = text, ValueMember = value });
            }
            return enumList;
        }
        
        /// <summary>
        /// 贷款申请对象
        /// </summary>
        /// <returns></returns>
        public static List<EnumHelper> InitLoanObjectToCombobox()
        {
            var enumList = new List<EnumHelper>();
            var array = System.Enum.GetValues(typeof(LoanObject));    // 获取枚举的所有值
            foreach (var arr in array)
            {
                var text = GetEnumDesc((LoanObject)arr);
                var value = (int)arr;
                if (value == 0) continue;
                enumList.Add(new EnumHelper { DisplayMember = text, ValueMember = value });
            }
            return enumList;        
        }

        /// <summary>
        /// 物业性质
        /// </summary>
        /// <returns></returns>
        public static List<EnumHelper> InitTenementNatureToCombobox()
        {
            var enumList = new List<EnumHelper>();
            var array = System.Enum.GetValues(typeof(TenementNature));
            enumList.Add(new EnumHelper { ValueMember = -999, DisplayMember = "请选择" });
            foreach (var arr in array)
            {
                var text = GetEnumDesc((TenementNature)arr);
                var value = (int)arr;
                if (value == 0) continue;
                enumList.Add(new EnumHelper { DisplayMember = text, ValueMember = value });
            }
            return enumList;        
        }
        
        /// <summary>
        /// 环线位置
        /// </summary>
        /// <returns></returns>
        public static List<EnumHelper> InitLinkLocationToCombobox()
        {
            var enumList = new List<EnumHelper>();
            var array = System.Enum.GetValues(typeof(LinkLocation));    // 获取枚举的所有值
            enumList.Add(new EnumHelper { ValueMember = -999, DisplayMember = "请选择" });
            foreach (var arr in array)
            {
                var text = GetEnumDesc((LinkLocation)arr);
                var value = (int)arr;
                if (value == 0) continue;
                enumList.Add(new EnumHelper { DisplayMember = text, ValueMember = value });
            }           
            return enumList;        
        }


        /// <summary>
        /// 居住类型
        /// </summary>
        /// <returns></returns>
        public static List<EnumHelper> InitResideTypeToCombobox()
        {
            var enumList = new List<EnumHelper>();
            var array = System.Enum.GetValues(typeof(ResideType));    // 获取枚举的所有值
            enumList.Add(new EnumHelper { ValueMember = -999, DisplayMember = "请选择" });
            foreach (var arr in array)
            {
                var text = GetEnumDesc((ResideType)arr);
                var value = (int)arr;
                if (value == 0) continue;
                enumList.Add(new EnumHelper { DisplayMember = text, ValueMember = value });
            }
            return enumList;
        }

        /// <summary>
        /// 获取地址类型
        /// </summary>
        /// <returns></returns>
        public static List<EnumHelper> InitAddressTypeToCombobox()
        {
            var enumList = new List<EnumHelper>();
            var array = System.Enum.GetValues(typeof(AddressType));    // 获取枚举的所有值
            foreach (var arr in array)
            {
                var text = GetEnumDesc((AddressType)arr);
                var value = (int)arr;
                enumList.Add(new EnumHelper { DisplayMember = text, ValueMember = value });
            }
            return enumList;         
        }

        /// <summary>
        /// 获取邮寄时间类型
        /// </summary>
        /// <returns></returns>
        public static List<EnumHelper> InitPostTimeTypeToCombobox()
        {
            var enumList = new List<EnumHelper>();
            var array = System.Enum.GetValues(typeof(PostTimeType));    // 获取枚举的所有值
            foreach (var arr in array)
            {
                var text = GetEnumDesc((PostTimeType)arr);
                var value = (int)arr;
                enumList.Add(new EnumHelper { DisplayMember = text, ValueMember = value });
            }
            return enumList;
        }


        /// <summary>
        /// 获取时间类型
        /// </summary>
        /// <returns></returns>
        public static List<EnumHelper> InitTimeTypeToCombobox()
        {
            var enumList = new List<EnumHelper>();
            var array = System.Enum.GetValues(typeof(PostTime));    // 获取枚举的所有值
            foreach (var arr in array)
            {
                var text = GetEnumDesc((PostTime)arr);
                var value = (int)arr;
                enumList.Add(new EnumHelper { DisplayMember = text, ValueMember = value });
            }
            return enumList;
        }


        /// <summary>
        /// 获取地区类型
        /// </summary>
        /// <returns></returns>
        public static List<EnumHelper> InitReceptionCenterToCombobox()
        {
            var enumList = new List<EnumHelper>();
            var array = System.Enum.GetValues(typeof(ReceptionCenter));    // 获取枚举的所有值
            foreach (var arr in array)
            {
                var text = GetEnumDesc((ReceptionCenter)arr);
                var value = (int)arr;
                enumList.Add(new EnumHelper { DisplayMember = text, ValueMember = value });
            }
            return enumList;
        }

        /// <summary>
        /// 获取付款用途
        /// </summary>
        /// <returns></returns>
        public static List<EnumHelper> InitOrderBizTypeToCombobox(bool isPayMent = false, List<OrderBizType> outStatus=null)
        {
            var enumList = new List<EnumHelper>();
            var array = System.Enum.GetValues(typeof(OrderBizType));    // 获取枚举的所有值
            foreach (var arr in array)
            {

                var text = "代收";
                if (isPayMent)
                {
                   text = "付";
                   if ((OrderBizType)arr == OrderBizType.Collection) continue;
                }
                if (outStatus != null)
                {
                    if (outStatus.Contains((OrderBizType)arr)) continue;
                }
                text +=GetEnumDesc((OrderBizType)arr);
                var value = (int)arr;
                enumList.Add(new EnumHelper { DisplayMember = text, ValueMember = value });
            }
            return enumList;
        }
        /// <summary>
        /// 获取案件来源
        /// </summary>
        /// <returns></returns>
        public static List<EnumHelper> InitSourceTypeCombobox()
        {
            var enumList = new List<EnumHelper>();
            var array = System.Enum.GetValues(typeof(SourceType)); // 获取枚举的所有值
            foreach (var arr in array)
            {
                var text = GetEnumDesc((SourceType)arr);
                var value = (int)arr;
                enumList.Add(new EnumHelper { DisplayMember = text, ValueMember = value });
            }
            return enumList;
        }
    }
}
