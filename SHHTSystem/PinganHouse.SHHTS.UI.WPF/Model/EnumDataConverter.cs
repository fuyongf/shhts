﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace PinganHouse.SHHTS.UI.WPF.Model
{
    [ValueConversion(typeof(Enum), typeof(string))]
    public class EnumDataConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return "--";
            Enum re = (Enum)value;
            return EnumHelper.GetEnumDesc(re);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }

    }


    public class MultiEnumDataConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null) return "--";
            Enum verValue = (Enum)values[0];

            //Enum verValue1 = (Enum)values[1];
            string inComeValue = (string)values[1];
            //入账没有值，直接显示对应状态，否则不显示，用--来替换
            if (inComeValue == "--")
            {
                return EnumHelper.GetEnumDesc(verValue);
            }
            else
            {
                return "--";
            }
            //return "--";
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
