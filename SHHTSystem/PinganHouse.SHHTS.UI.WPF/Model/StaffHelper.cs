﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy;

namespace PinganHouse.SHHTS.UI.WPF.Model
{
    public class StaffHelper
    {
       public static OperationResult ModifyPinganStaff(PinganStaff pinganStaff)
        {
            OperationResult result = new OperationResult(1);
            try
            {
                if (pinganStaff == null)
                    result.ResultMessage = "参数为空";
                result = PinganStaffServiceProxy.ModifyPinganStaff(pinganStaff);
                return result;
            }
            catch (Exception ex)
            {
                result.ResultMessage = ex.Message;
                return result;
            }
        }

        public static OperationResult CollectFingerprint(long userSysNo, string fpCode, string tag, long operatorSysNo)
        {
            OperationResult result = new OperationResult(1);
            try
            {
                if (string.IsNullOrWhiteSpace(fpCode) ||
                    userSysNo < 0 || userSysNo == 0 ||
                    operatorSysNo < 0 || operatorSysNo == 0)
                {
                    result.ResultMessage = "参数错误";
                }
                result = UserServiceProxy.CollectFingerprint(userSysNo, fpCode, tag, operatorSysNo);
                return result;
            }
            catch (Exception ex)
            {
                result.ResultMessage = ex.Message;
                return result;
            }
        }
    }
}
