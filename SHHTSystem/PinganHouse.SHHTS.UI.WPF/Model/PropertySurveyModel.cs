﻿using PinganHouse.SHHTS.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.UI.WPF.Model
{
    public class PropertySurveyModel
    {
        /// <summary>
        /// 产证编号
        /// </summary>
        public string TenementContract { get; set; }

        /// <summary>
        /// 产证地址
        /// </summary>
        public string TenementAddress { get; set; }

        /// <summary>
        /// 案件编号
        /// </summary>
        public string CaseId { get; set; }

        /// <summary>
        /// 申请时间
        /// </summary>
        public DateTime? ApplyDate{ get; set; }

        /// <summary>
        /// 权利人
        /// </summary>
        public string ProprietorName { get; set; }

        /// <summary>
        /// 产调时间
        /// </summary>
        public DateTime? SurveyDate { get; set; }

        /// <summary>
        /// 产调数
        /// </summary>
        public int SurveyNum { get; set; }

        /// <summary>
        /// 产调结果
        /// </summary>
        public string PropertySurveyResult { get; set; }

        /// <summary>
        /// 申请人
        /// </summary>
        public string ApplyName { get; set; }

        /// <summary>
        /// 操作人
        /// </summary>
        public string CreateUserName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateDate { get; set; }

        public static List<PropertySurveyModel> InitListModel(IList<PropertySurveyDto> propertySurveylist)
        {
            var listModel = new List<PropertySurveyModel>();
            if (propertySurveylist == null) return listModel;
            foreach (var item in propertySurveylist)
            {
                var model = new PropertySurveyModel();
                model.CreateDate = item.CreateDate;
                model.SurveyDate = item.SurveyDate;
                //操作人
                if (item.OtherData.ContainsKey("CreateUserName"))
                {
                    model.CreateUserName = Convert.ToString(item.OtherData["CreateUserName"]);
                }
                //申请人
                if (item.OtherData.ContainsKey("CompanyName"))
                {
                    model.ApplyName = Convert.ToString(item.OtherData["ProposerName"])
                        +"-"+Convert.ToString(item.OtherData["CompanyName"]);
                }
                string isGuaranty = string.Empty;
                if (item.IsGuaranty != null)
                {
                    if ((bool)item.IsGuaranty)
                    {
                        isGuaranty = "有抵押,";
                    }
                    else
                    {
                        isGuaranty = "无抵押,";
                    }
                }
                string isAttachment = string.Empty;
                if (item.IsAttachment != null)
                {
                    if ((bool)item.IsAttachment)
                    {
                        isAttachment = "有查封,";
                    }
                    else
                    {
                        isAttachment = "无查封,";
                    }
                }
                string isLeasing = string.Empty;
                if (item.IsLeasing != null)
                {
                    if ((bool)item.IsLeasing)
                    {
                        isLeasing = "有租赁,";
                    }
                    else
                    {
                        isLeasing = "无租赁,";
                    }
                }
                model.PropertySurveyResult =isGuaranty + isAttachment + isLeasing;
                if (!string.IsNullOrEmpty(model.PropertySurveyResult))
                {
                    model.PropertySurveyResult = model.PropertySurveyResult.Substring(0, model.PropertySurveyResult.Length - 1);
                }
                listModel.Add(model);
            }
            return listModel;
        }

        /// <summary>
        /// 返回产调列表
        /// </summary>
        /// <param name="propertySurvey"></param>
        /// <returns></returns>
        internal static List<PropertySurveyModel> InitListModel(IList<Tuple<PropertyCertificateDto, IEnumerable<PropertySurveyDto>>> propertySurveyList)
        {
            var listModel = new List<PropertySurveyModel>();
            foreach (var propertySurvey in propertySurveyList) {
                var model = new PropertySurveyModel();
                var certificateDto = propertySurvey.Item1;
                var surveyList = propertySurvey.Item2.ToList<PropertySurveyDto>();
                //案件编号
                if (certificateDto.OtherData.ContainsKey("CaseId"))
                {
                    model.CaseId =Convert.ToString(certificateDto.OtherData["CaseId"]);
                }
                //产权证号
                model.TenementContract = certificateDto.TenementContract;
                //产权地址
                model.TenementAddress = certificateDto.TenementAddress;
                //产调数
                model.SurveyNum = surveyList.Count;
                //权利人
                model.ProprietorName = certificateDto.ProprietorName;
                if (model.SurveyNum > 0)
                {
                    //产调结果
                    var propertySurveyDto = surveyList[model.SurveyNum - 1];
                    //产调时间
                    model.SurveyDate = propertySurveyDto.SurveyDate;
                    //申请时间
                    model.ApplyDate = propertySurveyDto.CreateDate;
                    //操作人
                    if (propertySurveyDto.OtherData.ContainsKey("CreateUserName"))
                    {
                        model.CreateUserName = Convert.ToString(propertySurveyDto.OtherData["CreateUserName"]);
                    }
                    //申请人
                    if (propertySurveyDto.OtherData.ContainsKey("CompanyName"))
                    {
                        model.ApplyName = Convert.ToString(propertySurveyDto.OtherData["ProposerName"])
                            + "-" + Convert.ToString(propertySurveyDto.OtherData["CompanyName"]);
                    }
                    //产调结果
                    string isGuaranty = string.Empty;
                    if (propertySurveyDto.IsGuaranty != null)
                    {
                        if ((bool)propertySurveyDto.IsGuaranty)
                        {
                            isGuaranty = "有抵押,";
                        }
                        else
                        {
                            isGuaranty = "无抵押,";
                        }
                    }
                    string isAttachment = string.Empty;
                    if (propertySurveyDto.IsAttachment != null)
                    {
                        if ((bool)propertySurveyDto.IsAttachment)
                        {
                            isAttachment = "有查封,";
                        }
                        else
                        {
                            isAttachment = "无查封,";
                        }
                    }
                    string isLeasing = string.Empty;
                    if (propertySurveyDto.IsLeasing != null)
                    {
                        if ((bool)propertySurveyDto.IsLeasing)
                        {
                            isLeasing = "有租赁,";
                        }
                        else
                        {
                            isLeasing = "无租赁,";
                        }
                    }
                    model.PropertySurveyResult = isGuaranty + isAttachment + isLeasing;
                    if (!string.IsNullOrEmpty(model.PropertySurveyResult))
                    {
                        model.PropertySurveyResult = model.PropertySurveyResult.Substring(0, model.PropertySurveyResult.Length - 1);
                    }
                }
                listModel.Add(model);
            }
            return listModel;
        }
    }
}
