﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PinganHouse.SHHTS.UI.WPF.Common;

namespace PinganHouse.SHHTS.UI.WPF.Model
{
    public class CaseHelper
    {
        /// <summary>
        /// 保存买卖家信息
        /// </summary>
        /// <param name="caseDto"></param>
        /// <param name="customerType">类型</param>
        /// <returns></returns>
        public static OperationResult AddUserIdentity(UserIdentityModel userIdentity, CustomerType customerType)
        {
            OperationResult result = new OperationResult(1);
            try
            {
                switch (customerType)
                {
                    case CustomerType.Buyer:
                        result = CustomerServiceProxy.CreateBuyer(userIdentity, LoginHelper.CurrentUser.SysNo);
                        break;
                    case CustomerType.Seller:
                        result = CustomerServiceProxy.CreateSeller(userIdentity, LoginHelper.CurrentUser.SysNo);
                        break;
                }
                return result;
            }
            catch (Exception ex)
            {
                result.ResultMessage = ex.Message;
                return result;
            }
        }

        /// <summary>
        /// 增加新的案件
        /// </summary>
        /// <param name="caseDto"></param>
        /// <returns></returns>
        public static OperationResult SaveCase(CaseDto caseDto)
        {
            OperationResult result = new OperationResult(1);
            try
            {
                if (caseDto.SysNo == 0)
                {
                    caseDto.ReceptionCenter = ConfigHelper.GetCurrentReceptionCenter();
                    result = CaseProxyService.GetInstanse().GenerateCase(caseDto);
                }
                else { 
                    //Todo:修改案件
                }
                return result;
            }
            catch (Exception ex)
            {
                result.ResultMessage = ex.Message;
                return result;
            }
        }

        /// <summary>
        /// 获取案件的所有附件归属
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public static CaseAttachmentDto GetCaseAttachmentDto(string caseId)
        {
            return CaseProxyService.GetInstanse().GetCaseAttenment(caseId);        
        }
        
        ///// <summary>
        ///// 上传上下家附件
        ///// </summary>
        ///// <param name="userIdentitySysNo">上/下家编号</param>
        ///// <param name="attachmentType">附件类型</param>
        ///// <param name="attachmentContentData">附件内容</param>
        ///// <returns></returns>
        //public static UploadAttachmentResult AddCustomerAttachment(long userIdentitySysNo, AttachmentType attachmentType, Stream attachmentContentData)
        //{
        //    UploadAttachmentResult result = new UploadAttachmentResult();
        //    try
        //    {
        //        AttachmentContent attachmentInfo = new AttachmentContent();
        //        if (userIdentitySysNo == 0 || attachmentType == AttachmentType.未知 || attachmentContentData == null) {
        //            throw new Exception("附件内容不完整无法上传保存...");
        //        }
        //        attachmentInfo.AssociationNo = Convert.ToString(userIdentitySysNo);
        //        attachmentInfo.AttachmentType = attachmentType;
        //        attachmentInfo.AttachmentContentData = attachmentContentData;
        //        result = AttachmentProxyService.GetInstanse().AddAttachment(attachmentInfo);
        //    }
        //    catch (Exception ex) {
        //        result.ResultMessage = ex.Message;
        //    }
        //    return result;
        //}

        /// <summary>
        /// 上传案件附件
        /// </summary>
        /// <param name="caseId">案件编号</param>
        /// <param name="attachmentType">附件类型</param>
        /// <param name="attachmentContentData">附件内容</param>
        /// <returns></returns>
        public static UploadAttachmentResult AddAttachment(string caseId,string fileName, AttachmentType attachmentType, Stream attachmentContentData, Action<AttachmentContent,UploadAttachmentResult> alert = null)
        {
            UploadAttachmentResult result = new UploadAttachmentResult();
            try
            {
                AttachmentContent attachmentInfo = new AttachmentContent();
                if (string.IsNullOrEmpty(caseId) || attachmentType == AttachmentType.未知 || attachmentContentData == null)
                {
                    throw new Exception("附件内容不完整无法上传保存...");
                }
                AttachmentProxyService.GetInstanse().OnCompleted += (sender, e) =>
                {
                    if (alert != null)
                        alert(e.AttachInfo, e.UploadResult);
                };

                attachmentInfo.AssociationNo = caseId;
                attachmentInfo.FileName = fileName;
                attachmentInfo.AttachmentType = attachmentType;
                attachmentInfo.AttachmentContentData = attachmentContentData;
                AttachmentProxyService.GetInstanse().AddAttachment(attachmentInfo, true);
                
            }
            catch (Exception ex)
            {
                result.ResultMessage = ex.Message;
            }
            return result;
        }
        /// <summary>
        /// 获得附件
        /// </summary>
        /// <param name="bizNo"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        public static IDictionary<AttachmentType, IList<string>> GetAttachments(string bizNo, List<AttachmentType> types)
        {
            try
            {              
                if (string.IsNullOrEmpty(bizNo))
                {
                    throw new Exception("传递参数为空");
                }
                if (types==null || types.Count==0)
                {
                    throw new Exception("传递列表为空");
                }
                var resultDictionary=AttachmentProxyService.GetInstanse().GetAttachments(bizNo, types);
                return resultDictionary;
         
            }
            catch (Exception ex)
            {
                
                throw;
            }
          
        }
        /// <summary>
        /// 获取附件的图片流
        /// </summary>
        /// <param name="fileId">附件的Id</param>
        /// <returns></returns>
        public static FileDownloadMessage DownloadAttachment(string fileId)
        {
            //FileDownloadMessage result=new FileDownloadMessage();
            if (string.IsNullOrWhiteSpace(fileId))
                return null;
            return  AttachmentProxyService.GetInstanse().DownloadAttachment(fileId);            
        }

        /// <summary>
        /// 获取操作信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static IList<CaseEvent> GetOperationInfos(string id, CaseStatus status)
        {
            IList<CaseEvent> result=new List<CaseEvent>();
            try
            {
                List<CaseStatus> caseStatus=new List<CaseStatus>();        //状态集合
                int totalCount;
                if (string.IsNullOrEmpty(id))
                {
                    throw new Exception("传递参数为空");
                }
                var statusString = status.ToString();
                var statusArray = statusString.Split(',');
                for (int i = 0; i < statusArray.Length; i++)
                {
                    caseStatus.Add((CaseStatus)Enum.Parse(typeof(CaseStatus), statusArray[i]));                                       
                }
                result = CaseProxyService.GetInstanse().GetCaseEvents(id, 0, 0, out totalCount, caseStatus.ToArray());
            }
            catch (Exception)
            {
               
            }
            return result;
        }
    }
}
