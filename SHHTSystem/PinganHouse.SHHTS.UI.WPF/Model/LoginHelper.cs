﻿using PinganHouse.SHHTS.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.UI.WPF.Model
{
    /// <summary>
    /// 系统登陆持久化类
    /// </summary>
    public class LoginHelper
    {
        /// <summary>
        /// 当前登录
        /// </summary>
        private static User _CurrentUser = null;

        public static User CurrentUser
        {
            get
            {
                if (_CurrentUser == null)
                    _CurrentUser = new User();
                return _CurrentUser;
            }
        }
    }
}
