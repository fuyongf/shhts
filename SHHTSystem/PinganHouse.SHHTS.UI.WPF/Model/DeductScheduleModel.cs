﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PinganHouse.SHHTS.DataTransferObjects;

namespace PinganHouse.SHHTS.UI.WPF.Model
{
    class DeductScheduleModel : DeductSchedule
    {
        /// <summary>
        /// 待扣金额
        /// </summary>
        public decimal NotDeductedAmount { get; set; }

        public static List<DeductScheduleModel> InitData(IList<DeductSchedule> deductScheduleList)
        {
            var deductScheduleModelList = new List<DeductScheduleModel>();

            foreach (var dto in deductScheduleList)
            {
                var model = new DeductScheduleModel();
                model.AccountSysNo = dto.AccountSysNo;
                model.DeductType = dto.DeductType;
                model.LimitSource = dto.LimitSource;
                model.ApplyAmount = dto.ApplyAmount;
                model.DeductedAmount = dto.DeductedAmount;
                model.Remark = dto.Remark;
                model.Finished = dto.Finished;
                model.Verson = dto.Verson;
                model.CaseId = dto.CaseId;
                model.TenementContract = dto.TenementContract;
                model.TenementAddress = dto.TenementAddress;
                model.AccountType = dto.AccountType;
                model.NotDeductedAmount = dto.ApplyAmount - dto.DeductedAmount;

                deductScheduleModelList.Add(model);
            }
            return deductScheduleModelList;
        }
    }
}
