﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.UI.WPF.Model
{
    public class CastModel : CaseDto
    {
        /// <summary>
        /// 除了已取消签约的案件，都可以进行案件中止的操作。1:表示是案件中止操作台
        /// </summary>
        public int CaseEndOperation { set; get; }


        /// <summary>
        /// 案件状态
        /// </summary>
        public string CaseStatusValue { get; set; }

        /// <summary>
        /// 案件状态
        /// </summary>
        public long CaseStatusKey { get; set; }

        /// <summary>
        /// 买方
        /// </summary>
        public string BuyerName2 { get; set; }

        /// <summary>
        /// 卖家
        /// </summary>
        public string SellerName2 { get; set; }

        /// <summary>
        /// 接单人()
        /// </summary>
        public string OperatorName2 { get; set; }

        /// <summary>
        /// 制定的操作事件 如 签约时间
        /// </summary>
        public DateTime? OperatorTime { get; set; }

        public static List<CastModel> InitData(IList<CaseDto> caseList, CaseStatus? caseStatus=null)
        {
            var castModelList = new List<CastModel>();

            foreach (var caseDto in caseList)
            {
                var castModel = new CastModel();
                castModel.CaseId = caseDto.CaseId;
                castModel.CaseStatus = caseDto.CaseStatus;
                castModel.AgencyName = caseDto.AgencyName;
                castModel.OperatorName = caseDto.OperatorName;
                castModel.TenementAddress = caseDto.TenementAddress;
                castModel.CompanyName = caseDto.CompanyName;
                castModel.CreateDate = caseDto.CreateDate;
                castModel.CreatorName = caseDto.CreatorName;
                castModel.ModifyDate = caseDto.ModifyDate;
                castModel.ModifyUserName = caseDto.ModifyUserName;

                if (!string.IsNullOrEmpty(caseDto.BuyerName))
                {
                    castModel.BuyerName2 = caseDto.BuyerName.Split(',')[0];
                }
                if (!string.IsNullOrEmpty(caseDto.SellerName))
                {
                    castModel.SellerName2 = caseDto.SellerName.Split(',')[0];
                }
                castModel.CaseStatusKey = (long)caseDto.CaseStatus;
                castModel.CaseStatusValue = GetCaseStatusValue(caseDto.CaseStatus);

                if (caseDto.OtherData.Count > 0 && caseStatus!=null)
                {
                    castModel.OperatorName =Convert.ToString(caseDto.OtherData[Convert.ToString(caseStatus)]);
                    castModel.OperatorTime = Convert.ToDateTime(caseDto.OtherData["time_"+Convert.ToString(caseStatus)]);
                }
                castModelList.Add(castModel);
            }
            return castModelList;
        }

        /// <summary>
        /// 用于主办台  
        /// </summary>
        /// <param name="caseList"></param>
        /// <param name="operationName2Status">PerationName 是主办人, OperationName2 受理人，发起人，核案  </param>
        /// <returns></returns>
        public static List<CastModel> InitData(IList<CaseDto> caseList,CaseStatus hostStatus,CaseStatus launchStatus)
        {
            var castModelList = new List<CastModel>();

            foreach (var caseDto in caseList)
            {
                var castModel = new CastModel();
                castModel.CaseId = caseDto.CaseId;
                castModel.CaseStatus = caseDto.CaseStatus;
                castModel.AgencyName = caseDto.AgencyName;
                castModel.OperatorName = caseDto.OperatorName;
                castModel.TenementAddress = caseDto.TenementAddress;
                castModel.CompanyName = caseDto.CompanyName;
                castModel.CreatorName = caseDto.CreatorName;
                //签约时间
                string key = string.Format("time_{0}", CaseStatus.HA2.ToString());
                if (caseDto.OtherData.ContainsKey(key)) castModel.CreateDate =  DateTime.Parse(caseDto.OtherData[key].ToString());
                //最后更新时间
                key = string.Format("time_{0}", caseDto.CaseStatus.ToString());
                if (caseDto.OtherData.ContainsKey(key)) castModel.ModifyDate = DateTime.Parse(caseDto.OtherData[key].ToString());

                castModel.ModifyUserName = caseDto.ModifyUserName;


                if (!string.IsNullOrEmpty(caseDto.BuyerName))
                {
                    castModel.BuyerName2 = caseDto.BuyerName.Split(',')[0];
                }
                if (!string.IsNullOrEmpty(caseDto.SellerName))
                {
                    castModel.SellerName2 = caseDto.SellerName.Split(',')[0];
                }
                castModel.CaseStatusKey = (long)caseDto.CaseStatus;
                castModel.CaseStatusValue = GetCaseStatusValue(caseDto.CaseStatus);

                if (caseDto.OtherData.Count > 0)
                {
                    foreach (var item in caseDto.OtherData)
                    {
                        if (item.Value != null)
                        {
                            if (item.Key == hostStatus.ToString())
                            {
                                castModel.OperatorName = item.Value.ToString();
                            }
                            else if (item.Key == launchStatus.ToString())
                            {
                                castModel.OperatorName2 = item.Value.ToString();
                            }
                        }
                    }
                }
                castModelList.Add(castModel);
            }
            return castModelList;
        }

        private static string GetCaseStatusValue(CaseStatus status)
        {
            var caseStatusList = EnumHelper.InitCaseStatusToCombobox();
            foreach (var item in caseStatusList)
            {
                if (item.ValueMember == (long)status)
                {
                    return item.DisplayMember;
                }
            }

            return null;
        }
    }
}
