﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PinganHouse.SHHTS.AccessControl.Proxy;

namespace PinganHouse.SHHTS.UI.WPF.Model
{
    public class UserPermissionModel : IUserIdentity
    {
        public string AppNo { get; set; }

        public IList<string> PrincipalNos { get; set; }

        public bool IsAuthenticated { get; set; }

        public DateTime AuthenticatedTime { get; set; }
    }
}
