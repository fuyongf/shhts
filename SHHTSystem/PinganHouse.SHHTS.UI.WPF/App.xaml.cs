﻿using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Login;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace PinganHouse.SHHTS.UI.WPF
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            //this.Startup += new StartupEventHandler(NetDetection);
            this.Startup += new StartupEventHandler(App_Startup);
            this.DispatcherUnhandledException += new DispatcherUnhandledExceptionEventHandler(App_UnhandledException);
        }

        void NetDetection(object sender, StartupEventArgs e)
        {
            try
            {
                var result = SystemServiceProxy.NetDetection();
                if (!result.Success)
                {
                    MessageBox.Show(result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch
            {
                throw;
            }
        }

        void App_Startup(object sender, StartupEventArgs e)
        {
            //获取更新地址
            string url = ConfigServiceProxy.GetAutoUpdateUrl();
            if (string.IsNullOrWhiteSpace(url))
            {
                //如访问服务端失败，启用客户端配置
                url = ConfigurationManager.AppSettings["AutoUpdateUrl"];
            }


            #region 应用程序自动更新

            var updater = FSLib.App.SimpleUpdater.Updater.Instance;

            //当检查发生错误时,这个事件会触发
            updater.Error += new EventHandler(Updater_Error);

            //没有找到更新的事件
            updater.NoUpdatesFound += new EventHandler(Updater_NoUpdatesFound);

            //找到更新的事件
            updater.UpdatesFound += new EventHandler(Updater_UpdatesFound);

            //更新完成事件
            updater.UpdateFinished += new EventHandler(Updater_Finished);

            //开始检查更新
            FSLib.App.SimpleUpdater.Updater.CheckUpdateSimple(url, "update_c.xml");
            updater.EnsureNoUpdate();
            #endregion
        }

        void App_UnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
#if DEBUG
            MessageBox.Show("系统发生错误：" + e.Exception.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
#endif
#if !DEBUG
            MessageBox.Show("系统发生错误", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
#endif
            e.Handled = true;
        }

        #region 自动更新相关事件
        static void Updater_Error(object sender, EventArgs e)
        {
            var updater = sender as FSLib.App.SimpleUpdater.Updater;
            MessageBox.Show(updater.Context.Exception.ToString());
        }

        static void Updater_UpdatesFound(object sender, EventArgs e)
        {

        }

        static void Updater_NoUpdatesFound(object sender, EventArgs e)
        {
            //MessageBox.Show("没有找到可用更新");
        }

        static void Updater_Finished(object sender, EventArgs e)
        {
        }
        #endregion

        /// <summary>
        /// 防止重复启动exe
        /// </summary>
        /// <param name="e"></param>
        protected override void OnStartup(StartupEventArgs e)
        {
            //当前运行WPF程序的进程实例
            Process process = Process.GetCurrentProcess();
            //遍历WPF程序的同名进程组
            foreach (Process p in Process.GetProcessesByName(process.ProcessName))
            {
                //不是同一进程并且本进程启动时间最晚,则关闭较早进程
                if (p.Id != process.Id && (p.StartTime - process.StartTime).TotalMilliseconds <= 0)
                {
                    p.Kill();//这个地方用kill 而不用Shutdown();的原因是,Shutdown关闭程序在进程管理器里进程的释放有延迟不是马上关闭进程的
                    //Application.Current.Shutdown();
                    return;
                }


            }
            base.OnStartup(e);
        }
    }
}
