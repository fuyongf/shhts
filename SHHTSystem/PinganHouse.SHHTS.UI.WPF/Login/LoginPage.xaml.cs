﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Configuration;
using System.Threading;
using System.Windows.Navigation;

using PinganHouse.SHHTS.AccessControl.Proxy;

using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy;

using PinganHouse.SHHTS.UI.WPF.Common;
using PinganHouse.SHHTS.UI.WPF.Model;

namespace PinganHouse.SHHTS.UI.WPF.Login
{
    /// <summary>
    /// LoginPage.xaml 的交互逻辑
    /// </summary>
    public partial class LoginPage : Window
    {
        private string _permissionAppNo;
        private bool _IsExit = true;
        public static int AreaSelect = 0;
        Register register = new Register();
        /// <summary>
        /// 构造函数
        /// </summary>
        public LoginPage()
        {
            InitializeComponent();
            AddConfigFile();
            _permissionAppNo = ConfigurationManager.AppSettings["PermissionAppNo"];
            if (string.IsNullOrWhiteSpace(_permissionAppNo))
            {
                MessageBox.Show("未配置权限系统编号", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                this.Close();
            }
        }

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                loading_Ctrl.IsBusy = true;
                loading_Ctrl.Text = "正在登录...";
                LoginSystem();
            }
            catch (Exception ex)
            {
                loading_Ctrl.IsBusy = false;

                MessageBox.Show("登陆发生系统异常错误：" + ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 系统登陆
        /// </summary>
        public async void LoginSystem()
        {
            try
            {
                if (AreaSelect == 0)
                {
                    AreaGrid.Visibility = Visibility.Visible;
                    loading_Ctrl.IsBusy = false;
                    return;
                }
                //001 用户名和密码是否为空检查
                var txtUserName = this.UserName.Text.Trim();
                var txtPassWord = this.PassWord.Password.Trim();


                //Todo: 非空验证
                var operationResult = await GetLoginResult(txtUserName, txtPassWord);
                //003 验证数据并数据持久化
                if (operationResult.Success)
                {
                    //成功登陆并持久化登陆数据 
                    var user = (User)operationResult.OtherData["UserInfo"];
                    LoginHelper.CurrentUser.SysNo = user.SysNo;
                    LoginHelper.CurrentUser.RealName = user.RealName;
                    LoginHelper.CurrentUser.TelPhone = user.TelPhone;

                    //写入权限信息
                    PermissionProxyService.GetInstance().SetUserIdentity(new UserPermissionModel
                    {
                        AppNo = _permissionAppNo,
                        PrincipalNos = new List<string> { user.UserId.ToString() },
                        AuthenticatedTime = DateTime.Now,
                        IsAuthenticated = true
                    });

                    //页面跳转
                    _IsExit = false;
                    new MainWindow().Show();
                    if (this.RememberFlag.IsChecked == true)
                    {
                        SetConfigFile(txtUserName);
                    }
                    this.Close();
                }
                else
                {
                    if (UserName.Text == string.Empty)
                    {
                        UserName.Text = string.Empty;
                    }

                    if (operationResult.ResultNo == -5)
                    {
                        PassWord.Password = string.Empty;
                    }
                    MessageBox.Show("登陆失败：" + operationResult.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    loading_Ctrl.IsBusy = false;
                }
            }
            catch (Exception ex)
            {
                loading_Ctrl.IsBusy = false;
                throw ex;
            }
        }

        /// <summary>
        /// 异步登录
        /// </summary>
        /// <param name="txtUserName"></param>
        /// <param name="txtPassWord"></param>
        /// <returns></returns>
        public Task<OperationResult> GetLoginResult(string txtUserName, string txtPassWord)
        {
            return Task.Run(() =>
            {
                return UserServiceProxy.Login(txtUserName, txtPassWord);
            });
        }

        /// <summary>
        /// 读取配置文件
        /// </summary>
        private void AddConfigFile()
        {
            try
            {

                //从注册表加载信息
                var register = new Register();
                var softName = ConfigurationManager.AppSettings["SoftName"];
                var rememberFlag = register.ReadRegeditKey("RememberFlag");
                var historyName = register.ReadRegeditKey("HistoryName");
                if (rememberFlag == null && historyName == null)
                {
                    register.WriteRegeditKey("RememberFlag", false);
                    register.WriteRegeditKey("HistoryName", string.Empty);
                }
                else
                {
                    bool IsChecked;
                    bool.TryParse(Convert.ToString(rememberFlag), out IsChecked);
                    this.RememberFlag.IsChecked = IsChecked;
                    if (IsChecked)
                    {
                        this.UserName.Text = Convert.ToString(historyName);
                        this.PassWord.Focus();
                    }
                }
            }
            catch (Exception)
            {
                this.RememberFlag.IsChecked = false;
            }

        }

        /// <summary>
        /// 修改配置文件
        /// </summary>
        private void SetConfigFile(string name)
        {
            try
            {

                Configuration fig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                fig.AppSettings.Settings["HistoryName"].Value = name;
                fig.AppSettings.Settings["RememberFlag"].Value = "true";
                fig.Save();

                //从注册表写入信息
                var register = new Register();
                register.WriteRegeditKey("RememberFlag", true);
                register.WriteRegeditKey("HistoryName", name);

            }
            catch (Exception)
            {
                return;
            }
        }

        /// <summary>
        /// Enter 事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (string.IsNullOrEmpty(this.UserName.Text.Trim()))
                {
                    this.UserName.Focus();
                    return;
                }

                if (string.IsNullOrEmpty(this.PassWord.Password.Trim()))
                {
                    this.PassWord.Focus();
                    return;
                }
                Button_Click(sender, e);
            }
        }

        /// <summary>
        /// 登录页面关闭事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Login_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_IsExit)
            {
                Environment.Exit(0);
            }
        }
        private void Login_Loaded(object sender, RoutedEventArgs e)
        {
            //AreaSelect =register.ReadRegeditKey("Area");

            var list = EnumHelper.InitReceptionCenterToCombobox();
            list.RemoveAt(0);
            CbArea.ItemsSource = list;
            CbArea.DisplayMemberPath = "DisplayMember";
            Area.ItemsSource = list;
            Area.DisplayMemberPath = "DisplayMember";
            AddAreaConfigFile();
            if (AreaSelect == 0)
            {
                AreaGrid.Visibility = Visibility.Visible;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            AreaGrid.Visibility = Visibility.Collapsed;
        }

        private void CbArea_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var value = (EnumHelper)CbArea.SelectedValue;
            AreaSelect = (int)value.ValueMember;
            SetAreaConfigFile(AreaSelect);
            CbArea.IsEditable = false;
        }

        private void CbNetwork_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = ((ComboBox)e.Source).SelectedItem as ComboBoxItem;
            if (item != null)
            {
                AppDomain.CurrentDomain.SetData("ServiceSuffix", item.Tag as string);
            }
        }

        private void Area_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var value = (EnumHelper)Area.SelectedValue;
            AreaSelect = (int)value.ValueMember;
            SetAreaConfigFile(AreaSelect);
            CbArea.SelectedIndex = AreaSelect - 1;
            Area.IsEditable = false;
        }
        #region 配置信息
        /// <summary>
        /// 读取配置文件
        /// </summary>
        private void AddAreaConfigFile()
        {
            try
            {
                //从注册表加载信息          
                var rememberArea = PrivateProfileUtils.GetContentValue("Area", "SH");
                if (!string.IsNullOrWhiteSpace(rememberArea))
                    CbArea.SelectedIndex = int.Parse(rememberArea) - 1;
            }
            catch (Exception)
            {

            }

        }
        /// <summary>
        /// 修改配置文件
        /// </summary>
        private void SetAreaConfigFile(int area)
        {
            try
            {
                //从注册表写入信息
                PrivateProfileUtils.Write("Area", "SH", area.ToString());
            }
            catch (Exception)
            {

            }
        }
        #endregion

        private void BtnTest_OnClick(object sender, RoutedEventArgs e)
        {
            //new StaffId().Show();
        }
    }
}