﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPF.Login;
//using PinganHouse.SHHTS.UI.WPF.Main.SettingPage;
using PinganHouse.SHHTS.UI.WPF.Main.Setting;

namespace PinganHouse.SHHTS.UI.WPF.Common
{
    class ConfigHelper
    {
        public static  ReceptionCenter GetCurrentReceptionCenter() 
        {
            //配置
            //return ReceptionCenter.PuDong;
            var area = LoginPage.AreaSelect;
            if (area == 0)
            {
                MessageBox.Show("请选择所属区域", "系统提示");
                return ReceptionCenter.Unkonw;
            }
            return (ReceptionCenter)area;
        }
    }
}
