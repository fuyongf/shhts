﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;

namespace PinganHouse.SHHTS.UI.WPF.Common
{
    public class PageHelper
    {
        /// <summary>
        /// 页面的切换
        /// </summary>
        /// <param name="page">旧的页面</param>
        /// <param name="newPage">新的页面</param>
        public static void PageNavigateHelper(Page page, Page newPage)
        {
            Frame pageFrame = null;
            DependencyObject currParent = VisualTreeHelper.GetParent(page);
            while (currParent != null && pageFrame == null)
            {
                pageFrame = currParent as Frame;
                currParent = VisualTreeHelper.GetParent(currParent);
            }
            // Change the page of the frame.
            if (pageFrame != null)
            {
                pageFrame.NavigationUIVisibility = NavigationUIVisibility.Hidden;
                pageFrame.Navigate(newPage);
                //pageFrame.Source = new Uri("Main/CaseCustomerInfo.xaml", UriKind.Relative);
            }
        }

        /// <summary>
        /// 页面切换
        /// </summary>
        /// <param name="page">旧的页面</param>
        /// <param name="newPageUrl">新页面的URL地址</param>
        public static void PageNavigateHelper(Page page, string newPageUrl)
        {
            Frame pageFrame = null;
            DependencyObject currParent = VisualTreeHelper.GetParent(page);
            while (currParent != null && pageFrame == null)
            {
                pageFrame = currParent as Frame;
                currParent = VisualTreeHelper.GetParent(currParent);
            }
            // Change the page of the frame.
            if (pageFrame != null)
            {
                pageFrame.NavigationUIVisibility = NavigationUIVisibility.Hidden;
                pageFrame.NavigationService.Navigate(new Uri(newPageUrl, UriKind.Relative));

            }
        }

        /// <summary>
        /// 页面切换(带参)
        /// </summary>
        /// <param name="page">旧的页面</param>
        /// <param name="newPageUrl">新页面的URL地址</param>
        public static void PageNavigateIdHelper(Page page, string newPageUrl,string id)
        {
            Frame pageFrame = null;
            DependencyObject currParent = VisualTreeHelper.GetParent(page);
            while (currParent != null && pageFrame == null)
            {
                pageFrame = currParent as Frame;
                currParent = VisualTreeHelper.GetParent(currParent);
            }
            // Change the page of the frame.
            if (pageFrame != null)
            {
                pageFrame.NavigationUIVisibility = NavigationUIVisibility.Hidden;
                pageFrame.NavigationService.Navigate(new Uri(newPageUrl, UriKind.Relative),id);

            }
        }

        /// <summary>
        ///  禁用Panle上的控件活性和非活性
        /// </summary>
        /// <param name="panle">禁用的Panle</param>
        /// <param name="ctrls">例外的控件</param>
        public static void PageControlToDisable(Panel panle, params System.Windows.Controls.Control[] ctrls)
        {
            try
            {
                UIElementCollection Childrens = panle.Children;
                ToSetStatusForControls(Childrens,false);
                ToSetStatusForControls(ctrls,false);
            }
            catch (Exception ex)
            {
                MessageBox.Show("设置控件状态发生系统异常："+ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 激活控件
        /// </summary>
        /// <param name="panle"></param>
        /// <param name="ctrls"></param>
        public static void PageControlToActivate(Panel panle, params System.Windows.Controls.Control[] ctrls)
        {
            try
            {
                UIElementCollection Childrens = panle.Children;
                ToSetStatusForControls(Childrens,true);
                ToSetStatusForControls(ctrls,true);
            }
            catch (Exception ex)
            {
                MessageBox.Show("设置控件状态发生系统异常：" + ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 激活控件
        /// </summary>
        /// <param name="ctrls"></param>
        private static void ToSetStatusForControls(System.Windows.Controls.Control[] ctrls, bool isEnabled)
        {
 	        if(ctrls==null || ctrls.Length==0) return;
            foreach(var ctrl in ctrls){
                ctrl.IsEnabled = !isEnabled;
            }
        }

        /// <summary>
        /// 禁用控件
        /// </summary>
        /// <param name="collections"></param>
        private static void ToSetStatusForControls(UIElementCollection collections, bool isEnabled)
        {
            foreach (UIElement ui in collections)
            {
                //ui转成控件,验证Tag是否必须输入
                switch (ui.DependencyObjectType.Name)
                {
                    case "Grid":
                    case "WrapPanel":
                    case "StackPanel":
                    case "Border":
                        var panle = ui as System.Windows.Controls.Panel;
                        if (panle != null)
                        {
                            var childrens = panle.Children;
                            ToSetStatusForControls(childrens, isEnabled);
                        }
                        break;
                    default:
                        ui.IsEnabled = isEnabled;
                        break;
                }              
            }
        }

        /// <summary>
        /// 页面激活loading控件
        /// </summary>
        /// <param name="ctrl">loading 控件</param>
        /// <param name="message">消息显示</param>
        public static void PageActiveLoadingControl(Control.BusyIndicator ctrl,string message)
        {
            try
            {
                ctrl.IsBusy = true;
                ctrl.Text = message;
            }
            catch (Exception)
            {
                ctrl.IsBusy = false;
            }
        }
    }
}
