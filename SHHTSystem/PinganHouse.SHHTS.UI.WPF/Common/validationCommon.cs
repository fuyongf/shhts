﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.UI.WPF.Common
{
    class ValidationCommon
    {     
        /// <summary>
        /// 判断是否为空
        /// </summary>
        /// <param name="str">验证参数</param>
        /// <returns>返回结果</returns>
        public static bool IsEmpty(string str)
        {
            return string.IsNullOrEmpty(str);
        }

        public static bool IsUserMatch(string str)
        {
            return new Regex(@"^[a-zA-Z\s\u4e00-\u9fa5]+$").IsMatch(str);
        }
        public static bool IsIdMatch(string str)
        {
            return new Regex(@"^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}(\d|x|X)$").IsMatch(str);
        }
        public static bool IsUmIdMatch(string str)
        {
            return new Regex(@"^([A-Za-z]+)\d{3}$").IsMatch(str);
        }
    }
}
