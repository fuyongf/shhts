﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace PinganHouse.SHHTS.UI.WPF.Common
{
    internal class IdentityCardHelper
    {
        [DllImport("Sdtapi.dll")]
        private static extern int InitComm(int iPort);

        [DllImport("Sdtapi.dll")]
        private static extern int Authenticate();

        [DllImport("Sdtapi.dll")]
        private static extern int ReadBaseInfos(
            StringBuilder name,
            StringBuilder gender,
            StringBuilder folk,
            StringBuilder birthDay,
            StringBuilder code,
            StringBuilder address,
            StringBuilder agency,
            StringBuilder expireStart,
            StringBuilder expireEnd);

        [DllImport("Sdtapi.dll")]
        private static extern int CloseComm();
        [DllImport("Sdtapi.dll")]
        private static extern int ReadBaseMsg(byte[] pMsg, ref int len);
        [DllImport("Sdtapi.dll")]
        private static extern int ReadBaseMsgW(byte[] pMsg, ref int len);

        [DllImport("Sdtapi.dll")]
        private static extern int Routon_IC_FindCard();
        [DllImport("Sdtapi.dll")]
        private static extern int Routon_IC_HL_ReadCardSN(StringBuilder serialNum);
        [DllImport("Sdtapi.dll")]
        private static extern int HID_BeepLED(bool beepOn,bool ledon, int duration);

        /// <summary>
        /// 机具连接
        /// </summary>
        public static bool OpenDevice(out string errorMessage)
        {
            try
            {         
                errorMessage = string.Empty;

                //打开端口
                int intOpenRet = InitComm(1001);
                if (intOpenRet != 1)
                {
                    errorMessage = "身份证读卡器未连接";
                    return false;
                }

                return true;
            }
            catch (Exception )
            {
                errorMessage = "缺少Sdtapi.dll文件,无法使用身份证读卡器";
                return false;
            }
        }

        /// <summary>
        /// 查找身份证卡
        /// </summary>
        public static bool FindCard(out string errorMessage)
        {
            errorMessage = string.Empty;
            //卡认证
            int intReadRet = Authenticate();
            if (intReadRet != 1)
            {
                errorMessage = "未找到身份证";

                //CloseComm();
                return false;
            }
            return true;
        }
        /// <summary>
        /// 查找员工卡
        /// </summary>
        public static bool FindStaffCard(out string errorMessage)
        {
            errorMessage = string.Empty;
            //卡认证
            int intReadRet = Routon_IC_FindCard();
            if (intReadRet != 3)
            {
                errorMessage = "未找到员工卡";
                return false;
            }
            return true;
        }

        public static bool Read(
            out StringBuilder name,
            out StringBuilder gender,
            out StringBuilder folk,
            out StringBuilder birthDay,
            out StringBuilder code,
            out StringBuilder address,
            out StringBuilder agency,
            out StringBuilder expireStart,
            out StringBuilder expireEnd,
            out byte[] photo,
            out string errorMessage)
        {
            errorMessage = string.Empty;
            name = new StringBuilder(31);
            gender = new StringBuilder(3);
            folk = new StringBuilder(10);
            birthDay = new StringBuilder(9);
            code = new StringBuilder(19);
            address = new StringBuilder(71);
            agency = new StringBuilder(31);
            expireStart = new StringBuilder(9);
            expireEnd = new StringBuilder(9);
            photo = null;

            int intReadBaseInfosRet = ReadBaseInfos(
                name,
                gender,
                folk,
                birthDay,
                code,
                address,
                agency,
                expireStart,
                expireEnd);

            if (intReadBaseInfosRet != 1)
            {
                errorMessage = "读卡失败";
                CloseComm();
                return false;
            }
            using (var photoStream = new FileStream("photo.bmp", FileMode.Open, FileAccess.ReadWrite))
            {
                photo = new byte[photoStream.Length];
                photoStream.Read(photo, 0, photo.Length);
            }
            CloseComm();
            return true;
        }
        /// <summary>
        /// 读取员工卡
        /// </summary>
        /// <param name="serialNum">返回员工编号</param>
        /// <param name="errorMessage">返回提示信息</param>
        /// <returns></returns>
        public static bool ReadStaffCard(out StringBuilder serialNum, out string errorMessage)
        {
            serialNum=new StringBuilder(31);
            errorMessage = string.Empty;
            int serialNumRet = Routon_IC_HL_ReadCardSN(serialNum);
            if (serialNumRet != 1)
            {
                errorMessage = "读卡失败";
                CloseComm();
                return false;
            }
            return true;
        }
        /// <summary>
        /// 读员工卡成功后给提示音
        /// </summary>
        /// <param name="errorMessage"></param>
        public static bool  BeepLed(out string errorMessage)
        {
            errorMessage = string.Empty;
            var ret=HID_BeepLED(true, true, 90);
            if (ret!=1)
            {
                errorMessage = "蜂鸣器或指示灯出现问题";
                return false;
            }
            return true;
        }
        /// <summary>
        /// 关闭连接
        /// </summary>
        /// <returns></returns>
        public static bool CloseDevice()
        {
           return CloseComm() == 0;
        }

       
    }
}
