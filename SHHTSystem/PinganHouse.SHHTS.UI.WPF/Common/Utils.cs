﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace PinganHouse.SHHTS.UI.WPF.Common
{
    class Utils
    {

        /// <summary>
        /// 获得父窗体
        /// </summary>
        /// <param name="dependencyObject"></param>
        /// <returns></returns>
        public static Window GetParentWindow(DependencyObject dependencyObject)
        {
            DependencyObject element = dependencyObject;
            while (true)
            {
                element = VisualTreeHelper.GetParent(element);
                if (element is Window)
                {
                    break;
                }
            }
            return element as Window;
        }

        public static bool PhoneNumValidate(string text)
        {
            if (string.IsNullOrEmpty(text))
                return true;
            return Regex.IsMatch(text, @"^(1|1|1|1|1|1)\d{10}$");

        }
        public static bool EmailValidate(string text)
        {
            if (string.IsNullOrEmpty(text))
                return true;
            return Regex.IsMatch(text, @"^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");
        }


        public static string GetVersion()
        {
            return  string.Format("平安好房二手房管理系统{0}", Assembly.GetExecutingAssembly().GetName().Version);
        }

        /// 格式化银行卡显示
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        public static string BankAccountFormat(string txt)
        {
            try
            {
                if (txt == null) return txt;
                txt = txt.Replace(" ", "");
                var size = txt.Length / 4;
                var newstr = new StringBuilder();
                for (int n = 0; n < size; n++)
                {
                    newstr.Append(txt.Substring(n * 4, 4));
                    newstr.Append(" ");
                }
                newstr.Append(txt.Substring(size * 4, txt.Length % 4));
                return newstr.ToString();
            }
            catch (Exception)
            {
                return txt;
            }
        }

    }
}
