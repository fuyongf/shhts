﻿using Microsoft.Win32;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.UI.WPF.Common
{
    /// <summary>
    /// 上传附件类
    /// </summary>
    public class UploadFileHelper
    {
        //上传成功事件
        public delegate void UpLoadSuccessHandle();
        //上传成功事件
        public event UpLoadSuccessHandle UpLoadSuccessEvent;

        //上传失败事件
        public delegate void UpLoadFailHandle();
        //上传失败事件
        public event UpLoadFailHandle UpLoadFailEvent;

        /// <summary>
        /// 上传文件类
        /// </summary>
        /// <param name="fileId">文件关联ID</param>
        /// <param name="type">类型</param>
        /// <param name="filesNum">选择数</param>
        public void UploadFile(string fileId, AttachmentType type, out int filesNum)
        {
            var openFiles = new OpenFileDialog
            {
                Filter = "图像文件(*.jpg,*.bmp,*.png)|*.jpg;*.bmp;*.png",
                Multiselect = true
            };
            var dialogResult = openFiles.ShowDialog();
            var files = openFiles.OpenFiles();
            filesNum = 0;
            if (dialogResult == true)
            {
                foreach (FileStream file in files)
                {
                    if (file != null)
                    {
                        //附件数
                        filesNum = files.Length;
                        //0001 把本地附件放到指定的目录
                        var filePath = string.Empty;
                        CreateFile(fileId, type, out filePath);
                        FileStream AttachFile;
                        using (
                            AttachFile =
                                new FileStream(filePath + "\\" + DateTime.Now.Ticks + ".bmp", FileMode.OpenOrCreate,
                                    FileAccess.Write))
                        {
                            byte[] fileByte = new byte[file.Length];
                            file.Read(fileByte, 0, fileByte.Length);
                            AttachFile.Write(fileByte, 0, fileByte.Length);
                        }
                        AddAttachment(fileId, AttachFile.Name, type, file);
                    }
                }
            }
        }


        /// <summary>
        /// 获取选择的文件
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, Stream> SelectUploadFile()
        {
            var fileList = new Dictionary<string, Stream>();
            var openFiles = new OpenFileDialog
            {
                Filter = "图像文件(*.jpg,*.bmp,*.png)|*.jpg;*.bmp;*.png",
                Multiselect = true
            };
            var dialogResult = openFiles.ShowDialog();
            var files = openFiles.OpenFiles();
            var fileNames=new List<string>();
            var fileFileStream=new List<FileStream>();
            if (dialogResult == true)
            {

                foreach (FileStream file in files)
                {
                    if (file != null)
                    {
                        if (fileList.ContainsKey(file.Name)) continue;
                        fileList.Add(file.Name, file);
                    }
                }
            }

            return fileList;
        }

        /// <summary>
        /// 上传附件
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="fileName"></param>
        /// <param name="attachmentType"></param>
        /// <param name="attachmentContentData"></param>
        /// <param name="alert"></param>
        /// <returns></returns>
        public  UploadAttachmentResult AddAttachment(string caseId, string fileName, AttachmentType attachmentType, Stream attachmentContentData)
        {
            UploadAttachmentResult result = new UploadAttachmentResult();
            try
            {
                AttachmentContent attachmentInfo = new AttachmentContent();
                if (string.IsNullOrEmpty(caseId) || attachmentType == AttachmentType.未知 || attachmentContentData == null)
                {
                    throw new Exception("附件内容不完整无法上传保存...");
                }
                AttachmentProxyService.GetInstanse().OnCompleted +=UploadFileHelper_OnCompleted;

                attachmentInfo.AssociationNo = caseId;
                attachmentInfo.FileName = fileName;
                attachmentInfo.AttachmentType = attachmentType;
                attachmentInfo.AttachmentContentData = attachmentContentData;
                AttachmentProxyService.GetInstanse().AddAttachment(attachmentInfo, true);

            }
            catch (Exception ex)
            {
                result.ResultMessage = ex.Message;
            }
            return result;
        }

        private void UploadFileHelper_OnCompleted(object sender, AttachmentProxyService.UploadEventArgs e)
        {
            if (!e.UploadResult.Success)
            {
                if (UpLoadFailEvent != null)
                {
                    UpLoadFailEvent();
                }
            }
            else
            {
                #region 删除文件
                try
                {
                    if (File.Exists(e.AttachInfo.FileName))
                    {
                        File.Delete(e.AttachInfo.FileName);
                    }
                }
                catch (Exception)
                {
                }
                finally
                {
                    e.AttachInfo.AttachmentContentData.Close();
                    e.AttachInfo.AttachmentContentData.Dispose();
                }
                #endregion
                if (UpLoadSuccessEvent != null)
                {
                    UpLoadSuccessEvent();
                }
            }
        }



        /// <summary>
        /// 创建本地图片文件夹
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <param name="path"></param>
        private void CreateFile(string id, AttachmentType type, out string path)
        {
            var filePath = new StringBuilder();
            //创建文件夹
            filePath.AppendFormat(Files.FileParh);
            if (!Directory.Exists(filePath.ToString()))
            {
                Directory.CreateDirectory(filePath.ToString());
            }
            filePath.AppendFormat("\\{0}", id);
            if (!Directory.Exists(filePath.ToString()))
            {
                Directory.CreateDirectory(filePath.ToString());
            }
            filePath.AppendFormat("\\{0}", type);
            if (!Directory.Exists(filePath.ToString()))
            {
                Directory.CreateDirectory(filePath.ToString());
            }
            path = filePath.ToString();
        }
    }
}
