﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;


namespace PinganHouse.SHHTS.UI.WPF.Common
{
    public class CameraInterfaceInvoke
    {
        #region "windows dll import api"
        [DllImport("kernel32.dll")]
        private extern static IntPtr LoadLibrary(string path);

        [DllImport("Kernel32")]
        public static extern IntPtr LoadLibraryEx(String funcname, IntPtr hFile, uint dwFlags);


        [DllImport("kernel32.dll")]
        private extern static IntPtr GetProcAddress(IntPtr lib, string funcName);

        [DllImport("kernel32.dll")]
        private extern static bool FreeLibrary(IntPtr lib);
        #endregion

        #region "函数delegate原型定义"
        public delegate UInt32 InitDeviceHandler(UInt32 iDeviceIndex);
        public delegate int CloseDeviceHandler(UInt32 deviceHandle);
        public delegate int getFormatHandler(UInt32 deviceHandle, int iFormatIndex, ref int iWidth, ref int iHeight);
        public delegate int setCaptureSizeByIndexHandler(UInt32 deviceHandle, int iFormatIndex);
        public delegate int setPreviewWindowHandler(UInt32 deviceHandle, Int32 previewHwnd);
        public delegate int startPreviewHandler(UInt32 deviceHandle);
        public delegate int isPreviewHandler(UInt32 deviceHandle);
        public delegate int stopPreviewHandler(UInt32 deviceHandle);
        public delegate int captureBitmapHandler(UInt32 deviceHandle, string strFileName);
        public delegate int captureImageHandler(UInt32 deviceHandle, string strFileName);
        public delegate IntPtr getLastErrorMessageHandler();
        public delegate int setAutoCropHandler(UInt32 deviceHandle, int iEnabled);
        public delegate int setCameraPropertyHandler(UInt32 deviceHandle);
        public delegate int startAutoCaptureHandler(UInt32 deviceHandle, int iCaptureCount, int iCaptureInterval, string strFolder);
        public delegate int stopAutoCaptureHandler(UInt32 deviceHandle);
        public delegate int RotateVideoByDegreeHandler(UInt32 deviceHandle, int iDegree);
        public delegate int SetCaptureFilterHandler(UInt32 deviceHandle, int propertyType, Int32 Property, Int32 lValue);
        #endregion 

        #region "本身类变量"
        public string strErrorMessage = string.Empty;
        private IntPtr ptrLibrary = IntPtr.Zero;
        #endregion
        
        #region "委托方法"
        public InitDeviceHandler func_InitDevice;
        public CloseDeviceHandler func_CloseDevice;
        public setPreviewWindowHandler func_setPreviewWindow;
        public startPreviewHandler func_startPreview;
        public captureBitmapHandler func_captureBitmap;
        public captureImageHandler func_captureImage;
        public getFormatHandler func_getFormat;
        public isPreviewHandler func_isPreview;
        public stopPreviewHandler func_stopPreview;
        public setCaptureSizeByIndexHandler func_setCaptureSizeByIndex;
        public setAutoCropHandler func_setAutoCrop;
        public setCameraPropertyHandler func_setCameraProperty;
        public startAutoCaptureHandler func_startAutoCapture;
        public stopAutoCaptureHandler func_stopAutoCapture;
        public getLastErrorMessageHandler func_getLastErrorMessage;
        //public RotateVideoByDegreeHandler func_RotateVideoByDegree;
        //public SetCaptureFilterHandler func_SetCaptureFilter;
        #endregion

        public CameraInterfaceInvoke(string strDllName)
        {
            try
            {
                ptrLibrary = LoadLibrary(strDllName);
                Int32 p = Marshal.GetLastWin32Error();
            }
            catch
            {
            }

            if (ptrLibrary == IntPtr.Zero)
            {
                try
                {
                    ptrLibrary = LoadLibraryEx(strDllName, IntPtr.Zero, 8);
                }
                catch (Exception ex)
                {
                    strErrorMessage = string.Format("装载dll时发生错误{0}", ex.Message);
                }
            }

            if (ptrLibrary != IntPtr.Zero)
            {
                InitAllFunction();
            }
            else
            {
                strErrorMessage = "装载高拍仪类库失败";
            }
        }

        private void InitAllFunction()
        {
            if (ptrLibrary != IntPtr.Zero)
            {
                func_InitDevice = (InitDeviceHandler)ProcedureInvoke("InitDevice", typeof(InitDeviceHandler));
                func_CloseDevice = (CloseDeviceHandler)ProcedureInvoke("CloseDevice", typeof(CloseDeviceHandler));
                func_startPreview = (startPreviewHandler)ProcedureInvoke("StartPreview", typeof(startPreviewHandler));
                func_setPreviewWindow = (setPreviewWindowHandler)ProcedureInvoke("SetPreviewWindow", typeof(setPreviewWindowHandler));
                func_captureBitmap = (captureBitmapHandler)ProcedureInvoke("CaptureBitmap", typeof(captureBitmapHandler));
                func_captureImage = (captureImageHandler)ProcedureInvoke("CaptureImage", typeof(captureImageHandler));                
                func_getFormat = (getFormatHandler)ProcedureInvoke("GetResolutionByIndex", typeof(getFormatHandler));
                func_isPreview = (isPreviewHandler)ProcedureInvoke("IsPreview", typeof(isPreviewHandler));
                func_stopPreview = (stopPreviewHandler)ProcedureInvoke("StopPreview", typeof(stopPreviewHandler));
                func_setCaptureSizeByIndex = (setCaptureSizeByIndexHandler)ProcedureInvoke("SetCaptureSizeByIndex", typeof(setCaptureSizeByIndexHandler));
                func_setAutoCrop = (setAutoCropHandler)ProcedureInvoke("SetAutoCrop", typeof(setAutoCropHandler));
                func_setCameraProperty = (setCameraPropertyHandler)ProcedureInvoke("SetCameraProperty", typeof(setCameraPropertyHandler));
                func_startAutoCapture = (startAutoCaptureHandler)ProcedureInvoke("StartAutoCapture", typeof(startAutoCaptureHandler));
                func_stopAutoCapture = (stopAutoCaptureHandler)ProcedureInvoke("StopAutoCapture", typeof(stopAutoCaptureHandler));
                func_getLastErrorMessage = (getLastErrorMessageHandler)ProcedureInvoke("GetLastErrorMessage", typeof(getLastErrorMessageHandler));
                //func_RotateVideoByDegree = (RotateVideoByDegreeHandler)ProcedureInvoke("RotateVideoByDegree", typeof(RotateVideoByDegreeHandler));
                //func_SetCaptureFilter = (SetCaptureFilterHandler)ProcedureInvoke("SetCaptureFilter", typeof(SetCaptureFilterHandler));
            }
        }

        
        private Delegate ProcedureInvoke(string strFunctionName, Type t)
        {
            try
            {
                IntPtr ptrFunction = GetProcAddress(ptrLibrary, strFunctionName);
                return (Delegate)Marshal.GetDelegateForFunctionPointer(ptrFunction, t);
            }
            catch (Exception ex)
            {
                strErrorMessage = string.Format("调用函数{0}时发生错误{1}", strFunctionName, ex.Message);
                return null;
            }
        }


        ~CameraInterfaceInvoke()
        {
            FreeLibrary(ptrLibrary);
        }
    }
}
