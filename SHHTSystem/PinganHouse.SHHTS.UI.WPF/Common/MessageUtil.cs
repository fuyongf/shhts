﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;

namespace PinganHouse.SHHTS.UI.WPF.Common
{
    /// <summary>
    /// 全局消息提示帮助类--实现消息的统一管理
    /// </summary>
    public class MessageUtil
    {
        //存储Xml配置的提示信息
        public static  Hashtable MsgTbl = new Hashtable();

        public static string GetMsg(string messageId, params string[] paramArray)
        {
            try
            {
                  //置换字符
                  const string strReplace = "%R";
                  //置换位置
                  int nPos = 0;
                  //输出消息
                  string strOutputMessage;
                  //消息显示图标
                  var msgIcon= messageId.Substring(0, 1);
                  if (string.IsNullOrEmpty(messageId)){
                      strOutputMessage = string.Empty;
                  }
                  else {
                      strOutputMessage =Convert.ToString(MsgTbl[messageId]);
                  }
                 Match m  = Regex.Match(strOutputMessage, "^(\\S+)[#](\\S+)$");
                 if (m.Success) {
                     strOutputMessage = m.Groups[1].Value + System.Environment.NewLine + "\r" + m.Groups[2].Value;
                 }
                  //循环传入参数
                 var i = 0;
                 foreach (var para in paramArray)
                  {
                       nPos = strOutputMessage.IndexOf(strReplace);
                       if (nPos == -1) { 
                           strOutputMessage = strOutputMessage.Remove(nPos, 2);
                           strOutputMessage = strOutputMessage.Insert(nPos,Convert.ToString(paramArray[i]));
                       }
                       i++;
                  }
                  return strOutputMessage;
            }
            catch (Exception e) {
                throw e;
            }
        }

        /// <summary>
        /// 显示对话框
        /// </summary>
        /// <param name="messageId">消息编号</param>
        /// <param name="btnType">消息类型</param>
        /// <param name="paramArray">综合传入参数</param>
        /// <returns></returns>
        public static MessageBoxResult ShowMessageBox(string messageId, MessageBoxButton btnType, params string[] paramArray)
        {
            try {
                //返回值
                MessageBoxResult ret;
                //置换字符
                const string strReplace = "%R";
                //置换位置
                int nPos = 0;
                //输出消息
                string strOutputMessage;
                //消息显示图标
                var msgIcon = messageId.Substring(0, 1);
                strOutputMessage = (string)(MsgTbl[messageId]);
                if(string.IsNullOrEmpty(strOutputMessage)){
                    strOutputMessage = string.Empty;
                }
                Match m = Regex.Match(strOutputMessage, @"^(\S+)[#](\S+)$");
                if (m.Success)
                {
                    strOutputMessage = m.Groups[1].Value + System.Environment.NewLine + "\r" + m.Groups[2].Value;
                }
                //循环传入参数
                var i = 0;
                foreach (var para in paramArray)
                {
                    nPos = strOutputMessage.IndexOf(strReplace);
                    if (nPos>-1)
                    {
                        strOutputMessage = strOutputMessage.Remove(nPos, 2);
                        strOutputMessage = strOutputMessage.Insert(nPos, Convert.ToString(paramArray[i]));
                    }
                    i++;
                }
                switch (msgIcon) { 
                    case "W":
                        //Warning 
                        ret = MessageBox.Show(strOutputMessage, "系统警告", btnType, MessageBoxImage.Warning);
                        break;
                    case "Q":
                        //Question
                        ret = MessageBox.Show(strOutputMessage, "系统询问", btnType, MessageBoxImage.Question);
                        break;
                    case "E":
                        //Error
                        ret = MessageBox.Show(strOutputMessage, "系统错误", btnType, MessageBoxImage.Error);
                        break;
                    default:
                        //Infomation
                        ret = MessageBox.Show(strOutputMessage, "信息", btnType, MessageBoxImage.Information);
                        break;
                }
                return ret;
            }
            catch (Exception e) {
                throw e;
            }
        }

        /// <summary>
        /// 加载xml配置消息信息
        /// </summary>
        /// <param name="xmlPath"></param>
        public static void LoadMessageData(){
            try
            {
                var map = new ExeConfigurationFileMap();
                map.ExeConfigFilename = AppDomain.CurrentDomain.BaseDirectory + "AppMessage.config";
                var config = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
                var settings = config.AppSettings.Settings;
                foreach (var item in settings) {
                    var element=item as KeyValueConfigurationElement;
                    if (element == null) continue;
                    MsgTbl.Add(element.Key,element.Value);
                }
            }
            catch (Exception) {
                return;
            }
        }
    }
}
