﻿using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.Text;
using System.Windows.Forms;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPF.Common.BarCode;

namespace PinganHouse.SHHTS.UI.WPF.Common
{
    public class CaseCodePrint
    {
        private string _inputString;
        private string _nullCode;
        private string _num;
        private string _czh;
        private string _date;
        private string _address;
        private string _address1;
        private readonly PrintDocument _printDocument = new PrintDocument();

        private int _addrLen;
        private int _addrLen1;
        private int _addrLen2;

        /// <summary>
        /// 打印机名称
        /// </summary>
        //private const string PrintName = "ZDesigner GT800 (EPL)";

        /// <summary>
        /// 打印机名字
        /// </summary>
        //private string _PrintName;

        #region 打印字体大小显示长度参数

        /// <summary>
        /// 打印字体大小显示长度
        /// </summary>
        int strTotalLength = 30;

        /// <summary>
        /// 字体大小为11时，每行显示字符数
        /// </summary>
        private int showShortStrLength = 15;

        /// <summary>
        /// 字体大小为9时，每行显示字符数
        /// </summary>
        private int showLongStrLength = 22;

        #endregion

        /// <summary>
        /// 案件条形码打印接口
        /// </summary>
        /// <param name="sCaseId">案件编号</param>
        public void PrintCaseBarCode(string sCaseId)
        {
            try
            {
                string PrintName = PrivateProfileUtils.GetContentValue("BarCodePrint", "PrintName"); //打印机名

                _printDocument.PrinterSettings.PrinterName = PrintName;
                if (PrinterSettings.InstalledPrinters.Count == 0)
                {
                    //打印机未安装
                }
                CaseDto caseDto = new CaseDto();
                caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(sCaseId);
                if (caseDto != null)
                {

                    //产权证号
                    string sTenementContract = "";
                    if (caseDto.TenementContract != null)
                    {
                        var temps = caseDto.TenementContract.Split('-');
                        if (temps.Length > 1)
                        {
                            sTenementContract = temps[0] +" "+ temps[1];
                        }
                        else
                        {
                            sTenementContract = temps[0];
                        }
                    }
                    PrintCaseAddress(sCaseId, "", sTenementContract, caseDto.CreateDate.ToString("yyyy/MM/dd"),
                        caseDto.TenementAddress);
                }
            }
            catch (Exception)
            {
                throw new ArgumentException("未连接打印设备");
            }
        }


        /// <summary>
        /// 条形码打印接口 地址
        /// </summary>
        /// <param name="sCaseId">案件编号</param>
        /// <param name="nullCode">""</param>
        /// <param name="sCzh">产权证号</param>
        /// <param name="sDate">日期</param>
        /// <param name="sAddress">地址</param>
        private void PrintCaseAddress(string sCaseId, string nullCode, string sCzh, string sDate, string sAddress)
        {
            string addressOne = sAddress;
            string addressTwo = "";

            //控制行的字数
            if (sAddress.Length < strTotalLength)
            {
                //每行字数为15
                if (sAddress.Length > showShortStrLength)
                {

                    addressOne = sAddress.Substring(0, showShortStrLength);
                    addressTwo = sAddress.Substring(showShortStrLength,
                        sAddress.Length - showShortStrLength);
                }

            }
            else
            {
                addressOne = sAddress.Substring(0, showLongStrLength);
                addressTwo = sAddress.Substring(showLongStrLength, sAddress.Length - showLongStrLength);
            }
            PrintBarCodes(sCaseId, nullCode, sCzh, sDate, addressOne, addressTwo);
        }


        /// <summary>
        /// 条形码打印接口
        /// </summary>
        /// <param name="strs">案件编号</param>
        /// <param name="skuCode">""</param>
        /// <param name="sCzh">产证号</param>
        /// <param name="sDate">日期</param>
        /// <param name="sAddress">房屋地址</param>
        /// <param name="sAddress1"></param>
        private void PrintBarCodes(string strs, string nullCode, string sCzh, string sDate, string sAddress, string sAddress1)
        {
            //_printDocument.PrinterSettings.PrintFileName = "ZDesigner GT800 (EPL)";
            _num = strs.Length.ToString(CultureInfo.InvariantCulture);
            _nullCode = nullCode;
            _czh = sCzh;
            _date = sDate;
            _address = sAddress;
            _address1 = sAddress1;

            _addrLen1 = sAddress.Length;
            _addrLen2 = sAddress1.Length;
            _addrLen = _addrLen1 + _addrLen2;

            PrintCaseCode(strs);

        }

        /// <summary>
        /// 打印案件码
        /// </summary>
        /// <param name="str">案件码</param>
        private void PrintCaseCode(string str)
        {
            _inputString = str;
            var controler = new StandardPrintController();

            try
            {
                _printDocument.PrintPage += PrintBarCodeNumber;
                _printDocument.PrintController = controler;
                _printDocument.DefaultPageSettings.PaperSize = new PaperSize("Custum", 237, 119); //119  169
                //_printDocument.PrinterSettings.LandscapeAngle = 180;
                //var pDialog = new PrintPreviewDialog { Document = _printDocument };
                //pDialog.ShowDialog();
                _printDocument.Print();
            }
            catch (Exception err)
            {
                throw; 
            }
            finally
            {
                _printDocument.Dispose();
            }
        }

        private static readonly int pointFX = -3;

        //private static int _code128Size =int.Parse(Config.Get("Code128Size", "30"));
        //private static float _sx = float.Parse(Config.Get("SX", "2.3"));
        //private static float _sy = float.Parse(Config.Get("SY", "2.8"));

        private static readonly Font Count_Font = new Font("OCR-B-10 BT", 15, FontStyle.Bold, GraphicsUnit.Pixel);
        //private static readonly Font SKUCode_Font = new Font("Times New Roman", 10, FontStyle.Regular, GraphicsUnit.Pixel);
        private static readonly Font Code128_Font = new Font("Code 128", 40, FontStyle.Regular, GraphicsUnit.Pixel);
        private static readonly Font Czh_Font = new Font("微软雅黑", 12, GraphicsUnit.Pixel);
        private static readonly Font Date_Font = new Font("微软雅黑", 12, GraphicsUnit.Pixel);
        private static readonly Font Addr_Font = new Font("微软雅黑 Light", 13, GraphicsUnit.Pixel);
        private static readonly Font Addr1_Font = new Font("微软雅黑 Light", 13, GraphicsUnit.Pixel);
        private static readonly Font Addr_Font9 = new Font("微软雅黑 Light", 9, GraphicsUnit.Pixel);
        private static readonly Font Addr1_Font9 = new Font("微软雅黑 Light", 9, GraphicsUnit.Pixel);

        private readonly static PointF BarLocation = new PointF(0, 15 + pointFX); //条形码
        private readonly static PointF BarCodeLocation = new PointF(13, 55 + pointFX); //编码
        private readonly static PointF NumLocation = new PointF(100, 40);

        private readonly static Brush MyBrush = new SolidBrush(Color.Black);

        #region
        private readonly static PointF CzhLocation = new PointF(13, 0 + pointFX); //产证号
        private readonly static PointF DateLocation = new PointF(135, 0 + pointFX); //日期
        private readonly static PointF AddrLocation = new PointF(13, 75 + pointFX); //地址
        private readonly static PointF Addr1Location = new PointF(13, 91 + pointFX); //地址
        #endregion

        #region 9
        private readonly static PointF BarLocation9 = new PointF(0, 15); //条形码
        private readonly static PointF BarCodeLocation9 = new PointF(13, 55); //编码
        private readonly static PointF CzhLocation9 = new PointF(13, 0); //产证号
        private readonly static PointF DateLocation9 = new PointF(135, 0); //日期
        private readonly static PointF AddrLocation9 = new PointF(13, 75); //地址
        private readonly static PointF Addr1Location9 = new PointF(13, 88); //地址
        #endregion


        private void PrintBarCodeNumber(Object sender, PrintPageEventArgs av)
        {
            BarCodeHelper bc = new BarCodeHelper();
            Font CountFont = new Font(bc.privateFontCollection.Families[0], 15, FontStyle.Bold, GraphicsUnit.Pixel);

            Font Code128Font = new Font(bc.privateFontCollection128.Families[0], 40, FontStyle.Regular, GraphicsUnit.Pixel);

            var bandCode = Code128String.Get(_inputString);

            //av.Graphics.ScaleTransform(_sx, _sy);
            //av.Graphics.DrawString(bandCode, BarCodeHelper.Code128Font, MyBrush, BarLocation);
            av.Graphics.DrawString(bandCode, Code128Font, MyBrush, BarLocation);
            av.Graphics.DrawString(_czh, Czh_Font, MyBrush, CzhLocation);
            av.Graphics.DrawString(_date, Date_Font, MyBrush, DateLocation);
            if (_addrLen < strTotalLength)
            {
                av.Graphics.DrawString(_address, Addr_Font, MyBrush, AddrLocation);
                av.Graphics.DrawString(_address1, Addr1_Font, MyBrush, Addr1Location);
            }
            else
            {
                av.Graphics.DrawString(_address, Addr_Font9, MyBrush, AddrLocation9);
                av.Graphics.DrawString(_address1, Addr1_Font9, MyBrush, Addr1Location9);
            }
            //av.Graphics.ResetTransform();

            string sStr1 = "";
            string sStr2 = "";
            string sStr3 = "";
            string sStr4 = "";
            StringBuilder newStr = new StringBuilder();
            sStr1 += _inputString.Substring(0, 4);
            newStr.Append(sStr1);
            newStr.Append(" ");
            sStr2 += _inputString.Substring(4, 4);
            newStr.Append(sStr2);
            newStr.Append(" ");
            sStr3 += _inputString.Substring(8, 4);
            newStr.Append(sStr3);
            newStr.Append(" ");
            sStr4 += _inputString.Substring(12, 2);
            newStr.Append(sStr4);

            //string res = "";

            //for (int i = 0; i < _inputString.Length; i++)
            //{
            //    int j;
            //    Math.DivRem(i, 4, out j);
            //    if (j == 0)
            //    {
            //        res += _inputString.Substring(i, 4) + " ";
            //    }
            //}

            //av.Graphics.DrawString(_inputString, Count_Font, MyBrush, BarCodeLocation);
            //av.Graphics.DrawString(newStr.ToString(), BarCodeHelper.CountFont, MyBrush, BarCodeLocation);
            av.Graphics.DrawString(newStr.ToString(), CountFont, MyBrush, BarCodeLocation);
            //av.Graphics.DrawString(_num, Count_Font, MyBrush, NumLocation);
            av.HasMorePages = false;
        }
    }
}
