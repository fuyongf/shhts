﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Drawing;

namespace PinganHouse.SHHTS.UI.WPF.Common
{
    public class Code128Auto
    {
        #region 自定义变量

        /// <summary>
        /// 高度
        /// </summary>
        private uint _mHeight = 100;

        public uint Height
        {
            get { return _mHeight; }
            set { _mHeight = value; }
        }

        /// <summary>
        /// 放大倍数
        /// </summary>
        private byte _mMagnify;

        public byte Magnify
        {
            get { return _mMagnify; }
            set { _mMagnify = value; }
        }

        /// <summary>
        /// 条码内容
        /// </summary>
        private string _strContents;

        public string StrContents
        {
            get { return _strContents; }
            set { _strContents = value; }
        }

        /// <summary>
        /// Code128编码值
        /// </summary>
        private string _barcodeContents;

        public string BarcodeContents
        {
            get { return _barcodeContents; }
            set { _barcodeContents = value; }
        }

        /// <summary>
        /// Code128编码表
        /// </summary>
        private readonly DataTable _dt;

        #endregion

        #region 构造函数

        /// <summary>
        /// 构造函数
        /// </summary>
        public Code128Auto(string strContents)
        {
            _strContents = strContents;
            _dt = new DataTable();
            _dt.Columns.Add("ID", typeof (string));
            _dt.Columns.Add("CODE128A", typeof (string));
            _dt.Columns.Add("CODE128B", typeof (string));
            _dt.Columns.Add("CODE128C", typeof (string));
            _dt.Columns.Add("BANDCODE", typeof (string));
            _dt.Rows.Add("0", "SP", "SP", "00", "212222");
            _dt.Rows.Add("1", "!", "!", "01", "222122");
            _dt.Rows.Add("2", "\"", "\"", "02", "222221");
            _dt.Rows.Add("3", "#", "#", "03", "121223");
            _dt.Rows.Add("4", "$", "$", "04", "121322");
            _dt.Rows.Add("5", "%", "%", "05", "131222");
            _dt.Rows.Add("6", "&", "&", "06", "122213");
            _dt.Rows.Add("7", "'", "'", "07", "122312");
            _dt.Rows.Add("8", "(", "(", "08", "132212");
            _dt.Rows.Add("9", ")", ")", "09", "221213");
            _dt.Rows.Add("10", "*", "*", "10", "221312");
            _dt.Rows.Add("11", "+", "+", "11", "231212");
            _dt.Rows.Add("12", ",", ",", "12", "112232");
            _dt.Rows.Add("13", "-", "-", "13", "122132");
            _dt.Rows.Add("14", ".", ".", "14", "122231");
            _dt.Rows.Add("15", "/", "/", "15", "113222");
            _dt.Rows.Add("16", "0", "0", "16", "123122");
            _dt.Rows.Add("17", "1", "1", "17", "123221");
            _dt.Rows.Add("18", "2", "2", "18", "223211");
            _dt.Rows.Add("19", "3", "3", "19", "221132");
            _dt.Rows.Add("20", "4", "4", "20", "221231");
            _dt.Rows.Add("21", "5", "5", "21", "213212");
            _dt.Rows.Add("22", "6", "6", "22", "223112");
            _dt.Rows.Add("23", "7", "7", "23", "312131");
            _dt.Rows.Add("24", "8", "8", "24", "311222");
            _dt.Rows.Add("25", "9", "9", "25", "321122");
            _dt.Rows.Add("26", ":", ":", "26", "321221");
            _dt.Rows.Add("27", ";", ";", "27", "312212");
            _dt.Rows.Add("28", "<", "<", "28", "322112");
            _dt.Rows.Add("29", "=", "=", "29", "322211");
            _dt.Rows.Add("30", ">", ">", "30", "212123");
            _dt.Rows.Add("31", "?", "?", "31", "212321");
            _dt.Rows.Add("32", "@", "@", "32", "232121");
            _dt.Rows.Add("33", "A", "A", "33", "111323");
            _dt.Rows.Add("34", "B", "B", "34", "131123");
            _dt.Rows.Add("35", "C", "C", "35", "131321");
            _dt.Rows.Add("36", "D", "D", "36", "112313");
            _dt.Rows.Add("37", "E", "E", "37", "132113");
            _dt.Rows.Add("38", "F", "F", "38", "132311");
            _dt.Rows.Add("39", "G", "G", "39", "211313");
            _dt.Rows.Add("40", "H", "H", "40", "231113");
            _dt.Rows.Add("41", "I", "I", "41", "231311");
            _dt.Rows.Add("42", "J", "J", "42", "112133");
            _dt.Rows.Add("43", "K", "K", "43", "112331");
            _dt.Rows.Add("44", "L", "L", "44", "132131");
            _dt.Rows.Add("45", "M", "M", "45", "113123");
            _dt.Rows.Add("46", "N", "N", "46", "113321");
            _dt.Rows.Add("47", "O", "O", "47", "133121");
            _dt.Rows.Add("48", "P", "P", "48", "313121");
            _dt.Rows.Add("49", "Q", "Q", "49", "211331");
            _dt.Rows.Add("50", "R", "R", "50", "231131");
            _dt.Rows.Add("51", "S", "S", "51", "213113");
            _dt.Rows.Add("52", "T", "T", "52", "213311");
            _dt.Rows.Add("53", "U", "U", "53", "213131");
            _dt.Rows.Add("54", "V", "V", "54", "311123");
            _dt.Rows.Add("55", "W", "W", "55", "311321");
            _dt.Rows.Add("56", "X", "X", "56", "331121");
            _dt.Rows.Add("57", "Y", "Y", "57", "312113");
            _dt.Rows.Add("58", "Z", "Z", "58", "312311");
            _dt.Rows.Add("59", "[", "[", "59", "332111");
            _dt.Rows.Add("60", @"\", @"\", "60", "314111");
            _dt.Rows.Add("61", "]", "]", "61", "221411");
            _dt.Rows.Add("62", "^", "^", "62", "431111");
            _dt.Rows.Add("63", "_", "_", "63", "111224");
            _dt.Rows.Add("64", "NUL", "`", "64", "111422");
            _dt.Rows.Add("65", "SOH", "a", "65", "121124");
            _dt.Rows.Add("66", "STX", "b", "66", "121421");
            _dt.Rows.Add("67", "ETX", "c", "67", "141122");
            _dt.Rows.Add("68", "EOT", "d", "68", "141221");
            _dt.Rows.Add("69", "ENQ", "e", "69", "112214");
            _dt.Rows.Add("70", "ACK", "f", "70", "112412");
            _dt.Rows.Add("71", "BEL", "g", "71", "122114");
            _dt.Rows.Add("72", "BS", "h", "72", "122411");
            _dt.Rows.Add("73", "HT", "i", "73", "142112");
            _dt.Rows.Add("74", "LF", "j", "74", "142211");
            _dt.Rows.Add("75", "VT", "k", "75", "241211");
            _dt.Rows.Add("76", "FF", "l", "76", "221114");
            _dt.Rows.Add("77", "CR", "m", "77", "413111");
            _dt.Rows.Add("78", "SO", "n", "78", "241112");
            _dt.Rows.Add("79", "SI", "o", "79", "134111");
            _dt.Rows.Add("80", "DLE", "p", "80", "111242");
            _dt.Rows.Add("81", "DC1", "q", "81", "121142");
            _dt.Rows.Add("82", "DC2", "r", "82", "121241");
            _dt.Rows.Add("83", "DC3", "s", "83", "114212");
            _dt.Rows.Add("84", "DC4", "t", "84", "124112");
            _dt.Rows.Add("85", "NAK", "u", "85", "124211");
            _dt.Rows.Add("86", "SYN", "v", "86", "411212");
            _dt.Rows.Add("87", "ETB", "w", "87", "421112");
            _dt.Rows.Add("88", "CAN", "x", "88", "421211");
            _dt.Rows.Add("89", "EM", "y", "89", "212141");
            _dt.Rows.Add("90", "SUB", "z", "90", "214121");
            _dt.Rows.Add("91", "ESC", "{", "91", "412121");
            _dt.Rows.Add("92", "FS", "|", "92", "111143");
            _dt.Rows.Add("93", "GS", "}", "93", "111341");
            _dt.Rows.Add("94", "RS", "~", "94", "131141");
            _dt.Rows.Add("95", "US", "DEL", "95", "114113");
            _dt.Rows.Add("96", "FNC3", "FNC3", "96", "114311");
            _dt.Rows.Add("97", "FNC2", "FNC2", "97", "411113");
            _dt.Rows.Add("98", "SHIFT", "SHIFT", "98", "411311");
            _dt.Rows.Add("99", "CODEC", "CODEC", "99", "113141");
            _dt.Rows.Add("100", "CODEB", "FNC4", "CODEB", "114131");
            _dt.Rows.Add("101", "FNC4", "CODEA", "CODEA", "311141");
            _dt.Rows.Add("102", "FNC1", "FNC1", "FNC1", "411131");
            _dt.Rows.Add("103", "StartA", "StartA", "StartA", "211412");
            _dt.Rows.Add("104", "StartB", "StartB", "StartB", "211214");
            _dt.Rows.Add("105", "StartC", "StartC", "StartC", "211232");
            _dt.Rows.Add("106", "Stop", "Stop", "Stop", "2331112");
        }

        #endregion

        /// <summary>
        /// 获取编码位图
        /// </summary>
        /// <returns></returns>
        public Bitmap GetCodeImage()
        {
            string strStart; //开始位         
            int intcheck = 0; //校验位(ID值)
            var strSplit = new List<string>(); //字符串拆分列表
            var rgTwoNum = new Regex(@"^[0-9]{2}.*"); //任意两个连续数字（适用CodeC编码）

            bool flagCodeC = false; //CodeC部分的标志
            bool flagCodeA = false; //CodeA部分的标志

            #region 对字符串进行拆分，拆分为CODEC和CODEA部分

            int i = 0;
            while (i < _strContents.Length)
            {
                if ((i != _strContents.Length-1) && (rgTwoNum.IsMatch(_strContents.Substring(i, 2))))
                {
                    if (flagCodeC == false)
                    {
                        strSplit.Add("CODEC");
                    }
                    strSplit.Add(_strContents.Substring(i, 2));
                    i += 2;
                    flagCodeC = true;
                    flagCodeA = false;
                }
                else
                {
                    if (flagCodeA == false)
                    {
                        strSplit.Add("CODEA");
                    }
                    flagCodeA = true;
                    flagCodeC = false;
                    strSplit.Add(_strContents.Substring(i, 1));
                    i += 1;
                }
            }
            

            #endregion

            #region 对起始位进行处理

            switch (strSplit[0].ToString(CultureInfo.InvariantCulture))
            {
                case "CODEA":
                    strSplit[0] = "103";  //用于校验位计算用
                    strStart = "211412";
                    break;
                case "CODEC":
                    strSplit[0] = "105";
                    strStart = "211232";
                    break;
                default:
                    strStart = "";
                    break;
            }

            #endregion

            #region 进行校验位的处理以及条码字符串编码化处理

            for (int j = 1; j < strSplit.Count; j++)
            {
                if (strSplit[j].ToString(CultureInfo.InvariantCulture).Length > 1)  
                {
                    switch (strSplit[j].ToString(CultureInfo.InvariantCulture))
                    {
                        case "CODEA":
                            intcheck += 101*j;
                            _barcodeContents += "311141";
                            break;
                        case "CODEC":
                            intcheck += 99*j;
                            _barcodeContents += "113141";
                            break;
                        default:
                            foreach (DataRow dr in _dt.Rows.Cast<DataRow>().
                                                       Where(dr => dr[3].ToString().Equals(strSplit[j].ToString(CultureInfo.InvariantCulture))))
                            {
                                intcheck += Int32.Parse(dr[0].ToString())*j;
                                _barcodeContents += dr[4].ToString();
                            }
                            break;
                    }
                }
                else
                {
                    foreach (DataRow dr in _dt.Rows.Cast<DataRow>().
                        Where(dr => dr[1].ToString().Equals(strSplit[j].ToString(CultureInfo.InvariantCulture))))
                    {
                        intcheck += Int32.Parse(dr[0].ToString())*j;
                        _barcodeContents += dr[4].ToString();
                    }
                }
            }

            #endregion

            #region 计算校验位，并获取其编码

            intcheck += Int32.Parse(strSplit[0]);
            string strcheck = (intcheck%103).ToString(CultureInfo.InvariantCulture);


            foreach (DataRow dr in _dt.Rows)
            {
                if (dr[0].ToString().Equals(strcheck))
                {
                    strcheck = dr[4].ToString();
                    break;
                }
            }

            #endregion

            _barcodeContents = strStart + _barcodeContents + strcheck + "2331112";
            _barcodeContents = _barcodeContents.Replace("…", "");
            Bitmap codeImage = GetImage(_barcodeContents);
            return codeImage;
        }

        /// <summary>
        /// 获得条码图形
        /// </summary>
        /// <param name="pText">文字</param>
        /// <returns>图形</returns>
        private Bitmap GetImage(string pText)
        {
            char[] value = pText.ToCharArray();
            int width = 0;
            for (int i = 0; i != value.Length; i++)
            {
                width += Int32.Parse(value[i].ToString(CultureInfo.InvariantCulture))*(_mMagnify + 1);
            }
            var codeImage = new Bitmap(width, (int) _mHeight);
            var garphics = Graphics.FromImage(codeImage);
            int lenEx = 0;
            for (int i = 0; i != value.Length; i++)
            {
                int valueNumb = Int32.Parse(value[i].ToString(CultureInfo.InvariantCulture))*(_mMagnify + 1); //获取宽和放大系数
                garphics.FillRectangle((i & 1) != 0 ? Brushes.White : Brushes.Black,
                                       new Rectangle(lenEx, 0, valueNumb, (int) _mHeight));
                lenEx += valueNumb;
            }
            garphics.Dispose();
            return codeImage;
        }
    }
}
