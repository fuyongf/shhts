﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.UI.WPF.Common.BarCode
{
    class CaseCodeImage
    {
        private static readonly int pointFX = -3;

        //案件编号
        private static readonly Font Count_Font = new Font("OCR-B-10 BT", 15, FontStyle.Bold, GraphicsUnit.Pixel);
        //案件编号条形码
        private static readonly Font Code128_Font = new Font("Code 128", 40, FontStyle.Regular, GraphicsUnit.Pixel);

        private readonly static PointF BarLocation = new PointF(0, 15 + pointFX); //条形码
        private readonly static PointF BarCodeLocation = new PointF(13, 55 + pointFX); //编码
    }
}
