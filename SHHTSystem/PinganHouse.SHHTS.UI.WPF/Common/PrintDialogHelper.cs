﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Printing;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace PinganHouse.SHHTS.UI.WPF.Common
{
    class PrintDialogHelper
    {
        //private const string PrintServerName = "servicecenter00";
        private const string PrintName = "HP LaserJet 200 color M251 PCL 6";

        public PrintDialogHelper()
        {
        }

        /// <summary>
        /// 打印控件
        /// </summary>
        /// <param name="element"></param>
        public void PrintVisual(System.Windows.Controls.Grid element)
        {
            var printDialog = new PrintDialog();

            PrintCapabilities pc = printDialog.PrintQueue.GetPrintCapabilities();

            SetPrintProperty(printDialog);
            var printQueue = SelectedPrintServer(System.Environment.MachineName, PrintName);
            if (printQueue != null) 
            {
                printDialog.PrintQueue = printQueue;
                printDialog.PrintVisual(element, "");
            }
        }

        /// <summary>
        /// 查找并获取打印机
        /// </summary>
        /// <param name="printerServerName">服务器名字： Lee-pc</param0
        /// <param name="printerName">打印机名字：Hp laserjet m1522 mfp series pcl 6 </param>
        /// <returns></returns>
        public static PrintQueue SelectedPrintServer(string printerServerName, string printerName)
        {
            try
            {
                var printers = PrinterSettings.InstalledPrinters;//获取本机上的所有打印机

                PrintServer printServer = null;
                foreach (string printer in printers)
                {
                    if (printer.Contains(printerName))
                        printServer = new PrintServer("\\\\" + printerServerName);
                }

                if (printServer == null) return null;//没有找到打印机服务器

                var printQueue = printServer.GetPrintQueue(printerName);

                //LocalPrintServer ps = null;
                //foreach (string printer in printers)
                //{
                //    if (printer.Contains(printerName))
                //    {
                //        ps = new LocalPrintServer();
                //    }
                //}
                //var printQueue = ps.GetPrintQueue(printerName);
                
                return printQueue;
            }
            catch (Exception)
            {
                return null;//没有找到打印机
            }
        }

        /// <summary>
        /// 设置打印格式
        /// </summary>
        /// <param name="printDialog">打印文档</param>
        /// <param name="pageSize">打印纸张大小 a4</param>
        /// <param name="pageOrientation">打印方向 竖向</param>
        public static void SetPrintProperty(PrintDialog printDialog, PageMediaSizeName pageSize = PageMediaSizeName.ISOA5, PageOrientation pageOrientation = PageOrientation.Landscape)
        {
            var printTicket = printDialog.PrintTicket;
            printTicket.PageMediaSize = new PageMediaSize(pageSize, 560, 795);//A4纸
            printTicket.PageOrientation = pageOrientation;//默认竖向打印
        }
    }
}
