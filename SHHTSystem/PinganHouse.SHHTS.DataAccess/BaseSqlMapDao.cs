using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using IBatisNet.Common.Exceptions;
using IBatisNet.DataMapper;
using IBatisNet.DataMapper.MappedStatements;
using IBatisNet.DataMapper.Scope;

using PinganHouse.SHHTS.Core.DataAccess;

namespace PinganHouse.SHHTS.DataAccess
{
    /// <summary>
    /// SqlMap访问层基类。
    /// </summary>
    /// <typeparam name="T">类型参数。</typeparam>
    public abstract class BaseSqlMapDao<T> where T : class
    {
        /// <summary>
        /// 获取本地的SqlMap指示器。
        /// </summary>
        /// <returns>本地的SqlMap指示器。</returns>
        protected ISqlMapper GetLocalSqlMap()
        {
            return ProviderBase.Get();
        }

        /// <summary>
        /// 查询出DataSet数据。
        /// </summary>
        /// <param name="statementName">给定的表达式名称。</param>
        /// <param name="parameterObject">给定的对象参数。</param>
        /// <param name="returnValues">返回值集合</param>
        /// <param name="commandType">执行类型</param>
        /// <returns>符合条件的DataSet。</returns>
        protected DataSet ExecuteQueryWithDataSet(string statementName, object parameterObject, out Hashtable returnValues, CommandType commandType)
        {
            returnValues = new Hashtable();

            DataSet ds = new DataSet();
            long ts = DateTime.Now.Ticks;
            ISqlMapper sqlMap = GetLocalSqlMap();
            try
            {
                IMappedStatement statement = sqlMap.GetMappedStatement(statementName);
                if (!sqlMap.IsSessionStarted)
                    sqlMap.OpenConnection();

                RequestScope scope = statement.Statement.Sql.GetRequestScope(
                    statement,
                    parameterObject,
                    sqlMap.LocalSession);

                statement.PreparedCommand.Create(
                    scope,
                    sqlMap.LocalSession,
                    statement.Statement,
                    parameterObject);

                IDbCommand command = sqlMap.LocalSession.CreateCommand(commandType);
                command.CommandText = scope.IDbCommand.CommandText;
                foreach (IDataParameter parameter in scope.IDbCommand.Parameters)
                {
                    IDbDataParameter para = sqlMap.LocalSession.CreateDataParameter();
                    para.ParameterName = parameter.ParameterName;

                    //修正IBatis.NET的一个传值总为DBNull
                    para.Value = parameter.Value == DBNull.Value
                        ? ((IDictionary)parameterObject)[parameter.ParameterName.TrimStart('@')]
                        : parameter.Value;

                    para.Direction = parameter.Direction;
                    command.Parameters.Add(para);
                }
                sqlMap.LocalSession.CreateDataAdapter(command).Fill(ds);

                foreach (IDataParameter parameter in command.Parameters)
                {
                    if (parameter.Direction == ParameterDirection.Output)
                        returnValues.Add(parameter.ParameterName, parameter.Value);

                }
                //Log.Dao(string.Format(Configs.WMSLogFormat, statementName, "ExecuteQueryWithDataSet", DateTime.Now.Ticks - ts, ""));
            }
            catch
            {
                //Log.Error(string.Format(Configs.WMSLogFormat, statementName, "ExecuteQueryWithDataSet", DateTime.Now.Ticks - ts, e.ToString()));
                throw;
            }
            finally
            {
                if (!(sqlMap.IsSessionStarted && sqlMap.LocalSession.IsTransactionStart))
                    sqlMap.CloseConnection();
            }

            return ds;
        }

        /// <summary>
        /// 查询出DataSet数据(sql语句)
        /// </summary>
        /// <param name="statementName">给定的表达式名称。</param>
        /// <param name="parameterObject">给定的对象参数。</param>
        /// <returns>符合条件的DataSet。</returns>
        protected DataSet ExecuteQueryWithDataSet(string statementName, object parameterObject)
        {
            Hashtable hs;
            return ExecuteQueryWithDataSet(statementName, parameterObject, out hs, CommandType.Text);

        }

        /// <summary>
        /// 根据给定的表达式名称和参数选出对象集合。
        /// </summary>
        /// <param name="statementName">给定的表达式名称。</param>
        /// <param name="parameterObject">给定的对象参数。</param>
        /// <returns>符合条件的对象集合。</returns>
        protected IList<T> ExecuteQueryForList(string statementName, object parameterObject)
        {
            IList<T> list;
            long ts = DateTime.Now.Ticks;
            ISqlMapper sqlMap = GetLocalSqlMap();
            try
            {
                list = sqlMap.QueryForList<T>(statementName, parameterObject);
                //Log.Dao(string.Format(Configs.WMSLogFormat, statementName, "ExecuteQueryForList", DateTime.Now.Ticks - ts, ""));
            }
            catch
            {
                //Log.Error(string.Format(Configs.WMSLogFormat, statementName, "ExecuteQueryForList", DateTime.Now.Ticks - ts, e));
                throw;
            }
            return list;
        }
        protected IList<K> ExecuteQueryForList<K>(string statementName, object parameterObject)
        {
            IList<K> list;
            long ts = DateTime.Now.Ticks;
            ISqlMapper sqlMap = GetLocalSqlMap();
            try
            {
                list = sqlMap.QueryForList<K>(statementName, parameterObject);
                //Log.Dao(string.Format(Configs.WMSLogFormat, statementName, "ExecuteQueryForList", DateTime.Now.Ticks - ts, ""));
            }
            catch
            {
                //Log.Error(string.Format(Configs.WMSLogFormat, statementName, "ExecuteQueryForList", DateTime.Now.Ticks - ts, e));
                throw;
            }
            return list;
        }

        /// <summary>
        /// 根据给定的表达式名称和参数选出对象集合。
        /// </summary>
        /// <param name="statementName">给定的表达式名称。</param>
        /// <param name="parameterObject">给定的对象参数。</param>
        /// <returns>符合条件的对象集合。</returns>
        protected IList ExecuteQueryForAnyList(string statementName, object parameterObject)
        {
            IList list;
            long ts = DateTime.Now.Ticks;
            ISqlMapper sqlMap = GetLocalSqlMap();
            try
            {
                list = sqlMap.QueryForList(statementName, parameterObject);
                //Log.Dao(string.Format(Configs.WMSLogFormat, statementName, "ExecuteQueryForAnyList", DateTime.Now.Ticks - ts, ""));
            }
            catch
            {
                //Log.Error(string.Format(Configs.WMSLogFormat, statementName, "ExecuteQueryForAnyList", DateTime.Now.Ticks - ts, e));
                throw;
            }
            return list;
        }

        /// <summary>
        /// 根据给定的表达式名称和参数选出对象集合。
        /// </summary>
        /// <param name="statementName">给定的表达式名称。</param>
        /// <param name="parameterObject">给定的对象参数。</param>
        /// <param name="keyProperty">指定Key的属性名 </param>
        /// <returns>符合条件的对象集合。</returns>
        protected IDictionary<string, T> ExecuteQueryForDictionary(string statementName, object parameterObject, string keyProperty)
        {
            ISqlMapper sqlMap = GetLocalSqlMap();

            try
            {
                return sqlMap.QueryForDictionary<string, T>(statementName, parameterObject, keyProperty);
            }
            catch (Exception e)
            {
                throw new IBatisNetException(
                    string.Format("Error executing query '{0}' for list. Cause: {1}", statementName, e.Message), e);
            }
        }

        /// <summary>
        /// 根据给定的表达式名称和参数选出对象集合。
        /// </summary>
        /// <param name="statementName">给定的表达式名称。</param>
        /// <param name="parameterObject">给定的对象参数。</param>
        /// <param name="keyProperty">指定Key的属性名 </param>
        /// <returns>符合条件的对象集合。</returns>
        protected IDictionary<TK, T> ExecuteQueryForDictionary<TK>(string statementName, object parameterObject, string keyProperty)
        {
            ISqlMapper sqlMap = GetLocalSqlMap();

            try
            {
                return sqlMap.QueryForDictionary<TK, T>(statementName, parameterObject, keyProperty);
            }
            catch (Exception e)
            {
                throw new IBatisNetException(
                    string.Format("Error executing query '{0}' for list. Cause: {1}", statementName, e.Message), e);
            }
        }

        /// <summary>
        /// 根据给定的表达式名称和参数选出对象集合。
        /// </summary>
        /// <param name="statementName">给定的表达式名称。</param>
        /// <param name="parameterObject">给定的对象参数。</param>
        /// <param name="keyProperty">指定Key的属性名 </param>
        /// <param name="valueProperty">指定Value的属性名 </param>
        /// <returns>符合条件的对象集合。</returns>
        protected IDictionary<TK, TV> ExecuteQueryForDictionary<TK, TV>(string statementName, object parameterObject, string keyProperty, string valueProperty)
        {
            ISqlMapper sqlMap = GetLocalSqlMap();

            try
            {
                return sqlMap.QueryForDictionary<TK, TV>(statementName, parameterObject, keyProperty, valueProperty);
            }
            catch (Exception e)
            {
                throw new IBatisNetException(
                    string.Format("Error executing query '{0}' for list. Cause: {1}", statementName, e.Message), e);
            }
        }

        /// <summary>
        /// 根据给定的表达式名称和参数选出对象集合。
        /// </summary>
        /// <param name="statementName">给定的表达式名称。</param>
        /// <param name="parameterObject">给定的对象参数。</param>
        /// <param name="skipResults">要跳过的行数。</param>
        /// <param name="maxResults">返回的最大行数。</param>
        /// <returns>符合条件的对象集合。</returns>
        protected IList<T> ExecuteQueryForList(string statementName, object parameterObject, int skipResults, int maxResults)
        {
            ISqlMapper sqlMap = GetLocalSqlMap();
            try
            {
                return sqlMap.QueryForList<T>(statementName, parameterObject, skipResults, maxResults);
            }
            catch (Exception e)
            {
                throw new IBatisNetException(
                                    string.Format("Error executing query '{0}' for list. Cause: {1}", statementName, e.Message), e);
            }
        }

        /// <summary>
        /// 根据给定的表达式名称和参数查询对象。
        /// </summary>
        /// <param name="statementName">给定的表达式名称。</param>
        /// <param name="parameterObject">给定的参数对象。</param>
        /// <returns>查询到的对象。</returns>
        protected T ExecuteQueryForObject(string statementName, object parameterObject)
        {
            ISqlMapper sqlMap = GetLocalSqlMap();

            try
            {
                return sqlMap.QueryForObject<T>(statementName, parameterObject);
            }
            catch (Exception e)
            {
                throw new IBatisNetException(
                                   string.Format("Error executing query '{0}' for object. Cause: {1}", statementName, e.Message), e);
            }
        }

        /// <summary>
        /// 根据给定的表达式名称和参数查询单一结果值。
        /// </summary>
        /// <param name="statementName">给定的表达式名称。</param>
        /// <param name="parameterObject">给定的参数对象。</param>
        /// <returns>查询到的对象。</returns>
        protected object ExecuteQueryForAnyObject(string statementName, object parameterObject)
        {
            ISqlMapper sqlMap = GetLocalSqlMap();

            try
            {
                return sqlMap.QueryForObject(statementName, parameterObject);
            }
            catch (Exception e)
            {
                throw new IBatisNetException(
                                   string.Format("Error executing query '{0}' for object. Cause: {1}", statementName, e.Message), e);
            }
        }

        /// <summary>
        /// 根据给定的表达式名称和参数进行更新。
        /// </summary>
        /// <param name="statementName">给定的表达式名称。</param>
        /// <param name="parameterObject">给定的参数。</param>
        /// <returns>该操作所影响的行数。</returns>
        protected int ExecuteUpdate(string statementName, object parameterObject)
        {
            ISqlMapper sqlMap = GetLocalSqlMap();
            try
            {
                return sqlMap.Update(statementName, parameterObject);
            }
            catch (Exception e)
            {
                throw new IBatisNetException(
                                  string.Format("Error executing query '{0}' for list. Cause: {1}", statementName, e.Message), e);
            }
        }

        /// <summary>
        /// 根据给定的表达式名称和参数进行元素删除
        /// </summary>
        /// <param name="statementName">给定的表达式名称。</param>
        /// <param name="parameterObject">给定的参数。</param>
        /// <returns>该操作所影响的行数。</returns>
        protected int ExecuteDelete(string statementName, object parameterObject)
        {
            ISqlMapper sqlMap = GetLocalSqlMap();
            try
            {
                return sqlMap.Delete(statementName, parameterObject);
            }
            catch (Exception e)
            {
                throw new IBatisNetException(
                                  string.Format("Error executing query '{0}' for list. Cause: {1}", statementName, e.Message), e);
            }
        }


        /// <summary>
        /// 根据给定的表达式名称和参数进行插入。
        /// </summary>
        /// <param name="statementName">给定的表达式名称。</param>
        /// <param name="parameterObject">给定的参数。</param>
        /// <returns>插入的对象所返回的主键。</returns>
        protected object ExecuteInsert(string statementName, object parameterObject)
        {
            ISqlMapper sqlMap = GetLocalSqlMap();
            try
            {
                return sqlMap.Insert(statementName, parameterObject);
            }
            catch (Exception e)
            {
                throw new IBatisNetException(
                    string.Format("Error executing query '{0}' for list. Cause: {1}", statementName, e.Message), e);
            }
        }

        /// <summary>
        /// 查询分页列表
        /// </summary>
        /// <param name="statementName"></param>
        /// <param name="table"></param>
        /// <param name="fields"></param>
        /// <param name="condition"></param>
        /// <param name="orderby"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        protected IList<T> ExecuteQueryForPaginatedList(string table, string fields, string condition, string orderby, int pageIndex, int pageSize, out int totalCount)
        {
            var sqlmap = GetLocalSqlMap();
            try
            {
                totalCount = 0;
                var parameters = SetParameters(table, fields, condition, orderby, pageIndex, pageSize, totalCount);
                var result = sqlmap.QueryForList<T>(string.Format("{0}.GetPaginatedList", typeof(T).Name), parameters);

                totalCount = (int)parameters["Count"];
                return result;
            }
            catch (Exception e)
            {
                throw new IBatisNetException(
                                     string.Format("Error executing query '{0}' for paginated list. Cause: {1}", string.Format("{0}.GetPaginatedList", typeof(T).Name), e.Message), e);
            }
        }

        /// <summary>
        /// 查询分页列表
        /// </summary>
        /// <param name="statementName"></param>
        /// <param name="table"></param>
        /// <param name="fields"></param>
        /// <param name="condition"></param>
        /// <param name="orderby"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        protected IList<TResult> ExecuteQueryForPaginatedList<TResult>(string table, string fields, string condition, string orderby, int pageIndex, int pageSize, out int totalCount)
        {
            var sqlmap = GetLocalSqlMap();
            try
            {
                totalCount = 0;
                var parameters = SetParameters(table, fields, condition, orderby, pageIndex, pageSize, totalCount);
                var result = sqlmap.QueryForList<TResult>(string.Format("{0}.GetPaginatedList", typeof(T).Name), parameters);

                totalCount = (int)parameters["Count"];
                return result;
            }
            catch (Exception e)
            {
                throw new IBatisNetException(
                                     string.Format("Error executing query '{0}' for paginated list. Cause: {1}", string.Format("{0}.GetPaginatedList", typeof(T).Name), e.Message), e);
            }
        }

        protected IList<TResult> ExecuteQueryForPaginatedList<TResult>(string stName, string table, string fields, string condition, string orderby, int pageIndex, int pageSize, out int totalCount)
        {
            var sqlmap = GetLocalSqlMap();
            try
            {
                totalCount = 0;
                var parameters = SetParameters(table, fields, condition, orderby, pageIndex, pageSize, totalCount);
                var result = sqlmap.QueryForList<TResult>(string.Format("{0}.{1}", typeof(T).Name, stName), parameters);

                totalCount = (int)parameters["Count"];
                return result;
            }
            catch (Exception e)
            {
                throw new IBatisNetException(
                                     string.Format("Error executing query '{0}' for paginated list. Cause: {1}", string.Format("{0}.{1}", typeof(T).Name, stName), e.Message), e);
            }
        }


        protected IList<T> ExecuteQueryForPaginatedList(string table, string fields, string condition, string orderby, string withQuery, int pageIndex, int pageSize, out int totalCount)
        {
            var sqlmap = GetLocalSqlMap();
            try
            {
                totalCount = 0;
                var parameters = SetParameters(table, fields, condition, orderby, pageIndex, pageSize, totalCount);
                parameters.Add("With", withQuery);
                var result = sqlmap.QueryForList<T>(string.Format("{0}.GetPaginatedList", typeof(T).Name), parameters);

                totalCount = (int)parameters["Count"];
                return result;
            }
            catch (Exception e)
            {
                throw new IBatisNetException(
                                     string.Format("Error executing query '{0}' for paginated list. Cause: {1}", string.Format("{0}.GetPaginatedList", typeof(T).Name), e.Message), e);
            }
        }



        //protected DataSet ExecuteQueryForDataSet(string table, string fields, string condition, string orderby, int pageIndex, int pageSize, out int totalCount, string groupby, string with=null)
        //{
        //    var sqlmap = GetLocalSqlMap();
        //    try
        //    {
        //        totalCount = 0;
        //        var parameters = SetParameters(table, fields, condition, orderby, pageIndex, pageSize, totalCount);
        //        parameters.Add("GroupBy", groupby);
        //        if(!string.IsNullOrWhiteSpace(with))
        //            parameters.Add("With", with);
        //        Hashtable hashtable =new Hashtable();
        //        var result = ExecuteQueryWithDataSet(string.Format("{0}.GetPaginatedData", typeof(T).Name), parameters, out hashtable, CommandType.StoredProcedure);
        //        totalCount = (int)hashtable["@Count"];
        //        return result;
        //    }
        //    catch (Exception e)
        //    {
        //        throw new IBatisNetException(
        //                             string.Format("Error executing query '{0}' for paginated list. Cause: {1}", string.Format("{0}.GetPaginatedList", typeof(T).Name), e.Message), e);
        //    }
        //}

        private Dictionary<string, object> SetParameters(string table, string fields, string condition, string orderby, int pageIndex, int pageSize, int totalCount)
        {
            return new Dictionary<string, object> 
                {
                    {"Table",table},
                    {"Fields",fields},
                    {"Where",condition},
                    {"OrderBy",orderby},
                    {"CurrentPage",pageIndex},
                    {"PageSize",pageSize},
                    {"GetCount",0},
                    {"Count",totalCount}
                };
        }
    }
}
