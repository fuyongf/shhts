﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using IBatisNet.Common.Utilities;
using IBatisNet.DataMapper;
using IBatisNet.DataMapper.Configuration;
using System.Web;

namespace PinganHouse.SHHTS.DataAccess
{
    /// <summary>
    /// 数据访问配置类。
    /// </summary>
    public class ProviderBase
    {
        private static volatile ISqlMapper _mapper;
        private static volatile ISqlMapper _localMapper;
        private static object HttpSyncRoot = new object();
        private static object LocalSyncRoot = new object();

        private ProviderBase() { }

        /// <summary>    
        /// Get the instance of the SqlMapper defined by the SqlMap.Config file.    
        /// </summary>    
        /// <returns>A SqlMapper initalized via the SqlMap.Config file.</returns>    
        public static ISqlMapper Instance()
        {
            if (_mapper == null)
            {
                lock (HttpSyncRoot)
                {
                    if (_mapper == null) // double-check    
                    {
                        InitMapper(1);
                    }
                }
            }
            return _mapper;
        }

        public static ISqlMapper LocalInstance() 
        {
            if (_localMapper == null)
            {
                lock (LocalSyncRoot)
                {
                    if (_localMapper == null) // double-check    
                    {
                        InitMapper(0);
                    }
                }
            }
            return _localMapper;
        }

        /// <summary>    
        /// Init the 'default' SqlMapper defined by the SqlMap.Config file.    
        /// </summary>    
        public static void InitMapper(int type)
        {
            try
            {
                var handler = new ConfigureHandler(Configure);
                var builder = new DomSqlMapBuilder();

                var connection = ConfigurationManager.ConnectionStrings["DefaultConnection"];

                //if (connection == null)
                //    throw new ConfigurationErrorsException("缺少数据库连接配置（" + Config.DefaultConnectionName + "）");

                var properties = new NameValueCollection
                                     {
                                         {"ConnectionString",connection.ConnectionString}
                                     };

                builder.Properties = properties;
                if (type > 0)
                    _mapper = builder.ConfigureAndWatch("sqlmap.config", handler);
                else
                    _localMapper = builder.ConfigureAndWatch("sqlmap.config", handler);

            }
            catch
            {
                //Log.Error("sqlmap.config配置错误:" + e.Message);
                throw;
            }
        }


        public static void Configure(object obj)
        {
            _mapper = null;
            _localMapper = null;
        }

        /// <summary>    
        /// Get the instance of the SqlMapper defined by the SqlMap.Config file. (Convenience form of Instance method.)    
        /// </summary>    
        /// <returns>A SqlMapper initalized via the SqlMap.Config file.</returns>    
        public static ISqlMapper Get()
        {
            if (HttpContext.Current != null)
                return Instance();
            return LocalInstance();
        }   
    }
}
