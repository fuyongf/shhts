﻿using MongoDB.Driver.Builders;
using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.DataAccess.MongoRepository
{
    public class CacheStorageRepository :  ICacheDataAccess
    {
        const string CollectionName = "CacheBase";
        public bool Upset<T>(Entities.CacheBase<T> cache)
        {
            if (cache==null || string.IsNullOrWhiteSpace(cache.Id))
                return false;
            return MongoRepository.GetMongoRepository().Save(CollectionName, cache);
        }

        public bool Remove(string key)
        {
            return MongoRepository.GetMongoRepository().Delete(CollectionName, Query.EQ("_id", key));
        }

        public T Get<T>(string key) where T:new()
        {
            if (string.IsNullOrWhiteSpace(key))
                return default(T);
            return MongoRepository.GetMongoRepository().FindById<T>(CollectionName, key, null);
        }
    }
}
