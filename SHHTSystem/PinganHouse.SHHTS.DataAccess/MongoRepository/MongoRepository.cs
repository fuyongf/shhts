﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using PinganHouse.SHHTS.Core.IMongoRepository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;


namespace PinganHouse.SHHTS.DataAccess.MongoRepository
{
    public class MongoRepository : IRepository
    {
        readonly static MongoUrl url = new MongoUrl(ConfigurationManager.ConnectionStrings["mongoDB"].ConnectionString);
        readonly static MongoClient client = new MongoClient(url);
        readonly MongoDatabase database;
        private static MongoRepository instance = new MongoRepository();
        private MongoRepository()
        {
            var server = client.GetServer();
            this.database = server.GetDatabase(url.DatabaseName);
        }
        public static MongoRepository GetMongoRepository()
        {
            return instance;
        }


        public IEnumerable<T> FindAs<T>(IMongoQuery query) where T : class, new()
        {
            var col = database.GetCollection<T>(typeof(T).Name);
            return col.Find(query);
        }

        public MongoCursor<T> Find<T>(IMongoQuery query) where T : class, new()
        {
            var col = database.GetCollection<T>(typeof(T).Name);
            return col.Find(query);
        }

        public MongoCursor Find(string collectionName, IMongoQuery query)
        {
            var col = database.GetCollection(collectionName);
            return col.Find(query);
        }

        public T Single<T>(IMongoQuery query) where T : class, new()
        {
            var col = database.GetCollection<T>(typeof(T).Name);
            return col.FindOneAs<T>(query); ;
        }

        /// <summary>
        /// 根据id查找文档
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        public T FindById<T>(string id) where T :  new()
        {
            var col = database.GetCollection<T>(typeof(T).Name);
            return col.FindOneByIdAs<T>(id);
        }

        /// <summary>
        /// 查找文档并映射类型
        /// </summary>
        /// <typeparam name="T">需映射类型</typeparam>
        /// <param name="collectionName">表名</param>
        /// <param name="id">唯一标识</param>
        /// <param name="fields">数据库字段</param>
        /// <returns></returns>
        public T FindById<T>(string collectionName, string id, IMongoFields fields) where T :  new()
        {
            if (string.IsNullOrEmpty(collectionName))
                throw new ArgumentNullException("集合名不得为空");
            var col = database.GetCollection(collectionName);
            var args = new FindOneArgs { Query = Query.EQ("_id", id), Fields = fields };
            return col.FindOneAs<T>(args);
        }

        public MongoCursor<T> Find<T>(string collectionName, IMongoQuery query) where T :  new()
        {
            if (string.IsNullOrEmpty(collectionName))
                throw new ArgumentNullException("集合名不得为空");
            var col = database.GetCollection(collectionName);
            return col.FindAs<T>(query);
        }

        public IQueryable<T> All<T>() where T : class, new()
        {
            var col = database.GetCollection<T>(typeof(T).Name);
            var result = col.FindAllAs<T>();
            return result.AsQueryable();
        }

        public MongoCursor<T> All<T>(string collectionName) where T : class, new()
        {
            var col = database.GetCollection(collectionName);
            return col.FindAllAs<T>(); ;
        }

        public T FindAndModify<T>(FindAndModifyArgs args) where T:class, new()
        {
            return  this.FindAndModify<T>(typeof(T).Name, args);
        }

        public T FindAndModify<T>(string collectionName, FindAndModifyArgs args) where T:class, new()
        {
            var col = database.GetCollection(collectionName);
            var result = col.FindAndModify(args);
            return result.GetModifiedDocumentAs<T>();
        }

        public bool Update<T>(IMongoQuery query, IMongoUpdate update) where T : class, new()
        {
            MongoCollection col = database.GetCollection(typeof(T).Name);
            return this.Update<T>(query, update, UpdateFlags.None);
        }

        public bool Update<T>(IMongoQuery query, IMongoUpdate update, UpdateFlags flags) where T : class, new()
        {

            MongoCollection col = database.GetCollection(typeof(T).Name);
            ////定义更新文档
            //var update = new UpdateDocument { { "$set", new QueryDocument { { "Sex", "wowen" } } } };
            //执行更新操作
            var result = col.Update(query, update, flags);
            return result.UpdatedExisting;
        }

        public void InsertBatch<T>(IEnumerable<T> items) where T : class, new()
        {
            var options = new MongoInsertOptions();
            this.InsertBatch<T>(items, typeof(T).Name, options);
        }

        public void InsertBatch<T>(IEnumerable<T> items, string collectionName, MongoInsertOptions options) where T : class, new()
        {
            MongoCollection col = database.GetCollection(collectionName);
            col.InsertBatch<T>(items, options);
        }

        public bool Add<T>(T item, bool safeMode=false) where T : class, new()
        {
            MongoCollection col = database.GetCollection<T>(typeof(T).Name);
            MongoInsertOptions option = new MongoInsertOptions
            {
                WriteConcern = new WriteConcern { FSync = safeMode }
            };
            var result = col.Insert<T>(item, option);
            return result.Ok;
        }

        public bool Add<T>(string collectionName, T item) where T : class, new()
        {
            MongoCollection col = database.GetCollection<T>(collectionName);
            var result = col.Insert<T>(item);
            return result.Ok;
        }

        public bool  Save<T>(T item) where T : class, new()
        {
            MongoCollection col = database.GetCollection<T>(typeof(T).Name);
            var result = col.Save<T>(item);
            return result.Ok;
        }

        public bool Save<T>(string collectionName, T item) where T : class, new()
        {
            MongoCollection col = database.GetCollection<T>(collectionName);
            var result = col.Save<T>(item);
            return result.Ok;
        }

        public bool Delete<T>(string id) where T : class, new()
        {
            MongoCollection col = database.GetCollection(typeof(T).Name);
            //执行删除操作
            var result = col.Remove(Query.EQ("_id", id));
            return result.DocumentsAffected != 0;
        }

        public bool Delete<T>(IMongoQuery query) where T : class, new()
        {
            MongoCollection col = database.GetCollection(typeof(T).Name);
            //执行删除操作
            var result = col.Remove(query);
            return result.DocumentsAffected != 0;
        }

        public bool Delete(string collectionName, IMongoQuery query)
        {
            MongoCollection col = database.GetCollection(collectionName);
            //执行删除操作
            var result = col.Remove(query);
            return result.DocumentsAffected!=0;
        }

        /// <summary>
        /// Finds one matching document using the query and sortBy parameters and removes
        //  it from this collection.
        /// </summary>
        /// <param name="collectName"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public BsonDocument FindAndRemove(string collectName, FindAndRemoveArgs args)
        {
            MongoCollection col = database.GetCollection(collectName);
            var result = col.FindAndRemove(args);
            return result.ModifiedDocument;
        }


        public T FindAndRemove<T>(string collectName, FindAndRemoveArgs args) where T: class, new()
        {
            MongoCollection col = database.GetCollection(collectName);
            var result = col.FindAndRemove(args);
            return result.GetModifiedDocumentAs<T>();
        }

        //public void DeleteAll<T>() where T : class, new()
        //{
        //    MongoCollection col = database.GetCollection(typeof(T).Name);
        //    col.RemoveAll();
        //}
    }
}
