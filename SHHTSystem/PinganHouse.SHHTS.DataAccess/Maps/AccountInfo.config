﻿<sqlMap namespace="AccountInfo" xmlns="http://ibatis.apache.org/mapping" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >
  <alias>
    <typeAlias alias="Account" type="PinganHouse.SHHTS.Entities.AccountInfo,PinganHouse.SHHTS.Entities"/>
    <typeAlias alias="OperationLog" type="PinganHouse.SHHTS.Entities.AccountOperationLog,PinganHouse.SHHTS.Entities"/>
    <typeAlias alias="AccountLedger" type="PinganHouse.SHHTS.Entities.AccountLedgerInfo,PinganHouse.SHHTS.Entities"/>
    <typeAlias alias="CaseAccountTrade" type="PinganHouse.SHHTS.Entities.CaseAccountTradeInfo,PinganHouse.SHHTS.Entities"/>
  </alias>

  <parameterMaps>
    <parameterMap id="PagerParameter" class="System.Collections.IDictionary">
      <parameter property="Table" column="Table" />
      <parameter property="Fields" column="Fields" />
      <parameter property="Where" column="Where" />
      <parameter property="OrderBy" column="OrderBy" />
      <parameter property="CurrentPage" column="CurrentPage" />
      <parameter property="PageSize" column="PageSize" />
      <parameter property="GetCount" column="GetCount" />
      <parameter property="Count" column="Count" direction="Output" />
    </parameterMap>
  </parameterMaps>
  
  <resultMaps>
    <resultMap id="AccountResult" class="Account" >
      <result property="SysNo" column="SysNo" />

      <result property="SubjectId" column="SubjectId" />
      <result property="AccountType" column="AccountType" />
      <result property="AccountCategory" column="AccountCategory" />
      <result property="Balance" column="Balance" />
      <result property="PreEntryMoney" column="PreEntryMoney" />
      <result property="LockedMoney" column="LockedMoney" />
      <result property="FrozenMoney" column="FrozenMoney" />
      <result property="IsLocked" column="IsLocked" />
      <result property="Verson" column="Verson" />
      
      <result property="CreateUserSysNo" column="CreateUserSysNo" />
      <result property="CreateDate" column="CreateDate" />
      <result property="ModifyDate" column="ModifyDate" />
      <result property="ModifyUserSysNo" column="ModifyUserSysNo" />
      <result property="IsDeleted" column="IsDeleted" />
    </resultMap>

    <resultMap id="OperationLogResult" class="OperationLog" >
      <result property="SysNo" column="SysNo" />

      <result property="OperationType" column="OperationType" />
      <result property="AccountSysNo" column="AccountSysNo" />
      <result property="RefBizNo" column="RefBizNo" />
      <result property="Amount" column="Amount" />
      <result property="Remark" column="Remark" />

      <result property="CreateUserSysNo" column="CreateUserSysNo" />
      <result property="CreateDate" column="CreateDate" />
      <result property="ModifyDate" column="ModifyDate" />
      <result property="ModifyUserSysNo" column="ModifyUserSysNo" />
      <result property="IsDeleted" column="IsDeleted" />
    </resultMap>

    <resultMap id="AccountLedgerResult" class="AccountLedger" >
      <result property="CaseId" column="CaseId" />
      <result property="TenementContract" column="TenementContract" />
      <result property="TenementAddress" column="TenementAddress" />
      <result property="BuyerBalance" column="BuyerBalance" />
      <result property="SellerBalance" column="SellerBalance" />
    </resultMap>

    <resultMap id="CaseAccountTradeResult" class="CaseAccountTrade" >
      <result property="AccountType" column="AccountType" />
      <result property="OrderType" column="OrderType" />
      <result property="Amount" column="Amount" />
    </resultMap>
  </resultMaps>

  <statements>
    <sql id="Cell">
      [SysNo]
      ,[SubjectId]
      ,[AccountType]
      ,[AccountCategory]
      ,[Balance]
      ,[PreEntryMoney]
      ,[LockedMoney]
      ,[FrozenMoney]
      ,[IsLocked]
      ,[Verson]
      ,[CreateUserSysNo]
      ,[CreateDate]
      ,[ModifyDate]
      ,[ModifyUserSysNo]
      ,[IsDeleted]
    </sql>
    
    <sql id="LogCell">
      [OperationType]
      ,[AccountSysNo]
      ,[RefBizNo]
      ,[Amount]
      ,[Remark]
      ,[CreateUserSysNo]
      ,[CreateDate]
      ,[ModifyDate]
      ,[ModifyUserSysNo]
      ,[IsDeleted]
    </sql>
    
    <select id="Get" parameterClass="long"  resultMap="AccountResult">
      SELECT
      <include refid="Cell"/>
      FROM [AccountInfo] WITH(NOLOCK)
      WHERE [SysNo] = #value# AND [IsDeleted] = 0
    </select>

    <select id="GetLogs" parameterClass="long" resultMap="OperationLogResult">
      SELECT [SysNo],
      <include refid="LogCell"/>
      FROM [AccountOperationLog]
      WHERE [AccountSysNo] = #value# AND [IsDeleted] = 0 
      <isPropertyAvailable property="StartDate" prepend="AND">
        [CreateDate] >= #StartDate#
      </isPropertyAvailable>
      <isPropertyAvailable property="EndDate" prepend="AND">
        <![CDATA[[CreateDate] <= #EndDate#]]>
      </isPropertyAvailable>
    </select>

    <select id="GetLogsByTypeAndSubject" parameterClass="System.Collections.IDictionary" resultMap="OperationLogResult">
      SELECT [SysNo],
      <include refid="LogCell"/>
      FROM [AccountOperationLog]
      WHERE [AccountSysNo] = (SELECT [SysNo]  FROM [AccountInfo] WITH(NOLOCK) WHERE [SubjectId] = #SubjectId# AND [AccountType] = #AccountType# AND [IsDeleted] = 0)
      AND [IsDeleted] = 0 
      <isPropertyAvailable property="StartDate" prepend="AND">
        [CreateDate] >= #StartDate# 
      </isPropertyAvailable>
      <isPropertyAvailable property="EndDate" prepend="AND">
        <![CDATA[[CreateDate] <= #EndDate#]]>
      </isPropertyAvailable>
    </select>

    <select id="GetByTypeAndSubject" parameterClass="System.Collections.IDictionary"  resultMap="AccountResult">
      SELECT
      <include refid="Cell"/>
      FROM [AccountInfo] WITH(NOLOCK)
      WHERE [SubjectId] = #SubjectId# AND [AccountType] = #AccountType# AND [IsDeleted] = 0
    </select>

    <select id="GetBalance" parameterClass="System.Collections.IDictionary"  resultClass="decimal">
      SELECT
      [Balance]
      FROM [AccountInfo] WITH(NOLOCK)
      WHERE [SubjectId] = #SubjectId# AND [AccountType] = #AccountType# AND [IsDeleted] = 0
    </select>

    <select id="GetCaseTradeAmount" parameterClass="string"  resultMap="CaseAccountTradeResult">
      SELECT [AccountType],[OrderType],SUM([Amount]) AS [Amount] FROM
      (
      SELECT A.[AccountType],O.[OrderType],T.[Amount] FROM [AccountInfo] A
      JOIN [OrderInfo] O ON A.[SysNo] = O.[AccountSysNo]
      JOIN [TradeInfo] T ON O.[OrderNo] = T.[OrderNo]
      WHERE A.[SubjectId] = #SubjectId# AND (A.[AccountType] = 0 OR A.[AccountType] = 1)
      AND T.[Status] = 3
      AND ((O.[OrderType] = 0 AND (O.[OrderStatus] = 0 OR O.[OrderStatus] = 3)) OR (O.[OrderType] = 1 AND O.[OrderStatus] = 6))
      ) T GROUP BY [AccountType],[OrderType]
    </select>

    <insert id="Insert" parameterClass="Account">
      INSERT INTO [AccountInfo]
      (<include refid="Cell"/>)
      VALUES (
      #SysNo#,
      #SubjectId#,
      #AccountType#,
      #AccountCategory#,
      #Balance#,
      #PreEntryMoney#,
      #LockedMoney#,
      #FrozenMoney#,
      #IsLocked#,
      0,
      #CreateUserSysNo#,
      GETDATE(),
      NULL,
      NULL,
      0
      )
    </insert>

    <insert id="InsertLog" parameterClass="OperationLog">
      INSERT INTO [AccountOperationLog]
      (<include refid="LogCell"/>)
      VALUES (
      #OperationType#,
      #AccountSysNo#,
      #RefBizNo#,
      #Amount#,
      #Remark#,
      #CreateUserSysNo#,
      GETDATE(),
      NULL,
      NULL,
      0
      )
    </insert>

    <update id="Update" parameterClass="Account">
      UPDATE [AccountInfo] SET [ModifyUserSysNo] = #ModifyUserSysNo#,[ModifyDate] = GETDATE(),
      [Balance] = #Balance#,
      [PreEntryMoney] = #PreEntryMoney#,
      [LockedMoney] = #LockedMoney#,
      [FrozenMoney] = #FrozenMoney#,
      [IsLocked] = #IsLocked#,
      [Verson] = #Verson# + 1
      WHERE [SysNo] = #SysNo# AND [Verson] = #Verson#
    </update>

    <delete id="Delete" parameterClass="System.Collections.IDictionary">
      UPDATE [AccountInfo] SET [ModifyDate] = GETDATE(),[ModifyUserSysNo] = #OperatorSysNo#,[IsDeleted] = 1
      WHERE [SysNo] = #SysNo#
    </delete>

    <delete id="Remove" parameterClass="long">
      DELETE FROM [Account] WHERE [SysNo] = #value#
    </delete>

    <!--分页-->
    <procedure id="GetPaginatedList" parameterMap="PagerParameter" resultMap="AccountLedgerResult">
      Pager
    </procedure>

  </statements>
</sqlMap>
