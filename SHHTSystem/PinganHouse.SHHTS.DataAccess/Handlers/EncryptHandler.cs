﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IBatisNet.DataMapper.TypeHandlers;

using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.DataAccess.Handlers
{
    public class EncryptHandler : ITypeHandlerCallback
    {
        public object GetResult(IResultGetter getter)
        {
            if (getter != null && getter.Value != null)
                return EncryptHelper.Decrypt(getter.Value.ToString());
            return null;
        }

        public object NullValue
        {
            get
            {
                return null;
            }
        }

        public void SetParameter(IParameterSetter setter, object parameter)
        {
            if (parameter == null)
            {
                setter.Value = System.DBNull.Value;
            }
            else
            {
                setter.Value = EncryptHelper.Encrypt(parameter.ToString());
            }
        }

        public object ValueOf(string s)
        {
            if (s != null && s.Length > 0)
                return EncryptHelper.Encrypt(s);
            return s;
        }
    }
}
