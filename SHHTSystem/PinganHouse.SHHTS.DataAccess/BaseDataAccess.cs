﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Utils.Log;

namespace PinganHouse.SHHTS.DataAccess
{
    public abstract class BaseDataAccess<T> : BaseSqlMapDao<T>, IDataAccess<T> where T : BaseEntity
    {
        private string m_MapNamespace;
        private string m_StatementPrefix = string.Empty;

        public BaseDataAccess() { }

        public BaseDataAccess(string mapNamespace, string statementPrefix)
        {
            m_MapNamespace = mapNamespace;
            m_StatementPrefix = statementPrefix;
        }

        public virtual T Get(long sysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForObject(string.Concat(StatementPrefix, "Get"), sysNo);
                Log.Dao(string.Format("Statement : {0}Get({1}),{2}", StatementPrefix, sysNo, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}GET({1}),Exception:{2}", StatementPrefix, sysNo, e));
                throw;
            }
        }

        public virtual bool Insert(T entity)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                if (entity == null)
                    return false;
                ExecuteInsert(string.Concat(StatementPrefix, "Insert"), entity);
                Log.Dao(string.Format("Dao -> Statement:{0}Insert({1}),{2}", StatementPrefix, entity.SysNo, DateTime.Now.Ticks - ts));
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}Insert({1}),Exception:{2}", StatementPrefix, entity.SysNo, e));
                throw;
            }
            return true;
        }

        public virtual bool Delete(long sysNo, long operatorSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteDelete(string.Concat(StatementPrefix, "Delete"), new Dictionary<string, object> { { "SysNo", sysNo }, { "OperatorSysNo", operatorSysNo } });
                Log.Dao(string.Format("Dao -> Statement:{0}Delete({1}),{2}", StatementPrefix, sysNo, DateTime.Now.Ticks - ts));
                return result > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}Delete({1}),Exception:{2}", StatementPrefix, sysNo, e));
                throw;
            }
        }

        public virtual bool Update(T entity)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteUpdate(string.Concat(StatementPrefix, "Update"), entity);
                Log.Dao(string.Format("Dao -> Statement:{0}Update({1}),Exception:{2}", StatementPrefix, entity.SysNo, DateTime.Now.Ticks - ts));
                return result > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}Update({1}),Exception:{2}", StatementPrefix, entity.SysNo, e));
                throw;
            }
        }

        public virtual bool Remove(long sysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteDelete(string.Concat(StatementPrefix, "Remove"), sysNo);
                Log.Dao(string.Format("Dao -> Statement:{0}Remove({1}),{2}", StatementPrefix, sysNo, DateTime.Now.Ticks - ts));
                return result > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}Remove({1}),Exception:{2}", StatementPrefix, sysNo, e));
                throw;
            }
        }

        public virtual bool Exists(long sysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForAnyObject(string.Concat(StatementPrefix, "Exists"), sysNo);
                Log.Dao(string.Format("Dao -> Statement:{0}Exists({1}),{2}", StatementPrefix, sysNo, DateTime.Now.Ticks - ts));
                return result != null;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}Exists({1}),Exception:{2}", StatementPrefix, sysNo, e));
                throw;
            }
        }

        protected string StatementPrefix
        {
            get
            {
                if (!string.IsNullOrEmpty(m_MapNamespace))
                    return string.Concat(m_MapNamespace, ".", string.IsNullOrEmpty(m_StatementPrefix) ? "" : string.Concat(m_StatementPrefix, "_"));
                return string.Concat(typeof(T).Name, ".");
            }
        }
    }
}
