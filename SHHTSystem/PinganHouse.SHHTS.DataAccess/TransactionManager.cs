﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.DataAccess
{
    public class TransactionManager
    {
        private static TransactionManager _instance;
        private static object _syncObject = new object();

        public static TransactionManager Instance 
        {
            get 
            {
                if (_instance == null)
                {
                    lock (_syncObject) 
                    {
                        if (_instance == null) 
                        {
                            _instance = new TransactionManager();
                        }
                    }
                }
                return _instance;
            }
        }

        /// <summary>
        /// 当前上下文事务是否已经开启
        /// </summary>
        public bool IsTransactionStarted 
        {
            get 
            {
                var sqlMapper = ProviderBase.Get();
                return sqlMapper.IsSessionStarted && sqlMapper.LocalSession.IsTransactionStart;
            }
        }

        /// <summary>
        /// 开始事务
        /// </summary>
        public void BeginTransaction() 
        {
            ProviderBase.Get().BeginTransaction();
        }
        /// <summary>
        /// 开始事务
        /// </summary>
        /// <param name="openConnection">是否同时开启数据库连接，默认:true</param>
        public void BeginTransaction(bool openConnection)
        {
            ProviderBase.Get().BeginTransaction(openConnection);
        }

        /// <summary>
        /// 提交事务
        /// </summary>
        public void CommitTransaction() 
        {
            ProviderBase.Get().CommitTransaction();
        }

        /// <summary>
        /// 提交事务
        /// </summary>
        /// <param name="closeConnection">是否关闭当前数据库连接，默认:false</param>
        public void CommitTransaction(bool closeConnection)
        {
            ProviderBase.Get().CommitTransaction(closeConnection);
        }

        /// <summary>
        /// 回滚事务
        /// </summary>
        public void RollBackTransaction() 
        {
            ProviderBase.Get().RollBackTransaction();
        }

        /// <summary>
        /// 回滚事务
        /// </summary>
        /// <param name="closeConnection">是否关闭当前数据库连接，默认:false</param>
        public void RollBackTransaction(bool closeConnection)
        {
            ProviderBase.Get().RollBackTransaction(closeConnection);
        }
    }
}
