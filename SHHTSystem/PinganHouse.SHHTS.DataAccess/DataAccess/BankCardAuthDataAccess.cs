﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.DataAccess
{
    public class BankCardAuthDataAccess : BaseDataAccess<BankCardAuthInfo>, IBankCardAuthDataAccess
    {
        public bool Exists(string caseId, string cardNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForAnyObject("BankCardAuthInfo.ExistsByCaseAndCard", new Dictionary<string, object> { { "CaseId", caseId }, { "CardNo", cardNo } });
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "BankCardAuthInfo.ExistsByCaseAndCard", cardNo, DateTime.Now.Ticks - ts));
                return result != null;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "BankCardAuthInfo.ExistsByCaseAndCard", cardNo, e));
                throw;
            }
        }

        public IList<BankCardAuthInfo> GetByCardNo(string cardNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForList("BankCardAuthInfo.GetByCardNo", cardNo);
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "BankCardAuthInfo.GetByCardNo", cardNo, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "BankCardAuthInfo.GetByCardNo", cardNo, e));
                throw;
            }
        }

        public IList<BankCardAuthInfo> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, string caseId, string accountNo, string accountName, string identityNo, string mobile)
        {
            var ts = DateTime.Now.Ticks;
            StringBuilder whereStr = new StringBuilder("[IsDeleted] = 0");
            try
            {
                if (!string.IsNullOrWhiteSpace(caseId))
                {
                    whereStr.AppendFormat(" AND [CaseId] = '{0}'", caseId);
                }
                if (!string.IsNullOrWhiteSpace(accountNo))
                {
                    whereStr.AppendFormat(" AND [CardNo] = '{0}'", accountNo);
                }
                if (!string.IsNullOrWhiteSpace(accountName))
                {
                    whereStr.AppendFormat(" AND [AccountName] = '{0}'", accountName);
                }
                if (!string.IsNullOrWhiteSpace(identityNo))
                {
                    whereStr.AppendFormat(" AND [IdentityNo] = '{0}'", identityNo);
                }
                if (!string.IsNullOrWhiteSpace(mobile))
                {
                    whereStr.AppendFormat(" AND [Mobile] = '{0}'", mobile);
                }

                var result = ExecuteQueryForPaginatedList("[BankCardAuthInfo]", "*",
                    whereStr.ToString(), "[CreateDate] DESC", pageIndex, pageSize, out totalCount);
                Log.Dao(string.Format("BankCardAuthInfo.GetPaginatedList({0}),{1}", whereStr, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("BankCardAuthInfo.GetPaginatedList({0}),{1}", whereStr, e));
                throw;
            }
        }

        public IList<BankCardAuthInfo> GetByCase(string caseId)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForList("BankCardAuthInfo.GetByCase", caseId);
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "BankCardAuthInfo.GetByCase", caseId, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "BankCardAuthInfo.GetByCase", caseId, e));
                throw;
            }
        }

        public BankCardAuthInfo GetByCardInCase(string caseId, string cardNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForObject("BankCardAuthInfo.GetByCardInCase", new Dictionary<string, object> { { "CaseId", caseId }, { "CardNo", cardNo } });
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "BankCardAuthInfo.GetByCardInCase", cardNo, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "BankCardAuthInfo.GetByCardInCase", cardNo, e));
                throw;
            }
        }
    }
}
