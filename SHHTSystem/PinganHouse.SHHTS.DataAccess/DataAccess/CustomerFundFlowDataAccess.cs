﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.DataAccess
{
    public class CustomerFundFlowDataAccess : BaseDataAccess<CustomerFundFlowInfo>, ICustomerFundFlowDataAccess
    {
        public IEnumerable<CustomerFundFlowInfo> Get(string caseId, CustomerType? customerType = null)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var parameters = new Dictionary<string, object> { { "CaseId", caseId }};
                if(customerType != null)
                    parameters.Add("CustomerType",customerType);
                var result = ExecuteQueryForList("CustomerFundFlowInfo.GetByCaseId", parameters);
                Log.Dao(string.Format("Statement : {0}.GetByCaseId({1}),{2}", "CustomerFundFlowInfo", caseId, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}.GetByCaseId({1}),Exception:{2}", "CustomerFundFlowInfo", caseId, e));
                throw;
            }
        }
    }
}
