﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.DataAccess
{
    public class MissCollectionTradeDataAccess : BaseDataAccess<MissCollectionTradeInfo>, IMissCollectionTradeDataAccess
    {
        public MissCollectionTradeInfo GetBySerialNo(string serialNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForObject("MissCollectionTradeInfo.GetBySerialNo", serialNo);
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "MissCollectionTradeInfo.GetBySerialNo", serialNo, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "MissCollectionTradeInfo.GetBySerialNo", serialNo, e));
                throw;
            }
        }

        public bool Exists(PaymentChannel channel, string serialNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForAnyObject("MissCollectionTradeInfo.ExistsBySerialNo", new Dictionary<string, object> { { "Channel", channel }, { "SerialNo", serialNo } });
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "MissCollectionTradeInfo.ExistsBySerialNo", serialNo, DateTime.Now.Ticks - ts));
                return result != null;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "MissCollectionTradeInfo.ExistsBySerialNo", serialNo, e));
                throw;
            }
        }

        public IList<MissCollectionTradeInfo> Get(string accountNo, PaymentChannel? channel, bool? confirmed, DateTime? startDate, DateTime? endDate)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var parameters = new Dictionary<string, object>();
                if (!string.IsNullOrEmpty(accountNo))
                    parameters.Add("BankAccountNo", accountNo);
                if (channel != null)
                    parameters.Add("PaymentChannel", channel);
                if (confirmed != null)
                    parameters.Add("Confirmed", confirmed);
                if (startDate != null)
                    parameters.Add("StartDate", startDate);
                if (endDate != null)
                    parameters.Add("EndDate", endDate);

                var result = ExecuteQueryForList("MissCollectionTradeInfo.GetAll", parameters);
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "MissCollectionTradeInfo.GetAll", "", DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "MissCollectionTradeInfo.GetAll", "", e));
                throw;
            }
        }

        public bool Confirm(long sysNo, string tradeNo, long auditorSysNo)
        {
            return ExecuteUpdate("MissCollectionTradeInfo.Confirm", new Dictionary<string, object> { { "SysNo", sysNo }, { "TradeNo", tradeNo }, { "AuditorSysNo", auditorSysNo } }) > 0;
        }
    }
}
