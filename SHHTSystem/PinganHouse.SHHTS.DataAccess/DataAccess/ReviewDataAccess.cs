﻿using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.DataAccess
{
    public class ReviewDataAccess : BaseDataAccess<ReviewInfo>, IReviewDataAccess
    {
        public override bool Insert(ReviewInfo entity)
        {
            entity.SysNo = Sequence.Get();
            return base.Insert(entity);
        }

    }
}
