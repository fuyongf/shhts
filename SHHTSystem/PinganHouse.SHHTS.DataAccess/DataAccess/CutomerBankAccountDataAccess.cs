﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.DataAccess
{
    public class CustomerBankAccountDataAccess : BaseDataAccess<CustomerBankAccountInfo>, ICustomerBankAccountDataAccess
    {
        public IList<CustomerBankAccountInfo> GetByCase(string caseId, CustomerType? customerType = null, BankAccountPurpose? accountType = null)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var parameters = new Dictionary<string, object> { { "CaseId", caseId } };
                if (customerType != null)
                    parameters.Add("CustomerType", customerType);
                if (accountType != null)
                    parameters.Add("AccountType", accountType);
                var result = ExecuteQueryForList("CustomerBankAccountInfo.GetByCaseId", parameters);
                Log.Dao(string.Format("Statement : {0}.GetByCaseId({1}),{2}", "CustomerBankAccountInfo", caseId, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}.GetByCaseId({1}),Exception:{2}", "CustomerBankAccountInfo", caseId, e));
                throw;
            }
        }

        public IList<CustomerBankAccountInfo> GetByCase(long caseSysNo, CustomerType? customerType = null, BankAccountPurpose? accountType = null)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var parameters = new Dictionary<string, object> { { "CaseSysNo", caseSysNo } };
                if (customerType != null)
                    parameters.Add("CustomerType", customerType);
                if (accountType != null)
                    parameters.Add("AccountType", accountType);
                var result = ExecuteQueryForList("CustomerBankAccountInfo.GetByCaseSysNo", parameters);
                Log.Dao(string.Format("Statement : {0}.GetByCaseSysNo({1}),{2}", "CustomerBankAccountInfo", caseSysNo, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}.GetByCaseSysNo({1}),Exception:{2}", "CustomerBankAccountInfo", caseSysNo, e));
                throw;
            }
        }

        public bool BatchInsert(IEnumerable<CustomerBankAccountInfo> entities)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                if (entities == null || entities.Count() == 0)
                    return false;
                TransactionManager.Instance.BeginTransaction();

                ExecuteDelete("CustomerBankAccountInfo.RemoveByCase", entities.ElementAt(0).CaseId);

                foreach (var entity in entities)
                {
                    ExecuteInsert("CustomerBankAccountInfo.Insert", entity);
                }
                TransactionManager.Instance.CommitTransaction();
                Log.Dao(string.Format("Dao -> Statement:{0}.Insert({1}),{2}", "CustomerBankAccountInfo", entities.Count(), DateTime.Now.Ticks - ts));
            }
            catch (Exception e)
            {
                if (TransactionManager.Instance.IsTransactionStarted)
                    TransactionManager.Instance.RollBackTransaction();
                Log.Error(string.Format("Dao -> Statement:{0}.Insert({1}),Exception:{2}", "CustomerBankAccountInfo", entities.Count(), e));
                throw;
            }
            return true;
        }
    }
}
