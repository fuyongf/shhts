﻿using IBatisNet.DataMapper;
using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.DataTransferObjects.Filters;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.DataAccess
{
    public class CaseDataAccess : BaseDataAccess<Case>, ICaseDataAccess
    {
        public Case GetCaseByCaseId(string caseId, ReceptionCenter? receptionCenter)
        {
            var dic = new Dictionary<string, object> { 
                {"CaseId", caseId}
            };
            if (receptionCenter != null && receptionCenter != ReceptionCenter.Unkonw)
                dic["ReceptionCenter"] = (int)receptionCenter;
            return ExecuteQueryForObject("Case.GetCaseByCaseId", dic);
        }

        public Case GetCaseByCaseIdAndCaseStatus(string caseId, CaseStatus caseStatus, bool? finished)
        {
            var dic = new Dictionary<string, object> { { "CaseId", caseId }, { "EventType", (long)caseStatus } };
            if (finished != null)
                dic["Finished"] = finished.Value;
            return ExecuteQueryForObject("Case.GetCaseByCaseIdAndCaseStatus", dic);
        }

        /// <summary>
        /// 添加案件及案件关联用户
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public override bool Insert(Case entity)
        {
            entity.SysNo = Sequence.Get();
            var sqlMap = GetLocalSqlMap();

            //当前状态
            var caseEvent = new CaseEventInfo
            {
                CaseId = entity.CaseId,
                CreateDate = DateTime.Now,
                CreateUserSysNo = entity.CreateUserSysNo,
                EventType = CaseStatus.YJ1,
                SourceStatus = CaseStatus.Unkown,
                OperatorSysNo = entity.CreateUserSysNo.Value,
                SysNo = Sequence.Get()
            };
            //案件关联账号
            var account = new AccountInfo
            {
                SubjectId = entity.CaseId,
                AccountType = AccountType.Buyer,
                CreateDate = DateTime.Now,
                CreateUserSysNo = entity.CreateUserSysNo,
                SysNo = Sequence.Get(),
            };
            //卖家
            var seller = new SellerInfo
            {
                SysNo = Sequence.Get(),
                CaseId = entity.CaseId,
                CreateDate = DateTime.Now,
                CreateUserSysNo = entity.CreateUserSysNo
            };
            //买家
            var buyer = new BuyerInfo
            {
                SysNo = Sequence.Get(),
                CaseId = entity.CaseId,
                CreateDate = DateTime.Now,
                CreateUserSysNo = entity.CreateUserSysNo
            };
            if (!TransactionManager.Instance.IsTransactionStarted)
                TransactionManager.Instance.BeginTransaction();
            try
            {
                ExecuteInsert("Case.Insert", entity);
                ExecuteInsert("CaseEventInfo.Insert", caseEvent);
                ExecuteInsert("AccountInfo.Insert", account);
                account.SysNo = Sequence.Get();
                account.AccountType = AccountType.Seller;
                ExecuteInsert("AccountInfo.Insert", account);
                ExecuteInsert("BuyerInfo.Insert", buyer);
                ExecuteInsert("SellerInfo.Insert", seller);
                InsertOrUpdatePropertyCertificate(entity.TenementContract, entity.TenementAddress, entity.Sellers, entity.CreateUserSysNo);
                InsertCaseUser(entity.CaseId, CaseUserType.Buyer, entity.Buyers.ToArray());
                InsertCaseUser(entity.CaseId, CaseUserType.Seller, entity.Sellers.ToArray());
                InsertCaseUser(entity.CaseId, CaseUserType.AgentStaff, entity.AgencySysNo);
                //InsertCaseUser(entity.CaseId, CaseUserType.Handler, entity.OperatorSysNo);
                if (TransactionManager.Instance.IsTransactionStarted)
                    TransactionManager.Instance.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                if (TransactionManager.Instance.IsTransactionStarted)
                    TransactionManager.Instance.RollBackTransaction();
                Log.Error(ex);
                throw;
            }
        }

        private void InsertOrUpdatePropertyCertificate(string tenementContract, string tenementAddress, List<long> sellers, long? userSysNo)
        {
            //产证
            var propertyCertificate = new PropertyCertificate
            {
                TenementContract = tenementContract,
                TenementAddress = tenementAddress,
                Proprietors = sellers,
            };
            if (!ServiceDepository.PropertyCertificateDataAccess.Exist(tenementContract))
            {
                propertyCertificate.SysNo = Sequence.Get();
                propertyCertificate.CreateDate = DateTime.Now;
                propertyCertificate.CreateUserSysNo = userSysNo;
                ExecuteInsert("PropertyCertificate.Insert", propertyCertificate);
            }
            else
            {
                propertyCertificate.ModifyDate = DateTime.Now;
                propertyCertificate.ModifyUserSysNo = userSysNo;
                ExecuteUpdate("PropertyCertificate.UpdateByTenementContract", propertyCertificate);
            }
        }

        public bool TransactionInsertCaseUser(string caseId, CaseUserType caseUserType, params long[] userSysNos)
        {
            try
            {
                if (!TransactionManager.Instance.IsTransactionStarted)
                    TransactionManager.Instance.BeginTransaction();
                InsertCaseUser(caseId, caseUserType, userSysNos);
                if (TransactionManager.Instance.IsTransactionStarted)
                    TransactionManager.Instance.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                if (TransactionManager.Instance.IsTransactionStarted)
                    TransactionManager.Instance.RollBackTransaction();
                Log.Error(ex);
                throw;
            }
        }

        private void InsertCaseUser(string caseId, CaseUserType caseUserType, params long[] userSysNos)
        {
            if (userSysNos == null || userSysNos.Length == 0)
                return;
            var du = userSysNos.Distinct();
            foreach (var us in du)
            {
                long sysno = Sequence.Get();
                var dic = new Dictionary<string, object> 
                    {
                        {"SysNo", sysno}, {"CaseId", caseId}, {"UserSysNo", us}, {"CaseUserType", caseUserType}
                    };
                ExecuteInsert("Case.InsertCaseUser", dic);
            }
        }

        public bool DeleteCaseByCaseId(string caseId, string modifyUserNo)
        {
            //var sqlMap = GetLocalSqlMap();
            //sqlMap.BeginTransaction();
            try
            {
                ServiceDepository.TransactionService.Begin();
                var dic = new Dictionary<string, string> { { "CaseId", caseId }, { "ModifyUserSysNo", modifyUserNo } };
                ExecuteUpdate("Case.DeleteByCaseId", dic);
                ExecuteUpdate("Case.DeleteReltionByCaseId", dic);
                ServiceDepository.AttachmentDataAccess.DeleteByAssociationNo(caseId, modifyUserNo);
                ServiceDepository.TransactionService.Commit();
                return true;
            }
            catch (Exception ex)
            {
                ServiceDepository.TransactionService.RollBack();
                //sqlMap.RollBackTransaction();
                Log.Error(ex);
                throw;
            }
        }

        public bool ExistByCaseId(string caseId)
        {
            var result = ExecuteQueryForAnyObject("Case.ExistByCaseId", caseId);
            return result != null && result.ToString().CompareTo("0") != 0;
        }

        public IList<CaseUserRelation> GetRelations(params string[] caseId)
        {
            return ExecuteQueryForList<CaseUserRelation>("Case.GetRelations", string.Join(",", caseId));
        }

        public IList<CaseUserRelation> GetRelation(string caseId)
        {
            return ExecuteQueryForList<CaseUserRelation>("Case.GetRelation", caseId);
        }

        public IList<CaseUserRelation> GetRelation(string caseId, int userType)
        {
            return ExecuteQueryForList<CaseUserRelation>("Case.GetRelationByCaseIdAndCaseUserType", new Dictionary<string, object> { { "CaseId", caseId }, { "CaseUserType", userType } });
        }

        public CaseUserRelation GetRelation(long userSysNo)
        {
            return ExecuteQueryForAnyObject("Case.GetRelationByUserSysNo", userSysNo) as CaseUserRelation;
        }

        public IList<CaseUserRelation> GetRelationByCustomerSysNos(params long[] userSysNo)
        {
            if (userSysNo == null || userSysNo.Count() == 0)
                return null;
            return ExecuteQueryForList<CaseUserRelation>("Case.GetRelationByUserSysNos", string.Join(",", userSysNo));
        }

        [Obsolete]
        public IList<Case> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, ReceptionCenter receptionCenter, CaseStatus allCaseStatus, long? belongUserSysNo, CaseStatus? topStatus, string caseId,
          DateTime? startDate, DateTime? endDate, IDictionary<string, object> data, CaseStatus? caseStatus, bool? finished)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                StringBuilder whereStr = new StringBuilder("C.IsDeleted=0 and E.IsDeleted=0 ");
                whereStr.AppendFormat(" AND ([ReceptionCenter]={0} OR GlobalFlag=1) ", (uint)receptionCenter);
                string table = " [Case] C JOIN [CaseEvent] E ON C.CaseId = E.CaseId ";
                string withQuery = string.Empty;
                if (finished != null)
                    whereStr.AppendFormat(" and E.Finished={0} ", finished.Value ? 1 : 0);
                var status = (long)allCaseStatus;
                string ceQuery = string.Format(" EventType & convert(bigint,{0}) !=0", status);

                //获取所属人案件 暂作废，目前存在这样一情况，同一页面：某操作需要其他人操作，比如新建报道，需要在同页面审批。
                string ueQuery = string.Empty;
                if (belongUserSysNo != null)
                    ueQuery = string.Format(" and e.CreateUserSysNo={0} ", belongUserSysNo.Value);
                //页面加载，获取当前个人及特定状态 暂业务矛盾不启用
                if (topStatus != null && belongUserSysNo != null && caseStatus == null && string.IsNullOrWhiteSpace(caseId) && (data == null || data.Count == 0) && endDate == null && startDate == null)
                {
                    ueQuery = string.Format(" and (e.CreateUserSysNo={0} OR (EventType & convert(bigint,{1}) !=0 AND  Finished = 0)) ", belongUserSysNo.Value, (long)topStatus.Value);
                }
                else
                {
                    if (caseStatus != null)
                        ceQuery = string.Format("  EventType & convert(bigint,{0}) !=0 AND (Finished = 0 OR (SELECT TOP 1 SysNo FROM CaseEvent WHERE CaseId = C.CaseId AND  EventType & convert(bigint,{1}) !=0 ORDER BY CreateDate DESC) = SysNo) ", (long)caseStatus, status);
                    if (!string.IsNullOrWhiteSpace(caseId))
                    {
                        whereStr.AppendFormat("  AND {0}='{1}'", " C.caseId ", caseId);
                    }
                    else if (data != null && data.Count > 0)
                    {
                        if (data.ContainsKey("tenementAddress"))
                        {
                            whereStr.AppendFormat(" AND {0} like '%{1}%'", "TenementAddress", data["tenementAddress"]);
                        }
                        if (data.ContainsKey("creatorName"))
                        {
                            whereStr.AppendFormat(" AND C.CreateUserSysNo IN (select sysno from UserInfo  where RealName like '{0}%')", data["creatorName"]);
                        }
                        if (data.ContainsKey("operatorName") && topStatus != null)
                        {
                            whereStr.AppendFormat(" AND E.CaseId IN (select ce.caseId from UserInfo u join CaseEvent ce on ce.CreateUserSysNo=u.sysno  where  RealName like '{0}%' and (EventType & convert(bigint,{1}) !=0 )) ", data["operatorName"], (long)topStatus.Value);
                        }
                        if (data.ContainsKey("buyerName") || data.ContainsKey("agencyName") || data.ContainsKey("sellerName"))
                        {
                            StringBuilder query = new StringBuilder();
                            if (data.ContainsKey("buyerName"))
                                query.AppendFormat(" select Distinct r.caseId from CaseUserRelation r join customer c on r.UserSysNo = c.SysNo join UserInfo u on c.UserSysNo=u.SysNo where u.RealName like '{0}%' and r.CaseUserType=1 and r.IsDeleted=0  ", data["buyerName"]);
                            if (data.ContainsKey("agencyName"))
                                query.AppendFormat("INTERSECT select Distinct r.caseId from CaseUserRelation r  join agentstaff s on r.UserSysNo = s.SysNo join UserInfo u on s.UserSysNo=u.SysNo where u.RealName like '{0}%' and r.CaseUserType=2 and r.IsDeleted=0  ", data["agencyName"]);
                            if (data.ContainsKey("sellerName"))
                                query.AppendFormat("INTERSECT select Distinct r.caseId from CaseUserRelation r  join customer c on r.UserSysNo = c.SysNo join UserInfo u on c.UserSysNo=u.SysNo where u.RealName like '{0}%' and r.CaseUserType=0 and r.IsDeleted=0  ", data["sellerName"]);

                            //query.Append(" INTERSECT ");
                            //买家

                            withQuery = string.Format("with caseIds (caseId) as ( {0} )", query.ToString().TrimStart("INTERSECT"));
                            table = string.Format("{0} join caseIds ids on ids.caseid=C.caseid ", table);
                        }

                    }
                    if (startDate != null && startDate.Value != default(DateTime))
                        whereStr.AppendFormat(" AND C.[CreateDate]>='{0}' ", startDate.Value.Date);
                    if (endDate != null && endDate.Value != default(DateTime))
                        whereStr.AppendFormat(" AND C.[CreateDate]<'{0}' ", endDate.Value.AddDays(1).Date);
                }
                string columns = " C.SysNo, C.CaseId, C.Disabled, C.CompanySysNo,C.Remark,C.TenementAddress,C.TenementContract,C.ReceptionCenter,C.SOURCETYPE, C.IsFund,C.FundTrusteeshipContract,C.IsCommission,CommissionTrusteeshipContract,C.CreateDate,C.CreateUserSysNo, C.ModifyUserSysNo, E.EventType as CaseStatus,  ";
                columns += "  CASE WHEN E.ModifyDate IS NULL THEN E.CreateDate ELSE E.ModifyDate END AS ModifyDate ";
                whereStr.AppendFormat(" and  E.SysNo = (SELECT TOP 1 SysNo FROM CaseEvent WHERE CaseId = C.CaseId  and {0} {1} ORDER BY CreateDate DESC ) ", ceQuery, ueQuery);
                return ExecuteQueryForPaginatedList(table, columns, whereStr.ToString(), "C.CreateDate  desc ", withQuery, pageIndex, pageSize, out totalCount);
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Case.GetPaginatedData {0}", e));
                throw;
            }
        }

        /// <summary>
        /// Gets the paginated list.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="totalCount">The total count.</param>
        /// <returns></returns>
        public IList<Case> GetPaginatedList(CaseFilter filter, int pageIndex, int pageSize, out int totalCount)
        {
            IList<Case> list = new List<Case>();
            totalCount = 0;

            /*
            SELECT  c.SysNo, c.CaseId, c.Disabled, c.CompanySysNo,c.Remark,c.TenementAddress,
            c.TenementContract,c.ReceptionCenter,c.SourceType, c.IsFund,c.FundTrusteeshipContract,
            c.IsCommission,c.CommissionTrusteeshipContract,c.CreateDate,c.CreateUserSysNo, c.ModifyUserSysNo,
            e.EventType as CaseStatus,e.Finished,CASE WHEN e.ModifyDate IS NULL THEN e.CreateDate ELSE e.ModifyDate END AS ModifyDate
            FROM [Case] c INNER JOIN [CaseEvent] e ON c.CaseId=e.CaseId
            */
            string table = " [Case] c JOIN [CaseEvent] e ON c.CaseId = e.CaseId ";
            string columns = "c.SysNo, c.CaseId, c.Disabled, c.CompanySysNo,c.Remark,c.TenementAddress,"
            + "c.TenementContract,c.ReceptionCenter,c.SourceType, c.IsFund,c.FundTrusteeshipContract,"
            + "c.IsCommission,c.CommissionTrusteeshipContract,c.CreateDate,c.CreateUserSysNo, c.ModifyUserSysNo,"
            + "e.EventType as CaseStatus,e.Finished,CASE WHEN e.ModifyDate IS NULL THEN e.CreateDate ELSE e.ModifyDate END AS ModifyDate";
            string where = string.Empty;
            IList<string> wherecs = new List<string>();

            try
            {
                if (filter != null)
                {
                    if (filter.IsDeleted.HasValue)
                    {
                        wherecs.Add(string.Format("c.IsDeleted={0}", filter.IsDeleted.Value ? 1 : 0));
                        wherecs.Add(string.Format("e.IsDeleted={0}", filter.IsDeleted.Value ? 1 : 0));
                    }

                    if (filter.CaseIds.Count > 0)
                    {
                        string[] caseIdsT = new string[filter.CaseIds.Count];
                        for (int i = 0; i < caseIdsT.Length; i++)
                            caseIdsT[i] = string.Format("'{0}'", filter.CaseIds[i]);

                        wherecs.Add(string.Format("c.CaseId IN ({0})", string.Join(",", caseIdsT)));
                    }

                    if (filter.Finished.HasValue)
                        wherecs.Add(string.Format("e.Finished={0}", filter.Finished.Value ? 1 : 0));

                    if (filter.ReceptionCenters.Count > 0)
                    {
                        int[] rcT = new int[filter.ReceptionCenters.Count];
                        for (int i = 0; i < rcT.Length; i++)
                            rcT[i] = (int)filter.ReceptionCenters[i];

                        wherecs.Add(string.Format("c.ReceptionCenter IN ({0})", string.Join(",", rcT)));
                    }

                    if (!string.IsNullOrWhiteSpace(filter.TenementAddress))
                        wherecs.Add(string.Format("c.TenementAddress LIKE '%{0}%'", filter.TenementAddress));

                    if (!string.IsNullOrWhiteSpace(filter.TenementContract))
                        wherecs.Add(string.Format("c.TenementContract LIKE '{0}%'", filter.TenementContract));

                    if (filter.SourceTypes != null && filter.SourceTypes.Count > 0)
                    {
                        int[] stypes = new int[filter.SourceTypes.Count];
                        for (int i = 0; i < stypes.Length; i++)
                            stypes[i] = (int)filter.SourceTypes[i];

                        wherecs.Add(string.Format("c.SourceType IN ({0})", string.Join(",", stypes)));
                    }


                    if (filter.Statuses != null && filter.Statuses.Count > 0)
                    {
                        long[] stts = new long[filter.Statuses.Count];
                        for (int i = 0; i < stts.Length; i++)
                            stts[i] = (long)filter.Statuses[i];

                        wherecs.Add(string.Format("e.SysNo =(SELECT TOP 1 SysNo FROM CaseEvent WHERE CaseId = c.CaseId and  EventType  IN ({0}) ORDER BY CreateDate DESC)", string.Join(",", stts)));
                    }

                    if (filter.StartDate.HasValue)
                        wherecs.Add(string.Format("c.CreateDate>='{0}'", filter.StartDate.Value.Date));
                    if (filter.EndDate.HasValue)
                        wherecs.Add(string.Format("c.CreateDate<'{0}'", filter.EndDate.Value.AddDays(1).Date));


                    if (!string.IsNullOrWhiteSpace(filter.CreatorName))
                        wherecs.Add(string.Format("c.CreateUserSysNo IN (select sysno from UserInfo  where RealName like '{0}%')", filter.CreatorName));

                    if (!string.IsNullOrWhiteSpace(filter.BuyerName))
                        wherecs.Add(string.Format("c.CaseId IN (select Distinct r.caseId from CaseUserRelation r join customer c on r.UserSysNo = c.SysNo join "
                            + "UserInfo u on c.UserSysNo=u.SysNo where u.RealName like '{0}%' and r.CaseUserType=1 and r.IsDeleted=0)", filter.BuyerName));

                    if (!string.IsNullOrWhiteSpace(filter.AgencyStaffName))
                        wherecs.Add(string.Format("c.CaseId IN (select Distinct r.caseId from CaseUserRelation r  join agentstaff s on r.UserSysNo = s.SysNo join " +
                            "UserInfo u on s.UserSysNo=u.SysNo where u.RealName like '{0}%' and r.CaseUserType=2 and r.IsDeleted=0)", filter.AgencyStaffName));

                    if (!string.IsNullOrWhiteSpace(filter.AgencyStaffPhone))
                        wherecs.Add(string.Format("c.CaseId IN (select Distinct r.caseId from CaseUserRelation r  join agentstaff s on r.UserSysNo = s.SysNo join " +
                            "UserInfo u on s.UserSysNo=u.SysNo where (u.Mobile like '{0}%' or s.Mobile1 like '{0}%') and r.CaseUserType=2 and r.IsDeleted=0)", filter.AgencyStaffPhone));

                    if (filter.AgencyStaffSysNos.Count > 0)
                        wherecs.Add(string.Format("c.CaseId IN (select Distinct r.caseId from CaseUserRelation r where r.UserSysNo in ({0}) and r.CaseUserType=2 and r.IsDeleted=0)", string.Join(",", filter.AgencyStaffSysNos.ToArray())));

                    if (filter.AgentCompanySysNos.Count > 0)
                        wherecs.Add(string.Format("c.CompanySysNo IN ({0})", string.Join(",", filter.AgentCompanySysNos.ToArray())));

                    //查跟进表
                    if (filter.ProccessTypes.Count > 0 || !string.IsNullOrWhiteSpace(filter.TrailerName))
                    {
                        var bst = "select Distinct t.caseId from CaseTrail t join UserInfo u on t.CreateUserSysNo=u.SysNo where t.IsDeleted=0";
                        if (filter.ProccessTypes.Count > 0)
                        {
                            int[] ptypes = new int[filter.ProccessTypes.Count];
                            for (int i = 0; i < ptypes.Length; i++)
                                ptypes[i] = (int)filter.ProccessTypes[i];

                            bst += string.Format(" and t.ProccessType IN ({0})", string.Join(",", ptypes));
                        }

                        if (!string.IsNullOrWhiteSpace(filter.TrailerName))
                            bst += string.Format(" and u.RealName like '{0}%'", filter.TrailerName);

                        wherecs.Add(string.Format("c.CaseId IN ({0})", bst));
                    }

                }

                where = string.Join(" AND ", wherecs.ToArray());
                list= ExecuteQueryForPaginatedList(table, columns, where, "c.CreateDate  DESC ", null, pageIndex, pageSize, out totalCount);
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Case.GetPaginatedList {0}", e));
                throw;
            }

            return list;
        }

        [Obsolete]
        public IList<Case> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, IEnumerable<AgentStaffInfo> users, AgentType agentType, CaseStatus allCaseStatus, string caseId, string tenementAddress,
                    DateTime? startDate, DateTime? endDate, IDictionary<string, object> data, CaseStatus? excludeCaseStatus)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                StringBuilder whereStr = new StringBuilder(" C.IsDeleted=0 and E.IsDeleted=0 ");

                string table = " [Case] C JOIN [CaseEvent] E ON C.CaseId = E.CaseId ";
                var status = (long)allCaseStatus;
                string ceQuery = string.Format(" EventType & convert(bigint,{0}) !=0", status);
                string withQuery = string.Empty;

                if (!string.IsNullOrWhiteSpace(caseId))
                {
                    whereStr.AppendFormat("  AND {0}='{1}'", " C.caseId ", caseId);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(tenementAddress))
                    {
                        whereStr.AppendFormat(" AND {0} like '%{1}%'", "TenementAddress", tenementAddress);
                    }

                }

                if (agentType == AgentType.Agency)
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (var user in users)
                    {
                        sb.AppendFormat("'{0}',", user.SysNo);
                    }
                    string query = string.Format("select Distinct r.caseId from CaseUserRelation r where r.UserSysNo in ({0}) and r.CaseUserType=2 and r.IsDeleted=0  ", sb.ToString().TrimEnd(','));
                    withQuery = string.Format("with caseIds (caseId) as ( {0} )", query);
                    table = string.Format("{0} join caseIds ids on ids.caseid=C.caseid ", table);
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (var user in users)
                    {
                        sb.AppendFormat("'{0}',", user.AgentCompanySysNo);
                    }
                    whereStr.AppendFormat(" and C.CompanySysNo in ({0}) ", sb.ToString().TrimEnd(','));
                    if (data != null && (data.ContainsKey("MoblePhone") || data.ContainsKey("AgentName")))
                    {

                        StringBuilder query = new StringBuilder();
                        if (data.ContainsKey("AgentName"))
                            query.AppendFormat(" select Distinct r.caseId from CaseUserRelation r  join agentstaff s on r.UserSysNo = s.SysNo join UserInfo u on s.UserSysNo=u.SysNo where u.RealName like '{0}%' and r.CaseUserType=2 and r.IsDeleted=0  ", data["AgentName"]);
                        if (data.ContainsKey("MoblePhone"))
                            query.AppendFormat("INTERSECT select Distinct r.caseId from CaseUserRelation r  join agentstaff s on r.UserSysNo = s.SysNo join UserInfo u on s.UserSysNo=u.SysNo where (u.Mobile like '{0}%' or s.Mobile1 like '{0}%') and r.CaseUserType=2 and r.IsDeleted=0  ", data["MoblePhone"]);
                        withQuery = string.Format("with caseIds (caseId) as ( {0} )", query.ToString().TrimStart("INTERSECT"));
                        table = string.Format("{0} join caseIds ids on ids.caseid=C.caseid ", table);
                    }
                }
                if (startDate != null && startDate.Value != default(DateTime))
                    whereStr.AppendFormat(" AND C.[CreateDate]>='{0}' ", startDate.Value.Date);
                if (endDate != null && endDate.Value != default(DateTime))
                    whereStr.AppendFormat(" AND C.[CreateDate]<'{0}' ", endDate.Value.AddDays(1).Date);
                string columns = " C.SysNo, C.CaseId, C.Disabled, C.CompanySysNo,C.Remark,C.TenementAddress,C.TenementContract,C.ReceptionCenter, C.SourceType, C.IsFund,C.FundTrusteeshipContract,C.IsCommission,CommissionTrusteeshipContract,C.CreateDate,C.CreateUserSysNo, C.ModifyUserSysNo, E.EventType as CaseStatus,  ";
                columns += "  CASE WHEN E.ModifyDate IS NULL THEN E.CreateDate ELSE E.ModifyDate END AS ModifyDate ";
                whereStr.AppendFormat(" and  E.SysNo = (SELECT TOP 1 SysNo FROM CaseEvent WHERE CaseId = C.CaseId  and {0} ORDER BY CreateDate DESC ) ", ceQuery);
                if (excludeCaseStatus != null)
                {
                    var es = excludeCaseStatus.Value.ToString().Split(',');
                    StringBuilder sb = new StringBuilder();
                    foreach (var s in es)
                    {
                        sb.AppendFormat("'{0}',", (long)(CaseStatus)Enum.Parse(typeof(CaseStatus), s));
                    }
                    whereStr.AppendFormat(" and EventType not in ({0}) ", sb.ToString().TrimEnd(','));
                }
                return ExecuteQueryForPaginatedList(table, columns, whereStr.ToString(), "C.CreateDate  desc ", withQuery, pageIndex, pageSize, out totalCount);
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Case.GetPaginatedData {0}", e));
                throw;
            }
        }



        public bool InsertCaseTrail(CaseTrail caseTrail)
        {
            try
            {
                caseTrail.SysNo = Sequence.Get();
                TransactionManager.Instance.BeginTransaction();
                //作废上一条跟进
                ExecuteUpdate("Case.DeleteCaseTrail", new Dictionary<string, object> { { "ModifyUserSysNo", caseTrail.CreateUserSysNo }, { "CaseId", caseTrail.CaseId } });
                //插入新的跟进
                ExecuteInsert("Case.InsertCaseTrail", caseTrail);
                TransactionManager.Instance.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                if (TransactionManager.Instance.IsTransactionStarted)
                    TransactionManager.Instance.RollBackTransaction();
                throw;
            }
        }

        public CaseTrail GetLastCaseTrailByCaseId(string caseId)
        {
            return ExecuteQueryForAnyObject("Case.GetLastCaseTrailByCaseId", caseId) as CaseTrail;
        }

        /// <summary>
        /// 根据案件跟进信息查询
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="receptionCenter"></param>
        /// <param name="proccessType"></param>
        /// <param name="startDate"></param>
        /// <param name="lastTrailer"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        [Obsolete]
        public IList<Case> GetCases(int pageIndex, int pageSize, out int totalCount, ReceptionCenter receptionCenter, ProccessType? proccessType, DateTime? startDate, string lastTrailer, Dictionary<string, object> data)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                StringBuilder whereStr = new StringBuilder("C.IsDeleted=0 and E.IsDeleted=0 ");
                whereStr.AppendFormat(" AND ([ReceptionCenter]={0} OR GlobalFlag=1) ", (uint)receptionCenter);
                string table = " [Case] C JOIN [CaseEvent] E ON C.CaseId = E.CaseId ";

                string withQuery = string.Empty;
                if (proccessType != null || !string.IsNullOrWhiteSpace(lastTrailer))
                {
                    StringBuilder query = new StringBuilder();
                    query.Append(" select  t.caseId from CaseTrail t join UserInfo u on t.CreateUserSysNo=u.SysNo where t.IsDeleted=0 ");
                    if (proccessType != null)
                        query.AppendFormat(" and proccessType={0} ", (int)proccessType);
                    if (!string.IsNullOrWhiteSpace(lastTrailer))
                        query.AppendFormat(" and  u.RealName like '{0}%'  ", lastTrailer);
                    withQuery = string.Format("with caseIds (caseId) as ( {0} )", query.ToString());
                    table = string.Format("{0} join caseIds ids on ids.caseid=C.caseid ", table);
                }
                string columns = " C.SysNo, C.CaseId, C.Disabled, C.CompanySysNo,C.Remark,C.TenementAddress,C.TenementContract,C.ReceptionCenter, C.SourceType, C.IsFund,C.FundTrusteeshipContract,C.IsCommission,CommissionTrusteeshipContract,C.CreateDate,C.CreateUserSysNo, C.ModifyUserSysNo, E.EventType as CaseStatus  ";
                columns += "  ,CASE WHEN E.ModifyDate IS NULL THEN E.CreateDate ELSE E.ModifyDate END AS ModifyDate ";
                string ceQuery = "";
                if (startDate != null && startDate != default(DateTime))
                    ceQuery = string.Format(" and CreateDate >= '{0}'", startDate.Value.Date);
                whereStr.AppendFormat(" and  E.SysNo = (SELECT TOP 1 SysNo FROM CaseEvent WHERE CaseId = C.CaseId {0} ORDER BY CreateDate DESC ) ", ceQuery);
                return ExecuteQueryForPaginatedList(table, columns, whereStr.ToString(), "E.CreateDate  desc ", withQuery, pageIndex, pageSize, out totalCount);
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Case.GetPaginatedData {0}", e));
                throw;
            }
        }

        [Obsolete]
        public IList<Case> GetCases(int pageIndex, int pageSize, out int totalCount, Dictionary<string, object> data)
        {
            try
            {
                StringBuilder whereStr = new StringBuilder(" IsDeleted=0 ");
                whereStr.AppendFormat(" AND ([ReceptionCenter]={0} OR GlobalFlag=1) ", (int)data["ReceptionCenter"]);
                string table = "  [Case] ";
                string columns = " * ";
                if (data.ContainsKey("CaseId"))
                {
                    whereStr.AppendFormat(" and CaseId='{0}' ", data["CaseId"]);
                }
                else if (data.ContainsKey("TenementContract"))
                {
                    whereStr.AppendFormat(" and TenementContract like '{0}' ", data["TenementContract"]);
                }
                else if (data.ContainsKey("TenementAddress"))
                {
                    whereStr.AppendFormat(" and TenementAddress like '{0}' ", data["TenementAddress"]);
                }

                return ExecuteQueryForPaginatedList<Case>("GetCaseList", table, columns, whereStr.ToString(), "CreateDate  desc ", pageIndex, pageSize, out totalCount);
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Case.GetCases {0}", e));
                throw;
            }
        }

        public bool UpdateState(string caseId, bool disabled, long modifyUserSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteUpdate("Case.UpdateState", new Dictionary<string, object> { { "CaseId", caseId }, { "Disabled", disabled }, { "ModifyUserSysNo", modifyUserSysNo } });
                Log.Dao(string.Format("Dao -> Statement:{0}.UpdateState({1}),Exception:{2}", "Case", caseId, DateTime.Now.Ticks - ts));
                return result > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}.UpdateState({1}),Exception:{2}", "Case", caseId, e));
                throw;
            }
        }

        public bool Update(string caseId, IDictionary<CaseUserType, List<long>> unBindUsers, List<long> buyers, List<long> sellers, long? agencySysNo, long modifyUserSysNo, Dictionary<string, object> data = null)
        {
            try
            {
                if (!TransactionManager.Instance.IsTransactionStarted)
                    TransactionManager.Instance.BeginTransaction();

                //更改产证信息
                data["CaseId"] = caseId;
                data["ModifyUserSysNo"] = modifyUserSysNo;
                ExecuteUpdate("Case.UpdateCase", data);
                InsertOrUpdatePropertyCertificate(data["TenementContract"].ToString(), data["TenementAddress"].ToString(), sellers, modifyUserSysNo);
                //绑定人员
                InsertCaseUser(caseId, CaseUserType.Buyer, buyers.ToArray());
                InsertCaseUser(caseId, CaseUserType.Seller, sellers.ToArray());
                if (agencySysNo != null && agencySysNo != 0)
                {
                    InsertCaseUser(caseId, CaseUserType.AgentStaff, agencySysNo.Value);
                }

                if (unBindUsers != null && unBindUsers.Count > 0)
                {
                    foreach (var user in unBindUsers)
                    {
                        if (user.Value != null)
                        {
                            foreach (var usysno in user.Value)
                            {
                                UnBindUserRelation(caseId, usysno, user.Key, modifyUserSysNo);
                            }
                        }

                    }
                }

                if (TransactionManager.Instance.IsTransactionStarted)
                    TransactionManager.Instance.CommitTransaction();

                return true;

            }
            catch (Exception ex)
            {
                if (TransactionManager.Instance.IsTransactionStarted)
                    TransactionManager.Instance.RollBackTransaction();
                Log.Error(ex);
                throw;
            }
        }

        public Case GetCaseByTenementContract(string tenementContract, ReceptionCenter? center)
        {
            var dic = new Dictionary<string, object> { { "TenementContract", tenementContract }, };
            if (center != null)
                dic["ReceptionCenter"] = (int)center;
            return ExecuteQueryForObject("Case.GetByTenementContract", dic);
        }
        public bool ExistByTenementContract(string tenementContract)
        {
            var result = ExecuteQueryForAnyObject("Case.ExistByTenementContract", tenementContract);
            return result != null && result.ToString().CompareTo("0") != 0;
        }
        public bool UnBindUserRelation(string caseId, long userSysNo, CaseUserType caseUserType, long modifyUserSysNo)
        {
            return ExecuteUpdate("Case.UnBindUserRelation", new Dictionary<string, object> { { "ModifyUserSysNo", modifyUserSysNo }, { "CaseId", caseId }, 
                { "UserSysNo", userSysNo }, { "CaseUserType", caseUserType } }) > 0;
        }

        public Case GetCaseByUserSysNo(long userSysNo)
        {
            return ExecuteQueryForObject("Case.GetCaseByUserSysNo", userSysNo);
        }

        public IList<string> GetIds(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, string caseId)
        {
            var ts = DateTime.Now.Ticks;
            StringBuilder whereStr = new StringBuilder(string.Format("([ReceptionCenter] = {0} OR GlobalFlag=1) AND [IsDeleted] = 0 ", (int)center));
            try
            {
                if (!string.IsNullOrWhiteSpace(caseId))
                {
                    whereStr.AppendFormat(" AND [CaseId] LIKE '{0}%'", caseId);
                }

                var result = ExecuteQueryForPaginatedList<CaseIdQueryMap>("GetPaginatedIdList", "[Case]", "[CaseId]",
                    whereStr.ToString(), "[CreateDate] DESC", pageIndex, pageSize, out totalCount);
                Log.Dao(string.Format("Case.GetPaginatedIdList({0}),{1}", whereStr, DateTime.Now.Ticks - ts));
                if (result == null)
                    return null;
                return result.Select(c => c.CaseId).ToList();
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Case.GetPaginatedIdList({0}),{1}", whereStr, e));
                throw;
            }
        }

        #region Remark

        public CaseRemarkInfo GetRemark(long sysNo)
        {
            return ExecuteQueryForAnyObject("Case.GetRemark", sysNo) as CaseRemarkInfo;
        }

        public IList<Case> GetCases(Dictionary<string, object> data)
        {
            return ExecuteQueryForList("Case.GetCases", data);
        }

        public bool InsertRemark(CaseRemarkInfo remark)
        {
            ExecuteInsert("Case.InsertRemark", remark);
            return true;
        }

        public bool UpdateRemark(CaseRemarkInfo remark)
        {
            return ExecuteUpdate("Case.UpdateRemark", remark) > 0;
        }
        #endregion

        public bool UpdateTrusteeshipInfo(long sysNo, bool? isFundTrusteeship = null, string ftContractNo = null, bool? isCommissionTrusteeship = null, string ctContractNo = null, long? operatorSysNo = null)
        {
            if (isFundTrusteeship == null && isCommissionTrusteeship == null)
                return false;
            var parameters = new Dictionary<string, object>() { { "SysNo", sysNo }, { "ModifyUserSysNo", operatorSysNo } };
            if (isFundTrusteeship != null)
            {
                parameters.Add("IsFund", isFundTrusteeship);
                parameters.Add("FundTrusteeshipContract", ftContractNo);
            }
            if (isCommissionTrusteeship != null)
            {
                parameters.Add("IsCommission", isCommissionTrusteeship);
                parameters.Add("CommissionTrusteeshipContract", ctContractNo);
            }

            return ExecuteUpdate("Case.UpdateTrusteeshipInfo", parameters) > 0;
        }
    }
}
