﻿using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace PinganHouse.SHHTS.DataAccess
{
    public class ParterUserInfoDataAccess : BaseDataAccess<ParterUserInfo>, IParterUserInfoDataAccess
    {
        public override bool Insert(ParterUserInfo entity)
        {
            try
            {
                if (!TransactionManager.Instance.IsTransactionStarted)
                    TransactionManager.Instance.BeginTransaction();
                this.DeleteByOpenId(entity.OpenID);
                entity.SysNo = Sequence.Get();
                ExecuteInsert("ParterUserInfo.Insert", entity);
                if (TransactionManager.Instance.IsTransactionStarted)
                    TransactionManager.Instance.CommitTransaction();
                return true;

            }catch(Exception ex)
            {
                if (TransactionManager.Instance.IsTransactionStarted)
                    TransactionManager.Instance.RollBackTransaction();
                Log.Error(ex);
                return false;
            }
        }

        public ParterUserInfo Get(string openID)
        {
            var dic = new Dictionary<string, object> { { "OpenID", openID } };
            return ExecuteQueryForObject("ParterUserInfo.GetParterUsers", dic);
        }

        public ParterUserInfo Get(string openID, long userSysNo)
        {
            var dic = new Dictionary<string, object> { { "OpenID", openID }, { "UserSysNo", userSysNo } };
            return ExecuteQueryForObject("ParterUserInfo.GetParterUser", dic);
        }

        public bool DeleteByOpenId(string openId)
        {
            return ExecuteDelete("ParterUserInfo.Delete", openId)!=0;
        }

        public bool RelieveBind(long userSysNo, long? modifyUserSysNo)
        {
            return ExecuteDelete("ParterUserInfo.RelieveBind", 
                new Dictionary<string, object>{{"ModifyUserSysNo", modifyUserSysNo},{"UserSysNo", userSysNo}}) != 0;
        }

        public IList<ParterUserInfo> GetByUserSysNo(long userSysNo)
        {
            return ExecuteQueryForList("ParterUserInfo.GetByUserSysNo", userSysNo);
        }

        public bool UpdateByOpenId(string openId, bool subscribe)
        {
            var result = ExecuteUpdate("ParterUserInfo.UpdateByOpenId", new Dictionary<string, object> { { "OpenID", openId }, { "Subscribe", subscribe } });
            return result > 0;
        }
    }
}
