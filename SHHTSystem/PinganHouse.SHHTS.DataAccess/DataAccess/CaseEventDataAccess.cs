﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.DataAccess
{
    public class CaseEventDataAccess : BaseDataAccess<CaseEventInfo>, ICaseEventDataAccess
    {
        [Obsolete]
        public bool Insert(CaseEventInfo entity, CaseStatus preStatus)
        {
            entity.SysNo = Sequence.Get();
            try
            {
                if (entity.EventType == CaseStatus.ZB2 || entity.EventType == CaseStatus.ZB7)
                {
                    TransactionManager.Instance.BeginTransaction();
                    ServiceDepository.CaseDataAccess.UpdateState(entity.CaseId, true, entity.OperatorSysNo);
                }

                if (!TransactionManager.Instance.IsTransactionStarted)
                    TransactionManager.Instance.BeginTransaction();
                Finish(entity.CaseId, preStatus, entity.CreateUserSysNo);

                bool result = base.Insert(entity);

                if (entity.OtherDatas != null)
                {
                    foreach (string key in entity.OtherDatas.Keys)
                    {
                        IDictionary<string, object> pas = new Dictionary<string, object>();
                        pas.Add("O_CaseId", entity.CaseId);
                        pas.Add("O_EventId", entity.SysNo);
                        pas.Add("Key", key);
                        pas.Add("Value", entity.OtherDatas[key]);

                        ExecuteInsert(string.Concat(StatementPrefix, "Insert_EventData"), pas);
                    }
                }

                if (TransactionManager.Instance.IsTransactionStarted)
                    TransactionManager.Instance.CommitTransaction();
                return result;
            }
            catch (Exception e)
            {
                if (TransactionManager.Instance.IsTransactionStarted)
                    TransactionManager.Instance.RollBackTransaction();
                Log.Error(e);
            }
            return false;
        }

        private void FillOtherDatas(CaseEventInfo entity)
        {
            if (entity != null)
            {
                var ds = ExecuteQueryWithDataSet(string.Concat(StatementPrefix, "GetEventOtherDatas"), entity.SysNo);
                if (ds.Tables.Count == 1)
                {
                    foreach (System.Data.DataRow r in ds.Tables[0].Rows)
                        entity.OtherDatas.Add(new KeyValuePair<string, object>(r.ItemArray[0].ToString(), r.ItemArray[1]));
                }
            }
        }

        public override bool Insert(CaseEventInfo entity)
        {
            if (entity == null)
                return false;

            var ts = DateTime.Now.Ticks;


            //事物放在外部开启

            //try
            //{
            //    //TransactionManager.Instance.BeginTransaction();

                ExecuteInsert(string.Concat(StatementPrefix, "Insert"), entity);


                if (entity.OtherDatas != null)
                {
                    foreach (string key in entity.OtherDatas.Keys)
                    {
                        IDictionary<string, object> pas = new Dictionary<string, object>();
                        pas.Add("O_CaseId", entity.CaseId);
                        pas.Add("O_EventId", entity.SysNo);
                        pas.Add("Key", key);
                        pas.Add("Value", entity.OtherDatas[key]);

                        ExecuteInsert(string.Concat(StatementPrefix, "Insert_EventData"), pas);
                    }
                }

                //TransactionManager.Instance.CommitTransaction();

                Log.Dao(string.Format("Dao -> Statement:{0}Insert({1}),{2}", StatementPrefix, entity.SysNo, DateTime.Now.Ticks - ts));
            //}
            //catch (Exception e)
            //{
            //    //TransactionManager.Instance.RollBackTransaction();
            //    Log.Error(string.Format("Dao -> Statement:{0}Insert({1}),Exception:{2}", StatementPrefix, entity.SysNo, e));
            //    throw;
            //}

            return true;
        }

        public override bool Update(CaseEventInfo entity)
        {
            if (entity == null)
                return false;

            var ts = DateTime.Now.Ticks;

            //事物放在外部开启

            //try
            //{
            //    TransactionManager.Instance.BeginTransaction();

            ExecuteUpdate(string.Concat(StatementPrefix, "Update"), entity);

            IDictionary<string, object> pas = new Dictionary<string, object>();
            pas.Add("O_CaseId", entity.CaseId);
            pas.Add("O_EventId", entity.SysNo);

            ExecuteDelete(string.Concat(StatementPrefix, "Remove_EventData"), pas);

            if (entity.OtherDatas != null)
            {
                foreach (string key in entity.OtherDatas.Keys)
                {
                    pas.Clear();
                    pas.Add("O_CaseId", entity.CaseId);
                    pas.Add("O_EventId", entity.SysNo);
                    pas.Add("Key", key);
                    pas.Add("Value", entity.OtherDatas[key]);

                    ExecuteInsert(string.Concat(StatementPrefix, "Insert_EventData"), pas);
                }
            }

            //TransactionManager.Instance.CommitTransaction();

            Log.Dao(string.Format("Dao -> Statement:{0}Update({1}),Exception:{2}", StatementPrefix, entity.SysNo, DateTime.Now.Ticks - ts));

            //}
            //catch (Exception e)
            //{
            //    TransactionManager.Instance.RollBackTransaction();
            //    Log.Error(string.Format("Dao -> Statement:{0}Update({1}),Exception:{2}", StatementPrefix, entity.SysNo, e));
            //    throw;
            //}

            return true;
        }

        public override CaseEventInfo Get(long sysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForObject(string.Concat(StatementPrefix, "Get"), sysNo);
                FillOtherDatas(result);
                Log.Dao(string.Format("Statement : {0}Get({1}),{2}", StatementPrefix, sysNo, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}GET({1}),Exception:{2}", StatementPrefix, sysNo, e));
                throw;
            }
        }


        //public bool Insert(CaseEventInfo entity, CaseStatus? preStatus = null)
        //{
        //    entity.SysNo = Sequence.Get();
        //    try
        //    {
        //        if (entity.EventType == CaseStatus.ZB2 || entity.EventType == CaseStatus.ZB7)
        //        {
        //            TransactionManager.Instance.BeginTransaction();
        //            ServiceDepository.CaseDataAccess.UpdateState(entity.CaseId, true, entity.OperatorSysNo);
        //        }
        //        if (preStatus != null)
        //        {                   
        //            if (!TransactionManager.Instance.IsTransactionStarted)
        //                TransactionManager.Instance.BeginTransaction();
        //            Finish(entity.CaseId, preStatus.Value, entity.CreateUserSysNo);
        //        }
        //        bool result = base.Insert(entity);

        //        if (TransactionManager.Instance.IsTransactionStarted)
        //            TransactionManager.Instance.CommitTransaction();
        //        return result;
        //    }
        //    catch (Exception e)
        //    {
        //        if (TransactionManager.Instance.IsTransactionStarted)
        //            TransactionManager.Instance.RollBackTransaction();
        //        Log.Error(e);
        //    }
        //    return false;
        //}

        public IEnumerable<CaseEventInfo> Get(string caseId, CaseStatus? sourceStatus = null, CaseStatus? eventType = null, bool? finished = null)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var parameters = new Dictionary<string, object> { { "CaseId", caseId } };
                if (sourceStatus != null)
                    parameters.Add("SourceStatus", sourceStatus);
                if (eventType != null)
                    parameters.Add("EventType", eventType);
                if (finished != null)
                    parameters.Add("Finished", finished);
                var result = ExecuteQueryForList("CaseEventInfo.GetByCaseId", parameters);
                foreach (var o in result)
                    FillOtherDatas(o);
                Log.Dao(string.Format("Statement : {0}.GetByCaseId({1}),{2}", "CaseEventInfo", caseId, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}.GetByCaseId({1}),Exception:{2}", "CaseEventInfo", caseId, e));
                throw;
            }
        }

        public IEnumerable<CaseEventInfo> GetCaseEvents(IEnumerable<string> caseIds, CaseStatus eventType)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var caseid in caseIds)
            {
                sb.AppendFormat("{0},", caseid);
            }
            var parameters = new Dictionary<string, object> { { "CaseId", sb.ToString().TrimEnd(',') }, { "EventType", (long)eventType } };
            var result = ExecuteQueryForList("CaseEventInfo.GetCaseEvents", parameters);
            foreach (var o in result)
                FillOtherDatas(o);
            return result;
        }

        public IEnumerable<CaseEventInfo> GetCaseEvents(CaseStatus eventType)
        {
            var result = ExecuteQueryForList("CaseEventInfo.GetCaseEventsByEventType", (long)eventType);
            foreach (var o in result)
                FillOtherDatas(o);
            return result;
        }

        public bool UpdateRemark(long sysno, string remark)
        {
            var parameters = new Dictionary<string, object> { { "SysNo", sysno } };
            parameters["Remark"] = remark;
            return ExecuteUpdate("CaseEventInfo.UpdateRemark", parameters) != 0;
        }

        public IEnumerable<CaseEventInfo> GetCaseEvents(string caseId, CaseStatus eventType)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var parameters = new Dictionary<string, object> { { "CaseId", caseId }, { "EventType", (long)eventType } };
                var result = ExecuteQueryForList("CaseEventInfo.GetByCaseIdAndStatus", parameters);
                foreach (var o in result)
                    FillOtherDatas(o);
                Log.Dao(string.Format("Statement : {0}.Get({1}),{2}", "GetCaseEvents", caseId, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}.Get({1}),Exception:{2}", "CaseEventInfo", caseId, e));
                throw;
            }
        }

        public CaseEventInfo GetNewByCaseId(string caseId, CaseStatus caseStatus)
        {

            var parameters = new Dictionary<string, object> { { "CaseId", caseId }, { "EventType", (long)caseStatus } };
            var result = ExecuteQueryForObject("CaseEventInfo.GetCaseEventByCaseId", parameters);
            FillOtherDatas(result);
            return result;
        }

        public bool Finish(string caseId, CaseStatus caseStatus, long? operatorSysNo)
        {
            return ExecuteUpdate("CaseEventInfo.Finish", new Dictionary<string, object> 
            {
                {"CaseId",caseId},{"EventType",(long)caseStatus},{"ModifyUserSysNo",operatorSysNo}
            }) > 0;
        }

        public IEnumerable<CaseEventInfo> Get(string caseId, int pageIndex, int pageSize, out int totalCount, IEnumerable<CaseStatus> status = null)
        {
            totalCount = 0;
            if (string.IsNullOrEmpty(caseId))
                return null;
            var ts = DateTime.Now.Ticks;
            try
            {
                StringBuilder whereStr = new StringBuilder(string.Format("[CaseId] = '{0}'", caseId));
                if (status != null && status.Count() > 0)
                {
                    List<long> statusValues = new List<long>();
                    foreach (var stat in Enum.GetValues(typeof(CaseStatus)))
                    {
                        if (status.Contains((CaseStatus)stat))
                            statusValues.Add((long)stat);
                    }
                    whereStr.AppendFormat(" AND [EventType] IN ({0})", string.Join(",", statusValues));
                }
                var result = ExecuteQueryForPaginatedList("[CaseEvent]", "*", whereStr.ToString(), "[CreateDate]", pageIndex, pageSize, out totalCount);
                foreach (var o in result)
                    FillOtherDatas(o);

                Log.Dao(string.Format("CaseEventInfo.GetPaginatedList({0}),{1}", caseId, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("CaseEventInfo.GetPaginatedList({0}),{1}", caseId, e));
                throw;
            }
        }

    }
}
