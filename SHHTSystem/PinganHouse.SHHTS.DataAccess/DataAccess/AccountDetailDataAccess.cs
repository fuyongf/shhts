﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.DataAccess
{
    public class AccountDetailDataAccess : BaseDataAccess<AccountDetailInfo>, IAccountDetailDataAccess
    {
        public override bool Insert(AccountDetailInfo entity)
        {
            entity.SysNo = Sequence.Get();
            return base.Insert(entity);
        }


        public IList<AccountDetailInfo> GetByAccount(long accountSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForList("AccountDetailInfo.GetByAccount", accountSysNo);
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "AccountDetailInfo.GetByAccount", accountSysNo, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "AccountDetailInfo.GetByAccount", accountSysNo, e));
                throw;
            }
        }

        public IList<AccountDetailInfo> GetByOrder(string orderNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForList("AccountDetailInfo.GetByOrder", orderNo);
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "AccountDetailInfo.GetByOrder", orderNo, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "AccountDetailInfo.GetByOrder", orderNo, e));
                throw;
            }
        }
    }
}
