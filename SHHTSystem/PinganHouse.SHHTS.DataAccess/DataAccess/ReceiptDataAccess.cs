﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils.Log;

namespace PinganHouse.SHHTS.DataAccess
{
    public class ReceiptDataAccess : BaseDataAccess<ReceiptInfo>, IReceiptDataAccess
    {
        public ReceiptInfo Get(string receiptNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForObject("ReceiptInfo.GetByNo", receiptNo);
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "ReceiptInfo.GetByNo", receiptNo, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "ReceiptInfo.GetByNo", receiptNo, e));
                throw;
            }
        }

        public override bool Insert(ReceiptInfo entity)
        {
            entity.SysNo = Sequence.Get();
            //DateTime now = DateTime.Now;
            //string ts = TimeStamp.Get(now).ToString();
            //entity.ReceiptNo = string.Concat(now.ToString("yyyyMMdd"), ts.Substring(ts.Length - 5, 5), RandomHelper.GetNumber(999, 3));
            //entity.CreateDate = DateTime.Now;
            return base.Insert(entity);
        }
        public ReceiptSerialInfo GetReceiptSerialInfo(long receiptSynNo)
        {
            return ExecuteQueryForAnyObject("ReceiptInfo.GetReceiptSerialInfo", receiptSynNo) as ReceiptSerialInfo;
        }

        public bool InsertReceiptSerialInfo(long sysNo, string receiptNo, string receiptSerialNo, string securityCode, string remark, long createUserSysNo)
        {
            ReceiptSerialInfo entity = new ReceiptSerialInfo
            {
                SecurityCode = securityCode,
                ReceiptNo = receiptNo,
                ReceiptSerialNo = receiptSerialNo,
                CreateDate = DateTime.Now,
                CreateUserSysNo = createUserSysNo,
                Remark = remark,
                SysNo = sysNo
            };
            ExecuteInsert("ReceiptInfo.ReceiptSerialInfoInsert", entity);
            return true;
        }

        public IList<ReceiptInfo> GetReceiptByOrderNo(string orderNo)
        {
            return ExecuteQueryForList("ReceiptInfo.GetReceiptByOrderNo", orderNo);
        }

        public IList<ReceiptInfo> GetReceiptSerialInfos(string orderNo)
        {
            return ExecuteQueryForList("ReceiptInfo.GetByOrderNo", orderNo);
        }

        public int GetReceiptPrintCount(string receiptNo)
        {
            return (int)ExecuteQueryForAnyObject("ReceiptInfo.GetReceiptPrintCount", receiptNo);
        }

        public ReceiptSerialInfo GetReceiptSerialBySecurityCode(string securityCode)
        {
            return ExecuteQueryForAnyObject("ReceiptInfo.GetReceiptSerialBySecurityCode", securityCode) as ReceiptSerialInfo;
        }

        public IList<ReceiptInfo> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, ReceiptStatus? status, string receiptNo)
        {
            var ts = DateTime.Now.Ticks;
            StringBuilder whereStr = new StringBuilder(string.Format("(C.[ReceptionCenter] = {0} OR C.GlobalFlag=1)", (int)center));
            try
            {
                if (!string.IsNullOrWhiteSpace(receiptNo))
                {
                    whereStr.AppendFormat(" AND [ReceiptNo] = '{0}'", receiptNo);
                }
                if (status != null)
                {
                    whereStr.AppendFormat(" AND [Status] = {0}", (int)status);
                }
                //[ReceiptInfo] R JOIN [AccountInfo] A ON R.[AccountSysNo] = A.[SysNo] JOIN [Case] C ON A.[SubjectId] = C.[CaseId] LEFT JOIN [UserInfo] U ON R.[CreateUserSysNo] = U.[SysNo] LEFT JOIN [UserInfo] U1 ON R.[ModifyUserSysNo] = U1.[SysNo]
                var result = ExecuteQueryForPaginatedList("[ReceiptInfo] R JOIN [OrderInfo] O ON R.[OrderNo] = O.[OrderNo] JOIN [AccountInfo] A ON O.[AccountSysNo] = A.[SysNo] JOIN [Case] C ON A.[SubjectId] = C.[CaseId] LEFT JOIN [UserInfo] U ON R.[CreateUserSysNo] = U.[SysNo] LEFT JOIN [UserInfo] U1 ON R.[ModifyUserSysNo] = U1.[SysNo]",
                    "R.*,U.[RealName] AS [CreateName],U1.[RealName] AS [ModifyName],C.[CaseId],C.[TenementAddress]",
                    whereStr.ToString(), "R.[CreateDate] DESC", pageIndex, pageSize, out totalCount);
                Log.Dao(string.Format("ReceiptInfo.GetPaginatedList({0}),{1}", whereStr, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("ReceiptInfo.GetPaginatedList({0}),{1}", whereStr, e));
                throw;
            }
        }
    }
}
