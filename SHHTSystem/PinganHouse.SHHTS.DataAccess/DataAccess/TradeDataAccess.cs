﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.DataAccess
{
    public class TradeDataAccess : BaseDataAccess<TradeInfo>, ITradeDataAccess
    {
        public TradeInfo Get(string tradeNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForObject("TradeInfo.GetByNo", tradeNo);
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "TradeInfo.GetByNo", tradeNo, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "TradeInfo.GetByNo", tradeNo, e));
                throw;
            }
        }

        public IList<TradeInfo> GetByOrder(string orderNo, params string[] tradeNos)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var data = new Dictionary<string, object> { { "OrderNo", orderNo } };
                if (tradeNos != null && tradeNos.Length > 0)
                    data["TradeNo"] = string.Join(",", tradeNos);
                var result = ExecuteQueryForList("TradeInfo.GetByOrderNo", data);
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "TradeInfo.GetByOrderNo", orderNo, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "TradeInfo.GetByOrderNo", orderNo, e));
                throw;
            }
        }

        public bool UpdateReceipt(string orderNo, long modifyUserSysNo, params string[] tradeNos)
        {
            var data = new Dictionary<string, object> { { "OrderNo", orderNo }, { "ModifyUserSysNo", modifyUserSysNo } };
            if (tradeNos != null && tradeNos.Length > 0)
                data["TradeNo"] = string.Join(",", tradeNos);
            return ExecuteUpdate("TradeInfo.UpdateReceipt", data) > 0;
        }

        public IList<TradeInfo> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, PaymentChannel? channel, string orderNo, string tradeNo, string caseId, string bankAccountNo, DateTime? startDate, DateTime? endDate)
        {
            var ts = DateTime.Now.Ticks;
            StringBuilder whereStr = new StringBuilder(string.Format("(C.[ReceptionCenter] = {0} OR C.GlobalFlag=1)", (int)center));
            try
            {
                if (!string.IsNullOrWhiteSpace(orderNo))
                {
                    whereStr.AppendFormat(" AND O.[OrderNo] = '{0}'", orderNo);
                }
                if (!string.IsNullOrWhiteSpace(tradeNo))
                {
                    whereStr.AppendFormat(" AND T.[TradeNo] = '{0}'", tradeNo);
                }
                if (!string.IsNullOrWhiteSpace(caseId))
                {
                    whereStr.AppendFormat(" AND C.[CaseId] = '{0}'", caseId);
                }
                if (channel != null)
                {
                    whereStr.AppendFormat(" AND O.[PaymentChannel] = {0}", (int)channel.Value);
                }
                if (!string.IsNullOrWhiteSpace(bankAccountNo))
                {
                    whereStr.AppendFormat(" AND T.[BankAccountNo] = '{0}'", bankAccountNo);
                }
                if (startDate != null)
                {
                    whereStr.AppendFormat(" AND T.[CreateDate] >= '{0}'", startDate.Value.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                }
                if (endDate != null)
                {
                    whereStr.AppendFormat(" AND T.[CreateDate] <= '{0}'", endDate.Value.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                }

                var result = ExecuteQueryForPaginatedList("[TradeInfo] T JOIN [OrderInfo] O ON O.[OrderNo] = T.[OrderNo] JOIN [AccountInfo] A ON  O.[AccountSysNo] = A.[SysNo] JOIN [Case] C ON C.[CaseId] = A.[SubjectId]",
                    @"C.[CaseId],C.[TenementAddress],A.[AccountType],O.[PaymentChannel],O.[OrderType],O.[OrderBizType],T.*",
                    whereStr.ToString(), "T.[CreateDate] DESC", pageIndex, pageSize, out totalCount);
                Log.Dao(string.Format("TradeInfo.GetPaginatedList({0}),{1}", whereStr, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("TradeInfo.GetPaginatedList({0}),{1}", whereStr, e));
                throw;
            }
        }

        public IList<TradeInfo> GetTrades(string caseId, AccountType accountType)
        {
            return ExecuteQueryForList("TradeInfo.GetTrades", new Dictionary<string, object> { { "CaseId", caseId }, { "AccountType", (int)accountType } });
        }

        public IList<TradeInfo> GetPrintReceiptTradesByOrder(string orderNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForList("TradeInfo.GetPrintReceiptTradesByOrder", orderNo);
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "TradeInfo.GetPrintReceiptTradesByOrder", orderNo, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "TradeInfo.GetPrintReceiptTradesByOrder", orderNo, e));
                throw;
            }
        }

        //public IList<TradeInfo> GetByOrder(string order, TradeStatus status)
        //{
        //    var ts = DateTime.Now.Ticks;
        //    try
        //    {
        //        var result = ExecuteQueryForList("TradeInfo.GetByOrderAndStatus", new Dictionary<string, object> { { "OrderNo", order }, { "TradeStatus", status } });
        //        Log.Dao(string.Format("Statement : {0}({1}),{2}", "TradeInfo.GetByOrderAndStatus", order, DateTime.Now.Ticks - ts));
        //        return result;
        //    }
        //    catch (Exception e)
        //    {
        //        Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "TradeInfo.GetByOrderAndStatus", order, e));
        //        throw;
        //    }
        //}

        public IList<TradeInfo> GetSendBillTrades()
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForList("TradeInfo.GetSendBillTrades", "");
                Log.Dao(string.Format("Statement : {0}(),{1}", "TradeInfo.GetSendBillTrades", DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}(),Exception:{1}", "TradeInfo.GetSendBillTrades", e));
                throw;
            }
        }

        public IList<TradeInfo> GetByCase(int pageIndex, int pageSize, out int totalCount, string caseId, AccountType? accountType, OrderType? orderType)
        {
            totalCount = 0;
            if (string.IsNullOrWhiteSpace(caseId))
                return null;
            var ts = DateTime.Now.Ticks;
            StringBuilder whereStr = new StringBuilder(string.Format("C.[CaseId] = '{0}' AND T.[Status] = {1} ", caseId, (int)TradeStatus.Completed));
            try
            {
                if (accountType != null)
                {
                    whereStr.AppendFormat(" AND A.[AccountType] = {0}", (int)accountType.Value);
                }
                if (orderType != null)
                {
                    whereStr.AppendFormat(" AND O.[OrderType] = {0}", (int)orderType.Value);
                }

                var result = ExecuteQueryForPaginatedList("[TradeInfo] T JOIN [OrderInfo] O ON O.[OrderNo] = T.[OrderNo] JOIN [AccountInfo] A ON  O.[AccountSysNo] = A.[SysNo] JOIN [Case] C ON C.[CaseId] = A.[SubjectId]",
                    @"C.[CaseId],C.[TenementAddress],A.[AccountType],O.[PaymentChannel],O.[OrderType],O.[OrderBizType],T.*",
                    whereStr.ToString(), "T.[CreateDate] DESC", pageIndex, pageSize, out totalCount);
                Log.Dao(string.Format("TradeInfo.GetPaginatedList({0}),{1}", whereStr, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("TradeInfo.GetPaginatedList({0}),{1}", whereStr, e));
                throw;
            }
        }

        public IList<TradeInfo> GetPaginatedListByAccount(int pageIndex, int pageSize, out int totalCount, bool isSelf, params long[] accounts) 
        {
            totalCount = 0;
            if (accounts == null || accounts.Count() == 0)
                return null;
            var ts = DateTime.Now.Ticks;
            StringBuilder whereStr = new StringBuilder("T.[IsDeleted] = 0 ");
            try
            {
                if (isSelf)
                    whereStr.AppendFormat(" AND (((O.[OrderType] = 0 OR O.[OrderType] = 4) AND O.[AccountSysNo] IN ({0})) OR O.[OppositeAccount] IN ({0}))", string.Join(",", accounts));
                else
                    whereStr.AppendFormat(" AND ((O.[AccountSysNo] IN ({0}) OR O.[OppositeAccount] IN ({0})) OR (A.SubjectId IN (SELECT [SubjectId] FROM [AccountInfo] WHERE [SysNo] IN ({0})) AND A.AccountType = 1 AND OrderBizType = 2))", string.Join(",", accounts));
                var result = ExecuteQueryForPaginatedList<TradeInfo>("[OrderInfo] O JOIN [AccountInfo] A ON O.[AccountSysNo] = A.[SysNo] JOIN [Case] C ON A.[SubjectId] = C.[CaseId] JOIN [TradeInfo] T ON O.[OrderNo] = T.[OrderNo]",
                    @"C.[CaseId],C.[TenementAddress],A.[AccountType],O.[PaymentChannel],O.[OrderType],O.[OrderBizType],T.*",
                    whereStr.ToString(), "O.[CreateDate] DESC", pageIndex, pageSize, out totalCount);
                Log.Dao(string.Format("OrderInfo.GetPaginatedList({0}),{1}", whereStr, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("OrderInfo.GetPaginatedList({0}),{1}", whereStr, e));
                throw;
            }
        }
    }
}
