﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.DataAccess
{
    public class CustomerDataAccess : BaseDataAccess<CustomerInfo>, ICustomerDataAccess
    {
        public override bool Insert(CustomerInfo entity)
        {
            throw new NotImplementedException();
        }
        public bool Insert(CustomerInfo customer, string loginName = null, string password = null)
        {
            try
            {
                customer.SysNo = Sequence.Get();
                long? userSysNo = null;
                if (!string.IsNullOrWhiteSpace(customer.IdentityNo))
                    userSysNo = ServiceDepository.UserDataAccess.GetSysNoByIdentityNo(customer.CertificateType, customer.IdentityNo);

                TransactionManager.Instance.BeginTransaction();

                if (userSysNo == null && !string.IsNullOrWhiteSpace(loginName))
                {
                    userSysNo = Sequence.Get();
                    if (!ServiceDepository.UserAccountDataAccess.Insert(new UserAccountInfo
                    {
                        CreateUserSysNo = customer.CreateUserSysNo,
                        LoginId = loginName,
                        PassWord = password,
                        SysNo = userSysNo.Value,
                        UserType = UserType.Customer,
                        AuthDisabled = true
                    }))
                    {
                        TransactionManager.Instance.RollBackTransaction();
                        return false;
                    }
                }

                customer.UserSysNo = userSysNo ?? Sequence.Get();

                ExecuteInsert("CustomerInfo.Insert", customer);

                TransactionManager.Instance.CommitTransaction();
                return true;
            }
            catch (Exception e)
            {
                if (TransactionManager.Instance.IsTransactionStarted)
                    TransactionManager.Instance.RollBackTransaction();
                Log.Error(e);
                throw;
            }
        }

        public bool Create(CustomerInfo entity, UserIdentityInfo identity)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                if (entity == null || identity == null)
                    return false;
                
                entity.SysNo = Sequence.Get();
                if (string.IsNullOrWhiteSpace(entity.IdentityNo))
                { 
                    entity.CertificateType = identity.CertificateType;
                    entity.IdentityNo = identity.Id;
                }
                long? userSysNo = ServiceDepository.UserDataAccess.GetSysNoByIdentityNo(entity.CertificateType, entity.IdentityNo);
                entity.UserSysNo = userSysNo ?? Sequence.Get();

                var identityExists = ServiceDepository.UserIdentityDataAccess.Get(identity.Id);
                TransactionManager.Instance.BeginTransaction();
                ExecuteInsert("CustomerInfo.Insert", entity);
                if (identityExists != null && !identityExists.IsTrusted)
                {
                    identity.ModifyDate = DateTime.Now;
                    identity.ModifyUserSysNo = entity.CreateUserSysNo;
                    ExecuteUpdate("UserIdentityInfo.Update", identity);
                }
                else if(identityExists == null)
                {
                    ExecuteInsert("UserIdentityInfo.Insert", identity);
                }
                TransactionManager.Instance.CommitTransaction();
                Log.Dao(string.Format("Dao -> CustomerInfo.Create({0}),{1}", entity.SysNo, DateTime.Now.Ticks - ts));
            }
            catch (Exception e)
            {
                if (TransactionManager.Instance.IsTransactionStarted)
                    TransactionManager.Instance.RollBackTransaction();
                Log.Error(string.Format("Dao -> CustomerInfo.Create({0}),Exception:{1}", entity.SysNo, e));
                throw;
            }
            return true;
        }

        public IList<CustomerInfo> Get(string userId)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForList("CustomerInfo.GetByUserId", userId);
                Log.Dao(string.Format("Statement : CustomerInfo.GetByUserId({0}),{1}", userId, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> CustomerInfo.GetByUserId({0}),Exception:{1}", userId, e));
                throw;
            }
        }

        public IList<CustomerInfo> GetByUserSysNo(long userSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForList("CustomerInfo.GetByUserSysNo", userSysNo);
                Log.Dao(string.Format("Statement : CustomerInfo.GetByUserSysNo({0}),{1}", userSysNo, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> CustomerInfo.GetByUserSysNo({0}),Exception:{1}", userSysNo, e));
                throw;
            }
        }


        public string GetRealName(long sysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForAnyObject("CustomerInfo.GetRealName", sysNo);
                Log.Dao(string.Format("Statement : CustomerInfo.GetRealName({0}),{1}", sysNo, DateTime.Now.Ticks - ts));
                return result as string;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> CustomerInfo.GetRealName({0}),Exception:{1}", sysNo, e));
                throw;
            }
        }

        public CustomerInfo GetWithAddress(long sysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForObject("CustomerInfo.GetWithAddress", sysNo);
                Log.Dao(string.Format("Statement : CustomerInfo.GetWithAddress({0}),{1}", sysNo, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> CustomerInfo.GetWithAddress({0}),Exception:{1}", sysNo, e));
                throw;
            }
        }

        public IList<CustomerInfo> GetPaginatedList(string condition, CustomerType? customerType, int pageIndex, int pageSize, out int totalCount, string orderBy = "C.[CreateDate] DESC", ConditionMatchingType matchingType = ConditionMatchingType.Fuzzy)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                StringBuilder whereStr = new StringBuilder("C.[IsDeleted] = 0");
                string matchingStr = matchingType.GetDescription();
                string value = matchingType == ConditionMatchingType.Fuzzy || matchingType == ConditionMatchingType.FullFuzzy ? condition : string.Format("'{0}'", condition);
                if (!string.IsNullOrEmpty(condition))
                {
                    //whereStr.Append(" AND (");
                    //whereStr.AppendFormat(matchingStr, "[LoginId]", value);
                    //whereStr.Append(" OR ");
                    whereStr.AppendFormat(matchingStr, "[RealName]", value);
                    //whereStr.Append(") ");
                }
                if (customerType != null)
                {
                    whereStr.Append(string.Format(" AND [CustomerType] = {0}", (int)customerType.Value));
                }
                var result = ExecuteQueryForPaginatedList("[UserInfo] U JOIN [Customer] C ON U.[SysNo] = C.[UserSysNo]",
                    @"U.[UserId],U.[RealName],U.[Mobile],U.[TelPhone],U.[Email],U.[Gender],U.[CertificateType],U.[IdentityNo],U.[FingerprintCount],
                    C.[CustomerType],C.[SysNo],C.[UserSysNo],C.[CreateUserSysNo],C.[CreateDate],C.[ModifyDate],C.[ModifyUserSysNo],C.[IsDeleted]",
                    whereStr.ToString(), orderBy, pageIndex, pageSize, out totalCount);
                Log.Dao(string.Format("CustomerInfo.GetPaginatedList({0}),{1}", condition, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("CustomerInfo.GetPaginatedList({0}),{1}", condition, e));
                throw;
            }
        }

        public IList<string> GetCustomerNames(IEnumerable<string> sysnos)
        {
            var dic = new Dictionary<string, string>
            {
                {"SysNo", string.Join(",", sysnos)},
            };
            return ExecuteQueryForList<string>("CustomerInfo.GetCustomers", dic);
        }

        public IList<CustomerInfo> GetCustomerByCase(string caseId)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForList("CustomerInfo.GetByCaseId", caseId);
                Log.Dao(string.Format("Statement : CustomerInfo.GetByCaseId({0}),{1}", caseId, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> CustomerInfo.GetByCaseId({0}),Exception:{1}", caseId, e));
                throw;
            }
        }

        public IList<CustomerInfo> GetBySysNos(IEnumerable<long> sysNos)
        {
            var ts = DateTime.Now.Ticks;
            if (sysNos == null || sysNos.Count() == 0)
                return null;
            string sysNoStr = string.Join(",", sysNos);
            try
            {
                var result = ExecuteQueryForList("CustomerInfo.GetBySysNos", sysNoStr);
                Log.Dao(string.Format("Statement : CustomerInfo.GetBySysNos({0}),{1}", sysNoStr, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> CustomerInfo.GetBySysNos({0}),Exception:{1}", sysNoStr, e));
                throw;
            }
        }

        public bool PrefectCustomer(long sysNo, string email, string mobile,long operatorSysNo)
        {
            if (email == null && mobile == null )
                 return false;
             Dictionary<string, object> parameters = new Dictionary<string, object>();
             parameters.Add("SysNo", sysNo);
             parameters.Add("ModifyUserSysNo", operatorSysNo);
             if (email != null)
                 parameters.Add("Email", email);
             if (mobile != null)
                 parameters.Add("Mobile", mobile);
             return ExecuteUpdate("CustomerInfo.PrefectCustomer", parameters) > 0;
        }


        public IList<CustomerInfo> GetByMobile(string mobile)
        {
            return ExecuteQueryForList("CustomerInfo.GetByMobile", mobile);
        }

        public IList<CustomerInfo> GetByIdentityNo(string identityNo)
        {
            return ExecuteQueryForList("CustomerInfo.GetByIdentityNo", identityNo);
        }
    }
}
