﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.DataAccess
{
    public class BillDataAccess : BaseDataAccess<BillInfo>, IBillDataAccess
    {
        public IList<BillQueryInfo> GetBillByOrder(string orderNo, BillCategory? category, BillType? type, bool? isBilled = null)
        {
            if (string.IsNullOrWhiteSpace(orderNo))
                return null;
            var parameters = new Dictionary<string, object>();
            parameters.Add("OrderNo", orderNo);
            if (category != null)
                parameters.Add("Category", category);
            if (type != null)
                parameters.Add("Type", type);
            if (isBilled != null)
                parameters.Add("IsBilled", isBilled);
            return ExecuteQueryForList<BillQueryInfo>("BillInfo.GetByOrder", parameters);
        }
    }
}
