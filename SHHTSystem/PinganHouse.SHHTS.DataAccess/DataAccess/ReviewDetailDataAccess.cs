﻿using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.DataAccess
{
    public class ReviewDetailDataAccess : BaseDataAccess<ReviewDetail>, IReviewDetailDataAccess
    {

        public override bool Insert(ReviewDetail entity)
        {
            entity.SysNo = Sequence.Get();
            return base.Insert(entity);
        }
        public IList<ReviewDetail> GetValidReviewDetail(long reviewSysNo)
        {
            return ExecuteQueryForList("ReviewDetail.GetValidReviewDetail", reviewSysNo);
        }

        public ReviewDetail GetLastValidReviewDetail(long reviewSysNo)
        {
            return ExecuteQueryForObject("ReviewDetail.GetLastValidReviewDetail", reviewSysNo);
        }
    }
}
