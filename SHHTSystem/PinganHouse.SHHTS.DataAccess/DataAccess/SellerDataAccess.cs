﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;

namespace PinganHouse.SHHTS.DataAccess
{
    public class SellerDataAccess : BaseDataAccess<SellerInfo>, ISellerDataAccess
    {
        public SellerInfo Get(string caseId)
        {
            return ExecuteQueryForObject("SellerInfo.GetByCaseId", caseId);
        }
    }
}
