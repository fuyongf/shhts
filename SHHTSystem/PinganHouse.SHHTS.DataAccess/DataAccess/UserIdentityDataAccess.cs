﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.DataAccess
{
    public class UserIdentityDataAccess : BaseSqlMapDao<UserIdentityInfo>, IUserIdentityDataAccess
    {
        public bool Insert(UserIdentityInfo identity)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                if (identity == null)
                    return false;
                ExecuteInsert("UserIdentityInfo.Insert", identity);
                Log.Dao(string.Format("Dao -> UserIdentityInfo.Insert({0}),{1}", identity.Id, DateTime.Now.Ticks - ts));
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> UserIdentityInfo.Insert({0}),Exception:{1}", identity.Id, e));
                throw;
            }
            return true;
        }

        public bool Update(UserIdentityInfo identity)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                if (identity == null)
                    return false;
                ExecuteUpdate("UserIdentityInfo.Update", identity);
                Log.Dao(string.Format("Dao -> UserIdentityInfo.Update({0}),{1}", identity.Id, DateTime.Now.Ticks - ts));
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> UserIdentityInfo.Update({0}),Exception:{1}", identity, e));
                throw;
            }
            return true;
        }

        public bool Remove(string id)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                if (string.IsNullOrEmpty(id))
                    return false;
                ExecuteDelete("UserIdentityInfo.Remove", id);
                Log.Dao(string.Format("Dao -> UserIdentityInfo.Remove({0}),{1}", id, DateTime.Now.Ticks - ts));
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> UserIdentityInfo.Remove({0}),Exception:{1}", id, e));
                throw;
            }
            return true;
        }

        public UserIdentityInfo Get(string id)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                if (string.IsNullOrEmpty(id))
                    return null;
                var result = ExecuteQueryForObject("UserIdentityInfo.Get", id);
                Log.Dao(string.Format("Dao -> UserIdentityInfo.Get({0}),{1}", id, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> UserIdentityInfo.Get({0}),Exception:{1}", id, e));
                throw;
            }
        }

        public bool Exists(string id)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                if (string.IsNullOrEmpty(id))
                    return false;
                var result = ExecuteQueryForAnyObject("UserIdentityInfo.Exists", id);
                Log.Dao(string.Format("Dao -> UserIdentityInfo.Exists({0}),{1}", id, DateTime.Now.Ticks - ts));
                return result != null;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> UserIdentityInfo.Exists({0}),Exception:{1}", id, e));
                throw;
            }
        }
    }

    public class FingerprintDataAccess : BaseDataAccess<UserFingerprintInfo>, IFingerprintDataAccess 
    {
        public IList<UserFingerprintInfo> GetByUserSysNo(long userSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForList("UserFingerprintInfo.GetByUserSysNo", userSysNo);
                Log.Dao(string.Format("Dao -> UserFingerprintInfo.GetByUserSysNo({0}),{1}", userSysNo, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> UserFingerprintInfo.GetByUserSysNo({0}),Exception:{1}", userSysNo, e));
                throw;
            }
        }

        public IList<UserFingerprintInfo> GetAll()
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForList("UserFingerprintInfo.GetAll", "");
                Log.Dao(string.Format("Dao -> UserFingerprintInfo.GetAll({0}),{1}", "", DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> UserFingerprintInfo.GetAll({0}),Exception:{1}", "", e));
                throw;
            }
        }
    }
}
