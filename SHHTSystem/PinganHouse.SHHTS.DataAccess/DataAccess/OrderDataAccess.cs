﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.DataAccess
{
    public class OrderDataAccess : BaseDataAccess<OrderInfo>, IOrderDataAccess
    {
        public IList<OrderInfo> GetOrders(AccountType accountType, string accountSubjectId, OrderType? orderType, OrderBizType? tradeType, DateTime? startDate, DateTime? endDate)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var parameters = new Dictionary<string, object>();
                parameters.Add("AccountType", accountType);
                parameters.Add("SubjectId", accountSubjectId);

                if (orderType != null)
                    parameters.Add("OrderType", orderType);
                if (tradeType != null)
                    parameters.Add("OrderBizType", tradeType);
                if (startDate != null)
                    parameters.Add("StartDate", startDate);
                if (endDate != null)
                    parameters.Add("EndDate", endDate);

                var result = ExecuteQueryForList("OrderInfo.GetOrders", parameters);
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "OrderInfo.GetOrders", accountSubjectId, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "OrderInfo.GetOrders", accountSubjectId, e));
                throw;
            }
        }

        public IList<OrderInfo> GetOrderByAccount(long accountSysNo, DateTime? startDate, DateTime? endDate)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var parameters = new Dictionary<string, object>();
                parameters.Add("AccountSysNo", accountSysNo);

                if (startDate != null)
                    parameters.Add("StartDate", startDate);
                if (endDate != null)
                    parameters.Add("EndDate", endDate);

                var result = ExecuteQueryForList("OrderInfo.GetByAccount", parameters);
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "OrderInfo.GetByAccount", accountSysNo, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "OrderInfo.GetByAccount", accountSysNo, e));
                throw;
            }
        }

        public IList<OrderQueryInfo> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, string caseId, AccountType? accountType, OrderType? orderType, PaymentChannel? channel, string orderNo, string createUserName, DateTime? startDate, DateTime? endDate, string orderBy = "O.[CreateDate] DESC")
        {
            var ts = DateTime.Now.Ticks;
            StringBuilder whereStr = new StringBuilder(string.Format("(C.[ReceptionCenter] = {0} OR C.GlobalFlag=1)", (int)center));
            try
            {
                if (!string.IsNullOrWhiteSpace(orderNo))
                {
                    whereStr.AppendFormat(" AND O.[OrderNo] = '{0}'", orderNo);
                }
                if (!string.IsNullOrWhiteSpace(caseId))
                {
                    whereStr.AppendFormat(" AND C.[CaseId] = '{0}'", caseId);
                }
                if (orderType != null)
                {
                    whereStr.Append(string.Format(" AND O.[OrderType] = {0}", (int)orderType.Value));
                }
                if (channel != null)
                {
                    whereStr.AppendFormat(" AND O.[PaymentChannel] = {0}", (int)channel.Value);
                }
                if (accountType != null)
                {
                    whereStr.AppendFormat(" AND A.[AccountType] = {0}", (int)accountType.Value);
                }
                if (startDate != null)
                {
                    whereStr.AppendFormat(" AND O.[CreateDate] >= '{0}'", startDate.Value.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                }
                if (endDate != null)
                {
                    whereStr.AppendFormat(" AND O.[CreateDate] <= '{0}'", endDate.Value.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                }
                if (!string.IsNullOrWhiteSpace(createUserName))
                {
                    whereStr.AppendFormat(" AND U.[RealName] LIKE '%{0}%'", createUserName);
                }

                var result = ExecuteQueryForPaginatedList<OrderQueryInfo>("[OrderInfo] O JOIN [AccountInfo] A ON O.[AccountSysNo] = A.[SysNo] JOIN [Case] C ON A.[SubjectId] = C.[CaseId] LEFT JOIN [UserInfo] U ON O.[CreateUserSysNo] = U.[SysNo]",
                    @"C.[CaseId],C.[TenementAddress],U.[RealName] AS [CreateUserName],(SELECT SUM([Amount]) FROM [TradeInfo] WHERE [OrderNo] = O.[OrderNo] AND [Status] = 3) AS CollectedBalance,A.[AccountType],O.*",
                    whereStr.ToString(), orderBy, pageIndex, pageSize, out totalCount);
                Log.Dao(string.Format("OrderInfo.GetPaginatedList({0}),{1}", whereStr, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("OrderInfo.GetPaginatedList({0}),{1}", whereStr, e));
                throw;
            }
        }

        public IList<OrderQueryInfo> GetPaginatedListByAccount(int pageIndex, int pageSize, out int totalCount, bool isSelf, params long[] accounts)
        {
            totalCount = 0;
            if (accounts == null || accounts.Count() == 0)
                return null;
            var ts = DateTime.Now.Ticks;
            StringBuilder whereStr = new StringBuilder("O.[IsDeleted] = 0 ");
            try
            {
                if (isSelf)
                    whereStr.AppendFormat(" AND (((O.[OrderType] = 0 OR O.[OrderType] = 4) AND O.[AccountSysNo] IN ({0})) OR O.[OppositeAccount] IN ({0}))", string.Join(",", accounts));
                else
                    whereStr.AppendFormat(" AND ((O.[AccountSysNo] IN ({0}) OR O.[OppositeAccount] IN ({0})) OR (A.SubjectId IN (SELECT [SubjectId] FROM [AccountInfo] WHERE [SysNo] IN ({0})) AND A.AccountType = 1 AND OrderBizType = 2))", string.Join(",", accounts));
                var result = ExecuteQueryForPaginatedList<OrderQueryInfo>("[OrderInfo] O JOIN [AccountInfo] A ON O.[AccountSysNo] = A.[SysNo] JOIN [Case] C ON A.[SubjectId] = C.[CaseId] LEFT JOIN [UserInfo] U ON O.[CreateUserSysNo] = U.[SysNo]",
                    @"C.[CaseId],C.[TenementAddress],U.[RealName] AS [CreateUserName],(SELECT SUM([Amount]) FROM [TradeInfo] WHERE [OrderNo] = O.[OrderNo] AND [Status] = 3) AS CollectedBalance,A.[AccountType],O.*",
                    whereStr.ToString(), "O.[CreateDate] DESC", pageIndex, pageSize, out totalCount);
                Log.Dao(string.Format("OrderInfo.GetPaginatedList({0}),{1}", whereStr, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("OrderInfo.GetPaginatedList({0}),{1}", whereStr, e));
                throw;
            }
        }

        //public IList<OrderQueryInfo> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, string caseId = null, string orderNo = null, DateTime? startDate = null, DateTime? endDate = null, IDictionary<string, object> data = null)
        //{
        //    string table = " [OrderInfo] O JOIN [AccountInfo] A ON O.[AccountSysNo] = A.[SysNo] JOIN [Case] C ON A.[SubjectId] = C.[CaseId] LEFT JOIN [UserInfo] U ON O.[CreateUserSysNo] = U.[SysNo] ";
        //    string fields = " C.[CaseId],U.[RealName] AS [CreateUserName],(SELECT SUM([Amount]) FROM [TradeInfo] WHERE [OrderNo] = O.[OrderNo] AND [Status] = 3) AS CollectedBalance,O.* ";
        //    StringBuilder whereStr = new StringBuilder(string.Format(" C.[ReceptionCenter] = {0} and O.IsDeleted=0", (int)center));
        //    if ((startDate != null && startDate.Value != default(DateTime)) || (endDate != null && endDate.Value != default(DateTime)))
        //    {
        //        whereStr.Append(" and  O.ORDERNO in ( SELECT ORDERNO from ReceiptInfo where IsDeleted=0 ");
        //        if (!string.IsNullOrWhiteSpace(orderNo))
        //        {
        //            whereStr.AppendFormat(" and ORDERNO='{0}'", orderNo);
        //        }
        //        whereStr.Append(" and  ReceiptNo in( SELECT ReceiptNo FROM ReceiptSerialInfo WHERE ISDELETED=0 ");
        //        if (startDate != null && startDate.Value != default(DateTime))
        //            whereStr.AppendFormat("and CreateDate >= '{0}'", startDate.Value.Date);
        //        if (endDate != null && endDate.Value != default(DateTime))
        //            whereStr.AppendFormat("and CreateDate < '{0}'", endDate.Value.AddDays(1).Date);
        //        whereStr.Append(" )) ");
        //    }
        //    else if (!string.IsNullOrWhiteSpace(orderNo))
        //    {
        //        whereStr.AppendFormat(" and O.ORDERNO='{0}'", orderNo);
        //    }
        //    if (!string.IsNullOrWhiteSpace(caseId))
        //    {
        //        whereStr.AppendFormat(" and  C.CASEID={0} ", caseId);
        //    }
        //    return ExecuteQueryForPaginatedList<OrderQueryInfo>(table, fields, whereStr.ToString(), "O.CreateDate desc", pageIndex, pageSize, out totalCount);
        //}

        public IList<RefundOrderQueryInfo> GetRefundPaginatedList(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, string orderNo, string createUserName, DateTime? startDate, DateTime? endDate)
        {
            var ts = DateTime.Now.Ticks;
            StringBuilder whereStr = new StringBuilder(string.Format("(C.[ReceptionCenter] = {0} OR C.GlobalFlag=1) AND O.[OrderType] = 0  AND (O.[OrderStatus] = 4 OR O.[OrderStatus] = 5) ", (int)center));
            try
            {
                if (!string.IsNullOrWhiteSpace(orderNo))
                {
                    whereStr.AppendFormat(" AND O.[OrderNo] = '{0}'", orderNo);
                }
                if (startDate != null)
                {
                    whereStr.AppendFormat(" AND O.[CreateDate] >= '{0}'", startDate.Value.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                }
                if (endDate != null)
                {
                    whereStr.AppendFormat(" AND O.[CreateDate] <= '{0}'", endDate.Value.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                }
                if (!string.IsNullOrWhiteSpace(createUserName))
                {
                    whereStr.AppendFormat(" AND U.[RealName] LIKE '%{0}%'", createUserName);
                }

                var result = ExecuteQueryForPaginatedList<RefundOrderQueryInfo>("GetRefundPaginatedList", "[OrderInfo] O JOIN [AccountInfo] A ON O.[AccountSysNo] = A.[SysNo] JOIN [Case] C ON C.[CaseId] = A.[SubjectId] JOIN [UserInfo] U ON O.[CreateUserSysNo] = U.[SysNo]",
                    @"O.[SysNo],O.[OrderNo],C.[CaseId],O.[OrderAmount],O.[CreateDate],U.[RealName] AS [CreateUserName],O.[OrderStatus],O.[Remark],O.[ModifyDate],O.[CreateUserSysNo],O.[ModifyUserSysNo],O.[IsDeleted],O.[AuditStatus],O.[OppositeAccount]",
                    whereStr.ToString(), "O.[CreateDate] DESC", pageIndex, pageSize, out totalCount);
                Log.Dao(string.Format("OrderInfo.GetRefundPaginatedList({0}),{1}", whereStr, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("OrderInfo.GetRefundPaginatedList({0}),{1}", whereStr, e));
                throw;
            }
        }

        public OrderInfo Get(string orderNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForObject("OrderInfo.GetOrder", orderNo);
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "OrderInfo.GetOrder", orderNo, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "OrderInfo.GetOrder", orderNo, e));
                throw;
            }
        }

        public bool Exists(string orderNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForAnyObject("OrderInfo.ExistsOrder", orderNo);
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "OrderInfo.ExistsOrder", orderNo, DateTime.Now.Ticks - ts));
                return result != null;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "OrderInfo.ExistsOrder", orderNo, e));
                throw;
            }
        }

        public IList<OrderInfo> GetOrdersByStatus(OrderType orderType, OrderStatus status)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForList("OrderInfo.GetOrdersByStatus", new Dictionary<string, object> { { "OrderType", orderType }, { "OrderStatus", status } });
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "OrderInfo.GetOrdersByStatus", status, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "OrderInfo.GetOrdersByStatus", status, e));
                throw;
            }
        }

        public ReceptionCenter GetReceptionCenterByOrder(string orderNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForAnyObject("OrderInfo.GetReceptionCenterByOrder", orderNo);
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "OrderInfo.GetReceptionCenterByOrder", orderNo, DateTime.Now.Ticks - ts));
                return (ReceptionCenter)int.Parse(result.ToString());
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "OrderInfo.GetReceptionCenterByOrder", orderNo, e));
                throw;
            }
        }


        public bool InsertPaymentExtend(PaymentOrderExtend extend)
        {
            try
            {
                ExecuteInsert("OrderInfo.InsertPaymentExtend", extend);
                return true;
            }
            catch (Exception e) 
            {
                Log.Error(e);
                return false;
            }
        }

        public PaymentOrderExtend GetPaymentExtend(string orderNo)
        {
            try
            {
                var result = ExecuteQueryForAnyObject("OrderInfo.GetPaymentExtend", orderNo);
                return result as PaymentOrderExtend;
            }
            catch (Exception e) 
            {
                Log.Error(e);
                throw;
            }
        }


        public bool InsertPaidOffExtend(PaidOffOrderExtend extend)
        {
            try
            {
                ExecuteInsert("OrderInfo.InsertPaidOffExtend", extend);
                return true;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return false;
            }
        }

        public PaidOffOrderExtend GetPaidOffExtend(string orderNo)
        {
            try
            {
                var result = ExecuteQueryForAnyObject("OrderInfo.GetPaidOffExtend", orderNo);
                return result as PaidOffOrderExtend;
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }

        public bool InsertTransferExtend(TransferOrderExtend extend)
        {
            try
            {
                ExecuteInsert("OrderInfo.InsertTransferExtend", extend);
                return true;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return false;
            }
        }

        public TransferOrderExtend GetTransferExtend(string orderNo)
        {
            try
            {
                var result = ExecuteQueryForAnyObject("OrderInfo.GetTransferExtend", orderNo);
                return result as TransferOrderExtend;
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }

        public bool InsertWithdrawExtend(WithdrawOrderExtend extend)
        {
            try
            {
                ExecuteInsert("OrderInfo.InsertWithdrawExtend", extend);
                return true;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return false;
            }
        }

        public WithdrawOrderExtend GetWithdrawExtend(string orderNo)
        {
            try
            {
                var result = ExecuteQueryForAnyObject("OrderInfo.GetWithdrawExtend", orderNo);
                return result as WithdrawOrderExtend;
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }
    }
}
