﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.DataAccess
{
    public class AgentStaffDataAccess : BaseDataAccess<AgentStaffInfo>, IAgentStaffDataAccess
    {
        public override bool Insert(AgentStaffInfo entity)
        {
            throw new NotImplementedException();
        }
        public bool Insert(AgentStaffInfo agentStaff, string loginName = null, string password = null)
        {
            try
            {
                agentStaff.SysNo = Sequence.Get();
                long? userSysNo = null;
                if (!string.IsNullOrWhiteSpace(agentStaff.IdentityNo))
                    userSysNo = ServiceDepository.UserDataAccess.GetSysNoByIdentityNo(agentStaff.CertificateType, agentStaff.IdentityNo);

                TransactionManager.Instance.BeginTransaction();

                if (userSysNo == null && !string.IsNullOrWhiteSpace(loginName))
                {
                    userSysNo = Sequence.Get();
                    if (!ServiceDepository.UserAccountDataAccess.Insert(new UserAccountInfo
                    {
                        CreateUserSysNo = agentStaff.CreateUserSysNo,
                        LoginId = loginName,
                        PassWord = password,
                        SysNo = userSysNo.Value,
                        UserType = UserType.AgentStaff,
                        AuthDisabled = !agentStaff.IsManager
                    }))
                    {
                        TransactionManager.Instance.RollBackTransaction();
                        return false;
                    }
                }

                agentStaff.UserSysNo = userSysNo ?? Sequence.Get();

                ExecuteInsert("AgentStaffInfo.Insert", agentStaff);

                TransactionManager.Instance.CommitTransaction();
                return true;
            }
            catch (Exception e)
            {
                TransactionManager.Instance.RollBackTransaction();
                Log.Error(e);
                throw;
            }
        }

        public IList<AgentStaffInfo> Get(string userId)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForList("AgentStaffInfo.GetByUserId", userId);
                Log.Dao(string.Format("Statement : AgentStaffInfo.GetByUserId({0}),{1}", userId, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> AgentStaffInfo.GetByUserId({0}),Exception:{1}", userId, e));
                throw;
            }
        }

        public IList<AgentStaffInfo> GetPaginatedList(string condition, long? agentCompanySysNo, bool? isManager, int pageIndex, int pageSize, out int totalCount, string identityNo = null, string name = null, string mobile = null, string orderBy = "S.[CreateDate] DESC", ConditionMatchingType matchingType = ConditionMatchingType.Fuzzy)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                StringBuilder whereStr = new StringBuilder("S.[IsDeleted] = 0");
                if (!string.IsNullOrEmpty(condition))
                {
                    string matchingStr = matchingType.GetDescription();
                    string value = matchingType == ConditionMatchingType.Fuzzy || matchingType == ConditionMatchingType.FullFuzzy ? condition : string.Format("'{0}'", condition);
                    whereStr.Append(" AND (");
                    //whereStr.AppendFormat(matchingStr, "[LoginId]", value);
                    //whereStr.Append(" OR ");
                    whereStr.AppendFormat(matchingStr, "[RealName]", value);
                    //whereStr.Append(" OR ");
                    //whereStr.AppendFormat(matchingStr, "[IdentityNo]", value);
                    whereStr.Append(" OR ");
                    whereStr.AppendFormat(matchingStr, "[Mobile]", value);
                    whereStr.Append(" OR ");
                    whereStr.AppendFormat(matchingStr, "[Mobile1]", value);
                    whereStr.Append(") ");
                }
                if (agentCompanySysNo != null)
                {
                    whereStr.Append(string.Format(" AND [AgentCompanySysNo] = {0}", agentCompanySysNo.Value));
                }
                if (isManager != null)
                {
                    whereStr.Append(string.Format(" AND [IsManager] = {0}", isManager.Value ? "1" : "0"));
                }
                if (!string.IsNullOrEmpty(identityNo))
                {
                    whereStr.AppendFormat(" AND [IdentityNo] = '{0}'", identityNo);
                }
                if (!string.IsNullOrEmpty(name))
                {
                    whereStr.AppendFormat(" AND [RealName] = '{0}'", name);
                }
                if (!string.IsNullOrEmpty(mobile))
                {
                    whereStr.AppendFormat(" AND ([Mobile] = '{0}' OR [Mobile1] = '{0}')", mobile);
                }
                var result = ExecuteQueryForPaginatedList("[UserInfo] U JOIN [AgentStaff] S ON U.[SysNo] = S.[UserSysNo]",
                    @"U.[UserId],U.[RealName],U.[Mobile],U.[TelPhone],U.[Email],U.[Gender],U.[CertificateType],U.[IdentityNo],U.[FingerprintCount],
                    S.[AgentCompanySysNo],S.[IsManager],S.[Mobile1],S.[SysNo],S.[UserSysNo],S.[CreateUserSysNo],S.[CreateDate],S.[ModifyDate],S.[ModifyUserSysNo],S.[IsDeleted]",
                    whereStr.ToString(), orderBy, pageIndex, pageSize, out totalCount);
                Log.Dao(string.Format("AgentStaffInfo.GetPaginatedList({0}),{1}", condition, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("AgentStaffInfo.GetPaginatedList({0}),{1}", condition, e));
                throw;
            }
        }

        public IList<AgentStaffPaginatedInfo> GetPaginatedListWithCompany(string condition, long? agentCompanySysNo, bool? isManager, int pageIndex, int pageSize, out int totalCount, string identityNo = null, string name = null, string mobile = null, string orderBy = "S.[CreateDate] DESC", ConditionMatchingType matchingType = ConditionMatchingType.Fuzzy)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                StringBuilder whereStr = new StringBuilder("S.[IsDeleted] = 0");
                if (!string.IsNullOrEmpty(condition))
                {
                    string matchingStr = matchingType.GetDescription();
                    string value = matchingType == ConditionMatchingType.Fuzzy || matchingType == ConditionMatchingType.FullFuzzy ? condition : string.Format("'{0}'", condition);
                    whereStr.Append(" AND (");
                    //whereStr.AppendFormat(matchingStr, "[LoginId]", value);
                    //whereStr.Append(" OR ");
                    whereStr.AppendFormat(matchingStr, "U.[RealName]", value);
                    //whereStr.Append(" OR ");
                    //whereStr.AppendFormat(matchingStr, "[IdentityNo]", value);
                    whereStr.Append(" OR ");
                    whereStr.AppendFormat(matchingStr, "U.[Mobile]", value);
                    whereStr.Append(" OR ");
                    whereStr.AppendFormat(matchingStr, "S.[Mobile1]", value);
                    whereStr.Append(" OR ");
                    whereStr.AppendFormat(matchingStr, "C.[CompanyName]", value);
                    whereStr.Append(" OR ");
                    whereStr.AppendFormat(matchingStr, "C.[Address]", value);
                    whereStr.Append(") ");
                }
                if (agentCompanySysNo != null)
                {
                    whereStr.Append(string.Format(" AND S.[AgentCompanySysNo] = {0}", agentCompanySysNo.Value));
                }
                if (isManager != null)
                {
                    whereStr.Append(string.Format(" AND S.[IsManager] = {0}", isManager.Value ? "1" : "0"));
                }
                if (!string.IsNullOrEmpty(identityNo))
                {
                    whereStr.AppendFormat(" AND U.[IdentityNo] = '{0}'", identityNo);
                }
                if (!string.IsNullOrEmpty(name))
                {
                    whereStr.AppendFormat(" AND U.[RealName] = '{0}'", name);
                }
                if (!string.IsNullOrEmpty(mobile))
                {
                    whereStr.AppendFormat(" AND (U.[Mobile] = '{0}' OR A.[Mobile1] = '{0}')", mobile);
                }
                var result = ExecuteQueryForPaginatedList<AgentStaffPaginatedInfo>("GetPaginatedListWithCompany", "[UserInfo] U JOIN [AgentStaff] S ON U.[SysNo] = S.[UserSysNo] JOIN [AgentCompany] C ON S.[AgentCompanySysNo] = C.SysNo",
                    @"U.[UserId],U.[RealName],U.[Mobile],U.[TelPhone],U.[Email],U.[Gender],U.[CertificateType],U.[IdentityNo],U.[FingerprintCount],
                    S.[AgentCompanySysNo],S.[IsManager],S.[Mobile1],S.[SysNo],S.[UserSysNo],S.[CreateUserSysNo],S.[CreateDate],S.[ModifyDate],S.[ModifyUserSysNo],S.[IsDeleted],
                    C.[CompanyName],C.[Address] AS [CompanyAddress]",
                    whereStr.ToString(), orderBy, pageIndex, pageSize, out totalCount);
                Log.Dao(string.Format("AgentStaffInfo.GetPaginatedListWithCompany({0}),{1}", condition, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("AgentStaffInfo.GetPaginatedListWithCompany({0}),{1}", condition, e));
                throw;
            }
        }

        public string GetRealName(long sysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForAnyObject("AgentStaffInfo.GetRealName", sysNo);
                Log.Dao(string.Format("Statement : AgentStaffInfo.GetRealName({0}),{1}", sysNo, DateTime.Now.Ticks - ts));
                return result as string;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> AgentStaffInfo.GetRealName({0}),Exception:{1}", sysNo, e));
                throw;
            }
        }

        public bool ExistsByIdentityNo(CertificateType cType, string identityNo)
        {
            if (string.IsNullOrWhiteSpace(identityNo))
                return false;
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForAnyObject("AgentStaffInfo.ExistsByIdentityNo", new Dictionary<string, object> { { "CertificateType", cType }, { "IdentityNo", identityNo } });
                Log.Dao(string.Format("Statement : AgentStaffInfo.ExistsByIdentityNo({0}),{1}", identityNo, DateTime.Now.Ticks - ts));
                return result != null;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> AgentStaffInfo.ExistsByIdentityNo({0}),Exception:{1}", identityNo, e));
                throw;
            }
        }
    }
}
