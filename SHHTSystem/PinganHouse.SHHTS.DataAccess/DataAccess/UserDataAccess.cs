﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.DataAccess
{
    public class UserDataAccess : BaseDataAccess<UserInfo>, IUserDataAccess
    {
        #region Not Supported Methods
        public override bool Insert(UserInfo entity)
        {
            throw new NotSupportedException("用户基础信息不支持此操作");
        }

        public override bool Update(UserInfo entity)
        {
            throw new NotSupportedException("用户基础信息不支持此操作");
        }

        public override bool Delete(long sysNo, long operatorSysNo)
        {
            throw new NotSupportedException("用户基础信息不支持此操作");
        }

        public override bool Remove(long sysNo)
        {
            throw new NotSupportedException("用户基础信息不支持此操作");
        }
        #endregion

        public UserInfo Get(string userId)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForObject("UserInfo.GetByUserId", userId);
                Log.Dao(string.Format("Statement : UserInfo.GetByUserId({0}),{1}", userId, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> UserInfo.GetByUserId({0}),Exception:{1}", userId, e));
                throw;
            }
        }

        public string GetUserName(long sysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForAnyObject("UserInfo.GetUserName", sysNo);
                Log.Dao(string.Format("Statement : UserInfo.GetUserName({0}),{1}", sysNo, DateTime.Now.Ticks - ts));
                return result == null ? null : result.ToString();
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> UserInfo.GetUserName({0}),Exception:{1}", sysNo, e));
                throw;
            }
        }

        public IList<string> GetUserName(string UserSysNo)
        {
            return ExecuteQueryForList<string>("UserInfo.BatchGetUserName", UserSysNo);
        }
        public IList<UserInfo> GetPaginatedList(string condition, UserType? userType, int pageIndex, int pageSize, out int totalCount, string orderBy = "[CreateDate]", ConditionMatchingType matchingType = ConditionMatchingType.Fuzzy)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                StringBuilder whereStr = new StringBuilder("[IsDeleted] = 0");
                string matchingStr = matchingType.GetDescription();
                string value = matchingType == ConditionMatchingType.Fuzzy || matchingType == ConditionMatchingType.FullFuzzy ? condition : string.Format("'{0}'", condition);
                if (!string.IsNullOrEmpty(condition))
                {
                    whereStr.Append(" AND (");
                    whereStr.AppendFormat(matchingStr, "[LoginId]", value);
                    whereStr.Append(" OR ");
                    whereStr.AppendFormat(matchingStr, "[RealName]", value);
                    whereStr.Append(") ");
                }
                if (userType != null)
                {
                    whereStr.Append(string.Format(" AND [UserType] = {0}", (int)userType.Value));
                }
                var result = ExecuteQueryForPaginatedList("[UserInfo]", "*", whereStr.ToString(), orderBy, pageIndex, pageSize, out totalCount);
                Log.Dao(string.Format("UserInfo.GetPaginatedList({0}),{1}", condition, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("UserInfo.GetPaginatedList({0}),{1}", condition, e));
                throw;
            }
        }

        //public IList<UserInfo> GetBySysNos(IEnumerable<long> sysNos)
        //{
        //    var ts = DateTime.Now.Ticks;
        //    if (sysNos == null || sysNos.Count() == 0)
        //        return null;
        //    string sysNoStr = string.Join(",", sysNos);
        //    try
        //    {
        //        var result = ExecuteQueryForList("UserInfo.GetBySysNos", sysNoStr);
        //        Log.Dao(string.Format("Statement : UserInfo.GetBySysNos({0}),{1}", sysNoStr, DateTime.Now.Ticks - ts));
        //        return result;
        //    }
        //    catch (Exception e)
        //    {
        //        Log.Error(string.Format("Dao -> UserInfo.GetBySysNos({0}),Exception:{1}", sysNoStr, e));
        //        throw;
        //    }
        //}

        public bool BindIdentity(long sysNo, UserIdentityInfo identity)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                if (identity == null)
                    return false;
                var user = Get(sysNo);
                if (user == null)
                    throw new Exception(string.Format("用户:{0}不存在", sysNo));
                if (string.IsNullOrEmpty(user.IdentityNo))
                    throw new Exception("证件号码不能为空");
                user.CertificateType = identity.CertificateType;
                user.IdentityNo = identity.Id;
                user.RealName = identity.Name;
                user.Gender = identity.Gender;
                user.ModifyUserSysNo = identity.CreateUserSysNo;

                var existsIdentity = ServiceDepository.UserIdentityDataAccess.Get(identity.Id);

                TransactionManager.Instance.BeginTransaction();
                ExecuteUpdate("UserInfo.Update", user);

                if (existsIdentity != null && !existsIdentity.IsTrusted)
                {
                    identity.ModifyDate = DateTime.Now;
                    identity.ModifyUserSysNo = identity.CreateUserSysNo;
                    ExecuteUpdate("UserIdentityInfo.Update", identity);
                }
                else if (existsIdentity == null)
                {
                    ExecuteInsert("UserIdentityInfo.Insert", identity);
                }
                TransactionManager.Instance.CommitTransaction();
                Log.Dao(string.Format("Dao -> UserInfo.BindIdentity({0},{1}),{2}", sysNo, identity.Id, DateTime.Now.Ticks - ts));
            }
            catch (Exception e)
            {
                if (TransactionManager.Instance.IsTransactionStarted)
                    TransactionManager.Instance.RollBackTransaction();
                Log.Error(string.Format("Dao -> UserInfo.BindIdentity({0},{1}),Exception:{2}", sysNo, identity.Id, e));
                throw;
            }
            return true;
        }

        //public bool UpdateBaseInfo(long sysNo, string name, CertificateType? certType, string idNo, string email, string mobile, Gender? gender, long operatorSysNo)
        //{
        //    if (name == null && certType == null && idNo == null && email == null && mobile == null && gender == null)
        //        return false;
        //    Dictionary<string, object> parameters = new Dictionary<string, object>();
        //    parameters.Add("SysNo", sysNo);
        //    parameters.Add("ModifyUserSysNo", operatorSysNo);
        //    if (name != null)
        //        parameters.Add("RealName", name);
        //    if (certType != null)
        //        parameters.Add("CertificateType", certType);
        //    if (idNo != null)
        //        parameters.Add("IdentityNo", idNo);
        //    if (email != null)
        //        parameters.Add("Email", email);
        //    if (mobile != null)
        //        parameters.Add("Mobile", mobile);
        //    if (gender != null)
        //        parameters.Add("Gender", gender);
        //    return ExecuteUpdate("UserInfo.UpdateBaseInfo", parameters) > 0;
        //}

        public long? GetSysNoByIdentityNo(CertificateType type, string identityNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForAnyObject("UserInfo.GetSysNoByIdentityNo", new Dictionary<string, object> { { "Type", (int)type }, { "IdentityNo", identityNo } });
                Log.Dao(string.Format("Statement : UserInfo.GetSysNoByIdentityNo({0}),{1}", identityNo, DateTime.Now.Ticks - ts));
                return result == null ? new Nullable<long>() : (long)result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> UserInfo.GetSysNoByIdentityNo({0}),Exception:{1}", identityNo, e));
                throw;
            }
        }

        public UserInfo GetByIdentityNo(CertificateType type, string identityNo)
        {
             return ExecuteQueryForObject("UserInfo.GetByIdentityNo",
                 new Dictionary<string, object> { { "Type", (int)type }, { "IdentityNo", identityNo } });
        }
    }

    public class UserAccountDataAccess : BaseDataAccess<UserAccountInfo>, IUserAccountDataAccess
    {
        public UserType? GetUserTypeByLoginId(string loginId)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForAnyObject("UserAccountInfo.GetUserTypeByLoginId", loginId);
                Log.Dao(string.Format("Statement : UserAccountInfo.GetUserTypeByLoginId({0}),{1}", loginId, DateTime.Now.Ticks - ts));
                return result == null ? null : (UserType?)Convert.ToInt32(result);
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> UserAccountInfo.GetUserTypeByLoginId({0}),Exception:{1}", loginId, e));
                throw;
            }
        }

        public UserAccountInfo GetByLoginId(string loginId)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForObject("UserAccountInfo.GetByLoginId", loginId);
                Log.Dao(string.Format("Statement : UserAccountInfo.GetByLoginId({0}),{1}", loginId, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> UserAccountInfo.GetByLoginId({0}),Exception:{1}", loginId, e));
                throw;
            }
        }

        public bool LoginIdExists(string loginId)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForAnyObject("UserAccountInfo.LoginIdExists", loginId);
                Log.Dao(string.Format("Statement : UserAccountInfo.LoginIdExists({0}),{1}", loginId, DateTime.Now.Ticks - ts));
                return result != null;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> UserAccountInfo.LoginIdExists({0}),Exception:{1}", loginId, e));
                throw;
            }
        }

        public bool ResetLoginDate(long userSysNo, DateTime? loginDate)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteUpdate("UserAccountInfo.ResetLoginDate", new Dictionary<string, object> { { "SysNo", userSysNo }, { "LastLoginDate", loginDate ?? DateTime.Now } });
                Log.Dao(string.Format("Dao -> UserAccountInfo.ResetLoginDate({0}),Exception:{1}", userSysNo, DateTime.Now.Ticks - ts));
                return result > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> UserAccountInfo.ResetLoginDate({0}),Exception:{1}", userSysNo, e));
                throw;
            }
        }

        public bool ChangePassword(long sysNo, string newPassword)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteUpdate("UserAccountInfo.ChangePassword", new Dictionary<string, object> { { "SysNo", sysNo }, { "PassWord", newPassword } });
                Log.Dao(string.Format("Dao -> UserAccountInfo.ChangePassword({0}),Exception:{1}", sysNo, DateTime.Now.Ticks - ts));
                return result > 0;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> UserAccountInfo.ChangePassword({0}),Exception:{1}", sysNo, e));
                throw;
            }
        }
    }
}
