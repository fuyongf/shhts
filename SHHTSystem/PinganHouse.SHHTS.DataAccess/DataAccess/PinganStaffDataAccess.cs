﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.DataAccess
{
    public class PinganStaffDataAccess : BaseDataAccess<PinganStaffInfo>, IPinganStaffDataAccess
    {
        public override bool Insert(PinganStaffInfo entity)
        {
            throw new NotImplementedException();
        }
        public bool Insert(PinganStaffInfo pinganStaff, string password = null)
        {
            try
            {
                pinganStaff.SysNo = Sequence.Get();
                long? userSysNo = null;
                if (!string.IsNullOrWhiteSpace(pinganStaff.IdentityNo))
                    userSysNo = ServiceDepository.UserDataAccess.GetSysNoByIdentityNo(pinganStaff.CertificateType, pinganStaff.IdentityNo);

                TransactionManager.Instance.BeginTransaction();

                if (userSysNo == null && !string.IsNullOrWhiteSpace(pinganStaff.UMCode))
                {
                    userSysNo = Sequence.Get();
                    if (!ServiceDepository.UserAccountDataAccess.Insert(new UserAccountInfo
                    {
                        CreateUserSysNo = pinganStaff.CreateUserSysNo,
                        LoginId = pinganStaff.UMCode,
                        PassWord = password,
                        SysNo = userSysNo.Value,
                        UserType = UserType.PinganStaff,
                        AuthDisabled = false
                    }))
                    {
                        TransactionManager.Instance.RollBackTransaction();
                        return false;
                    }
                }

                pinganStaff.UserSysNo = userSysNo ?? Sequence.Get();

                ExecuteInsert("PinganStaffInfo.Insert", pinganStaff);

                TransactionManager.Instance.CommitTransaction();
                return true;
            }
            catch (Exception e)
            {
                TransactionManager.Instance.RollBackTransaction();
                Log.Error(e);
                throw;
            }
        }

        public IList<PinganStaffInfo> Get(string userId)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForList("PinganStaffInfo.GetByUserId", userId);
                Log.Dao(string.Format("Statement : PinganStaffInfo.GetByUserId({0}),{1}", userId, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> PinganStaffInfo.GetByUserId({0}),Exception:{1}", userId, e));
                throw;
            }
        }

        public PinganStaffInfo GetByUMCode(string um) 
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForObject("PinganStaffInfo.GetByUMCode", um);
                Log.Dao(string.Format("Statement : PinganStaffInfo.GetByUMCode({0}),{1}", um, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> PinganStaffInfo.GetByUMCode({0}),Exception:{1}", um, e));
                throw;
            }
        }
        public PinganStaffInfo GetByStaffCardNo(string staffCardNo)
        {
            return ExecuteQueryForObject("PinganStaffInfo.GetByStaffCardNo", staffCardNo);
        }

        public IList<PinganStaffInfo> GetPaginatedList(string condition, int pageIndex, int pageSize, out int totalCount, string orderBy = "P.[CreateDate] DESC", ConditionMatchingType matchingType = ConditionMatchingType.Fuzzy)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                StringBuilder whereStr = new StringBuilder("P.[IsDeleted] = 0");
                string matchingStr = matchingType.GetDescription();
                string value = matchingType == ConditionMatchingType.Fuzzy || matchingType == ConditionMatchingType.FullFuzzy ? condition : string.Format("'{0}'", condition);
                if (!string.IsNullOrEmpty(condition))
                {
                    //whereStr.Append(" AND (");
                    //whereStr.AppendFormat(matchingStr, "[LoginId]", value);
                    //whereStr.Append(" OR ");
                    whereStr.AppendFormat(matchingStr, "[RealName]", value);
                    //whereStr.Append(") ");
                }
                var result = ExecuteQueryForPaginatedList("[UserInfo] U JOIN [PinganStaff] P ON U.[SysNo] = P.[UserSysNo]",
                    @"U.[UserId],U.[RealName],U.[Mobile],U.[TelPhone],U.[Email],U.[Gender],U.[CertificateType],U.[IdentityNo],U.[FingerprintCount],
                    P.[ReceptionCenter],P.[Duty],P.[StaffCardNo],P.[StaffNo],P.[SysNo],P.[UserSysNo],P.[CreateUserSysNo],P.[CreateDate],P.[ModifyDate],P.[ModifyUserSysNo],P.[IsDeleted]",
                    whereStr.ToString(), orderBy, pageIndex, pageSize, out totalCount);
                Log.Dao(string.Format("PinganStaffInfo.GetPaginatedList({0}),{1}", condition, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("PinganStaffInfo.GetPaginatedList({0}),{1}", condition, e));
                throw;
            }
        }
    }
}
