﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.DataAccess
{
    public class AgentCompanyDataAccess : BaseDataAccess<AgentCompanyInfo>, IAgentCompanyDataAccess
    {
        public override bool Insert(AgentCompanyInfo entity)
        {
            entity.SysNo = Sequence.Get();
            var account = new AccountInfo
            {
                SubjectId = entity.SysNo.ToString(),
                AccountType = AccountType.Agency,
                CreateDate = DateTime.Now,
                CreateUserSysNo = entity.CreateUserSysNo,
                SysNo = Sequence.Get(),
            };
            if (!TransactionManager.Instance.IsTransactionStarted)
                TransactionManager.Instance.BeginTransaction();
            try
            {
                ExecuteInsert("AgentCompanyInfo.Insert", entity);
                ExecuteInsert("AccountInfo.Insert", account);
                if (TransactionManager.Instance.IsTransactionStarted)
                    TransactionManager.Instance.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                if (TransactionManager.Instance.IsTransactionStarted)
                    TransactionManager.Instance.RollBackTransaction();
                Log.Error(ex);
                throw;
            }
        }
        public bool AgentCancel(long agentSysNo, string cancelReson, long operatorSysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                AgentCompanyInfo company = Get(agentSysNo);
                if (company == null)
                    return false;
                company.Status = Enumerations.AgentJointStatus.Cancelled;
                company.CancelledDate = DateTime.Now;
                company.CancelReson = cancelReson;
                company.ModifyUserSysNo = operatorSysNo;

                TransactionManager.Instance.BeginTransaction();
                if (base.Update(company))
                {
                    ExecuteUpdate("AgentStaffInfo.JointCancel", agentSysNo);
                    Log.Dao(string.Format("Statement : AgentCompany.AgentCancel({0}),{1}", agentSysNo, DateTime.Now.Ticks - ts));
                    TransactionManager.Instance.CommitTransaction();
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                if (TransactionManager.Instance.IsTransactionStarted)
                    TransactionManager.Instance.RollBackTransaction();
                Log.Error(string.Format("Dao -> AgentCompany.AgentCancel({0}),Exception:{1}", agentSysNo, e));
                throw;
            }
        }
        public IList<AgentCompanyInfo> GetPaginatedList(string condition, AgentJointStatus? status, int pageIndex, int pageSize, out int totalCount, string orderBy = "[CompanyName]", ConditionMatchingType matchingType = ConditionMatchingType.FullFuzzy)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                StringBuilder whereStr = new StringBuilder("[IsDeleted] = 0");
                string matchingStr = matchingType.GetDescription();
                string value = matchingType == ConditionMatchingType.Fuzzy || matchingType == ConditionMatchingType.FullFuzzy ? condition : string.Format("'{0}'", condition);
                if (!string.IsNullOrEmpty(condition))
                {
                    whereStr.Append(" AND ");
                    whereStr.AppendFormat(matchingStr, "[CompanyName]", value);
                }
                if (status != null)
                {
                    whereStr.Append(string.Format(" AND [Status] = {0}", (int)status.Value));
                }
                var result = ExecuteQueryForPaginatedList("[AgentCompany]", "*", whereStr.ToString(), orderBy, pageIndex, pageSize, out totalCount);
                Log.Dao(string.Format("AgentCompanyInfo.GetPaginatedList({0}),{1}", condition, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("AgentCompanyInfo.GetPaginatedList({0}),{1}", condition, e));
                throw;
            }
        }
    }
}
