﻿using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.DataAccess
{
    public class PropertyCertificateDataAccess : BaseDataAccess<PropertyCertificate>, IPropertyCertificateDataAccess
    {
        public bool Exist(string tenementContract)
        {
            var result = ExecuteQueryForAnyObject("PropertyCertificate.Exist", tenementContract);
            return result != null && result.ToString().CompareTo("0") != 0;
        }

        public override bool Insert(PropertyCertificate entity)
        {
            entity.SysNo = Sequence.Get();
            return base.Insert(entity);
        }

        public PropertyCertificate Get(string tenementContract)
        {
            return ExecuteQueryForObject("PropertyCertificate.GetByTenementContract", tenementContract);
        }


        public IList<PropertyCertificate> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, string tenementContract = null, string tenementAddress = null, DateTime? startDate = null, DateTime? endDate = null, IDictionary<string, object> data = null)
        {
            StringBuilder whereStr = new StringBuilder(" IsDeleted=0 ");
            if ((startDate != null  && startDate.Value!=default(DateTime))|| (endDate != null && endDate.Value != default(DateTime)))
            {
                whereStr.Append(" and  TenementContract in ( select TenementContract from PropertySurvey where IsDeleted=0  ");
                if (!string.IsNullOrWhiteSpace(tenementContract))
                    whereStr.AppendFormat(" and TenementContract='{0}' ", tenementContract);
                else if (data != null && data.ContainsKey("CaseId"))
                {
                    whereStr.AppendFormat(" and TenementContract=(select TenementContract from [case]  where caseId='{0}' and isdeleted=0) ", data["CaseId"]);
                }
                if (startDate != null && startDate.Value != default(DateTime))
                    whereStr.AppendFormat("and SurveyDate >= '{0}'", startDate.Value.Date);
                if (endDate != null && endDate.Value != default(DateTime))
                    whereStr.AppendFormat("and SurveyDate < '{0}'", endDate.Value.AddDays(1).Date);
                whereStr.Append(" ) ");
            }
            else if (!string.IsNullOrWhiteSpace(tenementContract))
            {
                whereStr.AppendFormat(" and TenementContract='{0}'", tenementContract);
            }
            else if (data != null && data.ContainsKey("CaseId"))
            {
                whereStr.AppendFormat(" and TenementContract=(select TenementContract from [case]  where caseId='{0}' and isdeleted=0) ", data["CaseId"]);
            }
            if (!string.IsNullOrWhiteSpace(tenementAddress))
            {
                whereStr.AppendFormat(" and tenementAddress like '%{0}%'", tenementAddress);
            }
            return ExecuteQueryForPaginatedList("[PropertyCertificate]", "*", whereStr.ToString(), "CreateDate desc", pageIndex, pageSize, out totalCount);
        }
    }
}
