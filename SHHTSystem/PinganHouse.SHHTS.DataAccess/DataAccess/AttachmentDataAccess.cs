﻿using IBatisNet.DataMapper;
using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.DataAccess
{
    public class AttachmentDataAccess : BaseDataAccess<AttachmentInfo>, IAttachmentDataAccess
    {

        public override bool Insert(AttachmentInfo entity)
        {
            entity.SysNo = Sequence.Get();
            entity.FileName = string.Empty;
            return base.Insert(entity);
        }


        public IList<AttachmentInfo> GetAttachments(string condition, int pageIndex, int pageCount, ref int totalCount, AttachmentType attachmentType = AttachmentType.未知, string orderBy = "[CreateDate] desc", ConditionMatchingType matchingType = ConditionMatchingType.Fuzzy)
        {
            StringBuilder whereStr = new StringBuilder("[IsDeleted] = 0");
            if (!string.IsNullOrEmpty(condition))
            {
                string matchingStr = matchingType.GetDescription();
                string value = matchingType == ConditionMatchingType.Fuzzy || matchingType == ConditionMatchingType.FullFuzzy ? condition : string.Format("'{0}'", condition);
                whereStr.Append(" AND ");
                whereStr.AppendFormat(matchingStr, "[AssociationNo]", value);
            }
            if (attachmentType != AttachmentType.未知)
                whereStr.AppendFormat("AND AttachmentType={0}", (int)attachmentType);
            return ExecuteQueryForPaginatedList("[Attachment]", "*", whereStr.ToString(), orderBy, pageIndex, pageCount, out totalCount);
            //var param = new Dictionary<string, object> { { "AssociationNo", associationNo }, };
            //if (attachmentType != AttachmentType.Unkown)
            //    param.Add("AttachmentType", attachmentType);
            //return  ExecuteQueryForList("AttachmentInfo.GetAttachments", param) ;
        }


        public void DeleteByAssociationNo(string associationNo, string modifyUserNo)
        {
            ExecuteUpdate("AttachmentInfo.DeleteByAssociationNo", new Dictionary<string, string> { { "AssociationNo", associationNo }, { "ModifyUserNo", modifyUserNo } });
        }

        public IList<AttachmentInfo> GetAttachments(IEnumerable<string> associationNos)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var association in associationNos)
            {
                sb.AppendFormat("'{0}',", association);
            }
            return ExecuteQueryForList("AttachmentInfo.GetAttachments", sb.ToString().TrimEnd(','));
        }


        public IList<AttachmentInfo> GetAttachments(string associationNo, AttachmentType? attachmentType)
        {
            var dic = new Dictionary<string, object> { { "AssociationNo", associationNo } };
            if (attachmentType != null)
                dic["AttachmentType"] = (int)attachmentType.Value;
            return ExecuteQueryForList("AttachmentInfo.GetAttachmentsByAssociationNo", dic);
        }

        public IList<AttachmentInfo> GetAttachments(string associationNo, IEnumerable<AttachmentType> types = null)
        {
            if (string.IsNullOrWhiteSpace(associationNo))
                return null;
            var parameters = new Dictionary<string, object> 
            {
                {"AssociationNo",associationNo}
            };
            if (types != null && types.Count() > 0)
                parameters.Add("AttachmentType", string.Join(",", types.Select(t => (int)t)));
            return ExecuteQueryForList("AttachmentInfo.GetAttachmentsByAssociationNo", parameters);
        }

        public bool Delete(string fileId, long operatorSysNo)
        {
            var result = ExecuteDelete("AttachmentInfo.DeleteByFileId", new Dictionary<string, object> { { "FileId", fileId }, { "OperatorSysNo", operatorSysNo } });          
            return result > 0;
        }
    }
}
