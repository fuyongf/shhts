﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.DataTransferObjects.Filters;

namespace PinganHouse.SHHTS.DataAccess
{
    public class NotaryDataAccess : BaseDataAccess<Notary>, INotaryDataAccess
    {
        public IList<Notary> Search(NotaryFilter filter)
        {
            try
            {
                IDictionary<string, object> param = new Dictionary<string, object>();

                if (filter != null)
                {
                    if (!string.IsNullOrWhiteSpace(filter.Keywords))
                        param.Add("Keywords", filter.Keywords + "%");
                    if (!string.IsNullOrWhiteSpace(filter.Name))
                        param.Add("Name", filter.Name);
                    if (!string.IsNullOrWhiteSpace(filter.Mobile))
                        param.Add("Mobile", filter.Mobile);
                }

                return ExecuteQueryForList("Notary.Search", param);
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:({0}),Exception:{1}", "Notary.Search", e));
                throw;
            }
        }
    }
}
