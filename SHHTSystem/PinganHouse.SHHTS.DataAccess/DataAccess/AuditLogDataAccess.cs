﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Utils.Log;

namespace PinganHouse.SHHTS.DataAccess
{
    public class AuditLogDataAccess : BaseDataAccess<AuditLogInfo> , IAuditLogDataAccess
    {
        public IList<AuditLogInfo> Get(AuditObjectType type, string subject)
        {
            if (string.IsNullOrWhiteSpace(subject))
                return null;
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForList("AuditLogInfo.GetByTypeAndSubjectId", new Dictionary<string, object> { { "SubjectId", subject }, { "AuditObjectType", type } });
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "AuditLogInfo.GetByTypeAndSubjectId", subject, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "AuditLogInfo.GetByTypeAndSubjectId", subject, e));
                throw;
            }
        }

        public IList<AuditLogInfo> GetTradeAuditLogByOrder(string orderNo) 
        {
            if (string.IsNullOrWhiteSpace(orderNo))
                return null;
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForList("AuditLogInfo.GetTradeAuditLogByOrder", orderNo);
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "AuditLogInfo.GetTradeAuditLogByOrder", orderNo, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "AuditLogInfo.GetTradeAuditLogByOrder", orderNo, e));
                throw;
            }
        }
    }
}
