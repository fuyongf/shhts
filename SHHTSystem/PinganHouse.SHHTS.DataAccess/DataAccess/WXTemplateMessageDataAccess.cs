﻿using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.DataAccess
{
    public class WXTemplateMessageDataAccess : BaseDataAccess<WXTemplateMessage>, IWXTemplateMessageDataAccess
    {
        public override bool Insert(WXTemplateMessage entity)
        {
            entity.SysNo = Sequence.Get();
            return base.Insert(entity);
        }
        public bool UpdateByMsgId(string msgId, Enumerations.WXStatus status)
        {
            var result = ExecuteUpdate("WXTemplateMessage.UpdateByMsgId", new Dictionary<string, object> { { "MsgId", msgId }, { "Status",status } });
            return result > 0;
        }

        public WXTemplateMessage GetMessageByMsgId(string msgId)
        {
            return ExecuteQueryForObject("WXTemplateMessage.GetMessageByMsgId",msgId);
        }
    }
}
