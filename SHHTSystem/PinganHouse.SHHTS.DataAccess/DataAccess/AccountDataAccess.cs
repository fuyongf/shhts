﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.DataAccess
{
    public class AccountDataAccess : BaseDataAccess<AccountInfo>, IAccountDataAccess
    {
        public override bool Insert(AccountInfo entity)
        {
            entity.SysNo = Sequence.Get();
            return base.Insert(entity);
        }

        public AccountInfo Get(long sysNo, OrderBizType bizType)
        {
            return base.Get(sysNo);
        }

        public AccountInfo Get(string subjectId, AccountType accountType, OrderBizType? orderBizType)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForObject("AccountInfo.GetByTypeAndSubject", new Dictionary<string, object> { { "SubjectId", subjectId }, { "AccountType", accountType } });
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "AccountInfo.GetByTypeAndSubject", subjectId, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "AccountInfo.GetByTypeAndSubject", subjectId, e));
                throw;
            }
        }

        public decimal GetBalance(string subjectId, AccountType accountType)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForAnyObject("AccountInfo.GetBalance", new Dictionary<string, object> { { "SubjectId", subjectId }, { "AccountType", accountType } });
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "AccountInfo.GetBalance", subjectId, DateTime.Now.Ticks - ts));
                return Convert.ToDecimal(result);
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "AccountInfo.GetBalance", subjectId, e));
                throw;
            }
        }

        public bool AddOperatorLog(AccountOperationLog log)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                if (log == null)
                    return false;
                ExecuteInsert("AccountInfo.InsertLog", log);
                Log.Dao(string.Format("Dao -> Statement:{0}InsertLog({1}),{2}", "AccountInfo.", log.AccountSysNo, DateTime.Now.Ticks - ts));
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}InsertLog({1}),Exception:{2}", "AccountInfo.", log.AccountSysNo, e));
                throw;
            }
            return true;
        }

        public IList<AccountOperationLog> GetOperationLogs(long accountSysNo, DateTime? startDate, DateTime? endDate)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var parameters = new Dictionary<string, object>();
                parameters.Add("AccountSysNo", accountSysNo);
                if (startDate != null)
                    parameters.Add("StartDate", startDate);
                if (endDate != null)
                    parameters.Add("EndDate", endDate);
                var result = ExecuteQueryForAnyList("AccountInfo.GetLogs", parameters);
                Log.Dao(string.Format("Statement : {0}GetLogs({1}),{2}", "AccountInfo.", accountSysNo, DateTime.Now.Ticks - ts));
                return result as IList<AccountOperationLog>;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}GetLogs({1}),Exception:{2}", "AccountInfo.", accountSysNo, e));
                throw;
            }
        }

        public IList<AccountOperationLog> GetOperationLogs(string subjectId, AccountType accountType, DateTime? startDate, DateTime? endDate)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var parameters = new Dictionary<string, object>();
                parameters.Add("SubjectId", subjectId);
                parameters.Add("AccountType", accountType);
                if (startDate != null)
                    parameters.Add("StartDate", startDate);
                if (endDate != null)
                    parameters.Add("EndDate", endDate);
                var result = ExecuteQueryForAnyList("AccountInfo.GetLogsByTypeAndSubject", parameters);
                Log.Dao(string.Format("Statement : {0}GetLogsByTypeAndSubject({1}),{2}", "AccountInfo.", subjectId, DateTime.Now.Ticks - ts));
                return result as IList<AccountOperationLog>;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}GetLogsByTypeAndSubject({1}),Exception:{2}", "AccountInfo.", subjectId, e));
                throw;
            }
        }

        public IList<AccountLedgerInfo> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, string caseId, string tenementNo, string tenementAddr, DateTime? startDate, DateTime? endDate)
        {
            var ts = DateTime.Now.Ticks;
            StringBuilder whereStr = new StringBuilder(string.Format("(C.[ReceptionCenter] = {0} OR GlobalFlag=1)", (int)center));
            try
            {
                if (!string.IsNullOrWhiteSpace(caseId))
                {
                    whereStr.AppendFormat(" AND C.[CaseId] = '{0}'", caseId);
                }
                if (!string.IsNullOrWhiteSpace(tenementNo))
                {
                    whereStr.AppendFormat(" AND C.[TenementContract] = '{0}'", tenementNo);
                }
                if (!string.IsNullOrWhiteSpace(tenementAddr))
                {
                    whereStr.AppendFormat(" AND C.[TenementAddress] = '{0}'", tenementAddr);
                }
                if (startDate != null)
                {
                    whereStr.AppendFormat(" AND C.[CreateDate] >= '{0}'", startDate.Value.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                }
                if (endDate != null)
                {
                    whereStr.AppendFormat(" AND C.[CreateDate] <= '{0}'", endDate.Value.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                }

                var result = ExecuteQueryForPaginatedList<AccountLedgerInfo>("[Case] C",
                    @"C.[CaseId],C.[TenementContract],C.[TenementAddress],(SELECT [Balance] FROM [AccountInfo] WHERE [SubjectId] = C.[CaseId] AND [AccountType] = 1) AS BuyerBalance,(SELECT [Balance] FROM [AccountInfo] WHERE [SubjectId] = C.[CaseId] AND [AccountType] = 0) AS SellerBalance",
                    whereStr.ToString(), "C.[CreateDate] DESC", pageIndex, pageSize, out totalCount);
                Log.Dao(string.Format("AccountInfo.GetPaginatedList({0}),{1}", whereStr, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("AccountInfo.GetPaginatedList({0}),{1}", whereStr, e));
                throw;
            }
        }

        public Tuple<IDictionary<OrderType, decimal>, Dictionary<OrderType, decimal>> GetCaseTradeAmount(string caseId)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForList<CaseAccountTradeInfo>("AccountInfo.GetCaseTradeAmount", caseId);
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "AccountInfo.GetCaseTradeAmount", caseId, DateTime.Now.Ticks - ts));
                if (result == null || result.Count == 0)
                    return null;

                return new Tuple<IDictionary<OrderType, decimal>, Dictionary<OrderType, decimal>>
                    (
                    new Dictionary<OrderType, decimal> 
                    { 
                        { OrderType.Collection,  result.Where(a => a.AccountType == AccountType.Seller && a.OrderType == OrderType.Collection).Sum(a => a.Amount) },
                        { OrderType.Payment,  result.Where(a => a.AccountType == AccountType.Seller && a.OrderType == OrderType.Payment).Sum(a => a.Amount)  }
                    },
                    new Dictionary<OrderType, decimal> 
                    { 
                        { OrderType.Collection,  result.Where(a => a.AccountType == AccountType.Buyer && a.OrderType == OrderType.Collection).Sum(a => a.Amount) },
                        { OrderType.Payment,  result.Where(a => a.AccountType == AccountType.Buyer && a.OrderType == OrderType.Payment).Sum(a => a.Amount)  }
                        
                    }
                    );
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "AccountInfo.GetCaseTradeAmount", caseId, e));
                throw;
            }
        }
    }

    public class AccountDataAccessV2 : BaseDataAccess<AccountInfo>, IAccountDataAccess
    {
        public AccountDataAccessV2() : base("AccountInfoV2", null) { }

        public override bool Insert(AccountInfo entity)
        {
            entity.SysNo = Sequence.Get();
            return base.Insert(entity);
        }

        public override AccountInfo Get(long sysNo)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForObject("AccountInfoV2.GetMainAccount", sysNo);
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "AccountInfoV2.GetMainAccount", sysNo, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "AccountInfoV2.GetMainAccount", sysNo, e));
                throw;
            }
        }

        public AccountInfo Get(long sysNo, OrderBizType bizType)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForObject("AccountInfoV2.Get", new Dictionary<string, object> { { "SysNo", sysNo }, { "AccountCategory", MapAccountCategory(bizType) } });
                if (result == null)
                {
                    var exist = ExecuteQueryForAnyObject("AccountInfoV2.ExistBySysNo", sysNo);
                    if (exist != null)
                    {
                        ExecuteInsert("AccountInfoV2.CreateSubAccount", new Dictionary<string, object> { { "SysNo", Sequence.Get() }, { "AccountSysNo", sysNo }, { "AccountCategory", MapAccountCategory(bizType) }, { "CreateUserSysNo", null } });
                        result = ExecuteQueryForObject("AccountInfoV2.Get", new Dictionary<string, object> { { "SysNo", sysNo }, { "AccountCategory", MapAccountCategory(bizType) } });
                    }
                }
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "AccountInfoV2.Get", sysNo, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "AccountInfoV2.Get", sysNo, e));
                throw;
            }
        }

        public AccountInfo Get(string subjectId, AccountType accountType, OrderBizType? orderBizType)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                AccountCategory category = orderBizType == null ? AccountCategory.None : MapAccountCategory(orderBizType.Value);
                var parameters = new Dictionary<string, object> { { "SubjectId", subjectId }, { "AccountType", accountType }, { "AccountCategory", category } };

                var result = ExecuteQueryForObject("AccountInfoV2.GetByTypeAndSubject", parameters);
                if (result == null && category != AccountCategory.None)
                {
                    var exist = ExecuteQueryForAnyObject("AccountInfoV2.ExistByTypeAndSubject", new Dictionary<string, object> { { "SubjectId", subjectId }, { "AccountType", accountType }, { "AccountCategory", category } });
                    if (exist != null)
                    {
                        ExecuteInsert("AccountInfoV2.CreateSubAccount", new Dictionary<string, object> { { "SysNo", Sequence.Get() }, { "AccountSysNo", Convert.ToInt32(exist) }, { "AccountCategory", category }, { "CreateUserSysNo", null } });
                        result = ExecuteQueryForObject("AccountInfoV2.GetByTypeAndSubject", parameters);
                    }
                }
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "AccountInfoV2.GetByTypeAndSubject", subjectId, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "AccountInfoV2.GetByTypeAndSubject", subjectId, e));
                throw;
            }
        }

        public decimal GetBalance(string subjectId, AccountType accountType)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForAnyObject("AccountInfoV2.GetBalance", new Dictionary<string, object> { { "SubjectId", subjectId }, { "AccountType", accountType } });
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "AccountInfoV2.GetBalance", subjectId, DateTime.Now.Ticks - ts));
                return Convert.ToDecimal(result);
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "AccountInfoV2.GetBalance", subjectId, e));
                throw;
            }
        }

        public bool AddOperatorLog(AccountOperationLog log)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                if (log == null)
                    return false;
                ExecuteInsert("AccountInfoV2.InsertLog", log);
                Log.Dao(string.Format("Dao -> Statement:{0}InsertLog({1}),{2}", "AccountInfoV2.", log.AccountSysNo, DateTime.Now.Ticks - ts));
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}InsertLog({1}),Exception:{2}", "AccountInfoV2.", log.AccountSysNo, e));
                throw;
            }
            return true;
        }

        public IList<AccountOperationLog> GetOperationLogs(long accountSysNo, DateTime? startDate, DateTime? endDate)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var parameters = new Dictionary<string, object>();
                parameters.Add("AccountSysNo", accountSysNo);
                if (startDate != null)
                    parameters.Add("StartDate", startDate);
                if (endDate != null)
                    parameters.Add("EndDate", endDate);
                var result = ExecuteQueryForAnyList("AccountInfoV2.GetLogs", parameters);
                Log.Dao(string.Format("Statement : {0}GetLogs({1}),{2}", "AccountInfoV2.", accountSysNo, DateTime.Now.Ticks - ts));
                return result as IList<AccountOperationLog>;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}GetLogs({1}),Exception:{2}", "AccountInfoV2.", accountSysNo, e));
                throw;
            }
        }

        public IList<AccountOperationLog> GetOperationLogs(string subjectId, AccountType accountType, DateTime? startDate, DateTime? endDate)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var parameters = new Dictionary<string, object>();
                parameters.Add("SubjectId", subjectId);
                parameters.Add("AccountType", accountType);
                if (startDate != null)
                    parameters.Add("StartDate", startDate);
                if (endDate != null)
                    parameters.Add("EndDate", endDate);
                var result = ExecuteQueryForAnyList("AccountInfoV2.GetLogsByTypeAndSubject", parameters);
                Log.Dao(string.Format("Statement : {0}GetLogsByTypeAndSubject({1}),{2}", "AccountInfoV2.", subjectId, DateTime.Now.Ticks - ts));
                return result as IList<AccountOperationLog>;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}GetLogsByTypeAndSubject({1}),Exception:{2}", "AccountInfoV2.", subjectId, e));
                throw;
            }
        }

        public IList<AccountLedgerInfo> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, string caseId, string tenementNo, string tenementAddr, DateTime? startDate, DateTime? endDate)
        {
            var ts = DateTime.Now.Ticks;
            StringBuilder whereStr = new StringBuilder(string.Format("(C.[ReceptionCenter] = {0} OR GlobalFlag=1)", (int)center));
            try
            {
                if (!string.IsNullOrWhiteSpace(caseId))
                {
                    whereStr.AppendFormat(" AND C.[CaseId] = '{0}'", caseId);
                }
                if (!string.IsNullOrWhiteSpace(tenementNo))
                {
                    whereStr.AppendFormat(" AND C.[TenementContract] = '{0}'", tenementNo);
                }
                if (!string.IsNullOrWhiteSpace(tenementAddr))
                {
                    whereStr.AppendFormat(" AND C.[TenementAddress] = '{0}'", tenementAddr);
                }
                if (startDate != null)
                {
                    whereStr.AppendFormat(" AND C.[CreateDate] >= '{0}'", startDate.Value.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                }
                if (endDate != null)
                {
                    whereStr.AppendFormat(" AND C.[CreateDate] <= '{0}'", endDate.Value.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                }

                var result = ExecuteQueryForPaginatedList<AccountLedgerInfo>("[Case] C",
                    @"C.[CaseId],C.[TenementContract],C.[TenementAddress],(SELECT [Balance] FROM [AccountInfo] WHERE [SubjectId] = C.[CaseId] AND [AccountType] = 1) AS BuyerBalance,(SELECT [Balance] FROM [AccountInfo] WHERE [SubjectId] = C.[CaseId] AND [AccountType] = 0) AS SellerBalance",
                    whereStr.ToString(), "C.[CreateDate] DESC", pageIndex, pageSize, out totalCount);
                Log.Dao(string.Format("AccountInfoV2.GetPaginatedList({0}),{1}", whereStr, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("AccountInfoV2.GetPaginatedList({0}),{1}", whereStr, e));
                throw;
            }
        }

        public Tuple<IDictionary<OrderType, decimal>, Dictionary<OrderType, decimal>> GetCaseTradeAmount(string caseId)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForList<CaseAccountTradeInfo>("AccountInfoV2.GetCaseTradeAmount", caseId);
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "AccountInfoV2.GetCaseTradeAmount", caseId, DateTime.Now.Ticks - ts));
                if (result == null || result.Count == 0)
                    return null;

                return new Tuple<IDictionary<OrderType, decimal>, Dictionary<OrderType, decimal>>
                    (
                    new Dictionary<OrderType, decimal> 
                    { 
                        { OrderType.Collection,  result.Where(a => a.AccountType == AccountType.Seller && a.OrderType == OrderType.Collection).Sum(a => a.Amount) },
                        { OrderType.Payment,  result.Where(a => a.AccountType == AccountType.Seller && a.OrderType == OrderType.Payment).Sum(a => a.Amount)  }
                    },
                    new Dictionary<OrderType, decimal> 
                    { 
                        { OrderType.Collection,  result.Where(a => a.AccountType == AccountType.Buyer && a.OrderType == OrderType.Collection).Sum(a => a.Amount) },
                        { OrderType.Payment,  result.Where(a => a.AccountType == AccountType.Buyer && a.OrderType == OrderType.Payment).Sum(a => a.Amount)  }
                        
                    }
                    );
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "AccountInfoV2.GetCaseTradeAmount", caseId, e));
                throw;
            }
        }

        private AccountCategory MapAccountCategory(OrderBizType bizType)
        {
            switch (bizType)
            {
                case OrderBizType.Collection: return AccountCategory.Recharge;
                case OrderBizType.AgencyFee: return AccountCategory.AgencyFee;
                case OrderBizType.HosingFund: return AccountCategory.HosingFund;
                case OrderBizType.Taxes: return AccountCategory.Taxes;
                case OrderBizType.Compensation: return AccountCategory.Compensation;
                case OrderBizType.ServiceCharge: return AccountCategory.ServiceCharge;
                case OrderBizType.EFQServiceCharge: return AccountCategory.EFQServiceCharge;
                default: throw new Exception("未知订单业务类型");
            }
        }
    }
}
