﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.DataAccess
{
    public class DeductScheduleDataAccess : BaseDataAccess<DeductScheduleInfo>, IDeductScheduleDataAccess
    {
        public override bool Delete(long sysNo, long operatorSysNo)
        {
            throw new NotImplementedException();
        }

        public bool Delete(long sysNo, long operatorSysNo, long version)
        {
            try
            {
                return ExecuteDelete("DeductScheduleInfo.Delete", new Dictionary<string, object> { { "SysNo", sysNo }, { "OperatorSysNo", operatorSysNo }, { "Verson", version } }) > 0;
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }

        public IList<DeductScheduleInfo> GetByAccount(long accountSysNo, bool? finished, DateTime? startDate = null, DateTime? endDate = null)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var parameters = new Dictionary<string, object>();
                parameters.Add("AccountSysNo", accountSysNo);

                if (finished != null)
                    parameters.Add("Finished", Convert.ToInt32(finished.Value));
                if (startDate != null)
                    parameters.Add("StartDate", startDate);
                if (endDate != null)
                    parameters.Add("EndDate", endDate);

                var result = ExecuteQueryForList("DeductScheduleInfo.GetByAccount", parameters);
                Log.Dao(string.Format("Statement : {0}({1}),{2}", "DeductScheduleInfo.GetByAccount", accountSysNo, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Statement:{0}({1}),Exception:{2}", "DeductScheduleInfo.GetByAccount", accountSysNo, e));
                throw;
            }
        }

        public IList<DeductScheduleQueryInfo> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, string caseId, bool? finished, DateTime? startDate, DateTime? endDate, string orderBy = "S.[CreateDate] DESC")
        {
            var ts = DateTime.Now.Ticks;
            StringBuilder whereStr = new StringBuilder(string.Format("S.[IsDeleted] = 0 AND (C.[ReceptionCenter] = {0} OR C.GlobalFlag=1)", (int)center));
            try
            {
                if (!string.IsNullOrWhiteSpace(caseId))
                {
                    whereStr.AppendFormat(" AND C.[CaseId] = '{0}'", caseId);
                }
                if (finished != null) 
                {
                    whereStr.Append(finished.Value ? " AND S.[DeductedAmount] < S.[ApplyAmount]" : " AND S.[DeductedAmount] >= S.[ApplyAmount]");
                }

                var result = ExecuteQueryForPaginatedList<DeductScheduleQueryInfo>("[DeductSchedule] S JOIN [AccountInfo] A ON S.[AccountSysNo] = A.[SysNo] JOIN [Case] C ON C.[CaseId] = A.[SubjectId]",
                    @"C.[CaseId],C.[TenementContract],C.[TenementAddress],A.[AccountType],S.*",
                    whereStr.ToString(), orderBy, pageIndex, pageSize, out totalCount);
                Log.Dao(string.Format("DeductScheduleInfo.GetPaginatedList({0}),{1}", whereStr, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("DeductScheduleInfo.GetPaginatedList({0}),{1}", whereStr, e));
                throw;
            }
        }
    }
}
