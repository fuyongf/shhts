﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.DataAccess
{
    public class ThirdpartyUserDataAccess : BaseDataAccess<ThirdpartyUserInfo>, IThirdpartyUserDataAccess
    {
        public override bool Insert(ThirdpartyUserInfo entity)
        {
            throw new NotImplementedException();
        }
        public bool Insert(ThirdpartyUserInfo tpUser, string loginName = null, string password = null)
        {
            try
            {
                tpUser.SysNo = Sequence.Get();
                long? userSysNo = null;
                if (!string.IsNullOrWhiteSpace(tpUser.IdentityNo))
                    userSysNo = ServiceDepository.UserDataAccess.GetSysNoByIdentityNo(tpUser.CertificateType, tpUser.IdentityNo);

                TransactionManager.Instance.BeginTransaction();

                if (userSysNo == null && !string.IsNullOrWhiteSpace(loginName))
                {
                    userSysNo = Sequence.Get();
                    if (!ServiceDepository.UserAccountDataAccess.Insert(new UserAccountInfo
                    {
                        CreateUserSysNo = tpUser.CreateUserSysNo,
                        LoginId = loginName,
                        PassWord = password,
                        SysNo = userSysNo.Value,
                        UserType = UserType.ThirdpartyUser,
                        AuthDisabled = false
                    }))
                    {
                        TransactionManager.Instance.RollBackTransaction();
                        return false;
                    }
                }

                tpUser.UserSysNo = userSysNo ?? Sequence.Get();

                ExecuteInsert("ThirdpartyUserInfo.Insert", tpUser);

                TransactionManager.Instance.CommitTransaction();
                return true;
            }
            catch (Exception e)
            {
                TransactionManager.Instance.RollBackTransaction();
                Log.Error(e);
                throw;
            }
        }

        public IList<ThirdpartyUserInfo> Get(string userId)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForList("ThirdpartyUserInfo.GetByUserId", userId);
                Log.Dao(string.Format("Statement : ThirdpartyUserInfo.GetByUserId({0}),{1}", userId, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> ThirdpartyUserInfo.GetByUserId({0}),Exception:{1}", userId, e));
                throw;
            }
        }

        public IList<ThirdpartyUserInfo> GetPaginatedList(string condition, string tpName, int pageIndex, int pageSize, out int totalCount, string orderBy = "T.[CreateDate] DESC", ConditionMatchingType matchingType = ConditionMatchingType.Fuzzy)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                StringBuilder whereStr = new StringBuilder("T.[IsDeleted] = 0");
                string matchingStr = matchingType.GetDescription();
                string value = matchingType == ConditionMatchingType.Fuzzy || matchingType == ConditionMatchingType.FullFuzzy ? condition : string.Format("'{0}'", condition);
                if (!string.IsNullOrEmpty(condition))
                {
                    //whereStr.Append(" AND (");
                    //whereStr.AppendFormat(matchingStr, "[LoginId]", value);
                    //whereStr.Append(" OR ");
                    whereStr.AppendFormat(matchingStr, "[RealName]", value);
                    //whereStr.Append(") ");
                }
                if (!string.IsNullOrEmpty(tpName))
                {
                    whereStr.AppendFormat(" AND [TpName] LIKE '{0}%'", tpName);
                }
                var result = ExecuteQueryForPaginatedList("[UserInfo] U JOIN [ThirdpartyUser] T ON U.[SysNo] = T.[UserSysNo]",
                    @"U.[UserId],U.[RealName],U.[Mobile],U.[TelPhone],U.[Email],U.[Gender],U.[CertificateType],U.[IdentityNo],U.[FingerprintCount],
                    T.[TpName],T.[SysNo],T.[UserSysNo],T.[CreateUserSysNo],T.[CreateDate],T.[ModifyDate],T.[ModifyUserSysNo],T.[IsDeleted]",
                    whereStr.ToString(), orderBy, pageIndex, pageSize, out totalCount);
                Log.Dao(string.Format("ThirdpartyUserInfo.GetPaginatedList({0}),{1}", condition, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("ThirdpartyUserInfo.GetPaginatedList({0}),{1}", condition, e));
                throw;
            }
        }
    }
}
