﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.DataAccess
{
    public class SettingDataAccess : BaseSqlMapDao<Setting>, ISettingDataAccess
    {
        public bool Insert(Setting setting)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                if (setting == null)
                    return false;
                ExecuteInsert("Setting.Insert", setting);
                Log.Dao(string.Format("Dao -> Setting.Insert({0}),{1}", setting.Id, DateTime.Now.Ticks - ts));
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Setting.Insert({0}),Exception:{1}", setting.Id, e));
                throw;
            }
            return true;
        }

        public bool Update(string id, string value, string desc)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                if (string.IsNullOrEmpty(id))
                    return false;
                ExecuteUpdate("Setting.Update", new Dictionary<string, string> { { "Id", id }, { "Value", value }, { "Description", desc } });
                Log.Dao(string.Format("Dao -> Setting.Update({0},{1}),{2}", id, value, DateTime.Now.Ticks - ts));
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Setting.Update({0},{1}),Exception:{2}", id, value, e));
                throw;
            }
            return true;
        }

        public bool Remove(string id)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                if (string.IsNullOrEmpty(id))
                    return false;
                ExecuteDelete("Setting.Remove", id);
                Log.Dao(string.Format("Dao -> Setting.Remove({0}),{1}", id, DateTime.Now.Ticks - ts));
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Setting.Remove({0}),Exception:{1}", id, e));
                throw;
            }
            return true;
        }

        public Setting Get(string id)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                if (string.IsNullOrEmpty(id))
                    return null;
                var result = ExecuteQueryForObject("Setting.Get", id);
                Log.Dao(string.Format("Dao -> Setting.Get({0}),{1}", id, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Setting.Get({0}),Exception:{1}", id, e));
                throw;
            }
        }

        public string GetValue(string id)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                if (string.IsNullOrEmpty(id))
                    return null;
                var result = ExecuteQueryForAnyObject("Setting.GetValue", id);
                Log.Dao(string.Format("Dao -> Setting.GetValue({0}),{1}", id, DateTime.Now.Ticks - ts));
                return result != null ? result.ToString() : null;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Setting.GetValue({0}),Exception:{1}", id, e));
                throw;
            }
        }

        public IEnumerable<Setting> GetItems(string id)
        {
            var ts = DateTime.Now.Ticks;
            try
            {
                var result = ExecuteQueryForList("Setting.GetItems", id);
                Log.Dao(string.Format("Dao -> Setting.GetItems({0}),{1}", id, DateTime.Now.Ticks - ts));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Dao -> Setting.GetItems({0}),Exception:{1}", id, e));
                throw;
            }
        }

        public IList<Setting> GetItemsByPath(string path)
        {
            return ExecuteQueryForList("Setting.GetItemsByPath", string.Format("{0}%",path));
        }
    }
}
