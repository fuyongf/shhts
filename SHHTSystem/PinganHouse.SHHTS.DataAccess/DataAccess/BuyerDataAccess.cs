﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;

namespace PinganHouse.SHHTS.DataAccess
{
    public class BuyerDataAccess : BaseDataAccess<BuyerInfo>, IBuyerDataAccess
    {
        public BuyerInfo Get(string caseId)
        {
            return ExecuteQueryForObject("BuyerInfo.GetByCaseId", caseId);
        }
    }
}
