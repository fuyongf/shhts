﻿using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.DataAccess
{
    public class PropertySurveyDataAccess : BaseDataAccess<PropertySurvey>, IPropertySurveyDataAccess
    {
        public override bool Insert(PropertySurvey entity)
        {
            entity.SysNo = Sequence.Get();
            return base.Insert(entity);
        }

        public IList<PropertySurvey>  GetPropertySurveys(string tenementContract)
        {
            return ExecuteQueryForList("PropertySurvey.GetPropertySurvey", tenementContract);
        }

        public IList<PropertySurvey> GetPropertySurveys(IEnumerable<string> tenementContracts)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var t in tenementContracts)
            {
                sb.AppendFormat("'{0}',", t);
            }
            return ExecuteQueryForList("PropertySurvey.GetPropertySurveys", sb.ToString().TrimEnd(','));
        }

    }
}
