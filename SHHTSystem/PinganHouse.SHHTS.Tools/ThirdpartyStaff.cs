﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using PinganHouse.SHHTS.RemoteServiceProxy;

namespace PinganHouse.SHHTS.Tools
{
    public partial class ThirdpartyStaff : Form
    {
        public ThirdpartyStaff()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var result = ThirdpartyUserServiceProxy.CreateThirdpartyUser(new ThirdpartyUserModel
            {
                Name = txtName.Text,
                Mobile = txtMobile.Text,
                IdentityNo = txtIdNo.Text,
                TpName = txtTpName.Text,
                LoginName = txtLoginName.Text,
                Password = txtPassword.Text
            }, 0);
            if (result.Success)
            {
                MessageBox.Show("添加成功");
                txtName.Text = string.Empty;
                txtMobile.Text = string.Empty;
                txtIdNo.Text = string.Empty;
                txtTpName.Text = string.Empty;
                txtLoginName.Text = string.Empty;
                txtPassword.Text = string.Empty;
            }
            else
                MessageBox.Show(result.ResultMessage);
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "Excel(*.xlsx)|*.xlsx|Excel(*.xls)|*.xls";
            openFile.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            openFile.Multiselect = false;
            if (openFile.ShowDialog() == DialogResult.Cancel) return;
            var dt = ExcelUtils.ReadToDataTable(openFile.FileName);
            if (dt == null)
            {
                MessageBox.Show("Excel文件读取错误");
                return;
            }
            MessageBox.Show(string.Format("读取到{0}条员工数据", dt.Rows.Count));

            foreach (DataRow row in dt.Rows)
            {
                try
                {
                    if (!ThirdpartyUserServiceProxy.CreateThirdpartyUser(new ThirdpartyUserModel 
                    {
                        Name = row[0].ToString(),
                        Mobile = row[2].ToString(),
                        TpName = "平安好房二手房团队",
                        LoginName = row[3].ToString(),
                        Password = row[2].ToString(),
                    },null).Success)
                        MessageBox.Show("添加失败，继续添加...");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("添加失败，继续添加...");
                    continue;
                }
            }
            MessageBox.Show("导入完成");
        }
    }
}
