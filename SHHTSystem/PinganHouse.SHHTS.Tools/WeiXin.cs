﻿using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PinganHouse.SHHTS.Tools
{
    public partial class WeiXin : Form
    {
        public WeiXin()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var bt = new List<Dictionary<string, object>>();

            var sbt1 = new List<Dictionary<string, object>>();
            var sbt11 = new Dictionary<string, object> { { "type", "click" }, { "key", 0 }, { "name", "交易进度" }, };
            //var sbt12 = new Dictionary<string, object> { { "type", "click" }, { "key", 1 }, { "name", "交易记录" }, };
            var sbt12 = new Dictionary<string, object> { { "type", "view" }, { "url", "http://erpweixin.pinganhaofang.com/Transaction/Index?isMine=0" }, { "name", "交易记录" }, };
            sbt1.Add(sbt11);
            sbt1.Add(sbt12);
            var bt1 = new Dictionary<string, object> { { "name", "我的" }, { "sub_button", sbt1 } };



            var bt2 = new Dictionary<string, object> { { "name", "我要" }, { "type", "click" }, { "key", "ok" } };
            //var sbt2 = new List<Dictionary<string, object>>();



            var sbt3 = new List<Dictionary<string, object>>();
            var sbt31 = new Dictionary<string, object> { { "type", "view" }, { "url", "http://erpweixin.pinganhaofang.com/LoanCalculation/Index" }, { "name", "房贷/税费计算器" }, };
            var sbt32 = new Dictionary<string, object> { { "type", "view" }, { "url", "http://erpweixin.pinganhaofang.com/LimitsPurchases/ShanghPeopleSearch" }, { "name", "名下住房查询" }, };
            //var sbt31 = new Dictionary<string, object> { { "type", "click" }, { "key", "ok" }, { "name", "房贷计算器" }, };
            //var sbt32 = new Dictionary<string, object> { { "type", "click" }, { "key", "ok" }, { "name", "限购查询" }, };
            var sbt33 = new Dictionary<string, object> { { "type", "click" }, { "key", 2 }, { "name", "联系我们" }, };
            sbt3.Add(sbt31);
            sbt3.Add(sbt32);
            sbt3.Add(sbt33);
            var bt3 = new Dictionary<string, object> { { "name", "好房助手" }, { "sub_button", sbt3 } };


            bt.Add(bt1);
            //bt.Add(bt2);
            bt.Add(bt3);
            var result = ParterProxyService.GetInstanse().CreateMenus(bt);
            MessageBox.Show("ok");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var bt = new List<Dictionary<string, object>>();

            var bt1 = new Dictionary<string, object> { { "type", "view" }, { "url", "http://erpweixin.pinganhaofang.com/Case/CaseStaffIndex" }, { "name", "我的案件" }, };


            var bt2 = new Dictionary<string, object> { { "name", "我要办理" }, { "type", "click" }, { "key", "ok" } };


            var sbt3 = new List<Dictionary<string, object>>();
            var sbt35 = new Dictionary<string, object> { { "type", "click" }, { "key", "ok" }, { "name", "优惠活动" }, };
            var sbt31 = new Dictionary<string, object> { { "type", "click" }, { "key", "ok" }, { "name", "产调申请" }, };
            var sbt32 = new Dictionary<string, object> { { "type", "view" }, { "url", "http://erpweixin.pinganhaofang.com/LoanCalculation/Index" }, { "name", "房贷/税费计算器" }, };
            var sbt33 = new Dictionary<string, object> { { "type", "view" }, { "url", "http://erpweixin.pinganhaofang.com/LimitsPurchases/ShanghPeopleSearch" }, { "name", "名下住房查询" }, };
            var sbt34 = new Dictionary<string, object> { { "type", "click" }, { "key", 2 }, { "name", "联系我们" }, };
            sbt3.Add(sbt35);
            sbt3.Add(sbt31);
            sbt3.Add(sbt32);
            sbt3.Add(sbt33);
            sbt3.Add(sbt34);

            var bt3 = new Dictionary<string, object> { { "name", "好房助手" }, { "sub_button", sbt3 } };


            bt.Add(bt1);
            bt.Add(bt2);
            bt.Add(bt3);
            var result = ParterProxyService.GetInstanse().CreateMenus(bt, AgentType.Agency);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var bt = new List<Dictionary<string, object>>();


            var sbt1 = new List<Dictionary<string, object>>();
            var sbt11 = new Dictionary<string, object> { { "type", "view" }, { "url", "http://erpweixin.pinganhaofang.com/Case/CaseAgentCompanyIndex" }, { "name", "案件" }, };
            var sbt12 = new Dictionary<string, object> { { "type", "view" }, { "url", "http://erpweixin.pinganhaofang.com/Bill/Index" }, { "name", "账单" }, };
            sbt1.Add(sbt11);
            sbt1.Add(sbt12);
            var bt1 = new Dictionary<string, object> { { "name", "业务管理" }, { "sub_button", sbt1 } };



            var bt2 = new Dictionary<string, object> { { "name", "人员管理" }, { "type", "view" }, { "url", "http://erpweixin.pinganhaofang.com/Staff/List" }, };


            var sbt3 = new List<Dictionary<string, object>>();
            var sbt35 = new Dictionary<string, object> { { "type", "click" }, { "key", "ok" }, { "name", "优惠活动" }, };
            var sbt31 = new Dictionary<string, object> { { "type", "click" }, { "key", "ok" }, { "name", "产调申请" }, };
            var sbt32 = new Dictionary<string, object> { { "type", "view" }, { "url", "http://erpweixin.pinganhaofang.com/LoanCalculation/Index" }, { "name", "房贷/税费计算器" }, };
            var sbt33 = new Dictionary<string, object> { { "type", "view" }, { "url", "http://erpweixin.pinganhaofang.com/LimitsPurchases/ShanghPeopleSearch" }, { "name", "名下住房查询" }, };
            var sbt34 = new Dictionary<string, object> { { "type", "click" }, { "key", 2 }, { "name", "联系我们" }, };
            sbt3.Add(sbt35);
            sbt3.Add(sbt31);
            sbt3.Add(sbt32);
            sbt3.Add(sbt33);
            sbt3.Add(sbt34);

            var bt3 = new Dictionary<string, object> { { "name", "好房助手" }, { "sub_button", sbt3 } };


            bt.Add(bt1);
            bt.Add(bt2);
            bt.Add(bt3);
            var result = ParterProxyService.GetInstanse().CreateMenus(bt, AgentType.AgentCompany);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int totalCount;
            var ds = AgentServiceProxy.GetAgentCompanyList(null, null, 1, 1500, out totalCount);
            foreach (var entity in ds)
            {
                ParterProxyService.GetInstanse().CreateDepartment(string.Format("{0}({1})", entity.CompanyName, entity.SysNo), (int)AgentType.AgentCompany, 9, (int)entity.SysNo);
            }
            MessageBox.Show("ok");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int totalCount;
            var ds = AgentServiceProxy.GetAgentCompanyList(null, null, 1, 1500, out totalCount);
            foreach (var d in ds)
            {
                ParterProxyService.GetInstanse().DeleteDepartment(d.SysNo.ToString());
            }
            MessageBox.Show("ok");
        }

        private void button6_Click(object sender, EventArgs e)
        {
             int totalCount;
             var agents = AgentServiceProxy.GetAgentStaffs(1, 100, out totalCount,null, null);
             foreach (var agent in agents)
             {
                var departments = new List<string> { ((int)AgentType.Agency).ToString(), };
                if (agent.IsManager)
                    departments.Add(agent.AgentCompanySysNo.ToString());
                ParterProxyService.GetInstanse().CreateUser(agent.UserId, agent.RealName, agent.Mobile, agent.Email, null, null, departments, agent.IdentityNo);
             }
             MessageBox.Show("ok");
        }
    }
}
