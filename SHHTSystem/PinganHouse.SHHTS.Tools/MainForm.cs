﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PinganHouse.SHHTS.Tools
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void btnAddAgentCompany_Click(object sender, EventArgs e)
        {
            AgentCompany company = new AgentCompany();
            company.ShowDialog();
        }

        private void btnAddAgentStaff_Click(object sender, EventArgs e)
        {
            AgentStaff agentStaff = new AgentStaff();
            agentStaff.ShowDialog();
        }

        private void btnAddTPStaff_Click(object sender, EventArgs e)
        {
            ThirdpartyStaff tpStaff = new ThirdpartyStaff();
            tpStaff.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Reconciliation re = new Reconciliation();
            re.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            PinganStaff staff = new PinganStaff();
            staff.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            WeiXin menu = new WeiXin();
            menu.ShowDialog();
        }
    }
}
