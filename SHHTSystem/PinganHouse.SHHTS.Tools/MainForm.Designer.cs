﻿namespace PinganHouse.SHHTS.Tools
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAddAgentCompany = new System.Windows.Forms.Button();
            this.btnAddAgentStaff = new System.Windows.Forms.Button();
            this.btnAddTPStaff = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnAddAgentCompany
            // 
            this.btnAddAgentCompany.Location = new System.Drawing.Point(11, 30);
            this.btnAddAgentCompany.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddAgentCompany.Name = "btnAddAgentCompany";
            this.btnAddAgentCompany.Size = new System.Drawing.Size(112, 38);
            this.btnAddAgentCompany.TabIndex = 0;
            this.btnAddAgentCompany.Text = "添加中介公司";
            this.btnAddAgentCompany.UseVisualStyleBackColor = true;
            this.btnAddAgentCompany.Click += new System.EventHandler(this.btnAddAgentCompany_Click);
            // 
            // btnAddAgentStaff
            // 
            this.btnAddAgentStaff.Location = new System.Drawing.Point(145, 30);
            this.btnAddAgentStaff.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddAgentStaff.Name = "btnAddAgentStaff";
            this.btnAddAgentStaff.Size = new System.Drawing.Size(112, 38);
            this.btnAddAgentStaff.TabIndex = 1;
            this.btnAddAgentStaff.Text = "添加经纪人";
            this.btnAddAgentStaff.UseVisualStyleBackColor = true;
            this.btnAddAgentStaff.Click += new System.EventHandler(this.btnAddAgentStaff_Click);
            // 
            // btnAddTPStaff
            // 
            this.btnAddTPStaff.Location = new System.Drawing.Point(292, 30);
            this.btnAddTPStaff.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddTPStaff.Name = "btnAddTPStaff";
            this.btnAddTPStaff.Size = new System.Drawing.Size(112, 38);
            this.btnAddTPStaff.TabIndex = 2;
            this.btnAddTPStaff.Text = "添加第三方员工";
            this.btnAddTPStaff.UseVisualStyleBackColor = true;
            this.btnAddTPStaff.Click += new System.EventHandler(this.btnAddTPStaff_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(41, 114);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 38);
            this.button1.TabIndex = 3;
            this.button1.Text = "对账";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(437, 30);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(112, 38);
            this.button2.TabIndex = 4;
            this.button2.Text = "添加经平安员工";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(12, 252);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(82, 39);
            this.button3.TabIndex = 5;
            this.button3.Text = "微信";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(575, 314);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnAddTPStaff);
            this.Controls.Add(this.btnAddAgentStaff);
            this.Controls.Add(this.btnAddAgentCompany);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "MainForm";
            this.Text = "SHHTSTool";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAddAgentCompany;
        private System.Windows.Forms.Button btnAddAgentStaff;
        private System.Windows.Forms.Button btnAddTPStaff;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}

