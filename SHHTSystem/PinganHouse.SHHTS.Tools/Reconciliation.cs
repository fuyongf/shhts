﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows.Forms;

namespace PinganHouse.SHHTS.Tools
{
    public partial class Reconciliation : Form
    {

        private const string SecurityKey = "7987ea40976f29c71526701ce748402e";
        protected const string SIGN_JOINT_STR = "_ping_an_fang_";
        public Reconciliation()
        {
            InitializeComponent();

            comboBox1.DataSource = new List<ComdDataSource> { 
                new ComdDataSource { Key = 1, Value= "成功" }, 
                new ComdDataSource {Key = 2, Value= "订单重复"  }, 
                new ComdDataSource {Key = 3, Value= "交易金额异常"  },
                new ComdDataSource {Key = 4, Value= "交易状态异常"  } ,
                new ComdDataSource {Key = 5, Value= "掉单"  } 
            };
            comboBox1.ValueMember = "Key";
            comboBox1.DisplayMember = "Value";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var data = new Dictionary<string, object>();
            data.Add("sBusinessOrderID", textBox1.Text);
            data.Add("iReconcileStatus", comboBox1.SelectedValue);
            data.Add("sBankSerialNum", textBox2.Text);
            data.Add("iTradeDate", dateTimePicker1.Value.ToString("yyyyMMdd"));
            data.Add("iAmount", (int)(decimal.Parse(textBox3.Text) * 100));
            data.Add("iRequestTime", UnixTimeStamp);
            data.Add("sSign", GetSign(data));
            try
            {
                var response = SendPostRequest("http://172.16.250.3:8080/api/fs/callback/ret/collection", BuildParameters(data), 10000, Encoding.UTF8, Encoding.UTF8, null);
                if (string.IsNullOrEmpty(response))
                    MessageBox.Show("响应数据错误");
                var result = JsonSerializer.DeserializeToObject<FsOperationResult>(response);
                MessageBox.Show(result.memo);
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("对账通知失败,{0}", ex.ToString()));
            }
        }

        protected string GetSign(IDictionary<string, object> parameters)
        {
            if (string.IsNullOrEmpty(SecurityKey))
                throw new ArgumentNullException("security key not found.");
            //sort
            if (!(parameters is SortedDictionary<string, object>))
            {
                var sortedDic = new SortedDictionary<string, object>();
                foreach (var item in parameters)
                {
                    sortedDic.Add(item.Key, item.Value == null ? string.Empty : item.Value.ToString());
                }
                parameters = sortedDic;
            }
            //json
            string json = JsonSerializer.SerializeToJson(parameters);

            //md5
            string sign = GetMD5(string.Concat(SecurityKey, SIGN_JOINT_STR, json));
            return GetMD5(string.Concat(sign, SIGN_JOINT_STR, SecurityKey));
        }

        protected string BuildParameters(IDictionary<string, object> parameters)
        {
            if (parameters == null || parameters.Count == 0)
                return null;
            StringBuilder str = new StringBuilder();
            foreach (var item in parameters)
            {
                str.AppendFormat("{0}={1}&", item.Key, item.Value);
            }
            return str.ToString().TrimEnd('&');
        }

        /// <summary>
        /// 通讯函数
        /// </summary>
        /// <param name="url">请求Url</param>
        /// <param name="para">请求参数</param>
        /// <returns></returns>
        public string SendPostRequest(string url, string paras, int timeout, Encoding reqEncoding, Encoding respEncoding, string contentType = null)
        {
            if (String.IsNullOrEmpty(url))
            {
                return String.Empty;
            }
            if (paras == null)
            {
                paras = String.Empty;
            }
            byte[] data = reqEncoding.GetBytes(paras);
            Stream requestStream = null;
            try
            {
                HttpWebRequest request = null;
                //如果是发送HTTPS请求  
                if (url.StartsWith("https", StringComparison.OrdinalIgnoreCase))
                {
                    ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);
                    request = WebRequest.Create(url) as HttpWebRequest;
                    request.ProtocolVersion = HttpVersion.Version10;
                }
                else
                {
                    request = WebRequest.Create(url) as HttpWebRequest;
                }
                request.Method = "POST";
                request.Timeout = timeout;
                request.ContentType = String.IsNullOrEmpty(contentType) ? "application/x-www-form-urlencoded;" : contentType;
                request.ContentLength = data.Length;

                // Send the data.  
                if (!String.IsNullOrEmpty(paras) && data.Length > 0)
                {
                    requestStream = request.GetRequestStream();
                    requestStream.Write(data, 0, data.Length);
                    requestStream.Close();
                }
                //Response
                HttpWebResponse myResponse = (HttpWebResponse)request.GetResponse();
                StreamReader reader = new StreamReader(myResponse.GetResponseStream(), respEncoding);
                string content = reader.ReadToEnd();

                //Dispose
                if (requestStream != null)
                {
                    requestStream.Dispose();
                }
                request.Abort();
                return content;
            }
            catch
            {
                throw;
            }
        }

        private bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
        {
            return true; //总是接受  
        }

        public string GetMD5(string s, string charset = "utf-8")
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] t = md5.ComputeHash(Encoding.GetEncoding(charset).GetBytes(s));
            StringBuilder sb = new StringBuilder(32);
            for (int i = 0; i < t.Length; i++)
            {
                sb.Append(t[i].ToString("x").PadLeft(2, '0'));
            }
            return sb.ToString();
        }

        protected int UnixTimeStamp
        {
            get
            {
                return GetUnixTimeStamp(DateTime.Now);
            }
        }

        protected int GetUnixTimeStamp(DateTime date)
        {
            return (int)((date - TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1))).Ticks / 10000000);
        }
    }

    public static class JsonSerializer
    {
        public static string SerializeToJson(object input)
        {
            return JsonConvert.SerializeObject(input);
        }

        public static IDictionary<string, object> DeserializeToDictionary(string json)
        {
            return JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        }

        public static T DeserializeToObject<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }
    }

    public class FsOperationResult
    {
        private string _code = string.Empty;
        public string code
        {
            get { return _code; }
            set { _code = value; }
        }

        private string _memo = string.Empty;
        public string memo
        {
            get { return _memo; }
            set { _memo = value; }
        }

        private IList _data = new List<object>();
        public IList data
        {
            get { return _data; }
            set { _data = value; }
        }
    }

    public class ComdDataSource
    {
        public int Key { get; set; }
        public string Value { get; set; }
    }
}
