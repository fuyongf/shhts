﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using PinganHouse.SHHTS.RemoteServiceProxy;

namespace PinganHouse.SHHTS.Tools
{
    public partial class AgentStaff : Form
    {
        public AgentStaff()
        {
            InitializeComponent();
        }

        private void AgentStaff_Load(object sender, EventArgs e)
        {
            int totalCount = 0;
            var company = AgentServiceProxy.GetAgentCompanyList(null, null, 1, 100000, out totalCount);
            if (company == null)
                return;
            cbCompany.DataSource = company;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var result = AgentServiceProxy.CreateAgentStaff(new AgentStaffModel
            {
                Name = txtName.Text,
                Mobile = txtMobile.Text,
                Mobile1 = txtMobile1.Text,
                IdentityNo = txtIdNo.Text,
                AgentCompanySysNo = (long)cbCompany.SelectedValue,
                LoginName = txtLoginName.Text,
                Password = txtPassword.Text,
                IsManager = cbIsManager.Checked
            }, 0);
            if (result.Success)
            {
                MessageBox.Show("添加成功");
                txtName.Text = string.Empty;
                txtMobile.Text = string.Empty;
                txtIdNo.Text = string.Empty;
                txtMobile1.Text = string.Empty;
                txtLoginName.Text = string.Empty;
                txtPassword.Text = string.Empty;
            }
            else
                MessageBox.Show(result.ResultMessage);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (cbCompany.SelectedIndex < 0)
            {
                MessageBox.Show("请先选择所属中介公司");
                return;
            }
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "Excel(*.xlsx)|*.xlsx|Excel(*.xls)|*.xls";
            openFile.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            openFile.Multiselect = false;
            if (openFile.ShowDialog() == DialogResult.Cancel) return;
            var dt = ExcelUtils.ReadToDataTable(openFile.FileName);
            if (dt == null)
            {
                MessageBox.Show("Excel文件读取错误");
                return;
            }
            MessageBox.Show(string.Format("读取到{0}条员工数据", dt.Rows.Count));

            foreach (DataRow row in dt.Rows)
            {
                try
                {
                    if (!AgentServiceProxy.CreateAgentStaff(
                        new AgentStaffModel
                        {
                            Name = row[1].ToString(),
                            Mobile = row[2].ToString(),
                            AgentCompanySysNo = (long)cbCompany.SelectedValue
                        }, 0).Success)
                        MessageBox.Show("添加失败，继续添加...");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("添加失败，继续添加...");
                    continue;
                }
            }
            MessageBox.Show("导入完成");
        }
    }
}
