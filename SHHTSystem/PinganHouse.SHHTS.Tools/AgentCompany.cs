﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using PinganHouse.SHHTS.RemoteServiceProxy;

namespace PinganHouse.SHHTS.Tools
{
    public partial class AgentCompany : Form
    {
        public AgentCompany()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var reusult = AgentServiceProxy.AgentJoint(new AgentCompanyModel
            {
                CompanyName = txtCompanyName.Text,
                Principal = txtPrincipal.Text,
                Address = txtAddress.Text,
                ContactInfo = txtContactInfo.Text,
                JointContractNo = txtContractNo.Text
            }, 0);
            if (reusult.Success)
            {
                MessageBox.Show("添加成功");
                txtCompanyName.Text = string.Empty;
                txtPrincipal.Text = string.Empty;
                txtAddress.Text = string.Empty;
                txtContactInfo.Text = string.Empty;
            }
            else
                MessageBox.Show(reusult.ResultMessage);
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "Excel(*.xlsx)|*.xlsx|Excel(*.xls)|*.xls";
            openFile.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            openFile.Multiselect = false;
            if (openFile.ShowDialog() == DialogResult.Cancel) return;
            var dt = ExcelUtils.ReadToDataTable(openFile.FileName);
            if (dt == null)
            {
                MessageBox.Show("Excel文件读取错误");
                return;
            }
            MessageBox.Show(string.Format("读取到{0}条中介数据",dt.Rows.Count));

            foreach (DataRow row in dt.Rows) 
            {
                try
                {
                    if(!AgentServiceProxy.AgentJoint(new AgentCompanyModel 
                    {
                        CompanyName = row[3].ToString(),
                        Address = row[4].ToString(),
                        Principal = row[5].ToString(),
                        ContactInfo = row[6].ToString(),
                        JointContractNo = row[1].ToString(),
                        JointDate = DateTime.Parse(row[2].ToString())
                    }, 0).Success)
                        MessageBox.Show("添加失败，继续添加...");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("添加失败，继续添加...");
                    continue;
                }
            }
            MessageBox.Show("导入完成");
        }
    }
}
