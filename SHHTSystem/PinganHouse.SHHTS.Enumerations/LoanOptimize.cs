﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Enumerations
{
    /// <summary>
    /// 套数认定
    /// </summary>
    public enum TaoShuRd
    {
        [Description("首套")]
        First,
        [Description("二套")]
        Second
    }

    /// <summary>
    /// 贷款还款方式
    /// </summary>
    public enum RepayType
    {
        /// <summary>
        /// 等额本息
        /// </summary>
        [Description("等额本息")]
        ConstantPayment,
        /// <summary>
        /// 等额本金
        /// </summary>
        [Description("等额本金")]
        ConstantAmortization
    }

    /// <summary>
    /// 婚姻状况
    /// </summary>
    public enum MaritalStatus
    {
        /// <summary>
        /// 单身
        /// </summary>
        [Description("单身")]
        Single,
        /// <summary>
        /// 已婚
        /// </summary>
        [Description("已婚")]
        Married
    }

    /// <summary>
    /// 户籍类型
    /// </summary>
    public enum DomicileType
    {
        /// <summary>
        /// 本地
        /// </summary>
        [Description("本地")]
        Local,
        /// <summary>
        /// 非本地
        /// </summary>
        [Description("非本地")]
        NonLocal,
        /// <summary>
        /// 境外
        /// </summary>
        [Description("境外")]
        Overseas
    }
}
