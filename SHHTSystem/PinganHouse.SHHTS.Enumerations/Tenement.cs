﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Enumerations
{
    public enum TenementNature
    {
        /// <summary>
        /// 未知
        /// </summary>
        [Description("未知")]
        UnKnow = 0,
        /// <summary>
        /// 普通住宅
        /// </summary>
        [Description("普通住宅")]
        Normal = 1,
        /// <summary>
        /// 花园洋房
        /// </summary>
        [Description("花园洋房")]
        GardenHouse = 2

    }

    public enum LinkLocation 
    {
        /// <summary>
        /// 未知
        /// </summary>
        [Description("未知")]
        Unknow = 0,
        /// <summary>
        /// 内环以内
        /// </summary>
        [Description("内环以内")]
        WithinInnerRing = 1,
        /// <summary>
        /// 内环之间
        /// </summary>
        [Description("内中环间")]
        BetweenInnerRing = 2,
        /// <summary>
        /// 外环
        /// </summary>
        [Description("中外环间")]
        BetweenRingRoad = 3,
        /// <summary>
        /// 外环以外
        /// </summary>
        [Description("外环以外")]
        OuterRingRoad = 4
    }
}
