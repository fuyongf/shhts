﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Enumerations
{
    public enum ConditionMatchingType
    {
        /// <summary>
        /// 相等
        /// </summary>
        [Description("{0} = {1}")]
        Equal = 0,
        /// <summary>
        /// 模糊匹配
        /// </summary>
        [Description("{0} LIKE '{1}%'")]
        Fuzzy = 1,
        /// <summary>
        /// 大于
        /// </summary>
        [Description("{0} > {1}")]
        Greate = 2,
        /// <summary>
        /// 小于
        /// </summary>
        [Description("{0} < {1}")]
        Less = 3,
        /// <summary>
        /// 大于等于
        /// </summary>
        [Description("{0} >= {1}")]
        GreateOrEqual = 4,
        /// <summary>
        /// 小于等于
        /// </summary>
        [Description("{0} <= {1}")]
        LessOrEqual = 5,
        /// <summary>
        /// 前后模糊匹配
        /// </summary>
        [Description("{0} LIKE '%{1}%'")]
        FullFuzzy = 6,
    }

    public enum Allege 
    {
        /// <summary>
        /// 是
        /// </summary>
        [Description("否")]
        False = 0,
        /// <summary>
        /// 否
        /// </summary>
        [Description("是")]
        True = 1
    }

    public enum Boole 
    {
        /// <summary>
        /// 无
        /// </summary>
        [Description("无")]
        False = 0,
        /// <summary>
        /// 有
        /// </summary>
        [Description("有")]
        True = 1
    }

    
}
