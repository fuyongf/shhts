﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Enumerations
{
    public enum ReviewCategory
    {
        /// <summary>
        /// 未知
        /// </summary>
        UnKonw=0,
        /// <summary>
        /// 收据
        /// </summary>
        Receipt=1,
    }
    /// <summary>
    /// 复核方式
    /// </summary>
    [Flags]
    public enum ReviewMode
    {
        /// <summary>
        /// 工卡
        /// </summary>
        Timecard=0,
        /// <summary>
        /// 指纹
        /// </summary>
        Fingerprint=1,
        /// <summary>
        /// 通行证
        /// </summary>
        Passport=2,
    }
}
