﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Enumerations
{
    public enum OrderType
    {
        /// <summary>
        /// 收款
        /// </summary>
        [Description("入账")]
        Collection = 0,
        /// <summary>
        /// 代付
        /// </summary>
        [Description("出账")]
        Payment = 1,
        /// <summary>
        /// 转账
        /// </summary>
        [Description("转账")]
        Transfer = 2,
        /// <summary>
        /// 提现
        /// </summary>
        [Description("提现")]
        Withdraw = 3,
        /// <summary>
        /// 消费
        /// </summary>
        [Description("费用")]
        Expend = 4,
        /// <summary>
        /// 销账
        /// </summary>
        [Description("销账")]
        PaidOff = 5,
        ///// <summary>
        ///// 退款订单
        ///// </summary>
        //[Description("退款")]
        //Refund = 3
    }

    public enum OrderStatus
    {
        /// <summary>
        /// 初始
        /// </summary>
        [Description("初始")]
        Initial = 0,
        /// <summary>
        /// 已提交
        /// </summary>
        [Description("已提交")]
        Submitted = 1,
        /// <summary>
        /// 交易关闭
        /// </summary>
        [Description("已关闭")]
        Closed = 2,
        /// <summary>
        /// 支付完成 收款完结
        /// </summary>
        [Description("已支付")]
        Paid = 3,
        /// <summary>
        /// 退款中
        /// </summary>
        [Description("退款中")]
        Refunding = 4,
        /// <summary>
        /// 已退款
        /// </summary>
        [Description("已退款")]
        Refunded = 5,
        /// <summary>
        /// 已完成 付款完结
        /// </summary>
        [Description("已完成")]
        Completed = 6
    }

    public enum OrderBizType
    {
        /// <summary>
        /// 收款
        /// </summary>
        [Description("收款")]
        Collection = 0,
        ///// <summary>
        ///// 退款
        ///// </summary>
        //[Description("退款")]
        //Refund = 1,
        /// <summary>
        /// 房款
        /// </summary>
        [Description("房款")]
        HosingFund = 2,
        /// <summary>
        /// 税费
        /// </summary>
        [Description("税费")]
        Taxes = 3,
        /// <summary>
        /// 中介费
        /// </summary>
        [Description("中介佣金")]
        AgencyFee = 4,
        /// <summary>
        /// 服务费
        /// </summary>
        [Description("其它服务费")]
        ServiceCharge = 5,
        /// <summary>
        /// 装修补偿
        /// </summary>
        [Description("装修补偿款")]
        Compensation = 6,
        // <summary>
        /// 壹房钱电商服务费
        /// </summary>
        [Description("E房钱电商服务费")]
        EFQServiceCharge = 7,
    }

    public enum TradeStatus
    {
        /// <summary>
        /// 初始
        /// </summary>
        [Description("待处理")]
        Initial = 0,
        /// <summary>
        /// 已提交
        /// </summary>
        [Description("待处理")]
        Submitted = 1,
        /// <summary>
        /// 已撤销
        /// </summary>
        [Description("已撤销")]
        Cancelled = 2,
        /// <summary>
        /// 已完成
        /// </summary>
        [Description("已完成")]
        Completed = 3,
        /// <summary>
        /// 已失败
        /// </summary>
        [Description("已失败")]
        Failed = 4,
        /// <summary>
        /// 警告
        /// </summary>
        [Description("警告")]
        Warning = 5,
        /// <summary>
        /// 已关闭
        /// </summary>
        [Description("已删除")]
        Closed = 6
    }

    public enum AuditStatus
    {
        /// <summary>
        /// 待审核 .
        /// </summary>
        [Description("待核案复审")]
        Initial = 0,
        /// <summary>
        /// 核案审核.
        /// </summary>
        [Description("待驻场财务审批")]
        Audited = 1,
        /// <summary>
        /// 审核驳回
        /// </summary>
        [Description("核案审核驳回")]
        AuditFailed = 2,
        /// <summary>
        /// 待总部财务审核.
        /// </summary>
        [Description("待总部财务审核")]
        ReAudited = 3,
        /// <summary>
        /// 审核驳回
        /// </summary>
        [Description("驻场财务驳回")]
        ReAuditFailed = 4,
        /// <summary>
        /// 财务审核通过.
        /// </summary>
        [Description("财务已审核")]
        FinanceAudited = 5,
        /// <summary>
        /// 财务审核驳回
        /// </summary>
        [Description("财务审核驳回")]
        FinanceAuditFailed = 6
    }

    public enum PaymentStatus
    {
        FinanceAuditPass = 0,
        Success = 1,
        Failed = 2,
        FinanceAuditFailed = 3
    }

    public enum PaidOffStatus
    {
        Init = 1,
        Success = 2,
        Failed = 3
    }

    public enum ReconciliationStatus
    {
        /// <summary>
        /// 尚未对帐
        /// </summary>
        [Description("尚未对帐")]
        Unknow = 0,
        /// <summary>
        /// 成功
        /// </summary>
        [Description("交易成功")]
        Success = 1,
        /// <summary>
        /// 流水号重复
        /// </summary>
        [Description("流水号重复")]
        Repetition = 2,
        /// <summary>
        /// 金额异常
        /// </summary>
        [Description("交易金额异常")]
        AmountError = 3,
        /// <summary>
        /// 状态异常
        /// </summary>
        [Description("交易状态异常")]
        StatusError = 4,
        /// <summary>
        /// 交易掉单
        /// </summary>
        [Description("掉单")]
        Lose = 5
    }

    public enum PaymentChannel
    {
        /// <summary>
        /// POS
        /// </summary>
        [Description("POS")]
        POS = 0,
        /// <summary>
        /// 转账
        /// </summary>
        [Description("转账")]
        Transfer = 1,
        /// <summary>
        /// 银行代扣
        /// </summary>
        [Description("银行代扣")]
        Entrust = 2,
        /// <summary>
        ///余额
        /// </summary>
        [Description("余额支付")]
        Balance = 3,
        ///// <summary>
        ///// 礼券
        ///// </summary>
        //[Description("礼券")]
        //Gift = 4
    }

    public enum BillCategory
    {
        /// <summary>
        /// POS
        /// </summary>
        [Description("POS票")]
        POS = 0
    }

    public enum BillType
    {
        /// <summary>
        /// 消费
        /// </summary>
        [Description("消费")]
        Expened = 0,
        /// <summary>
        /// 消费撤销
        /// </summary>
        [Description("消费撤销")]
        ExpenedCancelled = 1
    }

    public enum ReceiptStatus
    {
        /// <summary>
        /// 未回收
        /// </summary>
        [Description("未回收")]
        Resultful = 0,
        /// <summary>
        /// 已回收
        /// </summary>
        [Description("已回收")]
        Recycled = 1,
        /// <summary>
        /// 已作废
        /// </summary>
        [Description("已作废")]
        Cancellation = 2
    }
}
