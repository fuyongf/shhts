﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Enumerations
{
    public enum AuditObjectType
    {
        /// <summary>
        /// 案件
        /// </summary>
        Case = 0,
        /// <summary>
        /// 订单
        /// </summary>
        Order =1,
        /// <summary>
        /// 交易
        /// </summary>
        Trade = 2
    }
}
