﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Enumerations
{

    [Flags]
    public enum CaseStatus : long
    {
        /// <summary>
        /// 未知
        /// </summary>
        [Description("未知")]
        Unkown = 0,
        /// <summary>
        /// 创建等待签约
        /// </summary>
        [Description("等待签约")]
        YJ1 = 1,
        /// <summary>
        /// 签约已取消
        /// </summary>
        [Description("签约已取消")]
        ZB2 = 2,
        /// <summary>
        /// 签约中
        /// </summary>
        [Description("签约中")]
        HA1 = 4,
        /// <summary>
        /// 已签约
        /// </summary>
        [Description("已签约")]
        HA2 = 8,
        /// <summary>
        /// 报告填写中
        /// </summary>
        [Description("立案报告填写中")]
        HA3 = 16,
        /// <summary>
        /// 报告已提交
        /// </summary>
        [Description("立案报告已提交")]
        HA4 = 32,
        /// <summary>
        /// 立案结束
        /// </summary>
        [Description("立案结束")]
        HA5 = 64,
        /// <summary>
        /// 立案报告审批不通过
        /// </summary>
        [Description("立案报告审批不通过")]
        HA6 = 128,
        /// <summary>
        /// 已约审税限购时间
        /// </summary>
        [Description("已约审税限购时间")]
        ZB1 = 256,
        /// <summary>
        /// 已约审税限购时间（主办）
        /// </summary>
        [Description("已约审税限购时间")]
        ZB1A1 = 257,
        /// <summary>
        ///接受审税限购预约
        /// </summary>
        [Description("接受审税限购预约")]
        JY1 = 512,
        /// <summary>
        ///接受审税限购预约（主办）
        /// </summary>
        [Description("接受审税限购预约")]
        JY1A1 = 513,
        /// <summary>
        /// 已核价
        /// </summary>
        [Description("已核价")]
        JY2 = 1024,
        /// <summary>
        /// 已递交审税限购材料
        /// </summary>
        [Description("已递交审税限购材料")]
        JY3 = 2048,
        /// <summary>
        /// 限购查询中
        /// </summary>
        [Description("名下住房查询")]
        JY4 = 4096,
        /// <summary>
        /// 等出产证(他证)（贷款）
        /// </summary>
        [Description("等出产证(他证)")]
        JY5 = 8192,
        /// <summary>
        /// 等待预约过户
        /// </summary>
        [Description("等待预约过户")]
        JY6 = 16384,
        /// <summary>
        /// 客户已到场
        /// </summary>
        [Description("客户已到场")]
        JY7 = 32768,
        /// <summary>
        /// 等待过户
        /// </summary>
        [Description("等待过户")]
        JY8 = 65536,
        /// <summary>
        /// 等待过户（主办）
        /// </summary>
        [Description("等待过户")]
        JY8A1 = 65537,
        /// <summary>
        /// 已限购
        /// </summary>
        [Description("已限购")]
        JY9 = 131072,
        /// <summary>
        /// 税费已缴清
        /// </summary>
        [Description("税费已缴清")]
        JY10 = 262144,
        /// <summary>
        /// 等出产证(他证)（审税）
        /// </summary>
        [Description("等出产证(他证)")]
        JY11 = 524288,
        /// <summary>
        /// 过户完成
        /// </summary>
        [Description("过户完成")]
        JY12 = 1048576,
        /// <summary>
        /// 发起还贷申请
        /// </summary>
        [Description("发起还贷申请")]
        ZB3 = 2097152,
        /// <summary>
        /// 等待预约还款
        /// </summary>
        [Description("等待预约还款")]
        HD1 = 4194304,
        /// <summary>
        /// 等待预约还款（主办）
        /// </summary>
        [Description("等待预约还款")]
        HD1A1 = 4194305,
        /// <summary>
        /// 已确定还款时间
        /// </summary>
        [Description("已确定还款时间")]
        HD2 = 8388608,
        /// <summary>
        /// 卖方贷款已还清
        /// </summary>
        [Description("卖方贷款已还清")]
        HD3 = 16777216,
        /// <summary>
        /// 卖方贷款已结清
        /// </summary>
        [Description("卖方贷款已结清")]
        HD4 = 33554432,
        /// <summary>
        /// 卖方抵押已注销
        /// </summary>
        [Description("卖方抵押已注销")]
        HD5 = 67108864,
        /// <summary>
        /// 还贷完成
        /// </summary>
        [Description("还贷完成")]
        HD6 = 134217728,
        /// <summary>
        /// 发起贷款申请
        /// </summary>
        [Description("发起贷款申请")]
        DK1 = 268435456,
        /// <summary>
        /// 等待客户签约
        /// </summary>
        [Description("等待客户签约")]
        DK2 = 536870912,
        /// <summary>
        /// 等待客户签约（主办）
        /// </summary>
        [Description("等待客户签约")]
        DK2A1 = 536870913,
        /// <summary>
        /// 贷款合同签订
        /// </summary>
        [Description("贷款合同签订")]
        DK3 = 1073741824,
        /// <summary>
        /// 贷款银行审批中 
        /// </summary>
        [Description("贷款银行审批中")]
        DK4 = 2147483648,
        /// <summary>
        /// 贷款审批通过
        /// </summary>
        [Description("贷款审批通过")]
        DK5 = 4294967296,
        /// <summary>
        /// 贷款合同等送过户
        /// </summary>
        [Description("贷款合同等送过户")]
        DK6 = 8589934592,
        /// <summary>
        /// 等待银行放款
        /// </summary>
        [Description("等待银行放款")]
        DK7 = 17179869184,
        /// <summary>
        /// 银行已放款
        /// </summary>
        [Description("银行已放款")]
        DK8 = 34359738368,
        /// <summary>
        /// 审批不通过
        /// </summary>
        [Description("审批不通过")]
        DK9 = 68719476736,
        /// <summary>
        /// 审批不通过（主办）
        /// </summary>
        [Description("审批不通过")]
        DK9A1 = 68719476737,
        /// <summary>
        /// 等待预约审税
        /// </summary>
        [Description("等待预约审税")]
        ZB4 = 137438953472,
        /// <summary>
        /// 签约取消
        /// </summary>
        [Description("签约取消")]
        ZB5=274877906944,
        /// <summary>
        /// 案件中止
        /// </summary>
        [Description("案件中止")]
        ZB6 = 549755813888,
        /// <summary>
        /// 案件已中止
        /// </summary>
        [Description("案件已中止")]
        ZB7 = 2199023255552,
        /// <summary>
        /// 交房完成
        /// </summary>
        [Description("交房完成")]
        ZB8 = 4398046511104,
        /// <summary>
        /// 无贷款
        /// </summary>
        [Description("无贷款")]
        ZB9 = 8796093022208,
        /// <summary>
        /// 无抵押
        /// </summary>
        [Description("无抵押")]
        ZB10 = 17592186044416,
        /// <summary>
        /// 贷款申请已取消
        /// </summary>
        [Description("贷款申请已取消")]
        ZB11 = 35184372088832,
        /// <summary>
        /// 卖方还贷已取消
        /// </summary>
        [Description("卖方还贷已取消")]
        ZB12 = 70368744177664,
        /// <summary>
        /// 贷款资料补齐中
        /// </summary>
        [Description("贷款资料补齐中")]
        DK10 = 140737488355328,

        /// <summary>
        /// 等出产证(他证)（主办）
        /// </summary>
        [Description("等出产证(他证)")]
        JY11A1 = 281474976710656,

        /// <summary>
        /// 结案
        /// </summary>
        [Description("结案")]
        ZB13 = 562949953421312
    }

    /// <summary>
    /// 案件用户类型
    /// </summary>
    public enum CaseUserType
    {
        /// <summary>
        /// 卖方
        /// </summary>
        Seller = 0,
        /// <summary>
        /// 买方
        /// </summary>
        Buyer = 1,
        /// <summary>
        /// 中介(经纪人)
        /// </summary>
        AgentStaff = 2,
        /// <summary>
        /// 受理人
        /// </summary>
        Handler = 3,
    }

    public enum TradeType
    {
        /// <summary>
        /// 二手房
        /// </summary>
        [Description("二手房")]
        Normal = 1,
        /// <summary>
        /// 拍卖
        /// </summary>
        [Description("一手房代理")]
        Second = 2,
        /// <summary>
        /// 其他
        /// </summary>
        [Description("其他")]
        Other = 3,
        /// <summary>
        /// 未知
        /// </summary>
       [Description("")]
        Unknow = 0
    }

    public enum BankAccountPurpose
    {
        /// <summary>
        /// 其他
        /// </summary>
        Other = 0,
        /// <summary>
        /// 收款账户
        /// </summary>
        Receive = 1,
        /// <summary>
        /// 付款账户
        /// </summary>
        Payment = 2
    }

    /// <summary>
    /// 查询关键字类型
    /// </summary>
    public enum QueryStringType
    {
        /// <summary>
        /// 案件id
        /// </summary>
        CaseId=0,
        /// <summary>
        /// 产权号
        /// </summary>
        TenementContract=1,
        /// <summary>
        /// 物业地址
        /// </summary>
        TenementAddress=2,
    }

    /// <summary>
    /// 委托领证主体类型
    /// </summary>
    public enum DelegationType
    {
        /// <summary>
        /// 买家本身
        /// </summary>
        [Description("买家本身")]
        Self=0,
        /// <summary>
        /// 贷款银行
        /// </summary>
        [Description("贷款银行")]
        LoanBank=1,
        /// <summary>
        /// 公积金中心
        /// </summary>
        [Description("公积金中心")]
        Bourse = 2,
        /// <summary>
        /// 交易员
        /// </summary>
        [Description("交易员")]
        Trader = 3,
    }
    /// <summary>
    /// 房屋类型
    /// </summary>
    public enum ResideType 
    {
        /// <summary>
        /// 未知
        /// </summary>
        [Description("未知")]
        Unknow = 0,
        /// <summary>
        /// 普通住房
        /// </summary>
        [Description("普通住房")]
        Normal = 1,
        /// <summary>
        /// 非普通住房
        /// </summary>
        [Description("非普通住房")]
        UnNormal = 2,
        /// 非普通住房
        /// </summary>
        [Description("非居住用房")]
        UnReside = 3
    }

    /// <summary>
    /// 案件来源
    /// </summary>
    public enum SourceType
    {
        /// <summary>
        /// 受理中心
        /// </summary>
        [Description("受理中心")]
        ReceptionCenter=0,
        /// <summary>
        /// E房钱
        /// </summary>
      [Description("E房钱")]
        EHouseMoney=1,
    }

    /// <summary>
    /// 满几年
    /// </summary>
    public enum BuyOverYears
    {
        /// <summary>
        /// 未知
        /// </summary>
        [Description("未知")]
        Unknow = 0,
        /// <summary>
        /// 两年
        /// </summary>
        [Description("两年")]
        Two = 2,
        /// <summary>
        /// 五年
        /// </summary>
        [Description("五年")]
        Five = 5
    }
}
