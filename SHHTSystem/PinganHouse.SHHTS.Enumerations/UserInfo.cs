﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Enumerations
{
    public enum UserType
    {
        /// <summary>
        /// 上下家客户
        /// </summary>
        Customer = 0,
        /// <summary>
        /// 中介(经纪人)
        /// </summary>
        AgentStaff = 1,
        /// <summary>
        /// 平安员工
        /// </summary>
        PinganStaff = 2,
        /// <summary>
        /// 第三方公司员工
        /// </summary>
        ThirdpartyUser = 3
    }

    public enum CertificateType
    {
        /// <summary>
        /// 身份证
        /// </summary>
        [Description("身份证")]
        IdCard = 0,
        /// <summary>
        /// 护照
        /// </summary>
        [Description("护照")]
        Passport = 1,

        /// <summary>
        /// 台胞证
        /// </summary>
        //[Description("台胞证")]
        //MTP = 2,

        /// <summary>
        /// 港澳居民身份证
        /// </summary>
        [Description("港澳居民身份证")]
        HKMacaoIdCard = 2,

        /// <summary>
        /// 出生证
        /// </summary>
        [Description("出生证")]
        BirthCertificate = 3,

        /// <summary>
        /// 港澳居民往来大陆通行证
        /// </summary>
        [Description("港澳居民往来大陆通行证")]
        MacaoPermit = 4,

        /// <summary>
        /// 台湾居民往来大陆通行证
        /// </summary>
        [Description("台湾居民往来大陆通行证")]
        TaiwanPermit = 5,

        /// <summary>
        /// 军官证
        /// </summary>
        [Description("军官证")]
        CertificateOfOfficers = 6,

        /// <summary>
        /// 户口簿
        /// </summary>
        [Description("户口簿")]
        ResidenceBooklet = 7

        /// <summary>
        /// 回乡证
        /// </summary>
        //[Description("回乡证")]
        //ReentryPermit = 4
    }

    public enum CustomerType
    {
        /// <summary>
        /// 卖家/上家
        /// </summary>
        Seller = 0,
        /// <summary>
        /// 买家/下家
        /// </summary>
        Buyer = 1
    }

    public enum CustomerNature
    {
        /// <summary>
        /// 未知
        /// </summary>
        Unkonw = 0,
        /// <summary>
        /// 个人
        /// </summary>
        Personal = 1,
        /// <summary>
        /// 法人
        /// </summary>
        Corporation = 2
    }

    /// <summary>
    /// 贷款类型
    /// </summary>
    public enum CreditType
    {
        /// <summary>
        /// 商业贷款
        /// </summary>
        [Description("商业贷款")]
        Business = 1,
        /// <summary>
        /// 公积金
        /// </summary>
        [Description("住房公积金")]
        HousingFund = 2,
        /// <summary>
        /// 组合贷款
        /// </summary>
        [Description("组合贷款")]
        Combination = 3,
        /// <summary>
        /// 其它
        /// </summary>
        [Description("未知")]
        Unknow = 0
    }

    /// <summary>
    /// 自办贷款原因
    /// </summary>
    public enum SelfCreditReson
    {
        /// <summary>
        /// 未知
        /// </summary>
        [Description("未知")]
        Unknow = 0,
        /// <summary>
        /// 自有渠道
        /// </summary>
        [Description("自有渠道")]
        SelfChannel = 1,
        /// <summary>
        /// 中介引导
        /// </summary>
        [Description("中介指导")]
        AgentRecommend = 2,
        /// <summary>
        /// 其他
        /// </summary>
        [Description("其他")]
        Other = 3
    }

    //public enum LogOnType 
    //{
    //    /// <summary>
    //    /// 系统验证
    //    /// </summary>
    //    System =0,
    //    /// <summary>
    //    /// 平安登陆系统
    //    /// </summary>
    //    Pingan = 1
    //}

    public enum Gender
    {
        /// <summary>
        /// 未知
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// 男
        /// </summary>
        [Description("男")]
        Male = 1,
        /// <summary>
        /// 女
        /// </summary>
        [Description("女")]
        Female = 2
    }

    /// <summary>
    /// 贷款申请对象
    /// </summary>
    public enum LoanObject
    {
        /// <summary>
        /// 其它
        /// </summary>
        [Description("其它")]
        Other = 0,
        /// <summary>
        /// 银行
        /// </summary>
        [Description("银行")]
        Bank = 1,
        /// <summary>
        /// 专员
        /// </summary>
        [Description("贷款专员")]
        Commissioner = 2
    }


    /// <summary>
    /// 地址类型
    /// </summary>
    public enum AddressType
    {
        /// <summary>
        /// 家庭地址
        /// </summary>
        [Description("家庭地址")]
        Home = 0,
        /// <summary>
        /// 公司地址
        /// </summary>
        [Description("公司地址")]
        Company = 1
    }

    /// <summary>
    /// 邮寄时间类型
    /// </summary>
    public enum PostTimeType
    {
        /// <summary>
        /// 不限
        /// </summary>
        [Description("不限")]
        NoLimit = 0,
        /// <summary>
        /// 工作日
        /// </summary>
        [Description("周一到周五")]
        Workaday = 1,
        /// <summary>
        /// 休息时间
        /// </summary>
        [Description("周六周日")]
        Weekend = 2
    }
    /// <summary>
    /// 时间类型
    /// </summary>
    public enum PostTime
    {
        /// <summary>
        /// 不限
        /// </summary>
        [Description("不限")]
        NoLimit = 0,
        /// <summary>
        /// 白天
        /// </summary>
        [Description("9：00-18：00")]
        Daytime = 1,
        /// <summary>
        /// 夜晚
        /// </summary>
        [Description("18：00-22：00")]
        Night = 2
    }

    public enum ChangeFundType 
    {
        /// <summary>
        /// 收款
        /// </summary>
        Collection = 0,

        /// <summary>
        /// 付款
        /// </summary>
        Payment =1
    }
}
