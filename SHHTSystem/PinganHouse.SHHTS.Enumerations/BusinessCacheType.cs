﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.Enumerations
{
    public enum BusinessCacheType
    {
        /// <summary>
        /// 登录
        /// </summary>
        [Description("15")]
        Login=0,
        /// <summary>
        /// 短信
        /// </summary>
        [Description("2")]
        SMS=1,
        /// <summary>
        /// 绑定身份
        /// </summary>
        [Description("15")]
        BindIdentity=2,
    }
}
