﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Enumerations
{
    /// <summary>
    /// 操作类型
    /// </summary>
    public enum OperationType
    {
        /// <summary>
        /// 未知
        /// </summary>
        UnKown=0,
        /// <summary>
        /// 案件
        /// </summary>
        Case=1,
    }
}
