﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Enumerations
{
    /// <summary>
    /// 中介公司合作状态
    /// </summary>
    public enum AgentJointStatus 
    {
        /// <summary>
        /// 未知
        /// </summary>
        None = 0,
        /// <summary>
        /// 合作中
        /// </summary>
        Jointing = 1,
        /// <summary>
        /// 已解约
        /// </summary>
        Cancelled = 2
    }
}
