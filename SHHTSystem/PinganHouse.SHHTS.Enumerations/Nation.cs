﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Enumerations
{
    public enum Nation
    {
        未知 = 0,
        阿昌族 = 1,
        鄂温克族 = 2,
        傈僳族 = 3,
        水族 = 4,
        白族 = 5,
        高山族 = 6,
        珞巴族 = 7,
        塔吉克族 = 8,
        保安族 = 9,
        仡佬族 = 10,
        满族 = 11,
        塔塔尔族 = 12,
        布朗族 = 13,
        哈尼族 = 14,
        毛南族 = 15,
        土家族 = 16,
        布依族 = 17,
        哈萨克族 = 18,
        门巴族 = 19,
        土族 = 20,
        朝鲜族 = 21,
        汉族 = 22,
        蒙古族 = 23,
        佤族 = 24,
        达斡尔族 = 25,
        赫哲族 = 26,
        苗族 = 27,
        维吾尔族 = 28,
        傣族 = 29,
        回族 = 30,
        仫佬族 = 31,
        乌孜别克族 = 32,
        德昂族 = 33,
        基诺族 = 34,
        纳西族 = 35,
        锡伯族 = 36,
        东乡族 = 37,
        京族 = 38,
        怒族 = 39,
        瑶族 = 40,
        侗族 = 41,
        景颇族 = 42,
        普米族 = 43,
        彝族 = 44,
        独龙族 = 45,
        柯尔克孜族 = 46,
        羌族 = 47,
        裕固族 = 48,
        俄罗斯族 = 49,
        拉祜族 = 50,
        撒拉族 = 51,
        藏族 = 52,
        鄂伦春族 = 53,
        黎族 = 54,
        畲族 = 55,
        壮族 = 56
    }
}
