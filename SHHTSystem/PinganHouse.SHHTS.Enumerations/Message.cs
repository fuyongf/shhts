﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Enumerations
{
    public enum SmsCategory
    {
        /// <summary>
        /// 注册认证短信
        /// </summary>
        REG = 0,
        /// <summary>
        /// 更改密码短信
        /// </summary>
        UPDATE_PASSWORD = 1,
        /// <summary>
        /// 解绑短信
        /// </summary>
        UNBIND = 2,
        /// <summary>
        /// 绑定短信
        /// </summary>
        BIND = 3,
        /// <summary>
        /// 签约短信
        /// </summary>
        SIGN = 4,
        /// <summary>
        /// 交房短信佣金
        /// </summary>
        JIAOFANG = 5,
        /// <summary>
        /// 贷款材料短信
        /// </summary>
        DK_CL = 6,
        /// <summary>
        /// 交易材料短信
        /// </summary>
        JY_CL = 7,
        /// <summary>
        /// 交易案件预约短信
        /// </summary>
        JYANJ_YUYUE = 8,
        /// <summary>
        /// 过户短信
        /// </summary>
        GUOHU = 9,
        /// <summary>
        /// 收据寄出提醒短信
        /// </summary>
        SHOUJU_SEND = 10,
        /// <summary>
        /// 代收款转账短信
        /// </summary>
        DS_ZHUANGZHANG = 11,
        /// <summary>
        /// 代收款代扣短信
        /// </summary>
        DS_DAIKOU = 12,
        /// <summary>
        /// 代付款确认短信
        /// </summary>
        DS_QUEREN = 13,
        /// <summary>
        /// 立案确认短信
        /// </summary>
        LIAN_QUEREN = 14,
        /// <summary>
        /// 业主还贷预约确认代1短信
        /// </summary>
        HUANDAI_YUYUE_QUEREN1 = 15,
        /// <summary>
        /// 限购结果短信
        /// </summary>
        XIANGOUJIEGUO = 16,
        /// <summary>
        /// 交房短信
        /// </summary>
        JIAOFANG_NOTICE = 17,
        /// <summary>
        /// 领产证短信
        /// </summary>
        LINGZHENG = 18,
        /// <summary>
        /// 交易过户
        /// </summary>
        JIAOYI_GUOHU = 19,
        /// <summary>
        /// 贷款审批通过短信
        /// </summary>
        DAIKUANG_PASS = 20,
        /// <summary>
        /// 预约提前还贷2短信
        /// </summary>
        YUYUE_TQ_HUANDAI = 21,
        /// <summary>
        /// 预约提前还贷3短信
        /// </summary>
        YUYUE_TQ_HUANDAI3 = 22,
        /// <summary>
        /// 自行预约还贷3短信
        /// </summary>
        ZX_YUYUE_HUANDAI3 = 23,
        /// <summary>
        /// 自行预约还贷2短信
        /// </summary>
        ZX_YUYUE_HUANDAI2 = 24,
        /// <summary>
        /// 自行预约还贷1短信
        /// </summary>
        ZX_YUYUE_HUANDAI1 = 25,
        /// <summary>
        /// 签约完成
        /// </summary>
        QY_FINISH = 26,
        /// <summary>
        /// 预约房屋核价
        /// </summary>
        YY_HJ = 27,
        /// <summary>
        /// 预约房产税认定
        /// </summary>
        YY_FCS_RD = 28,
        /// <summary>
        /// 提交房屋状况查询 
        /// </summary>
        TJ_FW_CHAXUN = 29,
        /// <summary>
        /// 房屋交易中止
        /// </summary>
        FW_JIAOYI_END = 30,
        /// <summary>
        /// 预约办理过户
        /// </summary>
        YY_BL_GUOHU = 31,
        /// <summary>
        /// 买方完成房屋交易的全部手续
        /// </summary>
        BUY_WC_FW_JY = 32,
        /// <summary>
        /// 卖方完成房屋交易的全部手续
        /// </summary>
        MF_WC_FW_JY = 33,
        /// <summary>
        /// 签署贷款申请协议 
        /// </summary>
        QS_DK_SQ_XY = 34,
        /// <summary>
        /// 贷款申请通过 
        /// </summary>
        DAIKUANG_SQ_OK = 35,
        /// <summary>
        /// 贷款已放款
        /// </summary>
        DAIKUANG_SEND = 36,
        /// <summary>
        /// 取消贷款   
        /// </summary>
        QUXIAO_DAIKUANG = 37,
        /// <summary>
        /// 代收款项
        /// </summary>
        DAISHOU_KUANGXIANG=38,
        /// <summary>
        /// 转付房款
        /// </summary>
        ZHUANFU_FANGKUANG=39,
        /// <summary>
        /// 转付佣金
        /// </summary>
        ZHUANFU_YONGJ=40,
        /// <summary>
        /// 代付税费
        /// </summary>
        DAIFU_SHUIFEI=41,
        /// <summary>
        /// 用户支付电商服务费 
        /// </summary>
        USER_PAY_FWFEI=42,
        /// <summary>
        /// 卖方收房款（未扣除电商服务费）
        /// </summary>
        MFFK_NO_KCHU_FWF=43,
        /// <summary>
        /// 卖方收房款（扣除电商服务费）
        /// </summary>
        MFFK_KCHU_FWF=44,
        /// <summary>
        /// 退款
        /// </summary>
        TUIKUANG=45,
        /// <summary>
        /// 动态验证码
        /// </summary>
        USER_GET_OTHER_CODE=46,
    }
}
