﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Enumerations
{
    public enum AccountType
    {
        /// <summary>
        /// 卖方
        /// </summary>
        [Description("卖方")]
        Seller = 0,
        /// <summary>
        /// 买方
        /// </summary>
        [Description("买方")]
        Buyer = 1,
        /// <summary>
        /// 中介
        /// </summary>
        [Description("中介")]
        Agency = 2,
        /// <summary>
        /// 第三方
        /// </summary>
        [Description("第三方")]
        Thirdparty = 3,
        /// <summary>
        /// 自然人
        /// </summary>
        [Description("自然人")]
        Person = 4
    }

    /// <summary>
    /// 子账户类型
    /// </summary>
    public enum AccountCategory
    {
        /// <summary>
        /// 无，主账户
        /// </summary>
        None = 0,
        /// <summary>
        /// 储值
        /// </summary>
        [Description("储值")]
        Recharge = 1,
        /// <summary>
        /// 房款
        /// </summary>
        [Description("房款")]
        HosingFund = 2,
        /// <summary>
        /// 税费
        /// </summary>
        [Description("税费")]
        Taxes = 3,
        /// <summary>
        /// 中介费
        /// </summary>
        [Description("中介佣金")]
        AgencyFee = 4,
        /// <summary>
        /// 服务费
        /// </summary>
        [Description("其它服务费")]
        ServiceCharge = 5,
        /// <summary>
        /// 装修补偿
        /// </summary>
        [Description("装修补偿款")]
        Compensation = 6,
        // <summary>
        /// 壹房钱电商服务费
        /// </summary>
        [Description("E房钱电商服务费")]
        EFQServiceCharge = 7
    }

    public enum BankCardType
    {
        /// <summary>
        /// 未知
        /// </summary>
        Unknow = 0,
        /// <summary>
        /// 借记
        /// </summary>
        Debit = 1,
        /// <summary>
        /// 贷记
        /// </summary>
        Credit = 2
    }

    public enum BankAccountNature
    {
        /// <summary>
        /// 未知
        /// </summary>
        Unknow = 0,
        /// <summary>
        /// 个人
        /// </summary>
        Personal = 1,
        /// <summary>
        /// 企业
        /// </summary>
        Corporate = 2
    }

    public enum AccountOperationType
    {
        /// <summary>
        /// 入账
        /// </summary>
        Entry = 0,
        /// <summary>
        /// 出账
        /// </summary>
        ChargeOff = 1,
        /// <summary>
        /// 锁定
        /// </summary>
        Lock = 2,
        /// <summary>
        /// 解锁
        /// </summary>
        UnLock = 3,
        /// <summary>
        /// 冻结
        /// </summary>
        Frozen = 4,
        /// <summary>
        /// 解冻
        /// </summary>
        UnFrozen = 5,
        /// <summary>
        /// 锁定账户
        /// </summary>
        LockAccount = 6,
        /// <summary>
        /// 解锁账户
        /// </summary>
        UnLockAccount = 7
    }

    public enum AccountDetailType 
    {
        /// <summary>
        /// 充值
        /// </summary>
        Recharge = 0,
        /// <summary>
        /// 转账
        /// </summary>
        Transfer = 1,
        /// <summary>
        /// 提现
        /// </summary>
        Withdraw =2,
        /// <summary>
        /// 退款
        /// </summary>
        Refund =3
    }

    ///// <summary>
    ///// 资金走向
    ///// </summary>
    //public enum FundDirection
    //{
    //    /// <summary>
    //    /// 未知
    //    /// </summary>
    //    [Description("未知")]
    //    Unknow = 0,
    //    /// <summary>
    //    /// 卖家
    //    /// </summary>
    //    [Description("业主")]
    //    Seller = 1,
    //    /// <summary>
    //    /// 买家
    //    /// </summary>
    //    [Description("买家")]
    //    Buyer = 2,
    //    /// <summary>
    //    /// 中介
    //    /// </summary>
    //    [Description("中介")]
    //    Agency = 3,
    //    /// <summary>
    //    /// 平安
    //    /// </summary>
    //    [Description("平安好房")]
    //    Pingan = 4,
    //    /// <summary>
    //    /// 无
    //    /// </summary>
    //    [Description("无")]
    //    None = 5
    //}
}
