﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Enumerations
{
    public enum FundPlanPayChannel
    {

        /// <summary>
        /// 未知
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// 托管
        /// </summary>
        Trusteeship = 1,
        /// <summary>
        /// 贷款
        /// </summary>
        Loan  = 2,
        /// <summary>
        /// 自付
        /// </summary>
        SelfPay = 3
    }
}
