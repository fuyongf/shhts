﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Enumerations
{
    public enum ReceptionCenter 
    {
        /// <summary>
        /// 未知
        /// </summary>
        [Description("未知")]
        Unkonw = 0,
        /// <summary>
        /// 浦东
        /// </summary>
        [Description("浦东受理中心")]
        PuDong = 1,
        /// <summary>
        /// 浦东
        /// </summary>
        [Description("宝山受理中心")]
        BaoShan = 2,
    }

    public enum Duty 
    {
        /// <summary>
        /// 未知
        /// </summary>
        Unkonw = 0,
        /// <summary>
        /// 管理者
        /// </summary>
        Manager =1,
        /// <summary>
        /// 受理中心主管
        /// </summary>
        Director =2,
        /// <summary>
        /// 预检
        /// </summary>
        YuJian =3,
        /// <summary>
        /// 核案
        /// </summary>
        HeAn = 4,
        /// <summary>
        /// 主办
        /// </summary>
        ZhuBan =5,
        /// <summary>
        /// 交易
        /// </summary>
        JiaoYi =6,
        /// <summary>
        /// 贷款
        /// </summary>
        DaiKuan =7
    }
}
