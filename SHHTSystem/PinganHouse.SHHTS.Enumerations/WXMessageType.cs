﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Enumerations
{

    /// <summary>
    /// 微信推送消息类型
    /// </summary>
    public enum WXMessageType
    {
        /// <summary>
        /// 文字
        /// </summary>
        Text = 0,
        /// <summary>
        /// 事件
        /// </summary>
        Event = 1,
        /// <summary>
        /// 图片
        /// </summary>
        Image = 2,
        /// <summary>
        /// 语音
        /// </summary>
        Voice = 3,
        /// <summary>
        /// 视频
        /// </summary>
        Video = 4,
        /// <summary>
        /// 小视频
        /// </summary>
        ShortVideo = 5,
        /// <summary>
        /// 地理位置
        /// </summary>
        Location = 6,
        /// <summary>
        /// 链接
        /// </summary>
        Link = 7
    }
    /// <summary>
    /// 事件消息推送
    /// </summary>

    public enum EventType
    {
        /// <summary>
        /// 取消关注
        /// </summary>
        UnSubscribe = 0,
        /// <summary>
        /// 关注
        /// </summary>
        Subscribe = 1,
        /// <summary>
        /// 扫描
        /// </summary>
        Scan = 2,
        /// <summary>
        /// 点击菜单
        /// </summary>
        Click = 3,
        /// <summary>
        /// 跳转链接
        /// </summary>
        View = 4,
        /// <summary>
        /// 模板消息发送结束
        /// </summary>
        TemplateSendJobFinish = 5,
        /// <summary>
        /// 上报地址
        /// </summary>
        Location = 6,       
    }

    public enum WXStatus
    {
        /// <summary>
        /// 已发送
        /// </summary>
        Sended=0,
        /// <summary>
        /// 发送成功
        /// </summary>
        Success=1,
        /// <summary>
        /// 用户拒绝
        /// </summary>
        UserBlock=2,
        /// <summary>
        /// 系统错误
        /// </summary>
        SystemFailed=3,
    }

    /// <summary>
    /// 回复微信消息类型
    /// </summary>
    public enum WXReplyMsgType
    { 
        /// <summary>
        /// 文字消息
        /// </summary>
        Text=0,
        /// <summary>
        /// 图文消息
        /// </summary>
        News=1,
        /// <summary>
        /// 图片消息
        /// </summary>
        Image=2,
        /// <summary>
        /// 语音消息
        /// </summary>
        Voice=3,
        /// <summary>
        /// 视频消息
        /// </summary>
        Video=4,
        /// <summary>
        /// 音乐消息
        /// </summary>
        Music=5,
    }

    /// <summary>
    /// 微信点击按钮类型
    /// </summary>
    public enum ClickEventType
    {
        /// <summary>
        /// 案件进度
        /// </summary>
        CaseProcess=0,
        /// <summary>
        /// 交易流水
        /// </summary>
        Trade=1,
        /// <summary>
        /// 联系我们
        /// </summary>
        Contact=2,
    }

    /// <summary>
    /// 企业应用类型
    /// </summary>
    public enum AgentType
    {
        /// <summary>
        /// 企业小助手
        /// 如果id为0，则表示是整个企业号的关注/取消关注事件
        /// </summary>
        ALL=0,
        /// <summary>
        /// 中介
        /// </summary>
        Agency=4,
        /// <summary>
        /// 中介公司
        /// </summary>
        AgentCompany=5,
    }

    /// <summary>
    /// 收件人类型
    /// </summary>
    public enum ReceiverType
    {
        /// <summary>
        /// 用户
        /// </summary>
        [Description("touser")]
        ToUser = 0,
        /// <summary>
        /// 部门
        /// </summary>
        [Description("toparty")]
        ToDepartment = 1,
        /// <summary>
        /// 标签
        /// </summary>
        [Description("totag")]
        ToTag = 2,
    }

    /// <summary>
    /// 微信企业成员关注状态
    /// </summary>
    [Flags]
    public enum WXQYUserStatus
    {
        /// <summary>
        /// 全部
        /// </summary>
        All=0,
        /// <summary>
        /// 已关注
        /// </summary>
        Subscribe = 1,
        /// <summary>
        /// 发送成功
        /// </summary>
        Freeze = 2,
        /// <summary>
        /// 未关注
        /// </summary>
        UnSubscribe = 4,
    }
}
