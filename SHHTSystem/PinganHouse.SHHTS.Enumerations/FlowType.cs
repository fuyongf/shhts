﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Enumerations
{
    /// <summary>
    /// 流水用途
    /// </summary>
    public enum FlowEffectType
    {
        /// <summary>
        /// 未知
        /// </summary>
        UnKown=0,
        /// <summary>
        /// 税费
        /// </summary>
        Tax=1,
        /// <summary>
        /// 房款
        /// </summary>
        HouseFund=2,
        /// <summary>
        /// 佣金
        /// </summary>
        Commission=3,
        /// <summary>
        /// 服务费
        /// </summary>
        Fee=4,
    }

    /// <summary>
    /// 流水类型
    /// </summary>
    public enum FlowType
    {
        /// <summary>
        /// 未知
        /// </summary>
        UnKown=0,
        /// <summary>
        /// 出款
        /// </summary>
        Out=1,
        /// <summary>
        /// 入款
        /// </summary>
        In=2,
    }
}
