﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Enumerations
{
    /// <summary>
    /// 案件进度
    /// </summary>
    public enum ProccessType
    {
        /// <summary>
        /// 正常
        /// </summary>
        [Description("正常")]
        Normal=0,
        /// <summary>
        /// 延误
        /// 黄色预警
        /// </summary>
        [Description("延误（黄色预警）")]
        AverageDelay = 1,
        /// <summary>
        /// 延误
        /// 红色预警
        /// </summary>
        [Description("延误（红色预警）")]
        SeriousDelay =2,
    }
}
