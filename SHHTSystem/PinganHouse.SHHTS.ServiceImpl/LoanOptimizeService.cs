﻿using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Activation;

namespace PinganHouse.SHHTS.ServiceImpl
{
    /// <summary>
    /// LoanOptimService
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class LoanOptimizeService : ILoanOptimizeService
    {

        /// <summary>
        /// 获取全部方案.
        /// </summary>
        /// <param name="ips">用户输入数据</param>
        /// <returns></returns>
        public IList<LoanOptimizePlan> GetPlans(OInputs ips)
        {
            IList<LoanOptimizePlan> list = new List<LoanOptimizePlan>();

            if (ips != null)
            {
                ips.Normalize();

                if (ips.UserRequire1.HasValue)
                    list = GetPlansScenario0(ips);

                if (ips.UserRequire2.HasValue)
                    list = GetPlansScenario1(ips);
            }
            return list;
        }

        #region Helpers

        #region Scenario 0
        /// <summary>
        /// 按已知客户需求贷款金额计算.
        /// </summary>
        /// <param name="ips">The ips.</param>
        private IList<LoanOptimizePlan> GetPlansScenario0(OInputs ips)
        {
            List<LoanOptimizePlan> plans = new List<LoanOptimizePlan>();
            BankPolicy gBkp = new BankPolicy("GJJ");//公积金银行策略
            //公积金套数认定
            TaoShuRd gTaoShu = P03P04(ips, gBkp);

            foreach (var sBkp in GetSBankPolicies())
            {
                LoanOptimizePlan plan = new LoanOptimizePlan();
                plan.BankCode = sBkp.Code;
                plan.BankName = sBkp.Name;
                plan.BankLogo = sBkp.Logo;
                //还款方式
                var repayType = ips.UserRequire4;

                //商业银行套数认定
                TaoShuRd sTaoShu = P03P04(ips, sBkp);
                //公积金是否准入
                var gPass = P01(ips, gBkp, sBkp);
                //商贷是否准入
                var sPass = P02(ips, sBkp);

                //都不通过，继续计算下一个银行
                if (!gPass && !sPass)
                    continue;

                //套数认定相同且公积金准入，进入公积金，否则进入商贷
                if (gTaoShu == sTaoShu && gPass)
                {
                    bool needSd;
                    var c = GetPlansScenario0_Gjj(ips, gBkp, gTaoShu, ref repayType, out needSd);
                    plan.TaoShu = gTaoShu;
                    plan.GAmount = c.Amount;
                    plan.GInterest = c.Interest;
                    plan.GRepay = c.Repay;
                    plan.GYears = c.Years;

                    if (needSd)
                    {
                        //需要商贷
                        if (sPass)
                        {
                            //商贷通过

                            //通过流程计算而来的商贷金额与用户需求1取低 - 公积金贷款金额
                            decimal sAmount4 = (P08(ips, sBkp, sTaoShu) > ips.UserRequire1.Value ? ips.UserRequire1.Value : P08(ips, sBkp, sTaoShu)) - c.Amount;

                            int sYears = P09(ips, sBkp);//商贷年限
                            int sYears3 = P09(ips, sBkp) > ips.UserRequire3 ? ips.UserRequire3 : P09(ips, sBkp); //商贷年限3
                            var gYears = P05(ips, gBkp);//公积金贷款年限 c.Years=公积金贷款年限3


                            while (true)
                            {
                                //商贷利率
                                var sInterest = P07P10(ips, sBkp, new Interest("SDLL"), sTaoShu, sYears3);
                                //公积金月供，因为还款方式和c.Years可能变更，需要重新计算公积金月还款额
                                var gRepay1 = CalculateMonthlyRepay(c.Amount, c.Years, c.Interest, repayType);
                                //商贷月供1
                                var sRepay1 = CalculateMonthlyRepay(sAmount4, sYears3, sInterest, repayType);

                                //商贷月供2（以收入水平为依据）
                                var sRepay2 = (ips.UserBasic2 + ips.Gjj2 + ips.Gjj5) / 2 - gRepay1;

                                if (sRepay1 - sRepay2 > 0)
                                {
                                    if (sRepay2 > 0)
                                    {
                                        //收入水平 可以覆盖 商贷月供+公积金月供，则调整还款方式、公积金年限、商贷年限，以使sRepay1 - sRepay2=0
                                        if (repayType == RepayType.ConstantAmortization)
                                        {
                                            repayType = RepayType.ConstantPayment;
                                            continue;
                                        }
                                        //先调公积金年限
                                        if (c.Years - gYears < 0)
                                        {
                                            c.Years++;
                                            continue;
                                        }
                                        //再调商业年限
                                        if (sYears3 - sYears < 0)
                                        {
                                            sYears3++;
                                            continue;
                                        }

                                        //公积金和商贷年限都达到上限时，根据商贷月供2反算商贷金额
                                        plan.GYears = c.Years;
                                        plan.GRepay = gRepay1;
                                        plan.SAmount = CalculateLoanAmount(sRepay2, sYears3, sInterest, repayType);
                                        plan.SRepay = sRepay2;
                                        plan.SYears = sYears3;
                                        plan.SInterest = sInterest;
                                        break;

                                    }
                                    else
                                    {
                                        //收入水平 无法覆盖 商贷月供+公积金月供，此时无法商贷，因此调整还款方式或公积金年限，以使sRepay1 - sRepay2=0
                                        if (repayType == RepayType.ConstantAmortization)
                                        {
                                            repayType = RepayType.ConstantPayment;
                                            continue;
                                        }

                                        if (c.Years - gYears < 0)
                                        {
                                            c.Years++;
                                            continue;
                                        }

                                        //公积金年限达到上限，输出
                                        plan.GYears = c.Years;
                                        plan.GRepay = gRepay1;
                                        break;

                                    }
                                }
                                else
                                {
                                    //商贷+公积金贷款刚好
                                    plan.SRepay = sRepay1;
                                    plan.SAmount = sAmount4;
                                    plan.SInterest = sInterest;
                                    plan.SYears = sYears3;
                                    break;
                                }
                            }

                        }
                    }
                }
                else if (sPass)
                {
                    plan.TaoShu = sTaoShu;
                    //商贷准入
                    var c = GetPlansScenario0_Sd(ips, sBkp, plan.TaoShu, ref repayType);

                    plan.SAmount = c.Amount;
                    plan.SInterest = c.Interest;
                    plan.SYears = c.Years;
                    plan.SRepay = c.Repay;
                }

                //最终的还款方式
                plan.RepayType = repayType;
                //修正输出及精度
                if (plan.GAmount == 0)
                {
                    plan.GInterest = 0;
                    plan.GYears = 0;
                }
                plan.GAmount = Math.Round(plan.GAmount, 1);
                plan.GInterest = Math.Round(plan.GInterest, 2);
                plan.GRepay = Math.Round(plan.GRepay, 2);
                if (plan.SAmount == 0)
                {
                    plan.SYears = 0;
                    plan.SInterest = 0;

                    //因为商业银行不接受纯公积金贷款，故纯公积金方案须将银行改为GJJ
                    plan.BankCode = gBkp.Code;
                    plan.BankName = gBkp.Name;
                    plan.BankLogo = gBkp.Logo;
                }
                plan.SAmount = Math.Round(plan.SAmount, 1);
                plan.SInterest = Math.Round(plan.SInterest, 2);
                plan.SRepay = Math.Round(plan.SRepay, 2);

                if (plans.FirstOrDefault(o => o.BankCode == plan.BankCode) == null)
                    plans.Add(plan);
            }

            //将公积金方案移到最前面
            var gjjPlan= plans.SingleOrDefault(o => o.BankCode == gBkp.Code);
            if(gjjPlan!=null)
            {
                plans.Remove(gjjPlan);
                plans.Insert(0, gjjPlan);
            }

            return plans;
        }

        /// <summary>
        /// 计算公积金
        /// </summary>
        /// <param name="ips">The ips.</param>
        /// <param name="bkp">The BKP.</param>
        /// <param name="taoShu">The tao shu.</param>
        /// <param name="repayType">Type of the repay.</param>
        /// <param name="needSd">if set to <c>true</c> [need sd].</param>
        /// <returns></returns>
        private CReturn GetPlansScenario0_Gjj(OInputs ips, BankPolicy bkp, TaoShuRd taoShu,
            ref RepayType repayType,out bool needSd)
        {
            CReturn c = new CReturn();
            var gYears = P05(ips, bkp);//公积金贷款年限
            c.Years = gYears; //贷款年限3

            if (c.Years > ips.UserRequire3)
                c.Years = ips.UserRequire3;

            needSd = false;

            while (true)
            {
                //公积金贷款金额
                c.Amount = P06(ips, bkp, taoShu, c.Years);

                //公积金利率
                c.Interest = P07P10(ips, bkp, new Interest("GJJLL"), taoShu, c.Years);
                //月供
                c.Repay = CalculateMonthlyRepay(c.Amount, c.Years, c.Interest, repayType);

                if (c.Amount - ips.UserRequire1 < 0)
                {
                    if (c.Years - gYears < 0)
                    {
                        //基本公积金贷款金额1（夫妻双方）
                        var jbgjjAmountMin = P0601(ips, bkp, taoShu, c.Years);
                        //基本公积金贷款金额2
                        var jbgjjAmount2 = P0602(ips, c.Years);
                        //基本公积金贷款金额3
                        if (jbgjjAmountMin > P0603(ips))
                            jbgjjAmountMin = P0603(ips);
                        //基本公积金贷款金额4
                        if (jbgjjAmountMin > P0604(ips, taoShu))
                            jbgjjAmountMin = P0604(ips, taoShu);

                        if (jbgjjAmount2 - jbgjjAmountMin < 0)
                        {
                            c.Years += 1;
                            continue;
                        }
                        else
                        {
                            needSd = true;
                            break;
                        }
                    }
                    else
                    {
                        needSd = true;
                        break;
                    }
                }
                else
                {
                    c.Amount = ips.UserRequire1.Value;
                    c.Repay = CalculateMonthlyRepay(c.Amount, c.Years, c.Interest, repayType);
                    break;
                }
            }

            return c;
        }

        /// <summary>
        /// 计算商贷.
        /// </summary>
        /// <param name="ips">The ips.</param>
        /// <param name="bkp">The BKP.</param>
        /// <param name="taoShu">The tao shu.</param>
        /// <param name="repayType">还款方式.</param>
        /// <returns></returns>
        private CReturn GetPlansScenario0_Sd(OInputs ips, BankPolicy bkp, TaoShuRd taoShu,
            ref RepayType repayType)
        {
            //需用repayType代替ips.UserRequire4，因为还款方式会变更

            CReturn c = new CReturn();

            c.Amount = P08(ips, bkp, taoShu);
            if (c.Amount > ips.UserRequire1)
                c.Amount = ips.UserRequire1.Value;
            var sYears=P09(ips, bkp);
            c.Years =sYears;
            if (c.Years > ips.UserRequire3)
                c.Years = ips.UserRequire3;

            decimal sRepay2 = (ips.UserBasic2 + ips.Gjj2 + ips.Gjj5) / 2;

            while (true)
            {
                c.Interest = P07P10(ips, bkp, new Interest("SDLL"), taoShu, c.Years);
                decimal sRepay1 = CalculateMonthlyRepay(c.Amount, c.Years, c.Interest, repayType);

                if (sRepay1 - sRepay2 <= 0)
                {
                    c.Repay = sRepay1;
                    break;
                }

                //sRepay1 - sRepay2 > 0

                if (repayType == RepayType.ConstantAmortization)
                {
                    repayType = RepayType.ConstantPayment;
                    continue;
                }

                if (c.Years - sYears < 0)
                {
                    c.Years += 1;
                    continue;
                }
                else
                {
                    c.Repay = sRepay2;
                    c.Amount = CalculateLoanAmount(c.Repay, c.Years, c.Interest, repayType);
                    break;
                }

            }

            return c;
        }
        #endregion

        #region Scenario 1

        private IList<LoanOptimizePlan> GetPlansScenario1(OInputs ips)
        {
            List<LoanOptimizePlan> plans = new List<LoanOptimizePlan>();
            BankPolicy gBkp = new BankPolicy("GJJ");//公积金银行策略
            //公积金套数认定
            TaoShuRd gTaoShu = P03P04(ips, gBkp);

            foreach (var sBkp in GetSBankPolicies())
            {
                LoanOptimizePlan plan = new LoanOptimizePlan();
                plan.BankCode = sBkp.Code;
                plan.BankName = sBkp.Name;
                plan.BankLogo = sBkp.Logo;
                plan.RepayType = ips.UserRequire4;

                //公积金是否准入
                var gPass = P01(ips, gBkp, sBkp);
                //商业银行套数认定
                TaoShuRd sTaoShu = P03P04(ips, sBkp);
                //商贷是否准入
                var sPass = P02(ips, sBkp);

                //都不通过，继续计算下一个银行
                if (!gPass && !sPass)
                    continue;

                //套数认定相同且公积金准入，进入公积金，否则进入商贷
                if (gTaoShu == sTaoShu && gPass)
                {
                    //公积金
                    plan.TaoShu = gTaoShu;

                    var c = GetPlansScenario1_Gjj(ips, gBkp, plan.TaoShu);
                    plan.GAmount = c.Amount;
                    plan.GRepay = c.Repay;
                    plan.GYears = c.Years;
                    plan.GInterest = c.Interest;

                    if (ips.UserRequire2 - plan.GRepay > 0)
                    {
                        var repay1 = (ips.UserBasic2 + ips.Gjj2 + ips.Gjj5) / 2;
                        if (repay1 > ips.UserRequire2)
                            repay1 = ips.UserRequire2.Value;

                        if (repay1 - plan.GRepay > 0 && sPass)
                        {
                            c = GetPlansScenario1_Sd(ips, sBkp, plan.TaoShu, repay1 - plan.GRepay, plan.GAmount);
                            plan.SAmount = c.Amount;
                            plan.SInterest = c.Interest;
                            plan.SYears = c.Years;
                            plan.SRepay = c.Repay;
                        }

                    }
                    else
                    {
                        plan.GRepay = ips.UserRequire2.Value;
                        plan.GAmount = CalculateLoanAmount(plan.GRepay, plan.GYears, plan.GInterest, plan.RepayType);
                    }
                }
                else if (sPass)
                {
                    plan.TaoShu = sTaoShu;
                    //商贷准入
                    var c = GetPlansScenario1_Sd(ips, sBkp, plan.TaoShu);

                    plan.SAmount = c.Amount;
                    plan.SInterest = c.Interest;
                    plan.SYears = c.Years;
                    plan.SRepay = c.Repay;
                }


                //修正输出及精度
                if (plan.GAmount == 0)
                {
                    plan.GInterest = 0;
                    plan.GYears = 0;
                }
                plan.GAmount = Math.Round(plan.GAmount, 1);
                plan.GInterest = Math.Round(plan.GInterest, 2);
                plan.GRepay = Math.Round(plan.GRepay, 2);
                if (plan.SAmount == 0)
                {
                    plan.SYears = 0;
                    plan.SInterest = 0;

                    //因为商业银行不接受纯公积金贷款，故纯公积金方案须将银行改为GJJ
                    plan.BankCode = gBkp.Code;
                    plan.BankName = gBkp.Name;
                    plan.BankLogo = gBkp.Logo;
                }
                plan.SAmount = Math.Round(plan.SAmount, 1);
                plan.SInterest = Math.Round(plan.SInterest, 2);
                plan.SRepay = Math.Round(plan.SRepay, 2);

                if (plans.FirstOrDefault(o => o.BankCode == plan.BankCode) == null)
                    plans.Add(plan);
            }

            //将公积金方案移到最前面
            var gjjPlan = plans.SingleOrDefault(o => o.BankCode == gBkp.Code);
            if (gjjPlan != null)
            {
                plans.Remove(gjjPlan);
                plans.Insert(0, gjjPlan);
            }
            return plans;
        }


        /// <summary>
        /// 计算公积金
        /// </summary>
        /// <param name="ips">The ips.</param>
        /// <param name="bkp">The BKP.</param>
        /// <param name="taoShu">The tao shu.</param>
        /// <returns></returns>
        private CReturn GetPlansScenario1_Gjj(OInputs ips, BankPolicy bkp, TaoShuRd taoShu)
        {
            CReturn c = new CReturn();
            c.Years = P05(ips, bkp);
            if (c.Years > ips.UserRequire3)
                c.Years = ips.UserRequire3;
            c.Interest = P07P10(ips, bkp, new Interest("GJJLL"), taoShu, c.Years);
            c.Amount = P06(ips, bkp, taoShu, c.Years);
            c.Repay = CalculateMonthlyRepay(c.Amount, c.Years, c.Interest, ips.UserRequire4);

            return c;
        }

        /// <summary>
        /// 计算商贷.
        /// </summary>
        /// <param name="ips">The ips.</param>
        /// <param name="bkp">The BKP.</param>
        /// <param name="taoShu">The tao shu.</param>
        /// <param name="sRepay">外部指定的商贷月还款额，未指定则在方法内计算.</param>
        /// <param name="gAmount">外部输入的公积金贷款额</param>
        /// <returns></returns>
        private CReturn GetPlansScenario1_Sd(OInputs ips, BankPolicy bkp, TaoShuRd taoShu, 
            decimal? sRepay = null, decimal? gAmount=null)
        {
            CReturn c = new CReturn();
            c.Years = P09(ips, bkp);
            if (c.Years > ips.UserRequire3)
                c.Years = ips.UserRequire3;
            c.Interest = P07P10(ips, bkp, new Interest("SDLL"), taoShu, c.Years);

            if (sRepay == null)
            {
                c.Repay = ips.UserRequire2.Value;
                var repay1 = (ips.UserBasic2 + ips.Gjj2 + ips.Gjj5) / 2;
                if (c.Repay > repay1)
                    c.Repay = repay1;
            }
            else
                c.Repay = sRepay.Value;

            c.Amount = P08(ips, bkp, taoShu);
            var amount1 = CalculateLoanAmount(c.Repay, c.Years, c.Interest, ips.UserRequire4);
            if (c.Amount > amount1)
                c.Amount = amount1;

            if (gAmount != null)
            {
                //最高可贷款的商贷额=合同总价*成数-公积金
                var amount2 = ips.UserBasic3 * (taoShu == TaoShuRd.First ? bkp.GetValueDecimal(2) : bkp.GetValueDecimal(4)) - gAmount;

                //取低
                if (c.Amount > amount2)
                    c.Amount = amount2.Value;
            }

            //重算月供
            c.Repay = CalculateMonthlyRepay(c.Amount, c.Years, c.Interest, ips.UserRequire4);

            return c;
        }
        
        #endregion


        /// <summary>
        /// 计算（首）月还款金额
        /// </summary>
        /// <param name="loan">贷款总额（万元）.</param>
        /// <param name="years">贷款年限.</param>
        /// <param name="interest">年利率.</param>
        /// <param name="type">还款方式.</param>
        /// <returns></returns>
        private decimal CalculateMonthlyRepay(decimal loan, int years, decimal interest, RepayType type)
        {
            loan = loan * 10000;
            interest = interest / 100;

            if (type == RepayType.ConstantPayment)
                return (loan * (interest / 12) * (decimal)Math.Pow(1 + (double)(interest / 12), years * 12)) / ((decimal)Math.Pow(1 + (double)(interest / 12), years * 12) - 1);

            return loan / (years * 12) + loan * interest / 12 ;
        }

        /// <summary>
        /// 根据月供计算贷款总额（万元）
        /// </summary>
        /// <param name="repay">贷款月供.</param>
        /// <param name="years">贷款年限.</param>
        /// <param name="interest">年利率.</param>
        /// <param name="type">还款方式.</param>
        /// <returns></returns>
        private decimal CalculateLoanAmount(decimal repay, int years, decimal interest, RepayType type)
        {
            interest = interest / 100;

            if (type == RepayType.ConstantPayment)
                return ((repay * ((decimal)Math.Pow((1 + (double)(interest / 12)), years * 12) - 1)) / ((decimal)(interest / 12) * (decimal)Math.Pow((1 + (double)(interest / 12)), years * 12))) / 10000;

            return (years * 12 * repay / (1 + years * 12 * (interest / 12))) / 10000;
        }


        /// <summary>
        /// 获取商业银行策略列表
        /// </summary>
        /// <param name="ips">The ips.</param>
        /// <returns></returns>
        private IList<BankPolicy> GetSBankPolicies()
        {
            IList<BankPolicy> bps = new List<BankPolicy>();
            foreach (var bpi in ServiceDepository.SettingService.GetSettingItemsByPath("/Biz/LoanOptimize/BankPolicy"))
            {
                //跳过公积金
                if (bpi.Name == "GJJ")
                    continue;

                bps.Add(new BankPolicy(bpi.Name));
            }

            return bps;
        }


        /// <summary>
        /// 银行政策
        /// </summary>
        private class BankPolicy
        {
            const string SettingRootPath = "/Biz/LoanOptimize/BankPolicy";

            /// <summary>
            /// 银行政策
            /// </summary>
            /// <param name="bcd">银行代码</param>
            public BankPolicy(string bcd)
            {
                var item = ServiceDepository.SettingService.GetSettingItemByPath(BuildSettingPath(bcd));

                Code = item.Name;
                Name = item.Description;
                Logo = item.Value;
            }



            private string BuildSettingPath(params string [] vps)
            {
                return string.Join("/", SettingRootPath, string.Join("/", vps));
            }

            /// <summary>
            /// 获取银行代码
            /// </summary>
            public string Code { get; private set; }

            /// <summary>
            /// 获取银行名称
            /// </summary>
            public string Name { get; private set; }

            /// <summary>
            /// 获取银行Logo图片地址
            /// </summary>
            public string Logo { get; private set; }

            /// <summary>
            /// 获取策略的值
            /// </summary>
            /// <param name="pid">策略索引</param>
            /// <returns></returns>
            public string GetValueString(int pid)
            {
                return ServiceDepository.SettingService.GetSettingItemValueByPath(BuildSettingPath(Code, pid.ToString()));
            }

            /// <summary>
            /// 获取策略的值
            /// </summary>
            /// <param name="pid">策略索引</param>
            /// <returns></returns>
            public int GetValueInt(int pid)
            {
                return int.Parse(GetValueString(pid));
            }

            /// <summary>
            /// 获取策略的值
            /// </summary>
            /// <param name="pid">策略索引</param>
            /// <returns></returns>
            public bool GetValueBoolean(int pid)
            {
                return bool.Parse(GetValueString(pid));
            }

            /// <summary>
            /// 获取策略的值
            /// </summary>
            /// <param name="pid">策略索引</param>
            /// <returns></returns>
            public decimal GetValueDecimal(int pid)
            {
                return decimal.Parse(GetValueString(pid));
            }

        }
        /// <summary>
        /// 利率
        /// </summary>
        private class Interest
        {
            const string SettingRootPath = "/Biz/LoanOptimize/Interest";

            /// <summary>
            /// 利率
            /// </summary>
            /// <param name="code">利率代码</param>
            public Interest(string code)
            {
                var item = ServiceDepository.SettingService.GetSettingItemByPath(BuildSettingPath(code));

                Code = item.Name;

            }

            /// <summary>
            /// 获取利率代码
            /// </summary>
            public string Code { get; private set; }

            private string BuildSettingPath(params string[] vps)
            {
                return string.Join("/", SettingRootPath, string.Join("/", vps));
            }

            /// <summary>
            /// 获取利率.
            /// </summary>
            /// <param name="index">利率索引.</param>
            /// <returns></returns>
            public decimal Get(int index)
            {
                return decimal.Parse(ServiceDepository.SettingService.GetSettingItemValueByPath(BuildSettingPath(Code, index.ToString())));
            }

        }

        /// <summary>
        /// 计算结果
        /// </summary>
        private class CReturn
        {
            /// <summary>
            /// 获取或设置贷款金额(万元).
            /// </summary>
            public decimal Amount { get; set; }            
            /// <summary>
            /// 获取或设置贷款利率(%).
            /// </summary>
            public decimal Interest { get; set; }
            /// <summary>
            /// 获取或设置贷款年限.
            /// </summary>
            public int Years { get; set; }
            /// <summary>
            /// 获取或设置(首)月还款额(元).
            /// </summary>
            public decimal Repay { get; set; }
        }

        /// <summary>
        /// 公积金贷款准入判断
        /// </summary>
        /// <param name="ips">外部输入信息</param>
        /// <param name="gBkp">公积金银行策略.</param>
        /// <param name="sBkp">商业银行策略.</param>
        /// <returns></returns>
        private bool P01(OInputs ips, BankPolicy gBkp, BankPolicy sBkp)
        {
            if (PXG(ips))
                return false;

            if (ips.UserBasic7)
                return false;

            //商业银行政策6
            if (!sBkp.GetValueBoolean(6))
                return false;

            //夫妻双方都未缴满6个月
            if (!ips.Gjj7 && !ips.Gjj8)
                return false;

            if (ips.UserBasic1 == Gender.Male)
            {
                //男
                if (ips.UserBasic6 <= gBkp.GetValueInt(10))
                    return true;
            }
            if (ips.UserBasic1 == Gender.Female)
            {
                //女
                if (ips.UserBasic6 <= gBkp.GetValueInt(11))
                    return true;
            }


            return false;
        }

        /// <summary>
        /// 商业贷款准入判断
        /// </summary>
        /// <param name="ips">外部输入信息</param>
        /// <param name="bkp">银行策略.</param>
        /// <returns></returns>
        private bool P02(OInputs ips, BankPolicy bkp)
        {

            if (PXG(ips))
                return false;

            //计算房龄
            var ha = DateTime.Now.Year - ips.UserBasic4;

            if (bkp.GetValueInt(12) - ha < 0)
                return false;

            if (ips.UserBasic1 == Gender.Male)
            {
                //男
                if (bkp.GetValueInt(10) - ips.UserBasic6 >= 0)
                    return true;
            }

            if (ips.UserBasic1 == Gender.Female)
            {
                //女
                if (bkp.GetValueInt(11) - ips.UserBasic6 >= 0)
                    return true;
            }

            return false;
        }


        /// <summary>
        /// 是否限购限贷
        /// </summary>
        /// <param name="ips"></param>
        /// <returns></returns>
        private bool PXG(OInputs ips)
        {
            if (ips.Xgxd4 == DomicileType.Local)
            {
                if (ips.Xgxd1 == MaritalStatus.Married)
                    return 2 - ips.Xgxd2 <= 0;
            }

            if (ips.Xgxd4 == DomicileType.NonLocal)
            {
                if (ips.Xgxd1 == MaritalStatus.Single)
                    return true;
            }

            return 1 - ips.Xgxd2 <= 0;
        }

        /// <summary>
        /// 公积金/商业贷款套数认定
        /// </summary>
        /// <param name="ips">外部输入信息</param>
        /// <param name="bkp">银行策略.</param>
        /// <returns></returns>
        private TaoShuRd P03P04(OInputs ips, BankPolicy bkp)
        {
            if (ips.Xgxd3 > 0)
                return TaoShuRd.Second;

            if (ips.Xgxd2 == 0)
                return TaoShuRd.First;

            //普通住宅
            if (ips.UserBasic5 == ResideType.Normal)
                return TaoShuRd.First;

            //非普认定
            if (bkp.GetValueBoolean(1))
                return TaoShuRd.First;

            return TaoShuRd.Second;
        }

        /// <summary>
        /// 公积金贷款年限计算
        /// </summary>
        /// <param name="ips">外部输入信息</param>
        /// <param name="bkp">银行策略.</param>
        /// <returns></returns>
        private int P05(OInputs ips, BankPolicy bkp)
        {
            List<int> list = new List<int>(3);
            //年限1
            list.Add(ips.UserBasic1 == Gender.Male ? bkp.GetValueInt(7) - ips.UserBasic6: bkp.GetValueInt(8) - ips.UserBasic6);

            //年限2
            //计算房龄
            var ha = DateTime.Now.Year - ips.UserBasic4;

            if (ha >= 1 && ha <= 5)
                list.Add(30);
            if (ha >= 6 && ha <= 19)
                list.Add(35 - ha);
            if (ha >= 20)
                list.Add(15);

            //年限3，公积金最大贷款30年
            list.Add(30);

            list.Sort();

            return list[0];
        }

        /// <summary>
        /// 计算基本公积金贷款金额1（万元）
        /// </summary>
        /// <param name="ips">The ips.</param>
        /// <param name="bkp">The BKP.</param>
        /// <param name="taoshu">The taoshu.</param>
        /// <param name="years">The years.</param>
        /// <returns></returns>
        private decimal P0601(OInputs ips, BankPolicy bkp, TaoShuRd taoshu, int years)
        {
            decimal amount = 0;

            if (taoshu == TaoShuRd.Second)
            {
                //二套
                if (ips.UserBasic5 == ResideType.Normal)
                {
                    //普通住房

                    //基本公积金贷款金额1
                    amount = ips.UserBasic3 * bkp.GetValueDecimal(4);
                }

                if (ips.UserBasic5 == ResideType.UnNormal)
                {
                    //非普通住房

                    //基本公积金贷款金额1
                    amount = ips.UserBasic3 * bkp.GetValueDecimal(14);
                }

            }
            else
            {
                //首套

                //基本公积金贷款金额1
                amount = ips.UserBasic3 * bkp.GetValueDecimal(2);
            }

            return amount;
        }

        /// <summary>
        /// 计算基本公积金贷款金额2（万元）
        /// </summary>
        /// <param name="ips">The ips.</param>
        /// <param name="years">The years.</param>
        /// <returns></returns>
        private decimal P0602(OInputs ips, int years)
        {
            if (ips.Xgxd1 == MaritalStatus.Married)
                return (ips.Gjj2 >= 780 ? 50 : (ips.Gjj2 / 10000) / (decimal)0.14 * (decimal)0.5 * 12 * years)
                    + (ips.Gjj5 >= 780 ? 50 : (ips.Gjj5 / 10000) / (decimal)0.14 * (decimal)0.5 * 12 * years);

            return ips.Gjj2 >= 780 ? 50 : (ips.Gjj2 / 10000) / (decimal)0.14 * (decimal)0.5 * 12 * years;
        }

        /// <summary>
        /// 计算基本公积金贷款金额3（万元）
        /// </summary>
        /// <param name="ips">The ips.</param>
        /// <returns></returns>
        private decimal P0603(OInputs ips)
        {
            if (ips.Xgxd1 == MaritalStatus.Married)
                return (ips.Gjj11 ? 50 : ips.Gjj1 / 10000 * 40) + (ips.Gjj12 ? 50 : ips.Gjj4 / 10000 * 40);

            return ips.Gjj11 ? 50 : ips.Gjj1 / 10000 * 40;
        }

        /// <summary>
        /// 计算基本公积金贷款金额4（万元）
        /// </summary>
        /// <param name="ips">The ips.</param>
        /// <param name="ts">The ts.</param>
        /// <returns></returns>
        private decimal P0604(OInputs ips, TaoShuRd ts)
        {
            var a = 0;
            if (ts == TaoShuRd.First)
                //基本公积金贷款金额4
                a = 50;
            else
                //基本公积金贷款金额4
                a = 40;

            if (ips.Xgxd1 == MaritalStatus.Married)
                return a * 2;

            return a;
        }

        /// <summary>
        /// 计算补充公积金贷款金额.（万元）
        /// </summary>
        /// <param name="ips">The ips.</param>
        /// <returns></returns>
        private decimal P06BC(OInputs ips)
        {
            decimal a = 0;
            if (ips.Gjj9)
                a = (ips.Gjj3 / 10000) >= 0.5M ? 10 : (ips.Gjj3 / 10000) * 20;

            if (ips.Xgxd1 == MaritalStatus.Married && ips.Gjj10)
                a += (ips.Gjj6 / 10000) >= 0.5M ? 10 : (ips.Gjj6 / 10000) * 20;

            return a;
        }

        /// <summary>
        /// 公积金贷款金额计算（万元）
        /// </summary>
        /// <param name="ips">外部输入信息</param>
        /// <param name="bkp">银行策略.</param>
        /// <param name="taoShu">套数认定.</param>
        /// <param name="years">贷款年限.</param>
        /// <returns></returns>
        private decimal P06(OInputs ips, BankPolicy bkp, TaoShuRd taoShu, int years)
        {
            decimal am = P0601(ips, bkp, taoShu, years);

            if (am > P0602(ips, years))
                am = P0602(ips, years);


            if (am > P0603(ips))
                am = P0603(ips);

            if (am > P0604(ips,taoShu))
                am = P0604(ips, taoShu);

            am += P06BC(ips);

            return am;
        }

        /// <summary>
        /// 公积金/商业贷款利率计算(%)
        /// </summary>
        /// <param name="ips">外部输入信息</param>
        /// <param name="bkp">银行策略.</param>
        /// <param name="rate">利率.</param>
        /// <param name="taoshu">套数认定.</param>
        /// <param name="years">贷款年限.</param>
        /// <returns></returns>
        private decimal P07P10(OInputs ips, BankPolicy bkp, Interest rate, TaoShuRd taoshu, int years)
        {
            if (taoshu == TaoShuRd.First)
            {
                //首套
                if (years >= 1 && years <= 3)
                    return bkp.GetValueDecimal(3) * rate.Get(1);
                if (years > 3 && years <= 5)
                    return bkp.GetValueDecimal(3) * rate.Get(2);
                if (years > 5)
                    return bkp.GetValueDecimal(3) * rate.Get(3);
            }
            else
            {
                if (ips.UserBasic5 == ResideType.Normal)
                {
                    //普通住房
                    if (years >= 1 && years <= 3)
                        return bkp.GetValueDecimal(5) * rate.Get(1);
                    if (years > 3 && years <= 5)
                        return bkp.GetValueDecimal(5) * rate.Get(2);
                    if (years > 5)
                        return bkp.GetValueDecimal(5) * rate.Get(3);
                }

                if (ips.UserBasic5 == ResideType.UnNormal)
                {
                    //非普通住房
                    if (years >= 1 && years <= 3)
                        return bkp.GetValueDecimal(15) * rate.Get(1);
                    if (years > 3 && years <= 5)
                        return bkp.GetValueDecimal(15) * rate.Get(2);
                    if (years > 5)
                        return bkp.GetValueDecimal(15) * rate.Get(3);
                }
            }

            return 0;
        }


        /// <summary>
        /// 商业贷款金额计算
        /// </summary>
        /// <param name="ips">外部输入信息</param>
        /// <param name="bkp">银行策略.</param>
        /// <param name="taoshu">套数认定.</param>
        /// <returns></returns>
        private decimal P08(OInputs ips, BankPolicy bkp, TaoShuRd taoshu)
        {

            if (taoshu == TaoShuRd.Second)
            {
                //二套
                if (ips.UserBasic5 == ResideType.Normal)
                    //普通住房
                    return ips.UserBasic3 * bkp.GetValueDecimal(4);

                if (ips.UserBasic5 == ResideType.UnNormal)
                    //非普通住房
                    return ips.UserBasic3 * bkp.GetValueDecimal(14);
            }
            else
                //首套
                return ips.UserBasic3 * bkp.GetValueDecimal(2);


            //其他返回
            return 0;
        }

        /// <summary>
        /// 商业贷款年限计算
        /// </summary>
        /// <param name="ips">外部输入信息</param>
        /// <param name="bkp">银行策略.</param>
        /// <returns></returns>
        private int P09(OInputs ips, BankPolicy bkp)
        {
            List<int> list = new List<int>(3);
            //年限1
            list.Add(ips.UserBasic1 == Gender.Male ? bkp.GetValueInt(7) - ips.UserBasic6 : bkp.GetValueInt(8) - ips.UserBasic6);

            //年限2
            //计算房龄
            var ha = DateTime.Now.Year - ips.UserBasic4;
            list.Add(bkp.GetValueInt(9) - ha);

            //年限3 固定值30
            list.Add(30);

            list.Sort();

            return list[0];
        }


        #endregion
    }
}
