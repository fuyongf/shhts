﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel.Activation;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;

namespace PinganHouse.SHHTS.ServiceImpl
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AttachmentService : IAttachmentService
    {
        public UploadAttachmentResult AddAttachment(AttachmentContent attachmentInfo)
        {
            if (attachmentInfo == null || string.IsNullOrWhiteSpace(attachmentInfo.AssociationNo))
                return new UploadAttachmentResult { ResultMessage = "attachment is invalid" };
            if (attachmentInfo.AttachmentContentData == null)
                return new UploadAttachmentResult { ResultMessage = "attachment's content is null" };

            UploadResultMessage result = null;
            try
            {
                result = ServiceDepository.FileTransferService.Upload(new FileUploadMessage { FileData = attachmentInfo.AttachmentContentData, FileName = attachmentInfo.AssociationNo });
                attachmentInfo.AttachmentContentData.Dispose();
                if (result == null || !result.Status)
                    return new UploadAttachmentResult { ResultMessage = "file save is fail" };
                attachmentInfo.FileId = result.FileId;
                ServiceDepository.AttachmentDataAccess.Insert(ObjectMapper.Copy<AttachmentInfo>(attachmentInfo));
                return new UploadAttachmentResult { Success = true, FileId = attachmentInfo.FileId };
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                try
                {
                    if (result != null && result.Status)
                        ServiceDepository.FileTransferService.DeleteById(result.FileId);
                }
                catch (Exception e)
                {
                    Log.Error(string.Format("delete fileId:{0} is fail, errorMsg:{1}", result.FileId, e.Message));
                }
                return new UploadAttachmentResult { ResultMessage = "系统错误" };
            }
        }

        public AttachmentDtos GetAttachments(AttachmentQuery query)
        {
            if (query == null || string.IsNullOrWhiteSpace(query.AssociationNo) || query.PageIndex <= 0 || query.PageCount <= 0)
                return null;
            try
            {
                var totalCount = 0;
                var result = ServiceDepository.AttachmentDataAccess.GetAttachments(query.AssociationNo, query.PageIndex, query.PageCount, ref totalCount, (AttachmentType)query.AttachmentType, matchingType: ConditionMatchingType.Equal);
                if (result == null || result.Count==0)
                    return new AttachmentDtos();
                return new AttachmentDtos { Attachments = ObjectMapper.Copy<AttachmentInfo, AttachmentDto>(result), TotalCount = totalCount };
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }

        public FileDownloadMessage DownloadAttachment(AttachmentQuery query)
        {
            try
            {
                if (query == null || string.IsNullOrWhiteSpace(query.FileId))
                    return null;
                return ServiceDepository.FileTransferService.Download(new FileQueryMessage { FileId = query.FileId, });
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }

        public IList<AttachmentDto> GetAttachments(string bizNo, IEnumerable<AttachmentType> types)
        {
            if (string.IsNullOrWhiteSpace(bizNo))
                return null;
            var entities = ServiceDepository.AttachmentDataAccess.GetAttachments(bizNo, types);
            if (entities == null)
                return null;
            return entities.Select(e => ObjectMapper.Copy<AttachmentDto>(e)).ToList();
        }

        public DeleteAttachmentResult DeleteAttachment(DeleteAttachmentElement element)
        {
            if (element == null || string.IsNullOrWhiteSpace(element.FileId))
                new DeleteAttachmentResult { Success = false };
            try
            {
                ServiceDepository.TransactionService.Begin();
                var update = ServiceDepository.AttachmentDataAccess.Delete(element.FileId, element.ModifyUserSysNo);
                if (update)
                    ServiceDepository.FileTransferService.DeleteById(element.FileId);
                ServiceDepository.TransactionService.Commit();
                return new DeleteAttachmentResult { Success = update };
            }
            catch (Exception ex)
            {
                ServiceDepository.TransactionService.RollBack();
                Log.Error(ex);
                return new DeleteAttachmentResult { Success = false };
            }
        }
    }
}
