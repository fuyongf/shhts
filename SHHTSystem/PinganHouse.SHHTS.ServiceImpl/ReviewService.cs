﻿using HTB.DevFx.Utils;
using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using System.Text;

namespace PinganHouse.SHHTS.ServiceImpl
{
    /// <summary>
    /// 行为复核接口
    /// 复核流程不可逆
    /// 复核规则即时，即每发生一次行为复核，则产生新复核规则。
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ReviewService : IReviewService
    {
        const string Path = "/Biz/ReviewRule";
        public OperationResult ReviewRule(ReviewCategory reviewCategory, string affiliationNo, long? createUserSysNo, out ReviewDto reviewDto)
        {
            reviewDto = null;
            try
            {         
                if (reviewCategory == ReviewCategory.UnKonw)
                    throw new ArgumentException("复核类型不正确");
                var settings = ServiceDepository.SettingService.GetSettingItemsByPath(string.Format("{0}/{1}", Path, (int)reviewCategory));
                if (settings == null || settings.Count() == 0)
                    return new OperationResult(1, "该类别不需要复核");
                var isEnabled = settings.FirstOrDefault(s => string.Compare("IsEnabled", s.Name, true) == 0);
                //未开启
                if (isEnabled == null || isEnabled.Value == "0")
                    return new OperationResult(2, "该类别规则未启用");
                var reviewMode = settings.FirstOrDefault(s => string.Compare("ReviewMode", s.Name, true) == 0);
                if (reviewMode == null || string.IsNullOrWhiteSpace(reviewMode.Value))
                    throw new ArgumentException("复核方式未知");
                var authorizer = settings.FirstOrDefault(s => string.Compare("Authorizer", s.Name, true) == 0);
                if (authorizer == null || string.IsNullOrWhiteSpace(authorizer.Value))
                    throw new ArgumentException("复核人未知");
                var reviewInfo = new ReviewInfo
                {
                    CreateUserSysNo = createUserSysNo,
                    CreateDate = DateTime.Now,
                    IsPass = false,
                    IsEnabled = true,
                    ReviewMode = reviewMode.Value,
                    ReviewCategory = reviewCategory,
                    Reviewer = authorizer.Value,
                    AffiliationNo = affiliationNo
                };
                if (!ServiceDepository.ReviewDataAccess.Insert(reviewInfo))
                    throw new ArgumentException("复核规则设置失败");
                reviewDto = ConvertToReviewDto(reviewInfo, reviewInfo.Reviewer);
                return new OperationResult(0);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-1, "系统内部错误");
            }

        }

        public OperationResult ValidateReviewRule(long sysno, long reviewSysNo, ReviewMode reviewMode, out ReviewDto reviewDto)
        {
            reviewDto=null;        
            try
            {
                var reviewInfo = ServiceDepository.ReviewDataAccess.Get(sysno);
                if (reviewInfo == null)
                    return new OperationResult(-1, "复核规则不存在");
                if (reviewInfo.IsPass)
                    return new OperationResult(0, "复核已通过");
                var newReviewDetail = new ReviewDetail
                {
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = reviewSysNo,
                    IsValid = false,
                    ReviewMode = reviewMode,
                    ReviewSysNo = sysno,
                    UserSysNo = reviewSysNo,
                    CurrentOrder = 0,
                };
                var result = ValidateReviewRule(reviewInfo, reviewSysNo, reviewMode, ref newReviewDetail);
                ServiceDepository.TransactionService.Begin();
                if (!ServiceDepository.ReviewDetailDataAccess.Insert(newReviewDetail))
                    throw new ArgumentException("复核记录保存失败");
                if (result.Success)
                {
                    if (reviewInfo.IsPass)
                    {
                        reviewInfo.ModifyDate = DateTime.Now;
                        reviewInfo.ModifyUserSysNo = reviewSysNo;
                        if (!ServiceDepository.ReviewDataAccess.Update(reviewInfo))
                            throw new ArgumentException("复核规则保存失败");
                    }
                    else
                    {
                        reviewDto = ConvertToReviewDto(reviewInfo, reviewSysNo.ToString(), newReviewDetail.CurrentOrder + 1);
                    }
                }
                ServiceDepository.TransactionService.Commit();
                return result;
            }
            catch(Exception ex)
            {
                ServiceDepository.TransactionService.RollBack();
                Log.Error(ex);
                return new OperationResult(-2, "系统内部错误");
            }
           
        }

        /// <summary>
        /// 验证规则
        /// </summary>
        /// <param name="reviewInfo"></param>
        /// <param name="reviewSysNo"></param>
        /// <param name="reviewMode"></param>
        /// <param name="reviewDetail"></param>
        /// <returns></returns>
        private OperationResult ValidateReviewRule(ReviewInfo reviewInfo, long reviewSysNo, ReviewMode reviewMode, ref ReviewDetail reviewDetail)
        {
            if (!reviewInfo.Reviewer.Contains(reviewSysNo.ToString()))
            {
                reviewDetail.Remark="复核人无此权限";
                return new OperationResult(1, "复核人无此权限");
            }
            //最新有效复核记录
            var lastReviewDetail = ServiceDepository.ReviewDetailDataAccess.GetLastValidReviewDetail(reviewInfo.SysNo);
            //成功复核次数
            var reviewCount =0;
            //是否存在有效复核记录
            if (lastReviewDetail == null)
            {
                if (reviewMode != reviewInfo.ReviewModes[0])
                {
                    reviewDetail.Remark = "复核方式未匹配";
                    return new OperationResult(2, "复核方式未匹配");
                }      
                reviewCount=1;
            }
            else
            {
                //这次复核人与最新有效复核人未匹配，则未通过
                //因复核不可逆， 主复核一经有确定复核记录，则确定复核人。
                if (lastReviewDetail.UserSysNo != reviewSysNo)
                {
                    reviewDetail.Remark = "该次复核复核人未匹配";
                    return new OperationResult(3, "该次复核复核人未匹配");
                }
                //下一复核方式顺序
                var nextOrder = reviewDetail.CurrentOrder+1;
                
                if (reviewInfo.ReviewModes.Count < nextOrder || reviewInfo.ReviewModes[nextOrder] != reviewMode)
                {
                    reviewDetail.Remark = "复核方式未匹配";
                    return new OperationResult(4, "复核方式未匹配");
                }
                reviewDetail.CurrentOrder = nextOrder;
                reviewCount = ++nextOrder;        
            }          
            if(reviewCount==reviewInfo.ReviewModes.Count)
                reviewInfo.IsPass=true;
            reviewDetail.IsValid=true;
            return new OperationResult(0);
        }

        private ReviewDto ConvertToReviewDto(ReviewInfo reviewInfo, string reviewer, int index = 0)
        {
            return new ReviewDto
            {
                SysNo = reviewInfo.SysNo,
                CreateDate = reviewInfo.CreateDate,
                CreateUserSysNo = reviewInfo.CreateUserSysNo,
                ReviewMode = reviewInfo.ReviewModes[index],
                ReviewName = ServiceDepository.UserDataAccess.GetUserName(reviewer),
            };
        }
    }
}
