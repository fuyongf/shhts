﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Activation;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.ServiceImpl
{
     [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ConfigService : IConfigService
    {
        public IDictionary GetBanks()
        {
            var paras = ServiceDepository.SettingService.GetSettingItemsByPath("/Biz/Static/Banks");
            if (paras == null)
                return null;
            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (var item in paras)
            {
                if (!result.ContainsKey(item.Name))
                    result.Add(item.Name, item.Description);
            }
            return result;
        }

        public IDictionary GetBankDic()
        {
            var paras = ServiceDepository.SettingService.GetSettingItemsByPath("/Biz/Static/BankConfig");
            if (paras == null)
                return null;
            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (var item in paras)
            {
                if (!result.ContainsKey(item.Name))
                    result.Add(item.Name, item.Description);
            }
            return result;
        }

        [Obsolete("use GetIdentifyBankCfgs instead")]
        public IDictionary GetIdentifyBanks()
        {
            var paras = ServiceDepository.SettingService.GetSettingItemsByPath("/Biz/Static/IdentifyBanks");
            if (paras == null)
                return null;
            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (var item in paras)
            {
                if ("0".Equals(item.Value))
                    continue;
                if (!result.ContainsKey(item.Name))
                    result.Add(item.Name, item.Description);
            }
            return result;
        }

        public IList<BankIdentifyCfg> GetIdentifyBankCfgs()
        {
            IList<BankIdentifyCfg> list = new List<BankIdentifyCfg>();

            var paras = ServiceDepository.SettingService.GetSettingItemsByPath("/Biz/Static/IdentifyBanks");
            if (paras != null)
            {
                foreach (var item in paras)
                {
                    BankIdentifyCfg bc = new BankIdentifyCfg();
                    bc.BankCode = item.Name;
                    bc.BankName = item.Description;
                    foreach (var tt in ServiceDepository.SettingService.GetSettingItems(item.Id))
                    {
                        if (tt.Name == "BankId")
                            bc.BankId = int.Parse(tt.Value);
                        if (tt.Name == "ImageUrl")
                            bc.ImageUrl = tt.Value;
                        if (tt.Name == "IsNeedWithhold")
                            bc.IsNeedWithhold =bool.Parse(tt.Value);
                        if (tt.Name == "Withhold")
                            bc.Withhold = decimal.Parse(tt.Value);
                        if (tt.Name == "IsNeedName")
                            bc.IsNeedName = bool.Parse(tt.Value);
                        if (tt.Name == "IsNeedBankAccount")
                            bc.IsNeedBankAccount = bool.Parse(tt.Value);
                        if (tt.Name == "IsNeedIdCard")
                            bc.IsNeedIdCard = bool.Parse(tt.Value);
                        if (tt.Name == "IsNeedMobile")
                            bc.IsNeedMobile = bool.Parse(tt.Value);
                    }

                    list.Add(bc);
                }
            }

            return list;
        }


        public int GetBankIdByCode(string bankCode)
        {
            if (string.IsNullOrWhiteSpace(bankCode))
                throw new ArgumentNullException("bankCode");
            var setting = ServiceDepository.SettingService.GetSettingItemByPath(string.Format("/Biz/Static/Banks/{0}", bankCode));
            if (setting == null)
                throw new Exception(string.Format("not found bank :{0}", bankCode));
            return int.Parse(setting.Value);
        }

        public IList<string> GetProvidentFundAddress()
        {
            var setting = ServiceDepository.SettingService.GetSettingItemByPath("/Biz/Static/ProvidentFundAddress");
            if (setting == null)
                throw new Exception("not found setting");
            if (string.IsNullOrWhiteSpace(setting.Value))
                return null;
            return setting.Value.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        public string GetMaterial(string materialPath)
        {
            if (string.IsNullOrWhiteSpace(materialPath))
                return string.Empty;
            return ServiceDepository.SettingService.GetSettingItemValueByPath(materialPath);
        }

        public string GetReceptionCenter(string receptionCenter)
        {
            if (string.IsNullOrWhiteSpace(receptionCenter))
                return string.Empty;
            return ServiceDepository.SettingService.GetSettingItemValueByPath(string.Format("/Biz/Static/TradingCenter/{0}", receptionCenter));
        }

        public string GetBankNameByCode(string bankCode)
        {
            if (string.IsNullOrWhiteSpace(bankCode))
                throw new ArgumentNullException("bankCode");
            var setting = ServiceDepository.SettingService.GetSettingItemByPath(string.Format("/Biz/Static/Banks/{0}", bankCode));
            if (setting == null)
                throw new Exception(string.Format("not found bank :{0}", bankCode));
            return setting.Description;
        }

        public IList<string> GetCommissioners()
        {
            var paras = ServiceDepository.SettingService.GetSettingItemsByPath("/Biz/Static/Commissioners");
            if (paras == null)
                return null;
            return paras.Select(p => p.Name).ToList();
        }

        public IList<string> GetRegionInfo(string path = "")
        {
            if (string.IsNullOrWhiteSpace(path))
                path = string.Empty;
            var paras = ServiceDepository.SettingService.GetSettingItemsByPath(
                string.Format("/Biz/Static/RegionConfig{0}", path));
            if (paras == null)
                return null;
            return paras.Select(p => p.Name).ToList();
        }

        #region 打印相关
        public IList<PrintTemplate> GetEFangqianPrintTemplates()
        {
            var temps = ServiceDepository.SettingService.GetSettingItemsByPath("/Biz/Print/Template");
            if (temps == null || temps.Count() == 0)
                return null;
            const string category = "EFQ";
            var result = new List<PrintTemplate>();
            try
            {
                foreach (var temp in temps)
                {
                    if (!category.Equals(temp.Value))
                        continue;
                    var pages = ServiceDepository.SettingService.GetSettingItemsByPath(string.Concat(temp.Path, "/Pages"));

                    result.Add(new PrintTemplate
                    {
                        TemplateId = temp.Name,
                        TemplateName = temp.Description,
                        Pages = pages == null ? null : pages.Select(p => p.Name).ToList(),
                        Category = temp.Value
                    });
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw new Exception("打印模版参数解析错误", e);
            }
            return result;
        }

        public Dictionary<string, PrintTemplateValue> GetPrintTemplate(string templateId, string pageNo)
        {
            if (string.IsNullOrWhiteSpace(templateId))
                return null;
            var page = ServiceDepository.SettingService.GetSettingItemByPath(
               string.Format("/Biz/Print/Template/{0}/Pages/{1}", templateId, pageNo));
            if (page == null)
                throw new Exception("打印参数未配置");

            var result = new Dictionary<string, PrintTemplateValue>();
            try
            {
                var fields = ServiceDepository.SettingService.GetSettingItems(page.Id);
                if (fields != null && fields.Count() > 0)
                {
                    foreach (var f in fields)
                    {
                        if (result.ContainsKey(f.Name))
                            continue;
                        var paras = ServiceDepository.SettingService.GetSettingItems(f.Id);
                        result.Add(f.Name, new PrintTemplateValue
                        {
                            TemplateId = templateId,
                            PageNo = pageNo,
                            FieldKey = f.Name,
                            FieldName = f.Description,
                            Font = paras.First(p => p.Name == "Font").Value,
                            FontSize = int.Parse(paras.First(p => p.Name == "FontSize").Value),
                            PosX = int.Parse(paras.First(p => p.Name == "X").Value),
                            PosY = int.Parse(paras.First(p => p.Name == "Y").Value)
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw new Exception("打印模版参数解析错误", e);
            }
            return result;
        }
        #endregion

        public string GetAutoUpdateUrl()
        {
            return ServiceDepository.SettingService.GetSettingItemValueByPath("/System/Publish/AutoUpdateUrl");
        }

        public string GetNewAutoUpdateUrl()
        {
            return ServiceDepository.SettingService.GetSettingItemValueByPath("/System/Publish/NewAutoUpdateUrl");
        }

        public Tuple<string, string> GetWXKeys(string appNo)
        {
            string path = "/System/Parter/WeiXin/Keys";
            if (!string.IsNullOrWhiteSpace(appNo))
                path = string.Format("{0}/{1}", path, appNo);
            var settings = ServiceDepository.SettingDataAccess.GetItemsByPath(path);
            if (settings == null)
                throw new ArgumentException("未配置/System/Parter/WeiXin/Keys节点");
            var token = settings.FirstOrDefault(s => string.Compare(s.Name, "Token", true) == 0).Value;
            var aesKey = settings.FirstOrDefault(s => string.Compare(s.Name, "EncodingAESKey", true) == 0).Value;
            return Tuple.Create<string, string>(token, aesKey);
        }
        public IDictionary<string, IList<string>> GetChangeFundCondition(ChangeFundType type)
        {
            string path = string.Format("/Biz/Case/ChangeFundCondition/{0}", type.ToString());
            var paras = ServiceDepository.SettingService.GetSettingItemsByPath(path);
            if (paras == null)
                return null;
            IDictionary<string, IList<string>> result = new Dictionary<string, IList<string>>();
            foreach (var item in paras)
            {
                var children = ServiceDepository.SettingDataAccess.GetItems(item.Id);
                if (children == null)
                    result.Add(item.Name, null);
                else
                    result.Add(item.Name, children.Select(c => c.Name).ToList());
            }
            return result;
        }

        //public FundDirection GetFundDirection(OrderBizType tradeType)
        //{
        //    var setting = ServiceDepository.SettingService.GetSettingItemByPath(string.Format("/Biz/Trade/FundDirection/{0}", tradeType.ToString()));
        //    if (setting == null)
        //        return FundDirection.Unknow;
        //    return (FundDirection)int.Parse(setting.Value);
        //}

        public AuditStatus GetRefundSubmitCondition()
        {
            var setting = ServiceDepository.SettingService.GetSettingItemByPath("/Biz/Trade/SubmitStatus/Refund");
            if (setting == null)
                throw new Exception("参数未配置");
            return (AuditStatus)int.Parse(setting.Value);
        }


        public AuditStatus GetPaymentSubmitCondition()
        {
            var setting = ServiceDepository.SettingService.GetSettingItemByPath("/Biz/Trade/SubmitStatus/Payment");
            if (setting == null)
                throw new Exception("参数未配置");
            return (AuditStatus)int.Parse(setting.Value);
        }

        public AuditStatus GetWithdrawSubmitCondition()
        {
            var setting = ServiceDepository.SettingService.GetSettingItemByPath("/Biz/Trade/SubmitStatus/Withdraw");
            if (setting == null)
                throw new Exception("参数未配置");
            return (AuditStatus)int.Parse(setting.Value);
        }

        #region 审核相关配置
        public IList<AuditStatus> GetPaymentOrderAuditPreStatus(ReceptionCenter center, AuditStatus status)
        {
            var setting = ServiceDepository.SettingService.GetSettingItemByPath(string.Format("/Biz/Trade/Audit/{0}/Payment/{1}/PreStatus", (int)center, (int)status));
            if (setting == null || string.IsNullOrWhiteSpace(setting.Value))
                throw new Exception("参数未配置");
            string[] results = setting.Value.Split('|');
            var result = new List<AuditStatus>();
            foreach (string item in results)
            {
                result.Add((AuditStatus)int.Parse(item));
            }
            return result;
        }

        public IList<AuditStatus> GetWithdrawOrderAuditPreStatus(ReceptionCenter center, AuditStatus status)
        {
            var setting = ServiceDepository.SettingService.GetSettingItemByPath(string.Format("/Biz/Trade/Audit/{0}/Withdraw/{1}/PreStatus", (int)center, (int)status));
            if (setting == null || string.IsNullOrWhiteSpace(setting.Value))
                throw new Exception("参数未配置");
            string[] results = setting.Value.Split('|');
            var result = new List<AuditStatus>();
            foreach (string item in results)
            {
                result.Add((AuditStatus)int.Parse(item));
            }
            return result;
        }

        public IList<AuditStatus> GetRefundOrderAuditPreStatus(ReceptionCenter center, AuditStatus status)
        {
            var setting = ServiceDepository.SettingService.GetSettingItemByPath(string.Format("/Biz/Trade/Audit/{0}/Refund/{1}/PreStatus", (int)center, (int)status));
            if (setting == null || string.IsNullOrWhiteSpace(setting.Value))
                throw new Exception("参数未配置");
            string[] results = setting.Value.Split('|');
            var result = new List<AuditStatus>();
            foreach (string item in results)
            {
                result.Add((AuditStatus)int.Parse(item));
            }
            return result;
        }

        public IList<AuditStatus> GetCollectionOrderAuditPreStatus(ReceptionCenter center, AuditStatus status)
        {
            var setting = ServiceDepository.SettingService.GetSettingItemByPath(string.Format("/Biz/Trade/Audit/{0}/Collection/{1}/PreStatus", (int)center, (int)status));
            if (setting == null || string.IsNullOrWhiteSpace(setting.Value))
                throw new Exception("参数未配置");
            string[] results = setting.Value.Split('|');
            var result = new List<AuditStatus>();
            foreach (string item in results)
            {
                result.Add((AuditStatus)int.Parse(item));
            }
            return result;
        }

        public IList<long> GetPaymentOrderAuditors(ReceptionCenter center, AuditStatus status)
        {
            var setting = ServiceDepository.SettingService.GetSettingItemByPath(string.Format("/Biz/Trade/Audit/{0}/Payment/{1}/Auditor", (int)center, (int)status));
            if (setting == null || string.IsNullOrWhiteSpace(setting.Value))
                throw new Exception("参数未配置");
            string[] results = setting.Value.Split('|');
            var result = new List<long>();
            foreach (string item in results)
            {
                result.Add(long.Parse(item));
            }
            return result;
        }

        public IList<long> GetWithdrawOrderAuditors(ReceptionCenter center, AuditStatus status)
        {
            var setting = ServiceDepository.SettingService.GetSettingItemByPath(string.Format("/Biz/Trade/Audit/{0}/Withdraw/{1}/Auditor", (int)center, (int)status));
            if (setting == null || string.IsNullOrWhiteSpace(setting.Value))
                throw new Exception("参数未配置");
            string[] results = setting.Value.Split('|');
            var result = new List<long>();
            foreach (string item in results)
            {
                result.Add(long.Parse(item));
            }
            return result;
        }

        public IList<long> GetRefundOrderAuditors(ReceptionCenter center, AuditStatus status)
        {
            var setting = ServiceDepository.SettingService.GetSettingItemByPath(string.Format("/Biz/Trade/Audit/{0}/Refund/{1}/Auditor", (int)center, (int)status));
            if (setting == null || string.IsNullOrWhiteSpace(setting.Value))
                throw new Exception("参数未配置");
            string[] results = setting.Value.Split('|');
            var result = new List<long>();
            foreach (string item in results)
            {
                result.Add(long.Parse(item));
            }
            return result;
        }

        public IList<long> GetCollectionOrderAuditors(ReceptionCenter center, AuditStatus status)
        {
            var setting = ServiceDepository.SettingService.GetSettingItemByPath(string.Format("/Biz/Trade/Audit/{0}/Collection/{1}/Auditor", (int)center, (int)status));
            if (setting == null || string.IsNullOrWhiteSpace(setting.Value))
                throw new Exception("参数未配置");
            string[] results = setting.Value.Split('|');
            var result = new List<long>();
            foreach (string item in results)
            {
                result.Add(long.Parse(item));
            }
            return result;
        }
        #endregion

        public string GetSHHTMSAbooutInfo()
        {
            var para = ServiceDepository.SettingService.GetSettingItemByPath("/Biz/Static/SHHTMS/About");
            if (para == null)
                return null;
            return para.Value;
        }
    }
}
