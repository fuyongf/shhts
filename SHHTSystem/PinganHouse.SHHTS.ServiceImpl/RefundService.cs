﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.ServiceImpl
{
    public partial class TradeService : ITradeService
    {
        public OperationResult SubmitRefundOrder(AccountType accountType, string subjectId, decimal amount, string bankAccountNo, string bankCode, string accountName, string bankName, string remark, long operatorSysNo)
        {
            throw new NotImplementedException();
            //#region ###################参数验证###################
            //if (string.IsNullOrWhiteSpace(subjectId) || amount <= 0)
            //    return new OperationResult(-1, "非法参数信息");
            //if (string.IsNullOrWhiteSpace(bankCode) || string.IsNullOrWhiteSpace(accountName) || string.IsNullOrWhiteSpace(bankAccountNo))
            //    return new OperationResult(-2, "银行帐户信息错误");
            //var accountInfo = ServiceDepository.AccountDataAccess.Get(subjectId, accountType, OrderBizType.Refund);
            //if (accountInfo == null)
            //    return new OperationResult(-3, "账户不存在");
            //if (accountInfo.IsLocked)
            //    return new OperationResult(-3, "账户已锁定");
            //if (accountInfo.Balance < amount)
            //    return new OperationResult(-3, "账户余额不足");
            //#endregion

            //try
            //{
            //    ServiceDepository.TransactionService.Begin();
            //    #region ###################创建订单###################
            //    OrderInfo order = new OrderInfo
            //    {
            //        SysNo = Sequence.Get(),
            //        AccountSysNo = accountInfo.SysNo,
            //        OrderAmount = amount,
            //        OrderType = OrderType.Payment,
            //        OrderBizType = OrderBizType.Refund,
            //        PaymentChannel = PaymentChannel.Transfer,
            //        OrderStatus = OrderStatus.Initial,
            //        Remark = remark,
            //        CreateUserSysNo = operatorSysNo,
            //        CreateDate = DateTime.Now
            //    };
            //    if (!ServiceDepository.OrderDataAccess.Insert(order))
            //    {
            //        ServiceDepository.TransactionService.RollBack();
            //        return new OperationResult(-4, "订单创建失败");
            //    }
            //    #endregion

            //    #region ###################创建交易明细###################
            //    var detail = new TradeInfo
            //    {
            //        SysNo = Sequence.Get(),
            //        OrderNo = order.OrderNo,
            //        Amount = -amount,
            //        Status = TradeStatus.Initial,
            //        AccountName = accountName,
            //        BankAccountNo = bankAccountNo,
            //        BankCode = bankCode,
            //        SubBankName = bankName,
            //        Remark = remark,
            //        CreateUserSysNo = operatorSysNo,
            //        CreateDate = DateTime.Now
            //    };
            //    if (!ServiceDepository.TradeDataAccess.Insert(detail))
            //    {
            //        ServiceDepository.TransactionService.RollBack();
            //        return new OperationResult(-6, "交易明细创建失败");
            //    }
            //    #endregion

            //    #region ###################修改账户###################
            //    accountInfo.Balance -= order.OrderAmount;
            //    accountInfo.LockedMoney += order.OrderAmount;
            //    accountInfo.ModifyUserSysNo = operatorSysNo;
            //    if (!ServiceDepository.AccountDataAccess.Update(accountInfo))
            //    {
            //        ServiceDepository.TransactionService.RollBack();
            //        return new OperationResult(-7, "账户操作失败");
            //    }
            //    //资金日志
            //    var log = new AccountOperationLog
            //    {
            //        AccountSysNo = accountInfo.SysNo,
            //        Amount = order.OrderAmount,
            //        OperationType = AccountOperationType.Lock,
            //        RefBizNo = detail.TradeNo,
            //        Remark = remark,
            //        CreateUserSysNo = operatorSysNo,
            //        CreateDate = DateTime.Now
            //    };
            //    if (!ServiceDepository.AccountDataAccess.AddOperatorLog(log))
            //    {
            //        ServiceDepository.TransactionService.RollBack();
            //        return new OperationResult(-8, "操作日志创建失败");
            //    }
            //    #endregion
            //    var result = new OperationResult(0);
            //    result.OtherData.Add("OrderNo", order.OrderNo);
            //    result.OtherData.Add("TradeNo", detail.TradeNo);
            //    //var submit = SubmitPayRequest(accountInfo.SubjectId, order.OrderBizType, detail);
            //    //if (!submit.Success)
            //    //{
            //    //    ServiceDepository.TransactionService.RollBack();
            //    //    return submit;
            //    //}
            //    ServiceDepository.TransactionService.Commit();
            //    return result;
            //}
            //catch (Exception e)
            //{
            //    ServiceDepository.TransactionService.RollBack();
            //    Log.Error(e);
            //    return new OperationResult(-5, "系统错误");
            //}
        }

        public OperationResult SubmitRefund(string orderNo, string remark, long operatorSysNo)
        {
            #region ###################参数校验###################
            if (string.IsNullOrWhiteSpace(orderNo))
                return new OperationResult(-1, "非法参数信息");
            var order = ServiceDepository.OrderDataAccess.Get(orderNo);
            if (order == null)
                return new OperationResult(-2, "订单不存在");
            if (order.OrderType != OrderType.Collection)
                return new OperationResult(-3, "订单类型错误");
            if (order.OrderStatus != OrderStatus.Paid)
                return new OperationResult(-3, "订单状态错误");
            var trades = ServiceDepository.TradeDataAccess.GetByOrder(orderNo);
            if (trades != null && trades.Count > 0)
                trades = trades.Where(t => (t.Status == TradeStatus.Completed)).ToList();
            if (trades.Count == 0 || order.OrderAmount != trades.Sum(t => t.Amount))
                return new OperationResult(-4, "订单金额错误");
            var accountInfo = ServiceDepository.AccountDataAccess.Get(order.AccountSysNo, order.OrderBizType);
            if (accountInfo == null)
                return new OperationResult(-6, "账户不存在");
            if (accountInfo.IsLocked)
                return new OperationResult(-6, "账户已锁定");
            if (accountInfo.Balance < order.OrderAmount)
                return new OperationResult(-6, "账户余额不足");
            #endregion

            try
            {
                ServiceDepository.TransactionService.Begin();

                #region ###################修改订单###################
                order.OrderStatus = OrderStatus.Refunding;
                order.ModifyUserSysNo = operatorSysNo;
                if (!ServiceDepository.OrderDataAccess.Update(order))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-7, "订单创建失败");
                }
                #endregion

                #region ###################创建退款交易###################
                foreach (var trade in trades)
                {
                    var detail = new TradeInfo
                    {
                        SysNo = Sequence.Get(),
                        OrderNo = order.OrderNo,
                        Amount = -trade.Amount,
                        Status = TradeStatus.Initial,
                        AccountName = trade.AccountName,
                        BankAccountNo = trade.BankAccountNo,
                        BankCode = trade.BankCode,
                        SubBankName = trade.SubBankName,
                        Remark = trade.TradeNo,
                        CreateUserSysNo = operatorSysNo,
                        CreateDate = DateTime.Now
                    };
                    if (!ServiceDepository.TradeDataAccess.Insert(detail))
                    {
                        ServiceDepository.TransactionService.RollBack();
                        return new OperationResult(-8, "交易明细创建失败");
                    }

                    var log = new AccountOperationLog
                    {
                        AccountSysNo = accountInfo.SysNo,
                        Amount = trade.Amount,
                        OperationType = AccountOperationType.Lock,
                        RefBizNo = detail.TradeNo,
                        Remark = remark,
                        CreateUserSysNo = operatorSysNo,
                        CreateDate = DateTime.Now
                    };
                    if (!ServiceDepository.AccountDataAccess.AddOperatorLog(log))
                    {
                        ServiceDepository.TransactionService.RollBack();
                        return new OperationResult(-9, "操作日志创建失败");
                    }
                }
                #endregion

                #region ###################修改账户###################
                accountInfo.Balance -= order.OrderAmount;
                accountInfo.LockedMoney += order.OrderAmount;
                accountInfo.ModifyUserSysNo = operatorSysNo;
                if (!ServiceDepository.AccountDataAccess.Update(accountInfo))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-10, "账户操作失败");
                }
                #endregion

                ServiceDepository.TransactionService.Commit();
                return new OperationResult(0);
            }
            catch (Exception e)
            {
                ServiceDepository.TransactionService.RollBack();
                Log.Error(e);
                return new OperationResult(-5, "系统错误");
            }

        }

        public OperationResult RefundOrderAudit(ReceptionCenter center, string orderNo, AuditStatus status, string remark, long operatorSysNo, string operatorName)
        {
            if (string.IsNullOrWhiteSpace(orderNo))
                return new OperationResult(-1, "非法参数");
            var order = ServiceDepository.OrderDataAccess.Get(orderNo);
            if (order == null)
                return new OperationResult(-2, "订单不存在");
            var trades = ServiceDepository.TradeDataAccess.GetByOrder(order.OrderNo);
            if (trades == null || trades.Count == 0)
                return new OperationResult(-3, "交易不存在");
            trades = trades.Where(t => t.Status != TradeStatus.Completed && t.Amount < 0).ToList();
            if (trades == null || trades.Count == 0)
                return new OperationResult(-3, "交易不存在");

            var preStatus = ServiceDepository.ConfigService.GetRefundOrderAuditPreStatus(center, status);
            if (!preStatus.Contains(order.AuditStatus))
                return new OperationResult(-4, "交易状态错误");
            var auditors = ServiceDepository.ConfigService.GetRefundOrderAuditors(center, status);
            if (auditors == null || auditors.Count == 0 || !auditors.Contains(operatorSysNo))
                return new OperationResult(-6, "无此操作权限");
            try
            {
                var log = new AuditLogInfo
                {
                    AuditObjectType = AuditObjectType.Order,
                    ChangeValue = (int)status,
                    SubjectId = order.OrderNo,
                    AuditSysNo = operatorSysNo,
                    AuditUserName = operatorName,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = operatorSysNo,
                    Remark = remark
                };

                ServiceDepository.TransactionService.Begin();
                #region #################审核日志#################
                if (!ServiceDepository.AuditLogDataAccess.Insert(log))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-6, "审核日志创建失败");
                }
                #endregion

                #region #################交易状态#################
                //foreach (var trade in trades)
                //{
                //    trade.Status = status;
                //    trade.ModifyUserSysNo = operatorSysNo;
                //    if (!ServiceDepository.TradeDataAccess.Update(trade))
                //    {
                //        ServiceDepository.TransactionService.RollBack();
                //        return new OperationResult(-7, string.Format("交易流水:{0},状态修改失败", trade.TradeNo));
                //    }
                //}
                #endregion

                #region #################订单状态#################
                order.AuditStatus = status;
                order.ModifyUserSysNo = operatorSysNo;
                if (!ServiceDepository.OrderDataAccess.Update(order))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-8, string.Format("订单:{0},状态修改失败", order.OrderNo));
                }
                #endregion
                ServiceDepository.TransactionService.Commit();
                return new OperationResult(0);
            }
            catch (Exception e)
            {
                ServiceDepository.TransactionService.RollBack();
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }
    }
}
