﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.ServiceImpl
{
    public partial class TradeService : ITradeService
    {
        public Order GetOrderInfo(string orderNo)
        {
            if (string.IsNullOrWhiteSpace(orderNo))
                return null;
            var entity = ServiceDepository.OrderDataAccess.Get(orderNo);
            if (entity == null)
                return null;
            return ObjectMapper.Copy<Order>(entity);
        }

        public OrderDetail GetOrderDetail(string orderNo)
        {
            if (string.IsNullOrWhiteSpace(orderNo))
                return null;
            var entity = ServiceDepository.OrderDataAccess.Get(orderNo);
            if (entity == null)
                return null;
            var order = ObjectMapper.Copy<OrderDetail>(entity);

            if (order.OrderType == OrderType.Payment)
            {
                var extends = ServiceDepository.OrderDataAccess.GetPaymentExtend(order.OrderNo);
                if (extends != null)
                {
                    order.AccountName = extends.AccountName;
                    order.BankAccountNo = extends.BankAccountNo;
                    order.BankCode = extends.BankCode;
                    order.SubBankName = extends.SubBankName;
                }
                else // 兼容老数据查询
                {
                    var trade = ServiceDepository.TradeDataAccess.GetByOrder(order.OrderNo);
                    if (trade != null && trade.Count > 0)
                    {
                        order.AccountName = trade[0].AccountName;
                        order.BankAccountNo = trade[0].BankAccountNo;
                        order.BankCode = trade[0].BankCode;
                        order.SubBankName = trade[0].SubBankName;
                    }
                }
            }
            else if (order.OrderType == OrderType.PaidOff)
            {
                var extends = ServiceDepository.OrderDataAccess.GetPaidOffExtend(order.OrderNo);
                if (extends != null)
                {
                    order.AccountName = extends.AccountName;
                    order.BankAccountNo = extends.BankAccountNo;
                    order.BankCode = extends.BankCode;
                    order.SubBankName = extends.SubBankName;
                }
                else // 兼容老数据查询
                {
                    var trade = ServiceDepository.TradeDataAccess.GetByOrder(order.OrderNo);
                    if (trade != null && trade.Count > 0)
                    {
                        order.AccountName = trade[0].AccountName;
                        order.BankAccountNo = trade[0].BankAccountNo;
                        order.BankCode = trade[0].BankCode;
                        order.SubBankName = trade[0].SubBankName;
                    }
                }
            }
            return order;
        }

        public TradeDetail GetTradeInfo(string tradeNo)
        {
            if (string.IsNullOrWhiteSpace(tradeNo))
                return null;
            var entity = ServiceDepository.TradeDataAccess.Get(tradeNo);
            if (entity == null)
                return null;
            return ObjectMapper.Copy<TradeDetail>(entity);
        }

        public IList<Order> GetOrders(AccountType accountType, string accountSubjectId, OrderType? orderType, OrderBizType? tradeType, DateTime? startDate, DateTime? endDate)
        {
            if (string.IsNullOrWhiteSpace(accountSubjectId))
                return null;
            var entities = ServiceDepository.OrderDataAccess.GetOrders(accountType, accountSubjectId, orderType, tradeType, startDate, endDate);
            if (entities == null)
                return null;
            return entities.Select(e => ObjectMapper.Copy<Order>(e)).ToList();
        }

        public IList<OrderPaginatedInfo> GetOrders(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, string caseId, AccountType? accountType, OrderType? orderType, PaymentChannel? channel, string orderNo, string createUserName, DateTime? startDate, DateTime? endDate)
        {
            var results = ServiceDepository.OrderDataAccess.GetPaginatedList(pageIndex, pageSize, out totalCount, center, caseId, accountType, orderType, channel, orderNo, createUserName, startDate, endDate);
            if (results == null)
                return null;
            return results.Select(s => ObjectMapper.Copy<OrderPaginatedInfo>(s)).ToList();
        }

        public IList<TradePaginatedInfo> GetOrders(int pageIndex, int pageSize, out int totalCount, bool isSelf, IEnumerable<long> customerSysNos, string caseId)
        {
            totalCount = 0;
            if (customerSysNos == null || customerSysNos.Count() == 0)
                return null;
            try
            {
                var caseRels = ServiceDepository.CaseDataAccess.GetRelationByCustomerSysNos(customerSysNos.ToArray());
                if (caseRels == null || caseRels.Count == 0)
                    return null;
                var accounts = new List<long>();
                foreach (var rel in caseRels)
                {
                    if (!string.IsNullOrEmpty(caseId) && !caseId.Equals(rel.CaseId))
                        continue;
                    var account = ServiceDepository.AccountDataAccess.Get(rel.CaseId, (AccountType)(int)rel.CaseUserType, null);
                    if (account == null)
                        continue;
                    accounts.Add(account.SysNo);
                }
                if (accounts == null)
                    return null;
                var trades = ServiceDepository.TradeDataAccess.GetPaginatedListByAccount(pageIndex, pageSize, out totalCount, isSelf, accounts.ToArray());
                if (trades == null)
                    return null;
                var result = trades.Select(o => ObjectMapper.Copy<TradePaginatedInfo>(o)).ToList();
                //foreach (var item in result)
                //{
                //    var trades = ServiceDepository.TradeDataAccess.GetByOrder(item.OrderNo);
                //    if (trades == null)
                //        continue;
                //    item.TradeDetails = trades.Select(t => ObjectMapper.Copy<TradeDetail>(t)).ToList();
                //}
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }

        public IList<RefundOrderPaginatedInfo> GetRefundOrders(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, string orderNo, string createUserName, DateTime? startDate, DateTime? endDate)
        {
            var results = ServiceDepository.OrderDataAccess.GetRefundPaginatedList(pageIndex, pageSize, out totalCount, center, orderNo, createUserName, startDate, endDate);
            if (results == null)
                return null;
            return results.Select(s => ObjectMapper.Copy<RefundOrderPaginatedInfo>(s)).ToList();
        }

        public IList<TradeDetail> GetTradeByOrder(string orderNo)
        {
            if (string.IsNullOrWhiteSpace(orderNo))
                return null;
            var entities = ServiceDepository.TradeDataAccess.GetByOrder(orderNo);
            if (entities == null)
                return null;
            return entities.Select(e => ObjectMapper.Copy<TradeDetail>(e)).ToList();
        }

        public IList<TradePaginatedInfo> GetTrades(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, PaymentChannel? channel, string orderNo, string tradeNo, string caseId, string bankAccountNo, DateTime? startDate, DateTime? endDate)
        {
            var results = ServiceDepository.TradeDataAccess.GetPaginatedList(pageIndex, pageSize, out totalCount, center, channel, orderNo, tradeNo, caseId, bankAccountNo, startDate, endDate);
            if (results == null)
                return null;
            return results.Select(s => ObjectMapper.Copy<TradePaginatedInfo>(s)).ToList();
        }

        public IList<BillDetail> GetBillByOrder(string orderNo, BillCategory? category, BillType? type, bool? isBilled = null)
        {
            if (string.IsNullOrWhiteSpace(orderNo))
                return null;
            var entities = ServiceDepository.BillDataAccess.GetBillByOrder(orderNo, category, type, isBilled);
            if (entities == null)
                return null;
            return entities.Select(e => ObjectMapper.Copy<BillDetail>(e)).ToList();
        }

        public IList<MissCollectionTrade> GetMissCollectionTrade(bool? confirmed, DateTime? startDate, DateTime? endDate)
        {
            var entities = ServiceDepository.MissCollectionTradeDataAccess.Get(null, null, confirmed, startDate, endDate);
            if (entities == null)
                return null;
            return entities.Select(e => ObjectMapper.Copy<MissCollectionTrade>(e)).ToList();
        }

        public IList<MissCollectionTrade> GetMissTransferTradeByAccountNo(string accountNo)
        {
            if (string.IsNullOrWhiteSpace(accountNo))
                return null;
            var entities = ServiceDepository.MissCollectionTradeDataAccess.Get(accountNo, PaymentChannel.Transfer, false, null, null);
            if (entities == null)
                return null;
            return entities.Select(e => ObjectMapper.Copy<MissCollectionTrade>(e)).ToList();
        }

        public IList<AuditLog> GetOrderAuditLog(string orderNo)
        {
            if (string.IsNullOrWhiteSpace(orderNo))
                return null;
            var entities = ServiceDepository.AuditLogDataAccess.Get(AuditObjectType.Order, orderNo);
            if (entities == null)
                return null;
            return entities.Select(e => ObjectMapper.Copy<AuditLog>(e)).ToList();
        }

        public IList<TradePaginatedInfo> GetTradeByCase(int pageIndex, int pageSize, out int totalCount, string caseId, AccountType? accountType, OrderType? orderType)
        {
            totalCount = 0;
            if (string.IsNullOrWhiteSpace(caseId))
                return null;
            var results = ServiceDepository.TradeDataAccess.GetByCase(pageIndex, pageSize, out totalCount, caseId, accountType, orderType);
            if (results == null)
                return null;
            return results.Select(s => ObjectMapper.Copy<TradePaginatedInfo>(s)).ToList();
        }

        public IList<DeductSchedule> GetDeductSchedules(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, string caseId, bool? finished, DateTime? startDate, DateTime? endDate)
        {
            var entities = ServiceDepository.DeductScheduleDataAccess.GetPaginatedList(pageIndex, pageSize, out totalCount, center, caseId, finished, startDate, endDate);
            if (entities == null)
                return null;
            return entities.Select(e => ObjectMapper.Copy<DeductSchedule>(e)).ToList();
        }
    }
}
