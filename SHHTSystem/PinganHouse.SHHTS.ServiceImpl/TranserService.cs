﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.ServiceImpl
{
    public partial class TradeService : ITradeService
    {
        public OperationResult ChangeBalanceType(AccountType accountType, string subjectId, OrderBizType formType, OrderBizType toType, decimal changeAmount, string remark, long operatorSysNo)
        {
            #region ###################参数验证###################
            if (string.IsNullOrWhiteSpace(subjectId) || string.IsNullOrWhiteSpace(subjectId) || changeAmount <= 0)
                return new OperationResult(-1, "非法参数信息");
            var payerAccount = ServiceDepository.AccountDataAccess.Get(subjectId, accountType, formType);
            if (payerAccount == null)
                return new OperationResult(-2, "付款账户不存在");
            if (payerAccount.IsLocked)
                return new OperationResult(-2, "付款账户已锁定");
            if (payerAccount.Balance < changeAmount)
                return new OperationResult(-2, "付款账户余额不足");
            if (payerAccount.ReceiptBalance < changeAmount)
                return new OperationResult(-2, "付款账户可开收据金额不足");
            var payeeAccount = ServiceDepository.AccountDataAccess.Get(subjectId, accountType, toType);
            if (payeeAccount == null)
                return new OperationResult(-3, "收款账户不存在");
            #endregion

            try
            {
                ServiceDepository.TransactionService.Begin();
                #region ###################创建订单###################
                OrderInfo order = new OrderInfo
                {
                    SysNo = Sequence.Get(),
                    AccountSysNo = payerAccount.SysNo,
                    OppositeAccount = payeeAccount.SysNo,
                    OrderAmount = changeAmount,
                    OrderType = OrderType.Transfer,
                    OrderBizType = formType,
                    PaymentChannel = PaymentChannel.Transfer,
                    OrderStatus = OrderStatus.Initial,
                    Remark = remark,
                    CreateUserSysNo = operatorSysNo,
                    CreateDate = DateTime.Now
                };
                if (!ServiceDepository.OrderDataAccess.Insert(order))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-4, "订单创建失败");
                }

                var extend = new TransferOrderExtend
                {
                    OrderNo = order.OrderNo,
                    ReceiveAccountType = toType,
                    CreateUserSysNo = operatorSysNo
                };
                if (!ServiceDepository.OrderDataAccess.InsertTransferExtend(extend))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-6, "订单创建失败");
                }
                #endregion

                #region ###################锁定出账金额###################
                payerAccount.Balance -= order.OrderAmount;
                payerAccount.LockedMoney += order.OrderAmount;
                payerAccount.ModifyUserSysNo = operatorSysNo;
                if (!ServiceDepository.AccountDataAccess.Update(payerAccount))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-7, "资金锁定操作失败");
                }
                //资金日志
                var log = new AccountOperationLog
                {
                    AccountSysNo = payerAccount.SysNo,
                    Amount = order.OrderAmount,
                    OperationType = AccountOperationType.Lock,
                    RefBizNo = order.OrderNo,
                    Remark = string.Format("出账锁定：{0}", order.OrderNo),
                    CreateUserSysNo = operatorSysNo,
                    CreateDate = DateTime.Now
                };
                if (!ServiceDepository.AccountDataAccess.AddOperatorLog(log))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-8, "操作日志创建失败");
                }
                #endregion

                #region ###################开始转账###################
                payerAccount.Verson++;
                var transferRet = SubmitTransfer(order, payerAccount, payeeAccount, operatorSysNo);
                if (!transferRet.Success)
                {
                    ServiceDepository.TransactionService.RollBack();
                    return transferRet;
                }
                #endregion

                #region ###################开始转账###################
                payeeAccount.Verson++;
                payerAccount.Verson++;

                payerAccount.ReceiptBalance -= order.OrderAmount;
                payeeAccount.ReceiptBalance += order.OrderAmount;

                if (!ServiceDepository.AccountDataAccess.Update(payerAccount))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-9, "收据余额更新失败");
                }
                if (!ServiceDepository.AccountDataAccess.Update(payeeAccount))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-9, "收据余额更新失败");
                }
                #endregion

                var result = new OperationResult(0);
                result.OtherData.Add("OrderNo", order.OrderNo);

                ServiceDepository.TransactionService.Commit();
                return result;
            }
            catch (Exception e)
            {
                ServiceDepository.TransactionService.RollBack();
                Log.Error(e);
                return new OperationResult(-5, "系统错误");
            }
        }

        private OperationResult SubmitTransfer(OrderInfo order, AccountInfo payer, AccountInfo payee, long operatorSysNo, bool openTransaction = false)
        {
            try
            {
                if (openTransaction)
                    ServiceDepository.TransactionService.Begin();
                payer.LockedMoney -= order.OrderAmount;
                payee.Balance += order.OrderAmount;

                payer.ModifyUserSysNo = operatorSysNo;
                if (payer.LockedMoney < 0 || !ServiceDepository.AccountDataAccess.Update(payer))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-1, "转账失败");
                }
                payee.ModifyUserSysNo = operatorSysNo;
                if (!ServiceDepository.AccountDataAccess.Update(payee))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-2, "转账失败");
                }
                //账户资金明细
                if (!ServiceDepository.AccountDetailDataAccess.Insert(new AccountDetailInfo(AccountDetailType.Transfer)
                {
                    AccountSysNo = payer.SysNo,
                    Amount = -order.OrderAmount,
                    OrderNo = order.OrderNo,
                    CreateUserSysNo = operatorSysNo
                }))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-3, "转账失败");
                }
                if (!ServiceDepository.AccountDetailDataAccess.Insert(new AccountDetailInfo(AccountDetailType.Transfer)
                {
                    AccountSysNo = payee.SysNo,
                    Amount = order.OrderAmount,
                    OrderNo = order.OrderNo,
                    CreateUserSysNo = operatorSysNo
                }))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-4, "转账失败");
                }
                //账户操作日志
                if (!ServiceDepository.AccountDataAccess.AddOperatorLog(new AccountOperationLog
                {
                    AccountSysNo = payer.SysNo,
                    Amount = Math.Abs(order.OrderAmount),
                    OperationType = AccountOperationType.ChargeOff,
                    RefBizNo = order.OrderNo,
                    CreateUserSysNo = operatorSysNo,
                    CreateDate = DateTime.Now
                }))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-5, "转账失败");
                }
                if (!ServiceDepository.AccountDataAccess.AddOperatorLog(new AccountOperationLog
                {
                    AccountSysNo = payee.SysNo,
                    Amount = Math.Abs(order.OrderAmount),
                    OperationType = AccountOperationType.Entry,
                    RefBizNo = order.OrderNo,
                    CreateUserSysNo = operatorSysNo,
                    CreateDate = DateTime.Now
                }))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-6, "转账失败");
                }
                if (openTransaction)
                    ServiceDepository.TransactionService.Commit();
            }
            catch (Exception e)
            {
                Log.Error(e);
                ServiceDepository.TransactionService.RollBack();
                return new OperationResult(-5, "系统异常");
            }
            return new OperationResult(0);
        }
    }
}
