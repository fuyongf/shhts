﻿using HTB.DevFx.Config;
using PinganHouse.SHHTS.Core.MessageHandler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.ServiceImpl.MessageHandlers.Config
{
    internal class MessageHandlerSetting : ConfigSettingElement, IMessageHandlerSetting
    {
        protected override void OnConfigSettingChanged()
        {
            base.OnConfigSettingChanged();
            this.Name = this.GetSetting("name");
            this.TypeName = this.GetSetting("type");
        }

        public string Name { get; private set; }
        public string TypeName { get; private set; }
    }
}
