﻿using HTB.DevFx.Config;
using PinganHouse.SHHTS.Core.MessageHandler;
using PinganHouse.SHHTS.ServiceImpl.MessageHandlers.Config;
[assembly: ConfigResource("res://PinganHouse.SHHTS.ServiceImpl.CaseMessageHandlers.Config.htb.devfx.messageHandler.config", Index = 0)]
namespace PinganHouse.SHHTS.ServiceImpl.CaseMessageHandlers.Config
{
    public class CaseSetting : ConfigSettingElement
    {
        protected override void OnConfigSettingChanged()
        {
            base.OnConfigSettingChanged();
            this.MessageHandlers = this.GetSettings<MessageHandlerSetting>("messageHandlers", null).ToArray();           
        }
        public IMessageHandlerSetting[] MessageHandlers { get; private set; }
    }
}
