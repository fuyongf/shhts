﻿using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Core.MessageHandler;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.ServiceImpl.MessageHandlers
{
    internal class AgencyCompanyMessageHandler : IMessageHandler
    {
        public void Init(ICaseService caseService)
        {
            caseService.OnUpdated -= this.CaseEventHandler;
            caseService.OnUpdated += this.CaseEventHandler;
        }
        private void CaseEventHandler(CaseEventInfo ce, Case caseInfo)
        {
            try
            {
                var agency = ServiceDepository.CaseDataAccess.GetRelation(caseInfo.CaseId, 2);
                if (agency == null)
                    throw new ArgumentException("中介不存在");
                var agentStaff = ServiceDepository.AgentStaffDataAccess.Get(agency[0].UserSysNo);
                if (agentStaff == null)
                    return;
                var result = SetMessage(ce, caseInfo, agentStaff);
                if (!result.Success)
                    return;

                var stern = ServiceDepository.SettingService.GetSettingItemValueByPath("/System/Parter/WeiXin/QY/AgencyACompany/Messages");
                if (string.IsNullOrWhiteSpace(stern))
                    throw new ArgumentException("stern is null");
                //\n\r linux与windows回车分行不一样， Environment.NewLine是看部署环境
                string message = string.Format("{0}{1}{2}", result.ResultMessage, "\n\r", stern);
                ServiceDepository.ParterChannelService.SendTextMessage(AgentType.AgentCompany, message, ReceiverType.ToDepartment, agentStaff.AgentCompanySysNo.ToString());
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

        }

        private OperationResult SetMessage(CaseEventInfo ce, Case caseInfo, AgentStaffInfo agent)
        {
            var result = new OperationResult(1);
            switch (ce.EventType)
            {
                case CaseStatus.HA2:
                    {
                        var message = ServiceDepository.SettingService.GetSettingItemValueByPath(string.Format("/System/Parter/WeiXin/QY/AgencyACompany/Messages/{0}", ce.EventType));
                        if (!string.IsNullOrWhiteSpace(message))
                        {
                            result.ResultNo = 0;
                            result.ResultMessage = string.Format(message, agent.RealName, string.Format("{0}({1})",ce.CaseId, caseInfo.TenementAddress), ce.CreateDate.ToString("yyyy-MM-dd HH:mm"));
                        }

                    }
                    break;
            }
            return result;
        }
    }
}
