﻿using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Core.MessageHandler;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.ServiceImpl.MessageHandlers
{
    internal class AgencyMessageHandler : IMessageHandler
    {
        public void Init(ICaseService caseService)
        {
            caseService.OnUpdated -= this.CaseEventHandler;
            caseService.OnUpdated += this.CaseEventHandler;
        }
        private void CaseEventHandler(CaseEventInfo ce, Case caseInfo)
        {
            try
            {
                var result = SetMessage(ce, caseInfo);
                if (!result.Success)
                    return;
                var agency = ServiceDepository.CaseDataAccess.GetRelation(caseInfo.CaseId, 2);
                if (agency == null)
                    throw new ArgumentException("中介不存在");
                var agentStaff = ServiceDepository.AgentStaffDataAccess.Get(agency[0].UserSysNo);
                if (agentStaff == null)
                    return;
                var stern = ServiceDepository.SettingService.GetSettingItemValueByPath("/System/Parter/WeiXin/QY/Agency/Messages");
                if (string.IsNullOrWhiteSpace(stern))
                    throw new ArgumentException("stern is null");
                string message = string.Format("{0}{1}{2}", result.ResultMessage, "\n\r", stern);
                ServiceDepository.ParterChannelService.SendTextMessage(AgentType.Agency, message, ReceiverType.ToUser, agentStaff.UserId);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }
        private OperationResult SetMessage(CaseEventInfo ce, Case caseInfo)
        {
            var result = new OperationResult(1);
            switch (ce.EventType)
            {
                case CaseStatus.HA2:
                case CaseStatus.ZB7:
                case CaseStatus.JY11A1:
                case CaseStatus.JY11:
                    {
                        var message = ServiceDepository.SettingService.GetSettingItemValueByPath(string.Format("/System/Parter/WeiXin/QY/Agency/Messages/{0}", ce.EventType));
                        if (!string.IsNullOrWhiteSpace(message))
                        {
                            result.ResultNo = 0;
                            result.ResultMessage = string.Format(message, string.Format("{0}({1})", ce.CaseId, caseInfo.TenementAddress), ce.CreateDate.ToString("yyyy-MM-dd HH:mm"));
                        }

                    }
                    break;
                case CaseStatus.JY8:
                    {
                        var path = string.Format("/System/Parter/WeiXin/QY/Agency/Messages/{0}/FirstMessage", ce.EventType);
                        if (ce.OtherDatas != null && ce.OtherDatas.ContainsKey("reSend"))
                            path= string.Format("/System/Parter/WeiXin/QY/Agency/Messages/{0}/ChangeMessage", ce.EventType);
                        var message = ServiceDepository.SettingService.GetSettingItemValueByPath(path);

                        if (!string.IsNullOrWhiteSpace(message))
                        {
                            result.ResultNo = 0;
                            object time="--";
                            if (ce.OtherDatas != null && ce.OtherDatas.ContainsKey("booktime"))
                                time = DateTime.Parse(ce.OtherDatas["booktime"].ToString()).ToString("yyyy-MM-dd HH:mm");
                            result.ResultMessage = string.Format(message, time, string.Format("{0}({1})",ce.CaseId, caseInfo.TenementAddress));
                        }
                    }
                    break;
            }
            return result;
        }
    }
}
