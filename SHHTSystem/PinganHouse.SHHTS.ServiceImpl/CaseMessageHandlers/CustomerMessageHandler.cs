﻿using HTB.DevFx;
using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Core.MessageHandler;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace PinganHouse.SHHTS.ServiceImpl.MessageHandlers
{
    internal class CustomerMessageHandler : IMessageHandler
    {
#if DEBUG
        const string WEB_HOST = "test.erpweixin.pinganhaofang.com";
#else
        const string WEB_HOST = "erpweixin.pinganhaofang.com";
#endif
        public void Init(ICaseService caseService)
        {
            caseService.OnUpdated -= this.CaseEventHandler;
            caseService.OnUpdated += this.CaseEventHandler;
        }
        private void CaseEventHandler(CaseEventInfo ce, Case caseInfo)
        {
            try
            {
                var templateId = ServiceDepository.SettingService.GetSettingItemValueByPath(string.Format("/System/Parter/WeiXin/Templates/CaseStatus/{0}", ce.EventType));
                if (string.IsNullOrWhiteSpace(templateId))
                    return;
                //是否存在消息模板
                var subSettings = ServiceDepository.SettingService.GetSettingItemsByPath(string.Format("/System/Parter/WeiXin/Templates/CaseStatus/{0}", ce.EventType));
                if (subSettings == null || subSettings.Count()==0)
                    return;

                var users = ServiceDepository.CaseDataAccess.GetRelation(ce.CaseId);
                if (users == null || users.Count == 0)
                    throw new ArgumentNullException("案件未存在客户");
                var stern = ServiceDepository.SettingService.GetSettingItemValueByPath("/System/Parter/WeiXin/Templates/CaseStatus");

                var oper = new UserInfo();
                if(ce.CreateUserSysNo!=null)
                    oper = ServiceDepository.UserDataAccess.Get(ce.CreateUserSysNo.Value);

                foreach (var user in users)
                {
                    if (user.CaseUserType == CaseUserType.AgentStaff || user.CaseUserType == CaseUserType.Handler)
                        continue;
                    var contents = this.SetContent(ce, user.CaseUserType, subSettings, stern, oper, caseInfo.ReceptionCenter, caseInfo.TenementAddress);
                    foreach (var content in contents)
                    {
                        ServiceDepository.ParterChannelService.SendTemplateMessage(user.UserSysNo, templateId, content);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

        }

        private UserInfo GetOperator(CaseEventInfo ce, Case caseInfo)
        {
            long? sysno = ce.CreateUserSysNo;
            if ((ce.EventType & (CaseStatus.JY1 | CaseStatus.HD2)) == 0)
                sysno = caseInfo.CreateUserSysNo;
            if(sysno!=null)
                return ServiceDepository.UserDataAccess.Get(sysno.Value);
            return new UserInfo();
        }
        private IList<IDictionary<string, object>> SetContent(CaseEventInfo ce, CaseUserType userType, IEnumerable<Setting> settings, string stern, UserInfo oper, ReceptionCenter receptionCenter, string tenementAddress)
        {
            var ld = new List<IDictionary<string, object>>();
            var setting = settings.FirstOrDefault(s => string.Compare(s.Name, userType.ToString(), true) == 0);
            if (setting == null)
                return ld;
            object date;
            string remark=string.Empty;
            var dic = SetTopDic(ce.CaseId, userType, setting, receptionCenter, ce.OtherDatas, out date, ref remark);
            IDictionary<string, Dictionary<string, object>> data = new Dictionary<string, Dictionary<string, object>>();        
            switch (ce.EventType)
            {
                case CaseStatus.HD3:
                case CaseStatus.HD4:
                case CaseStatus.DK5:
                case CaseStatus.JY5:
                case CaseStatus.DK8:   
                case CaseStatus.DK2:
                case CaseStatus.DK3:
                case CaseStatus.JY2:
                case CaseStatus.JY3:
                case CaseStatus.JY4:
                case CaseStatus.JY6:
                case CaseStatus.JY10:
                    {
                        data = JsonSerializer.DeserializeToObject<Dictionary<string, Dictionary<string, object>>>(setting.Value);
                        data["keyword1"]["value"] = ce.CreateDate.ToString("yyyy-MM-dd HH:mm");
                        data["keyword4"]["value"] = date;
                        data["keyword5"]["value"] = string.Format("{0}{1}", oper.RealName, oper.Mobile);
                    };
                    break;
                case CaseStatus.JY11:
                case CaseStatus.JY11A1:
                    {
                        data = JsonSerializer.DeserializeToObject<Dictionary<string, Dictionary<string, object>>>(setting.Value);
                        data["keyword1"]["value"] = ce.CreateDate.ToString("yyyy-MM-dd HH:mm");
                        data["keyword4"]["value"] = date;
                        data["keyword5"]["value"] = string.Format("{0}{1}", oper.RealName, oper.Mobile);
                        data["remark"]["value"] = data["remark"]["value"].ToString().Replace("#Name#", remark);
                    };
                    break;
                case CaseStatus.HD6:
                   {
                       
                        data = JsonSerializer.DeserializeToObject<Dictionary<string, Dictionary<string, object>>>(setting.Value);
                        data["keyword1"]["value"] = ce.CreateDate.ToString("yyyy-MM-dd HH:mm");
                        var caseEvent = ServiceDepository.CaseEventDataAccess.GetNewByCaseId(ce.CaseId, CaseStatus.HD5);
                        if (caseEvent != null && caseEvent.OtherDatas != null && caseEvent.OtherDatas.ContainsKey("AttnTime"))
                            data["keyword1"]["value"] = DateTime.Parse(caseEvent.OtherDatas["AttnTime"].ToString()).ToString("yyyy-MM-dd HH:mm");
                        data["keyword4"]["value"] = date;
                        data["keyword5"]["value"] = string.Format("{0}{1}", oper.RealName, oper.Mobile);
                    };
                    break;
                case CaseStatus.JY8:
                    {                       
                        data = JsonSerializer.DeserializeToObject<Dictionary<string, Dictionary<string, object>>>(setting.Value);
                        data["keyword2"]["value"] = date;
                        if (ce.OtherDatas != null && ce.OtherDatas.ContainsKey("reSend"))
                            data["first"]["value"] = "亲~，已为您重新预约房屋缴税、过户";
                    };
                    break;
                case CaseStatus.HD2:
                    {
                        data = JsonSerializer.DeserializeToObject<Dictionary<string, Dictionary<string, object>>>(setting.Value);
                        data["keyword2"]["value"] = date;
                    };
                    break;
                case CaseStatus.DK9:
                case CaseStatus.ZB8:
                    {
                        data = JsonSerializer.DeserializeToObject<Dictionary<string, Dictionary<string, object>>>(setting.Value);
                        data["keyword1"]["value"] = string.Format("{0}({1})", ce.CaseId, tenementAddress);
                        data["keyword3"]["value"] = ce.CreateDate.ToString("yyyy-MM-dd HH:mm");
                    };
                    break;
                case CaseStatus.HA2:
                case CaseStatus.JY9:
                case CaseStatus.ZB7:
                case CaseStatus.ZB11:
                    {
                        data = JsonSerializer.DeserializeToObject<Dictionary<string, Dictionary<string, object>>>(setting.Value);
                        data["keyword1"]["value"] = string.Format("{0}({1})", ce.CaseId, tenementAddress);
                        data["keyword3"]["value"] = ce.CreateDate.ToString("yyyy-MM-dd HH:mm");
                    };
                    break;
                case CaseStatus.JY1:
                    {
                        var preEvent = ServiceDepository.CaseEventDataAccess.GetNewByCaseId(ce.CaseId, CaseStatus.ZB1);
                        dic = SetTopDic(ce.CaseId, userType, setting, receptionCenter, preEvent.OtherDatas,  out date, ref remark);
                        var ms = setting.Value.Split('@');
                        for (var i = 0; i < ms.Length; i++)
                        {
                            data = JsonSerializer.DeserializeToObject<Dictionary<string, Dictionary<string, object>>>(ms[i]);
                            data["keyword2"]["value"] = date;                         
                            if (i == ms.Length - 1)
                                continue;
                            data["remark"]["value"] = SetRemark(data["remark"]["value"].ToString(), stern, ce.CaseId, tenementAddress); 
                            dic["data"] = data;
                            ld.Add(dic);
                            dic = SetTopDic(ce.CaseId, userType, setting, receptionCenter, preEvent.OtherDatas, out date, ref remark, i + 1);
                        }
                    };
                    break;
            }
            data["remark"]["value"] = SetRemark(data["remark"]["value"].ToString(), stern, ce.CaseId, tenementAddress); 
            data["first"]["value"] = string.Format("{0}{1}", data["first"]["value"], Environment.NewLine);
            dic["data"] = data;
            ld.Add(dic);
            return ld;
        }

        private string SetRemark(string remark, string stern, string caseId, string tenementAddress)
        {
            remark = remark.Replace("#CaseId#", caseId).Replace("#TenementAddress#", tenementAddress);
            return string.Format("{0}{1}{2}", Environment.NewLine, remark, stern.TrimStart(Environment.NewLine));
        }
        private Dictionary<string, object> SetTopDic(string caseId, CaseUserType userType, Setting parentSetting, ReceptionCenter receptionCenter, IDictionary<string, object> data, out object dateTime, ref string remark, int index=0)
        {
            var dic = new Dictionary<string, object>
            {
                {"topcolor","#FF0000"},
            };
            
            //预计下次完成时间
            dateTime = "--";
            object date;
            if (data != null)
            {
                if (data.TryGetValue("AttnTime", out date) || data.TryGetValue("booktime", out date) || data.TryGetValue("SendDate", out date) || data.TryGetValue("ReceiveDate", out date))
                {
                    DateTime time = DateTime.Now;
                    if (DateTime.TryParse(date.ToString(), out time))
                        dateTime = time.ToString("yyyy-MM-dd HH:mm").TrimEnd("00:00");
                }
                else
                {
                    dateTime = "--";
                }

            }

            string address = GetAddress(caseId, receptionCenter, data, ref remark);
            var settings = ServiceDepository.SettingService.GetSettingItemsByPath(parentSetting.Path);
            //是否存在材料
            var materialSetting = settings.FirstOrDefault(s => s.Name.CompareTo("Material") == 0);
            if (materialSetting != null)
            {
                var scheduleSetting = settings.FirstOrDefault(s => s.Name.CompareTo("Schedule") == 0);
                //已完成流程
                var preProcesses = scheduleSetting.Description.Split('@');
                //待办理流程
                var nextProcesses = scheduleSetting.Value.Split('@');
                //所需材料
                var materiales = materialSetting.Value.Split('@');

                dic["url"] = string.Format("http://{8}/Case/ShowProcess?sendDate={0}&preProcess={1}&nextProcess={2}&nextDate={3}&receptionCenter={4}&path={5}&index={6}&address={7}"
                    , HttpUtility.UrlEncode(DateTime.Now.ToString("yyyy年MM月dd日")), HttpUtility.UrlEncode(preProcesses[index]), HttpUtility.UrlEncode(nextProcesses[index]), dateTime, receptionCenter,
                    HttpUtility.UrlEncode(string.Format("{0}/Material", parentSetting.Path)), index, HttpUtility.UrlEncode(address), WEB_HOST);
            }
            return dic;
        }

        private string GetAddress(string caseId, ReceptionCenter receptionCenter, IDictionary<string, object> data, ref string remark)
        {
            remark = "房地产交易中心";
            object address = ServiceDepository.SettingService.GetSettingItemValueByPath(string.Format("/Biz/Static/TradingCenter/{0}", receptionCenter));
            DelegationType delegationType;
            if (data == null || !data.ContainsKey("CheckResult") || !Enum.TryParse<DelegationType>(data["CheckResult"].ToString(), out delegationType))
                return address.ToString();
            switch(delegationType)
            {
                case DelegationType.LoanBank:
                    {
                        var dk1Event = ServiceDepository.CaseEventDataAccess.GetNewByCaseId(caseId, CaseStatus.DK1);
                        if (dk1Event != null && dk1Event.OtherDatas != null && dk1Event.OtherDatas.ContainsKey("LoanObject"))
                        {
                            LoanObject loan = (LoanObject)Enum.Parse(typeof(LoanObject), dk1Event.OtherDatas["LoanObject"].ToString(), true);
                            if (loan == LoanObject.Bank && dk1Event.OtherDatas.ContainsKey("ResponseObject"))
                            {
                                address = ServiceDepository.SettingService.GetSettingItemValueByPath(string.Format("/Biz/Static/BankConfig/{0}", dk1Event.OtherDatas["ResponseObject"]));
                                remark = "贷款银行";
                            }                
                        }
                    }
                    break;
                case DelegationType.Bourse:
                    data.TryGetValue("FundCentre", out address);
                    remark = "公积金管理中心";
                    break;
                case DelegationType.Trader:
                    address = ServiceDepository.SettingService.GetSettingItemValueByPath(string.Format("/Biz/Static/ReceptionCenter/{0}", receptionCenter));
                    remark = "平安好房业务受理中心";
                    break;
            }
            return address.ToString();
        }
    }
}
