﻿using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Core.MessageHandler;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.ServiceImpl.CaseMessageHandlers
{
    internal class CustomerSmsMessageHandler : IMessageHandler
    {
        public void Init(ICaseService service)
        {
            service.OnUpdated -= this.CaseEventHandler;
            service.OnUpdated += this.CaseEventHandler;
        }
        private void CaseEventHandler(CaseEventInfo ce, Case caseInfo)
        {
            try
            {
                var pl = SetParameters(ce, caseInfo);
                if (pl == null || pl.Count == 0)
                    return;
                //买卖家类别
                foreach (var p in pl)
                {
                    //人集合
                    foreach (var user in p.Item1)
                    {
                        var customer = ServiceDepository.CustomerDataAccess.Get(user.UserSysNo);
                        if (customer == null)
                            continue;
                        if (!Validation.Mobile(customer.Mobile))
                            continue;
                        var userExtend = ServiceDepository.UserExendDataAccess.Get(customer.UserSysNo);
                        if (userExtend != null && !userExtend.IsSendSms)
                            continue;
                        //短信类别
                        foreach (var c in p.Item3)
                        {
                            var result = ServiceDepository.SmsService.Send(c, customer.Mobile, p.Item2);
                            if (!result.Success)
                                Log.Error(string.Format("短信发送失败,模板：{0},{1}", c, result.ResultMessage));
                        }
                    }
                }          

            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

        }

        public List<Tuple<List<CaseUserRelation>, Dictionary<string, object>, List<SmsCategory>>> SetParameters(CaseEventInfo ce, Case caseInfo)
        {

            var relations = ServiceDepository.CaseDataAccess.GetRelation(caseInfo.CaseId);
            if (relations == null || relations.Count == 0)
                return null;
            var setting = ServiceDepository.SettingService.GetSettingItemByPath("/Biz/Static/Contact");
            string contact = "400-820-1100转2";
            if (setting != null && !string.IsNullOrWhiteSpace(setting.Value))
                contact = setting.Value;

            var parameters = new Dictionary<string, object> { { "phone", contact }, { "number", ce.CaseId }, { "datetime", "" }, { "date", "" } };
            var buyCategorys = new List<SmsCategory>();
            var sellerCategorys = new List<SmsCategory>();
            var buyers = new List<CaseUserRelation>();
            var sellers = new List<CaseUserRelation>();
            var pl = new List<Tuple<List<CaseUserRelation>, Dictionary<string, object>, List<SmsCategory>>>();
            switch (ce.EventType)
            {
                case CaseStatus.ZB1:
                    {
                        object date;
                        //存在预约时间才发送
                        if (ce.OtherDatas != null && ce.OtherDatas.TryGetValue("booktime", out date))
                        {
                            //买家
                            buyers = relations.Where(r => r.CaseUserType == CaseUserType.Buyer).ToList();
                            buyCategorys.Add(SmsCategory.YY_HJ);
                            buyCategorys.Add(SmsCategory.YY_FCS_RD);
                            //卖家
                            sellers = relations.Where(r => r.CaseUserType == CaseUserType.Seller).ToList();
                            sellerCategorys.Add(SmsCategory.YY_HJ);
                            sellerCategorys.Add(SmsCategory.YY_FCS_RD);
                            parameters["datetime"] = DateTime.Parse(date.ToString()).ToString("MM月dd日HH：mm");
                            parameters["number"] = ce.CaseId;
                        }
                    }
                    break;
                case CaseStatus.JY4:
                    {
                        buyers = relations.Where(r => r.CaseUserType == CaseUserType.Buyer).ToList();
                        buyCategorys.Add(SmsCategory.TJ_FW_CHAXUN);
                        parameters["number"] = ce.CaseId;
                        object date;
                        if (ce.OtherDatas != null && ce.OtherDatas.TryGetValue("SendDate", out date))
                            parameters["date"] = DateTime.Parse(date.ToString()).ToString("MM月dd日");
                    }
                    break;
                case CaseStatus.HA5:
                    {
                        buyers = relations.Where(r => r.CaseUserType == CaseUserType.Buyer).ToList();
                        sellers = relations.Where(r => r.CaseUserType == CaseUserType.Seller).ToList();
                        buyCategorys.Add(SmsCategory.QY_FINISH);
                        sellerCategorys.Add(SmsCategory.QY_FINISH);
                    }
                    break;
                case CaseStatus.ZB7:
                    {
                        buyers = relations.Where(r => r.CaseUserType == CaseUserType.Buyer).ToList();
                        sellers = relations.Where(r => r.CaseUserType == CaseUserType.Seller).ToList();
                        buyCategorys.Add(SmsCategory.FW_JIAOYI_END);
                        sellerCategorys.Add(SmsCategory.FW_JIAOYI_END);
                    }
                    break;
                case CaseStatus.JY8:
                    {
                        buyers = relations.Where(r => r.CaseUserType == CaseUserType.Buyer).ToList();
                        sellers = relations.Where(r => r.CaseUserType == CaseUserType.Seller).ToList();
                        buyCategorys.Add(SmsCategory.YY_BL_GUOHU);
                        sellerCategorys.Add(SmsCategory.YY_BL_GUOHU);
                        object date;
                        if (ce.OtherDatas != null && ce.OtherDatas.TryGetValue("booktime", out date))
                            parameters["date"] = DateTime.Parse(date.ToString()).ToString("MM月dd日");
                    }
                    break;
                case CaseStatus.ZB8:
                    {
                        buyers = relations.Where(r => r.CaseUserType == CaseUserType.Buyer).ToList();
                        sellers = relations.Where(r => r.CaseUserType == CaseUserType.Seller).ToList();
                        buyCategorys.Add(SmsCategory.BUY_WC_FW_JY);
                        parameters["address"] = caseInfo.TenementAddress;
                        sellerCategorys.Add(SmsCategory.MF_WC_FW_JY);
                    }
                    break;
                case CaseStatus.DK3:
                    {
                        buyers = relations.Where(r => r.CaseUserType == CaseUserType.Buyer).ToList();
                        buyCategorys.Add(SmsCategory.QS_DK_SQ_XY);
                        parameters["number"] = ce.CaseId;
                    }
                    break;
                case CaseStatus.DK5:
                    {
                        buyers = relations.Where(r => r.CaseUserType == CaseUserType.Buyer).ToList();
                        buyCategorys.Add(SmsCategory.DAIKUANG_SQ_OK);
                        parameters["number"] = ce.CaseId;
                    }
                    break;
                case CaseStatus.DK8:
                    {
                        buyers = relations.Where(r => r.CaseUserType == CaseUserType.Buyer).ToList();
                        buyCategorys.Add(SmsCategory.DAIKUANG_SEND);
                        parameters["number"] = ce.CaseId;
                    }
                    break;
                case CaseStatus.ZB11:
                    {
                        buyers = relations.Where(r => r.CaseUserType == CaseUserType.Buyer).ToList();
                        buyCategorys.Add(SmsCategory.QUXIAO_DAIKUANG);
                        parameters["number"] = ce.CaseId;
                    }
                    break;
                default:
                    return null;

            }
            //买方集合
            pl.Add(Tuple.Create(buyers, parameters, buyCategorys));
            //卖方集合
            pl.Add(Tuple.Create(sellers, parameters, sellerCategorys));
            return pl;
        }
    }
}
