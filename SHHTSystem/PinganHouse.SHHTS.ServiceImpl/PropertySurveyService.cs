﻿using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using System.ServiceModel.Activation;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.ServiceImpl
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PropertySurveyService : IPropertySurveyService
    {
        public OperationResult AddPropertySurvey(PropertySurveyDto propertySurveyDto, string tenementAddress, List<long> proprietors)
        {
            try 
            {
                if (propertySurveyDto == null)
                    return new OperationResult(1, "param is null");
                if (string.IsNullOrWhiteSpace(propertySurveyDto.TenementContract))
                    return new OperationResult(2, "TenementContract is null");
                if (!ServiceDepository.PropertyCertificateDataAccess.Exist(propertySurveyDto.TenementContract))
                {
                    PropertyCertificate propertyCertificate = new PropertyCertificate
                    {
                        TenementAddress = tenementAddress,
                        TenementContract = propertySurveyDto.TenementContract,
                        Proprietors = proprietors,
                        CreateUserSysNo = propertySurveyDto.CreateUserSysNo,
                        CreateDate = DateTime.Now,
                    };
                    if (!ServiceDepository.PropertyCertificateDataAccess.Insert(propertyCertificate))
                        return new OperationResult(3, "添加失败");
                }
                var entity = ObjectMapper.Copy<PropertySurvey>(propertySurveyDto);
                if (!ServiceDepository.PropertySurveyDataAccess.Insert(entity))
                    return new OperationResult(4, "保存失败");
                return new OperationResult(0);
            }
            catch (Exception ex) 
            {
                Log.Error(ex);
                return new OperationResult(-1, "系统内部错误");
            }
        }

        public Tuple<PropertyCertificateDto, IList<PropertySurveyDto>> GetPropertySurvey(string tenementContract)
        {
            if (string.IsNullOrWhiteSpace(tenementContract))
                return null;
            try 
            {
                var pc = new PropertyCertificateDto {
                    TenementContract = tenementContract,
                };  
                pc = SetPropertyCertificateDto(pc, false);
                if (pc == null)
                    return null;
                var result = ServiceDepository.PropertySurveyDataAccess.GetPropertySurveys(tenementContract);
                if (result == null || result.Count == 0)
                    return new Tuple<PropertyCertificateDto, IList<PropertySurveyDto>>(pc, null);
                var pds =  ObjectMapper.Copy<PropertySurvey, PropertySurveyDto>(result);
                SetOtherData(pds);
                return new Tuple<PropertyCertificateDto, IList<PropertySurveyDto>>(pc, pds);
            }
            catch (Exception ex)
            {
                Log.Error(ex);               
            }
            return null;
        }

        public IList<Tuple<PropertyCertificateDto, IEnumerable<PropertySurveyDto>>> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, string tenementContract,
                string tenementAddress, DateTime? startDate, DateTime? endDate, IDictionary<string, object> data)
        {
            totalCount = 0;
            try 
            {
                if (startDate != null && endDate != null && endDate.Value < startDate.Value)
                    return null;
                var pc = ServiceDepository.PropertyCertificateDataAccess.GetPaginatedList(pageIndex, pageSize, out  totalCount,
                    tenementContract, tenementAddress, startDate, endDate, data);
                if (pc == null || pc.Count == 0)
                    return null;
                var pcd = ObjectMapper.Copy<PropertyCertificate, PropertyCertificateDto>(pc);
                var ts = new List<Tuple<PropertyCertificateDto, IEnumerable<PropertySurveyDto>>>();
                var ps = ServiceDepository.PropertySurveyDataAccess.GetPropertySurveys(pc.Select(p=>p.TenementContract));
                IList<PropertySurveyDto> psd = new List<PropertySurveyDto>();
                if(ps!=null && ps.Count>0)
                    psd = ObjectMapper.Copy<PropertySurvey, PropertySurveyDto>(ps);

                //设置其他信息
                SetOtherData(psd);
                foreach (var pd in pcd)
                {
                    SetPropertyCertificateDto(pd, true);
                    ts.Add(Tuple.Create(pd, psd.Where(p=>p.TenementContract==pd.TenementContract).OrderBy(p=>p.CreateDate).AsEnumerable()));
                }
                return ts;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return null;
        }

        /// <summary>
        /// 设置关联案件等信息
        /// </summary>
        /// <param name="pcd"></param>
        /// <param name="search"></param>
        /// <returns></returns>
        private PropertyCertificateDto SetPropertyCertificateDto(PropertyCertificateDto pcd, bool search)
        {
            //目前产证信息先从案件获取
            var caseInfo = ServiceDepository.CaseDataAccess.GetCaseByTenementContract(pcd.TenementContract);
            if (caseInfo == null)
            {
                if (search)
                    return pcd;
                var propertyCertificate = ServiceDepository.PropertyCertificateDataAccess.Get(pcd.TenementContract);
                if (propertyCertificate == null)
                    return null;
                pcd = ObjectMapper.Copy<PropertyCertificateDto>(propertyCertificate);
            }
            else
            {
                pcd.TenementAddress = caseInfo.TenementAddress;
                pcd.OtherData["CaseId"] = caseInfo.CaseId;
                var users = ServiceDepository.CaseDataAccess.GetRelation(caseInfo.CaseId);
                if (users != null && users.Count > 0)
                {
                    foreach (var u in users)
                    {
                        if (u.CaseUserType == CaseUserType.Seller)
                        {
                            pcd.Proprietors.Add(u.UserSysNo);
                            pcd.ProprietorNameDic[u.UserSysNo] = ServiceDepository.CustomerDataAccess.GetRealName(u.UserSysNo);
                        }
                    }
                }
            }
            //获取产调附件数量
            var attements = ServiceDepository.AttachmentDataAccess.GetAttachments(pcd.TenementContract, AttachmentType.产调证);
            pcd.OtherData["AttachmentCount"] = 0;
            if (attements != null && attements.Count>0)
                pcd.OtherData["AttachmentCount"] = attements.Count;
            return pcd;
        }
        private void SetOtherData(IEnumerable<PropertySurveyDto> datas)
        {
            if(datas==null || datas.Count()==0)
                return;
            Dictionary<long, Dictionary<string, object>> cache = new Dictionary<long, Dictionary<string, object>>();
            foreach (var data in datas)
            {
                var udic = new Dictionary<string, object>();
                if (data.ProposerSysNo != null)
                {
                    if(!cache.ContainsKey(data.ProposerSysNo.Value) || !cache[data.ProposerSysNo.Value].ContainsKey("ProposerName"))
                    {
                        var user = ServiceDepository.AgentStaffDataAccess.Get(data.ProposerSysNo.Value);
                        udic["ProposerName"] = "";
                        udic["CompanyName"] = "";
                        if (user != null)
                        {
                            udic["ProposerName"] = user.RealName;
                            var company = ServiceDepository.AgentCompanyDataAccess.Get(user.AgentCompanySysNo);
                            if (company != null)
                                udic["CompanyName"] = company.CompanyName;
                        }
                        cache[data.ProposerSysNo.Value] = udic;
                        
                    }
                    //不能直接引用cache[data.ProposerSysNo.Value]对象，否则修改data.OtherData同时修改cache[data.ProposerSysNo.Value]
                    data.OtherData["ProposerName"] = cache[data.ProposerSysNo.Value]["ProposerName"];
                    data.OtherData["CompanyName"] = cache[data.ProposerSysNo.Value]["CompanyName"];
                }

                if(data.CreateUserSysNo!=null)
                {
                    if (!cache.ContainsKey(data.CreateUserSysNo.Value))
                    {
                        cache[data.CreateUserSysNo.Value] = new Dictionary<string, object>();
                        cache[data.CreateUserSysNo.Value]["CreateUserName"] = "";
                        var user = ServiceDepository.UserDataAccess.Get(data.CreateUserSysNo.Value);
                        if (user != null)
                            cache[data.CreateUserSysNo.Value]["CreateUserName"] = user.RealName;
                    }
                    else if (!cache[data.CreateUserSysNo.Value].ContainsKey("CreateUserName"))
                    { 
                        cache[data.CreateUserSysNo.Value]["CreateUserName"] = cache[data.CreateUserSysNo.Value]["ProposerName"];
                    }                    
                    data.OtherData["CreateUserName"] = cache[data.CreateUserSysNo.Value]["CreateUserName"];
                }
            }
        }
    }
}
