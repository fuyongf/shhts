﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Activation;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.ServiceImpl
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public partial class TradeService : ITradeService
    {
        public OperationResult SubmitPosTrade(string orderNo, string serialNo, string terminalNo, decimal amount, string bankAccountNo, string bankCode, string accountName, string bankName, DateTime paymentDate, string remark, long operatorSysNo)
        {
            #region ###################参数校验###################
            if (string.IsNullOrWhiteSpace(orderNo) || string.IsNullOrWhiteSpace(serialNo) || amount <= 0)
                return new OperationResult(-1, "非法参数信息");
            var order = ServiceDepository.OrderDataAccess.Get(orderNo);
            if (order == null)
                return new OperationResult(-2, "订单不存在");
            if (order.OrderStatus != OrderStatus.Initial)
                return new OperationResult(-3, "订单状态错误");
            var trades = ServiceDepository.TradeDataAccess.GetByOrder(orderNo);
            decimal tradeAmount = 0;
            if (trades != null && trades.Count > 0)
                tradeAmount = trades.Where(t => (t.Status != TradeStatus.Cancelled && t.Status != TradeStatus.Failed && t.Status != TradeStatus.Closed)).Sum(t => t.Amount);
            if ((order.OrderAmount - tradeAmount) < amount)
                return new OperationResult(-4, "订单金额错误");
            #endregion
            TradeInfo detail = new TradeInfo
            {
                SysNo = Sequence.Get(),
                Amount = amount,
                PaymentChannel = PaymentChannel.POS,
                OrderNo = orderNo,
                SerialNo = serialNo,
                AccountName = accountName,
                BankAccountNo = bankAccountNo,
                BankCode = bankCode,
                SubBankName = bankName,
                CreateUserSysNo = operatorSysNo,
                CreateDate = DateTime.Now,
                Remark = remark,
                Status = TradeStatus.Submitted,
                PaymentDate = paymentDate,
                OrderBizType = order.OrderBizType
            };
            BillInfo bill = null;
            if (order.PaymentChannel == PaymentChannel.POS)
            {
                bill = new BillInfo
                {
                    SysNo = Sequence.Get(),
                    BillCategory = BillCategory.POS,
                    BillType = BillType.Expened,
                    Amount = amount,
                    BillDate = paymentDate,
                    BillNo = serialNo,
                    TradeNo = detail.TradeNo,
                    TerminalNo = terminalNo,
                    Decription = remark,
                    CreateUserSysNo = operatorSysNo,
                    CreateDate = DateTime.Now,
                    PayAccount = bankAccountNo,
                    Payer = accountName
                };
            }
            try
            {
                ServiceDepository.TransactionService.Begin();
                //创建流水
                if (!ServiceDepository.TradeDataAccess.Insert(detail))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-6, "交易明细创建失败");
                }
                //创建票据
                if (order.PaymentChannel == PaymentChannel.POS && !ServiceDepository.BillDataAccess.Insert(bill))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-7, "票据信息创建失败");
                }
                //修改预入账金额
                var accountInfo = ServiceDepository.AccountDataAccess.Get(order.AccountSysNo, order.OrderBizType);

                if (order.OrderType == OrderType.Collection)
                {
                    accountInfo.PreEntryMoney += amount;
                    accountInfo.ModifyUserSysNo = operatorSysNo;
                    accountInfo.ModifyDate = DateTime.Now;

                    if (!ServiceDepository.AccountDataAccess.Update(accountInfo))
                    {
                        ServiceDepository.TransactionService.RollBack();
                        return new OperationResult(-8, "账户操作失败");
                    }
                }

                #region ###################最后提交对账请求到财务系统###################
                if (!SubmitCollectionRequest(accountInfo, order, detail, terminalNo).Success)
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-9, "收款对账请求提交失败");
                }
                #endregion

                ServiceDepository.TransactionService.Commit();

                OperationResult result = new OperationResult(0, "交易成功");
                result.OtherData.Add("TradeNo", detail.TradeNo);
                return result;
            }
            catch (Exception e)
            {
                ServiceDepository.TransactionService.RollBack();
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }

        public OperationResult ReconciliationCallback(string tradeNo, ReconciliationStatus status, decimal amount, long? operatorSysNo)
        {
            #region ###################参数校验###################
            if (string.IsNullOrEmpty(tradeNo))
                return new OperationResult(-1, "交易编号错误");
            var trade = ServiceDepository.TradeDataAccess.Get(tradeNo);
            if (trade == null)
                return new OperationResult(-2, "交易流水不存在");
            if (status == trade.ReconciliationStatus)
                return new OperationResult(0, "重复操作");
            if (trade.Status != TradeStatus.Submitted || trade.Status == TradeStatus.Warning)
                return new OperationResult(-3, "交易状态错误");
            if (trade.Amount != amount)
                return new OperationResult(-4, "交易金额不匹配");
            var order = ServiceDepository.OrderDataAccess.Get(trade.OrderNo);
            if (order == null)
                return new OperationResult(-6, "订单不存在");
            if (order.OrderStatus != OrderStatus.Initial)
                return new OperationResult(-7, "订单状态错误");
            var account = ServiceDepository.AccountDataAccess.Get(order.AccountSysNo, order.OrderBizType);
            #endregion
            try
            {
                #region ###################逻辑处理###################
                trade.Status = status == ReconciliationStatus.Success ? TradeStatus.Completed : (status == ReconciliationStatus.Repetition ? TradeStatus.Warning : TradeStatus.Failed);
                trade.ReconciliationStatus = status;
                //if (status != ReconciliationStatus.Repetition)
                //    account.PreEntryMoney -= trade.Amount;
                bool updateOrder = false;
                if (status == ReconciliationStatus.Success)
                {
                    var trades = ServiceDepository.TradeDataAccess.GetByOrder(order.OrderNo);
                    var completedTrades = trades.Where(t => t.Status == TradeStatus.Completed);
                    if ((completedTrades.Sum(t => t.Amount) + trade.Amount) == order.OrderAmount)
                    {
                        order.OrderStatus = OrderStatus.Paid;
                        updateOrder = true;
                    }
                }
                if (operatorSysNo != null)
                {
                    trade.ModifyUserSysNo = operatorSysNo;
                    if (updateOrder)
                        order.ModifyUserSysNo = operatorSysNo;
                }
                #endregion

                ServiceDepository.TransactionService.Begin();

                #region ###################事务保存###################
                if (!ServiceDepository.TradeDataAccess.Update(trade))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-9, "交易流水状态保存错误");
                }
                if (updateOrder && !ServiceDepository.OrderDataAccess.Update(order))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-10, "订单状态保存错误");
                }

                OperationResult confirmResult = null;
                switch (order.OrderType)
                {
                    case OrderType.Collection: confirmResult = ConfirmCollectionTrade(trade, order, account, operatorSysNo); break;
                    case OrderType.Expend: confirmResult = ConfirmExpendTrade(trade, order, account, operatorSysNo); break;
                    default: break;
                }

                if (confirmResult == null)
                {
                    ServiceDepository.TransactionService.RollBack();
                    throw new NotImplementedException(string.Format("not implemented confirm logic for order type {0}", order.OrderType.ToString()));
                }

                if (!confirmResult.Success)
                {
                    ServiceDepository.TransactionService.RollBack();
                    return confirmResult;
                }
                #endregion

                ServiceDepository.TransactionService.Commit();

                //收款消息及服务费
                if (order.OrderType == OrderType.Collection || order.OrderType == OrderType.Expend)
                    ServiceDepository.MessageService.SendCollectionMessage(trade.TradeNo, trade.ReconciliationStatus, trade.Amount);

                return new OperationResult(0);
            }
            catch (Exception e)
            {
                ServiceDepository.TransactionService.RollBack();
                Log.Error(e);
                return new OperationResult(-5, e.Message);
            }
        }

        public OperationResult PaymentCallback(string tradeNo, PaymentStatus status, string serialNo, DateTime? paymentTime, string rejectReason, long? operatorSysNo)
        {
            #region ###################参数校验###################
            if (string.IsNullOrEmpty(tradeNo))
                return new OperationResult(-1, "交易编号错误");
            var trade = ServiceDepository.TradeDataAccess.Get(tradeNo);
            if (trade == null)
                return new OperationResult(-2, "交易明细不存在");
            var order = ServiceDepository.OrderDataAccess.Get(trade.OrderNo);
            if (order == null)
                return new OperationResult(-3, "订单不存在");
            if ((trade.Status == TradeStatus.Completed && status == PaymentStatus.Success) ||
                (trade.Status == TradeStatus.Failed && status == PaymentStatus.Failed) ||
                (order.AuditStatus == AuditStatus.FinanceAudited && status == PaymentStatus.FinanceAuditPass) ||
                (order.AuditStatus == AuditStatus.FinanceAuditFailed && status == PaymentStatus.FinanceAuditFailed))
                return new OperationResult(0, "重复通知");
            if (trade.Status != TradeStatus.Submitted)
                return new OperationResult(-4, "交易状态错误");
            //if (order.OrderStatus != OrderStatus.Submitted && order.OrderStatus != OrderStatus.Refunding)
            //    return new OperationResult(-6, "订单状态错误");
            //var account = ServiceDepository.AccountDataAccess.Get(order.OrderType == OrderType.Payment ? order.OppositeAccount.Value : order.AccountSysNo);
            //if (account.LockedMoney < Math.Abs(trade.Amount))
            //    return new OperationResult(-7, "账户异常");
            #endregion
            try
            {
                ServiceDepository.TransactionService.Begin();

                #region ###################交易审核日志###################
                if (status == PaymentStatus.FinanceAuditPass || status == PaymentStatus.FinanceAuditFailed)
                {
                    if (!ServiceDepository.AuditLogDataAccess.Insert(new AuditLogInfo
                    {
                        AuditObjectType = AuditObjectType.Trade,
                        ChangeValue = (int)(status == PaymentStatus.FinanceAuditPass ? AuditStatus.FinanceAudited : AuditStatus.FinanceAuditFailed),
                        Department = "总部财务",
                        SubjectId = trade.TradeNo,
                        Remark = rejectReason
                    }))
                    {
                        ServiceDepository.TransactionService.RollBack();
                        return new OperationResult(-12, "审核日志创建失败");
                    }
                }
                #endregion

                #region ###################修改交易流水信息###################
                if (status != PaymentStatus.FinanceAuditPass)
                {
                    if (!string.IsNullOrWhiteSpace(serialNo))
                        trade.SerialNo = serialNo;
                    trade.Status = status == PaymentStatus.Success ? TradeStatus.Completed : TradeStatus.Failed;
                    trade.PaymentDate = paymentTime;
                    if (operatorSysNo != null)
                        trade.ModifyUserSysNo = operatorSysNo;
                    if (!ServiceDepository.TradeDataAccess.Update(trade))
                    {
                        ServiceDepository.TransactionService.RollBack();
                        return new OperationResult(-8, "更新交易流水状态错误");
                    }
                }
                #endregion

                OperationResult confirmResult = null;
                switch (order.OrderType)
                {
                    case OrderType.Payment: confirmResult = ConfirmPaymentTrade(trade, order, status, rejectReason, operatorSysNo); break;
                    case OrderType.Withdraw: confirmResult = ConfirmWithdrawTrade(trade, order, status, rejectReason, operatorSysNo); break;
                    default: break;
                }
                if (confirmResult == null)
                {
                    ServiceDepository.TransactionService.RollBack();
                    throw new NotImplementedException(string.Format("not implement confirm handler for order type {0}", order.OrderType.ToString()));
                }
                if (!confirmResult.Success)
                {
                    ServiceDepository.TransactionService.RollBack();
                    return confirmResult;
                }

                ServiceDepository.TransactionService.Commit();
                //付款微信消息推送
                ServiceDepository.MessageService.SendPaymentMessage(tradeNo, status);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, e.Message);
            }
            return new OperationResult(0);
        }

        public OperationResult SubmitDeductSchedule(AccountType accountType, string subjectId, OrderBizType deductType, decimal amount, string remark, long operatorSysNo, IEnumerable<OrderBizType> limitSource)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(subjectId))
                    return new OperationResult(-1, "账户编号错误");
                if (amount <= 0)
                    return new OperationResult(-2, "金额错误");
                var account = ServiceDepository.AccountDataAccess.Get(subjectId, accountType, deductType);
                if (account == null)
                    return new OperationResult(-3, "账户不存在");
                var sysNo = Sequence.Get();
                if (!ServiceDepository.DeductScheduleDataAccess.Insert(new DeductScheduleInfo
                {
                    SysNo = sysNo,
                    AccountSysNo = account.SysNo,
                    ApplyAmount = amount,
                    DeductType = deductType,
                    Remark = remark,
                    LimitSource = limitSource == null ? null : limitSource.ToList(),
                    CreateUserSysNo = operatorSysNo
                }))
                {
                    return new OperationResult(-4, "保存失败");
                }
                var result = new OperationResult(0);
                result.OtherData.Add("SysNo", sysNo);
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }

        public OperationResult DeleteDeductSchedule(long sysNo, long operatorSysNo, long verson)
        {
            try
            {
                var entity = ServiceDepository.DeductScheduleDataAccess.Get(sysNo);
                if (entity == null)
                    return new OperationResult(-1, "参数错误");
                if (entity.DeductedAmount > 0)
                    return new OperationResult(-2, "已扣款，不能删除");
                if (!ServiceDepository.DeductScheduleDataAccess.Delete(sysNo, operatorSysNo, verson))
                {
                    return new OperationResult(-3, "删除失败");
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
            return new OperationResult(0);
        }

        private OperationResult ReplenishTransferOrder(long sysNo, AccountType accountType, string accountSubjectId, OrderType orderType, OrderBizType bizType, long operatorSysNo)
        {
            #region ###################参数校验###################
            if (string.IsNullOrWhiteSpace(accountSubjectId))
                return new OperationResult(-1, "非法参数信息");
            var detail = ServiceDepository.MissCollectionTradeDataAccess.Get(sysNo);
            if (detail == null)
                return new OperationResult(-2, "掉单记录不存在");
            if (detail.Confirmed)
                return new OperationResult(-2, "重复操作");
            var accountInfo = ServiceDepository.AccountDataAccess.Get(accountSubjectId, accountType, bizType);
            if (accountInfo == null)
                return new OperationResult(-3, "账户不存在");
            #endregion

            try
            {
                ServiceDepository.TransactionService.Begin();

                #region ###################创建订单，补交易流水###################
                OperationResult orderResult = null;
                switch (orderType)
                {
                    case OrderType.Collection: orderResult = SubmitCollection(accountType, accountSubjectId, detail.Amount, bizType, detail.Channel, null, operatorSysNo); break;
                    case OrderType.Expend: orderResult = SubmitExpendOrder(accountType, accountSubjectId, detail.Amount, bizType, detail.Channel, null, operatorSysNo); break;
                    default: break;
                }
                if (orderResult == null)
                {
                    ServiceDepository.TransactionService.RollBack();
                    throw new NotImplementedException(string.Format("not implemented transfer order for order type {0}", orderType.ToString()));
                }
                if (!orderResult.Success)
                {
                    ServiceDepository.TransactionService.RollBack();
                    return orderResult;
                }
                string orderNo = orderResult.OtherData["OrderNo"].ToString();
                #endregion

                #region ###################逻辑处理###################
                TradeInfo trade = new TradeInfo
                {
                    SysNo = Sequence.Get(),
                    Amount = detail.Amount,
                    PaymentChannel = PaymentChannel.Transfer,
                    OrderNo = orderNo,
                    SerialNo = detail.SerialNo,
                    AccountName = detail.PayerName,
                    BankAccountNo = detail.PaymentAccount,
                    BankCode = detail.BankCode,
                    SubBankName = detail.BankName,
                    CreateUserSysNo = operatorSysNo,
                    CreateDate = DateTime.Now,
                    Status = TradeStatus.Submitted,
                    PaymentDate = detail.PaymentDate
                };
                //修改预入账金额
                accountInfo.PreEntryMoney += trade.Amount;
                accountInfo.ModifyUserSysNo = operatorSysNo;
                accountInfo.ModifyDate = DateTime.Now;
                #endregion

                if (!ServiceDepository.MissCollectionTradeDataAccess.Confirm(detail.SysNo, trade.TradeNo, operatorSysNo))
                {
                    Log.Error(string.Format("确认收款掉单数据失败,{0},{1}", detail.SysNo, trade.TradeNo));
                    return new OperationResult(-7, "确认收款掉单数据失败");
                }

                if (!ServiceDepository.TradeDataAccess.Insert(trade))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-8, "交易明细创建失败");
                }

                if (!ServiceDepository.AccountDataAccess.Update(accountInfo))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-9, "账户操作失败");
                }

                #region 最后提交对账请求到财务系统
                var order = ServiceDepository.OrderDataAccess.Get(orderNo);
                if (!SubmitCollectionRequest(accountInfo, order, trade, string.Empty).Success)
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-10, "收款对账请求提交失败");
                }
                #endregion

                ServiceDepository.TransactionService.Commit();

                var result = new OperationResult(0);
                result.OtherData.Add("OrderNo", orderNo);
                result.OtherData.Add("TradeNo", trade.TradeNo);
                return result;
            }
            catch (Exception e)
            {
                ServiceDepository.TransactionService.RollBack();
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }

        private OperationResult SubmitCollectionRequest(AccountInfo account, OrderInfo order, TradeInfo trade, string terminalNo)
        {
            var caseInfo = ServiceDepository.CaseDataAccess.GetCaseByCaseId(account.SubjectId);
            if (caseInfo == null)
                return new OperationResult(-1, "案件不存在");
            string serialNo = trade.SerialNo;
            //财务系统要求POS对账，流水号长度固定为6
            if (trade.PaymentChannel == PaymentChannel.POS)
            {
                if (string.IsNullOrWhiteSpace(trade.SerialNo) || trade.SerialNo.Length < 6)
                    return new OperationResult(-2, "流水号错误");
                serialNo = trade.SerialNo.Substring(trade.SerialNo.Length - 6);
            }
            return ServiceDepository.ExternalService.SubmitCollection(trade.TradeNo, serialNo, account.SubjectId,
                trade.OrderNo, order.OrderAmount, (int)order.PaymentChannel + 1, order.OrderBizType.GetDescription(), trade.AccountName, trade.BankAccountNo, trade.Amount,
                string.IsNullOrWhiteSpace(trade.BankCode) ? trade.SubBankName : trade.BankCode,
                trade.PaymentDate.Value, terminalNo, caseInfo.ReceptionCenter.GetDescription(), caseInfo.TenementAddress);
        }

        private OperationResult SubmitPayRequest(string caseId, OrderBizType tradeType, TradeInfo trade)
        {
            var caseInfo = ServiceDepository.CaseDataAccess.GetCaseByCaseId(caseId);
            if (caseInfo == null)
                return new OperationResult(-1, "案件不存在");

            return ServiceDepository.ExternalService.SubmitPayment(caseId, trade.TradeNo, caseInfo.TenementAddress,
                Math.Abs(trade.Amount), trade.BankCode, trade.BankAccountNo, trade.AccountName, tradeType.GetDescription());
        }
    }
}
