﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Activation;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.ServiceImpl
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class CustomerFundFlowService : ICustomerFundFlowService
    {
        public OperationResult AddFundFlow(CustomerFundFlow flow)
        {
            try
            {
                if (flow == null || string.IsNullOrEmpty(flow.CaseId))
                    return new OperationResult(-1);
                var caseInfo = ServiceDepository.CaseDataAccess.GetCaseByCaseId(flow.CaseId);
                if (caseInfo == null)
                    return new OperationResult(-2, "案件不存在");

                var entity = ObjectMapper.Copy<CustomerFundFlowInfo>(flow);
                entity.SysNo = Sequence.Get();
                if (ServiceDepository.CustomerFundFlowDataAccess.Insert(entity))
                    return new OperationResult(0);
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
            return new OperationResult(-4, "保存失败");
        }

        public IList<CustomerFundFlow> GetFundFlows(string caseId, CustomerType? customerType = null)
        {
            if (string.IsNullOrEmpty(caseId))
                return null;
            var entities = ServiceDepository.CustomerFundFlowDataAccess.Get(caseId, customerType);
            if (entities == null)
                return null;
            return entities.Select(e => ObjectMapper.Copy<CustomerFundFlow>(e)).ToList();
        }

        public OperationResult DeleteFundFlow(long sysNo, long operatorSysNo)
        {
            if (ServiceDepository.CustomerFundFlowDataAccess.Delete(sysNo, operatorSysNo))
                return new OperationResult(0);
            return new OperationResult(-5, "操作失败");
        }
    }
}
