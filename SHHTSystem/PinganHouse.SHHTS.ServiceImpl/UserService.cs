﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Activation;

using HTB.DevFx;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.ServiceImpl
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class UserService : IUserService
    {
        public T GetUser<T>(long sysNo) where T : User, new()
        {
            T result = null;
            try
            {
                Type type = typeof(T);
                UserInfo userInfo = null;
                if (type == typeof(User))
                {
                    userInfo = ServiceDepository.UserDataAccess.Get(sysNo);
                }
                else if (type == typeof(Customer))
                {
                    userInfo = ServiceDepository.CustomerDataAccess.Get(sysNo);
                }
                else if (type == typeof(AgentStaff))
                {
                    userInfo = ServiceDepository.AgentStaffDataAccess.Get(sysNo);
                }
                else if (type == typeof(PinganStaff))
                {
                    userInfo = ServiceDepository.PinganStaffDataAccess.Get(sysNo);
                }
                else if (type == typeof(ThirdpartyUser))
                {
                    userInfo = ServiceDepository.ThirdpartyUserDataAccess.Get(sysNo);
                }
                if (userInfo != null)
                    result = ObjectMapper.Copy<T>(userInfo);
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
            return result;
        }

        //public T GetUser<T>(string userId) where T : User, new()
        //{
        //    T result = null;
        //    try
        //    {
        //        Type type = typeof(T);
        //        UserInfo userInfo = null;
        //        if (type == typeof(User))
        //        {
        //            userInfo = ServiceDepository.UserDataAccess.Get(userId);
        //        }
        //        else if (type == typeof(Customer))
        //        {
        //            userInfo = ServiceDepository.CustomerDataAccess.Get(userId);
        //        }
        //        else if (type == typeof(AgentStaff))
        //        {
        //            userInfo = ServiceDepository.AgentStaffDataAccess.Get(userId);
        //        }
        //        else if (type == typeof(PinganStaff))
        //        {
        //            userInfo = ServiceDepository.PinganStaffDataAccess.Get(userId);
        //        }
        //        else if (type == typeof(ThirdpartyUser))
        //        {
        //            userInfo = ServiceDepository.ThirdpartyUserDataAccess.Get(userId);
        //        }
        //        if (userInfo != null)
        //            result = ObjectMapper.Copy<T>(userInfo);
        //    }
        //    catch (Exception e)
        //    {
        //        Log.Error(e);
        //    }
        //    return result;
        //}

        public IList<User> GetUsers(int pageIndex, int pageSize, out int totalCount, UserType? userType = null, string condition = null, ConditionMatchingType matchingType = ConditionMatchingType.Fuzzy)
        {
            var results = ServiceDepository.UserDataAccess.GetPaginatedList(condition, userType, pageIndex, pageSize, out totalCount, matchingType: matchingType);
            if (results == null)
                return null;
            return results.Select(s => ObjectMapper.Copy<User>(s)).ToList();
        }

        public string GetUserName(long userSysNo)
        {
            return ServiceDepository.UserDataAccess.GetUserName(userSysNo);
        }


        public OperationResult UpdateSendSmsFlag(long userSysNo, bool isSendSms, long? createUserSysNo)
        {
            if (userSysNo <= 0)
                return new OperationResult(1, "用户系统编号异常");
            var user = ServiceDepository.UserDataAccess.Get(userSysNo);
            if (user == null)
                return new OperationResult(2, "系统不存在该用户");
            var userExend = ServiceDepository.UserExendDataAccess.Get(userSysNo);
            if (userExend == null)
            {
                var entity = new UserExend { SysNo = userSysNo, IsSendSms = isSendSms, CreateDate = DateTime.Now, CreateUserSysNo = createUserSysNo };
                ServiceDepository.UserExendDataAccess.Insert(entity);
            }
            else if (userExend.IsSendSms != isSendSms)
            {
                userExend.IsSendSms = isSendSms;
                userExend.ModifyDate = DateTime.Now;
                userExend.ModifyUserSysNo = createUserSysNo;
                ServiceDepository.UserExendDataAccess.Update(userExend);
            }
            return new OperationResult(0);
        }


        #region 客户(上下家)
        public OperationResult CreateCustomer(Customer customer)
        {
            if (customer == null)
                return new OperationResult(-1);
            //if (customer.UserType != UserType.Customer)
            //    return new OperationResult(-2);
            //if (!CheckLoginId(customer.LoginId))
            //    return new OperationResult(-3, "登录名已存在");
            try
            {
                var entity = ObjectMapper.Copy<CustomerInfo>(customer);
                //entity.SysNo = Sequence.Get();
                entity.UserId = GenerateUserId(UserType.Customer);
                //entity.CreateDate = entity.CreateDate == DateTime.MinValue ? DateTime.Now : entity.CreateDate;

                if (ServiceDepository.CustomerDataAccess.Insert(entity, null, null))
                {
                    var result = new OperationResult(0);
                    result.OtherData.Add("UserSysNo", entity.SysNo);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return new OperationResult(-4, "保存失败");
        }

        public OperationResult CreateCustomer(Customer customer, UserIdentity identity)
        {
            if (customer == null || identity == null || string.IsNullOrWhiteSpace(identity.Id))
                return new OperationResult(-1);
            try
            {
                var entity = ObjectMapper.Copy<CustomerInfo>(customer);
                //entity.SysNo = Sequence.Get();
                entity.UserId = GenerateUserId(UserType.Customer);
                if (ServiceDepository.CustomerDataAccess.Create(entity, ObjectMapper.Copy<UserIdentityInfo>(identity)))
                {
                    var result = new OperationResult(0);
                    result.OtherData.Add("UserSysNo", entity.SysNo);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return new OperationResult(-4, "保存失败");
        }

        public Customer GetCustomer(long sysNo)
        {
            return this.GetUser<Customer>(sysNo);
        }

        //public Customer GetCustomer(string userId)
        //{
        //    return this.GetUser<Customer>(userId);
        //}

        public IList<Customer> GetCustomerByCase(string caseId)
        {
            var entities = ServiceDepository.CustomerDataAccess.GetCustomerByCase(caseId);
            if (entities == null)
                return null;
            return entities.Select(p => ObjectMapper.Copy<Customer>(p)).ToList();
        }

        public IList<Customer> GetCustomers(int pageIndex, int pageSize, out int totalCount, CustomerType? customerType = null, string condition = null, ConditionMatchingType matchingType = ConditionMatchingType.Equal)
        {
            var results = ServiceDepository.CustomerDataAccess.GetPaginatedList(condition, customerType, pageIndex, pageSize, out totalCount, matchingType: matchingType);
            if (results == null)
                return null;
            return results.Select(s => ObjectMapper.Copy<Customer>(s)).ToList();
        }

        public OperationResult ModifyCustomer(Customer customer)
        {
            try
            {
                var entity = ServiceDepository.CustomerDataAccess.Get(customer.SysNo);
                if (entity == null)
                    return new OperationResult(-1, "找不到指定客户信息");
                entity.RealName = customer.RealName;
                entity.Mobile = customer.Mobile;
                entity.TelPhone = customer.TelPhone;
                entity.Gender = customer.Gender;
                entity.CertificateType = customer.CertificateType;
                entity.IdentityNo = customer.IdentityNo;
                entity.ModifyUserSysNo = customer.ModifyUserSysNo;

                entity.CustomerType = customer.CustomerType;

                if (ServiceDepository.CustomerDataAccess.Update(entity))
                    return new OperationResult(0);
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
            return new OperationResult(-4, "操作失败");
        }

        public IDictionary<long, string> GetUserIdentityNoByCustomerSysNos(IEnumerable<long> userSysNos)
        {
            var users = ServiceDepository.CustomerDataAccess.GetBySysNos(userSysNos);
            if (users == null)
                return null;
            IDictionary<long, string> result = new Dictionary<long, string>();

            foreach (var item in users)
            {
                if (!result.ContainsKey(item.SysNo))
                    result.Add(item.SysNo, item.IdentityNo);
            }
            return result;
        }

        public Dictionary<long, IList<CustomerDetail>> GetCustomerDetails(string mobile)
        {
            if (!Validation.Mobile(mobile))
                return null;
            try
            {
                var customers = ServiceDepository.CustomerDataAccess.GetByMobile(mobile);
                return this.SetCustomerDetailDic(customers);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }

        public Dictionary<long, IList<CustomerDetail>> GetCustomerDetailsByIdentityNo(string identityNo)
        {
            if (string.IsNullOrWhiteSpace(identityNo))
                return null;
            try
            {
                var customers = ServiceDepository.CustomerDataAccess.GetByIdentityNo(identityNo);
                return this.SetCustomerDetailDic(customers);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }

        private Dictionary<long, IList<CustomerDetail>> SetCustomerDetailDic(IList<CustomerInfo> customers)
        {
            if (customers == null || customers.Count == 0)
                return null;
            var dic = new Dictionary<long, IList<CustomerDetail>>();
            foreach (var customer in customers)
            {
                var caseInfo = ServiceDepository.CaseDataAccess.GetCaseByUserSysNo(customer.SysNo);
                if (caseInfo == null)
                    continue;
                var customerDetail = ObjectMapper.Copy<CustomerDetail>(customer);
                customerDetail.CaseId = caseInfo.CaseId;
                customerDetail.TenementAddress = caseInfo.TenementAddress;
                if (dic.ContainsKey(customer.UserSysNo))
                {
                    customerDetail.IsSendSms = dic[customer.UserSysNo][0].IsSendSms;
                    customerDetail.IsConcern = dic[customer.UserSysNo][0].IsConcern;
                    dic[customer.UserSysNo].Add(customerDetail);
                }
                else
                {
                    var ds = new List<CustomerDetail>();
                    var userExend = ServiceDepository.UserExendDataAccess.Get(customer.UserSysNo);
                    if (userExend == null || userExend.IsSendSms)
                        customerDetail.IsSendSms = true;
                    var partUser = ServiceDepository.ParterUserInfoDataAccess.GetByUserSysNo(customer.UserSysNo);
                    if (partUser != null)
                        customerDetail.IsConcern = true;
                    ds.Add(customerDetail);
                    dic[customer.UserSysNo] = ds;
                }
            }
            return dic;
        }

        #endregion

        #region 中介公司员工

        public OperationResult CreateAgentStaff(AgentStaff agentStaff, string loginName, string password)
        {
            if (agentStaff == null)
                return new OperationResult(-1);
            if (!string.IsNullOrWhiteSpace(loginName) && string.IsNullOrWhiteSpace(password))
                return new OperationResult(-2, "密码不能为空");
            var agentCompany = ServiceDepository.AgentCompanyDataAccess.Get(agentStaff.AgentCompanySysNo);
            if (agentCompany == null)
                return new OperationResult(-3, "中介公司不存在");
            if (agentCompany.Status != AgentJointStatus.Jointing)
                return new OperationResult(-4, "中介公司状态错误");
            if (!CheckLoginId(loginName))
                return new OperationResult(-6, "登录名已存在");
            if (ServiceDepository.AgentStaffDataAccess.ExistsByIdentityNo(agentStaff.CertificateType, agentStaff.IdentityNo))
                return new OperationResult(-7, "经纪人已存在");
            try
            {
                var entity = ObjectMapper.Copy<AgentStaffInfo>(agentStaff);
                entity.UserId = GenerateUserId(UserType.AgentStaff);

                if (ServiceDepository.AgentStaffDataAccess.Insert(entity, loginName, EncryptPassword(password)))
                {
                    var departments = new List<string> { ((int)AgentType.Agency).ToString(), };
                    if (entity.IsManager)
                        departments.Add(agentStaff.AgentCompanySysNo.ToString());
                    //创建微信企业中介成员
                    ObjectService.GetObject<IParterService>().CreateUser(entity.UserId, entity.RealName, entity.Mobile, entity.Email, null, null, departments, entity.IdentityNo);
                    var result = new OperationResult(0);
                    result.OtherData.Add("UserSysNo", entity.SysNo);
                    return result;
                }
                return new OperationResult(-8, "保存失败");
                //var result = new OperationResult(0);
                //result.OtherData.Add("UserSysNo", entity.UserSysNo);
                //return result;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-5, "系统异常");
            }
        }

        public AgentStaff GetAgentSatff(long sysNo)
        {
            return this.GetUser<AgentStaff>(sysNo);
        }
        public AgentStaff GetAgentSatff(string userId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(userId))
                    return null;
                var agents = ServiceDepository.AgentStaffDataAccess.Get(userId);
                if (agents == null|| agents.Count==0)
                    return null;
                if (agents.Count > 1)
                    throw new ArgumentException("经纪人数据异常");
                return ObjectMapper.Copy<AgentStaff>(agents[0]);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }

        public OperationResult DeleteAgentStaff(long sysNo, string operatorUserId)
        {
            if (string.IsNullOrWhiteSpace(operatorUserId))
                return new OperationResult(-1, "参数错误");
            var managers = ServiceDepository.AgentStaffDataAccess.Get(operatorUserId);
            if (managers == null || managers.Count == 0)
                return new OperationResult(-2, "操作人信息不存在");
            var staff = ServiceDepository.AgentStaffDataAccess.Get(sysNo);
            if (staff == null)
                return new OperationResult(-3, "经纪人信息不存在");
            if (staff.IsManager)
                return new OperationResult(-6, "无此权限删除管理者");
            var manager = managers.Where(m => m.AgentCompanySysNo == staff.AgentCompanySysNo).FirstOrDefault();
            if (manager == null || !manager.IsManager)
                return new OperationResult(-4, "操作人无此权限");
            if (!ServiceDepository.AgentStaffDataAccess.Delete(sysNo, manager.UserSysNo))
                return new OperationResult(-5, "操作失败");
            //删除微信用户
            ServiceDepository.ParterChannelService.DeleteUser(staff.UserId);
            return new OperationResult(0);
        }

        //public AgentStaff GetAgentSatff(string userId)
        //{
        //    return this.GetUser<AgentStaff>(userId);
        //}

        public IList<AgentStaff> GetAgentStaffs(int pageIndex, int pageSize, out int totalCount, long? agentCompanySysNo, bool? isManager, string condition = null, string identityNo = null, string name = null, string mobile = null, ConditionMatchingType matchingType = ConditionMatchingType.Fuzzy)
        {
            var results = ServiceDepository.AgentStaffDataAccess.GetPaginatedList(condition, agentCompanySysNo, isManager, pageIndex, pageSize, out totalCount, identityNo, name, mobile, matchingType: matchingType);
            if (results == null)
                return null;
            return results.Select(s => ObjectMapper.Copy<AgentStaff>(s)).ToList();
        }

        public IList<AgentStaffQueryInfo> GetAgentStaffsWithCompany(int pageIndex, int pageSize, out int totalCount, long? agentCompanySysNo, bool? isManager, string condition = null, string identityNo = null, string name = null, string mobile = null, ConditionMatchingType matchingType = ConditionMatchingType.Fuzzy)
        {
            var results = ServiceDepository.AgentStaffDataAccess.GetPaginatedListWithCompany(condition, agentCompanySysNo, isManager, pageIndex, pageSize, out totalCount, identityNo, name, mobile, matchingType: matchingType);
            if (results == null)
                return null;
            return results.Select(s => ObjectMapper.Copy<AgentStaffQueryInfo>(s)).ToList();
        }

        public OperationResult ModifyAgentStaff(AgentStaff agentStaff)
        {
            try
            {
                var entity = ServiceDepository.AgentStaffDataAccess.Get(agentStaff.SysNo);
                if (entity == null)
                    return new OperationResult(-1, "找不到指定客户信息");
                entity.RealName = agentStaff.RealName;
                entity.Mobile = agentStaff.Mobile;
                entity.TelPhone = agentStaff.TelPhone;
                entity.Gender = agentStaff.Gender;
                entity.CertificateType = agentStaff.CertificateType;
                entity.IdentityNo = agentStaff.IdentityNo;
                entity.ModifyUserSysNo = agentStaff.ModifyUserSysNo;

                entity.AgentCompanySysNo = agentStaff.AgentCompanySysNo;
                entity.IsManager = agentStaff.IsManager;

                if (ServiceDepository.AgentStaffDataAccess.Update(entity))
                    return new OperationResult(0);
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
            return new OperationResult(-4, "操作失败");
        }
        #endregion

        #region 平安员工

        public OperationResult CreatePinganStaff(PinganStaff pinganStaff, string password = null)
        {
            if (pinganStaff == null)
                return new OperationResult(-1);
            if (string.IsNullOrEmpty(pinganStaff.UMCode))
                return new OperationResult(-2);
            if (!CheckLoginId(pinganStaff.UMCode))
                return new OperationResult(-3, "登录名已存在");
            try
            {
                var entity = ObjectMapper.Copy<PinganStaffInfo>(pinganStaff);
                entity.UserId = GenerateUserId(UserType.PinganStaff);

                if (!ServiceDepository.PinganStaffDataAccess.Insert(entity, EncryptPassword(password)))
                {
                    return new OperationResult(-4, "保存失败");
                }
                var result = new OperationResult(0);
                result.OtherData.Add("UserSysNo", entity.UserSysNo);
                return result;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-5, "系统异常");
            }

        }

        public PinganStaff GetPinganStaff(long sysNo)
        {
            return this.GetUser<PinganStaff>(sysNo);
        }

        //public PinganStaff GetPinganStaff(string userId)
        //{
        //    return this.GetUser<PinganStaff>(userId);
        //}

        public IList<PinganStaff> GetPinganStaffs(int pageIndex, int pageSize, out int totalCount, string condition = null, ConditionMatchingType matchingType = ConditionMatchingType.Fuzzy)
        {
            var results = ServiceDepository.PinganStaffDataAccess.GetPaginatedList(condition, pageIndex, pageSize, out totalCount, matchingType: matchingType);
            if (results == null)
                return null;
            return results.Select(s => ObjectMapper.Copy<PinganStaff>(s)).ToList();
        }

        public OperationResult ModifyPinganStaff(PinganStaff pinganStaff)
        {
            try
            {
                var entity = ServiceDepository.PinganStaffDataAccess.Get(pinganStaff.SysNo);
                if (entity == null)
                    return new OperationResult(-1, "找不到指定客户信息");
                entity.RealName = pinganStaff.RealName;
                entity.Mobile = pinganStaff.Mobile;
                entity.TelPhone = pinganStaff.TelPhone;
                entity.Gender = pinganStaff.Gender;
                entity.CertificateType = pinganStaff.CertificateType;
                entity.IdentityNo = pinganStaff.IdentityNo;
                entity.ModifyUserSysNo = pinganStaff.ModifyUserSysNo;

                entity.UMCode = pinganStaff.UMCode;
                entity.ReceptionCenter = pinganStaff.ReceptionCenter;
                entity.StaffCardNo = pinganStaff.StaffCardNo;
                entity.StaffNo = pinganStaff.StaffNo;

                if (ServiceDepository.PinganStaffDataAccess.Update(entity))
                    return new OperationResult(0);
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
            return new OperationResult(-4, "操作失败");
        }

        public PinganStaff GetByPinganStaffUMCode(string um)
        {
            var entity = ServiceDepository.PinganStaffDataAccess.GetByUMCode(um);
            if (entity == null)
                return null;
            return ObjectMapper.Copy<PinganStaff>(entity);
        }

        public PinganStaff GetByStaffCardNo(string staffCardNo)
        {
            if (string.IsNullOrWhiteSpace(staffCardNo))
                return null;
            var entity = ServiceDepository.PinganStaffDataAccess.GetByStaffCardNo(staffCardNo);
            if (entity == null)
                return null;
            return ObjectMapper.Copy<PinganStaff>(entity);
        }
        #endregion

        #region 第三方公司员工
        public OperationResult CreateThirdpartyUser(ThirdpartyUser tUser, string loginName, string passWord)
        {
            if (tUser == null)
                return new OperationResult(-1);
            if (!string.IsNullOrWhiteSpace(loginName) && string.IsNullOrWhiteSpace(passWord))
                return new OperationResult(-2, "密码不能为空");
            if (!CheckLoginId(loginName))
                return new OperationResult(-3, "登录名已存在");
            try
            {
                var entity = ObjectMapper.Copy<ThirdpartyUserInfo>(tUser);
                entity.UserId = GenerateUserId(UserType.ThirdpartyUser);

                if (!ServiceDepository.ThirdpartyUserDataAccess.Insert(entity, loginName, EncryptPassword(passWord)))
                {
                    return new OperationResult(-4, "保存失败");
                }

                var result = new OperationResult(0);
                result.OtherData.Add("UserSysNo", entity.UserSysNo);
                ServiceDepository.TransactionService.Commit();
                return result;
            }
            catch (Exception ex)
            {
                ServiceDepository.TransactionService.RollBack();
                Log.Error(ex);
                return new OperationResult(-5, "保存失败");
            }
        }

        public ThirdpartyUser GetThirdpartyUser(long sysNo)
        {
            return this.GetUser<ThirdpartyUser>(sysNo);
        }

        //public ThirdpartyUser GetThirdpartyUser(string userId)
        //{
        //    return this.GetUser<ThirdpartyUser>(userId);
        //}

        public IList<ThirdpartyUser> GetThirdpartyUsers(int pageIndex, int pageSize, out int totalCount, string condition = null, string tpName = null, ConditionMatchingType matchingType = ConditionMatchingType.Fuzzy)
        {
            var results = ServiceDepository.ThirdpartyUserDataAccess.GetPaginatedList(condition, tpName, pageIndex, pageSize, out totalCount, matchingType: matchingType);
            if (results == null)
                return null;
            return results.Select(s => ObjectMapper.Copy<ThirdpartyUser>(s)).ToList();
        }

        public OperationResult ModifyThirdpartyUser(ThirdpartyUser tpUser)
        {
            try
            {
                var entity = ServiceDepository.ThirdpartyUserDataAccess.Get(tpUser.SysNo);
                if (entity == null)
                    return new OperationResult(-1, "找不到指定客户信息");
                entity.RealName = tpUser.RealName;
                entity.Mobile = tpUser.Mobile;
                entity.TelPhone = tpUser.TelPhone;
                entity.Gender = tpUser.Gender;
                entity.CertificateType = tpUser.CertificateType;
                entity.IdentityNo = tpUser.IdentityNo;
                entity.ModifyUserSysNo = tpUser.ModifyUserSysNo;

                entity.TpName = tpUser.TpName;

                if (ServiceDepository.ThirdpartyUserDataAccess.Update(entity))
                    return new OperationResult(0);
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
            return new OperationResult(-4, "操作失败");
        }
        #endregion

        #region 登录相关
        public OperationResult Login(string loginId, string password)
        {
            if (string.IsNullOrEmpty(loginId) || string.IsNullOrEmpty(password))
                return new OperationResult(-1);
            try
            {
                var loginInfo = ServiceDepository.UserAccountDataAccess.GetByLoginId(loginId);
                if (loginInfo == null)
                    return new OperationResult(-2, "登录名不存在");
                var result = UserAuthProviderFactory.GetProvider(loginInfo.UserType).Authenticate(loginId, password);
                if (result.Success)
                {
                    var userInfo = ServiceDepository.UserDataAccess.Get(loginInfo.SysNo);
                    result.OtherData.Add("UserInfo", ObjectMapper.Copy<User>(userInfo));
                }
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
            return new OperationResult(-5, "系统错误");
        }

        public OperationResult GetStaffByFingerprint(string fpCode)
        {
            if (string.IsNullOrWhiteSpace(fpCode))
                return new OperationResult(-1, "指纹信息不能为空");
            long userSysNo = 0;
            try
            {
                if (!ServiceDepository.FingerService.VerifyFinger(fpCode, out userSysNo))
                    return new OperationResult(-2, "指纹校验失败");
                var account = ServiceDepository.UserAccountDataAccess.Get(userSysNo);
                if (account == null)
                    return new OperationResult(-3, "用户不存在");
                if (account.IsLocked)
                    return new OperationResult(-4, "该用户已被锁定");
                var result = new OperationResult(0);
                result.OtherData.Add("UserInfo", ObjectMapper.Copy<User>(ServiceDepository.UserDataAccess.Get(userSysNo)));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }

        public OperationResult LoginByFingerprint(string fpCode)
        {
            if (string.IsNullOrWhiteSpace(fpCode))
                return new OperationResult(-1, "指纹信息不能为空");
            long userSysNo = 0;
            try
            {
                if (!ServiceDepository.FingerService.VerifyFinger(fpCode, out userSysNo))
                    return new OperationResult(-2, "指纹校验失败");
                var account = ServiceDepository.UserAccountDataAccess.Get(userSysNo);
                if (account == null)
                    return new OperationResult(-3, "用户不存在");
                if (account.AuthDisabled)
                    return new OperationResult(-4, "该用户禁止登陆");
                ServiceDepository.UserAccountDataAccess.ResetLoginDate(userSysNo, DateTime.Now);
                var result = new OperationResult(0);
                result.OtherData.Add("UserInfo", ObjectMapper.Copy<User>(ServiceDepository.UserDataAccess.Get(userSysNo)));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }

        public OperationResult ChangePassword(long userSysNo, string oldPassword, string newPassword)
        {
            if (string.IsNullOrWhiteSpace(oldPassword) || string.IsNullOrWhiteSpace(newPassword))
                return new OperationResult(-1);
            try
            {
                var user = ServiceDepository.UserAccountDataAccess.Get(userSysNo);
                if (user == null)
                    return new OperationResult(-2, "用户不存在");
                if (string.Compare(user.PassWord, EncryptHelper.GetMD5(oldPassword)) != 0)
                    return new OperationResult(-3, "原始密码错误");

                if (ServiceDepository.UserAccountDataAccess.ChangePassword(userSysNo, EncryptHelper.GetMD5(newPassword)))
                    return new OperationResult(0);
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
            return new OperationResult(-4, "系统错误");
        }
        #endregion

        #region 身份认证信息
        public UserIdentity GetUserIdentity(string identityNo)
        {
            if (string.IsNullOrEmpty(identityNo))
                return null;
            var entity = ServiceDepository.UserIdentityDataAccess.Get(identityNo);
            if (entity == null)
                return null;
            return ObjectMapper.Copy<UserIdentity>(entity);
        }

        public UserIdentity GetUserIdentityByUser(long sysNo, UserType userType)
        {
            User user = null;
            switch (userType)
            {
                case UserType.Customer: user = GetUser<Customer>(sysNo); break;
                case UserType.AgentStaff: user = GetUser<AgentStaff>(sysNo); break;
                case UserType.PinganStaff: user = GetUser<PinganStaff>(sysNo); break;
                case UserType.ThirdpartyUser: user = GetUser<ThirdpartyUser>(sysNo); break;
                default: throw new NotImplementedException();
            }
            if (user == null || user.IdentityNo == null)
                return null;
            var entity = ServiceDepository.UserIdentityDataAccess.Get(user.IdentityNo);
            if (entity == null)
                return null;
            return ObjectMapper.Copy<UserIdentity>(entity);
        }

        public OperationResult BindUserIdentity(long sysNo, UserType userType, UserIdentity identity)
        {
            if (identity == null || string.IsNullOrWhiteSpace(identity.Id))
                return new OperationResult(-1);
            try
            {
                long? userSysNo;
                switch (userType)
                {
                    case UserType.Customer: Customer c = GetUser<Customer>(sysNo); userSysNo = c != null ? c.UserSysNo : new Nullable<long>(); break;
                    case UserType.AgentStaff: AgentStaff a = GetUser<AgentStaff>(sysNo); userSysNo = a != null ? a.UserSysNo : new Nullable<long>(); break;
                    case UserType.PinganStaff: PinganStaff p = GetUser<PinganStaff>(sysNo); userSysNo = p != null ? p.UserSysNo : new Nullable<long>(); break;
                    case UserType.ThirdpartyUser: ThirdpartyUser t = GetUser<ThirdpartyUser>(sysNo); userSysNo = t != null ? t.UserSysNo : new Nullable<long>(); break;
                    default: throw new NotImplementedException();
                }
                if (ServiceDepository.UserDataAccess.BindIdentity(userSysNo.Value, ObjectMapper.Copy<UserIdentityInfo>(identity)))
                    return new OperationResult(0);
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
            return new OperationResult(-4, "保存失败");
        }

        public OperationResult CollectFinger(long userSysNo, string fpCode, string tag, long operatorSysNo)
        {
            return ServiceDepository.FingerService.CollectFinger(userSysNo, fpCode, tag, operatorSysNo);
        }

        public OperationResult DeleteFinger(long fpSysNo, long operatorSysNo)
        {
            return ServiceDepository.FingerService.DeleteFinger(fpSysNo, operatorSysNo);
        }

        public IList<UserFingerprint> GetFingerByUserSysNo(long userSysNo)
        {
            return ServiceDepository.FingerService.GetFingerByUserSysNo(userSysNo);
        }
        #endregion

        private bool CheckLoginId(string loginId)
        {
            if (!string.IsNullOrEmpty(loginId))
            {
                if (ServiceDepository.UserAccountDataAccess.LoginIdExists(loginId))
                    return false;
            }

            return true;
        }

        private string EncryptPassword(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return null;
            return EncryptHelper.GetMD5(input);
        }

        private string GenerateUserId(UserType userType)
        {
            string prefix;
            switch (userType)
            {
                case UserType.Customer: prefix = "C"; break;
                case UserType.PinganStaff: prefix = "P"; break;
                case UserType.AgentStaff: prefix = "A"; break;
                case UserType.ThirdpartyUser: prefix = "T"; break;
                default: prefix = "O"; break;
            }
            return string.Format("{0}{1}{2}", prefix, DateTime.Now.ToString("yyssmmHHMMdd"), RandomHelper.GetNumber(999));
        }
    }
}
