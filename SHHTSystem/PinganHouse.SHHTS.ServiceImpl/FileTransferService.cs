﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.ServiceModel.Activation;
using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Core.IMongoRepository;
using PinganHouse.SHHTS.DataAccess.MongoRepository;

namespace PinganHouse.SHHTS.ServiceImpl
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class FileTransferService : IFileTransferService
    {
        static IGridFSRepository GridFSRepository = new GridFSRepository("Attachments");

        #region Upload by stream
        public UploadResult Upload(Stream stream) 
        {
            string fileId, fileName;
            try
            {
                MemoryStream ms = GetFileInfo(stream, out fileId, out fileName);
                if (ms == null)
                    throw new ArgumentNullException("stream");
                if (fileId == null)
                    fileId = GridFSRepository.Save(ms, fileName).ID;
                else
                {
                    using (Stream gfsStream = GridFSRepository.OpenStream(fileId, FileMode.Append, FileAccess.Write))
                    {
                        if (gfsStream == null)
                            throw new IOException("file.FileData is empty.");
                        gfsStream.Write(ms.ToArray(), (int)ms.Position, (int)(ms.Length - ms.Position));
                        gfsStream.Flush();
                        gfsStream.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("上传文件发生异常,{0}", ex.ToString()));
                return new UploadResult { ResultMessage = ex.Message };
            }
            return new UploadResult { Status = true, FileId = fileId };
        }
        #endregion

        /// <summary>
        /// 上传文件
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public UploadResultMessage Upload(FileUploadMessage file)
        {
            string fileId;
            try
            {
                if (file == null)
                    throw new ArgumentNullException("file");
                fileId = GridFSRepository.Save(file.FileData, file.FileName).ID;
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("上传文件发生异常,{0}", ex.ToString()));
                return new UploadResultMessage { Status = false };
            }
            return new UploadResultMessage { Status = true, FileId = fileId };
        }

        /// <summary>
        /// 断点上传
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public UploadResultMessage UploadAppend(FileUploadMessage file) 
        {
            string fileId;
            try
            {
                if (file == null)
                    throw new ArgumentNullException("file");
                if (string.IsNullOrEmpty(file.FileId))
                    fileId = GridFSRepository.Save(file.FileData, file.FileName).ID;
                else
                {
                    fileId = file.FileId;
                    byte[] bs = new byte[file.PackageSize];

                    //将MessageBodyStream拷贝到MemoryStream,
                    //MessageBodyStream无法调用Seek
                    //会导致MemoryStream.Read()数据异常
                    using (MemoryStream ms = new MemoryStream(bs.Length))
                    {
                        file.FileData.CopyTo(ms);

                        ms.Seek(0, SeekOrigin.Begin);
                        int count = ms.Read(bs, 0, bs.Length);

                        ms.Flush();
                        ms.Close();

                        if (count <= 0)
                            throw new IOException("file.FileData is empty.");
                    }

                    using (Stream gfsStream = GridFSRepository.OpenStream(fileId, FileMode.Append, FileAccess.Write))
                    {
                        if (gfsStream == null)
                            throw new IOException("file.FileData is empty.");
                        gfsStream.Write(bs, 0, bs.Length);
                        gfsStream.Flush();
                        gfsStream.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("上传文件发生异常,{0}", ex.ToString()));
                return new UploadResultMessage { Status = false, ResultMessage = ex.Message };
            }
            return new UploadResultMessage { Status = true, FileId = fileId };
        }

        /// <summary>
        /// 下载文件
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns></returns>
        public FileDownloadMessage Download(FileQueryMessage query)
        {
            try
            {
                FileDownloadMessage result = new FileDownloadMessage { FileId= query.FileId, FileName= query.FileName };

                if (query == null || (string.IsNullOrEmpty(query.FileId) && string.IsNullOrEmpty(query.FileName)))
                    throw new ArgumentNullException("query");

                GridFSFileInfo gfs = null;

                if (!string.IsNullOrEmpty(query.FileId)) 
                    gfs = GridFSRepository.FindByID(query.FileId);
                if (gfs == null && !string.IsNullOrEmpty(query.FileName))
                    gfs = GridFSRepository.FindByName(query.FileName).FirstOrDefault();

                if (gfs == null)
                {
                    result.FileData = new MemoryStream();
                    return result;
                }

                result.FileId = gfs.ID;
                result.FileName = gfs.RemoteFileName;
                result.FileSize = gfs.Length;
                result.FileData = gfs.FileStream;
                result.UploadDate = gfs.UploadDate;

                return result;
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("下载文件发生异常,{0}", ex.ToString()));
                throw;
            }
        }


        public void DeleteById(string fileId)
        {
            if (string.IsNullOrWhiteSpace(fileId))
                return;
            GridFSRepository.DeleteByID(fileId);
        }

        public void DeleteByFileName(string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName))
                return;
            GridFSRepository.DeleteByName(fileName);
        }
        /// <summary>
        /// 从数据流中解析文件信息
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="fileId"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private MemoryStream GetFileInfo(Stream stream, out string fileId, out string fileName)
        {
            fileId = null; fileName = null;
            if (stream == null)
                return null;
            MemoryStream ms = new MemoryStream();
            stream.CopyTo(ms);
            ms.Seek(0, SeekOrigin.Begin);

            //获取文件ID的长度
            byte[] fileIdLenData = new byte[4];
            ms.Read(fileIdLenData, 0, 4);
            int fileIdLen = BitConverter.ToInt32(fileIdLenData, 0);
            if (fileIdLen > 0)
            {
                //获取文件ID内容
                byte[] fileIdData = new byte[fileIdLen];
                ms.Read(fileIdData, 0, fileIdLen);
                fileId = BufferHelper.GetString(fileIdData);
            }
            //获取文件名称的长度
            byte[] fileNameLenData = new byte[4];
            ms.Read(fileNameLenData, 0, 4);
            int fileNameLen = BitConverter.ToInt32(fileNameLenData, 0);
            if (fileNameLen == 0)
                fileName = string.Empty;
            else 
            {
                //获取文件名称内容
                byte[] fileNameData = new byte[fileNameLen];
                ms.Read(fileNameData, 0, fileNameLen);
                fileName = BufferHelper.GetString(fileNameData);
            }
            return ms;
        }
    }
}
