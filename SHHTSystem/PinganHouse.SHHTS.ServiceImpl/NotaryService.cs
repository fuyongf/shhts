﻿using PinganHouse.SHHTS.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.DataTransferObjects.Filters;
using System.ServiceModel.Activation;
using HTB.DevFx.Core;
using PinganHouse.SHHTS.Utils.Log;

namespace PinganHouse.SHHTS.ServiceImpl
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class NotaryService : INotaryService
    {
        public NotaryDto Get(long sysNo)
        {
            var o = ServiceDepository.NotaryDataAccess.Get(sysNo);
            if (o == null)
                return null;

            return ObjectMapper.Copy<NotaryDto>(o);
        }

        public NotaryDto Get(string name, string mobile)
        {
            var obj = ServiceDepository.NotaryDataAccess.Search(new NotaryFilter { Name = name, Mobile = mobile }).FirstOrDefault();

            if (obj == null)
                return null;

            return ObjectMapper.Copy<NotaryDto>(obj);
        }

        public NotaryDto Insert(string name, string mobile, string officeName, long createUserSysNo)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new Exception("公证员姓名不能为空");
            if (string.IsNullOrWhiteSpace(mobile))
                throw new Exception("公证员手机号不能为空");
            if (createUserSysNo <= 0)
                throw new Exception("创建人编号不正确");

            if (Get(name, mobile) != null)
                new Exception("已存在同名公证员");

            Notary o = new Notary
            {
                SysNo = Sequence.Get(),
                CreateDate = DateTime.Now,
                CreateUserSysNo = createUserSysNo,
                Mobile = mobile,
                Name = name,
                OfficeName = officeName
            };

            ServiceDepository.NotaryDataAccess.Insert(o);

            return ObjectMapper.Copy<NotaryDto>(o);
        }

        //public OperationResult Insert(NotaryDto obj)
        //{
        //    if (obj == null)
        //        return new OperationResult(-1, "数据对象不存在");

        //    var existObj = Get(obj.Name, obj.Mobile);

        //    if (existObj != null)
        //        return new OperationResult(-1, "已存在同名公证人");

        //    Notary o = new Notary
        //    {
        //        SysNo = Sequence.Get(),
        //        CreateDate = DateTime.Now,
        //        CreateUserSysNo = obj.CreateUserSysNo,
        //        Mobile = obj.Mobile,
        //        Name = obj.Name,
        //        OfficeName = obj.OfficeName
        //    };

        //    ServiceDepository.NotaryDataAccess.Insert(o);

        //    OperationResult r = new OperationResult(0);
        //    r.OtherData["SysNo"] = o.SysNo;

        //    return r;
        //}

        public IList<NotaryDto> Search(NotaryFilter filter)
        {
            var rs = ServiceDepository.NotaryDataAccess.Search(filter);
            if (rs.Count == 0)
                return new List<NotaryDto>();
            return ObjectMapper.Copy<Notary, NotaryDto>(rs);
        }

        public void Update(long sysNo, string name, string mobile, string officeName, long modifyUserSysNo)
        {
            if (sysNo <= 0)
                throw new Exception("公证员编号不正确");
            if (string.IsNullOrWhiteSpace(name))
                throw new Exception("公证员姓名不能为空");
            if (string.IsNullOrWhiteSpace(mobile))
                throw new Exception("公证员手机号不能为空");
            if (modifyUserSysNo <= 0)
                throw new Exception("修改人编号不正确");

            Notary o = ServiceDepository.NotaryDataAccess.Get(sysNo);

            if (o == null)
                throw new Exception("公证员不存在");

            var existObj = Get(name, mobile);

            if (existObj != null && existObj.SysNo != sysNo)
                new Exception("已存在同名公证员");

            o.Name = name;
            o.Mobile = mobile;
            o.OfficeName = officeName;
            o.ModifyDate = DateTime.Now;
            o.ModifyUserSysNo = modifyUserSysNo;

            ServiceDepository.NotaryDataAccess.Update(o);
        }

        //public void Update(NotaryDto obj)
        //{
        //    if (obj == null)
        //        return;

        //    var o = ServiceDepository.NotaryDataAccess.Get(obj.SysNo);

        //    if (o == null)
        //        return;

        //    o.Name = obj.Name;
        //    o.Mobile = obj.Mobile;
        //    o.OfficeName = obj.OfficeName;
        //    o.ModifyDate = DateTime.Now;
        //    o.ModifyUserSysNo = obj.ModifyUserSysNo;

        //    var existObj = Get(obj.Name, obj.Mobile);

        //    if (existObj != null)
        //        throw new Exception("已存在同名公证人");

        //    ServiceDepository.NotaryDataAccess.Update(o);
        //}
    }
}
