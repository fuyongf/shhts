﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Activation;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.ServiceImpl
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AccountService : IAccountService
    {
        public decimal GetBalance(AccountType accountType, string accountSubjectId)
        {
            if (string.IsNullOrWhiteSpace(accountSubjectId))
                throw new ArgumentNullException("Illegal parameter 'accountSubjectId'");
            return ServiceDepository.AccountDataAccess.GetBalance(accountSubjectId, accountType);
        }

        public Account Get(long sysNo)
        {
            var entity = ServiceDepository.AccountDataAccess.Get(sysNo);
            if (entity == null)
                return null;
            return ObjectMapper.Copy<Account>(entity);
        }

        public Account Get(AccountType type, string subjectId)
        {
            var entity = ServiceDepository.AccountDataAccess.Get(subjectId, type, null);
            if (entity == null)
                return null;
            return ObjectMapper.Copy<Account>(entity);
        }

        public IList<AccountLedger> GetAccounts(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, string caseId, string tenementNo, string tenementAddr, DateTime? startDate, DateTime? endDate)
        {
            var results = ServiceDepository.AccountDataAccess.GetPaginatedList(pageIndex, pageSize, out totalCount, center, caseId, tenementNo, tenementAddr, startDate, endDate);
            if (results == null)
                return null;
            return results.Select(s => ObjectMapper.Copy<AccountLedger>(s)).ToList();
        }

        public Tuple<IDictionary<OrderType, decimal>, Dictionary<OrderType, decimal>> GetCaseTradeAmount(string caseId)
        {
            if (string.IsNullOrWhiteSpace(caseId))
                return null;
            return ServiceDepository.AccountDataAccess.GetCaseTradeAmount(caseId);
        }

        public OperationResult AuthenticateBankCard(string caseId, string bankCode, string cardNo, string name, string identityNo, string mobile, long operatorSysNo)
        {
            if (string.IsNullOrEmpty(caseId) || string.IsNullOrWhiteSpace(bankCode) || string.IsNullOrWhiteSpace(cardNo) || string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(identityNo) || string.IsNullOrWhiteSpace(mobile))
                return new OperationResult(-1, "参数错误");
            if (!ServiceDepository.CaseDataAccess.ExistByCaseId(caseId))
                return new OperationResult(-2, "案件编号错误");
            try
            {
                var result = ServiceDepository.ExternalService.AuthenticateBankCard(name, identityNo, mobile, cardNo, bankCode);
                if (result.Success)
                {
                    var exists = ServiceDepository.BankCardAuthDataAccess.GetByCase(caseId);
                    bool save = false;
                    if (exists == null || exists.Count == 0)
                    {
                        save = ServiceDepository.BankCardAuthDataAccess.Insert(new BankCardAuthInfo
                        {
                            CaseId = caseId,
                            AccountName = name,
                            BankCode = bankCode,
                            CardNo = cardNo,
                            IdentityNo = identityNo,
                            Mobile = mobile,
                            IsAuthed = true,
                            CreateUserSysNo = operatorSysNo
                        });
                    }
                    else
                    {
                        var entity = exists[0];
                        entity.CaseId = caseId;
                        entity.AccountName = name;
                        entity.BankCode = bankCode;
                        entity.CardNo = cardNo;
                        entity.IdentityNo = identityNo;
                        entity.Mobile = mobile;
                        entity.IsAuthed = true;
                        entity.ModifyUserSysNo = operatorSysNo;
                        save = ServiceDepository.BankCardAuthDataAccess.Update(entity);
                    }
                    if (!save)
                        return new OperationResult(-3, "鉴权信息保存失败");
                }
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, "系统错误");
            }
        }

        //public IList<AuthedBankCard> GetAuthedBankCards(int pageIndex, int pageSize, out int totalCount, string caseId, string cardNo, string accountName, string identityNo, string mobile)
        //{
        //    var entities = ServiceDepository.BankCardAuthDataAccess.GetPaginatedList(pageIndex, pageSize, out totalCount, caseId, cardNo, accountName, identityNo, mobile);
        //    if (entities == null)
        //        return null;
        //    return entities.Select(e => ObjectMapper.Copy<AuthedBankCard>(e)).ToList();
        //}

        public AuthedBankCard GetAuthedBankCardByCase(string caseId)
        {
            if (string.IsNullOrWhiteSpace(caseId))
                return null;
            var entities = ServiceDepository.BankCardAuthDataAccess.GetByCase(caseId);
            if (entities == null || entities.Count == 0)
                return null;
            return ObjectMapper.Copy<AuthedBankCard>(entities[0]);
        }

        public OperationResult DeleteAuthedBankCard(long sysNo, long operatorSysNo)
        {
            try
            {
                if (ServiceDepository.BankCardAuthDataAccess.Delete(sysNo, operatorSysNo))
                    return new OperationResult(0);
                return new OperationResult(-1, "操作失败");
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }

        public AccountBill GetAccountBill(AccountType accountType, string subjectId, DateTime? startDate, DateTime? endDate)
        {
            if (string.IsNullOrWhiteSpace(subjectId))
                return null;
            var account = ServiceDepository.AccountDataAccess.Get(subjectId, accountType, null);
            if (account == null)
                return null;
            AccountBill bill = new AccountBill()
            {
                AccountSysNo = account.SysNo,
                StartDate = startDate,
                EndDate = endDate,
                CategoryAmount = new Dictionary<OrderBizType, decimal>(),
                OrderDetails = new List<OrderBilledInfo>()
            };
            try
            {
                var orders = ServiceDepository.OrderDataAccess.GetOrderByAccount(account.SysNo, startDate, endDate);

                if (orders != null)
                {
                    foreach (var order in orders)
                    {
                        if ((order.OrderType == OrderType.Collection && order.AccountSysNo == account.SysNo) ||
                            ((order.OrderType == OrderType.Payment || order.OrderType == OrderType.Transfer) && order.OppositeAccount == account.SysNo))
                            bill.CollectionAmount += order.OrderAmount;
                        else if (order.AccountSysNo == account.SysNo)
                            bill.PaymentAmount += order.OrderAmount;
                        var orderDto = ObjectMapper.Copy<OrderBilledInfo>(order);
                        if (order.OrderType == OrderType.Payment && order.OrderBizType == OrderBizType.AgencyFee)
                        {
                            var payAccount = ServiceDepository.AccountDataAccess.Get(order.AccountSysNo);
                            var caseInfo = ServiceDepository.CaseDataAccess.GetCaseByCaseId(payAccount.SubjectId);
                            if (caseInfo != null)
                            {
                                orderDto.CaseId = caseInfo.CaseId;
                                orderDto.TenementAddress = caseInfo.TenementAddress;
                                var rel = ServiceDepository.CaseDataAccess.GetRelation(caseInfo.CaseId, 2);
                                if (rel != null && rel.Count > 0)
                                {
                                    orderDto.AgentName = ServiceDepository.AgentStaffDataAccess.Get(rel[0].UserSysNo).RealName;
                                }
                            }
                        }
                        bill.OrderDetails.Add(orderDto);
                        if (!bill.CategoryAmount.ContainsKey(order.OrderBizType))
                            bill.CategoryAmount.Add(order.OrderBizType, 0);
                        bill.CategoryAmount[order.OrderBizType] += order.OrderAmount;
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }

            return bill;
        }
    }
}
