﻿using HTB.DevFx.Core;
using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Core.MessageHandler;
using PinganHouse.SHHTS.Core.Wcf;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.DataTransferObjects.Filters;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.ServiceImpl.CaseMessageHandlers.Config;
using PinganHouse.SHHTS.ServiceImpl.Flows;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;

namespace PinganHouse.SHHTS.ServiceImpl
{
    [IocServiceBehavior]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)] //开启ASP.NET兼容模式，否则在IIS宿主下，获取不到HttpContext
    public class CaseService : ServiceBase<CaseSetting>, ICaseService
    {
        public event Action<Case, ICaseService> OnCreated;
        public event Action<CaseEventInfo, Case> OnUpdated;
        /// <summary>
        ///需通过DevFx实例化才能加载OnInit
        ///而该服务是对外发布的，默认wcf构造实例
        ///<%@ ServiceHost Service="PinganHouse.SHHTS.ServiceImpl.CaseService" %>
        ///需增加IocServiceBehavior来使用DevFx构造
        /// </summary>
        protected override void OnInit()
        {
            base.OnInit();
            var handlers = this.Setting.MessageHandlers;
            if (handlers != null && handlers.Length > 0)
            {
                foreach (var handler in handlers)
                {
                    var module = this.ObjectService.GetObject<IMessageHandler>(handler.TypeName);
                    if (module != null)
                    {
                        module.Init(this);
                    }
                }
            }
        }
        protected virtual void Complete(Case caseiIfo)
        {
            var handler = this.OnCreated;
            if (handler != null)
            {
                handler(caseiIfo, this);
            }
        }
        protected virtual void Updated(CaseEventInfo ce, Case caseInfo)
        {
            var handler = this.OnUpdated;
            if (handler != null)
            {
                handler(ce, caseInfo);
            }
        }

        public OperationResult GenerateCase(CaseDto caseDto)
        {
            try
            {
                string caseId = string.Empty;

                if (CheckCase(caseDto))
                {
                    caseId = Generator.GenerateCaseId((uint)caseDto.ReceptionCenter);
                    caseDto.CaseId = caseId;
                    var caseInfo = ObjectMapper.Copy<Case>(caseDto);
                    caseInfo.CaseStatus = CaseStatus.YJ1;
                    var agencyStaff = ServiceDepository.AgentStaffDataAccess.Get(caseInfo.AgencySysNo);
                    if (agencyStaff == null)
                        return new OperationResult(2, "中介所属公司不存在");
                    caseInfo.CompanySysNo = agencyStaff.AgentCompanySysNo;
                    caseInfo.GlobalFlag = caseInfo.SourceType == SourceType.EHouseMoney;
                    var success = ServiceDepository.CaseDataAccess.Insert(caseInfo);
                    if (!success)
                        return new OperationResult(1, "新增失败");

                    var ce = new CaseEventInfo
                    {
                        SysNo = Sequence.Get(),
                        CaseId = caseInfo.CaseId,
                        EventType = CaseStatus.YJ1,
                        SourceStatus = CaseStatus.Unkown,
                        CreateDate = DateTime.Now,
                        CreateUserSysNo = caseDto.OperatorSysNo,
                        OperatorSysNo = caseDto.OperatorSysNo
                    };

                    ServiceDepository.CaseEventDataAccess.Insert(ce);

                    this.Complete(caseInfo);
                }
                return new OperationResult(0, caseId);
            }
            catch (ArgumentException e)
            {
                return new OperationResult(2, e.Message);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-1, "系统内部错误");
            }
        }

        private bool CheckCase(CaseDto caseInfo)
        {
            if (caseInfo == null)
                throw new ArgumentException("case is null");
            if (caseInfo.Buyers == null || caseInfo.Buyers.Count == 0)
                throw new ArgumentException("buyer is null");
            if (caseInfo.Sellers == null || caseInfo.Sellers.Count == 0)
                throw new ArgumentException("seller is null");
            if (caseInfo.AgencySysNo <= 0)
                throw new ArgumentException("agency is null");
            if (string.IsNullOrWhiteSpace(caseInfo.TenementContract))
                throw new ArgumentException("TenementContract is null");
            if (string.IsNullOrWhiteSpace(caseInfo.TenementAddress))
                throw new ArgumentException("TenementAddress is null");
            if (caseInfo.CreateUserSysNo == null)
                throw new ArgumentNullException("CreateUserSysNo");
            if (ServiceDepository.CaseDataAccess.ExistByTenementContract(caseInfo.TenementContract))
                throw new ArgumentException("产权号已存在");
            return true;
        }

        public CaseDto GetCaseByCaseId(string caseId, ReceptionCenter receptionCenter)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(caseId) || receptionCenter == ReceptionCenter.Unkonw)
                    return null;
                var result = ServiceDepository.CaseDataAccess.GetCaseByCaseId(caseId, receptionCenter);
                if (result != null)
                    return SetUser(ObjectMapper.Copy<CaseDto>(result));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return null;
        }

        public CaseWithCustomer GetCaseWithCustomer(string caseId, ReceptionCenter receptionCenter)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(caseId) || receptionCenter == ReceptionCenter.Unkonw)
                    return null;
                var result = ServiceDepository.CaseDataAccess.GetCaseByCaseId(caseId, receptionCenter);
                if (result != null)
                    return SetUser(ObjectMapper.Copy<CaseWithCustomer>(result));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return null;
        }

        public CaseDto GetCaseByCaseId(string caseId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(caseId))
                    return null;
                var result = ServiceDepository.CaseDataAccess.GetCaseByCaseId(caseId);
                if (result != null)
                    return SetUser(ObjectMapper.Copy<CaseDto>(result));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return null;
        }

        public CaseDto GetCaseByCaseIdAndCaseStatus(string caseId, CaseStatus caseStatus)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(caseId) || caseStatus == CaseStatus.Unkown)
                    return null;
                var result = ServiceDepository.CaseDataAccess.GetCaseByCaseIdAndCaseStatus(caseId, caseStatus);
                if (result != null)
                    return SetUser(ObjectMapper.Copy<CaseDto>(result));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return null;
        }

        public CaseDto GetCaseAndProcess(string caseId, CaseStatus caseStatus, out Dictionary<CaseStatus, Tuple<string, DateTime>> process)
        {
            process = new Dictionary<CaseStatus, Tuple<string, DateTime>>();
            try
            {
                if (string.IsNullOrWhiteSpace(caseId) || caseStatus == CaseStatus.Unkown)
                    return null;
                var result = ServiceDepository.CaseDataAccess.GetCaseByCaseId(caseId);
                if (result == null)
                    return null;
                var caseEvents = ServiceDepository.CaseEventDataAccess.GetCaseEvents(result.CaseId, caseStatus);
                if (caseEvents != null && caseEvents.Count() > 0)
                {
                    var dic = new Dictionary<long, string>();
                    foreach (var e in caseEvents)
                    {
                        string name = "";
                        if (e.CreateUserSysNo != null)
                        {
                            if (!dic.ContainsKey(e.CreateUserSysNo.Value))
                            {
                                dic[e.CreateUserSysNo.Value] = "";
                                var user = ServiceDepository.UserDataAccess.Get(e.CreateUserSysNo.Value);
                                if (user != null)
                                    dic[e.CreateUserSysNo.Value] = user.RealName;
                            }
                            name = dic[e.CreateUserSysNo.Value];
                        }
                        process[e.EventType] = new Tuple<string, DateTime>(name, e.CreateDate);
                    }
                }
                return SetUser(ObjectMapper.Copy<CaseDto>(result));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }

        public CaseDto GetCase(long sysno)
        {
            try
            {
                if (sysno <= 0)
                    return null;
                var result = ServiceDepository.CaseDataAccess.Get(sysno);
                if (result != null)
                    return SetUser(ObjectMapper.Copy<CaseDto>(result));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return null;
        }

        public bool ExistTenementContract(string tenementContract)
        {
            return ServiceDepository.CaseDataAccess.ExistByTenementContract(tenementContract);
        }

        [Obsolete]
        public OperationResult UpdateCaseStatus(string caseId, CaseStatus caseStatus, long modifyUserSysNo, Dictionary<string, object> otherData)
        {
            string Path = "/Biz/Case/CaseStatusControl";
            try
            {
                if (string.IsNullOrWhiteSpace(caseId) || caseStatus == CaseStatus.Unkown)
                    return new OperationResult(1, "parameter is invalid");

                var setting = ServiceDepository.SettingService.GetSettingItemByPath(string.Format("{0}/{1}", Path, (long)caseStatus));
                if (setting == null || string.IsNullOrWhiteSpace(setting.Value))
                    return new OperationResult(3, "未配置状态控制流");
                CaseStatus status = (CaseStatus)Enum.Parse(typeof(CaseStatus), setting.Value, true);
                //dk10可自循环
                if ((caseStatus & CaseStatus.DK10) == 0 && (status & caseStatus) != 0)
                    return new OperationResult(7, "前置状态与修改状态相同");
                //获取是否存在前置状态及修改转态案件
                var caseInfo = ServiceDepository.CaseDataAccess.GetCaseByCaseIdAndCaseStatus(caseId, status | caseStatus);
                if (caseInfo == null)
                    return new OperationResult(6, "前置状态不正确");
                if (caseInfo.Disabled)
                    return new OperationResult(5, "案件已取消");
                if ((caseInfo.CaseStatus & status) == 0)
                    return new OperationResult(4, "前置状态不匹配");

                var ce = new CaseEventInfo
                {
                    CaseId = caseInfo.CaseId,
                    EventType = caseStatus,
                    SourceStatus = caseInfo.CaseStatus,
                    OtherDatas = otherData,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };
                if (ServiceDepository.CaseEventDataAccess.Insert(ce, caseInfo.CaseStatus))
                {
                    this.Updated(ce, caseInfo);
                    return new OperationResult(0);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-1, "系统错误");
            }
            return new OperationResult(-5, "操作失败");
        }

        public OperationResult UpdateCaseStatus(string caseId, string action, long modifyUserSysNo, Dictionary<string, object> otherData)
        {
            try
            {
                CaseStatusFlow cf = new CaseStatusFlow();
                cf.Updated = this.Updated;

                cf.UpdateStatus(caseId, action, modifyUserSysNo, otherData);

                return new OperationResult(0);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-1, ex.Message);
            }
        }

        public OperationResult DeleteCaseByCaseId(string caseId, string modifyUserNo)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(caseId))
                    return new OperationResult(1, "caseId is null");
                //逻辑删除案件及相关附件
                ServiceDepository.CaseDataAccess.DeleteCaseByCaseId(caseId, modifyUserNo);
                //物理删除相关图片
                ServiceDepository.FileTransferService.DeleteByFileName(caseId);

                return new OperationResult(0);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-1, "系统内部错误");
            }
        }

        public OperationResult UnBindUser(string caseId, long userSysNo, CaseUserType caseUserType, long modifyUserSysNo)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(caseId) || userSysNo == 0)
                    return new OperationResult(1, "caseId or userSysNo is null");
                var result = ServiceDepository.CaseDataAccess.UnBindUserRelation(caseId, userSysNo, caseUserType, modifyUserSysNo);
                if (result)
                    return new OperationResult(0);
                return new OperationResult(2, "解绑失败");

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-1, "系统错误");
            }
        }

        public OperationResult Update(string caseId, List<long> buyers, List<long> sellers, long agencySysNo, long modifyUserSysNo, string tenementAddress = null, string tenementContract = null, Dictionary<string, object> data = null)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(caseId))
                    return new OperationResult(1, "caseId is invalid");
                if (buyers == null || buyers.Count == 0)
                    return new OperationResult(2, "buyers is invalid");
                if (sellers == null || sellers.Count == 0)
                    return new OperationResult(3, "sellers is invalid");
                var rl = ServiceDepository.CaseDataAccess.GetRelation(caseId);
                if (rl == null || rl.Count == 0)
                    return new OperationResult(4, "案件关联用户异常");
                var caseinfo = ServiceDepository.CaseDataAccess.GetCaseByCaseId(caseId);
                if (caseinfo == null)
                    return new OperationResult(9, "案件不存在");
                if (caseinfo.Disabled)
                    return new OperationResult(10, "案件已取消");
                var caseEvent = ServiceDepository.CaseEventDataAccess.GetNewByCaseId(caseId, CaseStatus.YJ1 | CaseStatus.HA1);
                if (caseEvent == null || caseEvent.Finished)
                    return new OperationResult(8, "案件状态已完成签约");
                if (data == null)
                    data = new Dictionary<string, object> { { "TenementContract", caseinfo.TenementContract }, { "TenementAddress", caseinfo.TenementAddress } };

                if (!string.IsNullOrWhiteSpace(tenementContract) && tenementContract != caseinfo.TenementContract)
                {
                    if (ServiceDepository.CaseDataAccess.ExistByTenementContract(tenementContract))
                        return new OperationResult(7, "产权号已存在");
                    data["TenementContract"] = tenementContract;
                }
                if (!string.IsNullOrWhiteSpace(tenementAddress) && tenementAddress != caseinfo.TenementAddress)
                    data["TenementAddress"] = tenementAddress;

                //去除重复
                buyers = buyers.Distinct().ToList();
                sellers = sellers.Distinct().ToList();
                long? agency = null;
                var unBind = new Dictionary<CaseUserType, List<long>>();
                var users = new Dictionary<CaseUserType, List<long>>();

                //绑定人员
                foreach (var ur in rl)
                {
                    if (users.ContainsKey(ur.CaseUserType))
                    {
                        users[ur.CaseUserType].Add(ur.UserSysNo);
                    }
                    else
                    {
                        users[ur.CaseUserType] = new List<long> { ur.UserSysNo };
                    }

                    if (ur.CaseUserType == CaseUserType.Buyer)
                    {
                        if (buyers.Contains(ur.UserSysNo))
                        {
                            buyers.RemoveAll(u => u == ur.UserSysNo);
                            users[ur.CaseUserType].Remove(ur.UserSysNo);
                        }

                    }
                    else if (ur.CaseUserType == CaseUserType.Seller)
                    {
                        if (sellers.Contains(ur.UserSysNo))
                        {
                            sellers.RemoveAll(u => u == ur.UserSysNo);
                            users[ur.CaseUserType].Remove(ur.UserSysNo);
                        }

                    }
                    else if (ur.CaseUserType == CaseUserType.AgentStaff)
                    {
                        if (ur.UserSysNo != agencySysNo && agencySysNo != 0)
                        {
                            agency = agencySysNo;
                            var agencyStaff = ServiceDepository.AgentStaffDataAccess.Get(agencySysNo);
                            if (agencyStaff == null)
                                return new OperationResult(6, "中介所属公司不存在");
                            data["CompanySysNo"] = agencyStaff.AgentCompanySysNo;
                            unBind[CaseUserType.AgentStaff] = new List<long> { ur.UserSysNo };
                        }
                    }
                }

                //解绑人员
                var unbuyers = users[CaseUserType.Buyer].Except(buyers);
                if (unbuyers.Count() > 0)
                    unBind[CaseUserType.Buyer] = unbuyers.ToList();
                var unsellers = users[CaseUserType.Seller].Except(sellers);
                if (unsellers.Count() > 0)
                    unBind[CaseUserType.Seller] = unsellers.ToList();


                var result = ServiceDepository.CaseDataAccess.Update(caseId, unBind, buyers, sellers, agency, modifyUserSysNo, data);
                if (!result)
                    return new OperationResult(5, "更新失败");
                return new OperationResult(0);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-1, "系统内部错误");
            }
        }

        public OperationResult BindUsers(string caseId, CaseUserType caseUserType, params long[] userSysNos)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(caseId) || userSysNos == null || userSysNos.Length == 0)
                    return new OperationResult(1, "caseId or userSysNos is invalid ");
                var result = ServiceDepository.CaseDataAccess.TransactionInsertCaseUser(caseId, caseUserType, userSysNos);
                if (!result)
                    return new OperationResult(2, "绑定失败");
                return new OperationResult(0);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-1, "系统内部错误");
            }
        }

        [Obsolete]
        public IList<CaseDto> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, string userId, AgentType agentType, CaseStatus allCaseStatus, IEnumerable<CaseStatus> categorys, string caseId = null, string tenementAddress = null,
                 DateTime? startDate = null, DateTime? endDate = null, IDictionary<string, object> data = null, CaseStatus? excludeCaseStatus = null)
        {
            totalCount = 0;
            try
            {
                if (allCaseStatus == CaseStatus.Unkown)
                    return null;
                if (startDate != null && endDate != null && startDate.Value > endDate.Value)
                    return null;
                if (string.IsNullOrWhiteSpace(userId))
                    return null;
                var users = ServiceDepository.AgentStaffDataAccess.Get(userId);
                if (users == null || users.Count == 0)
                    return null;
                var result = ServiceDepository.CaseDataAccess.GetPaginatedList(pageIndex, pageSize, out totalCount, users, agentType, allCaseStatus, caseId, tenementAddress, startDate, endDate, data, excludeCaseStatus);
                if (result == null || result.Count == 0)
                    return null;
                var caseDtos = ObjectMapper.Copy<Case, CaseDto>(result);
                if (categorys != null && categorys.Count() > 0)
                {
                    foreach (var cd in caseDtos)
                    {
                        if (categorys != null && categorys.Count() >= 0)
                        {
                            foreach (var category in categorys)
                            {
                                var ce = ServiceDepository.CaseEventDataAccess.GetNewByCaseId(cd.CaseId, category);
                                if (ce != null)
                                    cd.OtherData[((long)category).ToString()] = ce.EventType.ToString();
                            }
                        }
                    }
                }
                return caseDtos;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }


        /// <summary>
        /// 获取案件列表.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="totalCount">The total count.</param>
        /// <returns></returns>
        public IList<CaseDto> GetPaginatedList(CaseFilter filter, int pageIndex, int pageSize, out int totalCount)
        {
            IList<CaseDto> list = new List<CaseDto>();
            totalCount = 0;

            try
            {
                var caseList = ServiceDepository.CaseDataAccess.GetPaginatedList(filter, pageIndex, pageSize, out totalCount);
                list = ObjectMapper.Copy<Case, CaseDto>(caseList);

                CaseStatusFlow csf = new CaseStatusFlow();
                foreach (var o in list)
                    o.NextActions = csf.GetNextActions(o.CaseId, o.CaseStatus);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return list;
        }

        [Obsolete]
        public IList<CaseDto> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, ReceptionCenter receptionCenter, CaseStatus allCaseStatus, long? belongUserSysNo, CaseStatus? topStatus, CaseStatus? caseStatus,
            string condition, DateTime? startDate, DateTime? endDate, CaseStatus? operationStatus, bool? finished = null, IDictionary<string, object> data = null)
        {
            totalCount = 0;
            try
            {
                if (allCaseStatus == CaseStatus.Unkown)
                    return null;
                if (startDate != null && endDate != null && startDate.Value > endDate.Value)
                    return null;
                var result = ServiceDepository.CaseDataAccess.GetPaginatedList(pageIndex, pageSize, out totalCount, receptionCenter, allCaseStatus, belongUserSysNo, topStatus, condition, startDate, endDate, data, caseStatus, finished);
                if (result == null || result.Count == 0)
                    return null;
                var caseDtos = ObjectMapper.Copy<Case, CaseDto>(result);
                var dic = caseDtos.ToDictionary(c => c.CaseId);
                return FillCaseDto(dic, operationStatus);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }

        public CaseAttachmentDto GetCaseAttenment(string caseId)
        {
            if (string.IsNullOrWhiteSpace(caseId))
                return null;
            try
            {
                var caseInfo = ServiceDepository.CaseDataAccess.GetCaseByCaseId(caseId);
                if (caseInfo == null)
                    return null;
                var result = ServiceDepository.CaseDataAccess.GetRelation(caseId);
                if (result == null || result.Count == 0)
                    return null;
                CaseAttachmentDto caseAttachmentDto = new CaseAttachmentDto();
                var attachments = ServiceDepository.AttachmentDataAccess.GetAttachments(result.Select(r => r.UserSysNo.ToString()));
                if (attachments != null && attachments.Count > 0)
                {
                    var attachmentDtos = ObjectMapper.Copy<AttachmentInfo, AttachmentDto>(attachments);
                    foreach (var r in result)
                    {
                        if (r.CaseUserType == CaseUserType.Buyer)
                        {
                            caseAttachmentDto.BuyerAttachments.Add(Tuple.Create(r.UserSysNo, ServiceDepository.CustomerDataAccess.GetRealName(r.UserSysNo), attachmentDtos.Where(a => a.AssociationNo == r.UserSysNo.ToString())));
                        }
                        else if (r.CaseUserType == CaseUserType.Seller)
                        {
                            caseAttachmentDto.SellerAttachments.Add(Tuple.Create(r.UserSysNo, ServiceDepository.CustomerDataAccess.GetRealName(r.UserSysNo), attachmentDtos.Where(a => a.AssociationNo == r.UserSysNo.ToString())));
                        }
                    }
                }
                var associationNos = new List<string> {
                caseId,
                caseInfo.TenementContract,
                };
                var ca = ServiceDepository.AttachmentDataAccess.GetAttachments(associationNos);
                if (ca != null && ca.Count > 0)
                {

                    caseAttachmentDto.OtherAttachments = ObjectMapper.Copy<AttachmentInfo, AttachmentDto>(ca);
                }

                return caseAttachmentDto;

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }

        public IList<CaseEvent> GetCaseEvents(string caseId, CaseStatus? currentCaseStatus, CaseStatus? preCaseStatus)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(caseId))
                    return null;
                var result = ServiceDepository.CaseEventDataAccess.Get(caseId, preCaseStatus, currentCaseStatus);
                if (result != null && result.Count() > 0)
                    return ObjectMapper.Copy<CaseEventInfo, CaseEvent>(result.ToList());
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return null;
        }

        public IList<CaseEvent> GetCaseEvents(string caseId, int pageIndex, int pageSize, out int totalCount, IEnumerable<CaseStatus> status = null)
        {
            totalCount = 0;
            if (status == null)
                return null;
            var entities = ServiceDepository.CaseEventDataAccess.Get(caseId, pageIndex, pageSize, out totalCount, status);
            if (entities == null)
                return null;
            return entities.Select(e => ObjectMapper.Copy<CaseEvent>(e)).ToList();
        }

        [Obsolete]
        public OperationResult UpdateCaseEvent(long sysno, Dictionary<string, object> dic, long modifyUserSysNo)
        {
            if (sysno == 0 || dic == null)
                return new OperationResult(1, "param is invalid");
            try
            {
                var caseEvent = ServiceDepository.CaseEventDataAccess.Get(sysno);
                if (caseEvent == null)
                    return new OperationResult(2, "案件状态不存在");
                caseEvent.ModifyDate = DateTime.Now;
                caseEvent.ModifyUserSysNo = modifyUserSysNo;
                caseEvent.OtherDatas = dic;
                if (!ServiceDepository.CaseEventDataAccess.Update(caseEvent))
                    return new OperationResult(3, "更新失败");
                SendBookTimeMessage(caseEvent);
                return new OperationResult(0);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-1, "系统内部错误");
            }
        }

        private void SendBookTimeMessage(CaseEventInfo caseEvent)
        {
            //是否更改预约时间
            if ((caseEvent.EventType & (CaseStatus.JY8 | CaseStatus.ZB1)) == 0)
                return;
            caseEvent.OtherDatas["reSend"] = "1";
            var caseInfo = ServiceDepository.CaseDataAccess.GetCaseByCaseId(caseEvent.CaseId);
            this.Updated(caseEvent, caseInfo);
        }

        public OperationResult SetCaseTrail(CaseTrailDto caseTrail)
        {
            try
            {
                if (caseTrail == null || string.IsNullOrWhiteSpace(caseTrail.CaseId))
                    return new OperationResult(1, "案件编号为空");
                var caseInfo = ServiceDepository.CaseDataAccess.GetCaseByCaseId(caseTrail.CaseId);
                if (caseInfo == null)
                    return new OperationResult(2, "案件不存在");

                var entity = ObjectMapper.Copy<CaseTrail>(caseTrail);
                if (!ServiceDepository.CaseDataAccess.InsertCaseTrail(entity))
                    return new OperationResult(3, "跟进失败");
                return new OperationResult(0);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-1, "系统内部错误");
            }
        }

        public IList<CaseTrailPaged> GetCaseTrailPaged(int pageIndex, int pageSize, out int totalCount, ReceptionCenter receptionCenter, CaseStatus operationStatus, IEnumerable<CaseStatus> categorys, ProccessType? proccessType, DateTime? startDate, string lastTrailer, Dictionary<string, object> data)
        {
            try
            {
                CaseFilter cf = new CaseFilter();
                cf.ReceptionCenters.Add(receptionCenter);
                if (proccessType.HasValue)
                    cf.ProccessTypes.Add(proccessType.Value);
                cf.TrailerName = lastTrailer;
                var cases = ServiceDepository.CaseDataAccess.GetPaginatedList(cf, pageIndex, pageSize, out totalCount);
                //var cases = ServiceDepository.CaseDataAccess.GetCases(pageIndex, pageSize, out totalCount, receptionCenter, proccessType, startDate, lastTrailer, data);
                if (cases == null || cases.Count == 0)
                    return null;
                return SetCaseTrailPaged(cases, operationStatus, categorys);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        private IList<CaseTrailPaged> SetCaseTrailPaged(IList<Case> cases, CaseStatus operationStatus, IEnumerable<CaseStatus> categorys)
        {
            var cts = new List<CaseTrailPaged>();
            IEnumerable<CaseEventInfo> caseEvents = ServiceDepository.CaseEventDataAccess.GetCaseEvents(cases.Select(c => c.CaseId), operationStatus);
            if (caseEvents == null)
                caseEvents = new List<CaseEventInfo>();
            var comDic = new Dictionary<long, string>();

            foreach (var c in cases)
            {
                var ct = new CaseTrailPaged
                {
                    CaseId = c.CaseId,
                    CreateDate = c.CreateDate,
                    SysNo = c.SysNo,
                    TenementAddress = c.TenementAddress,
                };
                //获取指定操作状态完成时间
                var caseEvent = caseEvents.FirstOrDefault(ce => c.CaseId == ce.CaseId);
                if (caseEvent != null)
                    ct.OperationDate = caseEvent.ModifyDate == null ? caseEvent.CreateDate : caseEvent.ModifyDate;

                //获取案件进度
                var caseTrail = ServiceDepository.CaseDataAccess.GetLastCaseTrailByCaseId(c.CaseId);
                if (caseTrail != null)
                {
                    ct.ProccessType = caseTrail.ProccessType;
                    ct.LastTrailer = caseTrail.ModifyUserName;
                    ct.ModifyDate = caseTrail.CreateDate;
                }
                //获取公司名称
                if (!comDic.ContainsKey(c.CompanySysNo))
                {
                    var company = ServiceDepository.AgentCompanyDataAccess.Get(c.CompanySysNo);
                    comDic[c.CompanySysNo] = "";
                    if (company != null)
                        comDic[c.CompanySysNo] = company.CompanyName;
                }
                ct.CompanyName = comDic[c.CompanySysNo];
                //获取案件人名
                var users = ServiceDepository.CaseDataAccess.GetRelation(c.CaseId);
                foreach (var user in users)
                {
                    if (user.CaseUserType == CaseUserType.AgentStaff)
                    {
                        ct.AgencyName = ServiceDepository.AgentStaffDataAccess.GetRealName(user.UserSysNo);
                    }
                    else if (user.CaseUserType == CaseUserType.Buyer)
                    {
                        ct.BuyerNameDic[user.UserSysNo] = ServiceDepository.CustomerDataAccess.GetRealName(user.UserSysNo);
                    }
                }
                if (categorys != null && categorys.Count() >= 0)
                {
                    foreach (var category in categorys)
                    {
                        var ce = ServiceDepository.CaseEventDataAccess.GetNewByCaseId(c.CaseId, category);
                        if (ce != null)
                            ct.LastStatusDic[category] = ce.EventType;
                    }
                }
                cts.Add(ct);
            }
            return cts;
        }

        private IList<CaseDto> FillCaseDto(Dictionary<string, CaseDto> dic, CaseStatus? operationStatus)
        {
            var rl = ServiceDepository.CaseDataAccess.GetRelations(dic.Keys.ToArray());
            //组装人
            foreach (var r in rl)
            {
                SetUser(r, dic[r.CaseId]);
            }
            //获取当前工作台操作人员
            if (operationStatus != null)
            {
                var caseEvents = ServiceDepository.CaseEventDataAccess.GetCaseEvents(dic.Keys, operationStatus.Value);
                if (caseEvents != null && caseEvents.Count() > 0)
                {
                    var userCache = new Dictionary<long, string>();
                    foreach (var ce in caseEvents)
                    {
                        if (ce.CreateUserSysNo != null)
                        {
                            if (!userCache.ContainsKey(ce.CreateUserSysNo.Value))
                            {
                                userCache[ce.CreateUserSysNo.Value] = "";
                                var user = ServiceDepository.UserDataAccess.Get(ce.CreateUserSysNo.Value);
                                if (user != null)
                                    userCache[ce.CreateUserSysNo.Value] = user.RealName;
                            }
                            dic[ce.CaseId].OtherData[ce.EventType.ToString()] = userCache[ce.CreateUserSysNo.Value];
                            dic[ce.CaseId].OtherData[string.Format("time_{0}", ce.EventType)] = ce.CreateDate;
                        }
                    }
                }
            }

            //组装公司
            SetShowInfo(dic.Values.ToArray());
            return dic.Values.ToList();
        }

        private void SetShowInfo(params CaseDto[] caseDtos)
        {
            //组装公司
            var comDic = new Dictionary<long, string>();
            var users = new Dictionary<long, string>();

            foreach (var c in caseDtos)
            {
                if (!comDic.ContainsKey(c.CompanySysNo))
                {
                    var company = ServiceDepository.AgentCompanyDataAccess.Get(c.CompanySysNo);
                    comDic[c.CompanySysNo] = "";
                    if (company != null)
                        comDic[c.CompanySysNo] = company.CompanyName;
                }
                c.CompanyName = comDic[c.CompanySysNo];

                //创建人
                if (c.CreateUserSysNo != null)
                {
                    if (!users.ContainsKey(c.CreateUserSysNo.Value))
                    {
                        var creator = ServiceDepository.UserDataAccess.Get(c.CreateUserSysNo.Value);
                        users[c.CreateUserSysNo.Value] = "";
                        if (creator != null)
                            users[c.CreateUserSysNo.Value] = creator.RealName;
                    }
                    c.CreatorName = users[c.CreateUserSysNo.Value];
                }

                //修改人
                if (c.ModifyUserSysNo != null)
                {
                    if (!users.ContainsKey(c.ModifyUserSysNo.Value))
                    {
                        var modifyUser = ServiceDepository.UserDataAccess.Get(c.CreateUserSysNo.Value);
                        users[c.ModifyUserSysNo.Value] = "";
                        if (modifyUser != null)
                            users[c.ModifyUserSysNo.Value] = modifyUser.RealName;
                    }
                    c.ModifyUserName = users[c.ModifyUserSysNo.Value];
                }

            }
        }

        private CaseDto SetUser(CaseDto caseDto)
        {
            var rs = ServiceDepository.CaseDataAccess.GetRelation(caseDto.CaseId);

            foreach (var r in rs)
            {
                SetUser(r, caseDto);
            }
            SetShowInfo(caseDto);
            return caseDto;
        }

        private CaseDto SetUser(CaseUserRelation r, CaseDto caseInfo)
        {
            switch (r.CaseUserType)
            {
                case CaseUserType.Seller:
                    caseInfo.AddSeller(r.UserSysNo);
                    caseInfo.SellerNameDic[r.UserSysNo] = ServiceDepository.CustomerDataAccess.GetRealName(r.UserSysNo);
                    break;
                case CaseUserType.Buyer:
                    caseInfo.AddBuyer(r.UserSysNo);
                    caseInfo.BuyerNameDic[r.UserSysNo] = ServiceDepository.CustomerDataAccess.GetRealName(r.UserSysNo);
                    break;
                case CaseUserType.AgentStaff:
                    caseInfo.AgencySysNo = r.UserSysNo;
                    caseInfo.AgencyName = ServiceDepository.AgentStaffDataAccess.GetRealName(r.UserSysNo);
                    break;
                default:
                    caseInfo.OperatorSysNo = r.UserSysNo;
                    //caseInfo.OperatorName = r.RealName;
                    break;
            }
            return caseInfo;
        }

        private CaseWithCustomer SetUser(CaseWithCustomer caseDto)
        {
            var rs = ServiceDepository.CaseDataAccess.GetRelation(caseDto.CaseId);

            foreach (var r in rs)
            {
                switch (r.CaseUserType)
                {
                    case CaseUserType.Seller:
                        caseDto.AddSeller(ObjectMapper.Copy<Customer>(ServiceDepository.CustomerDataAccess.Get(r.UserSysNo)));
                        break;
                    case CaseUserType.Buyer:
                        caseDto.AddBuyer(ObjectMapper.Copy<Customer>(ServiceDepository.CustomerDataAccess.Get(r.UserSysNo)));
                        break;
                    case CaseUserType.AgentStaff:
                        caseDto.AgencySysNo = r.UserSysNo;
                        caseDto.AgencyName = ServiceDepository.AgentStaffDataAccess.GetRealName(r.UserSysNo);
                        break;
                    default:
                        caseDto.OperatorSysNo = r.UserSysNo;
                        //caseInfo.OperatorName = r.RealName;
                        break;
                }
            }
            //组装公司
            var comDic = new Dictionary<long, string>();
            var users = new Dictionary<long, string>();

            if (!comDic.ContainsKey(caseDto.CompanySysNo))
            {
                var company = ServiceDepository.AgentCompanyDataAccess.Get(caseDto.CompanySysNo);
                comDic[caseDto.CompanySysNo] = "";
                if (company != null)
                    comDic[caseDto.CompanySysNo] = company.CompanyName;
            }
            caseDto.CompanyName = comDic[caseDto.CompanySysNo];

            //创建人
            if (caseDto.CreateUserSysNo != null)
            {
                if (!users.ContainsKey(caseDto.CreateUserSysNo.Value))
                {
                    var creator = ServiceDepository.UserDataAccess.Get(caseDto.CreateUserSysNo.Value);
                    users[caseDto.CreateUserSysNo.Value] = "";
                    if (creator != null)
                        users[caseDto.CreateUserSysNo.Value] = creator.RealName;
                }
                caseDto.CreatorName = users[caseDto.CreateUserSysNo.Value];
            }

            //修改人
            if (caseDto.ModifyUserSysNo != null)
            {
                if (!users.ContainsKey(caseDto.ModifyUserSysNo.Value))
                {
                    var modifyUser = ServiceDepository.UserDataAccess.Get(caseDto.CreateUserSysNo.Value);
                    users[caseDto.ModifyUserSysNo.Value] = "";
                    if (modifyUser != null)
                        users[caseDto.ModifyUserSysNo.Value] = modifyUser.RealName;
                }
                caseDto.ModifyUserName = users[caseDto.ModifyUserSysNo.Value];
            }
            return caseDto;
        }

        public OperationResult PerfectBuyer(string caseId, Buyer buyer, IDictionary<long, string> mobiles, IDictionary<long, string> emails, long operatorSysNo)
        {
            if (string.IsNullOrWhiteSpace(caseId))
                return new OperationResult(-1, "案件编号错误");
            var buyerInfo = ServiceDepository.BuyerDataAccess.Get(caseId);
            if (buyerInfo == null)
                return new OperationResult(-2, "数据异常");
            var entity = ObjectMapper.Copy<BuyerInfo>(buyer);
            entity.SysNo = buyerInfo.SysNo;
            entity.CaseId = buyerInfo.CaseId;
            entity.ModifyUserSysNo = operatorSysNo;

            try
            {
                ServiceDepository.TransactionService.Begin();
                if (!ServiceDepository.BuyerDataAccess.Update(entity))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-3, "买家信息保存失败");
                }
                List<long> updated = new List<long>();
                if (mobiles != null)
                {
                    foreach (var mobile in mobiles)
                    {
                        if (!ServiceDepository.CustomerDataAccess.PrefectCustomer(mobile.Key,
                            emails != null && emails.ContainsKey(mobile.Key) ? emails[mobile.Key] : null, mobile.Value, operatorSysNo))
                        {
                            ServiceDepository.TransactionService.RollBack();
                            return new OperationResult(-4, "客户基本信息保存失败");
                        }
                        updated.Add(mobile.Key);
                    }
                }
                if (emails != null)
                {
                    foreach (var email in emails)
                    {
                        if (updated.Contains(email.Key))
                            continue;
                        if (!ServiceDepository.CustomerDataAccess.PrefectCustomer(email.Key, email.Value, null, operatorSysNo))
                        {
                            ServiceDepository.TransactionService.RollBack();
                            return new OperationResult(-4, "客户基本信息保存失败");
                        }
                    }
                }
                ServiceDepository.TransactionService.Commit();
                return new OperationResult(0);
            }
            catch (Exception e)
            {
                ServiceDepository.TransactionService.RollBack();
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }

        public OperationResult PrefectSeller(string caseId, Seller seller, IDictionary<long, string> mobiles, IDictionary<long, string> emails, long operatorSysNo)
        {
            if (string.IsNullOrWhiteSpace(caseId))
                return new OperationResult(-1, "案件编号错误");
            var sellerInfo = ServiceDepository.SellerDataAccess.Get(caseId);
            if (sellerInfo == null)
                return new OperationResult(-2, "数据异常");
            var entity = ObjectMapper.Copy<SellerInfo>(seller);
            entity.SysNo = sellerInfo.SysNo;
            entity.CaseId = sellerInfo.CaseId;
            entity.ModifyUserSysNo = operatorSysNo;

            try
            {
                ServiceDepository.TransactionService.Begin();
                if (!ServiceDepository.SellerDataAccess.Update(entity))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-3, "卖家信息保存失败");
                }
                List<long> updated = new List<long>();
                if (mobiles != null)
                {
                    foreach (var mobile in mobiles)
                    {
                        if (!ServiceDepository.CustomerDataAccess.PrefectCustomer(mobile.Key,
                            emails != null && emails.ContainsKey(mobile.Key) ? emails[mobile.Key] : null, mobile.Value, operatorSysNo))
                        {
                            ServiceDepository.TransactionService.RollBack();
                            return new OperationResult(-4, ".");
                        }
                        updated.Add(mobile.Key);
                    }
                }
                if (emails != null)
                {
                    foreach (var email in emails)
                    {
                        if (updated.Contains(email.Key))
                            continue;
                        if (!ServiceDepository.CustomerDataAccess.PrefectCustomer(email.Key, email.Value, null, operatorSysNo))
                        {
                            ServiceDepository.TransactionService.RollBack();
                            return new OperationResult(-4, "客户基本信息保存失败");
                        }
                    }
                }
                ServiceDepository.TransactionService.Commit();
                return new OperationResult(0);
            }
            catch (Exception e)
            {
                ServiceDepository.TransactionService.RollBack();
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }

        public Buyer GetBuyer(string caseId)
        {
            if (string.IsNullOrWhiteSpace(caseId))
                return null;
            var entity = ServiceDepository.BuyerDataAccess.Get(caseId);
            if (entity == null)
                return null;
            try
            {
                var buyer = ObjectMapper.Copy<Buyer>(entity);
                var customers = ServiceDepository.CaseDataAccess.GetRelation(caseId).Where(r => r.CaseUserType == CaseUserType.Buyer);
                foreach (var relation in customers)
                {
                    buyer.Customers.Add(ObjectMapper.Copy<Customer>(ServiceDepository.CustomerDataAccess.Get(relation.UserSysNo)));
                }
                return buyer;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return null;
            }
        }

        public Seller GetSeller(string caseId)
        {
            if (string.IsNullOrWhiteSpace(caseId))
                return null;
            var entity = ServiceDepository.SellerDataAccess.Get(caseId);
            if (entity == null)
                return null;
            try
            {
                var seller = ObjectMapper.Copy<Seller>(entity);
                var customers = ServiceDepository.CaseDataAccess.GetRelation(caseId).Where(r => r.CaseUserType == CaseUserType.Seller);
                foreach (var relation in customers)
                {
                    seller.Customers.Add(ObjectMapper.Copy<Customer>(ServiceDepository.CustomerDataAccess.Get(relation.UserSysNo)));
                }
                return seller;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return null;
            }
        }

        public Tuple<Seller, Buyer> GetCaseCustomers(string caseId)
        {
            if (string.IsNullOrWhiteSpace(caseId))
                return null;
            var buyerInfo = ServiceDepository.BuyerDataAccess.Get(caseId);
            var sellerInfo = ServiceDepository.SellerDataAccess.Get(caseId);
            if (buyerInfo == null && sellerInfo == null)
                return null;
            Seller seller = null; Buyer buyer = null;
            try
            {
                var customers = ServiceDepository.CaseDataAccess.GetRelation(caseId);
                if (buyerInfo != null)
                {
                    buyer = ObjectMapper.Copy<Buyer>(buyerInfo);
                    var buyerRels = customers.Where(r => r.CaseUserType == CaseUserType.Buyer);
                    foreach (var relation in buyerRels)
                    {
                        var customer = ObjectMapper.Copy<Customer>(ServiceDepository.CustomerDataAccess.GetWithAddress(relation.UserSysNo));
                        var parterUsers = ServiceDepository.ParterUserInfoDataAccess.GetByUserSysNo(customer.UserSysNo);
                        if (parterUsers != null && parterUsers.Count > 0)
                            customer.IsConcern = true;
                        buyer.Customers.Add(customer);
                    }
                }
                if (sellerInfo != null)
                {
                    seller = ObjectMapper.Copy<Seller>(sellerInfo);
                    var sellerRels = customers.Where(r => r.CaseUserType == CaseUserType.Seller);
                    foreach (var relation in sellerRels)
                    {
                        var customer = ObjectMapper.Copy<Customer>(ServiceDepository.CustomerDataAccess.GetWithAddress(relation.UserSysNo));
                        var parterUsers = ServiceDepository.ParterUserInfoDataAccess.GetByUserSysNo(customer.UserSysNo);
                        if (parterUsers != null && parterUsers.Count > 0)
                            customer.IsConcern = true;
                        seller.Customers.Add(customer);
                    }
                }
                return new Tuple<Seller, Buyer>(seller, buyer);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return null;
            }
        }



        public CaseDto GetCaseByUserSysNo(long userSysNo)
        {
            try
            {
                if (userSysNo <= 0)
                    return null;
                var caseInfo = ServiceDepository.CaseDataAccess.GetCaseByUserSysNo(userSysNo);
                if (caseInfo == null)
                    return null;
                return ObjectMapper.Copy<CaseDto>(caseInfo);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        public IList<string> GetCaseIds(int pageIndex, int pageCount, out int totalCount, ReceptionCenter center, string caseId)
        {
            return ServiceDepository.CaseDataAccess.GetIds(pageIndex, pageCount, out totalCount, center, caseId);
        }

        #region Remark

        public OperationResult PerfectCaseInfo(CaseRemark remark, bool? isFundTrusteeship = null, string ftContractNo = null, bool? isCommissionTrusteeship = null, string ctContractNo = null, IEnumerable<CustomerBankAccount> accounts = null)
        {
            try
            {
                var caseInfo = ServiceDepository.CaseDataAccess.Get(remark.SysNo);
                bool result = false;
                if (caseInfo == null)
                    return new OperationResult(-1, "案件不存在");

                #region 案件基本信息
                if (isFundTrusteeship != null && isCommissionTrusteeship != null)
                {
                    if ((isFundTrusteeship.Value && string.IsNullOrWhiteSpace(ftContractNo)) || (isCommissionTrusteeship.Value && string.IsNullOrWhiteSpace(ctContractNo)))
                        return new OperationResult(-1, "资金托管合同号不能为空");
                    result = ServiceDepository.CaseDataAccess.UpdateTrusteeshipInfo(caseInfo.SysNo, isFundTrusteeship, ftContractNo, isCommissionTrusteeship, ctContractNo, remark.CreateUserSysNo);
                }
                else
                    result = true;
                if (!result)
                    return new OperationResult(-2, "案件信息保存失败");
                #endregion

                #region 案件扩展信息
                var entity = ServiceDepository.CaseDataAccess.GetRemark(remark.SysNo);
                if (entity == null)
                    result = ServiceDepository.CaseDataAccess.InsertRemark(ObjectMapper.Copy<CaseRemarkInfo>(remark));
                else
                {
                    remark.ModifyUserSysNo = remark.CreateUserSysNo;
                    remark.ModifyDate = DateTime.Now;
                    result = ServiceDepository.CaseDataAccess.UpdateRemark(ObjectMapper.Copy<CaseRemarkInfo>(remark));
                }
                if (!result)
                    return new OperationResult(-3, "案件扩展信息保存失败");

                #endregion

                #region 上下家银行账户信息

                if (((isFundTrusteeship != null && isFundTrusteeship.Value) || (isCommissionTrusteeship != null && isCommissionTrusteeship.Value)) && accounts != null && accounts.Count() > 0)
                {
                    //var existsBanks = ServiceDepository.CustomerBankAccountDataAccess.GetByCase(caseInfo.CaseId);
                    //IList<CustomerBankAccountInfo> insertBanks = new List<CustomerBankAccountInfo>();
                    //foreach (var bItem in accounts) 
                    //{
                    //    if (!existsBanks.Any(b => b.AccountNo == bItem.AccountNo))
                    //        insertBanks.Add(ObjectMapper.Copy<CustomerBankAccountInfo>(bItem));
                    //}
                    //if (insertBanks.Count > 0)
                    result = ServiceDepository.CustomerBankAccountDataAccess.BatchInsert(accounts.Select(a => ObjectMapper.Copy<CustomerBankAccountInfo>(a)));
                }

                if (!result)
                    return new OperationResult(-4, "客户银行账户信息保存失败");
                #endregion
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
            return new OperationResult(0);
        }

        public CaseRemark GetRemarkBySysNo(long sysNo)
        {
            var entity = ServiceDepository.CaseDataAccess.GetRemark(sysNo);
            if (entity == null)
                return null;
            return ObjectMapper.Copy<CaseRemark>(entity);
        }

        public IEnumerable<CustomerBankAccount> GetCustomerBankAccounts(long caseSysNo, CustomerType? customerType = null, BankAccountPurpose? accountType = null)
        {
            var entities = ServiceDepository.CustomerBankAccountDataAccess.GetByCase(caseSysNo, customerType, accountType);
            if (entities == null)
                return null;
            return entities.Select(e => ObjectMapper.Copy<CustomerBankAccount>(e));
        }

        public CaseRemark GetRemark(string tenementContract, ReceptionCenter center, Dictionary<string, object> data)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(tenementContract) || center == ReceptionCenter.Unkonw)
                    return null;
                var caseInfo = ServiceDepository.CaseDataAccess.GetCaseByTenementContract(tenementContract, center);
                if (caseInfo == null)
                    return null;
                var caseRemark = ServiceDepository.CaseDataAccess.GetRemark(caseInfo.SysNo);
                var cr = new CaseRemark();
                if (caseRemark != null)
                    cr = ObjectMapper.Copy<CaseRemark>(caseRemark);
                cr.OtherData["CaseId"] = caseInfo.CaseId;
                cr.OtherData["TenementContract"] = caseInfo.TenementContract;
                cr.OtherData["TenementAddress"] = caseInfo.TenementAddress;
                return cr;

            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return null;

        }

        public IDictionary<string, string> GetTenementContracts(QueryStringType queryStringType, string queryString, ReceptionCenter center, Dictionary<string, object> data = null)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(queryString) || center == ReceptionCenter.Unkonw)
                    return null;
                if (data == null)
                    data = new Dictionary<string, object>();
                data["ReceptionCenter"] = (int)center;
                SetQueryDic(queryStringType, queryString, data);
                var result = ServiceDepository.CaseDataAccess.GetCases(data);
                if (result == null || result.Count == 0)
                    return null;
                var dic = new Dictionary<string, string>();
                foreach (var c in result)
                {
                    dic[c.TenementContract] = c.TenementAddress;
                }
                return dic;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return null;
        }

        [Obsolete]
        public IList<CaseDto> GetCases(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, QueryStringType? queryStringType = null, string queryString = null, Dictionary<string, object> data = null)
        {
            if (data == null)
                data = new Dictionary<string, object>();
            data["ReceptionCenter"] = (int)center;
            if (queryStringType != null)
                SetQueryDic(queryStringType.Value, queryString, data);
            var cases = ServiceDepository.CaseDataAccess.GetCases(pageIndex, pageSize, out totalCount, data);
            if (cases == null || cases.Count == 0)
                return null;
            var caseDtos = ObjectMapper.Copy<Case, CaseDto>(cases);
            var dic = caseDtos.ToDictionary(c => c.CaseId);
            return FillCaseDto(dic, null);

        }

        private void SetQueryDic(QueryStringType queryStringType, string queryString, Dictionary<string, object> data)
        {
            switch (queryStringType)
            {
                case QueryStringType.CaseId:
                    data["CaseId"] = queryString;
                    break;
                case QueryStringType.TenementContract:
                    data["TenementContract"] = string.Format("{0}%", queryString);
                    break;
                default:
                    //不能加上''
                    data["TenementAddress"] = string.Format("%{0}%", queryString);
                    break;
            }
        }

        public OperationResult AddCustomerBankAccount(string caseId, CustomerType type, IEnumerable<BankAccountPurpose> purpose, string bankCode, string bankName, string subBankName, string bankAccount, string accountName, bool isMainAcccount, long operatorSysNo, bool needAuth = false, string identityNo = null, string mobile = null)
        {
            if (purpose == null || purpose.Count() == 0)
                return new OperationResult(-1, "账户用途错误");
            if (string.IsNullOrWhiteSpace(bankCode) || string.IsNullOrWhiteSpace(bankName) || string.IsNullOrWhiteSpace(subBankName) || string.IsNullOrWhiteSpace(accountName))
                return new OperationResult(-2, "账户信息错误");
            if (needAuth && string.IsNullOrWhiteSpace(identityNo))
                return new OperationResult(-3, "身份证不能为空");
            try
            {
                if (needAuth)
                {
                    var authRet = ServiceDepository.ExternalService.AuthenticateBankCard(accountName, identityNo, string.IsNullOrEmpty(mobile) ? "11111111111" : mobile, bankAccount, bankCode);
                    if (!authRet.Success)
                        return new OperationResult(-4, string.Format("鉴权失败:{0}", authRet.ResultMessage));
                }

                ServiceDepository.TransactionService.Begin();
                foreach (var item in purpose)
                {
                    if (!ServiceDepository.CustomerBankAccountDataAccess.Insert(new CustomerBankAccountInfo
                    {
                        CaseId = caseId,
                        CustomerType = type,
                        AccountType = item,
                        BankCode = bankCode,
                        BankName = bankName,
                        SubBankName = subBankName,
                        AccountNo = bankAccount,
                        AccountName = accountName,
                        IsMainAccount = isMainAcccount,
                        IdCardNo = identityNo,
                        IsAuthed = needAuth,
                        Mobile = mobile,
                        CreateUserSysNo = operatorSysNo
                    }))
                    {
                        throw new Exception(string.Format("{0}添加失败", item.ToString()));
                    }
                }
                ServiceDepository.TransactionService.Commit();
            }
            catch (Exception e)
            {
                ServiceDepository.TransactionService.RollBack();
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
            return new OperationResult(0);
        }

        #endregion
    }
}
