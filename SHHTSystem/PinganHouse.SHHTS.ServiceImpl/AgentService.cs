﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Activation;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.ServiceImpl
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AgentService : IAgentService
    {
        public OperationResult AgentJoint(long agentSysNo, long operatorSysNo)
        {
            try
            {
                var entity = ServiceDepository.AgentCompanyDataAccess.Get(agentSysNo);
                if (entity == null)
                    return new OperationResult(-2, "中介公司信息不存在");
                if (entity.Status != AgentJointStatus.None)
                    return new OperationResult(-3, "状态异常");
                entity.Status = AgentJointStatus.Jointing;
                entity.ModifyUserSysNo = operatorSysNo;
                entity.JointDate = DateTime.Now;
                if (ServiceDepository.AgentCompanyDataAccess.Update(entity))
                    return new OperationResult(0);
            }
            catch(Exception e) 
            {
                Log.Error(e);
            }
            return new OperationResult(-4, "保存失败");
        }

        public OperationResult AgentJoint(AgentCompany agentCompany)
        {
            if (agentCompany == null)
                return new OperationResult(-1);
            if (string.IsNullOrEmpty(agentCompany.CompanyName) || string.IsNullOrEmpty(agentCompany.Principal))
                return new OperationResult(-1);
            try
            {
                var entity = ObjectMapper.Copy<AgentCompanyInfo>(agentCompany);
                if (ServiceDepository.AgentCompanyDataAccess.Insert(entity))
                {              
                    //创建微信中介公司
                    ServiceDepository.ParterChannelService.CreateDepartment(string.Format("{0}({1})", agentCompany.CompanyName, entity.SysNo), (int)AgentType.AgentCompany, 9, (int)entity.SysNo);
                    return new OperationResult(0);
                }

            }
            catch (Exception e) 
            {
                Log.Error(e);
            }
            return new OperationResult(-4, "保存失败");
        }


        public OperationResult AgentCancel(long agentSysNo, string cancelReson, long operatorSysNo)
        {
            try
            {
                if (!ServiceDepository.AgentCompanyDataAccess.Exists(agentSysNo))
                    return new OperationResult(-2, "中介公司不存在");
                if (ServiceDepository.AgentCompanyDataAccess.AgentCancel(agentSysNo, cancelReson, operatorSysNo))
                    return new OperationResult(0);
            }
            catch (Exception e) 
            {
                Log.Error(e);
            }
            return new OperationResult(-4, "操作失败");
        }

        public AgentCompany GetAgentCompany(long agentSysNo)
        {
            var company = ServiceDepository.AgentCompanyDataAccess.Get(agentSysNo);
            if (company == null)
                return null;
            return ObjectMapper.Copy<AgentCompany>(company);
        }

        public IList<AgentCompany> GetAgentCompanyList(string condition, AgentJointStatus? status, int pageIndex, int pageSize, out int totalCount, ConditionMatchingType matchingType = ConditionMatchingType.FullFuzzy)
        {
            var results = ServiceDepository.AgentCompanyDataAccess.GetPaginatedList(condition, status, pageIndex, pageSize, out totalCount, matchingType: matchingType);
            if (results == null)
                return null;
            return results.Select(s => ObjectMapper.Copy<AgentCompany>(s)).ToList();
        }

        public OperationResult ModifyCompanyInfo(long companySysNo, string companyName, string principal, string address, string contactInfo, long operatorSysNo)
        {
            try
            {
                var entity = ServiceDepository.AgentCompanyDataAccess.Get(companySysNo);
                if (entity == null)
                    return new OperationResult(-1, "指定中介公司信息不存在");
                entity.CompanyName = string.IsNullOrEmpty(companyName) ? entity.CompanyName : companyName;
                entity.Principal = string.IsNullOrEmpty(principal) ? entity.Principal : principal;
                entity.Address = string.IsNullOrEmpty(address) ? entity.Address : address;
                entity.ContactInfo = string.IsNullOrEmpty(contactInfo) ? entity.ContactInfo : contactInfo;
                entity.ModifyUserSysNo = operatorSysNo;
                if (ServiceDepository.AgentCompanyDataAccess.Update(entity))
                    return new OperationResult(0);
            }
            catch (Exception e) 
            {
                Log.Error(e);
            }
            return new OperationResult(-4, "保存失败");
        }
    }
}
