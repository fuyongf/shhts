﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.ServiceImpl.External.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.ServiceImpl.External
{
    internal class WXQRCodeService : WeiXinAuthenticationServiceBase<QRCodeSetting>
    {

        public override OperationResult SendData(IDictionary<string, object> data)
        {
            if (data == null || data.Count == 0)
                return new OperationResult(1, "param is null");
            if (!data.ContainsKey("action_name"))
                return new OperationResult(2, "action_name is null");
            var result = this.UploadString(data, this.Setting.QRCodeTempUrl);
            if (result.Success)
                result.OtherData["ShowQRCodeUrl"] = string.Format(this.Setting.ShowQRCodeUrl, result.OtherData["ticket"]);
            return result;
        }
    }
}
