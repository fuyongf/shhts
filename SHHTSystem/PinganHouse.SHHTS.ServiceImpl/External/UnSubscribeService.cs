﻿using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.DataTransferObjects.External;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.ServiceImpl.External.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace PinganHouse.SHHTS.ServiceImpl.External
{
    internal class UnSubscribeService : WeiXinAuthenticationServiceBase<UserSetting>
    {
        public override OperationResult ValidateCallResult(HttpContextBase ctx, IDictionary<string, object> data)
        {
            var message = data["WXMessage"] as WXMessage;
            if (message == null || message.Event.Value != EventType.UnSubscribe)
                return new OperationResult((int)message.Event.Value, "订阅处理器不匹配");
            return this.Handle(message);

        }

        private OperationResult Handle(WXMessage message)
        {
            if (ServiceDepository.ParterUserInfoDataAccess.DeleteByOpenId(message.FromUserName))
                return new OperationResult(0,"解绑成功");
            return new OperationResult(1, "解绑失败");
        }

        public override OperationResult SendData(IDictionary<string, object> data)
        {
            throw new NotImplementedException();
        }
    }
}
