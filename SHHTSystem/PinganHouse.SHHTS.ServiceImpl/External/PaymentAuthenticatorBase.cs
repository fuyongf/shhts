﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.ServiceImpl.External.Config;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Utils.Log;

namespace PinganHouse.SHHTS.ServiceImpl.External
{
    internal abstract class PaymentAuthenticatorBase : PartnerAuthenticatorBase<PaymentSetting>
    {
        protected const string SIGN_JOINT_STR = "_ping_an_fang_";

        public override OperationResult SendData(IDictionary<string, object> data)
        {
            return SendData<object>(data);
        }

        public override OperationResult SendData<TResult>(IDictionary<string, object> data)
        {
            var check = CheckParams(data);
            if (!check.Success)
                return check;

            try
            {
                var response = Send<TResult>(data);

                if (response == null)
                    return new OperationResult(-2, "响应信息错误");
                if (!"1".Equals(response.code))
                    return new OperationResult(-3, string.Format("错误代码:{0},错误描述:{1}", response.code, response.memo));
                var result = new OperationResult(0);
                result.OtherData.Add("Data", response.data);
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, e.Message);
            }
        }

        public override OperationResult ValidateCallResult(HttpContextBase ctx, IDictionary<string, object> data)
        {
            var parameters = BuildParameters(ctx.Request);
            var checkParams = CheckCallBackParams(parameters);
            if (!checkParams.Success)
                return checkParams;
            //#if !DEBUG   
            string orginSign = parameters["sSign"].ToString();
            parameters.Remove("sSign");
            string sign = GetSign(parameters);
            if (!orginSign.Equals(sign))
                return new OperationResult(-100, "签名错误");
            //#endif
            return new OperationResult(0);
        }

        protected abstract OperationResult CheckParams(IDictionary<string, object> data);

        protected virtual OperationResult CheckCallBackParams(IDictionary<string, object> data)
        {
            if (data == null || data.Count == 0)
                return new OperationResult(-99, "参数错误");
            if (!data.ContainsKey("sSign"))
                return new OperationResult(-98, "未提供sSign");
            if (!data.ContainsKey("iRequestTime") || data["iRequestTime"] == null)
                return new OperationResult(-97, "未提供iRequestTime");
            //#if !DEBUG   
            string requestTime = data["iRequestTime"].ToString();
            int timestamp = UnixTimeStamp;
            if (!int.TryParse(requestTime, out timestamp))
                return new OperationResult(-96, "iRequestTime格式错误");
            if (Math.Abs(UnixTimeStamp - timestamp) > 600)
                return new OperationResult(-95, "请求超时");
            //#endif
            return new OperationResult(0);
        }

        protected string GetSign(IDictionary<string, object> parameters)
        {
            if (string.IsNullOrEmpty(this.Setting.SecurityKey))
                throw new ArgumentNullException("security key not found.");
            //sort
            if (!(parameters is SortedDictionary<string, object>))
            {
                var sortedDic = new SortedDictionary<string, object>();
                foreach (var item in parameters)
                {
                    sortedDic.Add(item.Key, item.Value == null ? string.Empty : item.Value.ToString());
                }
                parameters = sortedDic;
            }
            //json
            string json = JsonSerializer.SerializeToJson(parameters);
            //Log.Client(json);
            //md5
            string sign = EncryptHelper.GetMD5(string.Concat(this.Setting.SecurityKey, SIGN_JOINT_STR, json));
            //Log.Client(sign);
            return EncryptHelper.GetMD5(string.Concat(sign, SIGN_JOINT_STR, this.Setting.SecurityKey));
        }

        protected FsResponse<TResult> Send<TResult>(IDictionary<string, object> parameters)
        {
            if (parameters == null)
                parameters = new Dictionary<string, object>();

            parameters.Add("iRequestTime", UnixTimeStamp);
            parameters.Add("sSign", GetSign(parameters));

            var retStr = HttpUtils.SendPostRequest(Setting.RequestUrl, BuildParameters(parameters), 30000, Encoding.UTF8, Encoding.UTF8, null);
            if (string.IsNullOrEmpty(retStr))
                Log.Error("远程请求提交发生错误.");
            #if DEBUG
            Log.Info(retStr);
            #endif
            return JsonSerializer.DeserializeToObject<FsResponse<TResult>>(retStr);
        }

        protected DateTime ConvertFromUnixTimeStamp(string timestamp)
        {
            if (string.IsNullOrEmpty(timestamp))
                throw new ArgumentException("timestamp");
            return TimeStamp.ConvertToDateTime(long.Parse(timestamp));
        }
    }

    internal class FsResponse<T>
    {
        public string code { get; set; }

        public string memo { get; set; }

        public IList<T> data { get; set; }
    }
}
