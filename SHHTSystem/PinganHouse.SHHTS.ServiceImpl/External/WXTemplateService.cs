﻿using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Core.External;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.DataTransferObjects.External;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.ServiceImpl.External.Config;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace PinganHouse.SHHTS.ServiceImpl.External
{
    internal class WXTemplateService : WeiXinAuthenticationServiceBase<TemplateSetting>
    {
        public override OperationResult ValidateCallResult(HttpContextBase ctx, IDictionary<string, object> data)
        {
            var message = data["WXMessage"] as WXMessage;
            if (message == null || message.Event.Value != EventType.TemplateSendJobFinish)
                return new OperationResult((int)EventType.TemplateSendJobFinish, "处理器不匹配");
            this.Handle(message);
            return new OperationResult(0);
        }

        private void Handle(WXMessage message)
        {
            var status = message.Status.Split(':');
            //是否成功发送模板消息
            if (string.Compare("success", status[0], true) == 0)
            {
                ServiceDepository.WXTemplateMessageDataAccess.UpdateByMsgId(message.MsgID, WXStatus.Success);
            }
            else if (status.Length > 1)
            {
                var tMessage = ServiceDepository.WXTemplateMessageDataAccess.GetMessageByMsgId(message.MsgID);
                if (tMessage != null)
                {
                    if (string.Compare("user block", status[1], true) != 0 && tMessage.SendsCount > 5)
                    {
                        tMessage.SendsCount++;
                        tMessage.Status = WXStatus.SystemFailed;
                        this.SendData(tMessage.Data);
                    }
                    else
                    {
                        //用户拒绝
                        tMessage.Status = WXStatus.UserBlock;
                    }
                    tMessage.Remark += string.Format("{0}、{1}", tMessage.SendsCount, status[1]);
                    ServiceDepository.WXTemplateMessageDataAccess.Update(tMessage);
                }
            }
        }

        public override OperationResult SendData(IDictionary<string, object> data)
        {
            var result = CheckParams(data);
            if (!result.Success)
                return result;
            try
            {
                if(!data.ContainsKey("template_id"))
                {
                    var template_id_short = data.First(d => string.Compare(d.Key, "template_id_short", true) == 0);
                    result = this.UploadString(template_id_short, this.Setting.TemplateIdUrl);
                    if (!result.Success)
                        return result;                  
                    data["template_id"] = result.OtherData["template_id"];
                }
                data.Remove("template_id_short");
                return this.UploadString(data, this.Setting.TemplateUrl);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-1, "系统内部错误");
            }
        }

        protected OperationResult CheckParams(IDictionary<string, object> data)
        {
            if (data == null || data.Count == 0)
                return new OperationResult(1,"params is null");
            if ((!data.ContainsKey("template_id_short") || string.IsNullOrWhiteSpace(data["template_id_short"].ToString())) && (!data.ContainsKey("template_id") || string.IsNullOrWhiteSpace(data["template_id"].ToString())))
                return new OperationResult(2, "template is null");
            if (!data.ContainsKey("touser") || string.IsNullOrWhiteSpace(data["touser"].ToString()))
                return new OperationResult(3, "touser is null");
            if (!data.ContainsKey("data") || string.IsNullOrWhiteSpace(data["data"].ToString()))
                return new OperationResult(4, "data is null");
            
            return new OperationResult(0);
        }

        public override OperationResult SendData<TResult>(IDictionary<string, object> data)
        {
            throw new NotImplementedException();
        }
    }
}
