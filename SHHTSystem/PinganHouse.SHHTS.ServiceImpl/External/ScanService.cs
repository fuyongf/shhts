﻿using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.DataTransferObjects.External;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.ServiceImpl.External.Config;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace PinganHouse.SHHTS.ServiceImpl.External
{
    internal class ScanService : WeiXinAuthenticationServiceBase<UserSetting>
    {

        public override OperationResult ValidateCallResult(HttpContextBase ctx, IDictionary<string, object> data)
        {
            var message = data["WXMessage"] as WXMessage;
            if (message == null || message.Event.Value != EventType.Scan)
                return new OperationResult((int)message.Event.Value, "订阅处理器不匹配");
            return this.Handle(message);
        }

        private OperationResult Handle(WXMessage message)
        {
            long userSysNo;
            var result = new OperationResult(0);
            if(long.TryParse(message.EventKey, out userSysNo))
                result = this.BindParterUser(message.FromUserName, userSysNo);
            var messageFormat = ServiceDepository.SettingService.GetSettingItemValueByPath("/System/Parter/WeiXin/ReplyMessages/Text/Subscribe");
            return new OperationResult(0, string.Format(messageFormat, message.FromUserName, message.ToUserName, this.UnixTimeStamp, result.Success ? result.ResultMessage : ""));  
        }

        public override OperationResult SendData(IDictionary<string, object> data)
        {
            if (data == null || data.Count == 0)
                return new OperationResult(1, "param is null");
            if (!data.ContainsKey("openid") || data["openid"] == null)
                return new OperationResult(2, "openid is null");
            return this.DownLoadString(string.Format(this.Setting.UserUrl, this.AccessToken, data["openid"]));
        }
    }
}
