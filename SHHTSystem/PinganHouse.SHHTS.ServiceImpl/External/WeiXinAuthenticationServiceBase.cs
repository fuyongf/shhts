﻿using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataAccess.MongoRepository;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.DataTransferObjects.External;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.ServiceImpl.External.Config;
using PinganHouse.SHHTS.Utils;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Web;

namespace PinganHouse.SHHTS.ServiceImpl.External
{
    internal abstract class WeiXinAuthenticationServiceBase<TSetting> : PartnerAuthenticatorBase<TSetting>
        where TSetting : WeiXinSetting
    {
        protected virtual OperationResult UploadString(object data, string requestUrl)
        {
            using (var client = new WebClient { Encoding = Encoding.UTF8 })
            {
                var json = JsonSerializer.SerializeToJson(data);
                var result = client.UploadString(string.Format(requestUrl, AccessToken), json);
                if (string.IsNullOrWhiteSpace(result))
                    return new OperationResult(1, "推送失败");
                var dic = JsonSerializer.DeserializeToDictionary(result);
                if (!dic.ContainsKey("errcode"))
                    return new OperationResult(0) { OtherData = dic, };
                return new OperationResult(int.Parse(dic["errcode"].ToString()), dic["errmsg"].ToString())
                {
                    OtherData = dic,
                };
            }
        }

        protected virtual OperationResult DownLoadString(string requestUrl)
        {
            using (var client = new WebClient { Encoding = Encoding.UTF8 })
            {
                var result = client.DownloadString(requestUrl);
                if (string.IsNullOrWhiteSpace(result))
                    return new OperationResult(1, "推送失败");
                var dic = JsonSerializer.DeserializeToDictionary(result);
                if (!dic.ContainsKey("errcode"))
                    return new OperationResult(0, result) { OtherData = dic, };
                return new OperationResult(int.Parse(dic["errcode"].ToString()), dic["errmsg"].ToString())
                {
                    OtherData = dic,
                };
            }
        }

        protected virtual OperationResult DownLoadString<TResult>(string requestUrl)
        {
            using (var client = new WebClient { Encoding = Encoding.UTF8 })
            {
                var json = client.DownloadString(requestUrl);
                var val = JsonSerializer.DeserializeToObject<TResult>(json);
                var result = new OperationResult(0);
                if (val == null)
                    return new OperationResult(1);
                result.OtherData[typeof(TResult).Name] = val;
                return result;
            }
        }

        public override OperationResult ValidateCallResult(HttpContextBase ctx, IDictionary<string, object> data)
        {
            throw new NotImplementedException();
        }

        public override OperationResult SendData<TResult>(IDictionary<string, object> data)
        {
            throw new NotImplementedException();
        }

        protected virtual OperationResult BindParterUser(string openId, long userSysNo)
        {
            //获取合作用户
            var parterUser = ServiceDepository.ParterUserInfoDataAccess.Get(openId, userSysNo);
            //未存在
            if (parterUser == null)
            {
                if (ServiceDepository.UserDataAccess.Get(userSysNo) == null)
                    return new OperationResult(97, "系统未存在绑定用户");

                var result = this.SendData(new Dictionary<string, object> { { "openid", openId } });
                if (!result.Success)
                    return result;
                parterUser = JsonSerializer.DeserializeToObject<ParterUserInfo>(result.ResultMessage);
                parterUser.UserSysNo = userSysNo;
                parterUser.CreateDate = DateTime.Now;
                if (!ServiceDepository.ParterUserInfoDataAccess.Insert(parterUser))
                    return new OperationResult(98, "绑定失败");
            }
            //else
            //{
            //    if (!parterUser.Subscribe)
            //    {
            //        parterUser.Subscribe = true;
            //        if (!ServiceDepository.ParterUserInfoDataAccess.Update(parterUser))
            //            return new OperationResult(99, "更改关注类型失败");
            //    }
            //}
            return new OperationResult(0, parterUser.NickName);       
        }

        /// <summary>
        /// 获取AccessToken
        /// </summary>
        /// <returns></returns>
        protected  string AccessToken
        {
            get 
            {
                string accessToken = string.Empty;
                var result = MongoRepository.GetMongoRepository().FindById<WXAccessToken>(string.Format("{0}{1}", this.Setting.AppID, this.Setting.AppSecret));
                 if (result != null)
                     accessToken = result.AccessToken;
                 return accessToken;
            }
        }
     
    }


}
