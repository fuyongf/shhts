﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.ServiceImpl.External.Config;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;

namespace PinganHouse.SHHTS.ServiceImpl.External
{
    internal class PaySubmitService : PaymentAuthenticatorBase
    {
        public override OperationResult SendData(IDictionary<string, object> data)
        {
            data.Add("sBgUrl", Setting.CallBackUrl);
            return base.SendData(data);
        }
        protected override OperationResult CheckParams(IDictionary<string, object> data)
        {
            if (data == null || data.Count == 0)
                return new OperationResult(-1, "params is null");
            if (!data.ContainsKey("sCaseID"))
                return new OperationResult(-2, "sCaseID is invalid");
            if (!data.ContainsKey("sBusinessOrderID"))
                return new OperationResult(-3, "sBusinessOrderID is invalid");
            if (!data.ContainsKey("sPropertyAddr"))
                return new OperationResult(-4, "sPropertyAddr is null");
            if (!data.ContainsKey("iAmount"))
                return new OperationResult(-5, "iAmount is null");
            if (!data.ContainsKey("iBankID"))
                return new OperationResult(-6, "iBankID is null");
            if (!data.ContainsKey("sBankAccount"))
                return new OperationResult(-7, "sBankAccount is null");
            if (!data.ContainsKey("sBankAccountName"))
                return new OperationResult(-8, "sBankAccountName is null");
            if (!data.ContainsKey("sPayDesc"))
                return new OperationResult(-9, "sPayDesc is null");
            if (!data.ContainsKey("sBgUrl"))
                return new OperationResult(-10, "sBgUrl is null");
            return new OperationResult(0);
        }

        protected override OperationResult CheckCallBackParams(IDictionary<string, object> data)
        {
            if (!data.ContainsKey("sBusinessOrderID") || data["sBusinessOrderID"] == null)
                return new OperationResult(-89, "未提供sBusinessOrderID");
            if (!data.ContainsKey("sFinanceNum"))
                return new OperationResult(-88, "未提供sFinanceNum");
            if (!data.ContainsKey("iPayStatus") || data["iPayStatus"] == null)
                return new OperationResult(-87, "未提供iPayStatus");
            if (!data.ContainsKey("sRejectDesc"))
                return new OperationResult(-86, "未提供sRejectDesc");
            if (!data.ContainsKey("iSuccessTime"))
                return new OperationResult(-85, "未提供iSuccessTime");
            return base.CheckCallBackParams(data);
        }

        public override OperationResult ValidateCallResult(HttpContextBase ctx, IDictionary<string, object> data)
        {
            var result = base.ValidateCallResult(ctx, data);
            if (result.Success)
            {
                result.OtherData.Add("TradeNo", ctx.Request["sBusinessOrderID"]);
                result.OtherData.Add("FinanceNum", ctx.Request["sFinanceNum"]);
                result.OtherData.Add("PayStatus", ctx.Request["iPayStatus"]);
                if (!string.IsNullOrWhiteSpace(ctx.Request["sRejectDesc"]))
                    result.OtherData.Add("RejectDesc", ctx.Request["sRejectDesc"]);
                if (!string.IsNullOrWhiteSpace(ctx.Request["iSuccessTime"]) && !"0".Equals(ctx.Request["iSuccessTime"]))
                    result.OtherData.Add("PaymentTime", ConvertFromUnixTimeStamp(ctx.Request["iSuccessTime"]));
            }
            return result;
        }
    }
}
