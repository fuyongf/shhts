﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

using PinganHouse.SHHTS.DataTransferObjects;

using PinganHouse.SHHTS.ServiceImpl.External.Config;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;

namespace PinganHouse.SHHTS.ServiceImpl.External
{
    internal class PropertyAddressService : PaymentAuthenticatorBase
    {
        public override OperationResult ValidateCallResult(HttpContextBase ctx, IDictionary<string, object> data)
        {
            throw new NotImplementedException();
        }

        protected override OperationResult CheckParams(IDictionary<string, object> data)
        {
            if (data == null || data.Count == 0)
                return new OperationResult(-1, "params is null");
            if (!data.ContainsKey("sCaseID"))
                return new OperationResult(-2, "sCaseID is null");
            if (!data.ContainsKey("sPropertyAddr"))
                return new OperationResult(-3, "sPropertyAddr is null");
            return new OperationResult(0);
        }
    }
}
