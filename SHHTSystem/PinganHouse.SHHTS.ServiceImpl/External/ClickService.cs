﻿using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.DataTransferObjects.External;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.ServiceImpl.External.ClickHandlers;
using PinganHouse.SHHTS.ServiceImpl.External.Config;
using PinganHouse.SHHTS.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace PinganHouse.SHHTS.ServiceImpl.External
{
    internal class ClickService : WeiXinAuthenticationServiceBase<WeiXinSetting>
    {
        public override OperationResult SendData(IDictionary<string, object> data)
        {
            throw new NotImplementedException();
        }
        public override OperationResult ValidateCallResult(HttpContextBase ctx, IDictionary<string, object> data)
        {
            var message = data["WXMessage"] as WXMessage;
            if (message == null || message.Event.Value != EventType.Click)
                return new OperationResult((int)message.Event.Value, "按钮处理器不匹配");
            return this.Handle(message);
        }

        private OperationResult Handle(WXMessage message)
        {
            ClickEventType keyType;
            if (string.IsNullOrWhiteSpace(message.EventKey) || !Enum.TryParse<ClickEventType>(message.EventKey, out keyType))
                return new OperationResult(1, "无法响应该点击事件");
            var partnerUser = ServiceDepository.ParterUserInfoDataAccess.Get(message.FromUserName);
            //如未绑定暂时仅通知用户
            if (partnerUser == null && keyType !=  ClickEventType.Contact)
                return new OperationResult(999);
            return ObjectService.GetObject<IClickHandler>(keyType.ToString()).Handle(message, partnerUser);
        }
    }

}
