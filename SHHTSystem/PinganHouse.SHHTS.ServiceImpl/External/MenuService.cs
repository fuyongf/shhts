﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.ServiceImpl.External.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.ServiceImpl.External
{
    internal class MenuService : WeiXinAuthenticationServiceBase<MenuSetting>
    {
        public override OperationResult SendData(IDictionary<string, object> data)
        {
            var result = CheckParams(data);
            if (!result.Success)
                return result;
            string key = data["UrlKey"].ToString();
            //设置请求url
            string requestUrl = SetRequestUrl(data, key);
            data.Remove("UrlKey");
            if (string.Compare(key, "create") == 0)
                return this.UploadString(data, requestUrl);
            return this.DownLoadString(requestUrl);
        }

        private string SetRequestUrl(IDictionary<string, object> data, string key)
        {
            string requestUrl;
            if (data.ContainsKey("Agentid"))
                requestUrl = string.Format(this.Setting.MenuUrl, key, this.AccessToken, data["Agentid"]);
            else
                requestUrl = string.Format(this.Setting.MenuUrl, key, this.AccessToken);
            return requestUrl;
        }

        protected OperationResult CheckParams(IDictionary<string, object> data)
        {
            if (data == null || data.Count == 0)
                return new OperationResult(1, "params is null");
            if ((!data.ContainsKey("UrlKey")))
                return new OperationResult(2, "UrlKey is null");

            return new OperationResult(0);
        }
    }
}
