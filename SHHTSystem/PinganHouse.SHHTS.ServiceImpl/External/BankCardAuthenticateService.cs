﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.ServiceImpl.External.Config;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Utils.Log;

namespace PinganHouse.SHHTS.ServiceImpl.External
{
    internal class BankCardAuthenticateService : BankCardAuthenticatorBase
    {
        public override OperationResult SendData(IDictionary<string, object> data)
        {
            try
            {
                var result = base.SendData<BankCardAuthResult>(data);
                if (!result.Success)
                    return result;
                var ret = result.OtherData["Data"] as BankCardAuthResult;

                if (ret == null || ret.aAuthResult == null)
                    return new OperationResult(-4, "数据错误");
                if (ret.aAuthResult.iCode != 0)
                    return new OperationResult(-6, ret.aAuthResult.sMsg);
                return new OperationResult(0);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, e.Message);
            }
        }

        public override OperationResult ValidateCallResult(HttpContextBase ctx, IDictionary<string, object> data)
        {
            throw new NotImplementedException();
        }

        protected override OperationResult CheckParams(IDictionary<string, object> data)
        {
            if (data == null || data.Count == 0)
                return new OperationResult(-1, "params is null");
            if (!data.ContainsKey("name") || string.IsNullOrEmpty(data["name"] as string))
                return new OperationResult(-2, "name is invalid");
            if (!data.ContainsKey("idcard") || string.IsNullOrEmpty(data["idcard"] as string))
                return new OperationResult(-3, "idcard is invalid");
            if (!data.ContainsKey("cellphone") || string.IsNullOrEmpty(data["cellphone"] as string))
                return new OperationResult(-4, "cellphone is invalid");
            if (!data.ContainsKey("cardnumber") || string.IsNullOrEmpty(data["cardnumber"] as string))
                return new OperationResult(-5, "cardnumber is invalid");
            if (!data.ContainsKey("bankshort") || string.IsNullOrEmpty(data["bankshort"] as string))
                return new OperationResult(-6, "bankshort is invalid");
            return new OperationResult(0);
        }

        class BankCardAuthResult
        {
            public AuthInfo aAuthResult { get; set; }
        }
        class AuthInfo
        {
            public int iCode { get; set; }
            public string sMsg { get; set; }
        }
    }
}
