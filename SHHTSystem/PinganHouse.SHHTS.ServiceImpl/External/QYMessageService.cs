﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.ServiceImpl.External.Config;
using System.Collections.Generic;

namespace PinganHouse.SHHTS.ServiceImpl.External
{
    internal class QYMessageService : WeiXinAuthenticationServiceBase<QYMessageSetting>
    {

        public override OperationResult SendData(IDictionary<string, object> data)
        {
            var result = CheckParams(data);
            if (!result.Success)
                return result;
            return this.UploadString(data, this.Setting.RequestUrl);
        }
        private OperationResult CheckParams(IDictionary<string, object> data)
        {
            if (data == null || data.Count == 0)
                return new OperationResult(1, "param is invalid");
            if (!data.ContainsKey("touser") && !data.ContainsKey("toparty"))
                return new OperationResult(2, "receiver is null");
            if (!data.ContainsKey("msgtype"))
                return new OperationResult(3, "msgtype is null");
            if (!data.ContainsKey("agentid"))
                return new OperationResult(4, "agentid is null");
            return new OperationResult(0);
        }
    }
}
