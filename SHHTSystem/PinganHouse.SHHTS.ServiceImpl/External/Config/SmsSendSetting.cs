﻿using HTB.DevFx.Config;

namespace PinganHouse.SHHTS.ServiceImpl.External.Config
{
    internal class SmsSendSetting : ConfigSettingElement
    {
        protected override void OnConfigSettingChanged()
        {
            base.OnConfigSettingChanged();
            this.RequestUrl = this.GetRequiredSetting("requestUrl");
        }

        /// <summary>
        /// 请求地址
        /// </summary>
        public string RequestUrl { get; private set; }
    }
}
