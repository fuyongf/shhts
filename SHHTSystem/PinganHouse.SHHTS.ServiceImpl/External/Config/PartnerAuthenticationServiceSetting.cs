﻿using HTB.DevFx.Config;
using PinganHouse.SHHTS.Core.External;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
[assembly: ConfigResource("res://PinganHouse.SHHTS.ServiceImpl.External.Config.htb.devfx.extensions.config", Index = 0)]
namespace PinganHouse.SHHTS.ServiceImpl.External.Config
{
    public class PartnerAuthenticationServiceSetting : ConfigSettingElement
    {
        protected override void OnConfigSettingChanged()
        {
            base.OnConfigSettingChanged();
            this.Apps = this.GetSettings<PartnerApplicationSetting>("apps", null).ToArray();
        }
        public PartnerApplicationSetting[] Apps { get; private set; }
    }

    public class PartnerApplicationSetting : ConfigSettingElement
    {
        protected override void OnConfigSettingChanged()
        {
            base.OnConfigSettingChanged();
            this.AppNo = this.GetRequiredSetting("name");
            this.AuthenticatorType = this.GetRequiredSetting("authenticatorType");
        }

        public string AppNo { get; private set; }
        public string AuthenticatorType { get; private set; }
        public IPartnerAuthenticator Authenticator { get; set; }
    }
}
