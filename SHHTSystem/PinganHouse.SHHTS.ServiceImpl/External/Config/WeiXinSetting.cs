﻿using HTB.DevFx.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.ServiceImpl.External.Config
{
    internal class WeiXinSetting : ConfigSettingElement
    {
        protected override void OnConfigSettingChanged()
        {
            base.OnConfigSettingChanged();
            this.AppID = this.GetRequiredSetting("appID");
            this.AppSecret = this.GetRequiredSetting("appSecret");
        }

        public string AppID { get; private set; }
        public string AppSecret { get; private set; }
    }

    internal class TemplateSetting : WeiXinSetting
    {
        protected override void OnConfigSettingChanged()
        {
            base.OnConfigSettingChanged();
            this.TemplateIdUrl = this.GetRequiredSetting("templateIdUrl");
            this.TemplateUrl = this.GetRequiredSetting("templateUrl");
        }
        public string TemplateIdUrl { get; private set; }
        public string TemplateUrl { get; private set; }
    }
    internal class UserSetting : WeiXinSetting
    {
        protected override void OnConfigSettingChanged()
        {
            base.OnConfigSettingChanged();
            this.UserUrl = this.GetRequiredSetting("userUrl");
        }
        /// <summary>
        /// 用户url
        /// </summary>
        public string UserUrl { get; private set; }
    }

    internal class AccessTokenSetting : WeiXinSetting
    {
        protected override void OnConfigSettingChanged()
        {
            base.OnConfigSettingChanged();
            this.AccessTokenUrl = this.GetRequiredSetting("accessTokenUrl");
        }
        /// <summary>
        /// 获取access_token
        /// </summary>
        public string AccessTokenUrl { get; private set; }
    }

    internal class QRCodeSetting : WeiXinSetting
    {
        protected override void OnConfigSettingChanged()
        {
            base.OnConfigSettingChanged();
            this.QRCodeTempUrl = this.GetRequiredSetting("qrCodeTempUrl");
            this.ShowQRCodeUrl = this.GetRequiredSetting("showQRCodeUrl");
        }
        /// <summary>
        /// 获取临时地址
        /// </summary>
        public string QRCodeTempUrl { get; private set; }

        public string ShowQRCodeUrl { get; private set; }
    }

    internal class MenuSetting : WeiXinSetting
    {
        protected override void OnConfigSettingChanged()
        {
            base.OnConfigSettingChanged();
            this.MenuUrl = this.GetRequiredSetting("menuUrl");
        }
        public string MenuUrl { get; private set; }
    }

    internal class AuthenticationSetting : WeiXinSetting
    {
        protected override void OnConfigSettingChanged()
        {
            base.OnConfigSettingChanged();
            this.AuthorizeUrl = this.GetRequiredSetting("authorizeUrl");
            this.AccessTokenUrl = this.GetRequiredSetting("accessTokenUrl");
            this.UserUrl = this.GetSetting("userUrl");
        }
        public string AuthorizeUrl { get; private set; }
        public string AccessTokenUrl { get; private set; }
        public string UserUrl { get; private set; }
    }

    internal class MaterialSetting : WeiXinSetting
    {
        protected override void OnConfigSettingChanged()
        {
            base.OnConfigSettingChanged();
            this.MaterialUrl = this.GetRequiredSetting("materialUrl");
        }
        public string MaterialUrl { get; private set; }
    }


    internal class QYMessageSetting : WeiXinSetting
    {
        protected override void OnConfigSettingChanged()
        {
            base.OnConfigSettingChanged();
            this.RequestUrl = this.GetRequiredSetting("requestUrl");
        }
        public string RequestUrl { get; private set; }
    }

    internal class CustomMessageSetting : WeiXinSetting
    {
        protected override void OnConfigSettingChanged()
        {
            base.OnConfigSettingChanged();
            this.RequestUrl = this.GetRequiredSetting("requestUrl");
        }
        public string RequestUrl { get; private set; }
    }

    internal class DepartmentSetting : WeiXinSetting
    {
        protected override void OnConfigSettingChanged()
        {
            base.OnConfigSettingChanged();
            this.UpdateDepartmentUrl = this.GetRequiredSetting("departmentUrl");
            this.DeleteDepartmentUrl = this.GetRequiredSetting("deleteUrl");
            this.QueryDepartmentUrl = this.GetRequiredSetting("queryUrl");
        }
        public string UpdateDepartmentUrl { get; private set; }
        public string DeleteDepartmentUrl { get; private set; }
        public string QueryDepartmentUrl { get; private set; }
    }

    internal class QYUserSetting : WeiXinSetting
    {
        protected override void OnConfigSettingChanged()
        {
            base.OnConfigSettingChanged();
            this.UpdateUserUrl = this.GetRequiredSetting("userUrl");
            this.DeleteUserUrl = this.GetRequiredSetting("deleteUrl");
            this.QueryUserUrl = this.GetRequiredSetting("queryUrl");
            this.QueryDepartmentUsersUrl = this.GetRequiredSetting("queryDepartmentUsersUrl");
            this.InviteUrl = this.GetRequiredSetting("inviteUrl");
        }
        /// <summary>
        /// 新增、更改及批量地址
        /// </summary>
        public string UpdateUserUrl { get; private set; }
        /// <summary>
        /// 删除地址
        /// </summary>
        public string DeleteUserUrl { get; private set; }
        /// <summary>
        /// 查询单个用户地址
        /// </summary>
        public string QueryUserUrl { get; private set; }

        /// <summary>
        /// 获取部门用户
        /// </summary>
        public string QueryDepartmentUsersUrl { get; private set; }

        /// <summary>
        /// 邀请用户关注url
        /// </summary>
        public string InviteUrl { get; private set; }
    }
}
