﻿using HTB.DevFx.Config;

using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.ServiceImpl.External.Config
{
    internal class PaymentSetting : ConfigSettingElement
    {
        protected override void OnConfigSettingChanged()
        {
            base.OnConfigSettingChanged();
            this.RequestUrl = this.GetRequiredSetting("requestUrl");
            this.SecurityKey = this.GetRequiredSetting("securityKey");
            this.CallBackUrl = EncryptHelper.GetBase64(this.GetSetting("callBackUrl"));
        }

        /// <summary>
        /// 请求地址
        /// </summary>
        public string RequestUrl { get; private set; }

        /// <summary>
        /// 签名密钥
        /// </summary>
        public string SecurityKey { get; private set; }

        /// <summary>
        /// 回调URL
        /// </summary>
        public string CallBackUrl { get; private set; }
    }

    internal class PaySubmitSetting : PaymentSetting
    { 
    }

    internal class PaidOffSubmitSetting : PaymentSetting
    {
    }

    internal class CollectionPushSetting : PaymentSetting
    {
    }

    internal class CancelTradeSetting : PaymentSetting
    {
    }

    internal class PropertyAddressSetting : PaymentSetting
    {
    }
    internal class ReceiptPushSetting : PaymentSetting
    {
    }
    internal class BankSyncSetting : PaymentSetting
    {
    }
    internal class MissCollectionReceiveSetting : PaymentSetting
    {
    }
}
