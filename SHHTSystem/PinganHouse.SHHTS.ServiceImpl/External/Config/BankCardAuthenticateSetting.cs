﻿using HTB.DevFx.Config;

namespace PinganHouse.SHHTS.ServiceImpl.External.Config
{
    internal class BankCardAuthenticateSetting : ConfigSettingElement
    {
        protected override void OnConfigSettingChanged()
        {
            base.OnConfigSettingChanged();
            this.RequestUrl = this.GetRequiredSetting("requestUrl");
            this.SecurityKey = this.GetRequiredSetting("securityKey");
        }

        /// <summary>
        /// 请求地址
        /// </summary>
        public string RequestUrl { get; private set; }

        /// <summary>
        /// 签名密钥
        /// </summary>
        public string SecurityKey { get; private set; }
    }

    internal class IdentifyBankSyncSetting : BankCardAuthenticateSetting
    {
    }
}
