﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using HTB.DevFx.Core;

using PinganHouse.SHHTS.Core.External;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.ServiceImpl.External
{
    internal abstract class PartnerAuthenticatorBase<TSetting> : ServiceBase<TSetting>, IPartnerAuthenticator
    {
        public virtual void Init(IPartnerAuthenticationService service)
        {
            base.OnInit();
        }


        public abstract OperationResult SendData(IDictionary<string, object> data);

        public abstract OperationResult SendData<TResult>(IDictionary<string, object> data);

        public abstract OperationResult ValidateCallResult(HttpContextBase ctx, IDictionary<string, object> data);

        protected virtual string BuildParameters(IDictionary<string, object> parameters)
        {
            if (parameters == null || parameters.Count == 0)
                return null;
            StringBuilder str = new StringBuilder();
            foreach (var item in parameters)
            {
                str.AppendFormat("{0}={1}&", item.Key, item.Value);
            }
            return str.ToString().TrimEnd('&');
        }

        protected virtual IDictionary<string, object> BuildParameters(HttpRequestBase request)
        {
            IDictionary<string, object> result = new Dictionary<string, object>();
            if (request.QueryString.Count > 0)
            {
                foreach (var item in request.QueryString.Keys)
                {
                    string key = item.ToString();
                    if (!result.ContainsKey(key))
                        result.Add(key, request.QueryString[key]);
                }
            }
            if (request.Form.Count > 0)
            {
                foreach (var item in request.Form.Keys)
                {
                    string key = item.ToString();
                    if (!result.ContainsKey(key))
                        result.Add(key, request.Form[key]);
                }
            }
            return result;
        }
        protected int UnixTimeStamp
        {
            get
            {
                return TimeStamp.Get();
            }
        }

    }
}
