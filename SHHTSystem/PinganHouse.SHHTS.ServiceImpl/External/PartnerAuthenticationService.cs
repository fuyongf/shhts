﻿using HTB.DevFx.Core;
using PinganHouse.SHHTS.Core.External;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.ServiceImpl.External.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace PinganHouse.SHHTS.ServiceImpl.External
{
    public class PartnerAuthenticationService : ServiceBase<PartnerAuthenticationServiceSetting>, IPartnerAuthenticationService
    {
        protected Dictionary<string, PartnerApplicationSetting> Authenticators { get; private set; }
        protected override void OnInit()
        {
            base.OnInit();
            var apps = this.Setting.Apps;
            this.Authenticators = new Dictionary<string, PartnerApplicationSetting>();
            if (apps != null && apps.Length > 0)
            {
                foreach (var app in apps)
                {
                    app.Authenticator = this.ObjectService.GetObject<IPartnerAuthenticator>(app.AuthenticatorType);
                    if (app.Authenticator == null)
                    {
                        continue;
                    }
                    app.Authenticator.Init(this);
                    this.Authenticators.Add(app.AppNo, app);
                }
            }
        }

        public OperationResult SendData(string appNo, IDictionary<string, object> data)
        {
            PartnerApplicationSetting appSetting;
            if (!this.Authenticators.TryGetValue(appNo, out appSetting))
            {
                return new OperationResult(-1, "应用未找到");
            }
            return appSetting.Authenticator.SendData(data);
        }

        public OperationResult SendData<TResult>(string appNo, IDictionary<string, object> data)
        {
            PartnerApplicationSetting appSetting;
            if (!this.Authenticators.TryGetValue(appNo, out appSetting))
            {
                return new OperationResult(-1, "应用未找到");
            }
            return appSetting.Authenticator.SendData<TResult>(data);
        }

        public OperationResult ValidateCallResult(string appNo, HttpContextBase ctx, IDictionary<string, object> data)
        {
            PartnerApplicationSetting appSetting;
            if (!this.Authenticators.TryGetValue(appNo, out appSetting))
            {
                return new OperationResult(-1, "应用未找到");
            }
            return appSetting.Authenticator.ValidateCallResult(ctx, data);
        }
    }
}
