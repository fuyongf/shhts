﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

using PinganHouse.SHHTS.DataTransferObjects;

using PinganHouse.SHHTS.ServiceImpl.External.Config;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;

namespace PinganHouse.SHHTS.ServiceImpl.External
{
    internal class MissCollectionReceiveService : PaymentAuthenticatorBase
    {
        public override OperationResult SendData(IDictionary<string, object> data)
        {
            throw new NotImplementedException();
        }

        public override OperationResult SendData<TResult>(IDictionary<string, object> data)
        {
            throw new NotImplementedException();
        }

        protected override OperationResult CheckParams(IDictionary<string, object> data)
        {
            throw new NotImplementedException();
        }

        protected override OperationResult CheckCallBackParams(IDictionary<string, object> data)
        {
            if (!data.ContainsKey("sBankSerial") || data["sBankSerial"] == null)
                return new OperationResult(-89, "未提供sBankSerial");
            if (!data.ContainsKey("iAmount") || data["iAmount"] == null)
                return new OperationResult(-88, "未提供iAmount");
            if (!data.ContainsKey("iTradeTime") || data["iTradeTime"] == null)
                return new OperationResult(-87, "未提供iTradeTime");
            if (!data.ContainsKey("sPayerCardID") || data["sPayerCardID"] == null)
                return new OperationResult(-86, "未提供sPayerCardID");
            if (!data.ContainsKey("sAccountName") || data["sAccountName"] == null)
                return new OperationResult(-85, "未提供sAccountName");
            if (!data.ContainsKey("iThirdBankID") || data["iThirdBankID"] == null)
                return new OperationResult(-84, "未提供iThirdBankID");
            if (!data.ContainsKey("iPayType") || data["iPayType"] == null)
                return new OperationResult(-83, "未提供iPayType");
            return base.CheckCallBackParams(data);
        }

        public override OperationResult ValidateCallResult(HttpContextBase ctx, IDictionary<string, object> data)
        {
            var result = base.ValidateCallResult(ctx, data);
            if (result.Success)
            {
                result.OtherData.Add("SerialNo", ctx.Request["sBankSerial"]);
                result.OtherData.Add("Amount", long.Parse(ctx.Request["iAmount"]) / 100m);
                result.OtherData.Add("PaymentTime", ConvertFromUnixTimeStamp(ctx.Request["iTradeTime"]));
                result.OtherData.Add("AccountNo", ctx.Request["sPayerCardID"]);
                result.OtherData.Add("AccountName", ctx.Request["sAccountName"]);
                result.OtherData.Add("BankId", ctx.Request["iThirdBankID"]);
                result.OtherData.Add("PaymentType", int.Parse(ctx.Request["iPayType"]) - 1);
            }
            return result;
        }
    }
}
