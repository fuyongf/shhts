﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace PinganHouse.SHHTS.ServiceImpl.External
{
    class FingerExternalService
    {
        /// <summary>
        /// 十六进制格式的指纹模板/特征数据转Base64格式
        /// </summary>
        /// <param name="psSrcBuf">待转换数据数据</param>
        /// <param name="iSrcLen">待转换数据长度</param>
        /// <param name="psDesBuf">转换后数据</param>
        /// <param name="piDestLen">转换后数据长度</param>
        /// <param name="lpImageHeight">图像高度</param>
        /// <param name="lpImageLen">图像数据长度</param>
        /// <returns>返回 0：表示成功；返回-1：表示失败；</returns>
        [DllImport("JZT30Dev.dll")]
        public static extern int FPIHexFingerDataToBase64(byte[] psSrcBuf, int iSrcLen, byte[] psDesBuf, ref int piDestLen);

        /// <summary>
        /// Base64格式的指纹模板/特征数据转十六进制格式
        /// </summary>
        /// <param name="psSrcBuf">待转换数据数据</param>
        /// <param name="iSrcLen">待转换数据长度</param>
        /// <param name="psDesBuf">转换后数据</param>
        /// <param name="piDestLen">转换后数据长度</param>
        /// <param name="lpImageHeight">图像高度</param>
        /// <param name="lpImageLen">图像数据长度</param>
        /// <returns>返回 0：表示成功；返回-1：表示失败；</returns>
        [DllImport("JZT30Dev.dll")]
        public static extern int FPIBase64FingerDataToHex(byte[] psSrcBuf, int iSrcLen, byte[] psDesBuf, ref int piDestLen);

        /// <summary>
        /// 指纹比对
        /// </summary>
        /// <param name="psMB">指纹模板数据</param>
        /// <param name="psTZ">指纹特征数据</param>
        /// <param name="iLevel">安全等级1~5,越高越严,默认为3</param>
        /// <returns> 0 -- 成功，0--失败</returns>
        [DllImport("JZTAlg30Dll.dll")]
        public static extern int FPIMatch(byte[] psMB, byte[] psTZ, int iLevel);

    }
}
