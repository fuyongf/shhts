﻿using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.DataTransferObjects.External;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.ServiceImpl.External.ClickHandlers;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.ServiceImpl.External.WXClickHandlers
{
    /// <summary>
    /// 案件进度按钮处理器
    /// </summary>
    internal class CaseProcessHandler : IClickHandler
    {
        public OperationResult Handle(WXMessage message, ParterUserInfo user)
        {
            var customers = ServiceDepository.CustomerDataAccess.GetByUserSysNo(user.UserSysNo.Value);
            if (customers == null || customers.Count == 0)
                return new OperationResult(999, "绑定用户不存在");
            var settings = ServiceDepository.SettingDataAccess.GetItemsByPath("/System/Parter/WeiXin/ReplyMessages/Text/CaseProcess");
            if (settings == null || settings.Count < 2)
                throw new ArgumentException("/System/Parter/WeiXin/ReplyMessages/Text/CaseProcess");
            //var categories = new List<CaseStatus>();
          
            //categories.Add(CaseStatus.ZB3 | CaseStatus.HD1 | CaseStatus.HD2 | CaseStatus.HD3
            //| CaseStatus.HD4 | CaseStatus.HD5 | CaseStatus.HD6 | CaseStatus.ZB12);
            //categories.Add(CaseStatus.ZB1 |
            //                        CaseStatus.JY1 |
            //                        CaseStatus.JY2 |
            //                        CaseStatus.JY3 |
            //                        CaseStatus.JY4 |
            //                        CaseStatus.JY6 |
            //                        CaseStatus.JY7 |
            //                        CaseStatus.JY8 |
            //                        CaseStatus.JY9 |
            //                        CaseStatus.JY10 |
            //                        CaseStatus.JY11 |
            //                        CaseStatus.JY12 |
            //                        CaseStatus.JY13 |
            //                        CaseStatus.ZB6 |
            //                        CaseStatus.ZB7 | CaseStatus.ZB13);
            //categories.Add(CaseStatus.DK1 | CaseStatus.DK2 | CaseStatus.DK3 | CaseStatus.DK4 | CaseStatus.ZB11
            //| CaseStatus.DK5 | CaseStatus.DK6 | CaseStatus.DK7 | CaseStatus.DK8 | CaseStatus.DK9 | CaseStatus.DK10
            //| CaseStatus.JY5 | CaseStatus.ZB2 | CaseStatus.ZB7);
            //categories.Add(CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2
            //   | CaseStatus.ZB2 | CaseStatus.ZB5 | CaseStatus.ZB6 | CaseStatus.ZB7);
            var dic = new Dictionary<string, string>();
            StringBuilder content = new StringBuilder();
            string tail = "\n\r--------------------------\n\r";
            for (var index = 0; index <customers.Count;index++)
            {
                var caseInfo = ServiceDepository.CaseDataAccess.GetCaseByUserSysNo(customers[index].SysNo);
                if (caseInfo == null)
                    continue;
                if (!dic.ContainsKey(caseInfo.CaseId))
                {
                    //StringBuilder sb = new StringBuilder();
                    //for (var i = 0; i < categories.Count; i++)
                    //{
                    //    var ce = ServiceDepository.CaseEventDataAccess.GetNewByCaseId(caseInfo.CaseId, categories[i]);
                    //    if (ce != null)
                    //    {
                    //        if (sb.Length != 0 && categories.Count == (i + 1))
                    //            continue;
                    //        var de = ce.EventType.GetDescription();
                    //        if (sb.ToString().Contains(de))
                    //            continue;
                    //        sb.AppendFormat("{0}、", de);
                    //    }
                    //}
                    dic[caseInfo.CaseId] = caseInfo.CaseStatus.GetDescription();//sb.ToString().TrimEnd('、');
                    content.AppendFormat(settings[1].Value.Replace(Environment.NewLine, "\n\r"),
                        string.Format("{0}({1})", caseInfo.CaseId, caseInfo.TenementAddress), dic[caseInfo.CaseId], caseInfo.CaseId);
                    content.Append(tail);
                }
                //微信content长度限制2048 
                if (content.Length > 2048)
                {
                    content.Clear();
                    content.Append("您的交易记录有点多， 请联系工作人员查询");
                    break;
                }
            }
            if (content.Length == 0)
                content.Append("您未存在交易记录");
            return new OperationResult(0, string.Format(settings[0].Value, message.FromUserName, message.ToUserName, TimeStamp.Get(), content.ToString().TrimEnd(tail)));  

        }
    }
}
