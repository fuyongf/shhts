﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.DataTransferObjects.External;
using PinganHouse.SHHTS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.ServiceImpl.External.ClickHandlers
{
    public interface IClickHandler
    {
        /// <summary>
        /// 处理微信案件点击事件
        /// </summary>
        /// <param name="message"></param>
        /// <param name="users"></param>
        /// <returns></returns>
        OperationResult Handle(WXMessage message, ParterUserInfo user);
    }
}
