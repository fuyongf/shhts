﻿using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.DataTransferObjects.External;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.ServiceImpl.External.ClickHandlers;
using PinganHouse.SHHTS.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.ServiceImpl.External.WXClickHandlers
{
    internal class ContactHandler : IClickHandler
    {
        public OperationResult Handle(WXMessage message, ParterUserInfo users)
        {
            var setting = ServiceDepository.SettingService.GetSettingItemValueByPath("/System/Parter/WeiXin/ReplyMessages/Text/Contact");
            if (string.IsNullOrWhiteSpace(setting))
                return new OperationResult(1, "联系方式未配置");
            return new OperationResult(0, string.Format(setting, message.FromUserName, message.ToUserName, TimeStamp.Get()));
        }
    }
}
