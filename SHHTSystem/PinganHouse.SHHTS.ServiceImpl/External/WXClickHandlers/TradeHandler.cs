﻿using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.DataTransferObjects.External;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.ServiceImpl.External.ClickHandlers;
using PinganHouse.SHHTS.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.ServiceImpl.External.WXClickHandlers
{
    /// <summary>
    /// 需求变更
    /// 已废弃
    /// </summary>
    internal class TradeHandler : IClickHandler
    {

        public OperationResult Handle(WXMessage message, ParterUserInfo user)
        {
            var customers = ServiceDepository.CustomerDataAccess.GetByUserSysNo(user.UserSysNo.Value);
            if (customers == null || customers.Count == 0)
                return new OperationResult(1, "绑定用户不存在");
            var settings = ServiceDepository.SettingDataAccess.GetItemsByPath("/System/Parter/WeiXin/ReplyMessages/Text/Trade");
            if (settings == null || settings.Count < 3)
                throw new ArgumentException("未存在/System/Parter/WeiXin/ReplyMessages/Text/Trade节点");
            var dic = new Dictionary<string, string>();
            StringBuilder content = new StringBuilder();
            int totalCount;
            foreach (var customer in customers)
            {
                var relation = ServiceDepository.CaseDataAccess.GetRelation(customer.SysNo);
                if (relation == null)
                    continue;
                if (!dic.ContainsKey(relation.CaseId))
                {
                    var accountType =(AccountType)(int)relation.CaseUserType;
                    var trades = ServiceDepository.TradeDataAccess.GetByCase(1, 1000, out totalCount, relation.CaseId, accountType, null);
                    if (trades == null || trades.Count == 0)
                        continue;
                    StringBuilder sb = new StringBuilder();
                    foreach (var trade in trades)
                    {
                        if (trade.ReconciliationStatus != ReconciliationStatus.Success || trade.Status != TradeStatus.Completed)
                            continue;
                        sb.AppendLine(string.Format(settings[1].Value, trade.CreateDate.ToString("yyyy.MM.dd HH:mm"), trade.OrderBizType.GetDescription(),Math.Abs(trade.Amount)));
                    }
                    if (sb.Length == 0)
                        continue;
                    dic[relation.CaseId] = sb.ToString();
                    content.AppendLine(dic[relation.CaseId]);
                    content.AppendLine(string.Format(settings[2].Value, relation.CaseId, (int)accountType));
                }
            }
            if (content.Length == 0)
                content.Append("未产生相关交易流水");
            return new OperationResult(0, string.Format(settings[0].Value, message.FromUserName, message.ToUserName, TimeStamp.Get(), content)); 
        }
    }
}
