﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.ServiceImpl.External.Config;
using PinganHouse.SHHTS.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.ServiceImpl.External
{
    internal class DepartmentService : WeiXinAuthenticationServiceBase<DepartmentSetting>
    {
        public override OperationResult SendData(IDictionary<string, object> data)
        {
            string key = data["UrlKey"].ToString();
            data.Remove("UrlKey");
            if (string.Compare(key, "delete", true) != 0)
                return this.UploadString(data, string.Format(this.Setting.UpdateDepartmentUrl, key, this.AccessToken));
            return this.DownLoadString(string.Format(this.Setting.DeleteDepartmentUrl, this.AccessToken, data["id"]));
        }

        public override OperationResult SendData<TResult>(IDictionary<string, object> data)
        {
            var result = this.DownLoadString(string.Format(this.Setting.QueryDepartmentUrl, this.AccessToken, data["id"]));
            if (result.Success)
            {
                var ds = JsonSerializer.DeserializeToObject<List<TResult>>(result.OtherData["department"].ToString());
                result.OtherData[typeof(TResult).Name] = ds;
            }
            return result;
        }
    }
}
