﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.ServiceImpl.External.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace PinganHouse.SHHTS.ServiceImpl.External
{
    internal class AuthorizationService : WeiXinAuthenticationServiceBase<AuthenticationSetting>
    {
        /// <summary>
        /// 获取授权地址
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public override OperationResult SendData(IDictionary<string, object> data)
        {
            if (data == null || data.Count == 0)
                return new OperationResult(1, "data is null");
            if (!data.ContainsKey("redirect_uri") || string.IsNullOrWhiteSpace(data["redirect_uri"].ToString()))
                return new OperationResult(2, "redireceUrl is null");
            data["redirect_uri"] = HttpUtility.UrlEncode(data["redirect_uri"].ToString());
            var requestUrl = string.Format(this.Setting.AuthorizeUrl, this.Setting.AppID, data["redirect_uri"], data["state"]);
            return new OperationResult(0, requestUrl);
        }

        public override OperationResult ValidateCallResult(HttpContextBase ctx, IDictionary<string, object> data)
        {
            if (data == null || data.Count == 0)
                return new OperationResult(1, "data is null");
            if (!data.ContainsKey("code") || string.IsNullOrWhiteSpace(data["code"].ToString()))
                return new OperationResult(2, "code is null");
   
            //企业号
            if (data.ContainsKey("agentid"))
                return this.DownLoadString(string.Format(this.Setting.AccessTokenUrl, this.AccessToken, data["code"], data["agentid"]));
            //服务号
            var requestUrl = string.Format(this.Setting.AccessTokenUrl, this.Setting.AppID, this.Setting.AppSecret, data["code"]);
            var result = this.DownLoadString(requestUrl);
            if (result.Success)
                result= this.DownLoadString(string.Format(this.Setting.UserUrl, result.OtherData["access_token"], result.OtherData["openid"]));               
            return result;          
        }
    }
}
