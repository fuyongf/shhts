﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.ServiceImpl.External.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.ServiceImpl.External
{
    internal class CustomMessageService : WeiXinAuthenticationServiceBase<CustomMessageSetting>
    {

        public override OperationResult SendData(IDictionary<string, object> data)
        {
            if (data == null)
                return new OperationResult(1, "data is null");
            if (!data.ContainsKey("msgtype"))
                return new OperationResult(2, "messageType is null");
            if (!data.ContainsKey("touser"))
                return new OperationResult(3, "recipients is null");
            return this.UploadString(data, this.Setting.RequestUrl);
        }
    }
}
