﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using PinganHouse.SHHTS.DataTransferObjects;

using PinganHouse.SHHTS.ServiceImpl.External.Config;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;

namespace PinganHouse.SHHTS.ServiceImpl.External
{
    internal class IdentifyBankSyncService : BankCardAuthenticatorBase
    {
        public override OperationResult SendData<TResult>(IDictionary<string, object> data)
        {
            try
            {
                var result = base.SendData<IdentifyBankList<TResult>>(data);
                if (!result.Success)
                    return result;
                var ret = result.OtherData["Data"] as IdentifyBankList<TResult>;

                if (ret == null || ret.aBankList == null)
                    return new OperationResult(-4, "数据错误");

                var success = new OperationResult(0);
                success.OtherData.Add("Data", ret.aBankList);
                return success;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, e.Message);
            }
        }

        public override OperationResult ValidateCallResult(HttpContextBase ctx, IDictionary<string, object> data)
        {
            throw new NotImplementedException();
        }

        protected override OperationResult CheckParams(IDictionary<string, object> data)
        {
            if (data == null || data.Count == 0)
                return new OperationResult(-1, "params is null");
            if (!data.ContainsKey("type"))
                return new OperationResult(-2, "type is invalid");
            return new OperationResult(0);
        }
    }
    class IdentifyBankList<T>
    {
        public IList<T> aBankList { get; set; }
    }
}
