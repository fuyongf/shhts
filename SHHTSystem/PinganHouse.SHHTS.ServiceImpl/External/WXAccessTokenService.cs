﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.DataTransferObjects.External;
using PinganHouse.SHHTS.ServiceImpl.External.Config;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using PinganHouse.SHHTS.DataAccess.MongoRepository;

namespace PinganHouse.SHHTS.ServiceImpl.External
{
    internal class WXAccessTokenService : WeiXinAuthenticationServiceBase<AccessTokenSetting>
    {

        public override OperationResult SendData(IDictionary<string, object> data)
        {
            using (var client = new WebClient { Encoding = Encoding.UTF8 })
            {
                var result = client.DownloadString(string.Format(this.Setting.AccessTokenUrl, this.Setting.AppID, this.Setting.AppSecret));
                if (string.IsNullOrWhiteSpace(result))
                    return new OperationResult(1, "获取accessToken失败");
                var dic = JsonSerializer.DeserializeToDictionary(result);
                if (dic.ContainsKey("errcode") && string.Compare(dic["errcode"].ToString(), "0", true)!=0)
                    return new OperationResult(int.Parse(dic["errcode"].ToString()), dic["errmsg"].ToString());

                var accessToken = new WXAccessToken
                {
                    AccessToken = dic["access_token"].ToString(),
                    Id = string.Format("{0}{1}", this.Setting.AppID, this.Setting.AppSecret),
                    ExpireTime = 7200,
                    RefreshTime = DateTime.Now,
                };

                object expireTime;
                if (dic.TryGetValue("expires_in", out expireTime))
                    accessToken.ExpireTime = int.Parse(expireTime.ToString());
                if (MongoRepository.GetMongoRepository().Save<WXAccessToken>(accessToken))
                    return new OperationResult(0){ OtherData = dic, };     
                return new OperationResult(1, "access_token更新失败");            
            }                    
        }
    }
}
