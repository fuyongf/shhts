﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

using PinganHouse.SHHTS.DataTransferObjects;

using PinganHouse.SHHTS.ServiceImpl.External.Config;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;

namespace PinganHouse.SHHTS.ServiceImpl.External
{
    internal class CollectionPushService : PaymentAuthenticatorBase
    {
        protected override OperationResult CheckParams(IDictionary<string, object> data)
        {
            if (data == null || data.Count == 0)
                return new OperationResult(-1, "params is null");
            if (!data.ContainsKey("sBusinessID"))
                return new OperationResult(-2, "sBusinessID is invalid");
            if (!data.ContainsKey("sBankSerialNum"))
                return new OperationResult(-3, "sBankSerialNum is invalid");
            if (!data.ContainsKey("sCaseID"))
                return new OperationResult(-4, "sCaseID is null");
            if (!data.ContainsKey("sOrderID"))
                return new OperationResult(-5, "sOrderID is null");
            if (!data.ContainsKey("iOrderAmount"))
                return new OperationResult(-6, "iOrderAmount is null");
            if (!data.ContainsKey("iPayType"))
                return new OperationResult(-7, "iPayType is null");
            if (!data.ContainsKey("sPayee"))
                return new OperationResult(-8, "sPayee is null");
            if (!data.ContainsKey("iPayAmount"))
                return new OperationResult(-9, "iPayAmount is null");
            if (!data.ContainsKey("sBank"))
                return new OperationResult(-10, "sBank is null");
            if (!data.ContainsKey("iTradeDate"))
                return new OperationResult(-11, "iTradeDate is null");
            if (!data.ContainsKey("sPosID"))
                return new OperationResult(-13, "sPosID is null");
            if (!data.ContainsKey("sStoreID"))
                return new OperationResult(-14, "sStoreID is null");
            if (!data.ContainsKey("sPropertyAddr"))
                return new OperationResult(-15, "sPropertyAddr is null");
            return new OperationResult(0);
        }

        protected override OperationResult CheckCallBackParams(IDictionary<string, object> data)
        {
            if (!data.ContainsKey("sBusinessOrderID") || data["sBusinessOrderID"] == null)
                return new OperationResult(-89, "未提供sBusinessOrderID");
            if (!data.ContainsKey("iReconcileStatus") || data["iReconcileStatus"] == null)
                return new OperationResult(-88, "未提供iReconcileStatus");
            if (!data.ContainsKey("iAmount") || data["iAmount"] == null)
                return new OperationResult(-87, "未提供iAmount");
            if (!data.ContainsKey("sBankSerialNum") || data["sBankSerialNum"] == null)
                return new OperationResult(-86, "未提供sBankSerialNum");
            if (!data.ContainsKey("iTradeDate") || data["iTradeDate"] == null)
                return new OperationResult(-85, "未提供iTradeDate");
            return base.CheckCallBackParams(data);
        }

        public override OperationResult ValidateCallResult(HttpContextBase ctx, IDictionary<string, object> data)
        {
            var result = base.ValidateCallResult(ctx, data);
            if (result.Success)
            {
                result.OtherData.Add("TradeNo", ctx.Request["sBusinessOrderID"]);
                result.OtherData.Add("ReconcileStatus", ctx.Request["iReconcileStatus"]);
                result.OtherData.Add("Amount", long.Parse(ctx.Request["iAmount"]) / 100m);
                result.OtherData.Add("SerialNo", ctx.Request["sBankSerialNum"]);
                result.OtherData.Add("TradeDate", DateTime.ParseExact(ctx.Request["iTradeDate"], "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture));
            }
            return result;
        }
    }
}
