﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using PinganHouse.SHHTS.DataTransferObjects;

using PinganHouse.SHHTS.ServiceImpl.External.Config;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.ServiceImpl.External
{
    internal class SmsSendSevice : PartnerAuthenticatorBase<SmsSendSetting>
    {
        public override OperationResult SendData(IDictionary<string, object> data)
        {
            var check = CheckParams(data);
            if (!check.Success)
                return check;
            var response = HttpUtils.SendPostRequest(this.Setting.RequestUrl, BuildParameters(data), 5000, Encoding.UTF8, Encoding.UTF8, null);
            if (string.IsNullOrEmpty(response))
                return new OperationResult(-1, "请求响应错误");
            var result = JsonSerializer.DeserializeToObject<SmsSendResult>(response);
            if (result.iStatus != 1)
                return new OperationResult(-2, result.aErrInfo == null ? "发送失败" : result.aErrInfo.errorMsg);
            return new OperationResult(0);
        }

        public override OperationResult SendData<TResult>(IDictionary<string, object> data)
        {
            throw new NotImplementedException();
        }

        public override OperationResult ValidateCallResult(HttpContextBase ctx, IDictionary<string, object> data)
        {
            throw new NotImplementedException();
        }

        protected OperationResult CheckParams(IDictionary<string, object> data)
        {
            if (data == null || data.Count == 0)
                return new OperationResult(-1, "params is null");
            if (!data.ContainsKey("bcode") || string.IsNullOrEmpty(data["bcode"] as string))
                return new OperationResult(-2, "bcode is invalid");
            if (!data.ContainsKey("clientip") || string.IsNullOrEmpty(data["clientip"] as string))
                return new OperationResult(-3, "clientip is invalid");
            if (!data.ContainsKey("clientid") || string.IsNullOrEmpty(data["clientid"] as string))
                return new OperationResult(-4, "clientid is invalid");
            if (!data.ContainsKey("mobile") || string.IsNullOrEmpty(data["mobile"] as string))
                return new OperationResult(-5, "mobile is invalid");
            return new OperationResult(0);
        }

        internal class SmsSendResult
        {
            public int iStatus { get; set; }

            public SmsSendErrorInfo aErrInfo { get; set; }

            public SmsSendResultData aData { get; set; }
        }

        internal class SmsSendErrorInfo
        {
            public string errorMsg { get; set; }
        }
        internal class SmsSendResultData
        {
            public string msg { get; set; }
        }
    }
}
