﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.ServiceImpl.External.Config;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using System;
using System.Collections.Generic;

namespace PinganHouse.SHHTS.ServiceImpl.External
{
    internal class QYUserService : WeiXinAuthenticationServiceBase<QYUserSetting>
    {
        public override OperationResult SendData(IDictionary<string, object> data)
        {
            string key = data["UrlKey"].ToString();
            data.Remove("UrlKey");
            string requestUrl = string.Format(this.Setting.UpdateUserUrl, key, this.AccessToken);
            if (string.Compare(key, "invite", true) == 0)
            {
                requestUrl = string.Format(this.Setting.InviteUrl, this.AccessToken);                
            }
            else if (string.Compare(key, "delete", true) == 0)
            {
                requestUrl = string.Format(this.Setting.DeleteUserUrl, this.AccessToken, data["userid"]);
                return this.DownLoadString(requestUrl);
            }
            return this.UploadString(data, requestUrl);
            
        }

        public override OperationResult SendData<TResult>(IDictionary<string, object> data)
        {
            try
            {
                if (string.Compare(data["UrlKey"].ToString(), "get", true) == 0)
                    return this.DownLoadString<TResult>(string.Format(this.Setting.QueryUserUrl, this.AccessToken, data["userid"]));
                var result = this.DownLoadString(string.Format(this.Setting.QueryDepartmentUsersUrl, this.AccessToken, data["department_id"], data["fetch_child"], data["status"]));
                if (result.Success)
                    result.OtherData[typeof(TResult).Name] = JsonSerializer.DeserializeToObject<List<TResult>>(result.OtherData["userlist"].ToString());
                return result;               
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-1, "获取失败");
            }
        }
    }
}
