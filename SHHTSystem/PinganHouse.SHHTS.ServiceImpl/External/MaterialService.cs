﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.ServiceImpl.External.Config;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace PinganHouse.SHHTS.ServiceImpl.External
{
    internal class MaterialService : WeiXinAuthenticationServiceBase<MaterialSetting>
    {
        public override OperationResult SendData(IDictionary<string, object> data)
        {
            string requestUrl = string.Format(this.Setting.MaterialUrl, this.AccessToken, data["type"]);
            return Upload(requestUrl, data);
        }
        private OperationResult CheckParams(IDictionary<string, object> data)
        {
            if (data == null || data.Count == 0)
                return new OperationResult(1, "param is invalid");
            if (!data.ContainsKey("type"))
                return new OperationResult(2, "type is null");
            if (!data.ContainsKey("offset"))
                return new OperationResult(3, "offset is null");
            if (!data.ContainsKey("count"))
                return new OperationResult(4, "count is null");
            return new OperationResult(0);
        }

        /// <summary>
        /// 上传图片
        /// </summary>
        /// <param name="requestUrl"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public OperationResult Upload(string requestUrl, IDictionary<string, object> data)
        {           
            string boundary = DateTime.Now.Ticks.ToString("x");
            byte[] responseBytes;
            var encoding = Encoding.UTF8;
            try
            {
                var fileBytes = (byte[])data["file"];
                using (WebClient webClient = new WebClient())
                {
                    webClient.Headers.Add("Content-Type", string.Format("multipart/form-data; boundary={0}", boundary));
                    
                    string end = "\r\n";
                    string httpRowData = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"{1}\"; filename=\"{2}\"\r\nContent-Type: application/octet-stream\r\n\r\n", boundary, data["name"], data["filename"]);
                    byte[] headerBytes = encoding.GetBytes(httpRowData);
                    byte[] endBytes = encoding.GetBytes(end);
                    byte[] fileDataBytes = new byte[headerBytes.Length + fileBytes.Length + endBytes.Length];
                    headerBytes.CopyTo(fileDataBytes, 0);
                    fileBytes.CopyTo(fileDataBytes, headerBytes.Length);
                    endBytes.CopyTo(fileDataBytes, headerBytes.Length + fileBytes.Length);

                    string endBoundary = string.Format("--{0}--\r\n",boundary);
                    byte[] endBoundaryBytes = encoding.GetBytes(endBoundary);
                    byte[] bytes = new byte[fileDataBytes.Length + endBoundaryBytes.Length];
                    fileDataBytes.CopyTo(bytes, 0);
                    endBoundaryBytes.CopyTo(bytes, fileDataBytes.Length);
                    responseBytes = webClient.UploadData(requestUrl, bytes);
                    return new OperationResult(0, encoding.GetString(responseBytes));
                }             
            }
            catch (WebException ex)
            {
                Stream responseStream = ex.Response.GetResponseStream();
                responseBytes = new byte[ex.Response.ContentLength];
                responseStream.Read(responseBytes, 0, responseBytes.Length);
                Log.Error(ex);
                return new OperationResult(-1, encoding.GetString(responseBytes));
            }          
        }
    }
}
