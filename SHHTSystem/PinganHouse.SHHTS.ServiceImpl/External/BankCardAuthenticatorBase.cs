﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.ServiceImpl.External.Config;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Utils.Log;

namespace PinganHouse.SHHTS.ServiceImpl.External
{
    internal abstract class BankCardAuthenticatorBase : PartnerAuthenticatorBase<BankCardAuthenticateSetting>
    {
        public override OperationResult SendData(IDictionary<string, object> data)
        {
            return SendData<object>(data);
        }

        public override OperationResult SendData<TResult>(IDictionary<string, object> data)
        {
            var check = CheckParams(data);
            if (!check.Success)
                return check;

            try
            {
                var response = Send<TResult>(data);

                if (response == null)
                    return new OperationResult(-2, "响应信息错误");
                if (response.code != 0)
                    return new OperationResult(-3, string.Format("错误代码:{0},错误描述:{1}", response.code, response.msg));
                var result = new OperationResult(0);
                result.OtherData.Add("Data", response.data);
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, e.Message);
            }
        }

        public override OperationResult ValidateCallResult(HttpContextBase ctx, IDictionary<string, object> data)
        {
            return new OperationResult(0);
        }

        protected abstract OperationResult CheckParams(IDictionary<string, object> data);

        protected virtual OperationResult CheckCallBackParams(IDictionary<string, object> data)
        {
            return new OperationResult(0);
        }

        protected string GetSign(IDictionary<string, object> parameters)
        {
            if (string.IsNullOrEmpty(this.Setting.SecurityKey))
                throw new ArgumentNullException("security key not found.");
            object name = null;
            if (parameters.ContainsKey("name"))
            {
                name = parameters["name"];
                parameters.Remove("name");
            }
            string value = string.Join("", string.Concat(parameters.Values).Reverse());
            if (name != null)
            {
                parameters.Add("name", name);
            }
            return EncryptHelper.GetMD5(string.Concat(value, this.Setting.SecurityKey));
        }

        protected FsBcResponse<TResult> Send<TResult>(IDictionary<string, object> parameters)
        {
            if (parameters == null)
                parameters = new Dictionary<string, object>();

            parameters.Add("requestsignkey", GetSign(parameters));

            return JsonSerializer.DeserializeToObject<FsBcResponse<TResult>>(HttpUtils.SendPostRequest(Setting.RequestUrl, BuildParameters(parameters), 30000, Encoding.UTF8, Encoding.UTF8, null));
        }
    }

    class FsBcResponse<T>
    {
        public int code { get; set; }
        public string msg { get; set; }
        public T data { get; set; }
    }
}
