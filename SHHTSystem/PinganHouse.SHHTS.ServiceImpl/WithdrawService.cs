﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.ServiceImpl
{
    public partial class TradeService : ITradeService
    {
        public OperationResult SubmitWithdraw(AccountType accountType, string subjectId, OrderBizType bizType, decimal amount, string bankAccountNo, string bankCode, string accountName, string bankName, string remark, long operatorSysNo)
        {
            #region ###################参数验证###################
            if (string.IsNullOrWhiteSpace(subjectId) || string.IsNullOrWhiteSpace(subjectId) || amount <= 0)
                return new OperationResult(-1, "非法参数信息");
            if (string.IsNullOrWhiteSpace(bankCode) || string.IsNullOrWhiteSpace(accountName) || string.IsNullOrWhiteSpace(bankAccountNo))
                return new OperationResult(-2, "银行帐户信息错误");
            var account = ServiceDepository.AccountDataAccess.Get(subjectId, accountType, bizType);
            if (account == null)
                return new OperationResult(-3, "付款账户不存在");
            if (account.IsLocked)
                return new OperationResult(-3, "付款账户已锁定");
            if (account.Balance < amount)
                return new OperationResult(-3, "付款账户余额不足");
            #endregion

            try
            {
                ServiceDepository.TransactionService.Begin();
                #region ###################创建订单###################
                OrderInfo order = new OrderInfo
                {
                    SysNo = Sequence.Get(),
                    AccountSysNo = account.SysNo,
                    OrderAmount = amount,
                    OrderType = OrderType.Withdraw,
                    OrderBizType = bizType,
                    PaymentChannel = PaymentChannel.Transfer,
                    OrderStatus = OrderStatus.Initial,
                    Remark = remark,
                    CreateUserSysNo = operatorSysNo,
                    CreateDate = DateTime.Now
                };
                if (!ServiceDepository.OrderDataAccess.Insert(order))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-4, "订单创建失败");
                }
                #endregion


                #region ###################订单扩展信息###################
                var extend = new WithdrawOrderExtend
                {
                    OrderNo = order.OrderNo,
                    AccountName = accountName,
                    BankAccountNo = bankAccountNo,
                    BankCode = bankCode,
                    SubBankName = bankName,
                    CreateUserSysNo = operatorSysNo
                };
                if (!ServiceDepository.OrderDataAccess.InsertWithdrawExtend(extend))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-6, "订单创建失败");
                }
                #endregion

                #region ###################锁定出账金额###################
                account.Balance -= order.OrderAmount;
                account.LockedMoney += order.OrderAmount;
                account.ModifyUserSysNo = operatorSysNo;
                if (!ServiceDepository.AccountDataAccess.Update(account))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-7, "资金锁定操作失败");
                }
                //资金日志
                var log = new AccountOperationLog
                {
                    AccountSysNo = account.SysNo,
                    Amount = order.OrderAmount,
                    OperationType = AccountOperationType.Lock,
                    RefBizNo = order.OrderNo,
                    Remark = string.Format("提现锁定：{0}", order.OrderNo),
                    CreateUserSysNo = operatorSysNo,
                    CreateDate = DateTime.Now
                };
                if (!ServiceDepository.AccountDataAccess.AddOperatorLog(log))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-8, "操作日志创建失败");
                }
                #endregion
                var result = new OperationResult(0);
                result.OtherData.Add("OrderNo", order.OrderNo);

                ServiceDepository.TransactionService.Commit();
                return result;
            }
            catch (Exception e)
            {
                ServiceDepository.TransactionService.RollBack();
                Log.Error(e);
                return new OperationResult(-5, "系统错误");
            }
        }

        public OperationResult WithdrawOrderAudit(ReceptionCenter center, string orderNo, AuditStatus status, string remark, long operatorSysNo, string operatorName)
        {
            #region #################参数验证#################
            if (string.IsNullOrWhiteSpace(orderNo))
                return new OperationResult(-1, "非法参数");
            var order = ServiceDepository.OrderDataAccess.Get(orderNo);
            if (order == null)
                return new OperationResult(-2, "订单不存在");

            var preStatus = ServiceDepository.ConfigService.GetWithdrawOrderAuditPreStatus(center, status);
            if (!preStatus.Contains(order.AuditStatus))
                return new OperationResult(-3, "交易状态错误");
            var auditors = ServiceDepository.ConfigService.GetWithdrawOrderAuditors(center, status);
            if (auditors == null || auditors.Count == 0 || !auditors.Contains(operatorSysNo))
                return new OperationResult(-4, "无此操作权限");
            #endregion
            try
            {
                var log = new AuditLogInfo
                {
                    AuditObjectType = AuditObjectType.Order,
                    ChangeValue = (int)status,
                    SubjectId = order.OrderNo,
                    AuditSysNo = operatorSysNo,
                    AuditUserName = operatorName,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = operatorSysNo,
                    Remark = remark
                };

                ServiceDepository.TransactionService.Begin();
                #region #################审核日志#################
                if (!ServiceDepository.AuditLogDataAccess.Insert(log))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-7, "审核日志创建失败");
                }
                #endregion

                #region #################订单状态#################
                order.AuditStatus = status;
                if (status == AuditStatus.ReAuditFailed || status == AuditStatus.AuditFailed || status == AuditStatus.FinanceAuditFailed)
                    order.OrderStatus = OrderStatus.Closed;
                order.ModifyUserSysNo = operatorSysNo;
                if (!ServiceDepository.OrderDataAccess.Update(order))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-9, string.Format("订单:{0},状态修改失败", order.OrderNo));
                }
                #endregion

                #region #################资金操作#################
                if (status == AuditStatus.ReAuditFailed || status == AuditStatus.AuditFailed) // 审核拒绝，回滚锁定资金
                {
                    var account = ServiceDepository.AccountDataAccess.Get(order.AccountSysNo, order.OrderBizType);
                    if (account.LockedMoney < order.OrderAmount)
                    {
                        ServiceDepository.TransactionService.RollBack();
                        return new OperationResult(-10, "账户异常");
                    }
                    account.Balance += order.OrderAmount;
                    account.LockedMoney -= order.OrderAmount;
                    account.ModifyUserSysNo = operatorSysNo;
                    //更新余额
                    if (!ServiceDepository.AccountDataAccess.Update(account))
                    {
                        ServiceDepository.TransactionService.RollBack();
                        return new OperationResult(-10, "资金解锁错误");
                    }
                    //资金操作日志
                    if (!ServiceDepository.AccountDataAccess.AddOperatorLog(new AccountOperationLog
                    {
                        AccountSysNo = account.SysNo,
                        Amount = Math.Abs(order.OrderAmount),
                        OperationType = AccountOperationType.UnLock,
                        RefBizNo = order.OrderNo,
                        Remark = string.Format("支付状态,{0}", status.ToString()),
                        CreateUserSysNo = operatorSysNo,
                        CreateDate = DateTime.Now
                    }))
                    {
                        ServiceDepository.TransactionService.RollBack();
                        return new OperationResult(-10, "资金日志错误");
                    }
                }
                else
                {
                    AuditStatus submitStatus = ServiceDepository.ConfigService.GetPaymentSubmitCondition();
                    if (order.AuditStatus == submitStatus)
                    {
                        #region #################开始提现#################
                        var orderExtend = ServiceDepository.OrderDataAccess.GetWithdrawExtend(order.OrderNo);
                        if (orderExtend == null)
                        {
                            ServiceDepository.TransactionService.RollBack();
                            return new OperationResult(-13, "出款失败，出款帐号信息错误");
                        }

                        //创建出款流水
                        var detail = new TradeInfo
                        {
                            SysNo = Sequence.Get(),
                            OrderNo = order.OrderNo,
                            Amount = -order.OrderAmount,
                            Status = TradeStatus.Initial,
                            AccountName = orderExtend.AccountName,
                            BankAccountNo = orderExtend.BankAccountNo,
                            BankCode = orderExtend.BankCode,
                            SubBankName = orderExtend.SubBankName,
                            Remark = remark,
                            CreateUserSysNo = operatorSysNo,
                            CreateDate = DateTime.Now
                        };
                        if (!ServiceDepository.TradeDataAccess.Insert(detail))
                        {
                            ServiceDepository.TransactionService.RollBack();
                            return new OperationResult(-13, "出款明细创建失败");
                        }
                    }
                        #endregion
                }
                #endregion

                ServiceDepository.TransactionService.Commit();
                return new OperationResult(0);
            }
            catch (Exception e)
            {
                ServiceDepository.TransactionService.RollBack();
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }

        private OperationResult ConfirmWithdrawTrade(TradeInfo trade, OrderInfo order, PaymentStatus status, string rejectReason, long? operatorSysNo)
        {
            if (order.OrderStatus != OrderStatus.Submitted)
                return new OperationResult(-6, "订单状态错误");
            var account = ServiceDepository.AccountDataAccess.Get(order.AccountSysNo, order.OrderBizType);
            if (account == null || account.LockedMoney < Math.Abs(trade.Amount))
                return new OperationResult(-7, "账户异常");
            try
            {
                #region ###################修改订单信息###################
                if (operatorSysNo != null)
                    order.ModifyUserSysNo = operatorSysNo;

                if (status == PaymentStatus.FinanceAuditPass || status == PaymentStatus.FinanceAuditFailed)
                {
                    AuditStatus auditStatus = status == PaymentStatus.FinanceAuditPass ? AuditStatus.FinanceAudited : AuditStatus.FinanceAuditFailed;
                    #region 订单审核日志
                    if (!ServiceDepository.AuditLogDataAccess.Insert(new AuditLogInfo
                    {
                        AuditObjectType = AuditObjectType.Order,
                        ChangeValue = (int)auditStatus,
                        Department = "总部财务",
                        SubjectId = order.OrderNo,
                        Remark = rejectReason
                    }))
                    {
                        return new OperationResult(-13, "审核日志创建失败");
                    }
                    #endregion
                    order.AuditStatus = auditStatus;
                }
                if (status != PaymentStatus.FinanceAuditPass)
                {
                    order.OrderStatus = status == PaymentStatus.Success ? OrderStatus.Completed : OrderStatus.Closed;
                }
                if (!ServiceDepository.OrderDataAccess.Update(order))
                {
                    return new OperationResult(-11, "更新订单状态错误");
                }
                #endregion

                #region ###################修改资金账户信息###################
                if (status != PaymentStatus.FinanceAuditPass)
                {
                    account.LockedMoney -= Math.Abs(trade.Amount);
                    if (status == PaymentStatus.FinanceAuditFailed || status == PaymentStatus.Failed)
                        account.Balance += Math.Abs(trade.Amount);
                    if (!ServiceDepository.AccountDataAccess.Update(account))
                    {
                        return new OperationResult(-9, "更新账户信息错误");
                    }
                    //account log
                    if (!ServiceDepository.AccountDataAccess.AddOperatorLog(new AccountOperationLog
                    {
                        AccountSysNo = account.SysNo,
                        Amount = Math.Abs(trade.Amount),
                        OperationType = status == PaymentStatus.Success ? AccountOperationType.ChargeOff : AccountOperationType.UnLock,
                        RefBizNo = trade.TradeNo,
                        Remark = string.Format("支付状态,{0}", status.ToString()),
                        CreateUserSysNo = operatorSysNo,
                        CreateDate = DateTime.Now
                    }))
                    {
                        return new OperationResult(-10, "创建资金日志错误");
                    }
                    //资金账户明细
                    if (status == PaymentStatus.Success && !ServiceDepository.AccountDetailDataAccess.Insert(new AccountDetailInfo(AccountDetailType.Withdraw)
                    {
                        AccountSysNo = account.SysNo,
                        Amount = -order.OrderAmount,
                        OrderNo = order.OrderNo,
                        CreateUserSysNo = operatorSysNo
                    }))
                    {
                        return new OperationResult(-11, "创建资金明细错误");
                    }
                }
                #endregion

                return new OperationResult(0);
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }
    }
}
