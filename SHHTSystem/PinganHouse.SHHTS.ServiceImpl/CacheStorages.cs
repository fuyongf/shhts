﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Entities;
using MongoDB.Driver.Builders;
using PinganHouse.SHHTS.DataAccess.MongoRepository;

namespace PinganHouse.SHHTS.ServiceImpl
{
    public class CacheStorages : ICacheStorages
    {     

        public object this[string key]
        {
            get
            {
                return Get<object>(key);
            }
            set
            {
                this.Upset<object>(key, value);
            }
        }

        public object this[string key, TimeSpan expireTime]
        {
            set { this.Upset<object>(key, value, expireTime); }
        }

        public bool Upset<T>(string key, T value)
        {
            return this.UpsetInternal(key, value);
        }

        public bool Upset<T>(string key, T value, TimeSpan expireTime)
        {
            return this.UpsetInternal(key, value, DateTime.Now.AddTicks(expireTime.Ticks));
        }

        public T Get<T>(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
                return default(T);
            var cache = ServiceDepository.CacheDataAccess.Get<CacheBase<T>>(key);
            if (cache == null || cache.IsExpired)
                return default(T);
            return cache.TValue;
        }

        public bool TryGet<T>(string key, out T value)
        {
            value = default(T);
            try 
            {
                value = this.Get<T>(key);
                return true;
            }
            catch {
                return false;
            }
        }

        public bool Remove(string key)
        {
            return ServiceDepository.CacheDataAccess.Remove(key);
        }


        private bool UpsetInternal<T>(string key, T value, DateTime expireTime = default(DateTime))
        {
            if (string.IsNullOrWhiteSpace(key))
                return false;
            return ServiceDepository.CacheDataAccess.Upset(new CacheBase<T> { Id = key, ExpireTime = expireTime, TValue = value });
        }

    }
}
