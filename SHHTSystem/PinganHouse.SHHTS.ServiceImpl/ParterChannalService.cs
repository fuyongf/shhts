﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Utils;
using HTB.DevFx;
using PinganHouse.SHHTS.Core.External;
using PinganHouse.SHHTS.DataTransferObjects.External;

namespace PinganHouse.SHHTS.ServiceImpl
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public partial class ParterChannalService:IParterService
    {
        const int ErrorCount = 10;
        const int LockHour = 24;
        public OperationResult AcceptMessage(WXMessage message)
        {
            string handleKey =  message.MsgType.ToString();
            if (message.MsgType == WXMessageType.Event && message.Event != null)
            {
                handleKey = message.Event.ToString();
            }
            return this.HandleMessage(message, handleKey);
        }

        public OperationResult RefreshAccessToken(AgentType? agentType)
        {
            try
            {
                string appNo ="wx_AccessToken";
                if (agentType != null)
                    appNo = "wx_QY_AccessToken";
                return ServiceDepository.PartnerService.SendData(appNo, null);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-1, "系统内部错误");
            }
        }

        public OperationResult GetAuthRequestUrl(string redirectUrl, string state, string appNo)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(redirectUrl))
                    return new OperationResult(1, "redirectUrl is null"); ;
                var data = new Dictionary<string, object> { { "redirect_uri", redirectUrl }, { "state", state } };
                return ServiceDepository.PartnerService.SendData(appNo, data);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-1, "系统内部错误");
            }
        }

        public OperationResult ValidateAuth2CallResult(string code)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(code))
                    return new OperationResult(1, "用户未授权"); ;
                var data = new Dictionary<string, object> { { "code", code }};
                var result = ServiceDepository.PartnerService.ValidateCallResult("wx_Authorize", null, data);
                if (!result.Success)
                {
                    Log.Error("微信授权验证失败："+ result.ResultMessage);
                    return new OperationResult(2, "微信授权失败");
                }
                var dic = new Dictionary<string, object> { {"NickName", result.OtherData["nickname"]}, {"OpenId", result.OtherData["openid"] } };
                var parterUser = ServiceDepository.ParterUserInfoDataAccess.Get(result.OtherData["openid"].ToString());
                if (parterUser != null)
                {
                    var customers = ServiceDepository.CustomerDataAccess.GetByUserSysNo(parterUser.UserSysNo.Value);
                    if (customers != null && customers.Count > 0)
                        return new OperationResult(0, string.Join(",", customers.Select(pu => pu.SysNo))) { OtherData = dic };  
                }
                return new OperationResult(999, "未绑定系统用户") { OtherData = dic };
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-1, "系统内部错误");
            }
        }


        public OperationResult SendCustomerTextMessage(string touser, string message)
        {
            if (string.IsNullOrWhiteSpace(touser))
                return new OperationResult(1, "touser is null");
            if (string.IsNullOrWhiteSpace(message))
                return new OperationResult(2, "message is null");
            var msgDic = new Dictionary<string, string> { { "content", message } };
            var data = new Dictionary<string, object> { { "touser", touser }, { "msgtype", "text" }, { "text", msgDic } };
            return ServiceDepository.PartnerService.SendData("wx_CustomMessage", data);
        }

        public OperationResult SendTemplateMessage(long customerysNo, string templateId, IDictionary<string, object> data)
        {
            if (data == null || data.Count == 0)
                return new OperationResult(1, "消息内容为空");
            if (string.IsNullOrWhiteSpace(templateId))
                return new OperationResult(3, "未找到消息模版");

            var customer = ServiceDepository.CustomerDataAccess.Get(customerysNo);
            if (customer == null)
                return new OperationResult(4, "用户不存在");
            var parterUsers = ServiceDepository.ParterUserInfoDataAccess.GetByUserSysNo(customer.UserSysNo);
            if (parterUsers == null || parterUsers.Count==0)
                return new OperationResult(2, "该用户未关注服务号");
            data["template_id"] = templateId;
            var result = new OperationResult(1);
            foreach (var parterUser in parterUsers)
            {
                data["touser"] = parterUser.OpenID;             
                result = ServiceDepository.PartnerService.SendData("wx_TemplateSendJobFinish", data);
                if (result.Success)
                {
                    var message = new WXTemplateMessage
                    {
                        MsgId = result.OtherData["msgid"].ToString(),
                        Status = WXStatus.Sended,
                        SendsCount = 1,
                        Data = data,
                    };
                    //保存消息
                    ServiceDepository.WXTemplateMessageDataAccess.Insert(message);
                }
            }               
            return result;
        }

        public string GetQrCodeUrl(long userSysNo)
        {
            if (userSysNo <= 0)
                return null;
            var data = new Dictionary<string, object> { 
                    {"expire_seconds", 1800},
                    {"action_name","QR_SCENE"},
                };
            data["action_info"] = new Dictionary<string, object> { { "scene", new Dictionary<string, object> { { "scene_id", userSysNo } } } };
            return GetQrCodeUrl(data);
        }

        public string GetQrCodeUrl(string param)
        {
            if (string.IsNullOrWhiteSpace(param) || param.Length>64)
                return null;
            var data = new Dictionary<string, object> { 
                    {"action_name","QR_LIMIT_STR_SCENE"},
                };
            data["action_info"] = new Dictionary<string, object> { { "scene", new Dictionary<string, object> { { "scene_str", param } } } };
            return GetQrCodeUrl(data);
        }

        public OperationResult CreateMenus(IList<Dictionary<string, object>> menus, AgentType? agentType)
        {
            if (menus == null || menus.Count == 0)
                return new OperationResult(1, "menus is null");
            if (menus.Count > 3)
                return new OperationResult(2, "menus is invalid ");
            string appNo="wx_Menu";
            var data = new Dictionary<string, object> { { "UrlKey", "create" }, { "button", menus } };
            //是否为企业
            if (agentType != null)
            {
                data["Agentid"] = (int)agentType.Value;
                appNo = "wx_QY_Menu";
            }

            return ServiceDepository.PartnerService.SendData(appNo, data);
        }

        public OperationResult DeleteMenus(AgentType? agentType)
        {
            var data = new Dictionary<string, object> { { "UrlKey", "delete" },};
            string appNo = "wx_Menu";
            //是否为企业
            if (agentType != null)
            {
                data["Agentid"] = (int)agentType.Value;
                appNo = "wx_QY_Menu";
            }
            return ObjectService.GetObject<IPartnerAuthenticationService>().SendData(appNo, data);
        }

        private string GetQrCodeUrl(Dictionary<string, object>  data)
        {

            try
            {
                var result = ServiceDepository.PartnerService.SendData("wx_QRCode", data);
                if (result.Success)
                    return result.OtherData["ShowQRCodeUrl"].ToString();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return string.Empty;
        }

        private OperationResult HandleMessage(WXMessage message, string handleKey)
        {
            try
            {
                var dic = new Dictionary<string, Object> { { "WXMessage", message } };
                var result = ServiceDepository.PartnerService.ValidateCallResult(string.Format("wx_{0}", handleKey), null, dic);
                if (!result.Success)
                {
                    string path="/System/Parter/WeiXin/ReplyMessages/Text/Reply";
                    if (result.ResultNo == 999)
                        path = "/System/Parter/WeiXin/ReplyMessages/Text/UnBindMessage";
                    var messageFormat = ServiceDepository.SettingService.GetSettingItemValueByPath(path);
                    result.ResultMessage = string.Format(messageFormat, message.FromUserName, message.ToUserName, TimeStamp.Get());
                }
                return result;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return new OperationResult(1);
        }

        private string SetReplyNewsMessage(string toUser, string fromUser, string key)
        {
            var messageFormat = ServiceDepository.SettingService.GetSettingItemValueByPath("/System/Parter/WeiXin/ReplyMessages/News");
            var data = new Dictionary<string, object> { { "type", "news" }, { "offset", 0 }, { "count", 20 } };
            var result = ServiceDepository.PartnerService.SendData("wx_Material", data);
            var item = @"<item><Title><![CDATA[{1}]]></Title> <Description><![CDATA[{2}]]></Description><PicUrl><![CDATA[{2}]]></PicUrl>
                            <Url><![CDATA[{3}]]></Url></item>";
            return string.Format(messageFormat, toUser, fromUser, TimeStamp.Get(), 1, item);
        }

        #region 微信服务号自主绑定及解绑
        public OperationResult CheckUser(string openId, string identityNo, CertificateType certificateType, string captcha, string token)
        {
            if (string.IsNullOrWhiteSpace(openId))
                return new OperationResult(4, "用户唯一标识为空");
            if (string.IsNullOrWhiteSpace(captcha))
                return new OperationResult(1, "验证码错误,请重新输入");
            if (string.IsNullOrWhiteSpace(token))
                return new OperationResult(2, "token 为空");
            if (string.IsNullOrWhiteSpace(identityNo))
                return new OperationResult(3, "证件号为空");
            var errorCount = ServiceDepository.CacheStorages.Get<int>(openId);
            if (errorCount > ErrorCount)
                return new OperationResult(5, "系统已锁定， 请稍后再试");
            var result = this.CheckUserInternal(openId, identityNo, certificateType, captcha, token);
            this.UpdateLockCount(openId, errorCount, result);
            return result;
        }

        private void UpdateLockCount(string openId, int errorCount, OperationResult result)
        {
            if (!result.Success)
            {
                ServiceDepository.CacheStorages.Upset<int>(openId, ++errorCount, TimeSpan.FromHours(LockHour));
            }
            else if (errorCount > 0)
            {
                //移除前面异常的记录
                ServiceDepository.CacheStorages.Remove(openId);
            }
        }
        private OperationResult CheckUserInternal(string openId, string identityNo, CertificateType certificateType, string captcha, string token)
        {
            var result = ServiceDepository.CaptchaService.ValidateCaptcha(token, BusinessCacheType.BindIdentity, captcha);
            if (!result.Success)
                return result;
            var user = ServiceDepository.UserDataAccess.GetByIdentityNo(certificateType, identityNo);
            if (user == null)
                return new OperationResult(1, "系统未查询到您的证件号码，客服电话：4008681111转1。");

            result.OtherData["Mobile"] = user.Mobile;
            result.OtherData["UserId"] = user.UserId;
            return result;
        }

        public OperationResult SendBindCaptcha(string userId, string mobile)
        {
            if (string.IsNullOrWhiteSpace(userId))
                return new OperationResult(1, "参数错误");
            var user = ServiceDepository.UserDataAccess.Get(userId);
            if (user == null)
                return new OperationResult(2, "用户不存在");
            if (string.IsNullOrWhiteSpace(mobile))
                return new OperationResult(3, "手机号码错误");
            if (string.Compare(user.Mobile, mobile) != 0)
                return new OperationResult(4, "非法数据");
            return ServiceDepository.SmsService.SendBindCaptcha(user.Mobile);
            //间隔2分钟
            //var cache = ServiceDepository.CacheStorages.Get<string>(mobile);
            //if (!string.IsNullOrWhiteSpace(cache))
            //    return new OperationResult(5, "规定时间内不能重复发送");
            //var result = ServiceDepository.SmsService.SendBindCaptcha(user.Mobile);
            //if(result.Success)
            //    ServiceDepository.CacheStorages.Upset<string>(mobile, "0", TimeSpan.FromMinutes(2));
            //return result;
        }

        public OperationResult BindUser(string openId, string userId, string captcha, string token)
        {
            if (string.IsNullOrWhiteSpace(openId))
                return new OperationResult(1, "用户唯一标识为空");
            if (string.IsNullOrWhiteSpace(captcha))
                return new OperationResult(2, "验证码错误,请重新输入");
            if (string.IsNullOrWhiteSpace(token))
                return new OperationResult(3, "token 为空");
            if (string.IsNullOrWhiteSpace(userId))
                return new OperationResult(4, "用户编号异常");
            var result = ServiceDepository.CaptchaService.ValidateCaptcha(token, BusinessCacheType.SMS, captcha);
            if (!result.Success)
                return result;
            var user = ServiceDepository.UserDataAccess.Get(userId);
            if (user == null)
                return new OperationResult(5, "绑定用户不存在");
            result = ServiceDepository.PartnerService.SendData("wx_Subscribe", new Dictionary<string, object> { { "openid", openId } });
            if (!result.Success)
                return new OperationResult(6, "获取用户信息失败");
            var parterUser = JsonSerializer.DeserializeToObject<ParterUserInfo>(result.ResultMessage);
            parterUser.Subscribe = true;
            parterUser.UserSysNo = user.SysNo;
            parterUser.CreateDate = DateTime.Now;
            if (!ServiceDepository.ParterUserInfoDataAccess.Insert(parterUser))
                return new OperationResult(7, "绑定失败");
            var message = ServiceDepository.SettingService.GetSettingItemValueByPath("/System/Parter/WeiXin/ReplyMessages/Text/BindUserSuccessMessage");
            //如在数据库直接存储字符\r，则C#取出来变为\\r
            this.SendCustomerTextMessage(openId, string.Format(message, "\r"));
            return new OperationResult(0);         
        }

        public OperationResult RelieveBind(long userSysNo, long? modifyUserSysNo)
        {
            try
            {
                if (userSysNo <= 0)
                    return new OperationResult(1, "用户系统编号异常");
                var result = ServiceDepository.ParterUserInfoDataAccess.RelieveBind(userSysNo, modifyUserSysNo);
                if (!result)
                    return new OperationResult(2, "解绑失败");
                return new OperationResult(0);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-1, "系统内部错误");
            }         
        }
       #endregion
    }
}
