﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.ServiceImpl
{
    public partial class TradeService : ITradeService
    {
        public OperationResult SubmitExpendOrder(AccountType accountType, string subjectId, decimal amount, OrderBizType bizType, PaymentChannel channel, string remark, long operatorSysNo)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(subjectId) || amount <= 0)
                    return new OperationResult(-1, "非法参数信息");
                var accountInfo = ServiceDepository.AccountDataAccess.Get(subjectId, accountType, bizType);
                if (accountInfo == null)
                    return new OperationResult(-2, "账户不存在");
                OrderInfo order = new OrderInfo()
                {
                    SysNo = Sequence.Get(),
                    AccountSysNo = accountInfo.SysNo,
                    OrderAmount = amount,
                    OrderType = OrderType.Expend,
                    OrderBizType = bizType,
                    PaymentChannel = channel,
                    OrderStatus = OrderStatus.Initial,
                    Remark = remark,
                    CreateUserSysNo = operatorSysNo,
                    CreateDate = DateTime.Now,
                };
                if (!ServiceDepository.OrderDataAccess.Insert(order))
                    return new OperationResult(-3, "订单创建失败");
                var result = new OperationResult(0, "订单创建完成");

                result.OtherData.Add("OrderNo", order.OrderNo);
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }

        public OperationResult SubmitTransferExpend(long sysNo, AccountType accountType, string accountSubjectId, OrderBizType bizType, long operatorSysNo)
        {
            return ReplenishTransferOrder(sysNo, accountType, accountSubjectId, OrderType.Expend, bizType, operatorSysNo);
        }

       
        private OperationResult ConfirmExpendTrade(TradeInfo trade, OrderInfo order, AccountInfo account, long? operatorSysNo)
        {
            try
            {
                if (trade.Status == TradeStatus.Completed)
                {
                    #region 仅作资金明细记录，保持资金平衡，可不做账户操作日志
                    //入账明细
                    if (!ServiceDepository.AccountDetailDataAccess.Insert(new AccountDetailInfo(AccountDetailType.Recharge)
                    {
                        AccountSysNo = account.SysNo,
                        Amount = trade.Amount,
                        OrderNo = trade.OrderNo,
                        CreateUserSysNo = operatorSysNo
                    }))
                    {
                        return new OperationResult(-13, "资金明细创建错误");
                    }
                    //出账明细
                    if (!ServiceDepository.AccountDetailDataAccess.Insert(new AccountDetailInfo(AccountDetailType.Transfer)
                    {
                        AccountSysNo = account.SysNo,
                        Amount = -trade.Amount,
                        OrderNo = trade.OrderNo,
                        CreateUserSysNo = operatorSysNo
                    }))
                    {
                        return new OperationResult(-13, "资金明细创建错误");
                    }
                    #endregion
                }

                return new OperationResult(0);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, e.Message);
            }
        }
    }
}
