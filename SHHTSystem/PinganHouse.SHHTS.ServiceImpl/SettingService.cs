﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.ServiceImpl
{
    public class SettingService : ISettingService
    {
        public Setting CreateSettingItem(string name, string parentId, string value, string desc, string remark)
        {
            try
            {
                if (string.IsNullOrEmpty(name))
                    throw new Exception("名称不能为空！");
                if (name.Contains("/"))
                    throw new Exception("名称不能包含' / '");

                string path = string.Empty;
                if (string.IsNullOrEmpty(parentId))
                {
                    path = string.Format("/{0}", name);
                }
                else
                {
                    Setting parent = ServiceDepository.SettingDataAccess.Get(parentId);
                    if (parent == null)
                        throw new Exception("找不到ID为：" + parentId + "的记录！");

                    path = string.Format("{0}/{1}", parent.Path, name);
                }

                string id = EncryptHelper.GetMD5(path, "GB2312");
                Setting entity = new Setting();
                entity.Id = id;
                entity.Path = path;
                entity.Name = name;
                entity.ParentId = string.IsNullOrEmpty(parentId) ? null : parentId.Trim();
                entity.Description = desc;
                entity.Value = value;
                entity.CreateDate = DateTime.Now;
                entity.Remark = remark;

                if (!ServiceDepository.SettingDataAccess.Insert(entity))
                    return null;
                return entity;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        public Setting GetSettingItem(string id)
        {
            if (string.IsNullOrEmpty(id))
                return null;
            return ServiceDepository.SettingDataAccess.Get(id);
        }

        public Setting GetSettingItemByPath(string path)
        {
            if (string.IsNullOrEmpty(path))
                return null;
            return GetSettingItem(GetIdByPah(path));
        }

        public IEnumerable<Setting> GetSettingItems(string parentId)
        {
            return ServiceDepository.SettingDataAccess.GetItems(string.IsNullOrEmpty(parentId) ? null : parentId.Trim());
        }

        public IEnumerable<Setting> GetSettingItemsByPath(string parentPath)
        {
            if (string.IsNullOrEmpty(parentPath))
                return null;
            return GetSettingItems(GetIdByPah(parentPath));
        }

        public string GetSettingItemValueByPath(string path)
        {
            if (string.IsNullOrEmpty(path))
                return null;
            return ServiceDepository.SettingDataAccess.GetValue(GetIdByPah(path));
        }

        public bool DeleteSettingItem(string id)
        {
            if (string.IsNullOrEmpty(id))
                return false;
            return ServiceDepository.SettingDataAccess.Remove(id);
        }

        public bool ModifySetting(string id, string value, string desc)
        {
            if (string.IsNullOrEmpty(id))
                return false;
            return ServiceDepository.SettingDataAccess.Update(id, value, desc);
        }

        private string GetIdByPah(string path)
        {//固定格式，如: /FirstLevel/SecondLevel
            if (path == null)
                return null;
            if (!path.StartsWith("/"))
                path = string.Format("/{0}", path);
            if (path.EndsWith("/"))
                path = path.Substring(0, path.Length - 1);
            if (path.Length == 0)
                return null;

            return EncryptHelper.GetMD5(path, "GB2312");
        }
    }
}
