﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.ServiceImpl
{
    public class FingerService : IFingerService
    {
        private const string FINGER_CACHE_KEY = "Finger_All";

        public OperationResult CollectFinger(long userSysNo, string fpCode, string tag, long operatorSysNo)
        {
            if (string.IsNullOrWhiteSpace(fpCode))
                return new OperationResult(-1, "指纹信息错误");
            var user = ServiceDepository.UserDataAccess.Get(userSysNo);
            if (user == null)
                return new OperationResult(-2, "用户不存在");
            if (user.FingerprintCount >= 8)
                return new OperationResult(-3, "指纹信息最多录入8枚");
            try
            {
                if (ServiceDepository.FingerprintDataAccess.Insert(new UserFingerprintInfo
                {
                    UserSysNo = userSysNo,
                    Fingerprint = fpCode,
                    Tag = tag,
                    CreateUserSysNo = operatorSysNo
                }))
                {
                    return new OperationResult(0);
                }
                return new OperationResult(-6, "保存指纹信息失败");
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, "系统错误");
            }
        }

        public OperationResult DeleteFinger(long fpSysNo, long operatorSysNo)
        {
            if (ServiceDepository.FingerprintDataAccess.Delete(fpSysNo, operatorSysNo))
                return new OperationResult(0);
            return new OperationResult(-1, "操作失败");
        }

        public IList<UserFingerprint> GetFingerByUserSysNo(long userSysNo)
        {
            var entities = ServiceDepository.FingerprintDataAccess.GetByUserSysNo(userSysNo);
            if (entities == null)
                return null;
            return entities.Select(e => ObjectMapper.Copy<UserFingerprint>(e)).ToList();
        }

        public bool VerifyFinger(string fpCode, out long userSysNo)
        {
            userSysNo = 0;
            if (string.IsNullOrWhiteSpace(fpCode))
                return false;
            try
            {
                byte[] base64 = Encoding.UTF8.GetBytes(fpCode);
                byte[] fp = new byte[384];
                int fpLen = 0;
                int ret = External.FingerExternalService.FPIBase64FingerDataToHex(base64, base64.Length, fp, ref fpLen);
                if (ret != 0)
                    throw new Exception(string.Concat("指纹特征Base64解码失败", ret));

                Dictionary<long, IList<byte[]>> figerTemps = CacheHelper.Get(FINGER_CACHE_KEY) as Dictionary<long, IList<byte[]>>;

                #region #################################初始化模版数据######################################
                if (figerTemps == null)
                {
                    var entities = ServiceDepository.FingerprintDataAccess.GetAll();
                    if (entities == null)
                    {
                        Log.Error("指纹模版不存在.");
                        return false;
                    }
                    figerTemps = new Dictionary<long, IList<byte[]>>();
                    foreach (var item in entities)
                    {
                        try
                        {
                            byte[] tmpBase64 = Encoding.UTF8.GetBytes(item.Fingerprint);
                            byte[] tmp = new byte[384];
                            int tmpLen = 0;
                            int opt = External.FingerExternalService.FPIBase64FingerDataToHex(tmpBase64, tmp.Length, tmp, ref tmpLen);
                            if (opt != 0)
                                throw new Exception(string.Concat("指纹模版Base64解码失败", opt));
                            if (figerTemps.ContainsKey(item.UserSysNo))
                                figerTemps[item.UserSysNo].Add(tmp);
                            else
                                figerTemps.Add(item.UserSysNo, new List<byte[]> { tmp });
                        }
                        catch (Exception e)
                        {
                            Log.Error(string.Format("UserSysNo:{0},Finger:{1},解析失败/{2}", item.UserSysNo, item.Fingerprint, e.ToString()));
                        }
                    }
                    CacheHelper.Set(FINGER_CACHE_KEY, figerTemps, DateTime.Now.AddHours(1));
                }
                #endregion

                if (figerTemps == null || figerTemps.Count == 0)
                    return false;
                foreach (var item in figerTemps)
                {
                    if (item.Value == null || item.Value.Count == 0)
                        continue;
                    foreach (var tmp in item.Value)
                    {
                        int verify = External.FingerExternalService.FPIMatch(tmp, fp, 3);
                        if (verify == 0)
                        {
                            userSysNo = item.Key;
                            return true;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
            return false;
        }
    }
}
