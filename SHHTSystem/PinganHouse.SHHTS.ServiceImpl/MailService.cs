﻿using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils.Log;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web.Configuration;
namespace PinganHouse.SHHTS.ServiceImpl
{
    public class MailService : IMailService
    {

        public OperationResult Send(Message message, bool sysnc)
        {
            try
            {
                MailMessageInfo mailEntity = message as MailMessageInfo;
                if (message == null)
                    return new OperationResult(1, "邮件信息为空");
                if (message.ToAddresses == null || message.ToAddresses.Count==0)
                    return new OperationResult(2, "邮件内容或接收地址为空");
                var fromAddress = new MailAddress(smtp.Network.UserName, mailEntity.FromName); //含有发送人的发送地址
                
                var email = new MailMessage
                {
                    IsBodyHtml = mailEntity.IsBodyHtml,
                    Subject = mailEntity.Subject,
                    Body = mailEntity.Body,
                    BodyEncoding = mailEntity.Encoding,
                    From = fromAddress,
                     
                };
                //添加发送地址
                foreach (var toAddress in mailEntity.ToAddresses)
                {
                    email.To.Add(new MailAddress(toAddress));
                }
                ///添加抄送地址
                if (mailEntity.CC != null || mailEntity.CC.Count>0)
                {
                    foreach (var cc in mailEntity.CC)
                    {
                        email.CC.Add(new MailAddress(cc));
                    }
   
                }
                //发送附件
                if (mailEntity.Attachments != null && mailEntity.Attachments.Count>0)
                {
                    foreach(var a in mailEntity.Attachments)
                    {
                        using(Stream fileStream = File.OpenRead(a.Key))
                        {
                            ContentType ct = new ContentType(MediaTypeNames.Text.Plain);
                            var attachment = new Attachment(fileStream, ct);
                            ContentDisposition disposition = attachment.ContentDisposition;
                            //文件名不支持中文。如果是disposition.FileName为中文名则发生失败
                            disposition.FileName = a.Value;
                            email.Attachments.Add(attachment);
                        }                
                    }

                }
                return PublicTo(email, sysnc);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-1, e.Message);
            }
        }
        //在windows服务中需要这一段读app.config 而在web程序中必须注释，打开下面那段
        //static Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        //static SmtpSection smtp = NetSectionGroup.GetSectionGroup(config).MailSettings.Smtp;
        static readonly SmtpSection smtp = NetSectionGroup.GetSectionGroup(WebConfigurationManager.OpenWebConfiguration("~/web.config")).MailSettings.Smtp;

        OperationResult PublicTo(MailMessage mail, bool sync = true)
        {
            try
            {
                using (var client = new SmtpClient
                {
                    Host = smtp.Network.Host,
                    Credentials = new NetworkCredential(smtp.Network.UserName, smtp.Network.Password),
                    Port = smtp.Network.Port,
                })
                {
                    //开始发送
                    client.Send(mail);
                    return new OperationResult(0);
                };

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-1, ex.Message);
            }


        }
    }
}
