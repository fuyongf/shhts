﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.ServiceImpl
{
    public partial class TradeService : ITradeService
    {
        public OperationResult SubmitCollection(AccountType accountType, string accountSubjectId, decimal amount, OrderBizType bizType, PaymentChannel channel, string remark, long operatorSysNo)
        {
            return SubmitCollection(accountType, accountSubjectId, amount, bizType, channel, remark, operatorSysNo, null);
        }

        public OperationResult SubmitCollection(AccountType accountType, string accountSubjectId, decimal totalAmount, decimal tradeAmount, OrderBizType bizType, PaymentChannel channel, string serialNo, string terminalNo, string bankAccountNo, string bankCode, string accountName, string bankName, DateTime paymentDate, string remark, long operatorSysNo)
        {
            if (string.IsNullOrWhiteSpace(accountSubjectId) || totalAmount <= 0 || tradeAmount <= 0 || totalAmount < tradeAmount)
                return new OperationResult(-1, "非法参数信息");
            try
            {
                ServiceDepository.TransactionService.Begin();
                var accountInfo = ServiceDepository.AccountDataAccess.Get(accountSubjectId, accountType, bizType);
                if (accountInfo == null)
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-2, "账户不存在");
                }
                var orderResult = SubmitCollection(accountType, accountSubjectId, totalAmount, bizType, channel, remark, operatorSysNo, accountInfo);
                if (!orderResult.Success)
                {
                    ServiceDepository.TransactionService.RollBack();
                    return orderResult;
                }
                var tradeResult = SubmitCollectionTrade(orderResult.OtherData["OrderNo"].ToString(), serialNo, terminalNo, tradeAmount, bankAccountNo, bankCode, accountName, bankName, paymentDate, null, operatorSysNo, accountInfo);
                if (!tradeResult.Success)
                {
                    ServiceDepository.TransactionService.RollBack();
                }
                else
                {
                    tradeResult.OtherData.Add("OrderSysNo", orderResult.OtherData["OrderSysNo"]);
                    tradeResult.OtherData.Add("OrderNo", orderResult.OtherData["OrderNo"]);
                }
                //这里不需要Commit
                return tradeResult;
            }
            catch (Exception e)
            {
                ServiceDepository.TransactionService.RollBack();
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }

        //public OperationResult SubmitCollectionTrade(string orderNo, string serialNo, string terminalNo, decimal amount, string bankAccountNo, string bankCode, string accountName, string bankName, DateTime paymentDate, string remark, long operatorSysNo)
        //{
        //    return SubmitCollectionTrade(orderNo, serialNo, terminalNo, amount, bankAccountNo, bankCode, accountName, bankName, paymentDate, remark, operatorSysNo, null);
        //}

        //public OperationResult CancelCollectionOrder(string orderNo, long operatorSysNo)
        //{
        //    #region ###################参数校验###################
        //    if (string.IsNullOrWhiteSpace(orderNo))
        //        return new OperationResult(-1, "订单编号不能为空");
        //    var order = ServiceDepository.OrderDataAccess.Get(orderNo);
        //    if (order == null)
        //        return new OperationResult(-2, "订单不存在");
        //    if (order.OrderStatus != OrderStatus.Initial)
        //        return new OperationResult(-3, "订单状态错误，不能取消");
        //    var trades = ServiceDepository.TradeDataAccess.GetByOrder(orderNo);
        //    if (trades != null && trades.Count > 0 &&
        //        trades.Any(t => t.Status == TradeStatus.Completed || t.Status == TradeStatus.Submitted || t.Status == TradeStatus.Initial))
        //        return new OperationResult(-4, "订单支付中，不能取消");
        //    #endregion
        //    try
        //    {
        //        order.OrderStatus = OrderStatus.Closed;
        //        order.ModifyUserSysNo = operatorSysNo;
        //        if (!ServiceDepository.OrderDataAccess.Update(order))
        //            return new OperationResult(-6, "订单状态更新失败");
        //    }
        //    catch (Exception e)
        //    {
        //        Log.Error(e);
        //        return new OperationResult(-5, "系统错误");
        //    }
        //    return new OperationResult(0);
        //}

        public OperationResult CancelCollectionTrade(string tradeNo, long operatorSysNo)
        {
            return CancelCollectionTrade(tradeNo, TradeStatus.Closed, operatorSysNo);
        }

        public OperationResult CancelCollectionTrade(string tradeNo, BillDetail bill, long operatorSysNo)
        {
            try
            {
                if (bill == null)
                    return new OperationResult(-1, "票据为空");
                if (bill.BillType != BillType.ExpenedCancelled)
                    return new OperationResult(-2, "票据类型错误");
                ServiceDepository.TransactionService.Begin();
                if (!ServiceDepository.BillDataAccess.Insert(ObjectMapper.Copy<BillInfo>(bill)))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-3, "票据保存失败");
                }
                return CancelCollectionTrade(tradeNo, TradeStatus.Cancelled, operatorSysNo);
            }
            catch (Exception e)
            {
                ServiceDepository.TransactionService.RollBack();
                Log.Error(e);
                return new OperationResult(-5, "系统错误");
            }
        }

        public OperationResult AddMissCollectionTrade(MissCollectionTrade trade)
        {
            if (trade == null)
                return new OperationResult(-1, "参数错误");
            if (string.IsNullOrEmpty(trade.SerialNo))
                return new OperationResult(-2, "流水号不能为空");
            if (trade.Amount <= 0)
                return new OperationResult(-3, "交易金额错误");
            if (trade.PaymentDate == DateTime.MinValue)
                return new OperationResult(-4, "交易时间错误");
            if (ServiceDepository.MissCollectionTradeDataAccess.Exists(trade.Channel, trade.SerialNo))
                return new OperationResult(0, "重复推送");
            try
            {
                var entity = ObjectMapper.Copy<MissCollectionTradeInfo>(trade);
                entity.SysNo = Sequence.Get();
                entity.CreateDate = DateTime.Now;
                entity.Confirmed = false;
                entity.AuditorSysNo = null;

                if (!ServiceDepository.MissCollectionTradeDataAccess.Insert(entity))
                {
                    return new OperationResult(-6, "掉单信息保存失败");
                }
                var result = new OperationResult(0);
                result.OtherData.Add("SysNo", entity.SysNo);
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }

        public OperationResult SubmitTransferCollection(long sysNo, AccountType accountType, string accountSubjectId, OrderBizType bizType, long operatorSysNo)
        {
            return ReplenishTransferOrder(sysNo, accountType, accountSubjectId, OrderType.Collection, bizType, operatorSysNo);
            //#region ###################参数校验###################
            //if (string.IsNullOrWhiteSpace(accountSubjectId))
            //    return new OperationResult(-1, "非法参数信息");
            //var detail = ServiceDepository.MissCollectionTradeDataAccess.Get(sysNo);
            //if (detail == null)
            //    return new OperationResult(-2, "掉单记录不存在");
            //if (detail.Confirmed)
            //    return new OperationResult(-2, "重复操作");
            //var accountInfo = ServiceDepository.AccountDataAccess.Get(accountSubjectId, accountType);
            //if (accountInfo == null)
            //    return new OperationResult(-3, "账户不存在");
            //#endregion

            //try
            //{
            //    ServiceDepository.TransactionService.Begin();

            //    #region ###################创建订单，补交易流水###################
            //    var orderResult = SubmitCollection(accountType, accountSubjectId, detail.Amount, bizType, detail.Channel, null, operatorSysNo, accountInfo);
            //    if (!orderResult.Success)
            //    {
            //        ServiceDepository.TransactionService.RollBack();
            //        return orderResult;
            //    }
            //    string orderNo = orderResult.OtherData["OrderNo"].ToString();
            //    var tradeResult = ReplenishCollectionTrade(detail.SysNo, orderNo, operatorSysNo);

            //    if (!tradeResult.Success)
            //    {
            //        ServiceDepository.TransactionService.RollBack();
            //    }
            //    else
            //    {
            //        tradeResult.OtherData.Add("OrderNo", orderNo);
            //    }
            //    #endregion

            //    //这里不需要Commit
            //    return tradeResult;
            //}
            //catch (Exception e)
            //{
            //    ServiceDepository.TransactionService.RollBack();
            //    Log.Error(e);
            //    return new OperationResult(-5, "系统异常");
            //}
        }

        public OperationResult ConfirmMissCollectionTrade(long sysNo, string tradeNo, long operatorSysNo)
        {
            if (string.IsNullOrWhiteSpace(tradeNo))
                return new OperationResult(-1, "交易流水号为空");
            var detail = ServiceDepository.MissCollectionTradeDataAccess.Get(sysNo);
            if (detail == null)
                return new OperationResult(-2, "掉单记录不存在");
            if (detail.Confirmed)
                return new OperationResult(-3, "掉单记录已被确认");
            var trade = ServiceDepository.TradeDataAccess.Get(tradeNo);
            if (trade == null)
                return new OperationResult(-4, "交易流水不存在");
            if (trade.Amount != detail.Amount)
                return new OperationResult(-5, "交易金额不匹配");
            if (!ServiceDepository.MissCollectionTradeDataAccess.Confirm(detail.SysNo, trade.TradeNo, operatorSysNo))
            {
                Log.Error(string.Format("确认收款掉单数据失败,{0},{1}", sysNo, tradeNo));
                return new OperationResult(-6, "确认收款掉单数据失败");
            }
            Log.Error(string.Format("确认收款掉单数据成功,{0},{1}", sysNo, tradeNo));
            return new OperationResult(0);
        }

        private OperationResult SubmitCollection(AccountType accountType, string accountSubjectId, decimal amount, OrderBizType bizType, PaymentChannel channel, string remark, long operatorSysNo, AccountInfo accountInfo)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(accountSubjectId) || amount <= 0)
                    return new OperationResult(-1, "非法参数信息");
                if (accountInfo == null)
                    accountInfo = ServiceDepository.AccountDataAccess.Get(accountSubjectId, accountType, bizType);
                if (accountInfo == null)
                    return new OperationResult(-2, "账户不存在");
                OrderInfo order = new OrderInfo()
                {
                    SysNo = Sequence.Get(),
                    AccountSysNo = accountInfo.SysNo,
                    OrderAmount = amount,
                    OrderType = OrderType.Collection,
                    OrderBizType = bizType,
                    PaymentChannel = channel,
                    OrderStatus = OrderStatus.Initial,
                    Remark = remark,
                    CreateUserSysNo = operatorSysNo,
                    CreateDate = DateTime.Now,
                };
                if (!ServiceDepository.OrderDataAccess.Insert(order))
                    return new OperationResult(-3, "订单创建失败");
                var result = new OperationResult(0, "订单创建完成");
                result.OtherData.Add("OrderSysNo", order.SysNo);
                result.OtherData.Add("OrderNo", order.OrderNo);
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }

        private OperationResult SubmitCollectionTrade(string orderNo, string serialNo, string terminalNo, decimal amount, string bankAccountNo, string bankCode, string accountName, string bankName, DateTime paymentDate, string remark, long operatorSysNo, AccountInfo accountInfo)
        {
            #region ###################参数校验###################
            if (string.IsNullOrWhiteSpace(orderNo) || string.IsNullOrWhiteSpace(serialNo) || amount <= 0)
                return new OperationResult(-1, "非法参数信息");
            var order = ServiceDepository.OrderDataAccess.Get(orderNo);
            if (order == null)
                return new OperationResult(-2, "订单不存在");
            if (order.OrderStatus != OrderStatus.Initial)
                return new OperationResult(-3, "订单状态错误");
            var trades = ServiceDepository.TradeDataAccess.GetByOrder(orderNo);
            decimal tradeAmount = 0;
            if (trades != null && trades.Count > 0)
                tradeAmount = trades.Where(t => (t.Status != TradeStatus.Cancelled && t.Status != TradeStatus.Failed && t.Status != TradeStatus.Closed)).Sum(t => t.Amount);
            if ((order.OrderAmount - tradeAmount) < amount)
                return new OperationResult(-4, "订单金额错误");
            #endregion
            TradeInfo detail = new TradeInfo
            {
                SysNo = Sequence.Get(),
                Amount = amount,
                PaymentChannel = Enumerations.PaymentChannel.POS,
                OrderNo = orderNo,
                SerialNo = serialNo,
                AccountName = accountName,
                BankAccountNo = bankAccountNo,
                BankCode = bankCode,
                SubBankName = bankName,
                CreateUserSysNo = operatorSysNo,
                CreateDate = DateTime.Now,
                Remark = remark,
                Status = TradeStatus.Submitted,
                PaymentDate = paymentDate,
                OrderBizType = order.OrderBizType
            };
            BillInfo bill = null;
            if (order.PaymentChannel == PaymentChannel.POS)
            {
                bill = new BillInfo
                {
                    SysNo = Sequence.Get(),
                    BillCategory = BillCategory.POS,
                    BillType = BillType.Expened,
                    Amount = amount,
                    BillDate = paymentDate,
                    BillNo = serialNo,
                    TradeNo = detail.TradeNo,
                    TerminalNo = terminalNo,
                    Decription = remark,
                    CreateUserSysNo = operatorSysNo,
                    CreateDate = DateTime.Now,
                    PayAccount = bankAccountNo,
                    Payer = accountName
                };
            }
            try
            {
                ServiceDepository.TransactionService.Begin();
                //创建流水
                if (!ServiceDepository.TradeDataAccess.Insert(detail))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-6, "交易明细创建失败");
                }
                //创建票据
                if (order.PaymentChannel == PaymentChannel.POS && !ServiceDepository.BillDataAccess.Insert(bill))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-7, "票据信息创建失败");
                }

                if (accountInfo == null)
                    accountInfo = ServiceDepository.AccountDataAccess.Get(order.AccountSysNo, order.OrderBizType);
                //修改预入账金额
                if (order.OrderType == OrderType.Collection)
                {
                    accountInfo.PreEntryMoney += amount;
                    accountInfo.ModifyUserSysNo = operatorSysNo;
                    accountInfo.ModifyDate = DateTime.Now;

                    if (!ServiceDepository.AccountDataAccess.Update(accountInfo))
                    {
                        ServiceDepository.TransactionService.RollBack();
                        return new OperationResult(-8, "账户操作失败");
                    }
                }

                #region ###################最后提交对账请求到财务系统###################
                if (!SubmitCollectionRequest(accountInfo, order, detail, terminalNo).Success)
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-9, "收款对账请求提交失败");
                }
                #endregion

                ServiceDepository.TransactionService.Commit();

                OperationResult result = new OperationResult(0, "交易成功");
                result.OtherData.Add("TradeSysNo", detail.SysNo);
                result.OtherData.Add("TradeNo", detail.TradeNo);
                return result;
            }
            catch (Exception e)
            {
                ServiceDepository.TransactionService.RollBack();
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }

        private OperationResult CancelCollectionTrade(string tradeNo, TradeStatus status, long operatorSysNo)
        {
            #region ###################参数校验###################
            if (string.IsNullOrWhiteSpace(tradeNo))
                return new OperationResult(-1, "交易编号不能为空");
            var trade = ServiceDepository.TradeDataAccess.Get(tradeNo);
            if (trade == null)
                return new OperationResult(-2, "交易信息不存在");
            if (trade.Status != TradeStatus.Submitted)
                return new OperationResult(-3, "交易状态错误");
            var order = ServiceDepository.OrderDataAccess.Get(trade.OrderNo);
            if (order == null)
                return new OperationResult(-4, "订单不存在");
            var account = ServiceDepository.AccountDataAccess.Get(order.AccountSysNo,order.OrderBizType);
            if (account == null)
                return new OperationResult(-6, "账户不存在");
            if (order.OrderType == OrderType.Collection && account.PreEntryMoney < trade.Amount)
                return new OperationResult(-7, "账户错误");
            #endregion

            trade.Status = status;
            trade.ModifyUserSysNo = operatorSysNo;
            account.PreEntryMoney -= trade.Amount;
            try
            {
                ServiceDepository.TransactionService.Begin();

                #region ###################事务保存###################
                if (!ServiceDepository.TradeDataAccess.Update(trade))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-8, "交易状态更新错误");
                }
                if (!ServiceDepository.AccountDataAccess.Update(account))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-9, "账户操作失败");
                }
                var sendResult = ServiceDepository.ExternalService.CancelCollection(tradeNo);
                if (!sendResult.Success)
                {
                    ServiceDepository.TransactionService.RollBack();
                    Log.Error(string.Format("交易撤销申请提交错误,{0},{1}", tradeNo, sendResult.ResultMessage));
                    return new OperationResult(-10, "交易撤销申请提交错误");
                }
                Log.Info(string.Format("交易撤销申请提交成功,{0}", tradeNo));
                #endregion

                ServiceDepository.TransactionService.Commit();
            }
            catch (Exception e)
            {
                ServiceDepository.TransactionService.RollBack();
                Log.Error(e);
                return new OperationResult(-5, "系统错误");
            }
            return new OperationResult(0);
        }

        private OperationResult ConfirmCollectionTrade(TradeInfo trade, OrderInfo order, AccountInfo account, long? operatorSysNo)
        {
            try
            {
                if (account.PreEntryMoney < trade.Amount)
                    return new OperationResult(-8, "账户异常");
                if (trade.ReconciliationStatus != ReconciliationStatus.Repetition)
                    account.PreEntryMoney -= trade.Amount;
                if (trade.ReconciliationStatus == ReconciliationStatus.Success)
                {
                    account.Balance += trade.Amount;
                    account.ReceiptBalance += trade.Amount;
                }
                if (operatorSysNo != null)
                    account.ModifyUserSysNo = operatorSysNo.Value;

                if (!ServiceDepository.AccountDataAccess.Update(account))
                    return new OperationResult(-11, "账户修改错误");

                if (trade.Status == TradeStatus.Completed && !ServiceDepository.AccountDetailDataAccess.Insert(new AccountDetailInfo(AccountDetailType.Recharge)
                {
                    AccountSysNo = account.SysNo,
                    Amount = trade.Amount,
                    OrderNo = trade.OrderNo,
                    CreateUserSysNo = operatorSysNo
                }))
                {
                    return new OperationResult(-13, "资金明细创建错误");
                }
                if (trade.Status == TradeStatus.Completed && !ServiceDepository.AccountDataAccess.AddOperatorLog(new AccountOperationLog
                {
                    AccountSysNo = account.SysNo,
                    Amount = Math.Abs(trade.Amount),
                    OperationType = AccountOperationType.Entry,
                    RefBizNo = trade.TradeNo,
                    Remark = string.Format("对账状态,{0}", trade.ReconciliationStatus.ToString()),
                    CreateUserSysNo = operatorSysNo,
                    CreateDate = DateTime.Now
                }))
                {
                    return new OperationResult(-12, "资金日志创建错误");
                }

                return new OperationResult(0);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, e.Message);
            }
        }

        //private OperationResult ReplenishCollectionTrade(long sysNo, string orderNo, long operatorSysNo)
        //{
        //    #region ###################参数校验###################
        //    if (string.IsNullOrWhiteSpace(orderNo))
        //        return new OperationResult(-1, "订单编号不能为空");
        //    var detail = ServiceDepository.MissCollectionTradeDataAccess.Get(sysNo);
        //    if (detail == null)
        //        return new OperationResult(-2, "掉单记录不存在");
        //    if (detail.Confirmed)
        //        return new OperationResult(-2, "重复操作");
        //    var order = ServiceDepository.OrderDataAccess.Get(orderNo);
        //    if (order == null)
        //        return new OperationResult(-3, "订单不存在");
        //    if (order.OrderStatus != OrderStatus.Initial)
        //        return new OperationResult(-4, "订单状态错误");

        //    var trades = ServiceDepository.TradeDataAccess.GetByOrder(orderNo);
        //    decimal tradeAmount = 0;
        //    if (trades != null && trades.Count > 0)
        //        tradeAmount = trades.Where(t => (t.Status != TradeStatus.Cancelled && t.Status != TradeStatus.Failed && t.Status != TradeStatus.Closed)).Sum(t => t.Amount);
        //    if ((order.OrderAmount - tradeAmount) < detail.Amount)
        //        return new OperationResult(-6, "订单金额错误");
        //    #endregion

        //    #region ###################逻辑处理###################
        //    TradeInfo trade = new TradeInfo
        //    {
        //        SysNo = Sequence.Get(),
        //        Amount = detail.Amount,
        //        PaymentChannel = PaymentChannel.Transfer,
        //        OrderNo = orderNo,
        //        SerialNo = detail.SerialNo,
        //        AccountName = detail.PayerName,
        //        BankAccountNo = detail.PaymentAccount,
        //        BankCode = detail.BankCode,
        //        SubBankName = detail.BankName,
        //        CreateUserSysNo = operatorSysNo,
        //        CreateDate = DateTime.Now,
        //        Remark = "补单",
        //        Status = TradeStatus.Submitted,
        //        PaymentDate = detail.PaymentDate
        //    };
        //    //修改预入账金额
        //    var accountInfo = ServiceDepository.AccountDataAccess.Get(order.AccountSysNo);
        //    accountInfo.PreEntryMoney += trade.Amount;
        //    accountInfo.ModifyUserSysNo = operatorSysNo;
        //    accountInfo.ModifyDate = DateTime.Now;
        //    #endregion

        //    try
        //    {
        //        ServiceDepository.TransactionService.Begin();

        //        #region ###################事务保存###################
        //        if (!ServiceDepository.MissCollectionTradeDataAccess.Confirm(detail.SysNo, trade.TradeNo, operatorSysNo))
        //        {
        //            Log.Error(string.Format("确认收款掉单数据失败,{0},{1}", detail.SysNo, trade.TradeNo));
        //            return new OperationResult(-7, "确认收款掉单数据失败");
        //        }

        //        //创建流水
        //        if (!ServiceDepository.TradeDataAccess.Insert(trade))
        //        {
        //            ServiceDepository.TransactionService.RollBack();
        //            return new OperationResult(-8, "交易明细创建失败");
        //        }

        //        if (!ServiceDepository.AccountDataAccess.Update(accountInfo))
        //        {
        //            ServiceDepository.TransactionService.RollBack();
        //            return new OperationResult(-9, "账户操作失败");
        //        }

        //        #region 最后提交对账请求到财务系统
        //        if (!SubmitCollectionRequest(accountInfo, order, trade, string.Empty).Success)
        //        {
        //            ServiceDepository.TransactionService.RollBack();
        //            return new OperationResult(-10, "收款对账请求提交失败");
        //        }
        //        #endregion

        //        #endregion

        //        ServiceDepository.TransactionService.Commit();

        //        OperationResult result = new OperationResult(0, "补单成功");
        //        result.OtherData.Add("TradeSysNo", detail.SysNo);
        //        result.OtherData.Add("TradeNo", detail.TradeNo);
        //        return result;
        //    }
        //    catch (Exception e)
        //    {
        //        ServiceDepository.TransactionService.RollBack();
        //        Log.Error(e);
        //        return new OperationResult(-5, "系统错误");
        //    }
        //}

    }
}
