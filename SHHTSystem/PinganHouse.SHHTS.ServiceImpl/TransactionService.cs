﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataAccess;

namespace PinganHouse.SHHTS.ServiceImpl
{
    public class TransactionService : ITransactionService
    {
        public bool Begin()
        {
            if (TransactionManager.Instance.IsTransactionStarted)
                return false;
            TransactionManager.Instance.BeginTransaction();
            return true;
        }

        public bool Commit()
        {
            if (!TransactionManager.Instance.IsTransactionStarted)
                return false;
            TransactionManager.Instance.CommitTransaction();
            return true;
        }

        public bool RollBack()
        {
            if (!TransactionManager.Instance.IsTransactionStarted)
                return false;
            TransactionManager.Instance.RollBackTransaction();
            return true;
        }
    }
}
