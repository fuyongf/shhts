﻿using System;
using System.Collections.Generic;
using System.ServiceModel.Activation;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.ServiceImpl
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public partial class SmsService : ISmsService
    {
        const string Factor="0123456789";
        const string SMS_CATEGORY_PREFIX = "HF_ESFSYSTEM_";
        public OperationResult Send(SmsCategory category, string mobile, IDictionary<string, object> parameters, bool mustSend=false)
        {
            if (string.IsNullOrWhiteSpace(mobile))
                return new OperationResult(-1, "手机号不能为空");
            var mobileFlag = ServiceDepository.SettingService.GetSettingItemValueByPath("/Biz/Static/MobileFlagConfig");
            if (!mustSend && (string.IsNullOrWhiteSpace(mobileFlag) || string.Compare(mobileFlag, "0") == 0))
                return new OperationResult(-2,"未开启发送短信开关");
            return ServiceDepository.ExternalService.SendSMS(string.Concat(SMS_CATEGORY_PREFIX, category.ToString()), mobile, parameters);
        }

        public OperationResult SendRegCode(string mobile)
        {
            string code = RandomHelper.GetNumber(9999999, 7);
            var result = Send(SmsCategory.REG, mobile, new Dictionary<string, object> { { SmsTemplateCode.CODE, code } });
            if (result.Success) result.OtherData.Add("Code", code);
            return result;
        }

        public OperationResult SendBindCaptcha(string mobile)
        {
            if(!Validation.Mobile(mobile))
                return new OperationResult(1,"手机号码不正确");
            var captcha = ServiceDepository.CaptchaService.RequetCaptcha(mobile, BusinessCacheType.SMS, Factor, 4);
            var dic = new Dictionary<string, object> { { "code", captcha.Value} };
            var result = this.Send(SmsCategory.USER_GET_OTHER_CODE, mobile, dic, true);
            if (result.Success)
                result.ResultMessage = captcha.Key;
            return result;
        }

        public OperationResult SendUpdatePwdCode(string mobile)
        {
            string code = RandomHelper.GetNumber(9999999, 7);
            var result = Send(SmsCategory.UPDATE_PASSWORD, mobile, new Dictionary<string, object> { { SmsTemplateCode.CODE, code } });
            if (result.Success) result.OtherData.Add("Code", code);
            return result;
        }

        public OperationResult SendUnBindMessage(string mobile, string name, string unbindMobile, string tailNum)
        {
            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(unbindMobile) || string.IsNullOrWhiteSpace(tailNum))
                return new OperationResult(-1, "参数错误");
            return Send(SmsCategory.UNBIND, mobile, new Dictionary<string, object> 
            {
                { SmsTemplateCode.NAME, name },
                { SmsTemplateCode.TEL, unbindMobile },
                { SmsTemplateCode.WEIHAO, tailNum }
            });
        }


        public OperationResult SendBindMessage(string mobile, string name, string tel, string idTailNum)
        {
            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(tel) || string.IsNullOrWhiteSpace(idTailNum))
                return new OperationResult(-1, "参数错误");
            return Send(SmsCategory.BIND, mobile, new Dictionary<string, object> 
            {
                { SmsTemplateCode.NAME, name },
                { SmsTemplateCode.TEL, tel },
                { SmsTemplateCode.WEIHAO, idTailNum }
            });
        }

        public OperationResult SendSignMessage(string mobile, string name, string tel, string idTailNum, DateTime signDate, string address)
        {
            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(tel)
                || string.IsNullOrWhiteSpace(idTailNum) || string.IsNullOrWhiteSpace(address))
                return new OperationResult(-1, "参数错误");
            return Send(SmsCategory.SIGN, mobile, new Dictionary<string, object> 
            {
                { SmsTemplateCode.NAME, name },
                { SmsTemplateCode.TEL, tel },
                { SmsTemplateCode.WEIHAO, idTailNum },
                { SmsTemplateCode.DATE, signDate.ToString("MM/dd") },
                { SmsTemplateCode.ADDR, address }
            });
        }

        public OperationResult SendPayAgentFeeMessage(string mobile, string name, string tel, string address, decimal agentFee)
        {
            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(tel) || string.IsNullOrWhiteSpace(address))
                return new OperationResult(-1, "参数错误");
            return Send(SmsCategory.JIAOFANG, mobile, new Dictionary<string, object> 
            {
                { SmsTemplateCode.NAME, name },
                { SmsTemplateCode.TEL, tel },
                { SmsTemplateCode.MONEY, agentFee.ToString("f2") },
                { SmsTemplateCode.ADDR, address }
            });
        }

        public OperationResult SendCreditMaterialNotice(string mobile, string name, string tel)
        {
            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(tel))
                return new OperationResult(-1, "参数错误");
            return Send(SmsCategory.DK_CL, mobile, new Dictionary<string, object> 
            {
                { SmsTemplateCode.NAME, name },
                { SmsTemplateCode.TEL, tel }
            });
        }

        public OperationResult SendTradeMaterialNotice(string mobile, string name, string tel)
        {
            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(tel))
                return new OperationResult(-1, "参数错误");
            return Send(SmsCategory.JY_CL, mobile, new Dictionary<string, object> 
            {
                { SmsTemplateCode.NAME, name },
                { SmsTemplateCode.TEL, tel }
            });
        }

        public OperationResult SendCaseArrangeMessage(string mobile, string name, string tel, DateTime date, string center)
        {
            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(tel) || string.IsNullOrWhiteSpace(center))
                return new OperationResult(-1, "参数错误");
            return Send(SmsCategory.JYANJ_YUYUE, mobile, new Dictionary<string, object> 
            {
                { SmsTemplateCode.NAME, name },
                { SmsTemplateCode.TEL, tel },
                { SmsTemplateCode.DATE, date.ToString("MM/dd日HH时") },
                { SmsTemplateCode.QU, center }
            });
        }

        public OperationResult SendTransferArrangeMessage(string mobile, string name, string tel, DateTime date, string center)
        {
            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(tel) || string.IsNullOrWhiteSpace(center))
                return new OperationResult(-1, "参数错误");
            return Send(SmsCategory.GUOHU, mobile, new Dictionary<string, object> 
            {
                { SmsTemplateCode.NAME, name },
                { SmsTemplateCode.TEL, tel },
                { SmsTemplateCode.DATE, date.ToString("MM/dd日HH时") },
                { SmsTemplateCode.QU, center }
            });
        }

        public OperationResult SendPostBillNotice(string mobile)
        {
            return Send(SmsCategory.SHOUJU_SEND, mobile, null);
        }

        public OperationResult SendCollectionNotice(string mobile, DateTime date, string bankCardTailNum, decimal amount)
        {
            if (string.IsNullOrWhiteSpace(bankCardTailNum))
                return new OperationResult(-1, "参数错误");
            return Send(SmsCategory.DS_ZHUANGZHANG, mobile, new Dictionary<string, object> 
            {
                { SmsTemplateCode.DATE, date.ToString("MM/dd") },
                { SmsTemplateCode.WEIHAO, bankCardTailNum },
                { SmsTemplateCode.MONEY, amount.ToString("f2") }
            });
        }

        public OperationResult SendEntrustPayNotice(string mobile, decimal amount)
        {
            return Send(SmsCategory.DS_DAIKOU, mobile, new Dictionary<string, object> 
            {
                { SmsTemplateCode.MONEY, amount.ToString("f2") }
            });
        }

        public OperationResult SendWithdrawNotice(string mobile, decimal amount, string bankCardTailNum, DateTime date)
        {
            if (string.IsNullOrWhiteSpace(bankCardTailNum))
                return new OperationResult(-1, "参数错误");
            return Send(SmsCategory.DS_QUEREN, mobile, new Dictionary<string, object> 
            {
                { SmsTemplateCode.DATE, date.ToString("MM/dd HH:mm") },
                { SmsTemplateCode.WEIHAO, bankCardTailNum },
                { SmsTemplateCode.MONEY, amount.ToString("f2") }
            });
        }

        public OperationResult SendConfirmCaseMessage(string mobile, string name, string name1, string tel)
        {
            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(name1) || string.IsNullOrWhiteSpace(tel))
                return new OperationResult(-1, "参数错误");
            return Send(SmsCategory.LIAN_QUEREN, mobile, new Dictionary<string, object> 
            {
                { SmsTemplateCode.NAME, name },
                { SmsTemplateCode.NAME1, name1 },
                { SmsTemplateCode.TEL, tel}
            });
        }

        public OperationResult SendRepayArrangeMessage(string mobile, DateTime date)
        {
            return Send(SmsCategory.HUANDAI_YUYUE_QUEREN1, mobile, new Dictionary<string, object> 
            {
                { SmsTemplateCode.DATE, date.ToString("MM/dd") },
            });
        }

        public OperationResult SendLimitBuyMessage(string mobile, string result)
        {
            if (string.IsNullOrWhiteSpace(result))
                return new OperationResult(-1, "参数错误");
            return Send(SmsCategory.XIANGOUJIEGUO, mobile, new Dictionary<string, object> { { "result", result } });
        }

        public OperationResult SendDeliveryHouseNotice(string mobile)
        {
            return Send(SmsCategory.JIAOFANG_NOTICE, mobile, null);
        }

        public OperationResult SendGetCredentialNotice(string mobile, string address, string date)
        {
            if (string.IsNullOrWhiteSpace(address) || string.IsNullOrWhiteSpace(date))
                return new OperationResult(-1, "参数错误");
            return Send(SmsCategory.LINGZHENG, mobile, new Dictionary<string, object> 
            {
                { SmsTemplateCode.ADDR, address } ,
                { SmsTemplateCode.DATE,date }
            });
        }

        public OperationResult SendHandleTransferNotice(string mobile, string address, string date, string center, string name, string tel)
        {
            if (string.IsNullOrWhiteSpace(address) || string.IsNullOrWhiteSpace(center)
                || string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(tel) || string.IsNullOrWhiteSpace(date))
                return new OperationResult(-1, "参数错误");
            return Send(SmsCategory.JIAOYI_GUOHU, mobile, new Dictionary<string, object> 
            {
                { SmsTemplateCode.ADDR, address } ,
                { SmsTemplateCode.DATE,date },
                { SmsTemplateCode.QU, center } ,
                { SmsTemplateCode.NAME,name },
                { SmsTemplateCode.TEL,tel }
            });
        }

        public OperationResult SendCreditPassNotice(string mobile, string name, string tel)
        {
            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(tel))
                return new OperationResult(-1, "参数错误");
            return Send(SmsCategory.DAIKUANG_PASS, mobile, new Dictionary<string, object> 
            {
                { SmsTemplateCode.NAME, name },
                { SmsTemplateCode.TEL, tel }
            });
        }

        public OperationResult SendRepayArrange2Message(string mobile, decimal amount, DateTime date)
        {
            return Send(SmsCategory.YUYUE_TQ_HUANDAI, mobile, new Dictionary<string, object> 
            {
                { SmsTemplateCode.MONEY, amount.ToString("f2") } ,
                { SmsTemplateCode.DATE,date.ToString("MM/dd") }
            });
        }

        public OperationResult SendRepayArrange3Message(string mobile, decimal amount, DateTime date1, DateTime date2)
        {
            return Send(SmsCategory.YUYUE_TQ_HUANDAI3, mobile, new Dictionary<string, object> 
            {
                { SmsTemplateCode.MONEY, amount.ToString("f2") },
                { SmsTemplateCode.DATE,date1.ToString("MM/dd") },
                { SmsTemplateCode.DATE2,date2.ToString("MM/dd") }
            });
        }

        public OperationResult SendSelfRepay3Message(string mobile, decimal amount, DateTime date1, DateTime date2)
        {
            return Send(SmsCategory.ZX_YUYUE_HUANDAI3, mobile, new Dictionary<string, object> 
            {
                { SmsTemplateCode.MONEY, amount.ToString("f2") },
                { SmsTemplateCode.DATE,date1.ToString("MM/dd") },
                { SmsTemplateCode.DATE2,date2.ToString("MM/dd") }
            });
        }

        public OperationResult SendSelfRepay2Message(string mobile, decimal amount, DateTime date)
        {
            return Send(SmsCategory.ZX_YUYUE_HUANDAI2, mobile, new Dictionary<string, object> 
            {
                { SmsTemplateCode.MONEY, amount.ToString("f2") },
                { SmsTemplateCode.DATE,date.ToString("MM/dd") },
            });
        }

        public OperationResult SendSelfRepayMessage(string mobile, DateTime date)
        {
            return Send(SmsCategory.ZX_YUYUE_HUANDAI1, mobile, new Dictionary<string, object> 
            {
                { SmsTemplateCode.DATE,date.ToString("MM/dd") },
            });
        }

        internal class SmsTemplateCode
        {
            public const string CODE = "code";
            public const string NAME = "name";
            public const string NAME1 = "name1";
            public const string TEL = "tel";
            public const string DATE = "date";
            public const string DATE2 = "date2";
            public const string ADDR = "addr";
            public const string MONEY = "money";
            public const string WEIHAO = "weihao";
            public const string QU = "qu";
        }
    }
}
