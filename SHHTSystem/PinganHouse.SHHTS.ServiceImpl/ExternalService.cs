﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.DataTransferObjects.External;

namespace PinganHouse.SHHTS.ServiceImpl
{
    public class ExternalService : IExternalService
    {
        const string SMS_PARAMS_TEMPLATE = "params[{0}]";

        public OperationResult SubmitCollection(string tradeNo, string serialNo, string caseId, string orderNo, decimal orderAmount, int channel, string tradeType, string payer, string payerAccount, decimal tradeAmount, string bankName, DateTime tradeDate, string terminalNo, string store, string pAddr)
        {
            try
            {
                IDictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("sBusinessID", tradeNo);
                parameters.Add("sBankSerialNum", serialNo);
                parameters.Add("sCaseID", caseId);
                parameters.Add("sOrderID", orderNo);
                parameters.Add("iOrderAmount", (long)(orderAmount * 100));
                parameters.Add("iPayType", channel);
                parameters.Add("sPayee", payer);
                parameters.Add("iPayAmount", (long)(tradeAmount * 100));
                parameters.Add("sBank", bankName);
                parameters.Add("iTradeDate", tradeDate.ToString("yyyyMMdd"));
                parameters.Add("sPosID", terminalNo);
                parameters.Add("sStoreID", store);
                parameters.Add("sPropertyAddr", pAddr);
                if (!string.IsNullOrWhiteSpace(payerAccount))
                    parameters.Add("sPayeeAccount", payerAccount);
                if (!string.IsNullOrWhiteSpace(tradeType))
                    parameters.Add("sPayDesc", tradeType);

                var result = ServiceDepository.PartnerService.SendData("collection", parameters);
                if (!result.Success)
                    Log.Error(string.Format("对账交易提交失败,{0},{1}", result.ResultNo, result.ResultMessage));
                else
                    Log.Info(string.Format("对账请求提交成功,{0},{1}", tradeNo, tradeAmount));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }

        public OperationResult CancelCollection(string tradeNo)
        {
            if (string.IsNullOrWhiteSpace(tradeNo))
                return new OperationResult(-1, "交易编号不能为空");
            try
            {
                var result = ServiceDepository.PartnerService.SendData("cancel", new Dictionary<string, object> { { "sBusinessID", tradeNo } });
                if (!result.Success)
                    Log.Error(string.Format("代收请求提交失败,{0},{1}", result.ResultNo, result.ResultMessage));
                else
                    Log.Info(string.Format("代收请求提交成功,{0},{1}", tradeNo, tradeNo));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tradeNo">流水号</param>
        /// <param name="billNo">业务票据流水编码</param>
        /// <returns></returns>
        public OperationResult SendReceipt(string tradeNo, string billNo)
        {
            if (string.IsNullOrWhiteSpace(tradeNo) || string.IsNullOrWhiteSpace(billNo))
                return new OperationResult(-1, "交易编号和收据编号不能为空");
            try
            {
                var result = ServiceDepository.PartnerService.SendData("receipt", new Dictionary<string, object> { { "sBusinessID", tradeNo }, { "sReceiptNum", billNo } });
                if (!result.Success)
                    Log.Error(string.Format("新增收据请求提交失败,{0},{1}", result.ResultNo, result.ResultMessage));
                else
                    Log.Info(string.Format("新增收据提交成功,{0},{1}", tradeNo, tradeNo));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }

        public OperationResult UpdatePropertyAddress(string caseId, string address)
        {
            if (string.IsNullOrWhiteSpace(caseId) || string.IsNullOrWhiteSpace(address))
                return new OperationResult(-1, "交易编号和收据编号不能为空");
            try
            {
                var result = ServiceDepository.PartnerService.SendData("property", new Dictionary<string, object> { { "sCaseID", caseId }, { "sPropertyAddr", address } });
                if (!result.Success)
                    Log.Error(string.Format("新增收据请求提交失败,{0},{1}", result.ResultNo, result.ResultMessage));
                else
                    Log.Info(string.Format("新增收据提交成功,{0},{1}", caseId, address));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }

        public OperationResult SubmitPayment(string caseId, string tradeNo, string pAddr, decimal amount, string bankCode, string accountNo, string payee, string tradeType)
        {
            try
            {
                IDictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("sCaseID", caseId);
                parameters.Add("sBusinessOrderID", tradeNo);
                parameters.Add("sPropertyAddr", pAddr);
                parameters.Add("iAmount", (long)(Math.Abs(amount) * 100));
                parameters.Add("iBankID", ServiceDepository.ConfigService.GetBankIdByCode(bankCode));
                parameters.Add("sBankAccount", accountNo);
                parameters.Add("sBankAccountName", payee);
                parameters.Add("sPayDesc", tradeType);

                var result = ServiceDepository.PartnerService.SendData("pay", parameters);
                if (!result.Success)
                    Log.Error(string.Format("支付请求提交失败,{0},{1}", result.ResultNo, result.ResultMessage));
                else
                    Log.Info(string.Format("支付请求提交成功,{0},{1}", tradeNo, tradeNo));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }

        public OperationResult SubmitPaidOff(string caseId, string tradeNo, string pAddr, decimal amount, string bankName, string accountNo, string payee, string tradeType) 
        {
            try
            {
                IDictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("sCaseID", caseId);
                parameters.Add("sBusinessOrderID", tradeNo);
                parameters.Add("sPropertyAddr", pAddr);
                parameters.Add("iAmount", (long)(Math.Abs(amount) * 100));
                parameters.Add("iBankID", 0);
                parameters.Add("sBankName", bankName);
                parameters.Add("sBankAccount", accountNo);
                parameters.Add("sBankAccountName", payee);
                parameters.Add("sPayDesc", tradeType);

                var result = ServiceDepository.PartnerService.SendData("paidoff", parameters);
                if (!result.Success)
                    Log.Error(string.Format("销帐请求提交失败,{0},{1}", result.ResultNo, result.ResultMessage));
                else
                    Log.Info(string.Format("销帐请求提交成功,{0},{1}", tradeNo, tradeNo));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }

        public IList<FsBankInfo> GetBanks()
        {
            try
            {
                var result = ServiceDepository.PartnerService.SendData<FsBankInfo>("bank", null);
                if (result == null)
                    return null;
                if (!result.Success || !result.OtherData.ContainsKey("Data"))
                {
                    Log.Error("获取银行列表失败");
                    return null;
                }
                return result.OtherData["Data"] as IList<FsBankInfo>;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return null;
            }
        }

        public OperationResult AuthenticateBankCard(string name, string idcard, string cellphone, string cardnumber, string bankshort)
        {
            try
            {
                IDictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("name", name);
                parameters.Add("idcard", idcard);
                parameters.Add("cellphone", cellphone);
                parameters.Add("cardnumber", cardnumber);
                parameters.Add("bankshort", bankshort);

                var result = ServiceDepository.PartnerService.SendData("bankCardAuth", parameters);
                if (!result.Success)
                    Log.Error(string.Format("银行卡验证请求提交失败,{0},{1}/{2},{3},{4},{5},{6}", result.ResultNo, result.ResultMessage, name, idcard, cellphone, cardnumber, bankshort));
                else
                    Log.Info(string.Format("银行卡验证请求提交成功,{0},{1},{2},{3},{4}", name, idcard, cellphone, cardnumber, bankshort));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }

        public IList<FsIdentifyBankInfo> GetIdentifyBanks() 
        {
            try
            {
                IDictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("type", 0);

                var result = ServiceDepository.PartnerService.SendData<FsIdentifyBankInfo>("identifyBank", parameters);
                if (!result.Success)
                    Log.Error(string.Format("获取支持鉴权银行信息请求提交失败,{0},{1}", result.ResultNo, result.ResultMessage));
                else
                    Log.Info("获取支持鉴权银行信息请求提交成功");
                return result.OtherData["Data"] as IList<FsIdentifyBankInfo>;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return null;
            }
        }

        public OperationResult SendSMS(string bcode, string mobile, IDictionary<string, object> parameters)
        {
            try
            {
                parameters = parameters ?? new Dictionary<string, object>();
                string requestIp = IPUtils.GetWebClientIp();

                IDictionary<string, object> data = new Dictionary<string, object>();
                data.Add("bcode", bcode);
                data.Add("clientip", IPUtils.GetServerCurrentIp());
                data.Add("clientid", string.IsNullOrEmpty(requestIp) ? Guid.NewGuid().ToString("N") : requestIp);
                data.Add("mobile", mobile);
                foreach (var p in parameters)
                {
                    data.Add(string.Format(SMS_PARAMS_TEMPLATE, p.Key), p.Value);
                }

                var result = ServiceDepository.PartnerService.SendData("sms", data);
                if (!result.Success)
                    Log.Error(string.Format("短信发送请求提交失败,{0},{1}", result.ResultNo, result.ResultMessage));
                else
                    Log.Info(string.Format("短信发送请求提交成功,{0},{1}", bcode, mobile));
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }
    }
}
