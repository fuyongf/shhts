﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using HTB.DevFx;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.ServiceImpl
{
    public class UserAuthProviderFactory 
    {
        public static IUserAuthProvider GetProvider(UserType userType)
        {
            switch (userType) 
            {
                case UserType.Customer: 
                case UserType.AgentStaff :
                case UserType.ThirdpartyUser :
                case UserType.PinganStaff:
                    return ObjectService.GetObject<IUserAuthProvider>("SystemAuthProvider");
                //case UserType.PinganStaff: return ObjectService.GetObject<IUserAuthProvider>("PinganUMAuthProvider");
                default: return null;
            }
        }
    }   
}
