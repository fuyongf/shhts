﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;

namespace PinganHouse.SHHTS.ServiceImpl.UserAuthProviders
{
    public class SystemAuthProvider : IUserAuthProvider
    {
        public OperationResult Authenticate(string loginId, string password)
        {
            if (string.IsNullOrEmpty(loginId) || string.IsNullOrEmpty(password))
                return new OperationResult(-1,"用户名和密码不能为空");
            try
            {
                var userInfo = ServiceDepository.UserAccountDataAccess.GetByLoginId(loginId);
                if (userInfo == null)
                    return new OperationResult(-2, "用户不存在");
                if (userInfo.AuthDisabled)
                    return new OperationResult(-3, "该用户禁止登陆");
                if (userInfo.IsLocked)
                    return new OperationResult(-4, "该用户已被锁定");
                if (string.IsNullOrEmpty(userInfo.PassWord))
                    return new OperationResult(1, "请重置密码");
                if (string.Compare(userInfo.PassWord, EncryptHelper.GetMD5(password), false) != 0)
                    return new OperationResult(-5, "密码错误");
                ServiceDepository.UserAccountDataAccess.ResetLoginDate(userInfo.SysNo, DateTime.Now);
            }
            catch (Exception e) 
            {
                Log.Error(e);
                return new OperationResult(-9, "系统异常");
            }
            return new OperationResult(0);
        }
    }
}
