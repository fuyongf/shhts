﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.ServiceImpl
{
    public partial class TradeService : ITradeService
    {
        public OperationResult SubmitEntrustPay(AccountType payerType, string payerSubjectId, AccountType payeeType, string payeeSubjectId, OrderBizType tradeType, decimal amount, string bankAccountNo, string bankCode, string accountName, string bankName, string remark, long operatorSysNo)
        {
            #region ###################参数验证###################
            if (string.IsNullOrWhiteSpace(payerSubjectId) || string.IsNullOrWhiteSpace(payeeSubjectId) || amount <= 0)
                return new OperationResult(-1, "非法参数信息");
            if (string.IsNullOrWhiteSpace(bankCode) || string.IsNullOrWhiteSpace(accountName) || string.IsNullOrWhiteSpace(bankAccountNo))
                return new OperationResult(-2, "银行帐户信息错误");
            var payerAccount = ServiceDepository.AccountDataAccess.Get(payerSubjectId, payerType, tradeType);
            if (payerAccount == null)
                return new OperationResult(-3, "付款账户不存在");
            if (payerAccount.IsLocked)
                return new OperationResult(-3, "付款账户已锁定");
            if (payerAccount.Balance < amount)
                return new OperationResult(-3, "付款账户余额不足");
            var payeeAccount = ServiceDepository.AccountDataAccess.Get(payeeSubjectId, payeeType, tradeType);
            if (payeeAccount == null)
                return new OperationResult(-9, "收款账户不存在");
            #endregion

            try
            {
                ServiceDepository.TransactionService.Begin();
                #region ###################创建订单###################
                OrderInfo order = new OrderInfo
                {
                    SysNo = Sequence.Get(),
                    AccountSysNo = payerAccount.SysNo,
                    OppositeAccount = payeeAccount.SysNo,
                    OrderAmount = amount,
                    OrderType = OrderType.Payment,
                    OrderBizType = tradeType,
                    PaymentChannel = PaymentChannel.Transfer,
                    OrderStatus = OrderStatus.Initial,
                    Remark = remark,
                    CreateUserSysNo = operatorSysNo,
                    CreateDate = DateTime.Now
                };
                if (!ServiceDepository.OrderDataAccess.Insert(order))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-4, "订单创建失败");
                }
                #endregion


                #region ###################订单扩展信息###################
                var extend = new PaymentOrderExtend
                {
                    OrderNo = order.OrderNo,
                    AccountName = accountName,
                    BankAccountNo = bankAccountNo,
                    BankCode = bankCode,
                    SubBankName = bankName,
                    CreateUserSysNo = operatorSysNo
                };
                if (!ServiceDepository.OrderDataAccess.InsertPaymentExtend(extend))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-6, "订单创建失败");
                }
                #endregion

                #region ###################锁定出账金额###################
                payerAccount.Balance -= order.OrderAmount;
                payerAccount.LockedMoney += order.OrderAmount;
                payerAccount.ModifyUserSysNo = operatorSysNo;
                if (!ServiceDepository.AccountDataAccess.Update(payerAccount))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-7, "资金锁定操作失败");
                }
                //资金日志
                var log = new AccountOperationLog
                {
                    AccountSysNo = payerAccount.SysNo,
                    Amount = order.OrderAmount,
                    OperationType = AccountOperationType.Lock,
                    RefBizNo = order.OrderNo,
                    Remark = string.Format("出账锁定：{0}", order.OrderNo),
                    CreateUserSysNo = operatorSysNo,
                    CreateDate = DateTime.Now
                };
                if (!ServiceDepository.AccountDataAccess.AddOperatorLog(log))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-8, "操作日志创建失败");
                }
                #endregion
                var result = new OperationResult(0);
                result.OtherData.Add("OrderNo", order.OrderNo);

                ServiceDepository.TransactionService.Commit();
                return result;
            }
            catch (Exception e)
            {
                ServiceDepository.TransactionService.RollBack();
                Log.Error(e);
                return new OperationResult(-5, "系统错误");
            }
        }

        private OperationResult ConfirmPaymentTrade(TradeInfo trade, OrderInfo order, PaymentStatus status, string rejectReason, long? operatorSysNo)
        {
            if (order.OrderStatus != OrderStatus.Submitted)
                return new OperationResult(-6, "订单状态错误");
            var account = ServiceDepository.AccountDataAccess.Get(order.OppositeAccount.Value, order.OrderBizType);
            if (account == null || account.LockedMoney < Math.Abs(trade.Amount))
                return new OperationResult(-7, "账户异常");
            try
            {
                #region ###################修改订单信息###################
                if (operatorSysNo != null)
                    order.ModifyUserSysNo = operatorSysNo;

                if (status == PaymentStatus.FinanceAuditPass || status == PaymentStatus.FinanceAuditFailed)
                {
                    AuditStatus auditStatus = status == PaymentStatus.FinanceAuditPass ? AuditStatus.FinanceAudited : AuditStatus.FinanceAuditFailed;
                    #region 订单审核日志
                    if (!ServiceDepository.AuditLogDataAccess.Insert(new AuditLogInfo
                    {
                        AuditObjectType = AuditObjectType.Order,
                        ChangeValue = (int)auditStatus,
                        Department = "总部财务",
                        SubjectId = order.OrderNo,
                        Remark = rejectReason
                    }))
                    {
                        return new OperationResult(-13, "审核日志创建失败");
                    }
                    #endregion
                    order.AuditStatus = auditStatus;
                }
                if (status != PaymentStatus.FinanceAuditPass)
                {
                    order.OrderStatus = status == PaymentStatus.Success ? OrderStatus.Completed : OrderStatus.Closed;
                }
                if (!ServiceDepository.OrderDataAccess.Update(order))
                {
                    return new OperationResult(-11, "更新订单状态错误");
                }
                #endregion

                #region ###################修改资金账户信息###################
                if (status != PaymentStatus.FinanceAuditPass)
                {
                    account.LockedMoney -= Math.Abs(trade.Amount);
                    if (status == PaymentStatus.FinanceAuditFailed || status == PaymentStatus.Failed)
                        account.Balance += Math.Abs(trade.Amount);
                    if (!ServiceDepository.AccountDataAccess.Update(account))
                    {
                        return new OperationResult(-9, "更新账户信息错误");
                    }
                    //account log
                    if (!ServiceDepository.AccountDataAccess.AddOperatorLog(new AccountOperationLog
                    {
                        AccountSysNo = account.SysNo,
                        Amount = Math.Abs(trade.Amount),
                        OperationType = status == PaymentStatus.Success ? AccountOperationType.ChargeOff : AccountOperationType.UnLock,
                        RefBizNo = trade.TradeNo,
                        Remark = string.Format("支付状态,{0}", status.ToString()),
                        CreateUserSysNo = operatorSysNo,
                        CreateDate = DateTime.Now
                    }))
                    {
                        return new OperationResult(-10, "创建资金日志错误");
                    }
                    //资金账户明细
                    if (status == PaymentStatus.Success && !ServiceDepository.AccountDetailDataAccess.Insert(new AccountDetailInfo(AccountDetailType.Withdraw)
                    {
                        AccountSysNo = account.SysNo,
                        Amount = -order.OrderAmount,
                        OrderNo = order.OrderNo,
                        CreateUserSysNo = operatorSysNo
                    }))
                    {
                        return new OperationResult(-11, "创建资金明细错误");
                    }
                }
                #endregion

                return new OperationResult(0);
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }

        public OperationResult PaymentOrderAudit(ReceptionCenter center, string orderNo, AuditStatus status, string remark, long operatorSysNo, string operatorName)
        {
            #region #################参数验证#################
            if (string.IsNullOrWhiteSpace(orderNo))
                return new OperationResult(-1, "非法参数");
            var order = ServiceDepository.OrderDataAccess.Get(orderNo);
            if (order == null)
                return new OperationResult(-2, "订单不存在");

            var preStatus = ServiceDepository.ConfigService.GetPaymentOrderAuditPreStatus(center, status);
            if (!preStatus.Contains(order.AuditStatus))
                return new OperationResult(-3, "交易状态错误");
            var auditors = ServiceDepository.ConfigService.GetPaymentOrderAuditors(center, status);
            if (auditors == null || auditors.Count == 0 || !auditors.Contains(operatorSysNo))
                return new OperationResult(-4, "无此操作权限");
            #endregion
            var expendOrders = new List<OrderInfo>();
            try
            {
                var log = new AuditLogInfo
                {
                    AuditObjectType = AuditObjectType.Order,
                    ChangeValue = (int)status,
                    SubjectId = order.OrderNo,
                    AuditSysNo = operatorSysNo,
                    AuditUserName = operatorName,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = operatorSysNo,
                    Remark = remark
                };

                ServiceDepository.TransactionService.Begin();
                #region #################审核日志#################
                if (!ServiceDepository.AuditLogDataAccess.Insert(log))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-7, "审核日志创建失败");
                }
                #endregion

                #region #################订单状态#################
                order.AuditStatus = status;
                if (status == AuditStatus.ReAuditFailed || status == AuditStatus.AuditFailed || status == AuditStatus.FinanceAuditFailed)
                    order.OrderStatus = OrderStatus.Closed;
                order.ModifyUserSysNo = operatorSysNo;
                if (!ServiceDepository.OrderDataAccess.Update(order))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-9, string.Format("订单:{0},状态修改失败", order.OrderNo));
                }
                #endregion

                #region #################资金操作#################
                if (status == AuditStatus.ReAuditFailed || status == AuditStatus.AuditFailed) // 审核拒绝，回滚锁定资金
                {
                    var account = ServiceDepository.AccountDataAccess.Get(order.AccountSysNo, order.OrderBizType);
                    if (account.LockedMoney < order.OrderAmount)
                    {
                        ServiceDepository.TransactionService.RollBack();
                        return new OperationResult(-10, "账户异常");
                    }
                    account.Balance += order.OrderAmount;
                    account.LockedMoney -= order.OrderAmount;
                    account.ModifyUserSysNo = operatorSysNo;
                    //更新余额
                    if (!ServiceDepository.AccountDataAccess.Update(account))
                    {
                        ServiceDepository.TransactionService.RollBack();
                        return new OperationResult(-10, "资金解锁错误");
                    }
                    //资金操作日志
                    if (!ServiceDepository.AccountDataAccess.AddOperatorLog(new AccountOperationLog
                    {
                        AccountSysNo = account.SysNo,
                        Amount = Math.Abs(order.OrderAmount),
                        OperationType = AccountOperationType.UnLock,
                        RefBizNo = order.OrderNo,
                        Remark = string.Format("支付状态,{0}", status.ToString()),
                        CreateUserSysNo = operatorSysNo,
                        CreateDate = DateTime.Now
                    }))
                    {
                        ServiceDepository.TransactionService.RollBack();
                        return new OperationResult(-10, "资金日志错误");
                    }
                }
                else
                {
                    //代付订单，先转账，再提现
                    if (order.OrderType == OrderType.Payment || order.OrderType == OrderType.PaidOff)
                    {
                        AuditStatus submitStatus = ServiceDepository.ConfigService.GetPaymentSubmitCondition();
                        if (order.AuditStatus == submitStatus)
                        {
                            #region #################转账操作#################
                            var payerAccount = ServiceDepository.AccountDataAccess.Get(order.AccountSysNo, order.OrderBizType);
                            var payeeAccount = ServiceDepository.AccountDataAccess.Get(order.OppositeAccount.Value, order.OrderBizType);
                            if (!SubmitTransfer(order, payerAccount, payeeAccount, operatorSysNo).Success)
                            {
                                ServiceDepository.TransactionService.RollBack();
                                return new OperationResult(-11, "转账失败");
                            }
                            #endregion

                            decimal orderBalance = order.OrderAmount;
                            #region #################检查收款账户扣款计划，支付消费订单#################
                            if (order.OrderType == OrderType.Payment) //销帐订单不做虚拟账户扣款，因为钱已经出去了
                            {
                                var schedules = ServiceDepository.DeductScheduleDataAccess.GetByAccount(payeeAccount.SysNo, false);
                                if (schedules != null && schedules.Count > 0)
                                {
                                    var deductItems = schedules.Where(s => s.LimitSource == null || s.LimitSource.Contains(order.OrderBizType));
                                    if (deductItems != null && deductItems.Count() > 0)
                                    {
                                        foreach (var deductItem in deductItems)
                                        {
                                            if (orderBalance == 0)
                                                break;
                                            decimal diffAmount = deductItem.ApplyAmount - deductItem.DeductedAmount;
                                            decimal deductAmount = diffAmount < order.OrderAmount ? diffAmount : order.OrderAmount;
                                            deductItem.DeductedAmount += deductAmount;
                                            deductItem.ModifyUserSysNo = operatorSysNo;
                                            //更新计划
                                            if (!ServiceDepository.DeductScheduleDataAccess.Update(deductItem))
                                            {
                                                ServiceDepository.TransactionService.RollBack();
                                                return new OperationResult(-12, "扣费失败");
                                            }
                                            //创建消费订单
                                            var expendOrder = new OrderInfo
                                            {
                                                SysNo = Sequence.Get(),
                                                OrderType = OrderType.Expend,
                                                AccountSysNo = payeeAccount.SysNo,
                                                OrderBizType = deductItem.DeductType,
                                                OrderAmount = deductAmount,
                                                OrderStatus = Enumerations.OrderStatus.Paid,
                                                PaymentChannel = Enumerations.PaymentChannel.Balance,
                                                CreateUserSysNo = operatorSysNo,
                                                Remark = order.OrderNo
                                            };
                                            if (!ServiceDepository.OrderDataAccess.Insert(expendOrder))
                                            {
                                                ServiceDepository.TransactionService.RollBack();
                                                return new OperationResult(-12, "扣费失败");
                                            }
                                            //更新账户余额
                                            payeeAccount.Verson++;
                                            payeeAccount.Balance -= deductAmount;
                                            payeeAccount.ModifyUserSysNo = operatorSysNo;
                                            if (!ServiceDepository.AccountDataAccess.Update(payeeAccount))
                                            {
                                                ServiceDepository.TransactionService.RollBack();
                                                return new OperationResult(-12, "扣费失败");
                                            }
                                            //创建资金明细
                                            if (!ServiceDepository.AccountDetailDataAccess.Insert(new AccountDetailInfo(AccountDetailType.Transfer)
                                            {
                                                AccountSysNo = payeeAccount.SysNo,
                                                Amount = -deductAmount,
                                                OrderNo = expendOrder.OrderNo,
                                                CreateUserSysNo = operatorSysNo
                                            }))
                                            {
                                                ServiceDepository.TransactionService.RollBack();
                                                return new OperationResult(-12, "扣费失败");
                                            }
                                            //创建资金账户日志
                                            if (!ServiceDepository.AccountDataAccess.AddOperatorLog(new AccountOperationLog
                                            {
                                                AccountSysNo = payeeAccount.SysNo,
                                                Amount = deductAmount,
                                                OperationType = AccountOperationType.ChargeOff,
                                                RefBizNo = expendOrder.OrderNo,
                                                CreateUserSysNo = operatorSysNo,
                                                CreateDate = DateTime.Now
                                            }))
                                            {
                                                ServiceDepository.TransactionService.RollBack();
                                                return new OperationResult(-12, "扣费失败");
                                            }
                                            expendOrders.Add(expendOrder);
                                            orderBalance -= deductAmount;
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region #################提现#################
                            if (orderBalance > 0)
                            {
                                string accountName, bankAccountNo, bankCode, bankName;
                                if (order.OrderType == OrderType.Payment)
                                {
                                    var orderExpend = ServiceDepository.OrderDataAccess.GetPaymentExtend(order.OrderNo);
                                    if (orderExpend == null)
                                    {
                                        ServiceDepository.TransactionService.RollBack();
                                        return new OperationResult(-13, "出款失败，出款帐号信息错误");
                                    }
                                    accountName = orderExpend.AccountName;
                                    bankAccountNo = orderExpend.BankAccountNo;
                                    bankCode = orderExpend.BankCode;
                                    bankName = orderExpend.SubBankName;
                                }
                                else if (order.OrderType == OrderType.PaidOff)
                                {
                                    var orderExpend = ServiceDepository.OrderDataAccess.GetPaidOffExtend(order.OrderNo);
                                    if (orderExpend == null)
                                    {
                                        ServiceDepository.TransactionService.RollBack();
                                        return new OperationResult(-13, "出款失败，出款帐号信息错误");
                                    }
                                    accountName = orderExpend.AccountName;
                                    bankAccountNo = orderExpend.BankAccountNo;
                                    bankCode = orderExpend.BankCode;
                                    bankName = orderExpend.SubBankName;
                                }
                                else
                                {
                                    ServiceDepository.TransactionService.RollBack();
                                    throw new NotImplementedException(string.Format("未实现订单类型为{0}的扩展信息", order.OrderType.GetDescription()));
                                }

                                //创建出款流水
                                var detail = new TradeInfo
                                {
                                    SysNo = Sequence.Get(),
                                    OrderNo = order.OrderNo,
                                    Amount = -orderBalance,
                                    Status = TradeStatus.Initial,
                                    AccountName = accountName,
                                    BankAccountNo = bankAccountNo,
                                    BankCode = bankCode,
                                    SubBankName = bankName,
                                    Remark = remark,
                                    CreateUserSysNo = operatorSysNo,
                                    CreateDate = DateTime.Now
                                };
                                if (!ServiceDepository.TradeDataAccess.Insert(detail))
                                {
                                    ServiceDepository.TransactionService.RollBack();
                                    return new OperationResult(-13, "出款明细创建失败");
                                }
                                //修改账户余额
                                payeeAccount.Verson++;
                                payeeAccount.Balance -= orderBalance;
                                payeeAccount.LockedMoney += orderBalance;
                                if (!ServiceDepository.AccountDataAccess.Update(payeeAccount))
                                {
                                    ServiceDepository.TransactionService.RollBack();
                                    return new OperationResult(-13, "出款失败");
                                }
                                //账户操作日志
                                if (!ServiceDepository.AccountDataAccess.AddOperatorLog(new AccountOperationLog
                                {
                                    AccountSysNo = payeeAccount.SysNo,
                                    Amount = orderBalance,
                                    OperationType = AccountOperationType.Lock,
                                    RefBizNo = order.OrderNo,
                                    CreateUserSysNo = operatorSysNo,
                                    CreateDate = DateTime.Now
                                }))
                                {
                                    ServiceDepository.TransactionService.RollBack();
                                    return new OperationResult(-13, "出款失败");
                                }
                            }
                            #endregion
                        }
                    }
                }
                #endregion

                #region #################交易流水#################
                //if (status == AuditStatus.ReAuditFailed || status == AuditStatus.AuditFailed || status == AuditStatus.FinanceAuditFailed)
                //{
                //    foreach (var trade in trades)
                //    {
                //        trade.Status = TradeStatus.Failed;
                //        trade.ModifyUserSysNo = operatorSysNo;
                //        if (!ServiceDepository.TradeDataAccess.Update(trade))
                //        {
                //            ServiceDepository.TransactionService.RollBack();
                //            return new OperationResult(-8, string.Format("交易流水:{0},状态修改失败", trade.TradeNo));
                //        }
                //    }
                //}
                #endregion

                ServiceDepository.TransactionService.Commit();
                //是否存在支付电商服务费,如存在则发消息
                ServiceDepository.MessageService.SendExpendSmsMessage(expendOrders);
                return new OperationResult(0);
            }
            catch (Exception e)
            {
                ServiceDepository.TransactionService.RollBack();
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }

        public OperationResult SubmitPaidOff(AccountType payerType, string payerSubjectId, AccountType payeeType, string payeeSubjectId, OrderBizType tradeType, decimal amount, string bankAccountNo, string bankCode, string accountName, string bankName, string remark, long operatorSysNo)
        {
            #region ###################参数验证###################
            if (string.IsNullOrWhiteSpace(payerSubjectId) || string.IsNullOrWhiteSpace(payeeSubjectId) || amount <= 0)
                return new OperationResult(-1, "非法参数信息");
            if (string.IsNullOrWhiteSpace(bankName) || string.IsNullOrWhiteSpace(accountName) || string.IsNullOrWhiteSpace(bankAccountNo))
                return new OperationResult(-2, "银行帐户信息错误");
            var payerAccount = ServiceDepository.AccountDataAccess.Get(payerSubjectId, payerType, tradeType);
            if (payerAccount == null)
                return new OperationResult(-3, "付款账户不存在");
            if (payerAccount.IsLocked)
                return new OperationResult(-3, "付款账户已锁定");
            if (payerAccount.Balance < amount)
                return new OperationResult(-3, "付款账户余额不足");
            var payeeAccount = ServiceDepository.AccountDataAccess.Get(payeeSubjectId, payeeType, tradeType);
            if (payeeAccount == null)
                return new OperationResult(-9, "收款账户不存在");
            #endregion

            try
            {
                ServiceDepository.TransactionService.Begin();
                #region ###################创建订单###################
                OrderInfo order = new OrderInfo
                {
                    SysNo = Sequence.Get(),
                    AccountSysNo = payerAccount.SysNo,
                    OppositeAccount = payeeAccount.SysNo,
                    OrderAmount = amount,
                    OrderType = OrderType.PaidOff,
                    OrderBizType = tradeType,
                    PaymentChannel = PaymentChannel.Transfer,
                    OrderStatus = OrderStatus.Initial,
                    Remark = remark,
                    CreateUserSysNo = operatorSysNo,
                    CreateDate = DateTime.Now
                };
                if (!ServiceDepository.OrderDataAccess.Insert(order))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-4, "订单创建失败");
                }
                #endregion

                #region ###################订单扩展信息###################
                var extend = new PaidOffOrderExtend
                {
                    OrderNo = order.OrderNo,
                    AccountName = accountName,
                    BankAccountNo = bankAccountNo,
                    BankCode = bankCode,
                    SubBankName = bankName,
                    CreateUserSysNo = operatorSysNo
                };
                if (!ServiceDepository.OrderDataAccess.InsertPaidOffExtend(extend))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-6, "订单创建失败");
                }
                #endregion
                //#region ###################创建交易明细###################
                //var detail = new TradeInfo
                //{
                //    SysNo = Sequence.Get(),
                //    OrderNo = order.OrderNo,
                //    Amount = -amount,
                //    Status = TradeStatus.Initial,
                //    AccountName = accountName,
                //    BankAccountNo = bankAccountNo,
                //    BankCode = bankCode,
                //    SubBankName = bankName,
                //    Remark = remark,
                //    CreateUserSysNo = operatorSysNo,
                //    CreateDate = DateTime.Now
                //};
                //if (!ServiceDepository.TradeDataAccess.Insert(detail))
                //{
                //    ServiceDepository.TransactionService.RollBack();
                //    return new OperationResult(-6, "交易明细创建失败");
                //}
                //#endregion

                #region ###################锁定出账金额###################
                payerAccount.Balance -= order.OrderAmount;
                payerAccount.LockedMoney += order.OrderAmount;
                payerAccount.ModifyUserSysNo = operatorSysNo;
                if (!ServiceDepository.AccountDataAccess.Update(payerAccount))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-7, "资金锁定操作失败");
                }
                //资金日志
                var log = new AccountOperationLog
                {
                    AccountSysNo = payerAccount.SysNo,
                    Amount = order.OrderAmount,
                    OperationType = AccountOperationType.Lock,
                    RefBizNo = order.OrderNo,
                    Remark = string.Format("出账锁定：{0}", order.OrderNo),
                    CreateUserSysNo = operatorSysNo,
                    CreateDate = DateTime.Now
                };
                if (!ServiceDepository.AccountDataAccess.AddOperatorLog(log))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-8, "操作日志创建失败");
                }
                #endregion
                var result = new OperationResult(0);
                result.OtherData.Add("OrderNo", order.OrderNo);

                ServiceDepository.TransactionService.Commit();
                return result;
            }
            catch (Exception e)
            {
                ServiceDepository.TransactionService.RollBack();
                Log.Error(e);
                return new OperationResult(-5, "系统错误");
            }
        }

        public OperationResult PaidOffCallback(string tradeNo, PaidOffStatus status, string serialNo, string rejectReason, long? operatorSysNo)
        {
            #region ###################参数校验###################
            if (string.IsNullOrEmpty(tradeNo))
                return new OperationResult(-1, "交易编号错误");
            var trade = ServiceDepository.TradeDataAccess.Get(tradeNo);
            if (trade == null)
                return new OperationResult(-2, "交易明细不存在");
            var order = ServiceDepository.OrderDataAccess.Get(trade.OrderNo);
            if (order == null)
                return new OperationResult(-3, "订单不存在");
            if (status == PaidOffStatus.Init || (trade.Status == TradeStatus.Completed && status == PaidOffStatus.Success) ||
                (trade.Status == TradeStatus.Failed && status == PaidOffStatus.Failed))
                return new OperationResult(0, "重复通知");
            if (trade.Status != TradeStatus.Submitted)
                return new OperationResult(-4, "交易状态错误");
            if (order.OrderStatus != OrderStatus.Submitted)
                return new OperationResult(-6, "订单状态错误");
            var account = ServiceDepository.AccountDataAccess.Get(order.OppositeAccount.Value, order.OrderBizType);
            if (account.LockedMoney < Math.Abs(trade.Amount))
                return new OperationResult(-7, "账户异常");
            #endregion
            try
            {
                ServiceDepository.TransactionService.Begin();

                #region ###################审核日志###################
                if (!ServiceDepository.AuditLogDataAccess.Insert(new AuditLogInfo
                {
                    AuditObjectType = AuditObjectType.Trade,
                    ChangeValue = (int)(status == PaidOffStatus.Success ? AuditStatus.FinanceAudited : AuditStatus.FinanceAuditFailed),
                    Department = "总部财务",
                    SubjectId = trade.TradeNo,
                    Remark = rejectReason
                }))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-12, "审核日志创建失败");
                }
                if (!ServiceDepository.AuditLogDataAccess.Insert(new AuditLogInfo
                {
                    AuditObjectType = AuditObjectType.Order,
                    ChangeValue = (int)(status == PaidOffStatus.Success ? AuditStatus.FinanceAudited : AuditStatus.FinanceAuditFailed),
                    Department = "总部财务",
                    SubjectId = trade.OrderNo,
                    Remark = rejectReason
                }))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-12, "审核日志创建失败");
                }
                #endregion

                #region ###################修改交易流水信息###################
                if (!string.IsNullOrWhiteSpace(serialNo))
                    trade.SerialNo = serialNo;
                trade.Status = status == PaidOffStatus.Success ? TradeStatus.Completed : TradeStatus.Failed;
                trade.PaymentDate = DateTime.Now; //实际销帐时间
                if (operatorSysNo != null)
                    trade.ModifyUserSysNo = operatorSysNo;
                if (!ServiceDepository.TradeDataAccess.Update(trade))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-8, "更新交易流水状态错误");
                }
                #endregion

                #region ###################修改订单信息###################
                if (operatorSysNo != null)
                    order.ModifyUserSysNo = operatorSysNo;

                order.OrderStatus = status == PaidOffStatus.Success ? OrderStatus.Completed : OrderStatus.Closed;
                order.AuditStatus = status == PaidOffStatus.Success ? AuditStatus.FinanceAudited : AuditStatus.FinanceAuditFailed;

                if (!ServiceDepository.OrderDataAccess.Update(order))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-11, "更新订单状态错误");
                }
                #endregion

                #region ###################修改资金账户信息###################
                account.LockedMoney -= Math.Abs(trade.Amount);
                if (status == PaidOffStatus.Failed)
                    account.Balance += Math.Abs(trade.Amount);
                if (!ServiceDepository.AccountDataAccess.Update(account))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-9, "更新账户信息错误");
                }
                //account log
                if (!ServiceDepository.AccountDataAccess.AddOperatorLog(new AccountOperationLog
                {
                    AccountSysNo = account.SysNo,
                    Amount = Math.Abs(trade.Amount),
                    OperationType = status == PaidOffStatus.Success ? AccountOperationType.ChargeOff : AccountOperationType.UnLock,
                    RefBizNo = trade.TradeNo,
                    Remark = string.Format("支付状态,{0}", status.ToString()),
                    CreateUserSysNo = operatorSysNo,
                    CreateDate = DateTime.Now
                }))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-10, "创建资金日志错误");
                }
                //资金账户明细
                if (status == PaidOffStatus.Success && !ServiceDepository.AccountDetailDataAccess.Insert(new AccountDetailInfo(AccountDetailType.Withdraw)
                {
                    AccountSysNo = account.SysNo,
                    Amount = -order.OrderAmount,
                    OrderNo = order.OrderNo,
                    CreateUserSysNo = operatorSysNo
                }))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-11, "创建资金明细错误");
                }
                #endregion

                ServiceDepository.TransactionService.Commit();
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, e.Message);
            }
            return new OperationResult(0);
        }
    }
}
