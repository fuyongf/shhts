﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Activation;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.ServiceImpl
{
    public partial class TradeService : ITradeService
    {
        const string RECEIPT_PATH = "/Biz/Trade/Receipt/Payee";
        const string RECEIPTNO_TEMPLATE = "{0}-{1}-{2}-{3}-{4}";
        const string RECEIPT_SERIALNO_TEMPLATE = "{0}-{1}";
        const string RECEIPT_SEQUENCENAME_TEMPLATE = "PinganHouse.Receipt.{0}";
        const string RECEIPT_SECURITYCODE_TEMPLATE = "{0},{1},{2}";

        public bool AuthProvideReceiptPermission(string identity)
        {
            if (string.IsNullOrWhiteSpace(identity))
                return false;
            var sw = ServiceDepository.SettingService.GetSettingItemValueByPath("/Biz/Trade/Receipt/Authenticate/Switch");
            if (string.IsNullOrWhiteSpace(sw))
                throw new Exception("参数未配置");
            if (!"1".Equals(sw))
                return true;
            var setting = ServiceDepository.SettingService.GetSettingItemByPath("/Biz/Trade/Receipt/Authenticate/Identities");
            if (setting == null || string.IsNullOrWhiteSpace(setting.Value))
                return false;
            return setting.Value.Split(',').Contains(identity);
        }

        public OperationResult GenerateReceipt(string orderNo, long createUserSysNo, out ReceiptView receiptDto, IDictionary<string, object> data, params string[] tradeNos)
        {
            receiptDto = new ReceiptView();
            try
            {
                receiptDto = PreviewReceipt(orderNo, tradeNos);
                if (receiptDto == null)
                    return new OperationResult(1, "params is invalid");
                string receiptNo = GenerateReceiptNo(orderNo);
                long serialSysNo = Sequence.Get();
                string securityCode = GenerateReceiptSecurityCode(serialSysNo, receiptNo, receiptNo);
                receiptDto.ReceiptNo = receiptNo;
                receiptDto.ReceiptSerialNo = receiptNo;
                receiptDto.SecurityCode = securityCode;
                receiptDto.CreateUserSysNo = createUserSysNo;
                receiptDto.CreateDate = DateTime.Now;
                receiptDto.OrderNo = orderNo;
                receiptDto.Address = data["Address"].ToString();
                receiptDto.MobilePhone = data["MobilePhone"].ToString();
                receiptDto.PostCode = data["PostCode"].ToString();
                var entity = ObjectMapper.Copy<ReceiptInfo>(receiptDto);

                try
                {
                    ServiceDepository.TransactionService.Begin();
                    ServiceDepository.ReceiptDataAccess.Insert(entity);
                    var update = ServiceDepository.TradeDataAccess.UpdateReceipt(orderNo, createUserSysNo, entity.TradeDetails.ToArray());
                    if (!update)
                        throw new ArgumentException("更新票据标识失败");
                    ServiceDepository.ReceiptDataAccess.InsertReceiptSerialInfo(serialSysNo, entity.ReceiptNo, entity.ReceiptNo, securityCode, null, createUserSysNo);
                    ServiceDepository.TransactionService.Commit();
                }
                catch
                {
                    ServiceDepository.TransactionService.RollBack();
                    throw;
                }
                receiptDto.ReceiptNo = entity.ReceiptNo;
                receiptDto.SysNo = entity.SysNo;
                return new OperationResult(0);
            }
            catch (ArgumentException ae)
            {
                return new OperationResult(-1, ae.Message);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-1, "系统内部错误");
            }

        }

        public ReceiptView PreviewReceipt(string orderNo, params string[] tradeNos)
        {
            if (string.IsNullOrWhiteSpace(orderNo) && (tradeNos == null || tradeNos.Length == 0))
                return null;
            try
            {
                var ts = ServiceDepository.TradeDataAccess.GetByOrder(orderNo, tradeNos);

                if (ts == null || ts.Count == 0)
                    throw new ArgumentException("未存在交易流水");
                decimal totalMoney = 0;
                var tl = new List<string>();
                foreach (var t in ts)
                {
                    if (t.Status == TradeStatus.Cancelled || t.Status == TradeStatus.Failed || t.Status == TradeStatus.Closed)
                        throw new ArgumentException(string.Format("交易流水 {0} 交易异常", t.TradeNo));
                    if (t.IsBilled)
                        throw new ArgumentException(string.Format("交易流水 {0} 系统已出过收据", t.TradeNo));

                    tl.Add(t.TradeNo);
                    //收据金额
                    totalMoney += t.Amount;
                }

                return SetReceiptDto(ts[0], totalMoney, tl);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        public ReceiptView GetReceiptInfo(long receiptSysNo)
        {
            try
            {
                var result = ServiceDepository.ReceiptDataAccess.GetReceiptSerialInfo(receiptSysNo);
                if (result == null)
                    return null;
                var ts = ServiceDepository.TradeDataAccess.GetByOrder(result.OrderNo, result.TradeDetail);
                if (ts == null || ts.Count == 0)
                    return null;
                var rd = ObjectMapper.Copy<ReceiptView>(result);
                rd.AccountType = ts[0].AccountType;
                rd.CaseId = ts[0].CaseId;
                rd.PaymentChannel = ts[0].PaymentChannel;
                rd.OrderBizType = ts[0].OrderBizType;
                rd.TenementAddress = ts[0].TenementAddress;
                return rd;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        public OperationResult RePrintReceipt(long receiptSysNo, long createUserSysNo, out ReceiptView receipt, string remark = null)
        {
            receipt = new ReceiptView();
            if (receiptSysNo <= 0)
                return new OperationResult(1, "参数错误");
            try
            {
                receipt = GetReceiptInfo(receiptSysNo);
                if (receipt == null)
                    return new OperationResult(2, "未存在该票据");
                long serialSysNo = Sequence.Get();
                string serialNo = GenerateReceiptSerialNo(receipt.ReceiptNo);
                string securityCode = GenerateReceiptSecurityCode(serialSysNo, receipt.ReceiptNo, serialNo);
                if (!ServiceDepository.ReceiptDataAccess.InsertReceiptSerialInfo(serialSysNo, receipt.ReceiptNo, serialNo, securityCode, remark, createUserSysNo))
                    return new OperationResult(3, "票据打印失败");
                receipt.SecurityCode = securityCode;
                receipt.ReceiptSerialNo = serialNo;
                return new OperationResult(0);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-1, "系统内部错误");
            }

        }

        public IList<Receipt> GetReceipts(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, ReceiptStatus? status, string receiptNo)
        {
            totalCount = 0;
            try
            {
                var entities = ServiceDepository.ReceiptDataAccess.GetPaginatedList(pageIndex, pageSize, out totalCount, center, status, receiptNo);
                if (entities == null)
                    return null;
                return entities.Select(r => ObjectMapper.Copy<Receipt>(r)).ToList();
                //var orders = ServiceDepository.OrderDataAccess.GetPaginatedList(pageIndex, pageSize, out totalCount, center, caseId, orderNo, startDate, endDate, data);
                //if (orders == null || orders.Count == 0)
                //    return null;
                //var rl = new List<ReceiptPaginated>();

                //foreach (var order in orders)
                //{
                //    var rp = new ReceiptPaginated
                //    {
                //        OrderNo = order.OrderNo,
                //        CaseId = order.CaseId,
                //        TotalMoney = order.OrderAmount,
                //        CollectedBalance = order.CollectedBalance,
                //    };
                //    var receipts = ServiceDepository.ReceiptDataAccess.GetReceiptSerialInfos(order.OrderNo);
                //    if (receipts != null && receipts.Count > 0)
                //    {
                //        var dic = new Dictionary<string, int>();
                //        decimal receiptMoney = 0;
                //        foreach (var r in receipts)
                //        {
                //            if (!dic.ContainsKey(r.ReceiptNo))
                //            {
                //                receiptMoney += r.Money;
                //                dic[r.ReceiptNo] = 0;
                //            }
                //        }
                //        rp.ReceiptCount = dic.Keys.Count;
                //        rp.RePrintCount = receipts.Count - rp.ReceiptCount;
                //        rp.LastPrintTime = receipts[0].CreateDate;
                //        rp.ReceiptMoney = receiptMoney;
                //    }
                //    rl.Add(rp);
                //}
                //return rl;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }

        public IList<ReceiptView> GetReceiptsByOrderNo(string orderNo)
        {
            if (string.IsNullOrWhiteSpace(orderNo))
                return null;
            try
            {
                var rs = ServiceDepository.ReceiptDataAccess.GetReceiptByOrderNo(orderNo);
                if (rs == null || rs.Count == 0)
                    return null;
                var ts = ServiceDepository.TradeDataAccess.GetByOrder(orderNo);
                if (ts == null || ts.Count == 0)
                    return null;
                var rl = new List<ReceiptView>();
                foreach (var r in rs)
                {
                    var trade = ts.FirstOrDefault(t => r.TradeDetails.Contains(t.TradeNo));
                    var rd = SetReceiptDto(trade, r.Money, r.TradeDetails);

                    rl.Add(rd);
                }
                return rl;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }

        public ReceiptView GetReceiptBySecurityCode(string securityCode)
        {
            var entity = ServiceDepository.ReceiptDataAccess.GetReceiptSerialBySecurityCode(securityCode);
            if (entity == null)
                return null;
            return ObjectMapper.Copy<ReceiptView>(entity);
        }

        public OperationResult RecycleReceipt(long receiptSysNo, long operatorSysNo)
        {
            try
            {
                var entity = ServiceDepository.ReceiptDataAccess.Get(receiptSysNo);
                if (entity == null)
                    return new OperationResult(-1, "收据不存在");
                if (entity.Status != ReceiptStatus.Resultful)
                    return new OperationResult(-2, "收据状态错误");
                entity.Status = ReceiptStatus.Recycled;
                entity.ModifyUserSysNo = operatorSysNo;
                if (!ServiceDepository.ReceiptDataAccess.Update(entity))
                    return new OperationResult(-3, "操作失败");
                return new OperationResult(0);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }

        public OperationResult AbolishReceipt(long receiptSysNo, long operatorSysNo)
        {
            try
            {
                var entity = ServiceDepository.ReceiptDataAccess.Get(receiptSysNo);
                if (entity == null)
                    return new OperationResult(-1, "收据不存在");
                if (entity.Status != ReceiptStatus.Resultful)
                    return new OperationResult(-2, "收据状态错误");
                entity.Status = ReceiptStatus.Cancellation;
                entity.ModifyUserSysNo = operatorSysNo;
                if (!ServiceDepository.ReceiptDataAccess.Update(entity))
                    return new OperationResult(-3, "操作失败");
                return new OperationResult(0);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }

        public IList<ReceiptTradeInfo> GetPrintReceiptTradesByOrder(string orderNo)
        {
            if (string.IsNullOrWhiteSpace(orderNo))
                return null;
            var entities = ServiceDepository.TradeDataAccess.GetPrintReceiptTradesByOrder(orderNo);
            if (entities == null)
                return null;
            return entities.Select(e => ObjectMapper.Copy<ReceiptTradeInfo>(e)).ToList();
        }

        private ReceiptView SetReceiptDto(TradeInfo trade, decimal totalMoney, IList<string> tl)
        {
            var setting = ServiceDepository.SettingService.GetSettingItemByPath(RECEIPT_PATH);
            if (setting == null)
                throw new ArgumentNullException("未配置收款单位");
            return new ReceiptView
            {

                Money = totalMoney,
                TradeDetails = tl,
                ReceiptDate = DateTime.Now,
                AccountType = trade.AccountType,
                CaseId = trade.CaseId,
                PaymentChannel = trade.PaymentChannel,
                OrderBizType = trade.OrderBizType,
                OrderNo = trade.OrderNo,
                TenementAddress = trade.TenementAddress,
                Payer = trade.AccountName,
                Payee = setting.Value,//收款单位 待确认收据编号

            };
        }

        private string GenerateReceiptNo(string orderNo)
        {
            if (string.IsNullOrEmpty(orderNo))
                throw new ArgumentNullException("orderNo");
            try
            {
                var order = ServiceDepository.OrderDataAccess.Get(orderNo);
                if (order == null)
                    throw new ArgumentException("order is null");
                ReceptionCenter center = ServiceDepository.OrderDataAccess.GetReceptionCenterByOrder(orderNo);
                return string.Format(RECEIPTNO_TEMPLATE,
                    order.OrderType == OrderType.Collection ? "DS" : "FW",
                    ((int)center).ToString().PadLeft(4, '0'),
                    DateTime.Now.Year,
                    DateTime.Now.Month.ToString().PadLeft(2, '0'),
                    Sequence.Get(string.Format(RECEIPT_SEQUENCENAME_TEMPLATE, center.ToString())).ToString().PadLeft(4, '0'));
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }

        private string GenerateReceiptSecurityCode(long sysNo, string receiptNo, string serialNo)
        {
            return EncryptHelper.GetMD5(string.Format(RECEIPT_SECURITYCODE_TEMPLATE, sysNo, receiptNo, serialNo));
        }

        private string GenerateReceiptSerialNo(string receiptNo)
        {
            int count = ServiceDepository.ReceiptDataAccess.GetReceiptPrintCount(receiptNo);
            if (count == 0)
                throw new Exception("收据打印信息不存在");
            return string.Format(RECEIPT_SERIALNO_TEMPLATE, receiptNo, count);
        }
    }

    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ReceiptServiceV2 : IReceiptServiceV2 
    {
        const string RECEIPT_PATH = "/Biz/Trade/Receipt/Payee";
        const string RECEIPTNO_TEMPLATE = "{0}-{1}-{2}-{3}-{4}";
        const string RECEIPT_SERIALNO_TEMPLATE = "{0}-{1}";
        const string RECEIPT_SEQUENCENAME_TEMPLATE = "PinganHouse.Receipt.{0}";
        const string RECEIPT_SECURITYCODE_TEMPLATE = "{0},{1},{2}";

        public bool AuthProvideReceiptPermission(string identity)
        {
            if (string.IsNullOrWhiteSpace(identity))
                return false;
            var sw = ServiceDepository.SettingService.GetSettingItemValueByPath("/Biz/Trade/Receipt/Authenticate/Switch");
            if (string.IsNullOrWhiteSpace(sw))
                throw new Exception("参数未配置");
            if (!"1".Equals(sw))
                return true;
            var setting = ServiceDepository.SettingService.GetSettingItemByPath("/Biz/Trade/Receipt/Authenticate/Identities");
            if (setting == null || string.IsNullOrWhiteSpace(setting.Value))
                return false;
            return setting.Value.Split(',').Contains(identity);
        }

        public OperationResult GenerateReceipt(AccountType accountType, string subjectId, OrderBizType bizType, decimal amount, long operatorSysNo, out ReceiptView receiptDto)
        {
            receiptDto = new ReceiptView();
            try
            {
                ReceptionCenter center = ReceptionCenter.Unkonw;
                AccountInfo account = null;
                receiptDto = PreviewReceipt(accountType, subjectId, bizType, amount, out account, out center);
                if (receiptDto == null)
                    return new OperationResult(1, "params is invalid");
                string receiptNo = GenerateReceiptNo(center);
                long serialSysNo = Sequence.Get();
                string securityCode = GenerateReceiptSecurityCode(serialSysNo, receiptNo, receiptNo);
                receiptDto.ReceiptNo = receiptNo;
                receiptDto.ReceiptSerialNo = receiptNo;
                receiptDto.SecurityCode = securityCode;
                receiptDto.CreateUserSysNo = operatorSysNo;
                receiptDto.CreateDate = DateTime.Now;
                var entity = ObjectMapper.Copy<ReceiptInfo>(receiptDto);

                try
                {
                    account.ReceiptBalance -= amount;
                    account.ModifyUserSysNo = operatorSysNo;

                    ServiceDepository.TransactionService.Begin();
                    ServiceDepository.ReceiptDataAccess.Insert(entity);
                    var update = ServiceDepository.AccountDataAccess.Update(account);
                    if (!update)
                        throw new ArgumentException("更新收据余额失败");
                    ServiceDepository.ReceiptDataAccess.InsertReceiptSerialInfo(serialSysNo, entity.ReceiptNo, entity.ReceiptNo, securityCode, null, operatorSysNo);
                    ServiceDepository.TransactionService.Commit();
                }
                catch
                {
                    ServiceDepository.TransactionService.RollBack();
                    throw;
                }
                receiptDto.ReceiptNo = entity.ReceiptNo;
                receiptDto.SysNo = entity.SysNo;
                return new OperationResult(0);
            }
            catch (ArgumentException ae)
            {
                return new OperationResult(-5, ae.Message);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-5, "系统内部错误");
            }

        }

        public ReceiptView PreviewReceipt(AccountType accountType, string subjectId, OrderBizType bizType, decimal amount)
        {
            ReceptionCenter center = ReceptionCenter.Unkonw;
            AccountInfo account = null;
            return PreviewReceipt(accountType, subjectId, bizType, amount, out account, out center);
        }

        public OperationResult RePrintReceipt(long receiptSysNo, long createUserSysNo, out ReceiptView receipt, string remark = null)
        {
            receipt = new ReceiptView();
            if (receiptSysNo <= 0)
                return new OperationResult(1, "参数错误");
            try
            {
                receipt = GetReceiptInfo(receiptSysNo);
                if (receipt == null)
                    return new OperationResult(2, "未存在该票据");
                if (receipt.Status != ReceiptStatus.Resultful)
                    return new OperationResult(2, "收据已失效");
                long serialSysNo = Sequence.Get();
                string serialNo = GenerateReceiptSerialNo(receipt.ReceiptNo);
                string securityCode = GenerateReceiptSecurityCode(serialSysNo, receipt.ReceiptNo, serialNo);
                if (!ServiceDepository.ReceiptDataAccess.InsertReceiptSerialInfo(serialSysNo, receipt.ReceiptNo, serialNo, securityCode, remark, createUserSysNo))
                    return new OperationResult(3, "生成收据失败");
                receipt.SecurityCode = securityCode;
                receipt.ReceiptSerialNo = serialNo;
                return new OperationResult(0);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-1, "系统内部错误");
            }
        }

        public ReceiptView GetReceiptInfo(long receiptSysNo)
        {
            try
            {
                var result = ServiceDepository.ReceiptDataAccess.GetReceiptSerialInfo(receiptSysNo);
                if (result == null)
                    return null;
                //var ts = ServiceDepository.TradeDataAccess.GetByOrder(result.OrderNo, result.TradeDetail);
                //if (ts == null || ts.Count == 0)
                //    return null;
                var rd = ObjectMapper.Copy<ReceiptView>(result);

                var account = ServiceDepository.AccountDataAccess.Get(result.AccountSysNo, result.OrderBizType);
                rd.AccountType = account.AccountType;

                var caseInfo = ServiceDepository.CaseDataAccess.GetCaseByCaseId(account.SubjectId);
                rd.CaseId = caseInfo.CaseId;
                rd.TenementAddress = caseInfo.TenementAddress;

                return rd;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        public IList<Receipt> GetReceipts(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, ReceiptStatus? status, string receiptNo)
        {
            totalCount = 0;
            try
            {
                var entities = ServiceDepository.ReceiptDataAccess.GetPaginatedList(pageIndex, pageSize, out totalCount, center, status, receiptNo);
                if (entities == null)
                    return null;
                return entities.Select(r => ObjectMapper.Copy<Receipt>(r)).ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }

        public ReceiptView GetReceiptBySecurityCode(string securityCode)
        {
            var entity = ServiceDepository.ReceiptDataAccess.GetReceiptSerialBySecurityCode(securityCode);
            if (entity == null)
                return null;
            return ObjectMapper.Copy<ReceiptView>(entity);
        }

        public OperationResult RecycleReceipt(long receiptSysNo, long operatorSysNo)
        {
            try
            {
                var entity = ServiceDepository.ReceiptDataAccess.Get(receiptSysNo);
                if (entity == null)
                    return new OperationResult(-1, "收据不存在");
                if (entity.Status != ReceiptStatus.Resultful)
                    return new OperationResult(-2, "收据状态错误");
                entity.Status = ReceiptStatus.Recycled;
                entity.ModifyUserSysNo = operatorSysNo;

                var account = ServiceDepository.AccountDataAccess.Get(entity.AccountSysNo, entity.OrderBizType);
                account.ReceiptBalance += entity.Money;
                account.ModifyUserSysNo = operatorSysNo;
                
                ServiceDepository.TransactionService.Begin();
                if (!ServiceDepository.ReceiptDataAccess.Update(entity))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-3, "操作失败");
                }
                if (!ServiceDepository.AccountDataAccess.Update(account)) 
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-3, "操作失败");
                }
                ServiceDepository.TransactionService.Commit();
                return new OperationResult(0);
            }
            catch (Exception e)
            {
                ServiceDepository.TransactionService.RollBack();
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }

        public OperationResult AbolishReceipt(long receiptSysNo, long operatorSysNo)
        {
            try
            {
                var entity = ServiceDepository.ReceiptDataAccess.Get(receiptSysNo);
                if (entity == null)
                    return new OperationResult(-1, "收据不存在");
                if (entity.Status != ReceiptStatus.Resultful)
                    return new OperationResult(-2, "收据状态错误");
                entity.Status = ReceiptStatus.Cancellation;
                entity.ModifyUserSysNo = operatorSysNo;

                var account = ServiceDepository.AccountDataAccess.Get(entity.AccountSysNo, entity.OrderBizType);
                account.ReceiptBalance += entity.Money;
                account.ModifyUserSysNo = operatorSysNo;

                ServiceDepository.TransactionService.Begin();
                if (!ServiceDepository.ReceiptDataAccess.Update(entity))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-3, "操作失败");
                }
                if (!ServiceDepository.AccountDataAccess.Update(account))
                {
                    ServiceDepository.TransactionService.RollBack();
                    return new OperationResult(-3, "操作失败");
                }
                ServiceDepository.TransactionService.Commit();
                return new OperationResult(0);
            }
            catch (Exception e)
            {
                ServiceDepository.TransactionService.RollBack();
                Log.Error(e);
                return new OperationResult(-5, "系统异常");
            }
        }


        private ReceiptView PreviewReceipt(AccountType accountType, string subjectId, OrderBizType bizType, decimal amount, out AccountInfo account, out ReceptionCenter center)
        {
            center = ReceptionCenter.Unkonw;
            account = null;
            if (string.IsNullOrWhiteSpace(subjectId))
                return null;
            try
            {
                account = ServiceDepository.AccountDataAccess.Get(subjectId, accountType, bizType);
                if (account == null)
                    return null;
                if (account.ReceiptBalance < amount)
                    throw new ArgumentException("账户可开收据金额不足");

                var setting = ServiceDepository.SettingService.GetSettingItemByPath(RECEIPT_PATH);
                if (setting == null)
                    throw new ArgumentNullException("未配置收款单位");

                var caseInfo = ServiceDepository.CaseDataAccess.GetCaseByCaseId(account.SubjectId);
                if (caseInfo == null)
                    throw new ArgumentException("案件信息不存在");
                center = caseInfo.ReceptionCenter;

                var users = ServiceDepository.CaseDataAccess.GetRelations(caseInfo.CaseId);
                if (users == null)
                    throw new ArgumentException("系统异常，未获取到客户信息");

                int userType = (int)account.AccountType;
                users = users.Where(r => (int)r.CaseUserType == userType).ToList();
                if (users == null || users.Count == 0)
                    throw new ArgumentException("系统异常，未获取到客户信息");

                string payers = string.Empty;
                foreach (var u in users)
                {
                    payers += ServiceDepository.CustomerDataAccess.GetRealName(u.UserSysNo) + ",";
                }

                return new ReceiptView
                {
                    Money = amount,
                    ReceiptDate = DateTime.Now,
                    AccountType = account.AccountType,
                    CaseId = account.SubjectId,
                    OrderBizType = bizType,
                    TenementAddress = caseInfo.TenementAddress,
                    Payer = payers.TrimEnd(','),
                    Payee = setting.Value
                };
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        private string GenerateReceiptNo(ReceptionCenter center)
        {
            try
            {
                return string.Format(RECEIPTNO_TEMPLATE,
                    "DS",
                    ((int)center).ToString().PadLeft(4, '0'),
                    DateTime.Now.Year,
                    DateTime.Now.Month.ToString().PadLeft(2, '0'),
                    Sequence.Get(string.Format(RECEIPT_SEQUENCENAME_TEMPLATE, center.ToString())).ToString().PadLeft(4, '0'));
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }

        private string GenerateReceiptSecurityCode(long sysNo, string receiptNo, string serialNo)
        {
            return EncryptHelper.GetMD5(string.Format(RECEIPT_SECURITYCODE_TEMPLATE, sysNo, receiptNo, serialNo));
        }

        private string GenerateReceiptSerialNo(string receiptNo)
        {
            int count = ServiceDepository.ReceiptDataAccess.GetReceiptPrintCount(receiptNo);
            if (count == 0)
                throw new Exception("收据打印信息不存在");
            return string.Format(RECEIPT_SERIALNO_TEMPLATE, receiptNo, count);
        }
    }
}
