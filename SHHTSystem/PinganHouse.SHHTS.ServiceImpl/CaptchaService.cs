﻿using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;
using System;
using System.ServiceModel.Activation;
using RandomHelper = HTB.DevFx.Utils.RandomHelper;

namespace PinganHouse.SHHTS.ServiceImpl
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class CaptchaService : ICaptchaService
    {
        const int Length = 4;
        static string keyFormat = "{0}_{1}";
        public Captcha RequetCaptcha(string token, BusinessCacheType businessType)
        {
            return RequetCaptcha(token, businessType, RandomHelper.READ_CHARLIST, Length);
        }

        public Captcha RequetCaptcha(string token, BusinessCacheType businessType, string factor, int length)
        {
            string key = SetKey(token, businessType);
            var captcha = NewCaptcha(key, factor, length);
            var expireTime = int.Parse(businessType.GetDescription());
            ServiceDepository.CacheStorages.Upset<string>(captcha.Key, captcha.Value, TimeSpan.FromMinutes(expireTime));
            return captcha;
        }


        public OperationResult ValidateCaptcha(string token, BusinessCacheType businessType, string value)
        {
            if (string.IsNullOrWhiteSpace(token))
                return new OperationResult(1, "token is null");
            string key = SetKey(token, businessType);
            return ValidateCaptcha(key, value);
        }


        public OperationResult ValidateCaptcha(string key, string value)
        {
            if (string.IsNullOrWhiteSpace(key))
                return new OperationResult(1, "key is null");
            if (string.IsNullOrWhiteSpace(value))
                return new OperationResult(2, "value is null");
            var captcha = ServiceDepository.CacheStorages.Get<string>(key);
            if (captcha == null)
                return new OperationResult(3, "验证码失效");
            if (string.Compare(captcha, value, true) != 0)
                return new OperationResult(4, "验证码错误,请重新输入");
            return new OperationResult(0);
        }

        private string SetKey(string token, BusinessCacheType businessType)
        {
            return string.Format(keyFormat, token, (int)businessType);
        }
        private static Captcha NewCaptcha(string token, string factor, int length)
        {
            var codeValue = RandomHelper.GetRandomString(length, factor);
            return new Captcha(token, codeValue);
        }

    }
}
