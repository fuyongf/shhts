﻿using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.DataTransferObjects.External;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.ServiceImpl
{
    public partial class ParterChannalService : IParterService
    {
        public OperationResult AcceptQYMessage(QYWXMessage message)
        {
            string handleKey = string.Format("{0}_{1}", message.AgentID, message.MsgType.ToString());
            if (message.MsgType == WXMessageType.Event && message.Event != null)
            {
                handleKey = string.Format("{0}_{1}", message.AgentID, message.Event.ToString());
            }
            return this.HandleMessage(message, handleKey);
        }

        public OperationResult ValidateAuth2CallResult(string code, int agentid)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(code))
                    return new OperationResult(1, "用户未授权"); ;
                var data = new Dictionary<string, object> { { "code", code }, {"agentid",agentid}};
                var result = ServiceDepository.PartnerService.ValidateCallResult("wx_QYAuthorize", null, data);
                if (!result.Success)
                    return result;
                return new OperationResult(0, result.OtherData["UserId"].ToString());
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-1, "系统内部错误");
            }
        }

        #region 部门
        public OperationResult CreateDepartment(string name, int parentId, int order, int? id)
        {
            if (string.IsNullOrWhiteSpace(name))
                return new OperationResult(1, "部门名称为空");
            if (name.Length > 64)
                name = name.Substring(0, 64);
            if (parentId <= 0)
                return new OperationResult(2, "上级部门编号异常");
            var data = new Dictionary<string, object> 
            { 
                {"UrlKey", "create"},
                {"name", name },
                {"parentid", parentId },
                {"order", order },
            };
            if (id!=null)
                data["id"] = id.Value;
            return ServiceDepository.PartnerService.SendData("wx_Organization_Department", data);
        }

        public OperationResult UpdateDepartment(string id, string name, string parentId, int? order)
        {
            if (string.IsNullOrWhiteSpace(id))
                return new OperationResult(1, "部门id为空");
            var data = new Dictionary<string, object>();
            if (!string.IsNullOrWhiteSpace(name))
                data["name"] = name;
            if (!string.IsNullOrWhiteSpace(parentId))
                data["parentid"] = parentId;
            if (order != null)
                data["order"] = order.Value;
            if (data.Count == 0)
                return new OperationResult(2, "参数异常");
            data["UrlKey"] = "update";
            data["id"] = id;
            return ServiceDepository.PartnerService.SendData("wx_Organization_Department", data); 
        }

        public IList<WXQYDepartment> GetDepartments(string id = null)
        {
            var dic = new Dictionary<string, object> { {"id", id}};
            var result = ServiceDepository.PartnerService.SendData<WXQYDepartment>("wx_Organization_Department", dic); 
            if(result.Success)
                return result.OtherData[typeof(WXQYDepartment).Name] as IList<WXQYDepartment>;
            return null;
        }

        public OperationResult DeleteDepartment(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return new OperationResult(1,"Id 为空");
            var data = new Dictionary<string, object> { { "id", id }, { "UrlKey","delete" } };
            return ServiceDepository.PartnerService.SendData("wx_Organization_Department", data); 
        }
        #endregion

        #region 用户
        public OperationResult CreateUser(string userId, string name, string mobile, string email, string weixinid, string position, IEnumerable<string> departmentIds, string identityNo=null)
        {
            if (string.IsNullOrWhiteSpace(userId))
                return new OperationResult(1, "userId 为空");
            if (string.IsNullOrWhiteSpace(name))
                return new OperationResult(2, "成员名称为空");
            if (string.IsNullOrWhiteSpace(mobile) && string.IsNullOrWhiteSpace(email) && string.IsNullOrWhiteSpace(weixinid))
                return new OperationResult(3, "mobile/weixinid/email三者同时为空");
            
            var data = new Dictionary<string, object> { 
                {"UrlKey", "create"},
                {"userid", userId},
                {"name", name},
                {"department", departmentIds},
                {"position", position},
                {"mobile", mobile},
                {"weixinid", weixinid},
                {"email", email},
            };
            if (!string.IsNullOrWhiteSpace(identityNo))
            {
                var attrs = new Dictionary<string, IDictionary<string, object>> { { "attrs",new Dictionary<string, object>{{"name", "证件号"},{"value", identityNo}} } };
                data["extattr"] = attrs;
            }
            
            var result =  ServiceDepository.PartnerService.SendData("wx_Organization_User", data);
            if (result.Success)
                InviteUserSubscribe(userId);
            return result;
        }

        public OperationResult UpdateUser(string userId, string name = null, string mobile = null, string email = null, string weixinid = null, string position = null, bool? enable = null, params string[] departmentId)
        {
            if (string.IsNullOrWhiteSpace(userId))
                return new OperationResult(1, "userId 为空");
            var data = new Dictionary<string, object>();
            if (!string.IsNullOrWhiteSpace(name))
                data["name"] = name;
            if (!string.IsNullOrWhiteSpace(mobile))
                data["mobile"] = mobile;
            if (!string.IsNullOrWhiteSpace(email))
                data["email"] = email;
            if (!string.IsNullOrWhiteSpace(weixinid))
                data["weixinid"] = weixinid;
            if (!string.IsNullOrWhiteSpace(position))
                data["position"] = position;
            if (enable!=null)
                data["enable"] = enable.Value?1:0;
            if (departmentId != null && departmentId.Length != 0)
                data["department"] = departmentId;
            if (data.Count == 0)
                return new OperationResult(2, "参数异常");
            data["UrlKey"] = "update";
            data["userid"] = userId;
            return ServiceDepository.PartnerService.SendData("wx_Organization_User", data);
        }

        public WXQYUser GetUser(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId))
                throw new ArgumentException("userId is null");
            var data = new Dictionary<string, object> { { "userid", userId }, {"UrlKey", "get"} };
            var result = ServiceDepository.PartnerService.SendData<WXQYUser>("wx_Organization_User", data);
            if (!result.Success)
                return null;
            return result.OtherData[typeof(WXQYUser).Name] as WXQYUser;
        }

        public OperationResult DeleteUser(string[] userIds)
        {
            if (userIds == null || userIds.Length==0)
                return new OperationResult(1, "userIds in null");
            var data = new Dictionary<string, object> { { "useridlist", userIds }, { "UrlKey", "batchdelete" } };
            return ServiceDepository.PartnerService.SendData("wx_Organization_User", data);
        }

        public OperationResult DeleteUser(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId))
                return new OperationResult(1, "userId in null");
            var data = new Dictionary<string, object> { { "userid", userId }, { "UrlKey", "delete" } };
            return ServiceDepository.PartnerService.SendData("wx_Organization_User", data);
        }

        public IList<WXQYUser> GetUserByDepartment(string departmentId, bool fetchChild, WXQYUserStatus status)
        {
            if (string.IsNullOrWhiteSpace(departmentId))
                throw new ArgumentException("departmentId is invalid");
            var data = new Dictionary<string, object>
            {
                {"UrlKey", "list"},
                {"department_id", departmentId},               
            };
            data["fetch_child"] = fetchChild?1:0;
            data["status"] = (int)status;             
            var result = ServiceDepository.PartnerService.SendData<WXQYUser>("wx_Organization_User", data);
            if (!result.Success)              
                return null;
            return result.OtherData[typeof(WXQYUser).Name] as IList<WXQYUser>;
        }


        public OperationResult InviteUserSubscribe(string userId, string invateContent=null)
        {
            if (string.IsNullOrWhiteSpace(userId))
                return new OperationResult(1, "userId in null");
            if (string.IsNullOrWhiteSpace(invateContent))
                invateContent = ServiceDepository.SettingService.GetSettingItemValueByPath("/System/Parter/WeiXin/QY/InvateContent");
            var data = new Dictionary<string, object> { { "userid", userId }, { "UrlKey", "invite" }, { "invite_tips", invateContent } };
            return ServiceDepository.PartnerService.SendData("wx_Organization_User", data);
        }
        #endregion

        public OperationResult SendTextMessage(AgentType agentType, string content, ReceiverType receiverType, params string[] toAddresses)
        {
            if (toAddresses == null || toAddresses.Length == 0)
                return new OperationResult(2, "发送人列表为空");
            return SendTextMessage(agentType, content, string.Join("|", toAddresses), receiverType);
        }

        public OperationResult SendTextMessage(AgentType agentType, string content)
        {
            return SendTextMessage(agentType, content, "@all", ReceiverType.ToUser);
        }

        public OperationResult SendImageMessage(AgentType agentType, string mediaId)
        {
            if (string.IsNullOrWhiteSpace(mediaId))
                return new OperationResult(1, "mediaId is null");
            var dic = new Dictionary<string, object> 
            { 
                {"agentid", (int)agentType},
                {"msgtype", "image"},
                {"touser", "@all"},
            };

            dic["image"] = new Dictionary<string, string> 
            {
                {"media_id",mediaId},
            };
            return ServiceDepository.PartnerService.SendData("wx_QY_Message", dic);
        }

        private OperationResult SendTextMessage(AgentType agentType, string content, string toAddress, ReceiverType receiverType)
        {
            if (string.IsNullOrWhiteSpace(content))
                return new OperationResult(1, "消息内容为空");
            var dic = new Dictionary<string, object> 
            { 
                {"agentid", (int)agentType},
                {"msgtype", "text"},
                {receiverType.GetDescription(), toAddress},
            };

            dic["text"] = new Dictionary<string, string> 
            {
                {"content",content},
            };
            return ServiceDepository.PartnerService.SendData("wx_QY_Message", dic);
        }


        public OperationResult UploadMaterial(string name, string fileName, FileStream fs)
        {
            if (fs.Length > 1048576)
                return new OperationResult(1, "微信图片素材不能超过1M");
            fs.Seek(0, SeekOrigin.Begin);
            byte[] fileBytes = new byte[fs.Length];
            fs.Read(fileBytes, 0, fileBytes.Length);
            fs.Close(); 
            fs.Dispose();
            var data = new Dictionary<string, object> 
            {
                {"type", "image"},
                {"name", name},
                {"filename",fileName},
                {"file", fileBytes},
            };
            return ServiceDepository.PartnerService.SendData("wx_QYMaterial", data);
        }
    }
}
