﻿using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace PinganHouse.SHHTS.ServiceImpl.Flows
{
    /// <summary>
    /// 案件状态流操作接口
    /// </summary>
    public interface ICaseStatusFlow
    {
        /// <summary>
        /// 更新案件状态.
        /// </summary>
        /// <param name="caseObj">案件对象.</param>
        /// <param name="action">动作名称.</param>
        /// <param name="modifyUserSysNo">修改人编号.</param>
        /// <param name="otherData">其他数据.</param>
        /// <returns>新状态事件对象，如果更新失败，则返回Null</returns>
        CaseEventInfo UpdateStatus(Case caseObj, string action,
            long modifyUserSysNo, Dictionary<string, object> otherData = null);


        /// <summary>
        /// 获取下一步动作编号列表
        /// </summary>
        /// <param name="currentStatus">当前状态.</param>
        /// <returns></returns>
        IList<string> GetNextActions(CaseStatus currentStatus);
    }

    /// <summary>
    /// 案件状态流
    /// </summary>
    public class CaseStatusFlow
    {
        /// <summary>
        /// 状态更新完成后触发的动作.
        /// </summary>
        public Action<CaseEventInfo, Case> Updated { get; set; }


        /// <summary>
        /// Builds the instance.
        /// </summary>
        /// <param name="caseObj">The case object.</param>
        /// <returns></returns>
        ICaseStatusFlow BuildInstance(Case caseObj)
        {
            switch (caseObj.ReceptionCenter)
            {
                case ReceptionCenter.BaoShan:
                case ReceptionCenter.PuDong: return new ShanghaiStrategy();
                default: return null;
            }
        }

        /// <summary>
        /// 更新案件状态.
        /// </summary>
        /// <param name="caseId">案件编号.</param>
        /// <param name="action">动作名称.</param>
        /// <param name="modifyUserSysNo">修改人编号.</param>
        /// <param name="otherData">其他数据.</param>
        public void UpdateStatus(string caseId, string action,
            long modifyUserSysNo, Dictionary<string, object> otherData = null)
        {
            if (string.IsNullOrWhiteSpace(caseId) || string.IsNullOrWhiteSpace(action))
                throw new ArgumentException("无效参数");

            var caseObj = ServiceDepository.CaseDataAccess.GetCaseByCaseId(caseId);

            if (caseObj == null || caseObj.Disabled)
                throw new Exception("案件不存在或已取消");

            var instance = BuildInstance(caseObj);

            if (instance == null)
                throw new NotSupportedException(string.Format("无法找到匹配的地区策略: {0}", caseObj.ReceptionCenter));

            var ne = instance.UpdateStatus(caseObj, action, modifyUserSysNo, otherData);

            if (Updated != null && ne != null)
                Updated(ne, caseObj);
        }

        /// <summary>
        /// 获取下一步动作编号列表.
        /// </summary>
        /// <param name="caseId">案件编号.</param>
        /// <param name="currentStatus">案件当前状态.</param>
        /// <returns></returns>
        public IList<string> GetNextActions(string caseId, CaseStatus currentStatus)
        {
            if (string.IsNullOrWhiteSpace(caseId))
                throw new ArgumentException("无效参数");

            var caseObj = ServiceDepository.CaseDataAccess.GetCaseByCaseId(caseId);

            if (caseObj == null || caseObj.Disabled)
                return new List<string>();

            var instance = BuildInstance(caseObj);

            if (instance == null)
                throw new NotSupportedException(string.Format("无法找到匹配的地区策略: {0}", caseObj.ReceptionCenter));

            return instance.GetNextActions(currentStatus);
        }


        /// <summary>
        /// ActionCfg
        /// </summary>
        class ActionCfg
        {
            /// <summary>
            /// 获取或设置动作名称
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// 获取或设置源状态值,-1表示任意源状态.
            /// </summary>
            public long From { get; set; }
            /// <summary>
            /// 获取或设置目标状态值.
            /// </summary>
            public long? To { get; set; }

            /// <summary>
            /// 获取或设置是否为结束动作.
            /// </summary>
            public bool IsFinish { get; set; }

            /// <summary>
            /// 获取或设置自定义处理函数名称.
            /// </summary>
            public string Handler { get; set; }

            /// <summary>
            /// Loads the specified document.
            /// </summary>
            /// <param name="doc">The document.</param>
            /// <returns></returns>
            public static IList<ActionCfg> Load(XDocument doc)
            {
                IList<ActionCfg> cfg = new List<ActionCfg>();

                if (doc == null)
                    return cfg;

                var rcfgs = new Dictionary<string, ActionCfg>();

                foreach (var act in doc.Descendants("action"))
                {

                    string name = act.Attribute("name") != null ? act.Attribute("name").Value : null;

                    if (name == null)
                        continue;
                    string handler = act.Attribute("handler") != null ? act.Attribute("handler").Value : null;
                    var fromAttr = act.Attribute("from");
                    long from = -1;
                    if (fromAttr == null)
                        continue;
                    //* 表示任意源状态
                    if (fromAttr.Value == "*")
                        from = -1;
                    else if (!long.TryParse(fromAttr.Value, out from))
                        continue;
                    var toAttr = act.Attribute("to");
                    long to = -1;
                    if (toAttr != null)
                    {
                        if (!long.TryParse(toAttr.Value, out to))
                            to = -1;
                    }
                    //handler为空时，或者from=-1时，to不能为-1(未设置)
                    if ((string.IsNullOrWhiteSpace(handler) || from == -1) && to == -1)
                        continue;

                    var finishAttr = act.Attribute("finish");
                    bool finish = false;
                    if (finishAttr != null)
                        bool.TryParse(finishAttr.Value, out finish);

                    var acit = new ActionCfg
                      {
                          From=from,
                          Name = name.Trim(),
                          Handler = handler,
                          IsFinish = finish
                      };

                    if (to > -1)
                        acit.To = to;

                    rcfgs[name.Trim().ToLower()] = acit;
                }

                cfg = rcfgs.Values.ToList();

                return cfg;
            }
        }

        /// <summary>
        /// 上海的策略
        /// </summary>
        class ShanghaiStrategy : ICaseStatusFlow
        {
            const string XmlConfigName = "PinganHouse.SHHTS.ServiceImpl.Flows.Config.CaseStatusFlow.Shanghai.xml";

            public ShanghaiStrategy()
            {
                ActionCfgs = ActionCfg.Load(System.Xml.Linq.XDocument.Load(typeof(ShanghaiStrategy).Assembly.GetManifestResourceStream(XmlConfigName)));
            }

            /// <summary>
            /// 获取或设置动作配置.
            /// </summary>
            IList<ActionCfg> ActionCfgs { get; set; }

            /// <summary>
            /// 将来源动作设为完成.
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="actionCfg">The action CFG.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <returns></returns>
            void FinishAncestorAction(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo)
            {
                //查找未完成的源事件
                var fromEvents = ServiceDepository.CaseEventDataAccess.Get(caseObj.CaseId, null, (CaseStatus)actionCfg.From, false);

                //没有源状态记录
                if (fromEvents.Count()==0)
                    throw new ArgumentException("源状态不存在或已完成，请刷新后重试");

                foreach (var fromEvent in fromEvents)
                {
                    if (!fromEvent.Finished)
                    {
                        //源状态设为完成
                        fromEvent.Finished = true;
                        fromEvent.ModifyUserSysNo = modifyUserSysNo;
                        fromEvent.ModifyDate = DateTime.Now;
                        ServiceDepository.CaseEventDataAccess.Update(fromEvent);
                    }
                }

            }

            /// <summary>
            /// 获取下一步动作编号列表
            /// </summary>
            /// <param name="currentStatus">当前状态.</param>
            /// <returns></returns>
            public IList<string> GetNextActions(CaseStatus currentStatus)
            {
                //不包含案件已中止，交房，结案三个动作
                return ActionCfgs.Where(o => o.From == (long)currentStatus).Select(o => o.Name).ToList();
            }

            /// <summary>
            /// 更新案件状态.
            /// </summary>
            /// <param name="caseId">案件编号.</param>
            /// <param name="action">动作编号.</param>
            /// <param name="modifyUserSysNo">修改人编号.</param>
            /// <param name="otherData">其他数据.</param>
            /// <returns>新状态事件对象，如果更新失败，则返回Null</returns>
            public CaseEventInfo UpdateStatus(Case caseObj, string action, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {

                //从配置中匹配，是否有此动作
                var actionCfg = ActionCfgs.SingleOrDefault(o => o.Name == action);

                //无匹配的配置
                if (actionCfg == null)
                    throw new ArgumentException(string.Format("动作 {0} 不存在", actionCfg.Name));

                if (string.IsNullOrWhiteSpace(actionCfg.Handler) && actionCfg.To == -1)
                        throw new ArgumentException(string.Format("动作 {0} 的目标状态未配置", actionCfg.Name));


                try
                {
                    ServiceDepository.TransactionService.Begin();

                    CaseEventInfo newEvent = null;


                    if (string.IsNullOrWhiteSpace(actionCfg.Handler))
                    {
                        // 默认处理

                        FinishAncestorAction(caseObj, actionCfg, modifyUserSysNo);

                        //写入新状态
                        newEvent = new CaseEventInfo
                        {
                            SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                            CaseId = caseObj.CaseId,
                            EventType = (CaseStatus)actionCfg.To,
                            SourceStatus = (CaseStatus)actionCfg.From,
                            OtherDatas = otherData,
                            CreateDate = DateTime.Now,
                            CreateUserSysNo = modifyUserSysNo,
                            OperatorSysNo = modifyUserSysNo,
                            Finished = actionCfg.IsFinish
                        };

                        ServiceDepository.CaseEventDataAccess.Insert(newEvent);
                    }
                    else
                    {
                        //自定义函数
                        System.Reflection.MethodInfo mf = typeof(ShanghaiStrategy).GetMethod(actionCfg.Handler, System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);

                        if (mf != null)
                            newEvent = mf.Invoke(this, new object[] { caseObj, actionCfg, modifyUserSysNo, otherData }) as CaseEventInfo;
                    }

                    ServiceDepository.TransactionService.Commit();

                    return newEvent;
                }
                catch
                {
                    ServiceDepository.TransactionService.RollBack();
                    throw;
                }

            }

            /// <summary>
            /// 交房完成
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="actionCfg">The action CFG.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            /// <returns></returns>
            CaseEventInfo Any_ZB8(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {
                //查找全部的源事件
                var fromEvents = ServiceDepository.CaseEventDataAccess.Get(caseObj.CaseId).OrderByDescending(x => x.CreateDate);

                //检查过户完成，且 银行已放款 或 无贷款 条件是否成立
                if (fromEvents.Count(o => o.EventType == CaseStatus.JY12 && o.Finished) == 0)
                    throw new Exception("过户未完成，不能交房");

                //满足DK8必然满足JY12
                CaseStatus lastStatus = CaseStatus.DK8;

                if (fromEvents.Count(o => o.EventType == CaseStatus.DK8 && o.Finished) == 0)
                {
                    //不能出现ZB9的下一步DK1
                    if (fromEvents.Count(o => o.EventType == CaseStatus.ZB9) > 0
                        && fromEvents.Count(o => o.EventType == CaseStatus.DK1) == 0)
                        lastStatus = CaseStatus.ZB9;
                    else
                        throw new Exception("银行放款或无贷款时，才能交房");
                }


                //写入新状态
                CaseEventInfo newEvent = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.ZB8,
                    SourceStatus = lastStatus,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo,
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent);


                return newEvent;
            }



            /// <summary>
            /// 任意状态-案件已中止.
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="actionCfg">The action CFG.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            /// <returns></returns>
            CaseEventInfo Any_ZB7(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {

                //查找全部的源事件
                var fromEvents = ServiceDepository.CaseEventDataAccess.Get(caseObj.CaseId).OrderByDescending(x => x.CreateDate);

                //最后一个状态
                var lastStatus = fromEvents.FirstOrDefault() == null ? CaseStatus.Unkown : fromEvents.FirstOrDefault().EventType;

                if (lastStatus == CaseStatus.YJ1 || lastStatus == CaseStatus.HA1 || lastStatus == CaseStatus.ZB8)
                    throw new ArgumentException("等待签约、签约中及交房完成的案件无法中止");

                foreach (var fe in fromEvents)
                {
                    if (!fe.Finished)
                    {
                        //源状态设为完成
                        fe.Finished = true;
                        fe.ModifyUserSysNo = modifyUserSysNo;
                        fe.ModifyDate = DateTime.Now;
                        ServiceDepository.CaseEventDataAccess.Update(fe);
                    }
                }

                //写入新状态
                CaseEventInfo newEvent = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.ZB7,
                    SourceStatus = lastStatus,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo,
                    Finished = true
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent);

                caseObj.Disabled = true;
                caseObj.ModifyDate = DateTime.Now;
                caseObj.ModifyUserSysNo = modifyUserSysNo;
                ServiceDepository.CaseDataAccess.Update(caseObj);


                return newEvent;
            }

            /// <summary>
            /// 结案.
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="actionCfg">The action CFG.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            /// <returns></returns>
            CaseEventInfo Any_ZB13(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {
                //查找全部的源事件
                var fromEvents = ServiceDepository.CaseEventDataAccess.Get(caseObj.CaseId).OrderByDescending(x => x.CreateDate);

                //必须满足HA5完成，HD6或ZB10完成，交房完成
                if (fromEvents.Count(o => o.EventType == CaseStatus.HA5 && o.Finished) == 0)
                    throw new Exception("立案未结束，不能结案");

                if (fromEvents.Count(o => o.EventType == CaseStatus.ZB8 && o.Finished) == 0)
                    throw new Exception("交房未完成，不能结案");

                if (fromEvents.Count(o => o.EventType == CaseStatus.HD6 && o.Finished) == 0)
                {
                    //不能出现ZB10的下一步ZB3
                    if (fromEvents.Count(o => o.EventType == CaseStatus.ZB10) == 0
                        || fromEvents.Count(o => o.EventType == CaseStatus.ZB3) > 0)
                        throw new Exception("还贷未完成，不能结案");
                }

                #region 实收实付验证
                Decimal seller_IncomeAmount = 0;
                Decimal seller_ExpenditureAmount = 0;
                Decimal buyer_IncomeAmount = 0;
                Decimal buyer_ExpenditureAmount = 0;
                //验证实收实付
                var tradeAmountData = ServiceDepository.AccountService.GetCaseTradeAmount(caseObj.CaseId);
                if (tradeAmountData != null)
                {
                    //卖方
                    var sellerAccount = tradeAmountData.Item1;
                    //买方
                    var buyerAccount = tradeAmountData.Item2;

                    //卖方实收
                    seller_IncomeAmount = sellerAccount[OrderType.Collection];
                    //卖方实付
                    seller_ExpenditureAmount = sellerAccount[OrderType.Payment];
                    //买方实收
                    buyer_IncomeAmount = buyerAccount[OrderType.Collection];
                    //买方实付
                    buyer_ExpenditureAmount = buyerAccount[OrderType.Payment];
                }
                #endregion

                #region 应收应付验证
                var customerFundFlowList = ServiceDepository.CustomerFundFlowService.GetFundFlows(caseObj.CaseId);
                Decimal incomeAmount = 0;
                Decimal expenditureAmount = 0;
                //卖方应收
                Decimal incomeAmount_Seller = 0;
                //卖方应付
                Decimal expenditureAmount_Seller = 0;

                //买方应收
                Decimal incomeAmount_Buyer = 0;
                //买方应付
                Decimal expenditureAmount_Buyer = 0;
                foreach (var flow in customerFundFlowList)
                {
                    if (flow.Amount > 0)
                    {
                        incomeAmount += flow.Amount;
                        if (flow.CustomerType == CustomerType.Seller)
                            incomeAmount_Seller += flow.Amount;
                        else if (flow.CustomerType == CustomerType.Buyer)
                            incomeAmount_Buyer += flow.Amount;
                    }
                    else
                    {
                        expenditureAmount += flow.Amount;
                        if (flow.CustomerType == CustomerType.Seller)
                            expenditureAmount_Seller += flow.Amount;
                        else if (flow.CustomerType == CustomerType.Buyer)
                            expenditureAmount_Buyer += flow.Amount;
                    }
                }
                #endregion

                #region 验证金额是否相等
                //买方应收和买方实收 相等
                if (incomeAmount_Buyer != buyer_IncomeAmount)
                {
                    throw new Exception("买方应收和买方实收不相等,无法结案.");
                }
                //买方应付和买方实收 相等
                if (expenditureAmount_Buyer != buyer_ExpenditureAmount)
                {
                    throw new Exception("买方应付和买方实付不相等,无法结案.");
                }
                //卖方应收和卖方实收 相等
                if (seller_IncomeAmount != incomeAmount_Seller)
                {
                    throw new Exception("卖方应收和卖方实收不相等,无法结案.");
                }
                //卖方应付和卖方实付 相等
                if (expenditureAmount_Seller != seller_ExpenditureAmount)
                {
                    throw new Exception("卖方应付和卖方实付不相等,无法结案.");
                }
                #endregion

                #region 验证账户余额
                var buyerAccountBalance = ServiceDepository.AccountService.GetBalance(AccountType.Buyer, caseObj.CaseId);
                var sellerAccountBalance = ServiceDepository.AccountService.GetBalance(AccountType.Seller, caseObj.CaseId);
                if (buyerAccountBalance > 0 && sellerAccountBalance > 0)
                {
                    throw new Exception("买卖方钱款均未结清,无法结案.");
                }
                else if (buyerAccountBalance > 0)
                {
                    throw new Exception("买方钱款未结清,无法结案.");
                }
                else if (sellerAccountBalance > 0)
                {
                    throw new Exception("卖方钱款未结清,无法结案.");
                }
                #endregion


                foreach (var fe in fromEvents)
                {
                    if (!fe.Finished)
                    {
                        //源状态设为完成
                        fe.Finished = true;
                        fe.ModifyUserSysNo = modifyUserSysNo;
                        fe.ModifyDate = DateTime.Now;
                        ServiceDepository.CaseEventDataAccess.Update(fe);
                    }
                }

                //写入新状态
                CaseEventInfo newEvent = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.ZB13,
                    SourceStatus = CaseStatus.ZB8,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo,
                    Finished = true
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent);

                return newEvent;

            }

            /// <summary>
            /// 签约完成，并后处理分支动作
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="actionCfg">The action CFG.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            /// <returns></returns>
            CaseEventInfo HA1_HA2(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {
                FinishAncestorAction(caseObj, actionCfg, modifyUserSysNo);

                //是否审税
                bool isTrialTax = false;
                if (otherData != null && otherData.ContainsKey("_isTrialTax"))
                {
                    isTrialTax = (bool)otherData["_isTrialTax"];
                    otherData.Remove("_isTrialTax");
                }
                //是否申请贷款
                bool isLoan = false;
                if (otherData != null && otherData.ContainsKey("_isLoan"))
                {
                    isLoan = (bool)otherData["_isLoan"];
                    otherData.Remove("_isLoan");
                }

                //网签价格
                decimal netlabelPrice = 0;
                if (otherData != null && otherData.ContainsKey("_netlabelPrice"))
                {
                    netlabelPrice = (decimal)otherData["_netlabelPrice"];
                    otherData.Remove("_netlabelPrice");
                }

                if(netlabelPrice<=0)
                    throw new Exception("请填写网签价格");

                var caseRemark = new PinganHouse.SHHTS.DataTransferObjects.CaseRemark
                {
                    SysNo = caseObj.SysNo,
                    NetlabelPrice = netlabelPrice,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseService.PerfectCaseInfo(caseRemark);



                //写入新状态
                CaseEventInfo newEvent = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.HA2,
                    SourceStatus = CaseStatus.HA1,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent);

                //签约完成后处理分支动作

                CaseEventInfo loanEvent = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = isLoan ? CaseStatus.DK1 : CaseStatus.ZB9,
                    SourceStatus = CaseStatus.HA1,
                    OtherDatas = otherData,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseEventDataAccess.Insert(loanEvent);


                CaseEventInfo taxEvent = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = isTrialTax ? CaseStatus.ZB1 : CaseStatus.ZB4,
                    SourceStatus = CaseStatus.HA1,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseEventDataAccess.Insert(taxEvent);

                //关联状态ZB1A1，将数据带到关联状态
                if (isTrialTax)
                {
                    CaseEventInfo taxEvent2 = new CaseEventInfo
                    {
                        SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                        CaseId = caseObj.CaseId,
                        EventType = CaseStatus.ZB1A1,
                        SourceStatus = CaseStatus.HA1,
                        CreateDate = DateTime.Now,
                        OtherDatas = otherData,
                        CreateUserSysNo = modifyUserSysNo,
                        OperatorSysNo = modifyUserSysNo
                    };

                    ServiceDepository.CaseEventDataAccess.Insert(taxEvent2);
                }

                return newEvent;
            }

            /// <summary>
            /// 立案结束，并处理分支动作
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="actionCfg">The action CFG.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            /// <returns></returns>
            CaseEventInfo HA4_HA5(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {
                FinishAncestorAction(caseObj, actionCfg, modifyUserSysNo);

                //立案报告是否通过
                bool isPass = false;
                if (otherData != null && otherData.ContainsKey("_isPass"))
                {
                    isPass = (bool)otherData["_isPass"];
                    otherData.Remove("_isPass");
                }

                CaseEventInfo newEvent = null;

                if (isPass)
                {
                    //是否抵押
                    bool isMortgage = false;
                    if (otherData != null && otherData.ContainsKey("_isMortgage"))
                    {
                        isMortgage = (bool)otherData["_isMortgage"];
                        otherData.Remove("_isMortgage");
                    }

                    //写入新状态
                    newEvent = new CaseEventInfo
                    {
                        SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                        CaseId = caseObj.CaseId,
                        EventType = CaseStatus.HA5,
                        SourceStatus = CaseStatus.HA4,
                        OtherDatas = otherData,
                        CreateDate = DateTime.Now,
                        CreateUserSysNo = modifyUserSysNo,
                        OperatorSysNo = modifyUserSysNo,
                        Finished = true
                    };

                    ServiceDepository.CaseEventDataAccess.Insert(newEvent);


                    CaseEventInfo moEvent = new CaseEventInfo
                    {
                        SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                        CaseId = caseObj.CaseId,
                        EventType = isMortgage ? CaseStatus.ZB3 : CaseStatus.ZB10,
                        SourceStatus = CaseStatus.HA4,
                        CreateDate = DateTime.Now,
                        OtherDatas = otherData,
                        CreateUserSysNo = modifyUserSysNo,
                        OperatorSysNo = modifyUserSysNo
                    };

                    ServiceDepository.CaseEventDataAccess.Insert(moEvent);
                }
                else
                {
                    newEvent = new CaseEventInfo
                    {
                        SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                        CaseId = caseObj.CaseId,
                        EventType = CaseStatus.HA6,
                        SourceStatus = CaseStatus.HA4,
                        OtherDatas = otherData,
                        CreateDate = DateTime.Now,
                        CreateUserSysNo = modifyUserSysNo,
                        OperatorSysNo = modifyUserSysNo
                    };

                    ServiceDepository.CaseEventDataAccess.Insert(newEvent);
                }

                return newEvent;
            }


            /// <summary>
            /// 发起还贷申请，立即接单
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="actionCfg">The action CFG.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            /// <returns></returns>
            CaseEventInfo ZB3_HD1(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {
                FinishAncestorAction(caseObj, actionCfg, modifyUserSysNo);

                //写入新状态
                CaseEventInfo newEvent = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.HD1,
                    SourceStatus = CaseStatus.ZB3,
                    OtherDatas = otherData,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent);


                //写入关联状态，将数据带到关联状态
                CaseEventInfo newEvent1 = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.HD1A1,
                    SourceStatus = CaseStatus.ZB3,
                    OtherDatas = otherData,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent1);

                return newEvent;
            }

            /// <summary>
            /// 等待预约还款（主办）自循环
            /// </summary>
            /// <param name="caseObj"></param>
            /// <param name="fromStatus"></param>
            /// <param name="toStatus"></param>
            /// <param name="modifyUserSysNo"></param>
            /// <param name="otherData"></param>
            /// <returns></returns>
            CaseEventInfo HD1A1_HD1A1(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {


                /*
                 * 关联状态 自循环
                 * 1.不关闭来源动作，避免自身被finish掉
                 * 1.更新自循环数据
                 * 1.将数据的更新，同时发送给自身和原状态
                 * 3.返回关联事件对象
                 */

                //更新自循环关联事件
                CaseEventInfo newEvent = ServiceDepository.CaseEventDataAccess.Get(caseObj.CaseId, null, CaseStatus.HD1A1, false).SingleOrDefault();
                if (newEvent != null)
                {
                    newEvent.OtherDatas = otherData;
                    newEvent.ModifyUserSysNo = modifyUserSysNo;
                    newEvent.ModifyDate = DateTime.Now;
                    ServiceDepository.CaseEventDataAccess.Update(newEvent);
                }

                //更新主事件数据
                var fromEvent2 = ServiceDepository.CaseEventDataAccess.Get(caseObj.CaseId, null, CaseStatus.HD1, false).SingleOrDefault();
                if (fromEvent2 != null)
                {
                    fromEvent2.OtherDatas = otherData;
                    fromEvent2.ModifyUserSysNo = modifyUserSysNo;
                    fromEvent2.ModifyDate = DateTime.Now;
                    ServiceDepository.CaseEventDataAccess.Update(fromEvent2);
                }

                return newEvent;
            }

            /// <summary>
            /// 等待预约还款（主办）-卖家还贷已取消
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="fromStatus">From status.</param>
            /// <param name="toStatus">To status.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            /// <returns></returns>
            CaseEventInfo HD1A1_ZB12(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {
                /*
                 * 关联状态->原状态
                 * 关联的原状态，全部设置Finished=true
                 * 写入新的原状态
                 */

                FinishAncestorAction(caseObj, actionCfg, modifyUserSysNo);

                //Finished 原状态
                var aent = ServiceDepository.CaseEventDataAccess.Get(caseObj.CaseId, null, CaseStatus.HD1, false).SingleOrDefault();
                if (aent != null)
                {
                    aent.Finished = true;
                    aent.ModifyUserSysNo = modifyUserSysNo;
                    aent.ModifyDate = DateTime.Now;
                    ServiceDepository.CaseEventDataAccess.Update(aent);
                }

                //写入新状态
                CaseEventInfo newEvent = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.ZB12,
                    SourceStatus = CaseStatus.HD1A1,
                    OtherDatas = otherData,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent);

                return newEvent;
            }

            /// <summary>
            /// 等待预约还款-已确定还款时间
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="fromStatus">From status.</param>
            /// <param name="toStatus">To status.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            /// <returns></returns>
            CaseEventInfo HD1_HD2(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {
                /*
                 * 原状态->原状态
                 * 关联的关联状态，全部设置Finished=true
                 * 不修改关联状态的数据
                 */

                FinishAncestorAction(caseObj, actionCfg, modifyUserSysNo);

                //Finished 关联状态
                var aent = ServiceDepository.CaseEventDataAccess.Get(caseObj.CaseId, null, CaseStatus.HD1A1, false).SingleOrDefault();
                if (aent != null)
                {
                    aent.Finished = true;
                    aent.ModifyUserSysNo = modifyUserSysNo;
                    aent.ModifyDate = DateTime.Now;
                    ServiceDepository.CaseEventDataAccess.Update(aent);
                }

                //写入新状态
                CaseEventInfo newEvent = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.HD2,
                    SourceStatus = CaseStatus.HD1,
                    OtherDatas = otherData,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent);

                return newEvent;

            }


            /// <summary>
            /// 发起贷款申请，立即接单
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="fromStatus">From status.</param>
            /// <param name="toStatus">To status.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            /// <returns></returns>
            CaseEventInfo DK1_DK2(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {
                FinishAncestorAction(caseObj, actionCfg, modifyUserSysNo);

                //写入新状态
                CaseEventInfo newEvent = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.DK2,
                    SourceStatus = CaseStatus.DK1,
                    OtherDatas = otherData,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent);


                //写入关联状态，将数据带到关联状态
                CaseEventInfo newEvent1 = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.DK2A1,
                    SourceStatus = CaseStatus.DK1,
                    OtherDatas = otherData,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent1);

                return newEvent;
            }

            /// <summary>
            /// 等待客户预约（主办）自循环
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="actionCfg">The action CFG.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            /// <returns></returns>
            CaseEventInfo DK2A1_DK2A1(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {
                //更新自循环关联事件
                CaseEventInfo newEvent = ServiceDepository.CaseEventDataAccess.Get(caseObj.CaseId, null, CaseStatus.DK2A1, false).SingleOrDefault();
                if (newEvent != null)
                {
                    newEvent.OtherDatas = otherData;
                    newEvent.ModifyUserSysNo = modifyUserSysNo;
                    newEvent.ModifyDate = DateTime.Now;
                    ServiceDepository.CaseEventDataAccess.Update(newEvent);


                    //更新主事件数据
                    var fromEvent2 = ServiceDepository.CaseEventDataAccess.Get(caseObj.CaseId, newEvent.SourceStatus, CaseStatus.DK2, false).SingleOrDefault();
                    if (fromEvent2 != null)
                    {
                        fromEvent2.OtherDatas = otherData;
                        fromEvent2.ModifyUserSysNo = modifyUserSysNo;
                        fromEvent2.ModifyDate = DateTime.Now;
                        ServiceDepository.CaseEventDataAccess.Update(fromEvent2);
                    }
                }

                return newEvent;
            }

            /// <summary>
            /// 等待客户签约（主办）-贷款申请已取消
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="actionCfg">The action CFG.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            /// <returns></returns>
            CaseEventInfo DK2A1_ZB11(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {
                /*
                 * 关联状态->原状态
                 * 关联的原状态，全部设置Finished=true
                 * 写入新的原状态
                 */

                FinishAncestorAction(caseObj, actionCfg, modifyUserSysNo);

                //Finished 原状态
                var aent = ServiceDepository.CaseEventDataAccess.Get(caseObj.CaseId, null, CaseStatus.DK2, false).SingleOrDefault();
                if (aent != null)
                {
                    aent.Finished = true;
                    aent.ModifyUserSysNo = modifyUserSysNo;
                    aent.ModifyDate = DateTime.Now;
                    ServiceDepository.CaseEventDataAccess.Update(aent);
                }

                //写入新状态
                CaseEventInfo newEvent = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.ZB11,
                    SourceStatus = CaseStatus.DK2A1,
                    OtherDatas = otherData,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent);

                return newEvent;
            }

            /// <summary>
            /// 等待客户签约到贷款合同已签订
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="fromStatus">From status.</param>
            /// <param name="toStatus">To status.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            /// <returns></returns>
            CaseEventInfo DK2_DK3(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {
                FinishAncestorAction(caseObj, actionCfg, modifyUserSysNo);

                //Finished 关联状态
                var aent = ServiceDepository.CaseEventDataAccess.Get(caseObj.CaseId, null, CaseStatus.DK2A1, false).SingleOrDefault();
                if (aent != null)
                {
                    aent.Finished = true;
                    aent.ModifyUserSysNo = modifyUserSysNo;
                    aent.ModifyDate = DateTime.Now;
                    ServiceDepository.CaseEventDataAccess.Update(aent);
                }

                //写入新状态
                CaseEventInfo newEvent = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.DK3,
                    SourceStatus = CaseStatus.DK2,
                    OtherDatas = otherData,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent);

                return newEvent;
            }

            /// <summary>
            /// 贷款审批不通过（主办）-贷款申请已取消
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="fromStatus">From status.</param>
            /// <param name="toStatus">To status.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            /// <returns></returns>
            CaseEventInfo DK9A1_ZB11(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {
                /*
                 * 关联状态->原状态
                 * 关联的原状态，全部设置Finished=true
                 * 写入新的原状态
                 */
                FinishAncestorAction(caseObj, actionCfg, modifyUserSysNo);

                //Finished 原状态
                var aent = ServiceDepository.CaseEventDataAccess.Get(caseObj.CaseId, null, CaseStatus.DK9, false).SingleOrDefault();
                if (aent != null)
                {
                    aent.Finished = true;
                    aent.ModifyUserSysNo = modifyUserSysNo;
                    aent.ModifyDate = DateTime.Now;
                    ServiceDepository.CaseEventDataAccess.Update(aent);
                }

                //写入新状态
                CaseEventInfo newEvent = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.ZB11,
                    SourceStatus = CaseStatus.DK9A1,
                    OtherDatas = otherData,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent);

                return newEvent;
            }

            /// <summary>
            /// 贷款审批不通过（主办）-发起贷款申请
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="fromStatus">From status.</param>
            /// <param name="toStatus">To status.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            /// <returns></returns>
            CaseEventInfo DK9A1_DK1(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {
                /*
                 * 关联状态->原状态
                 * 关联的原状态，全部设置Finished=true
                 * 写入新的原状态
                 */

                FinishAncestorAction(caseObj, actionCfg, modifyUserSysNo);

                //Finished 原状态
                var aent = ServiceDepository.CaseEventDataAccess.Get(caseObj.CaseId, null, CaseStatus.DK9, false).SingleOrDefault();
                if (aent != null)
                {
                    aent.Finished = true;
                    aent.ModifyUserSysNo = modifyUserSysNo;
                    aent.ModifyDate = DateTime.Now;
                    ServiceDepository.CaseEventDataAccess.Update(aent);
                }

                //写入新状态
                CaseEventInfo newEvent = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.DK1,
                    SourceStatus = CaseStatus.DK9A1,
                    OtherDatas = otherData,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent);

                return newEvent;
            }

            /// <summary>
            /// 贷款银行审批中 到 贷款审批通过
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="fromStatus">From status.</param>
            /// <param name="toStatus">To status.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            /// <returns></returns>
            CaseEventInfo DK4_DK5(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {
                FinishAncestorAction(caseObj, actionCfg, modifyUserSysNo);


                //是否通过
                bool isPass = false;
                //是否资料不齐
                bool isFailMaterial = false;
                if (otherData != null && otherData.ContainsKey("_isPass"))
                {
                    isPass = (bool)otherData["_isPass"];
                    otherData.Remove("_isPass");
                }
                if (otherData != null && otherData.ContainsKey("_isFailMaterial"))
                {
                    isFailMaterial = (bool)otherData["_isFailMaterial"];
                    otherData.Remove("_isFailMaterial");
                }

                //新状态
                CaseEventInfo newEvent = null;


                //通过
                if (isPass)
                {
                    //Finished 关联状态
                    var aent = ServiceDepository.CaseEventDataAccess.Get(caseObj.CaseId, null, CaseStatus.DK9A1, false).SingleOrDefault();
                    if (aent != null)
                    {
                        aent.Finished = true;
                        aent.ModifyUserSysNo = modifyUserSysNo;
                        aent.ModifyDate = DateTime.Now;
                        ServiceDepository.CaseEventDataAccess.Update(aent);
                    }

                    //写入新状态
                    newEvent = new CaseEventInfo
                   {
                       SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                       CaseId = caseObj.CaseId,
                       EventType = CaseStatus.DK5,
                       SourceStatus = CaseStatus.DK4,
                       OtherDatas = otherData,
                       CreateDate = DateTime.Now,
                       CreateUserSysNo = modifyUserSysNo,
                       OperatorSysNo = modifyUserSysNo
                   };

                    ServiceDepository.CaseEventDataAccess.Insert(newEvent);


                }
                else
                {
                    //不通过
                    if (isFailMaterial)
                    {
                        //资料补齐

                        //写入新状态
                        newEvent = new CaseEventInfo
                        {
                            SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                            CaseId = caseObj.CaseId,
                            EventType = CaseStatus.DK10,
                            SourceStatus = CaseStatus.DK4,
                            OtherDatas = otherData,
                            CreateDate = DateTime.Now,
                            CreateUserSysNo = modifyUserSysNo,
                            OperatorSysNo = modifyUserSysNo
                        };

                        ServiceDepository.CaseEventDataAccess.Insert(newEvent);
                    }
                    else
                    {
                        newEvent = new CaseEventInfo
                        {
                            SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                            CaseId = caseObj.CaseId,
                            EventType = CaseStatus.DK9,
                            SourceStatus = CaseStatus.DK4,
                            OtherDatas = otherData,
                            CreateDate = DateTime.Now,
                            CreateUserSysNo = modifyUserSysNo,
                            OperatorSysNo = modifyUserSysNo
                        };

                        ServiceDepository.CaseEventDataAccess.Insert(newEvent);

                        //关联状态
                        var newEvent2 = new CaseEventInfo
                         {
                             SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                             CaseId = caseObj.CaseId,
                             EventType = CaseStatus.DK9A1,
                             SourceStatus = CaseStatus.DK4,
                             OtherDatas = otherData,
                             CreateDate = DateTime.Now,
                             CreateUserSysNo = modifyUserSysNo,
                             OperatorSysNo = modifyUserSysNo
                         };

                        ServiceDepository.CaseEventDataAccess.Insert(newEvent);
                    }
                }


                return newEvent;
            }

            /// <summary>
            /// 限购查询中 -等待预约过户
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="actionCfg">The action CFG.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            CaseEventInfo JY4_JY6(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {
                FinishAncestorAction(caseObj, actionCfg, modifyUserSysNo);

                //是否限购
                bool isLimit = false;
                if (otherData != null && otherData.ContainsKey("_isLimit"))
                {
                    isLimit = (bool)otherData["_isLimit"];
                    otherData.Remove("_isLimit");
                }

                //新状态
                CaseEventInfo newEvent = null;

                if (!isLimit)
                {
                    //写入新状态
                    newEvent = new CaseEventInfo
                    {
                        SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                        CaseId = caseObj.CaseId,
                        EventType = CaseStatus.JY6,
                        SourceStatus = CaseStatus.JY4,
                        OtherDatas = otherData,
                        CreateDate = DateTime.Now,
                        CreateUserSysNo = modifyUserSysNo,
                        OperatorSysNo = modifyUserSysNo
                    };

                    ServiceDepository.CaseEventDataAccess.Insert(newEvent);
                }
                else
                {
                    //写入新状态
                    newEvent = new CaseEventInfo
                    {
                        SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                        CaseId = caseObj.CaseId,
                        EventType = CaseStatus.JY9,
                        SourceStatus = CaseStatus.JY4,
                        OtherDatas = otherData,
                        CreateDate = DateTime.Now,
                        CreateUserSysNo = modifyUserSysNo,
                        OperatorSysNo = modifyUserSysNo
                    };

                    ServiceDepository.CaseEventDataAccess.Insert(newEvent);
                }

                return newEvent;
            }

            /// <summary>
            /// 等待出产证（他证） 贷款 - 等待银行放款
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="actionCfg">The action CFG.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            /// <returns></returns>
            CaseEventInfo JY5_DK7(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {

                //依赖于领新产证
                var cen = ServiceDepository.CaseEventDataAccess.Get(caseObj.CaseId, null, CaseStatus.JY12, true).SingleOrDefault();

                if (cen == null)
                    throw new Exception("等待领取新产证");

                FinishAncestorAction(caseObj, actionCfg, modifyUserSysNo);

                //写入新状态

                CaseEventInfo newEvent = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.DK7,
                    SourceStatus = CaseStatus.JY5,
                    OtherDatas = otherData,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent);

                return newEvent;
            }

            /// <summary>
            /// 等待预约审税-已约审税限购时间
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="actionCfg">The action CFG.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            /// <returns></returns>
            CaseEventInfo ZB4_ZB1(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {
                FinishAncestorAction(caseObj, actionCfg, modifyUserSysNo);

                //写入新状态
                CaseEventInfo newEvent = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.ZB1,
                    SourceStatus = CaseStatus.ZB4,
                    OtherDatas = otherData,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent);

                //写入关联状态
                CaseEventInfo newEvent2 = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.ZB1A1,
                    SourceStatus = CaseStatus.ZB4,
                    OtherDatas = otherData,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent2);

                return newEvent;
            }


            /// <summary>
            /// 已约审税限购时间（主办）自循环
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="actionCfg">The action CFG.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            /// <returns></returns>
            CaseEventInfo ZB1A1_ZB1A1(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {
                //更新自循环关联事件
                CaseEventInfo newEvent = ServiceDepository.CaseEventDataAccess.Get(caseObj.CaseId, null, CaseStatus.ZB1A1, false).SingleOrDefault();
                if (newEvent != null)
                {
                    newEvent.OtherDatas = otherData;
                    newEvent.ModifyUserSysNo = modifyUserSysNo;
                    newEvent.ModifyDate = DateTime.Now;
                    ServiceDepository.CaseEventDataAccess.Update(newEvent);



                    //更新主事件数据
                    var fromEvent2 = ServiceDepository.CaseEventDataAccess.Get(caseObj.CaseId, newEvent.SourceStatus, CaseStatus.ZB1, false).SingleOrDefault();
                    if (fromEvent2 != null)
                    {
                        fromEvent2.OtherDatas = otherData;
                        fromEvent2.ModifyUserSysNo = modifyUserSysNo;
                        fromEvent2.ModifyDate = DateTime.Now;
                        ServiceDepository.CaseEventDataAccess.Update(fromEvent2);
                    }
                }

                return newEvent;
            }

            /// <summary>
            /// 已约审税限购时间-接受审税限购预约.
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="actionCfg">The action CFG.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            /// <returns></returns>
            CaseEventInfo ZB1_JY1(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {
                FinishAncestorAction(caseObj, actionCfg, modifyUserSysNo);

                //写入新状态
                CaseEventInfo newEvent = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.JY1,
                    SourceStatus = CaseStatus.ZB1,
                    OtherDatas = otherData,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent);

                //写入关联状态
                CaseEventInfo newEvent2 = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.JY1A1,
                    SourceStatus = CaseStatus.ZB1,
                    OtherDatas = otherData,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent2);

                //关闭ZB1的关联关联状态
                var event3 = ServiceDepository.CaseEventDataAccess.Get(caseObj.CaseId, null, CaseStatus.ZB1A1, false).SingleOrDefault();
                if (event3 != null)
                {
                    event3.Finished = true;
                    event3.ModifyUserSysNo = modifyUserSysNo;
                    event3.ModifyDate = DateTime.Now;
                    ServiceDepository.CaseEventDataAccess.Update(event3);
                }

                return newEvent;
            }

            /// <summary>
            /// 接受审税限购预约（主办）重新发单 已约审税限购时间.
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="actionCfg">The action CFG.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            /// <returns></returns>
            CaseEventInfo JY1A1_ZB1(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {
                FinishAncestorAction(caseObj, actionCfg, modifyUserSysNo);

                //写入新状态
                CaseEventInfo newEvent = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.ZB1,
                    SourceStatus = CaseStatus.JY1A1,
                    OtherDatas = otherData,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent);

                //写入关联状态
                CaseEventInfo newEvent2 = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.ZB1A1,
                    SourceStatus = CaseStatus.JY1A1,
                    OtherDatas = otherData,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent2);

                //Finished 原状态
                var aent = ServiceDepository.CaseEventDataAccess.Get(caseObj.CaseId, null, CaseStatus.JY1, false).SingleOrDefault();
                if (aent != null)
                {
                    aent.Finished = true;
                    aent.ModifyUserSysNo = modifyUserSysNo;
                    aent.ModifyDate = DateTime.Now;
                    ServiceDepository.CaseEventDataAccess.Update(aent);
                }

                return newEvent;
            }

            /// <summary>
            /// js the y1_ j y2.
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="actionCfg">The action CFG.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            /// <returns></returns>
            CaseEventInfo JY1_JY2(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {
                FinishAncestorAction(caseObj, actionCfg, modifyUserSysNo);

                //写入新状态
                CaseEventInfo newEvent = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.JY2,
                    SourceStatus = CaseStatus.JY1,
                    OtherDatas = otherData,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent);

                //Finished 关联状态
                var aent = ServiceDepository.CaseEventDataAccess.Get(caseObj.CaseId, null, CaseStatus.JY1A1, false).SingleOrDefault();
                if (aent != null)
                {
                    aent.Finished = true;
                    aent.ModifyUserSysNo = modifyUserSysNo;
                    aent.ModifyDate = DateTime.Now;
                    ServiceDepository.CaseEventDataAccess.Update(aent);
                }

                return newEvent;
            }

            /// <summary>
            /// 等待预约过户- 等待过户
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="actionCfg">The action CFG.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            /// <returns></returns>
            CaseEventInfo JY6_JY8(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {
                FinishAncestorAction(caseObj, actionCfg, modifyUserSysNo);

                //写入新状态
                CaseEventInfo newEvent = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.JY8,
                    SourceStatus = CaseStatus.JY6,
                    OtherDatas = otherData,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent);

                //写入关联状态
                CaseEventInfo newEvent2 = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.JY8A1,
                    SourceStatus = CaseStatus.JY6,
                    OtherDatas = otherData,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent2);

                return newEvent;
            }

            /// <summary>
            /// 等待过户（主办）自循环
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="fromStatus">From status.</param>
            /// <param name="toStatus">To status.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            /// <returns></returns>
            CaseEventInfo JY8A1_JY8A1(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {
                //更新自循环关联事件
                CaseEventInfo newEvent = ServiceDepository.CaseEventDataAccess.Get(caseObj.CaseId, null, CaseStatus.JY8A1, false).SingleOrDefault();
                if (newEvent != null)
                {
                    newEvent.OtherDatas = otherData;
                    newEvent.ModifyUserSysNo = modifyUserSysNo;
                    newEvent.ModifyDate = DateTime.Now;
                    ServiceDepository.CaseEventDataAccess.Update(newEvent);
                }

                //更新主事件数据
                var fromEvent2 = ServiceDepository.CaseEventDataAccess.Get(caseObj.CaseId, null, CaseStatus.JY8, false).SingleOrDefault();
                if (fromEvent2 != null)
                {
                    fromEvent2.OtherDatas = otherData;
                    fromEvent2.ModifyUserSysNo = modifyUserSysNo;
                    fromEvent2.ModifyDate = DateTime.Now;
                    ServiceDepository.CaseEventDataAccess.Update(fromEvent2);
                }

                return newEvent;
            }

            /// <summary>
            /// 等待过户-客户已到场
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="fromStatus">From status.</param>
            /// <param name="toStatus">To status.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            /// <returns></returns>
            CaseEventInfo JY8_JY7(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {
                FinishAncestorAction(caseObj, actionCfg, modifyUserSysNo);

                //写入新状态
                CaseEventInfo newEvent = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.JY7,
                    SourceStatus = CaseStatus.JY8,
                    OtherDatas = otherData,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent);

                //Finished 关联状态
                var aent = ServiceDepository.CaseEventDataAccess.Get(caseObj.CaseId, null, CaseStatus.JY8A1, false).SingleOrDefault();
                if (aent != null)
                {
                    aent.Finished = true;
                    aent.ModifyUserSysNo = modifyUserSysNo;
                    aent.ModifyDate = DateTime.Now;
                    ServiceDepository.CaseEventDataAccess.Update(aent);
                }

                return newEvent;
            }

            /// <summary>
            /// 税费已缴清-等出产证(他证)
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="fromStatus">From status.</param>
            /// <param name="toStatus">To status.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            /// <returns></returns>
            CaseEventInfo JY10_JY11(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {
                FinishAncestorAction(caseObj, actionCfg, modifyUserSysNo);

                //写入新状态
                CaseEventInfo newEvent = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.JY11,
                    SourceStatus = CaseStatus.JY10,
                    OtherDatas = otherData,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent);

                //写入关联状态
                CaseEventInfo newEvent2 = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.JY11A1,
                    SourceStatus = CaseStatus.JY10,
                    OtherDatas = otherData,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent2);

                return newEvent;
            }

            /// <summary>
            /// 等出产证(他证)（主办）- 过户完成.
            /// </summary>
            /// <param name="caseObj">The case object.</param>
            /// <param name="fromStatus">From status.</param>
            /// <param name="toStatus">To status.</param>
            /// <param name="modifyUserSysNo">The modify user system no.</param>
            /// <param name="otherData">The other data.</param>
            /// <returns></returns>
            CaseEventInfo JY11A1_JY12(Case caseObj, ActionCfg actionCfg, long modifyUserSysNo, Dictionary<string, object> otherData = null)
            {
                FinishAncestorAction(caseObj, actionCfg, modifyUserSysNo);

                //写入新状态
                CaseEventInfo newEvent = new CaseEventInfo
                {
                    SysNo = PinganHouse.SHHTS.Utils.Sequence.Get(),
                    CaseId = caseObj.CaseId,
                    EventType = CaseStatus.JY12,
                    SourceStatus = CaseStatus.JY11A1,
                    OtherDatas = otherData,
                    CreateDate = DateTime.Now,
                    CreateUserSysNo = modifyUserSysNo,
                    OperatorSysNo = modifyUserSysNo,
                    Finished=true
                };

                ServiceDepository.CaseEventDataAccess.Insert(newEvent);

                //Finished 关联的原状态
                var aent = ServiceDepository.CaseEventDataAccess.Get(caseObj.CaseId, null, CaseStatus.JY11, false).SingleOrDefault();
                if (aent != null)
                {
                    aent.Finished = true;
                    aent.ModifyUserSysNo = modifyUserSysNo;
                    aent.ModifyDate = DateTime.Now;
                    ServiceDepository.CaseEventDataAccess.Update(aent);
                }

                return newEvent;
            }
        }
    }
}
