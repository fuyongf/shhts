﻿using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.ServiceImpl
{
    public class MessageService : IMessageService
    {
#if DEBUG
        const string WEB_HOST = "test.erpweixin.pinganhaofang.com";
#else
        const string WEB_HOST = "erpweixin.pinganhaofang.com";
#endif
        /// <summary>
        /// 发送付款消息
        /// </summary>
        /// <param name="trade"></param>
        /// <param name="order"></param>
        /// <param name="account"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public OperationResult SendPaymentMessage(string tradeNo, PaymentStatus status)
        {
            try
            {
                if (status != PaymentStatus.Success)
                    return new OperationResult(1, "支付未完成");
                var trade = ServiceDepository.TradeDataAccess.Get(tradeNo);
                if (trade == null)
                    return new OperationResult(2, "交易明细不存在");
                var order = ServiceDepository.OrderDataAccess.Get(trade.OrderNo);
                if (order == null)
                    return new OperationResult(3, "订单不存在");
                if (order.OrderType != OrderType.Payment)
                    return new OperationResult(4, "订单用途不正确");
                //付款帐户
                var payerAccount = ServiceDepository.AccountDataAccess.Get(order.AccountSysNo, order.OrderBizType);
                if (payerAccount == null)
                    return new OperationResult(5, "付款帐号不存在");
                var caseInfo = ServiceDepository.CaseDataAccess.GetCaseByCaseId(payerAccount.SubjectId);
                if (caseInfo == null)
                    return new OperationResult(7, "帐号主体有误");
                //发送客户消息
                SendCustomerMessage(trade, order, payerAccount, caseInfo);
                //企业号消息
                SendAgencyFeeMessage(trade, order, payerAccount.SubjectId, caseInfo);
                SendPaymentSmsMessage(trade, order, payerAccount, caseInfo);
                return new OperationResult(0);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-1, "系统内部错误");
            }
        }

        /// <summary>
        /// 发送收款消息
        /// </summary>
        /// <param name="trade"></param>
        /// <param name="order"></param>
        /// <param name="account"></param>
        /// <param name="status"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public OperationResult SendCollectionMessage(string tradeNo, ReconciliationStatus status, decimal amount)
        {
            try
            {
                if (status != ReconciliationStatus.Success)
                    return new OperationResult(1, "收款未成功");
                var trade = ServiceDepository.TradeDataAccess.Get(tradeNo);
                if (trade == null)
                    return new OperationResult(2, "交易流水不存在");
                var order = ServiceDepository.OrderDataAccess.Get(trade.OrderNo);
                if (order == null)
                    return new OperationResult(3, "订单不存在");
                if (order.OrderType != OrderType.Collection && order.OrderType != OrderType.Expend)
                    return new OperationResult(4, "该订单不是收款订单或消费订单");
                var account = ServiceDepository.AccountDataAccess.Get(order.AccountSysNo, order.OrderBizType);
                if (account == null)
                    return new OperationResult(5, "收款帐号不存在");
                if (order.OrderBizType == OrderBizType.HosingFund && account.AccountType != AccountType.Buyer)
                    return new OperationResult(6, "房款归属错误");
                if (order.OrderBizType == OrderBizType.Collection)// || order.OrderBizType == OrderBizType.Refund
                    return new OperationResult(3, "该订单用途无需发消息");
                IList<CaseUserRelation> users = ServiceDepository.CaseDataAccess.GetRelation(account.SubjectId);
                if (users == null)
                    throw new ArgumentException("客户不存在");
                var caseInfo = ServiceDepository.CaseDataAccess.GetCaseByCaseId(account.SubjectId);
                if (caseInfo == null)
                    return new OperationResult(7, "账户主体不存在");
                SmsCategory smsCategory = SmsCategory.USER_PAY_FWFEI;
                //如果是收款订单
                if (order.OrderType == OrderType.Collection)               
                    smsCategory = SmsCategory.DAISHOU_KUANGXIANG;
                this.SendCollectioWXMessage(caseInfo, account, trade, order, Math.Abs(amount), users);
                this.SendCollectioSmsMessage(caseInfo, account, order, Math.Abs(amount), users, smsCategory);
                return new OperationResult(0);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-1, "系统内部错误");
            }
        }

        public OperationResult SendExpendSmsMessage(List<OrderInfo> orders)
        {
            try
            {
                if (orders == null || orders.Count == 0)
                    return new OperationResult(1, "未存在支付服务单");
                var account = ServiceDepository.AccountDataAccess.Get(orders[0].AccountSysNo);
                if (account == null)
                    return new OperationResult(4, "收款帐号不存在");
                IList<CaseUserRelation> users = ServiceDepository.CaseDataAccess.GetRelation(account.SubjectId);
                if (users == null)
                    throw new ArgumentException("客户不存在");
                var caseInfo = ServiceDepository.CaseDataAccess.GetCaseByCaseId(account.SubjectId);
                if (caseInfo == null)
                    return new OperationResult(5, "账户主体不存在");
                foreach (var order in orders)
                {
                    if (order.OrderType != OrderType.Expend)
                        return new OperationResult(3, "订单类型不匹配");
                    if (order.OrderBizType != OrderBizType.EFQServiceCharge)
                        return new OperationResult(2, "交易类型不正确");
                    this.SendCollectioSmsMessage(caseInfo, account, order, order.OrderAmount, users, SmsCategory.USER_PAY_FWFEI);
                    return new OperationResult(0);
                }
                return new OperationResult(0);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-1, "系统内部错误");
            }
        }

        /// <summary>
        /// 发送过户提醒消息
        /// 经纪人
        /// </summary>
        /// <param name="caseEvent"></param>
        /// <param name="bookTime"></param>
        /// <returns></returns>
        public OperationResult SendTransferRemindMessage(CaseEventInfo caseEvent, DateTime bookTime)
        {
            var caseInfo = ServiceDepository.CaseDataAccess.GetCaseByCaseId(caseEvent.CaseId);
            if (caseInfo == null)
                return new OperationResult(1, "案件不存在");
            var agency = ServiceDepository.CaseDataAccess.GetRelation(caseInfo.CaseId, 2);
            if (agency == null)
                new OperationResult(2, "案件不存在中介");
            var agentStaff = ServiceDepository.AgentStaffDataAccess.Get(agency[0].UserSysNo);
            if (agentStaff == null)
                new OperationResult(3, "中介异常");
            var messageContent = ServiceDepository.SettingService.GetSettingItemValueByPath("/System/Parter/WeiXin/QY/Agency/Messages/TransferRemind");
            if (string.IsNullOrWhiteSpace(messageContent))
                return new OperationResult(4, "未存在消息内容");
            var stern = ServiceDepository.SettingService.GetSettingItemValueByPath("/System/Parter/WeiXin/QY/Agency/Messages");
            if (string.IsNullOrWhiteSpace(stern))
                return new OperationResult(5, "stern is null");
            string contact = string.Empty;
            if (caseInfo.CreateUserSysNo != null)
            {
                var user = ServiceDepository.UserDataAccess.Get(caseInfo.CreateUserSysNo.Value);
                if (user != null)
                    contact = string.Format("{0} {1}", user.RealName, user.Mobile);
            }
            string message = string.Format("{0}{1}{2}", string.Format(messageContent, bookTime.ToString("yyyy-MM-dd HH:mm"), string.Format("{0}({1})", caseInfo.CaseId, caseInfo.TenementAddress)), "\n\r",
                string.Format(stern, contact));

            return ServiceDepository.ParterChannelService.SendTextMessage(AgentType.Agency, message, ReceiverType.ToUser, agentStaff.UserId);
        }

        /// <summary>
        /// 发送月度账单
        /// 经纪公司
        /// </summary>
        /// <param name="companys"></param>
        public void SendMonthBillMessage(IList<AgentCompanyInfo> companys)
        {
            if (companys == null || companys.Count == 0)
                return;
            var messageFomat = ServiceDepository.SettingService.GetSettingItemValueByPath("/System/Parter/WeiXin/QY/AgencyACompany/Messages/Trade/MonthlyBiil");
            if (string.IsNullOrWhiteSpace(messageFomat))
                return;
            var beforeMonth = DateTime.Now.AddDays(-(DateTime.Now.Day));
            var days = DateTime.DaysInMonth(beforeMonth.Year, beforeMonth.Month) - 1;
            var startDate = beforeMonth.AddDays(-days).Date;
            var endDate = DateTime.Now.AddDays(-(DateTime.Now.Day - 1)).Date;
            string stern = ServiceDepository.SettingService.GetSettingItemValueByPath("/System/Parter/WeiXin/QY/AgencyACompany/Messages");
            Parallel.ForEach(companys, c =>
            {
                var key = c.SysNo.ToString();
                try
                {
                    var account = ServiceDepository.AccountService.GetAccountBill(AccountType.Agency, key, startDate, endDate);
                    if (account != null)
                    {
                        var message = string.Format(messageFomat, startDate.ToString("yyyy/MM/dd"), endDate.AddSeconds(-1).ToString("yyyy/MM/dd"),
                                        account.CollectionAmount, account.PaymentAmount, account.CollectionAmount - account.PaymentAmount, stern, key, beforeMonth.Month);
                        var result = ServiceDepository.ParterChannelService.SendTextMessage(AgentType.AgentCompany, message, ReceiverType.ToDepartment, key);
                        if (!result.Success)
                            Log.Info(string.Format("公司 {0} 的月度账单发送失败，失败原因：{1}{2}消息内容：{3}", key, result.ResultMessage, Environment.NewLine, message));
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                }
            });
        }

        #region 微信付款消息组装

        private OperationResult SendCustomerMessage(TradeInfo trade, OrderInfo order, AccountInfo payerAccount, Case caseInfo)
        {
            try
            {
                var settings = ServiceDepository.SettingService.GetSettingItemsByPath(string.Format("/System/Parter/WeiXin/Templates/Trade/Payment/Success/{0}", (int)order.OrderBizType));
                if (settings == null || settings.Count() == 0)
                    return new OperationResult(6, "消息配置不存在");

                IList<CaseUserRelation> users = null;
                //房款买卖家都发消息
                if (order.OrderBizType == OrderBizType.HosingFund)
                {
                    users = ServiceDepository.CaseDataAccess.GetRelation(caseInfo.CaseId);
                }
                else
                {
                    users = ServiceDepository.CaseDataAccess.GetRelation(caseInfo.CaseId, (int)payerAccount.AccountType);
                }
                var ster = ServiceDepository.SettingService.GetSettingItemValueByPath("/System/Parter/WeiXin/Templates/Trade");
                foreach (var user in users)
                {
                    if (user.CaseUserType == CaseUserType.AgentStaff || user.CaseUserType == CaseUserType.Handler)
                        continue;
                    var setting = settings.FirstOrDefault(s => string.Compare(s.Name, user.CaseUserType.ToString()) == 0);
                    if (setting == null)
                        continue;
                    var dic = SetPaymentDic(trade, order, setting, ster, caseInfo);
                    ServiceDepository.ParterChannelService.SendTemplateMessage(user.UserSysNo, setting.Description, dic);
                }
                return new OperationResult(0);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new OperationResult(-1, "系统错误");
            }

        }

        private IDictionary<string, object> SetPaymentDic(TradeInfo trade, OrderInfo order, Setting setting, string stern, Case caseInfo)
        {
            var dic = new Dictionary<string, object>
                {
                    {"topcolor","#FF0000"},
                };

            var data = JsonSerializer.DeserializeToObject<Dictionary<string, Dictionary<string, object>>>(setting.Value);
            dic["url"] = string.Format("http://{3}/Transaction/TransferDetails?tradeNo={0}&caseId={1}&orderNo={2}", trade.TradeNo, caseInfo.CaseId, order.OrderNo, WEB_HOST);
            data = JsonSerializer.DeserializeToObject<Dictionary<string, Dictionary<string, object>>>(setting.Value);
            data["keyword1"]["value"] = trade.PaymentDate.Value.ToString("yyyy-MM-dd HH:mm:ss").TrimEnd("00:00:00");
            data["keyword3"]["value"] = Math.Abs(trade.Amount).ToString("0.00");
            data["remark"]["value"] = SetRemark(data["remark"]["value"].ToString().Replace("#Purpose#", order.OrderBizType.GetDescription())
                .Replace("#TenementAddress#", caseInfo.TenementAddress).Replace("#CaseId#", caseInfo.CaseId)
                .Replace("#BankName#", trade.SubBankName).Replace("#BankCard#", trade.BankAccountNo), stern);
            data["first"]["value"] = string.Format("{0}{1}", data["first"]["value"], Environment.NewLine);
            dic["data"] = data;
            return dic;
        }

        private void SendAgencyFeeMessage(TradeInfo trade, OrderInfo order, string caseId, Case caseInfo)
        {
            if (order.OrderBizType != OrderBizType.AgencyFee)
                return;
            var agency = ServiceDepository.CaseDataAccess.GetRelation(caseId, 2);
            if (agency == null || agency.Count == 0)
                return;
            //案件中经纪人仅且一个
            var agentStaff = ServiceDepository.AgentStaffDataAccess.Get(agency[0].UserSysNo);
            if (agentStaff == null)
                return;
            var stern = ServiceDepository.SettingService.GetSettingItemValueByPath("/System/Parter/WeiXin/QY/Agency/Messages");
            if (string.IsNullOrWhiteSpace(stern))
                return;
            var settings = ServiceDepository.SettingService.GetSettingItemsByPath("/System/Parter/WeiXin/QY/Agency/Messages/Trade/Payment/AgencyFee");
            if (settings == null || settings.Count() == 0)
                return;
            stern = string.Format("\n\r{0}", stern);
            //发送付中介佣金成功消息
            this.SendAgencyFeeSuccessMessage(trade, order, caseInfo, stern, settings, agentStaff);
            //发送中介佣金已结清消息
            this.SendSettledMessage(trade, caseInfo, stern, settings.FirstOrDefault(s => string.Compare(s.Name, "Settlement", true) == 0), agentStaff);
        }

        private void SendAgencyFeeSuccessMessage(TradeInfo trade, OrderInfo order, Case caseInfo, string stern, IEnumerable<Setting> settings, AgentStaffInfo agentStaff)
        {
            var successMessage = settings.FirstOrDefault(s => string.Compare(s.Name, "Success", true) == 0);
            if (successMessage == null)
                return;
            string message = string.Format(successMessage.Value, string.Format("{0}({1})", caseInfo.CaseId, caseInfo.TenementAddress), Math.Abs(trade.Amount).ToString("0.00"),
                trade.PaymentDate != null ? trade.PaymentDate.Value.ToString("yyyy-MM-dd HH:mm") : "--", stern, trade.TradeNo, caseInfo.CaseId, order.OrderNo);
            ServiceDepository.ParterChannelService.SendTextMessage(AgentType.Agency, message, ReceiverType.ToUser, agentStaff.UserId);
            this.SendAgencyFeeSuccessMessageInCompany(trade, order, caseInfo, agentStaff);
        }

        private void SendAgencyFeeSuccessMessageInCompany(TradeInfo trade, OrderInfo order, Case caseInfo, AgentStaffInfo agentStaff)
        {
            var setting = ServiceDepository.SettingService.GetSettingItemValueByPath("/System/Parter/WeiXin/QY/AgencyACompany/Messages/Trade/Payment/AgencyFee/Success");
            if (string.IsNullOrWhiteSpace(setting))
                return;
            var stern = ServiceDepository.SettingService.GetSettingItemValueByPath("/System/Parter/WeiXin/QY/AgencyACompany/Messages");
            string message = string.Format(setting, agentStaff.RealName, string.Format("{0}({1})", caseInfo.CaseId, caseInfo.TenementAddress), Math.Abs(trade.Amount).ToString("0.00"),
                    trade.PaymentDate != null ? trade.PaymentDate.Value.ToString("yyyy-MM-dd HH:mm") : "--", string.Format("\n\r{0}", stern), trade.TradeNo, caseInfo.CaseId, order.OrderNo);
            ServiceDepository.ParterChannelService.SendTextMessage(AgentType.AgentCompany, message, ReceiverType.ToDepartment, agentStaff.AgentCompanySysNo.ToString());
        }

        private void SendSettledMessageInCompany(TradeInfo trade, Case caseInfo, AgentStaffInfo agentStaff)
        {
            var setting = ServiceDepository.SettingService.GetSettingItemValueByPath("/System/Parter/WeiXin/QY/AgencyACompany/Messages/Trade/Payment/AgencyFee/Settlement");
            if (string.IsNullOrWhiteSpace(setting))
                return;
            var stern = ServiceDepository.SettingService.GetSettingItemValueByPath("/System/Parter/WeiXin/QY/AgencyACompany/Messages");
            string message = string.Format(setting, agentStaff.RealName, string.Format("{0}({1})", caseInfo.CaseId, caseInfo.TenementAddress),
                    trade.PaymentDate != null ? trade.PaymentDate.Value.ToString("yyyy-MM-dd HH:mm") : "--");
            message += string.Format("\n\r{0}", stern);
            ServiceDepository.ParterChannelService.SendTextMessage(AgentType.AgentCompany, message, ReceiverType.ToDepartment, agentStaff.AgentCompanySysNo.ToString());
        }

        private void SendSettledMessage(TradeInfo trade, Case caseInfo, string stern, Setting setting, AgentStaffInfo agentStaff)
        {
            if (setting == null)
                return;
            var customerFundFlowInfos = ServiceDepository.CustomerFundFlowDataAccess.Get(caseInfo.CaseId);
            if (customerFundFlowInfos == null || customerFundFlowInfos.Count() == 0)
                return;
            decimal buyerAgencyFree = 0;
            decimal sellerAgencyFree = 0;
            foreach (var c in customerFundFlowInfos)
            {
                if (c.Brokerage > 0)
                    continue;
                var brokerage = Math.Abs(c.Brokerage);
                if (c.CustomerType == CustomerType.Buyer)
                    buyerAgencyFree += brokerage;
                else
                    sellerAgencyFree += brokerage;
            }

            var buyerOrders = ServiceDepository.OrderDataAccess.GetOrders(AccountType.Buyer, caseInfo.CaseId, OrderType.Payment, OrderBizType.AgencyFee, null, null);
            decimal actualBuyerAmount = 0;
            if (buyerOrders != null)
                actualBuyerAmount = buyerOrders.Where(b => b.OrderStatus == OrderStatus.Completed).Sum(o => Math.Abs(o.OrderAmount));
            decimal actualSellerAmount = 0;
            var sellerOrders = ServiceDepository.OrderDataAccess.GetOrders(AccountType.Seller, caseInfo.CaseId, OrderType.Payment, OrderBizType.AgencyFee, null, null);
            if (sellerOrders != null)
                actualSellerAmount = sellerOrders.Where(b => b.OrderStatus == OrderStatus.Completed).Sum(o => Math.Abs(o.OrderAmount));
            if (buyerAgencyFree == actualBuyerAmount && actualSellerAmount == sellerAgencyFree)
            {
                string message = string.Format(setting.Value, string.Format("{0}({1})", caseInfo.CaseId, caseInfo.TenementAddress),
                    trade.PaymentDate != null ? trade.PaymentDate.Value.ToString("yyyy-MM-dd HH:mm") : "--", stern);
                ServiceDepository.ParterChannelService.SendTextMessage(AgentType.Agency, message, ReceiverType.ToUser, agentStaff.UserId);
                this.SendSettledMessageInCompany(trade, caseInfo, agentStaff);
            }
        }

        #endregion
        private IDictionary<string, object> SetCollectionDic(TradeInfo trade, OrderInfo order, Case caseInfo, string setting, string stern, decimal amount, string paymenter)
        {
            var dic = new Dictionary<string, object>
                {
                    {"topcolor","#FF0000"},
                };

            dic["url"] = string.Format("http://{3}/Transaction/TransferDetails?tradeNo={0}&caseId={1}&orderNo={2}", trade.TradeNo, caseInfo.CaseId, order.OrderNo, WEB_HOST);
            var data = JsonSerializer.DeserializeToObject<Dictionary<string, Dictionary<string, object>>>(setting);
            data["first"]["value"] = string.Format("{0}{1}", data["first"]["value"].ToString().Replace("#Purpose#", order.OrderBizType.GetDescription()).Replace("#paymenter#", paymenter), Environment.NewLine);
            data["keyword1"]["value"] = trade.PaymentDate.Value.ToString("yyyy-MM-dd HH:mm:ss").TrimEnd("00:00:00");
            data["keyword2"]["value"] = order.PaymentChannel.GetDescription();
            data["keyword3"]["value"] = amount.ToString("0.00");

            data["remark"]["value"] = SetRemark(data["remark"]["value"].ToString().Replace("#Purpose#", order.OrderBizType.GetDescription())
                .Replace("#TenementAddress#", caseInfo.TenementAddress).Replace("#CaseId#", caseInfo.CaseId), stern);
            dic["data"] = data;
            return dic;
        }

        private string SetRemark(string remark, string stern)
        {
            return string.Format("{0}{1}{2}", Environment.NewLine, remark, stern);
        }

        #region 短信相关
        private void SendCollectioSmsMessage(Case caseInfo, AccountInfo account, OrderInfo order, decimal amount, IList<CaseUserRelation> users, SmsCategory smsCategory)
        {
            try
            {
                //收款发送房款及电商服务费
                var setting = ServiceDepository.SettingService.GetSettingItemByPath("/Biz/Static/Contact");
                var contact = GetContact();
                var us = users.Where(u => u.CaseUserType == (CaseUserType)((int)account.AccountType)).ToList();
                var dic = new Dictionary<string, object> 
                    {
                        { "money", Math.Abs(amount) }, 
                        { "number", caseInfo.CaseId },
                        { "phone", contact } 
                    };
                foreach (var u in us)
                {
                    var customer = ServiceDepository.CustomerDataAccess.Get(u.UserSysNo);
                    if (customer == null)
                        continue;
                    if (!ValidationMobile(customer))
                        continue;
                    var result = ServiceDepository.SmsService.Send(smsCategory, customer.Mobile, dic);
                    if (!result.Success)
                        Log.Error(string.Format("短信发送失败,模板：{0},{1}", smsCategory, result.ResultMessage));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        private bool ValidationMobile(CustomerInfo customer)
        {
            if (!Validation.Mobile(customer.Mobile))
                return false;
            var userExtend = ServiceDepository.UserExendDataAccess.Get(customer.UserSysNo);
            if (userExtend != null && !userExtend.IsSendSms)
                return false;
            return true;

        }

        private void SendCollectioWXMessage(Case caseInfo, AccountInfo account, TradeInfo trade, OrderInfo order, decimal amount, IList<CaseUserRelation> users)
        {
            try
            {
                //如果订单类型为消费订单，而用途不是服务费，则不发消息
                if (order.OrderType == OrderType.Expend && order.OrderBizType != OrderBizType.EFQServiceCharge)
                    return;
                string paymenter = "买方";
                var ul = users;
                //房款买卖家都发消息
                if (order.OrderBizType != OrderBizType.HosingFund)
                {
                    paymenter = "您";
                    ul = users.Where(u => u.CaseUserType == (CaseUserType)((int)account.AccountType)).ToList();
                }
                var templateId = ServiceDepository.SettingService.GetSettingItemValueByPath("/System/Parter/WeiXin/Templates/Trade/Collection/Success");
                if (string.IsNullOrWhiteSpace(templateId))
                    throw new ArgumentException("templateId is null");
                var settings = ServiceDepository.SettingService.GetSettingItemsByPath("/System/Parter/WeiXin/Templates/Trade/Collection/Success");
                if (settings == null || settings.Count() == 0)
                    throw new ArgumentException("messageSettings is null");
                var ster = ServiceDepository.SettingService.GetSettingItemValueByPath("/System/Parter/WeiXin/Templates/Trade");
                foreach (var user in ul)
                {
                    if (user.CaseUserType == CaseUserType.AgentStaff || user.CaseUserType == CaseUserType.Handler)
                        continue;
                    var setting = settings.FirstOrDefault(s => string.Compare(s.Name, user.CaseUserType.ToString()) == 0);
                    if (setting == null)
                        continue;
                    var dic = SetCollectionDic(trade, order, caseInfo, setting.Value, ster, amount, paymenter);
                    ServiceDepository.ParterChannelService.SendTemplateMessage(user.UserSysNo, templateId, dic);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        private void SendPaymentSmsMessage(TradeInfo trade, OrderInfo order, AccountInfo payerAccount, Case caseInfo)
        {
            try
            {
                var pl = SetParameters(trade, order, payerAccount, caseInfo);
                if (pl == null || pl.Count == 0)
                    return;
                foreach (var p in pl)
                {
                    //人集合
                    foreach (var user in p.Item1)
                    {
                        var customer = ServiceDepository.CustomerDataAccess.Get(user.UserSysNo);
                        if (customer == null)
                            continue;
                        if (!ValidationMobile(customer))
                            continue;
                        //短信类别
                        foreach (var c in p.Item3)
                        {
                            var result = ServiceDepository.SmsService.Send(c, customer.Mobile, p.Item2);
                            if (!result.Success)
                                Log.Error(string.Format("短信发送失败,模板：{0},{1}", c, result.ResultMessage));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

        }

        private string GetContact()
        {
            var setting = ServiceDepository.SettingService.GetSettingItemByPath("/Biz/Static/Contact");
            string contact = "400-820-1100转2";
            if (setting != null && !string.IsNullOrWhiteSpace(setting.Value))
                contact = setting.Value;
            return contact;
        }

        public List<Tuple<List<CaseUserRelation>, Dictionary<string, object>, List<SmsCategory>>> SetParameters(TradeInfo trade, OrderInfo order, AccountInfo payerAccount, Case caseInfo)
        {

            var relations = ServiceDepository.CaseDataAccess.GetRelation(caseInfo.CaseId);
            if (relations == null || relations.Count == 0)
                return null;
            var colAccount = ServiceDepository.AccountDataAccess.Get(order.OppositeAccount.Value);
            if (colAccount == null)
                return null;
            var contact = GetContact();
            var parameters = new Dictionary<string, object> { { "phone", contact }, { "number", caseInfo.CaseId }, { "money", trade.Amount }, };
            var buyCategorys = new List<SmsCategory>();
            var sellerCategorys = new List<SmsCategory>();
            //付款
            var buyers = new List<CaseUserRelation>();
            //收款
            var sellers = new List<CaseUserRelation>();
            var pl = new List<Tuple<List<CaseUserRelation>, Dictionary<string, object>, List<SmsCategory>>>();
            switch (order.OrderBizType)
            {
                case OrderBizType.HosingFund:
                    {
                        //付款方
                        buyers = relations.Where(r => r.CaseUserType == (CaseUserType)(int)(payerAccount.AccountType)).ToList();
                        buyCategorys.Add(SmsCategory.ZHUANFU_FANGKUANG);
                        //收款方
                        sellers = relations.Where(r => r.CaseUserType == (CaseUserType)(int)(colAccount.AccountType)).ToList();
                        sellerCategorys.Add(SmsCategory.MFFK_NO_KCHU_FWF);
                        parameters["date"] = trade.PaymentDate.Value.ToString("MM月dd日");
                        parameters["weihao"] = trade.BankAccountNo.Substring(trade.BankAccountNo.Length - 4);
                        parameters["bank"] = trade.SubBankName;
                    }
                    break;
                case OrderBizType.Taxes:
                    {
                        buyers = relations.Where(r => r.CaseUserType == (CaseUserType)(int)(payerAccount.AccountType)).ToList();
                        buyCategorys.Add(SmsCategory.DAIFU_SHUIFEI);
                       
                    }
                    break;
                case OrderBizType.AgencyFee:
                    {
                        buyers = relations.Where(r => r.CaseUserType == (CaseUserType)(int)(payerAccount.AccountType)).ToList();
                        buyCategorys.Add(SmsCategory.ZHUANFU_YONGJ);
                    }
                    break;
                default:
                    return null;

            }
            //买方集合
            pl.Add(Tuple.Create(buyers, parameters, buyCategorys));
            //卖方集合
            pl.Add(Tuple.Create(sellers, parameters, sellerCategorys));
            return pl;
        }

        #endregion
    }
}
