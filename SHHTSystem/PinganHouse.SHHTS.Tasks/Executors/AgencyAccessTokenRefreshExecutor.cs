﻿using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Tasks.Executors
{
    class AgencyAccessTokenRefreshExecutor : ScheduleExecutorBase
    {
        public AgencyAccessTokenRefreshExecutor(string name, bool enabled, int interval, bool stopWhenExceptin)
            : base(name, enabled, interval, stopWhenExceptin) { }

        protected override void Execute()
        {
            var result = ServiceDepository.ParterChannelService.RefreshAccessToken(AgentType.Agency);
            if (result.Success)
                Log.Info("刷新企业号accessToken成功");
            else
                Log.Error(result.ResultMessage);
        }
    }
}
