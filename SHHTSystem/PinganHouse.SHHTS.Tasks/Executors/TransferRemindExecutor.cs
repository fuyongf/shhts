﻿using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Tasks.Executors
{
    internal class TransferRemindExecutor : ScheduleExecutorBase
    {
        public TransferRemindExecutor(string name, bool enabled, int interval, bool stopWhenExceptin)
            : base(name, enabled, interval, stopWhenExceptin) { }
        const string Remark="已通知";
        bool isSend = false;
        //线上仅开启一个任务
        protected override void Execute()
        {
            try
            {
                var hour=10;
                var time = ServiceDepository.SettingService.GetSettingItemValueByPath("/System/Parter/WeiXin/QY/Agency/Messages/TransferRemind/RemindTime");
                if (string.IsNullOrWhiteSpace(time) || !int.TryParse(time, out hour))
                    hour = 10;

                if (DateTime.Now.Hour != hour && isSend)
                    isSend = false;
                if (DateTime.Now.Hour != hour || isSend)
                    return;
                isSend = true;
                var caseEvents = ServiceDepository.CaseEventDataAccess.GetCaseEvents(CaseStatus.JY8);
                if (caseEvents == null || caseEvents.Count() == 0)
                    return;
                foreach (var caseEvent in caseEvents)
                {
                    if (caseEvent.OtherDatas == null || !caseEvent.OtherDatas.ContainsKey("booktime"))
                        continue;
                    DateTime bookTime;
                    if (!DateTime.TryParse(caseEvent.OtherDatas["booktime"].ToString(), out bookTime))
                        continue;
                    if (bookTime.Date != DateTime.Now.Date.AddDays(1) || caseEvent.CreateDate.Date == DateTime.Now.Date || (caseEvent.ModifyDate != null && caseEvent.ModifyDate.Value.Date== DateTime.Now.Date))
                        continue;
                    //历史已过户的事件也更新
                    var result = new OperationResult(0);
                    //预约过户时间前一天，且预约过户时间与创建时间间隔超过一天，因为预约过户系统已发消息，一天发一条消息
                    if (caseEvent.CreateDate.Date != DateTime.Now.Date)
                    {
                        result = ServiceDepository.MessageService.SendTransferRemindMessage(caseEvent, bookTime);
                    }
                    if (result.Success)
                        ServiceDepository.CaseEventDataAccess.UpdateRemark(caseEvent.SysNo, Remark);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }
    }
}
