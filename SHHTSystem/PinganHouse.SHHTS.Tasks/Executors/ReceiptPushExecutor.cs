﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.DataTransferObjects.External;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Tasks.Executors
{
    internal class ReceiptPushExecutor : ScheduleExecutorBase
    {
        public ReceiptPushExecutor(string name, bool enabled, int interval, bool stopWhenExceptin)
            : base(name, enabled, interval, stopWhenExceptin) { }

        protected override void Execute()
        {
            Log.Info("开始推送收据信息");
            var trades = ServiceDepository.TradeDataAccess.GetSendBillTrades();
            if (trades == null || trades.Count == 0)
                return;
            Log.Info(string.Format("获取到待推送收据交易信息:{0}条", trades.Count));
            foreach (var trade in trades)
            {
                var bills = ServiceDepository.ReceiptDataAccess.GetReceiptByOrderNo(trade.OrderNo);
                if (bills == null || bills.Count == 0)
                    continue;
                foreach (var bill in bills)
                {
                    if (bill.TradeDetails == null || !bill.TradeDetails.Contains(trade.TradeNo))
                        continue;
                    var result = ServiceDepository.ExternalService.SendReceipt(trade.TradeNo, bill.ReceiptNo);
                    if (result.Success)
                        Log.Info(string.Format("收据:{0},交易流水:{1}信息已推送", bill.ReceiptNo, trade.TradeNo));
                    else
                        Log.Error(string.Format("收据:{0},交易流水:{1}信息推送失败,{2}", bill.ReceiptNo, trade.TradeNo, result.ResultMessage));
                }
            }
            Log.Info("完成推送收据信息");
        }
    }
}
