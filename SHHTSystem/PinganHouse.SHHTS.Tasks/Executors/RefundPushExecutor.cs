﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;

namespace PinganHouse.SHHTS.Tasks.Executors
{
    internal class RefundPushExecutor : ScheduleExecutorBase
    {
        public RefundPushExecutor(string name, bool enabled, int interval, bool stopWhenExceptin)
            : base(name, enabled, interval, stopWhenExceptin) { }

        protected override void Execute()
        {
            //Log.Info("开始推送退款订单");
            //var orders = ServiceDepository.OrderDataAccess.GetOrdersByStatus(OrderType.Collection, OrderStatus.Refunding);
            //if (orders == null || orders.Count == 0)
            //    return;
            //Log.Info(string.Format("获取到退款订单:{0}条", orders.Count));
            //AuditStatus status = ServiceDepository.ConfigService.GetRefundSubmitCondition();
            //foreach (var order in orders)
            //{
            //    if (order.AuditStatus != status)
            //        continue;
            //    var trades = ServiceDepository.TradeDataAccess.GetByOrder(order.OrderNo);
            //    if (trades == null || trades.Count == 0)
            //        continue;
            //    trades = trades.Where(t => t.Amount < 0).ToList();
            //    if (trades.Count == 0)
            //        continue;
            //    try
            //    {
            //        foreach (var trade in trades)
            //        {
            //            try
            //            {
            //                ServiceDepository.TransactionService.Begin();
            //                #region ###################修改流水状态###################
            //                trade.Status = TradeStatus.Submitted;
            //                if (!ServiceDepository.TradeDataAccess.Update(trade))
            //                {
            //                    ServiceDepository.TransactionService.RollBack();
            //                    Log.Error(string.Format("退款交易:{0},状态修改失败", trade.TradeNo));
            //                    continue;
            //                }
            //                #endregion
            //                #region ###################提交退款请求###################
            //                var submit = ServiceDepository.ExternalService.SubmitPayment(trade.CaseId, trade.TradeNo, trade.TenementAddress, Math.Abs(trade.Amount), trade.BankCode, trade.BankAccountNo, trade.AccountName, OrderBizType.Refund.GetDescription());
            //                if (!submit.Success)
            //                {
            //                    ServiceDepository.TransactionService.RollBack();
            //                    Log.Error(string.Format("退款交易:{0},提交退款请求失败,{1},{2}", trade.TradeNo, submit.ResultNo, submit.ResultMessage));
            //                    continue;
            //                }
            //                #endregion

            //                Log.Info(string.Format("退款交易:{0}已推送", trade.TradeNo));
            //                ServiceDepository.TransactionService.Commit();
            //            }
            //            catch (Exception e)
            //            {
            //                ServiceDepository.TransactionService.RollBack();
            //                Log.Error(string.Format("退款交易:{0}推送失败,{1}", trade.TradeNo, e.ToString()));
            //                continue;
            //            }
            //        }
            //    }
            //    catch (Exception e)
            //    {
            //        ServiceDepository.TransactionService.RollBack();
            //        Log.Error(string.Format("退款订单:{0}推送失败,{1}", order.OrderNo, e.ToString()));
            //        continue;
            //    }
            //}
            //Log.Info("完成推送退款订单");
        }
    }
}
