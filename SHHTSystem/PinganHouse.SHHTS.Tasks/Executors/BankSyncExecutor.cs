﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.DataTransferObjects.External;

namespace PinganHouse.SHHTS.Tasks.Executors
{
    internal class BankSyncExecutor : ScheduleExecutorBase
    {
        public BankSyncExecutor(string name, bool enabled, int interval, bool stopWhenExceptin)
            : base(name, enabled, interval, stopWhenExceptin) { }

        protected override void Execute()
        {
            var banks = ServiceDepository.ExternalService.GetBanks();
            if (banks == null || banks.Count == 0)
                Log.Error("获取银行列表为空");
            var settingRoot = ServiceDepository.SettingService.GetSettingItemByPath("/Biz/Static/Banks");
            var settings = ServiceDepository.SettingService.GetSettingItems(settingRoot.Id);
            foreach (var bank in banks)
            {
                if (!settings.Any(s => s.Name == bank.sShortName))
                {
                    var item = ServiceDepository.SettingService.CreateSettingItem(bank.sShortName, settingRoot.Id, bank.ID, bank.sBankName, null);
                    Log.Info(string.Format("银行:{0},{1},{2}添加{3}", bank.ID, bank.sShortName, bank.sBankName, item == null ? "失败" : "成功"));
                }
            }
            Log.Info("银行数据同步完成");
        }
    }

}
