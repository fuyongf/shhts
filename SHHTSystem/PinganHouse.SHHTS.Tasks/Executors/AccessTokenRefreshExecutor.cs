﻿using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Utils.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Tasks.Executors
{
    internal class AccessTokenRefreshExecutor : ScheduleExecutorBase
    {
        public AccessTokenRefreshExecutor(string name, bool enabled, int interval, bool stopWhenExceptin)
            : base(name, enabled, interval, stopWhenExceptin) { }
        //上线后关闭线下任务器
        protected override void Execute()
        {
            var result = ServiceDepository.ParterChannelService.RefreshAccessToken();
            if (result.Success)
                Log.Info("刷新公众号accessToken成功");
            else
                Log.Error(result.ResultMessage);
        }
    }
}
