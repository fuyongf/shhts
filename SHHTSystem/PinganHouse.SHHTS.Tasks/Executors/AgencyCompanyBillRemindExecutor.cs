﻿using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.Tasks.Executors
{
    internal class AgencyCompanyBillRemindExecutor : ScheduleExecutorBase
    {
        public AgencyCompanyBillRemindExecutor(string name, bool enabled, int interval, bool stopWhenExceptin)
            : base(name, enabled, interval, stopWhenExceptin) { }
        bool isSend = false;
        //线上仅开启一个任务
        protected override void Execute()
        {
            try
            {            
                var day = 5;
                var hour = 10;
                var setting = ServiceDepository.SettingService.GetSettingItemByPath("/System/Parter/WeiXin/QY/AgencyACompany/Messages/Trade/MonthlyBiil/RemindTime");
                if (setting == null || !int.TryParse(setting.Value, out hour) || !int.TryParse(setting.Description, out day))
                {
                    hour = 10;
                    day = 5;
                }
                if (DateTime.Now.Hour != hour && isSend)
                    isSend = false;

                if (DateTime.Now.Hour != hour || DateTime.Now.Day != day || isSend)
                    return;
                Log.Info("开始推送公司月度账单");
                isSend = true;
                var totalCount =0;
                var pageCount=1000;
                var pageIndex=0;
                do
                {
                    pageIndex++;
                    var companys = ServiceDepository.AgentCompanyDataAccess.GetPaginatedList(null, AgentJointStatus.Jointing, pageIndex, pageCount, out totalCount);
                    ServiceDepository.MessageService.SendMonthBillMessage(companys);
                    
                } while (totalCount > pageIndex * pageCount);
                           
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }
    }
}
