﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Utils.Log;
using PinganHouse.SHHTS.DataTransferObjects.External;

namespace PinganHouse.SHHTS.Tasks.Executors
{
    internal class IdentifyBankSyncExecutor : ScheduleExecutorBase
    {
        public IdentifyBankSyncExecutor(string name, bool enabled, int interval, bool stopWhenExceptin)
            : base(name, enabled, interval, stopWhenExceptin) { }

        protected override void Execute()
        {
            var banks = ServiceDepository.ExternalService.GetIdentifyBanks();
            if (banks == null || banks.Count == 0)
                Log.Error("获取鉴权银行列表为空");
            var settingRoot = ServiceDepository.SettingService.GetSettingItemByPath("/Biz/Static/IdentifyBanks");
            var settings = ServiceDepository.SettingService.GetSettingItems(settingRoot.Id);
            foreach (var bank in banks)
            {
                if (!settings.Any(s => s.Name == bank.sShortName))
                {
                    var item = ServiceDepository.SettingService.CreateSettingItem(bank.sShortName, settingRoot.Id, bank.iStatus.ToString(), bank.sBankName, null);
                    if (item != null)
                    {
                        try
                        {
                            ServiceDepository.SettingService.CreateSettingItem("BankId", item.Id, bank.iBankID.ToString(), "", null);
                            ServiceDepository.SettingService.CreateSettingItem("ImageUrl", item.Id, bank.sBankImgUrl.ToString(), "", null);
                        }
                        catch (Exception e)
                        {
                            Log.Error(e);
                            continue;
                        }
                    }
                    Log.Info(string.Format("鉴权银行:{0},{1},{2}添加{3}", bank.iBankID, bank.sShortName, bank.sBankName, item == null ? "失败" : "成功"));
                }
            }
            Log.Info("鉴权银行数据同步完成");
        }
    }
}
