﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Core;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;
using PinganHouse.SHHTS.Utils.Log;

namespace PinganHouse.SHHTS.Tasks.Executors
{
    internal class PaymentPushExecutor : ScheduleExecutorBase
    {
        public PaymentPushExecutor(string name, bool enabled, int interval, bool stopWhenExceptin)
            : base(name, enabled, interval, stopWhenExceptin) { }

        protected override void Execute()
        {
            Log.Info("开始推送付款订单");
            var orders = ServiceDepository.OrderDataAccess.GetOrdersByStatus(OrderType.Payment, OrderStatus.Initial);
            if (orders == null || orders.Count == 0)
                return;
            Log.Info(string.Format("获取到付款订单:{0}条", orders.Count));
            AuditStatus status = ServiceDepository.ConfigService.GetPaymentSubmitCondition();
            foreach (var order in orders)
            {
                if (order.AuditStatus != status)
                    continue;
                var trades = ServiceDepository.TradeDataAccess.GetByOrder(order.OrderNo);
                if (trades == null || trades.Count == 0)
                    continue;
                try
                {
                    ServiceDepository.TransactionService.Begin();

                    foreach (var trade in trades)
                    {
                        try
                        {
                            #region ###################修改流水状态###################
                            trade.Status = TradeStatus.Submitted;
                            if (!ServiceDepository.TradeDataAccess.Update(trade))
                            {
                                ServiceDepository.TransactionService.RollBack();
                                Log.Error(string.Format("付款交易:{0},{1},流水状态修改失败", order.OrderNo, trade.TradeNo));
                                continue;
                            }
                            #endregion

                            #region ###################提交付款请求###################
                            var submit = ServiceDepository.ExternalService.SubmitPayment(trade.CaseId, trade.TradeNo, trade.TenementAddress, Math.Abs(trade.Amount), trade.BankCode, trade.BankAccountNo, trade.AccountName, order.OrderBizType.GetDescription());
                            if (!submit.Success)
                            {
                                ServiceDepository.TransactionService.RollBack();
                                Log.Error(string.Format("付款交易:{0},提交付款请求失败,{1},{2}", trade.TradeNo, submit.ResultNo, submit.ResultMessage));
                                continue;
                            }
                            #endregion

                            Log.Info(string.Format("付款交易:{0}已推送", trade.TradeNo));
                        }
                        catch (Exception e)
                        {
                            ServiceDepository.TransactionService.RollBack();
                            Log.Error(string.Format("付款交易:{0}推送失败,{1}", trade.TradeNo, e.ToString()));
                            continue;
                        }

                        #region ###################修改订单状态###################
                        order.OrderStatus = OrderStatus.Submitted;
                        if (!ServiceDepository.OrderDataAccess.Update(order))
                        {
                            ServiceDepository.TransactionService.RollBack();
                            Log.Error(string.Format("付款交易:{0},{1},订单状态修改失败", order.OrderNo, trade.TradeNo));
                            continue;
                        }
                        #endregion

                        ServiceDepository.TransactionService.Commit();
                    }
                }
                catch (Exception e)
                {
                    ServiceDepository.TransactionService.RollBack();
                    Log.Error(string.Format("付款订单:{0}推送失败,{1}", order.OrderNo, e.ToString()));
                    continue;
                }
            }
            Log.Info("完成推送付款订单");
        }
    }
}
