﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Threading;

using PinganHouse.SHHTS.Utils.Log;

namespace PinganHouse.SHHTS.Tasks
{
    public class ScheduleManager
    {
        private static Dictionary<string, ScheduleExecutorBase> Executors = new Dictionary<string, ScheduleExecutorBase>();

        private static bool m_Initializing = false;

        /// <summary>
        /// 是否已初始化
        /// </summary>
        public static bool Initialized { get; set; }

        /// <summary>
        /// 初始化相关信息
        /// </summary>
        /// <returns></returns>
        public static bool Init() 
        {
            if (!m_Initializing)
            {
                m_Initializing = true;
                try
                {
                    ScheduleSection config = ConfigurationManager.GetSection("schedules") as ScheduleSection;
                    if (config == null)
                        throw new ConfigurationErrorsException("section 'schedules' not found.");

                    if (config.Schedules != null && config.Schedules.Count > 0) 
                    {
                        foreach (ScheduleElement schedule in config.Schedules) 
                        {
                            if (!schedule.Enabled)
                                continue;

                            try
                            {
                                ScheduleExecutorBase executor = Activator.CreateInstance(Type.GetType(schedule.Executor), new object[] { schedule.Name, schedule.Enabled, schedule.Interval, schedule.StopWhenExceptin }) as ScheduleExecutorBase;
                                if (executor == null)
                                    throw new TypeLoadException(string.Format("类型{0}加载失败，或者它不是ScheduleExecutorBase的子类型."));

                                Executors.Add(schedule.Name, executor);
                            }
                            catch(Exception ex)
                            {
                                Log.Error(string.Format("{0} 任务初始化失败，{1},{2}",schedule.Name,ex.Message,ex.StackTrace));
                            }

                        }
                    }

                    Initialized = true;
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    Initialized = false;
                    throw;
                }

                m_Initializing = false;
            }
            return Initialized;
        }

        public static void StartAll()
        {
            int tryTimes = 0;
            while (!Initialized && tryTimes < 10)
            {
                Init();
                tryTimes++;
                Thread.Sleep(100);
            }

            if (!Initialized)
                throw new Exception("Schedule尚未初始化.");

            foreach (var executor in Executors) 
            {
                if (executor.Value.IsRuning)
                    continue;
                executor.Value.Start();
                Log.Info(string.Format("任务：{0}已启动。", executor.Key));
            }
        }

        public static void Start(string name) 
        {
            int tryTimes = 0;
            while (!Initialized && tryTimes < 10)
            {
                Init();
                tryTimes++;
                Thread.Sleep(100);
            }

            if (!Initialized)
                throw new Exception("Schedule尚未初始化.");

            if (!Executors.ContainsKey(name))
                throw new KeyNotFoundException(string.Format("指定任务:{0}不存在", name));

            if (Executors[name].IsRuning)
                return;
                    
            Executors[name].Start();

            Log.Info(string.Format("任务：{0}已启动。", name));
        }

        public static void StopAll() 
        {
            if (!Initialized)
                return;

            foreach (var executor in Executors)
            {
                if (!executor.Value.IsRuning)
                    continue;
                executor.Value.Stop();
                Log.Info(string.Format("任务：{0}已停止。", executor.Key));
            }
        }

        public static void Stop(string name) 
        {
            if (!Initialized)
                return;
            if (!Executors.ContainsKey(name))
                throw new KeyNotFoundException(string.Format("指定任务:{0}不存在", name));

            if (!Executors[name].IsRuning)
                return;

            Executors[name].Stop();

            Log.Info(string.Format("任务：{0}已停止。", name));
        }
    }
}
