﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using PinganHouse.SHHTS.Utils.Log;

namespace PinganHouse.SHHTS.Tasks
{
    internal abstract class ScheduleExecutorBase
    {
        private Thread m_Thread = null;

        private string m_Name;
        private bool m_Enabled;
        private int m_Interval = 1000;
        private bool m_StopWhenExceptin;

        public ScheduleExecutorBase(string name, bool enabled, int interval, bool stopWhenExceptin)
        {
            m_Name = name;
            m_Interval = interval;
            m_Enabled = enabled;
            m_StopWhenExceptin = stopWhenExceptin;

            m_Thread = new Thread(new ThreadStart(DoWork));
            m_Thread.IsBackground = true;
        }

        public string Name { get { return m_Name; } }

        public bool Enabled { get { return m_Enabled; } }

        public int Interval { get { return m_Interval; } }

        public bool StopWhenExceptin { get { return m_StopWhenExceptin; } }

        public bool IsRuning { get; set; }

        private void DoWork() 
        {
            while (true)
            {
                if (m_Enabled)
                {
                    try
                    {
                        Execute();
                    }
                    catch (Exception ex) 
                    {
                        if (m_StopWhenExceptin)
                        {
                            Stop();
                            throw;
                        }
                        Log.Error(ex);
                    }
                }
                m_Thread.Join(m_Interval);
            }
        }

        protected abstract void Execute();

        public void Start() 
        {
            if (m_Thread != null)
                m_Thread.Start();
            Log.Info(string.Format("{0},schedule task runing.", this.Name));
            IsRuning = true;
        }

        public void Stop() 
        {
            try
            {
                m_Thread.Abort();
            }
            catch (ThreadAbortException) 
            {

            }
            catch
            {
                Log.Info(string.Format("{0},schedule task stoped.", this.Name));
            }
            finally
            {
                IsRuning = false;
            }
        }
    }
}
