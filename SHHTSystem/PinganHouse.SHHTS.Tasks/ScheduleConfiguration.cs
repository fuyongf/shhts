﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Tasks
{
    public class ScheduleSection : ConfigurationSection
    {
        [ConfigurationProperty("", IsDefaultCollection = true)]
        public ScheduleCollection Schedules
        {
            get
            {
                return (ScheduleCollection)base[""];
            }
        }

        public static ScheduleSection Instance 
        {
            get
            {
                ScheduleSection section = ConfigurationManager.GetSection("schedules") as ScheduleSection;
                if (section == null)
                {
                    throw new Exception("Section schedules  is not found");
                }
                return section;
            }
        }
    }

    public class ScheduleCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ScheduleElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ScheduleElement)element).Name;
        }

        protected override string ElementName
        {
            get { return "schedule"; }
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMap;
            }
        }

        public ScheduleElement this[int index]
        {
            get
            {
                return (ScheduleElement)BaseGet(index);
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }
    }

    public class ScheduleElement : ConfigurationElement
    {
        private readonly IDictionary<string, object> _savedSettings = new Dictionary<string, object>();

        /// <summary>
        /// 任务名称
        /// </summary>
        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        [StringValidator(InvalidCharacters =
                        " ~!@#$%^&*()[]{}/;'\"|\\", MaxLength = 60)]
        public string Name
        {
            get { return (string)Get("name"); }
            set { Set("name", value); }
        }

        /// <summary>
        /// 是否启用
        /// </summary>
        [ConfigurationProperty("enabled", IsRequired = true)]
        public bool Enabled
        {
            get { return (bool)Get("enabled"); }
            set { Set("enabled", value); }
        }

        /// <summary>
        /// 执行周期
        /// </summary>
        [ConfigurationProperty("interval", IsRequired = true)]
        public int Interval
        {
            get { return (int)Get("interval"); }
            set { Set("interval", value); }
        }

        /// <summary>
        /// 执行周期
        /// </summary>
        [ConfigurationProperty("executor", IsRequired = true)]
        public string Executor
        {
            get { return (string)Get("executor"); }
            set { Set("executor", value); }
        }

        /// <summary>
        /// 发生异常时终止任务
        /// </summary>
        [ConfigurationProperty("stopWhenExceptin", IsRequired = false)]
        public bool StopWhenExceptin
        {
            get { return (bool)Get("stopWhenExceptin"); }
            set { Set("stopWhenExceptin", value); }
        }

        private void Set(string key, object value)
        {
            _savedSettings[key] = value;
        }
        private object Get(string key)
        {
            if (_savedSettings.ContainsKey(key))
            {
                return _savedSettings[key];
            }
            else
            {
                return this[key];
            }
        }
    }
}
