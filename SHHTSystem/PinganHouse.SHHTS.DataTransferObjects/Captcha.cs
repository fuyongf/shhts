﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace PinganHouse.SHHTS.DataTransferObjects
{
    /// <summary>
    /// 验证码类。
    /// </summary>
    [Serializable]
    public class Captcha
    {
        private readonly string code;
        private readonly string token;

        public Captcha(string token, string code)
        {
            this.code = code;
            this.token = token;
        }

        public string Value
        {
            get { return code; }
        }

        public string Key { get { return token; } }


        /// <summary>
        /// 生成验证码图片。
        /// </summary>
        /// <returns>所生成的图片。</returns>
        public Bitmap ToBitmap(int width = 80, int height = 40)
        {
            var random = new Random();
            var codeChars = code.ToCharArray();
            /* 待处理的字符图片 */
            var images = new Bitmap[codeChars.Length];
            /* 随机选择一种字体 */
            var allFontFamily = new[] { "宋体" };
            var fonts = new Font[codeChars.Length];
            for (int i = 0; i < fonts.Length; i++)
            {
                fonts[i] = new Font(allFontFamily[random.Next(0, allFontFamily.Length)], random.Next(12, 16), FontStyle.Regular);
            }
            /* 定义颜色、画笔等 */
            var fgColors = new[] { Color.Black, Color.Blue, Color.Brown };//候选前景色
            var fgColor = fgColors[random.Next(0, fgColors.Length)];//随机取前景色
            var bgColor = Color.White;//背景色
            var brush = new SolidBrush(fgColor);

            /****逐个画字符图片*****/
            for (int i = 0; i < codeChars.Length; i++)
            {
                images[i] = new Bitmap(36, 36);
                Graphics gImg = Graphics.FromImage(images[i]);
                gImg.RotateTransform(random.Next(-14, 14));
                gImg.ScaleTransform(2f, 1.5f);
                int p_x = 0;
                int p_y = 0;
                gImg.SmoothingMode = SmoothingMode.HighSpeed;
                gImg.DrawString(codeChars[i].ToString(), fonts[i], brush, p_x, p_y);
                fonts[i].Dispose();
                gImg.Dispose();
            }

            /****画干扰曲线并组合图片****/
            Bitmap finalImage = new Bitmap(width, height);
            Graphics g = Graphics.FromImage(finalImage);
            //--开始画背景
            Brush brushBg = new SolidBrush(bgColor);
            g.FillRectangle(brushBg, 0, 0, width, height);
            brushBg.Dispose();

            //--开始画干扰曲线
            var linePen = new Pen(fgColor, 1);
            for (int i = 0; i < 2; i++)
            {
                for (var j = 0; j < 8; j = j + 8)
                {
                    var x = 0; // (image.Width - r) / 2;
                    var y = random.Next(2, finalImage.Height - 2);
                    var x1 = x + random.Next(6, 10);
                    var y1 = random.Next(0, finalImage.Height + 8);
                    var x2 = x1 + random.Next(10, 18);
                    var y2 = random.Next(0, finalImage.Height);
                    g.DrawBezier(linePen, x, y, x1, y1, x2, y2, finalImage.Width, random.Next(0, finalImage.Height));
                }
            }
            linePen.Dispose();

            //--组合图片
            AppendImages(g, images);

            brush.Dispose();
            g.Dispose();
            return finalImage;
        }

        /// <summary>
        /// 将验证码图片写入流。
        /// </summary>
        /// <returns>验证码图片流。</returns>
        public Stream WriteToStream()
        {
            var image = ToBitmap();
            var stream = new MemoryStream();
            image.Save(stream, ImageFormat.Gif);
            stream.Position = 0;
            return stream;
        }

        // 组合图片
        private void AppendImages(Graphics g, Bitmap[] images)
        {
            if (images == null)
                return;

            // 当前正在处理的图片
            int index = 0;
            // 绘制下一个图片时的位置
            int drawX = 0;
            int distance = -1;

            while (index < images.Length)
            {
                if (index > 0)
                    distance = CalculateDistanceBetweenChar(images[index - 1], images[index]);

                g.DrawImage(images[index], drawX - distance - 1, 0, images[index].Width, images[0].Height);
                drawX += images[index].Width - distance - 1;
                index++;
            }
        }

        // 计算图片碰撞间距
        private int CalculateDistanceBetweenChar(Bitmap leftImage, Bitmap rightImage)
        {
            // 左图每行右侧空白字符个数列表
            int[][] left = CalculateBlankNum(leftImage);
            // 右图每行左侧空白字符个数列表
            int[][] right = CalculateBlankNum(rightImage);
            int tempDistance = int.MaxValue;
            for (int i = 0; i < left.Length; i++)
            {
                var temp = 0;
                if (right[i][0] == 0)
                {
                    temp = left[i][1] + leftImage.Width;
                }
                else
                {
                    temp = left[i][1] + right[i][0];
                }
                if (temp < tempDistance)
                    tempDistance = temp;
            }

            return tempDistance;
        }

        // 计算每个图片每行的左右空白像素个数. int[row][0]存储左边空白像素个数 int[row][1]存储右边空白像素个数
        private int[][] CalculateBlankNum(Bitmap image)
        {

            int width = image.Width;
            int height = image.Height;

            var result = new int[height][];
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = new[] { 0, width };
            }

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    var c = image.GetPixel(j, i);
                    if (c.ToArgb() != Color.Empty.ToArgb())
                    {
                        if (result[i][0] == 0)
                        {
                            result[i][0] = j;
                        }
                        result[i][1] = width - j - 1;
                    }
                }
            }

            return result;
        }
    }
}
