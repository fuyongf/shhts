﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace PinganHouse.SHHTS.DataTransferObjects
{
    [Serializable]
    public class OperationResult
    {
        public OperationResult(int resultNo, string description = "")
        {
            ResultNo = resultNo;
            ResultMessage = description;
            this.OtherData= new Dictionary<string, object>();
        }

        public int ResultNo { get; set; }
        public string ResultMessage { get; set; }
        public IDictionary<string,object> OtherData { get; set; }

        public bool Success
        {
            get{return ResultNo == 0;}
        }
    }
}
