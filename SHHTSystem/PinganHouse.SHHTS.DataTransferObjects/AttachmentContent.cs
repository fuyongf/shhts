﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace PinganHouse.SHHTS.DataTransferObjects
{
    [MessageContract]
    public class AttachmentContent : AttachmentDto
    {
        /// <summary>
        /// 附件内容流
        /// </summary>
        [MessageBodyMember(Order = 1)]
        public Stream AttachmentContentData { get; set; }
    }
}
