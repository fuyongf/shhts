﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.DataTransferObjects
{
    public class PropertyCertificateDto
    {
        /// <summary>
        /// 产证编号
        /// </summary>
        public string TenementContract { get; set; }

        /// <summary>
        /// 物业地址
        /// </summary>
        public string TenementAddress { get; set; }

        public string Proprietor
        {
            get
            {
                return string.Join(",", proprietors);
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    foreach (var p in value.Split(','))
                    {
                        long sysno;
                        if (long.TryParse(p, out sysno))
                        {
                            if (proprietors.Contains(sysno))
                                proprietors.Add(sysno);
                        }
                    }
                }
            }
        }

        private List<long> proprietors = new List<long>();
        /// <summary>
        /// 业主
        /// </summary>
        public List<long> Proprietors
        {
            get { return proprietors; }
            set
            {
                if (value == null)
                    value = new List<long>();
                proprietors = value;
            }
        }

        public string ProprietorName
        {
            get { return string.Join(",", proprietorNameDic.Values); }
        }

        Dictionary<long, string> proprietorNameDic = new Dictionary<long, string>();
        /// <summary>
        /// 买家姓名集合
        /// </summary>
        public Dictionary<long, string> ProprietorNameDic
        {
            get { return this.proprietorNameDic; }
            set 
            { 
                if(value==null)
                    value = new Dictionary<long,string>();
                this.proprietorNameDic = value; 
            }
        }

        Dictionary<string, object> otherData = new Dictionary<string, object>();
        public Dictionary<string, object> OtherData 
        {
            get { return this.otherData; }
            set 
            { 
                this.otherData = value; 
            }
        }
    }
}
