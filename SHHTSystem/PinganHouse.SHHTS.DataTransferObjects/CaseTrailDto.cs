﻿using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.DataTransferObjects
{
    public class CaseTrailDto:BaseDataTransferObject
    {
        /// <summary>
        /// 案件id
        /// </summary>
        public string CaseId { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 进度
        /// </summary>
        public ProccessType? ProccessType { get; set; }
    }

    public class CaseTrailPaged : CaseTrailDto
    {
        /// <summary>
        /// 中介公司
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// 经纪人
        /// </summary>
        public string AgencyName { get; set; }

        /// <summary>
        /// 最后跟进人
        /// </summary>
        public string LastTrailer { get; set; }


        private Dictionary<long, string> buyerNameDic = new Dictionary<long, string>();
        /// <summary>
        /// 买家姓名集合
        /// </summary>
        public Dictionary<long, string> BuyerNameDic
        {
            get { return this.buyerNameDic; }
            set { this.buyerNameDic = value; }
        }

        /// <summary>
        /// 买家姓名
        /// </summary>
        public string BuyerName {
            get 
            {
                if (buyerNameDic == null || buyerNameDic.Count==0)
                    return string.Empty;
                return string.Join(",", buyerNameDic.Values);
            }
        }

        /// <summary>
        /// 物业地址
        /// </summary>
        public string TenementAddress { get; set; }

        /// <summary>
        /// 操作完成时间
        /// </summary>
        public DateTime? OperationDate { get; set; }

        private Dictionary<CaseStatus, CaseStatus> lastStatusDic = new Dictionary<CaseStatus, CaseStatus>();
        /// <summary>
        /// 状态
        /// key 为状态类别，  value为该类别中最新状态
        /// </summary>
        public Dictionary<CaseStatus, CaseStatus> LastStatusDic
        {
            get { return this.lastStatusDic; }
            set { this.lastStatusDic = value; }
        }
    }
}
