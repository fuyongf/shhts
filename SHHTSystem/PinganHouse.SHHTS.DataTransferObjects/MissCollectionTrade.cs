﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.DataTransferObjects
{
    public class MissCollectionTrade : BaseDataTransferObject
    {
        /// <summary>
        /// 银行交易流水号
        /// </summary>
        public string SerialNo { get; set; }

        /// <summary>
        /// 交易金额
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// 付款时间
        /// </summary>
        public DateTime? PaymentDate { get; set; }

        /// <summary>
        /// 付款人
        /// </summary>
        public string PayerName { get; set; }

        /// <summary>
        /// 付款银行代码
        /// </summary>
        public string BankCode { get; set; }

        /// <summary>
        /// 付款银行名称
        /// </summary>
        public string BankName { get; set; }

        /// <summary>
        /// 支付帐号
        /// </summary>
        public string PaymentAccount { get; set; }

        /// <summary>
        /// 支付渠道
        /// </summary>
        public PaymentChannel Channel { get; set; }

        /// <summary>
        /// 已确认
        /// </summary>
        public bool Confirmed { get; set; }

        /// <summary>
        /// 业务交易流水号
        /// </summary>
        public string TradeNo { get; set; }

        /// <summary>
        /// 审核人
        /// </summary>
        public long? AuditorSysNo { get; set; }
    }
}
