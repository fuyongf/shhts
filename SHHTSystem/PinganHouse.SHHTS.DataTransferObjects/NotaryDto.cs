﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.DataTransferObjects
{
    /// <summary>
    /// 公证员
    /// </summary>
    public class NotaryDto : BaseDataTransferObject
    {
        /// <summary>
        /// 获取或设置姓名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 获取或设置手机号码
        /// </summary>
        public string Mobile { get; set; }
        /// <summary>
        /// 获取或设置公正机构名称
        /// </summary>
        public string OfficeName { get; set; }
    }
}
