﻿using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.DataTransferObjects.Filters
{
    /// <summary>
    /// 案件过滤条件
    /// </summary>
    public sealed class CaseFilter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CaseFilter"/> class.
        /// </summary>
        public CaseFilter()
        {
            //默认未删除且未完成的
            IsDeleted = false;
            Finished = false;

            CaseIds = new List<string>();
            ReceptionCenters = new List<ReceptionCenter>();
            SourceTypes = new List<SourceType>();
            //默认获取受理中心和e房钱
            SourceTypes.Add(SourceType.ReceptionCenter);
            SourceTypes.Add(SourceType.EHouseMoney);

            Statuses = new List<CaseStatus>();
            AgencyStaffSysNos = new List<long>();
            AgentCompanySysNos = new List<long>();
            ProccessTypes = new List<ProccessType>();
        }

        /// <summary>
        /// 获取或设置指定的案件编号列表.
        /// </summary>
        public IList<string> CaseIds { get; set; }
        /// <summary>
        /// 获取或设置需查询受理中心列表.
        /// </summary>
        public IList<ReceptionCenter> ReceptionCenters { get; set; }
        /// <summary>
        /// 获取或设置需检索的状态列表.
        /// </summary>
        public IList<CaseStatus> Statuses { get; set; }
        /// <summary>
        /// 获取或设置是否检索已完成的状态.
        /// </summary>
        public bool? Finished { get; set; }
        /// <summary>
        /// 获取或设置案件来源类型列表.
        /// </summary>
        public IList<SourceType> SourceTypes { get; set; }
        /// <summary>
        /// 获取或设置是否已删除
        /// </summary>
        public bool? IsDeleted { get; set; }
        /// <summary>
        /// 获取或设置开始日期.
        /// </summary>
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// 获取或设置结束日期.
        /// </summary>
        public DateTime? EndDate { get; set; }
        /// <summary>
        /// 获取或设置物业地址检索关键字
        /// </summary>
        public string TenementAddress { get; set; }
        /// <summary>
        /// 获取或设置产权号检索关键字.
        /// </summary>
        public string TenementContract { get; set; }
        /// <summary>
        /// 获取或设置创建（主办）人名称检索关键字.
        /// </summary>
        public string CreatorName { get; set; }
        /// <summary>
        /// 获取或设置买方名称检索关键字.
        /// </summary>
        public string BuyerName { get; set; }
        /// <summary>
        /// 获取或设置经纪人姓名检索关键字.
        /// </summary>
        public string AgencyStaffName { get; set; }
        /// <summary>
        /// 获取或设置经纪人手机检索关键字.
        /// </summary>
        public string AgencyStaffPhone { get; set; }
        /// <summary>
        /// 获取或设置经纪人SysNo列表.
        /// </summary>
        public IList<long> AgencyStaffSysNos { get; set; }
        /// <summary>
        /// 获取或设置经纪公司SysNo列表.
        /// </summary>
        public IList<long> AgentCompanySysNos { get; set; }

        /// <summary>
        /// 获取或设置要检索的案件进度类型列表.
        /// </summary>
        public IList<ProccessType> ProccessTypes { get; set; }

        /// <summary>
        /// 获取或设置跟进人名称检索关键字.
        /// </summary>
        public string TrailerName { get; set; }
    }
}
