﻿using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace PinganHouse.SHHTS.DataTransferObjects
{
    [MessageContract]
    public class FileUploadMessage
    {
        [MessageHeader]
        public string FileId { get; set; }

        [MessageHeader(MustUnderstand = true)]
        public string FileName { get; set; }

        [MessageHeader(MustUnderstand = true)]
        public long FileSize { get; set; }

        [MessageHeader(MustUnderstand = true)]
        public long PackageSize { get; set; }

        [MessageBodyMember(Order = 1)]
        public Stream FileData { get; set; }
    }

    [MessageContract]
    public class UploadResultMessage
    {
        [MessageBodyMember]
        public bool Status { get; set; }

        [MessageBodyMember]
        public string FileId { get; set; }

        [MessageBodyMember]
        public string ResultMessage { get; set; }
    }

    [MessageContract]
    public class FileQueryMessage
    {
        [MessageBodyMember]
        public string FileId { get; set; }

        [MessageBodyMember]
        public string FileName { get; set; }
    }

    [MessageContract]
    public class FileDownloadMessage
    {
        [MessageHeader]
        public string FileId { get; set; }

        [MessageHeader]
        public string FileName { get; set; }

        [MessageHeader]
        public long FileSize { get; set; }

        [MessageHeader]
        public DateTime UploadDate { get; set; }

        [MessageBodyMember]
        public Stream FileData { get; set; }
    }

    #region DataContract Object for Win8 App
    public class UploadResult
    {
        public bool Status { get; set; }

        public string FileId { get; set; }

        public string ResultMessage { get; set; }
    }
    #endregion
}
