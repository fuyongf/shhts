﻿using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.DataTransferObjects
{
    public class Receipt : BaseDataTransferObject
    {
        /// <summary>
        /// 收据据编号
        /// </summary>
        public string ReceiptNo { get; set; }

        /// <summary>
        /// 收款单位
        /// </summary>
        public string Payee { get; set; }

        /// <summary>
        /// 付款单位
        /// </summary>
        public string Payer { get; set; }

        /// <summary>
        /// 出具日期
        /// </summary>
        public DateTime ReceiptDate { get; set; }

        /// <summary>
        /// 收据金额
        /// </summary>
        public decimal Money { get; set; }

        #region 待废弃

        public string OrderNo { get; set; }


        private IList<string> tradeDetails = new List<string>();
        /// <summary>
        /// 对应交易流水
        /// </summary>
        public IList<string> TradeDetails
        {
            get { return this.tradeDetails; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("TradeDetails", "交易流水号不能为空");
                this.tradeDetails = value;
            }
        }
        
        #endregion

        /// <summary>
        /// 收据状态
        /// </summary>
        public ReceiptStatus Status { get; set; }

        /// <summary>
        /// 打印次数
        /// </summary>
        public int PrintCount { get; set; }
        public int RePrintCount { get { return PrintCount - 1; } }

        public string CreateName { get; set; }

        public string ModifyName { get; set; }

        /// <summary>
        /// 案件编号
        /// </summary>
        public string CaseId { get; set; }

        /// <summary>
        /// 物业地址
        /// </summary>
        public string TenementAddress { get; set; }
    }

    public class ReceiptView : Receipt
    {
        /// <summary>
        /// 票据流水号
        /// </summary>
        public string ReceiptSerialNo { get; set; }

        /// <summary>
        /// 验真码
        /// </summary>
        public string SecurityCode { get; set; }

        #region 待废弃

        /// <summary>
        /// 支付方式
        /// </summary>
        public PaymentChannel PaymentChannel { get; set; }

        /// <summary>
        /// 付款人手机号码
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 详细地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 邮编
        /// </summary>
        public string PostCode { get; set; }

        #endregion

        /// <summary>
        /// 业务类型
        /// </summary>
        public OrderBizType OrderBizType { get; set; }

        /// <summary>
        /// 账户类型
        /// </summary>
        public AccountType AccountType { get; set; }

        public string TradeProperty
        {
            get
            {
                return "买卖";
            }
        }
    }

    //public class ReceiptPaginated
    //{
    //    /// <summary>
    //    /// 订单编号
    //    /// </summary>
    //    public string OrderNo { get; set; }

    //    /// <summary>
    //    /// 案件编号
    //    /// </summary>
    //    public string CaseId { get; set; }

    //    /// <summary>
    //    /// 订单总金额
    //    /// </summary>
    //    public decimal TotalMoney { get; set; }

    //    /// <summary>
    //    /// 到账金额
    //    /// </summary>
    //    public decimal CollectedBalance { get; set; }

    //    /// <summary>
    //    /// 开票金额
    //    /// </summary>
    //    public decimal ReceiptMoney { get; set; }
    //    public decimal UnReceiptMoney
    //    {
    //        get
    //        {
    //            return TotalMoney - ReceiptCount;
    //        }
    //    }

    //    /// <summary>
    //    /// 开票次数
    //    /// </summary>
    //    public int ReceiptCount { get; set; }

    //    /// <summary>
    //    /// 补打次数
    //    /// </summary>
    //    public int RePrintCount { get; set; }

    //    /// <summary>
    //    /// 最后打印时间
    //    /// </summary>
    //    public DateTime LastPrintTime { get; set; }

    //    public bool OpenReceipt
    //    {
    //        get
    //        {
    //            return UnReceiptMoney > 0;
    //        }
    //    }
    //    public bool ReOpenReceipt
    //    {
    //        get
    //        {
    //            return ReceiptMoney > 0;
    //        }
    //    }
    //}
}
