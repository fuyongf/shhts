﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.DataTransferObjects
{
    public class Account : BaseDataTransferObject
    {
        /// <summary>
        /// 主体编号
        /// </summary>
        public string SubjectId { get; set; }

        /// <summary>
        /// 账户类型
        /// </summary>
        public AccountType AccountType { get; set; }

        /// <summary>
        /// 余额
        /// </summary>
        public decimal Balance { get; set; }

        /// <summary>
        /// 待入账金额
        /// </summary>
        public decimal PreEntryMoney { get; set; }

        /// <summary>
        /// 锁定金额
        /// </summary>
        public decimal LockedMoney { get; set; }

        /// <summary>
        /// 冻结金额
        /// </summary>
        public decimal FrozenMoney { get; set; }

        /// <summary>
        /// 是否已锁定
        /// </summary>
        public bool IsLocked { get; set; }
    }

    public class AccountLedger
    {
        public string CaseId { get; set; }

        public string TenementContract { get; set; }

        public string TenementAddress { get; set; }

        public decimal BuyerBalance { get; set; }

        public decimal SellerBalance { get; set; }
    }

    public class AccountBill 
    {
        /// <summary>
        /// 账户编号
        /// </summary>
        public long AccountSysNo { get; set; }

        /// <summary>
        /// 开始日期
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// 结束日期
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// 支出总额
        /// </summary>
        public decimal PaymentAmount { get; set; }

        /// <summary>
        /// 收入总额
        /// </summary>
        public decimal CollectionAmount { get; set; }

        /// <summary>
        /// 分类总额
        /// </summary>
        public IDictionary<OrderBizType, decimal> CategoryAmount { get; set; }
        
        /// <summary>
        /// 订单明细
        /// </summary>
        public IList<OrderBilledInfo> OrderDetails { get; set; }
    }
}
