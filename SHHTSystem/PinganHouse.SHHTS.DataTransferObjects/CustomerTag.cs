﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.DataTransferObjects
{
    public class CustomerTag : BaseDataTransferObject
    {
        /// <summary>
        /// 客户编号
        /// </summary>
        public long CustomerSysNo { get; set; }

        /// <summary>
        /// 标签
        /// </summary>
        public string Tag { get; set; }
    }
}
