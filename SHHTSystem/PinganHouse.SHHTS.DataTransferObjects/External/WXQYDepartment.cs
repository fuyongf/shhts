﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.DataTransferObjects.External
{
    public class WXQYDepartment
    {
        /// <summary>
        /// 部门名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 部门id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 上级部门
        /// 上级部门为0为顶级节点
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public int Order { get; set; }
    }
}
