﻿using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.DataTransferObjects.External
{
    /// <summary>
    /// 微信推送消息体
    /// </summary>
    public class WXMessage
    {

        private string toUserName;
        /// <summary>
        /// 开发者微信号
        /// </summary>
        public string ToUserName
        {
            get { return this.toUserName; }
            set { this.toUserName = value; }
        }

        private string fromUserName;
        /// <summary>
        /// 发送方帐号（一个OpenID）
        /// </summary>
        public string FromUserName
        {
            get { return this.fromUserName; }
            set { this.fromUserName = value; }
        }

        private DateTime createTime;
        /// <summary>
        /// 消息创建时间 
        /// </summary>
        public DateTime CreateTime
        {
            get
            {
                return this.createTime;
            }
            set { this.createTime = value; }
        }


        private WXMessageType msgType;
        /// <summary>
        /// 消息类型
        /// </summary>
        public WXMessageType MsgType
        {
            get
            {
                return this.msgType;
            }
            set { this.msgType = value; }
        }

        /// <summary>
        /// 事件消息类型
        /// </summary>
        public EventType? Event
        {
            get;
            set;
        }

        private string eventKey;

        /// <summary>
        /// 事件KEY值
        /// 未关注 qrscene_为前缀，后面为二维码的参数值
        /// </summary>
        public string EventKey
        {
            get { return this.eventKey; }
            set { this.eventKey = value; }
        }


        private string status;
        /// <summary>
        /// 推送消息状态
        /// </summary>
        public string Status
        {
            get { return this.status; }
            set { this.status = value; }
        }
        /// <summary>
        /// 推送消息id
        /// </summary>
        public string MsgID { get; set; }

        /// <summary>
        /// 二维码的ticket，可用来换取二维码图片
        /// </summary>
        public string Ticket { get; set; }

        /// <summary>
        /// 消息媒体id
        /// </summary>
        public string MediaId { get; set; }

        /// <summary>
        /// 文本消息
        /// </summary>
        public string Content { get; set; }
    }

    public class QYWXMessage : WXMessage
    {
        /// <summary>
        /// 应用编号
        /// </summary>
        public AgentType AgentID { get; set; }
    }
}
