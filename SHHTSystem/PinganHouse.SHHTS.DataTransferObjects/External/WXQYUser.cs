﻿using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.DataTransferObjects.External
{
    /// <summary>
    /// 微信企业成员
    /// </summary>
    [Serializable]
    public class WXQYUser
    {

        /// <summary>
        /// 成员id
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// 成员名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 成员所属部门
        /// </summary>
        public List<string> Department { get; set; }

        /// <summary>
        /// 职位
        /// </summary>
        public string Position { get; set; }

        /// <summary>
        /// 手机号码
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 微信号
        /// </summary>
        public string WeiXinId { get; set; }

        /// <summary>
        /// 用户头像地址
        /// </summary>
        public string Avatar { get; set; }

        /// <summary>
        /// 成员状态
        /// </summary>
        public WXQYUserStatus Status { get; set; }
    }
}
