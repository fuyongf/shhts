﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.DataTransferObjects.External
{
    public class FsIdentifyBankInfo
    {
        public int iBankID { get; set; }

        public string sBankName { get; set; }

        public string sShortName { get; set; }

        public int iStatus { get; set; }

        public string sBankImgUrl { get; set; }
    }
}
