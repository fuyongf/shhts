﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.DataTransferObjects.External
{
    public class FsBankInfo
    {
        public string ID { get; set; }

        public string sBankName { get; set; }

        public string sShortName { get; set; }
    }
}
