﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.DataTransferObjects
{
    public class CustomerBankAccount : BaseDataTransferObject
    {
        /// <summary>
        /// 案件编号
        /// </summary>
        public string CaseId { get; set; }

        /// <summary>
        /// 客户类型
        /// </summary>
        public CustomerType CustomerType { get; set; }

        /// <summary>
        /// 账户类型
        /// </summary>
        public BankAccountPurpose AccountType { get; set; }

        /// <summary>
        /// 业务类型
        /// </summary>
        public OrderBizType BizType { get; set; }

        /// <summary>
        /// 银行代码
        /// </summary>
        public string BankCode { get; set; }

        /// <summary>
        /// 开户银行
        /// </summary>
        public string BankName { get; set; }

        /// <summary>
        /// 支行名称
        /// </summary>
        public string SubBankName { get; set; }

        /// <summary>
        /// 收款帐号
        /// </summary>
        public string AccountNo { get; set; }

        /// <summary>
        /// 收款人
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// 身份证号
        /// </summary>
        public string IdCardNo { get; set; }

        /// <summary>
        /// 手机号码
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// 是否是主帐号
        /// </summary>
        public bool IsMainAccount { get; set; }

        /// <summary>
        /// 已鉴权
        /// </summary>
        public bool IsAuthed { get; set; }
    }
}
