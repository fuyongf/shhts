﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.DataTransferObjects
{
    public class DeductSchedule : BaseDataTransferObject
    {
        /// <summary>
        /// 申请方账户
        /// </summary>
        public long AccountSysNo { get; set; }

        /// <summary>
        /// 扣款用途
        /// </summary>
        public OrderBizType DeductType { get; set; }

        /// <summary>
        /// 扣款源
        /// </summary>
        public IList<OrderBizType> LimitSource { get; set; }

        /// <summary>
        /// 申请金额
        /// </summary>
        public decimal ApplyAmount { get; set; }

        /// <summary>
        /// 已付金额
        /// </summary>
        public decimal DeductedAmount { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 已完成
        /// </summary>
        public bool Finished { get; set; }

        /// <summary>
        /// 版本号
        /// </summary>
        public long Verson { get; set; }

        /// <summary>
        /// 案件编号
        /// </summary>
        public string CaseId { get; set; }

        /// <summary>
        /// 物业合同标识
        /// </summary>
        public string TenementContract { get; set; }

        /// <summary>
        /// 物业地址
        /// </summary>
        public string TenementAddress { get; set; }

        /// <summary>
        /// 账户类型
        /// </summary>
        public AccountType AccountType { get; set; }
    }
}
