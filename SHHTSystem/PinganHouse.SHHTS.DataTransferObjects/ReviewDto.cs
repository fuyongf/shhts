﻿using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.DataTransferObjects
{
    public class ReviewDto:BaseDataTransferObject
    {
        /// <summary>
        /// 复核方式
        /// </summary>
        public ReviewMode ReviewMode { get; set; }

        /// <summary>
        /// 复核人姓名
        /// </summary>
        public IList<string> ReviewName { get; set; }
    }
}
