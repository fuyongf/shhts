﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.DataTransferObjects
{
    public class TradeDetail : BaseDataTransferObject
    {
        /// <summary>
        /// 交易流水号
        /// </summary>
        public string TradeNo { get; set; }

        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderNo { get; set; }

        ///// <summary>
        ///// 支付方式
        ///// </summary>
        //public PaymentChannel PaymentChannel { get; set; }

        /// <summary>
        /// 银行流水号
        /// </summary>
        public string SerialNo { get; set; }

        /// <summary>
        /// 交易金额
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// 支付时间
        /// </summary>
        public DateTime? PaymentDate { get; set; }

        /// <summary>
        /// 银行帐号
        /// </summary>
        public string BankAccountNo { get; set; }

        /// <summary>
        /// 银行代码
        /// </summary>
        public string BankCode { get; set; }

        /// <summary>
        /// 开户行
        /// </summary>
        public string SubBankName { get; set; }

        /// <summary>
        /// 开户名
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// 交易状态
        /// </summary>
        public TradeStatus Status { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 已出票据
        /// </summary>
        public bool IsBilled { get; set; }

        /// <summary>
        /// 对账状态
        /// </summary>
        public ReconciliationStatus ReconciliationStatus { get; set; }
    }

    public class TradePaginatedInfo : TradeDetail 
    {
        /// <summary>
        /// 案件编号
        /// </summary>
        public string CaseId { get; set; }

        /// <summary>
        /// 物业地址
        /// </summary>
        public string TenementAddress { get; set; }

        /// <summary>
        /// 账户类型
        /// </summary>
        public AccountType AccountType { get; set; }

        public string IncomeAmount {
            get 
            {
                if (Amount > 0)
                    return Amount.ToString("C").Substring(1);
                else
                    return "--";
            } 
        }
        public string OutlayAmount
        {
            get
            {
                if (Amount < 0)
                    return Amount.ToString("C").Substring(1);
                else
                    return "--";
            }
        }
        /// <summary>
        /// 支付方式
        /// </summary>
        public PaymentChannel PaymentChannel { get; set; }

        /// <summary>
        /// 订单类型
        /// </summary>
        public OrderType OrderType { get; set; }

        /// <summary>
        /// 业务类型
        /// </summary>
        public OrderBizType OrderBizType { get; set; }
    }

    public class ReceiptTradeInfo : TradeDetail 
    {
        /// <summary>
        /// 终端编号
        /// </summary>
        public string TerminalNo { get; set; }
    }
}
