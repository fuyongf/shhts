﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.DataTransferObjects
{
    public class PrintTemplate 
    {
        /// <summary>
        /// 模版编号
        /// </summary>
        public string TemplateId { get; set; }

        /// <summary>
        /// 模版名称
        /// </summary>
        public string TemplateName { get; set; }

        /// <summary>
        /// 类别
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// 页码
        /// </summary>
        public IList<string> Pages { get; set; }
    }

    public class PrintTemplateValue
    {
        /// <summary>
        /// 模版编号
        /// </summary>
        public string TemplateId { get; set; }

        /// <summary>
        /// 页码
        /// </summary>
        public string PageNo { get; set; }

        /// <summary>
        /// 字段
        /// </summary>
        public string FieldKey { get; set; }

        /// <summary>
        /// 字段名称
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// 字体
        /// </summary>
        public string Font { get; set; }

        /// <summary>
        /// 字体大小
        /// </summary>
        public int FontSize { get; set; }

        /// <summary>
        /// 横坐标
        /// </summary>
        public int PosX { get; set; }

        /// <summary>
        /// 纵坐标
        /// </summary>
        public int PosY { get; set; }
    }
}
