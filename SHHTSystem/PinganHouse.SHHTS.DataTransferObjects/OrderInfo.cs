﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace PinganHouse.SHHTS.DataTransferObjects
{
    /// <summary>
    /// 订单信息
    /// </summary>
    public class Order : BaseDataTransferObject
    {
        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 订单类型
        /// </summary>
        public OrderType OrderType { get; set; }

        /// <summary>
        /// 支付方式
        /// </summary>
        public PaymentChannel PaymentChannel { get; set; }

        /// <summary>
        /// 账户编号
        /// </summary>
        public long AccountSysNo { get; set; }

        /// <summary>
        /// 对方账户(收款订单：空，付款(转账)订单：收款账户，提现(退款)订单：空)
        /// </summary>
        public long? OppositeAccount { get; set; }

        /// <summary>
        /// 订单总金额
        /// </summary>
        public decimal OrderAmount { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 订单业务类型
        /// </summary>
        public OrderBizType OrderBizType { get; set; }

        /// <summary>
        /// 订单状态
        /// </summary>
        public OrderStatus OrderStatus { get; set; }

        /// <summary>
        /// 审核状态
        /// </summary>
        public AuditStatus AuditStatus { get; set; }
    }

    public class OrderDetail : Order
    {
        #region Payment Order | PaidOff Order
        /// <summary>
        /// 银行代码
        /// </summary>
        public string BankCode { get; set; }

        /// <summary>
        /// 开户行
        /// </summary>
        public string SubBankName { get; set; }

        /// <summary>
        /// 银行帐号
        /// </summary>
        public string BankAccountNo { get; set; }

        /// <summary>
        /// 开户名
        /// </summary>
        public string AccountName { get; set; }
        #endregion
    }

    public class OrderPaginatedInfo : Order
    {
        public static string GetEnumDesc(object e)
        {
            var enumInfo = e.GetType().GetField(e.ToString());
            var enumAttributes = (DescriptionAttribute[])enumInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return enumAttributes.Length > 0 ? enumAttributes[0].Description : e.ToString();
        }
        public string  OrderTypeDes{
            get
            {
                   return GetEnumDesc(OrderType);
            }
        }
        /// <summary>
        /// 案件编号
        /// </summary>
        public string CaseId { get; set; }

        /// <summary>
        /// 物业地址
        /// </summary>
        public string TenementAddress { get; set; }

        /// <summary>
        /// 到账金额
        /// </summary>
        public decimal CollectedBalance { get; set; }

        /// <summary>
        /// 账户类型
        /// </summary>
        public AccountType AccountType { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreateUserName { get; set; }

        public string OrderStatusDes 
        {
            get
            {
                if (this.OrderType == OrderType.Payment || this.PaymentChannel == PaymentChannel.Entrust)
                    return GetEnumDesc(this.AuditStatus);
                else 
                    return "--";
            }
        }

        /// <summary>
        /// 已开收据总金额
        /// </summary>
        public decimal OpenRecieptAccount { get; set; }

        public bool InputPOSRetureReceipt
        {
            get
            {
                return ((this.OrderType == OrderType.Collection) && (this.PaymentChannel == PaymentChannel.POS) && (this.CollectedBalance < this.OrderAmount) 
                    || ((this.OrderType == OrderType.Expend) && (this.PaymentChannel == PaymentChannel.POS) && (this.CollectedBalance < this.OrderAmount)));
            }
        }
        public bool OpenReceipt
        {
            get
            {
                return this.OrderType == OrderType.Collection && (this.OpenRecieptAccount < this.CollectedBalance || this.PaymentChannel==PaymentChannel.POS);
            }
        }
        public bool Refund
        {
            get { return ((this.OrderType == OrderType.Collection && this.OrderStatus == OrderStatus.Paid) || (OrderType == OrderType.Expend && this.OrderStatus == OrderStatus.Paid)); }
        }
        public bool Approve
        {
            get
            {
                return this.OrderType == OrderType.Collection && this.PaymentChannel == PaymentChannel.Entrust && this.OrderStatus == OrderStatus.Initial 
                        || (this.OrderType == OrderType.Payment && (this.AuditStatus == AuditStatus.Initial || this.AuditStatus == AuditStatus.Audited))
                        || (this.OrderType == OrderType.PaidOff && (this.AuditStatus == AuditStatus.Initial || this.AuditStatus == AuditStatus.Audited));
            }
        }

        public string PaymentChannelDisplay
        {
            get
            {
                if (this.OrderType == OrderType.PaidOff || this.OrderType == OrderType.Payment)
                {
                    return "--";
                }
                else
                {
                    return GetEnumDesc(PaymentChannel);  
                }
            }
        }
        /// <summary>
        /// 交易列表
        /// </summary>
        //public IList<TradeDetail> TradeDetails { get; set; }
    }

    public class RefundOrderPaginatedInfo : BaseDataTransferObject
    {
        public string OrderNo { get; set; }

        public string CaseId { get; set; }

        public decimal OrderAmount { get; set; }

        public string CreateUserName { get; set; }

        public OrderStatus OrderStatus { get; set; }

        public AuditStatus AuditStatus { get; set; }

        public bool Approve
        {
            get
            {
                return AuditStatus == AuditStatus.Initial || AuditStatus == AuditStatus.Audited;
            }
        }

        public string Remark { get; set; }
    }

    public class OrderBilledInfo : Order 
    {
        /// <summary>
        /// 案件编号
        /// </summary>
        public string CaseId { get; set; }

        /// <summary>
        /// 物业地址
        /// </summary>
        public string TenementAddress { get; set; }

        /// <summary>
        /// 经纪人
        /// </summary>
        public string AgentName { get; set; }
    }
}
