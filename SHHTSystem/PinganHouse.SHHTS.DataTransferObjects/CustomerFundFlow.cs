﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.DataTransferObjects
{
    public class CustomerFundFlow : BaseDataTransferObject
    {
        /// <summary>
        /// 案件编号
        /// </summary>
        public string CaseId { get; set; }

        /// <summary>
        /// 客户类型
        /// </summary>
        public CustomerType CustomerType { get; set; }

        /// <summary>
        /// 条件类型
        /// </summary>

        public string ConditionType { get; set; }

        /// <summary>
        /// 条件名称
        /// </summary>

        public string ConditionName { get; set; }

        /// <summary>
        /// 条件值
        /// </summary>

        public string ConditionValue { get; set; }

        /// <summary>
        /// 支付渠道
        /// </summary>
        public FundPlanPayChannel PaymentChannel { get; set; }

        /// <summary>
        /// 房款
        /// </summary>
        public decimal HouseFund { get; set; }

        /// <summary>
        /// 税费
        /// </summary>
        public decimal Taxes { get; set; }

        /// <summary>
        /// 中介佣金
        /// </summary>
        public decimal Brokerage { get; set; }

        /// <summary>
        /// 装修补偿
        /// </summary>
        public decimal DecorateCompensation { get; set; }

        /// <summary>
        /// 定金
        /// </summary>
        public decimal Earnest { get; set; }

        /// <summary>
        /// 其他费用
        /// </summary>
        public decimal OtherCharges { get; set; }

        /// <summary>
        /// 合计
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// 账户余额
        /// </summary>
        public decimal AccountBalance { get; set; }
    }
}
