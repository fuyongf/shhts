﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.DataTransferObjects
{
    /// <summary>
    /// 银行鉴权配置
    /// </summary>
    public class BankIdentifyCfg
    {
        /// <summary>
        /// 获取或设置银行代码.
        /// </summary>
        public string BankCode { get; set; }
        /// <summary>
        /// 获取或设置银行Id.
        /// </summary>
        public int BankId { get; set; }
        /// <summary>
        /// 获取或设置图片Url.
        /// </summary>
        public string ImageUrl { get; set; }
        /// <summary>
        /// 获取或设置银行名称.
        /// </summary>
        public string BankName { get; set; }
        /// <summary>
        /// 获取或设置需要代扣
        /// </summary>
        public bool IsNeedWithhold { get; set; }
        /// <summary>
        /// 获取或设置代扣金额
        /// </summary>
        public decimal Withhold { get; set; }
        /// <summary>
        /// 获取或设置需要姓名
        /// </summary>
        public bool IsNeedName { get; set; }
        /// <summary>
        /// 获取或设置需要银行账户
        /// </summary>
        public bool IsNeedBankAccount { get; set; }
        /// <summary>
        /// 获取或设置需要身份证
        /// </summary>
        public bool IsNeedIdCard { get; set; }
        /// <summary>
        /// 获取或设置需要手机
        /// </summary>
        public bool IsNeedMobile { get; set; }
    }
}
