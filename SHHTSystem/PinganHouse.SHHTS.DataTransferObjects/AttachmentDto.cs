﻿using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace PinganHouse.SHHTS.DataTransferObjects
{
    [MessageContract]
    public class AttachmentDto
    {
        public AttachmentDto()
        { }
        /// <summary>
        /// 关联业务编号
        /// </summary>
        [MessageHeader(MustUnderstand = true)]
        public string AssociationNo { get; set; }
        /// <summary>
        /// 附件id
        /// </summary>
        [MessageHeader(MustUnderstand = true)]
        public string FileId { get; set; }

        /// <summary>
        /// 附件类型
        /// </summary>
        [MessageHeader(MustUnderstand = true)]
        public AttachmentType AttachmentType { get; set; }

        /// <summary>
        /// 附件名称
        /// </summary>
        [MessageHeader(MustUnderstand = true)]
        public string FileName { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [MessageHeader(MustUnderstand = true)]
        public string Remark { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [MessageHeader(MustUnderstand = true)]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// 创建人系统编号
        /// </summary>
        [MessageHeader(MustUnderstand = true)]
        public long CreateUserSysNo { get; set; }

    }


    [MessageContract]
    public class AttachmentDtos
    {
        [MessageBodyMember]
        public IList<AttachmentDto> Attachments { get; set; }
        /// <summary>
        /// 总页数
        /// </summary>
        [MessageBodyMember]
        public int TotalCount
        {
            get;
            set;
        }

    }
    [MessageContract]
    public class UploadAttachmentResult
    {
        [MessageBodyMember]
        public string FileId { get; set; }

        [MessageBodyMember]
        public string ResultMessage { get; set; }

         [MessageBodyMember]
         public bool Success
         {
             get;
             set;
         }
    }

    [MessageContract]
    public class DeleteAttachmentResult
    {
        [MessageBodyMember]
        public string ResultMessage { get; set; }

        [MessageBodyMember]
        public bool Success
        {
            get;
            set;
        }
    }

    [MessageContract]
    public class DeleteAttachmentElement
    {
        [MessageBodyMember]
        public string FileId { get; set; }

        [MessageBodyMember]
        public long ModifyUserSysNo { get; set; }

    }

    [MessageContract]
    public class AttachmentQuery
    {
        [MessageBodyMember]
        public string FileId { get; set; }

        [MessageBodyMember]
        public string AssociationNo { get; set; }

        [MessageBodyMember]
        public AttachmentType AttachmentType { get; set; }

        /// <summary>
        /// 当前页数
        /// </summary>
        [MessageBodyMember]
        public int PageIndex { get; set; }

        /// <summary>
        /// 行数
        /// </summary>
        [MessageBodyMember]
        public int PageCount { get; set; }
    }
}
