﻿using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.DataTransferObjects
{
    /// <summary>
    /// 案件dto
    /// </summary>
    public class CaseDto:BaseDataTransferObject
    {
        public CaseDto()
        { }
        /// <summary>
        /// 案件编号
        /// </summary>
        public string CaseId { get; set; }

        private List<long> sellers =new List<long>();
        /// <summary>
        /// 卖家
        /// </summary>
        public List<long> Sellers
        {
            get { return sellers; }
            set 
            {
                if (value == null)
                    value = new List<long>();
                sellers = value;
            }
        }

        private List<long> buyers = new List<long>();

        public List<long> Buyers
        {
            get { return buyers; }
            set 
            {
                if (value == null)
                    value = new List<long>();
                buyers = value;
            }
        }

        /// <summary>
        /// 受理人
        /// </summary>
        public long OperatorSysNo { get; set; }

        /// <summary>
        /// 中介
        /// </summary>
        public long AgencySysNo { get; set; }


        /// <summary>
        /// 物业合同标识
        /// </summary>
        public string TenementContract { get; set; }

        /// <summary>
        /// 中介公司
        /// </summary>
        public long CompanySysNo { get; set; }

        /// <summary>
        /// 物业地址
        /// </summary>
        public string TenementAddress { get; set; }

        /// <summary>
        /// 是否资金托管
        /// </summary>
        public bool IsFund { get; set; }

        private string fundTrusteeshipContract;
        /// <summary>
        /// 托管资金合同号
        /// </summary>
        public string FundTrusteeshipContract
        {
            get { return this.fundTrusteeshipContract; }
            set
            {
                if (IsFund && string.IsNullOrWhiteSpace(value))
                    throw new ArgumentNullException("TrusteeshipContract", "TrusteeshipContract is null");
                this.fundTrusteeshipContract = value;
            }
        }

        /// <summary>
        /// 是否佣金托管
        /// </summary>
        public bool IsCommission { get; set; }

        private string commissionTrusteeshipContract;
        /// <summary>
        /// 佣金托管合同
        /// </summary>
        public string CommissionTrusteeshipContract
        {
            get { return this.commissionTrusteeshipContract; }
            set
            {
                if (IsCommission && string.IsNullOrWhiteSpace(value))
                    throw new ArgumentNullException("CommissionTrusteeshipContract", "CommissionTrusteeshipContract is null");
                this.commissionTrusteeshipContract = value;
            }
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 已失效
        /// </summary>
        public bool Disabled { get; set; }

        /// <summary>
        /// 案件状态
        /// </summary>
        public CaseStatus CaseStatus { get; set; }

        /// <summary>
        /// 案件归属地
        /// </summary>
        public ReceptionCenter ReceptionCenter { get; set; }

        /// <summary>
        /// 案件来源
        /// </summary>
        public SourceType SourceType { get; set; }

        #region 查询字段
        /// <summary>
        /// 公司名称
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// 操作者名字
        /// </summary>
        public string OperatorName { get; set; }

        public string SellerName {
            get 
            {
                if (sellerNameDic == null || sellerNameDic.Count == 0)
                    return string.Empty;
                return string.Join(",", sellerNameDic.Values); 
            }
        }

        public string BuyerName {
            get 
            {
                if (buyerNameDic == null || buyerNameDic.Count==0)
                    return string.Empty;
                return string.Join(",", buyerNameDic.Values);
            }
        }

        private Dictionary<long, string> sellerNameDic = new Dictionary<long, string>();
        /// <summary>
        /// 卖家姓名集合
        /// </summary>
        public Dictionary<long, string> SellerNameDic {
            get { return this.sellerNameDic; }
            set 
            {
                this.sellerNameDic = value;
            }
        }

        private Dictionary<long, string> buyerNameDic = new Dictionary<long, string>();
        /// <summary>
        /// 买家姓名集合
        /// </summary>
        public Dictionary<long, string> BuyerNameDic
        {
            get { return this.buyerNameDic; }
            set { this.buyerNameDic = value; }
        }
        /// <summary>
        /// 中介名字
        /// </summary>
        public string AgencyName { get; set; }

        public string CreatorName { get; set; }

        public string ModifyUserName { get; set; }

        private Dictionary<string, object> otherData =new Dictionary<string,object>();
        //value需为基类型
        public Dictionary<string, object> OtherData
        {
            get { return this.otherData; }
            set { this.otherData = value; }
        }

        IList<string> nextActions = new List<string>();
        /// <summary>
        /// 获取或设置下一步动作列表.
        /// </summary>
        public IList<string> NextActions
        {
            get { return nextActions; }
            set { nextActions = value; }
        }
        #endregion

        /// <summary>
        /// 添加买家
        /// </summary>
        /// <param name="buyer"></param>
        public void AddBuyer(params long[] buyer)
        {
            this.Add(buyers, buyer);
        }

        /// <summary>
        /// 添加卖家
        /// </summary>
        /// <param name="buyer"></param>
        public void AddSeller(params long[] seller)
        {
            this.Add(sellers, seller);
        }
     


        private void Add(List<long> ps, params long[] person)
        {
            
            if (person == null || person.Length == 0)
                throw new ArgumentException("未提供买家或卖家");
            foreach (var p in person)
            {
                if (!ps.Contains(p))
                    ps.Add(p);
            }
        }
    }

    public class CaseWithCustomer : BaseDataTransferObject
    {
        /// <summary>
        /// 案件编号
        /// </summary>
        public string CaseId { get; set; }

        private List<Customer> sellers = new List<Customer>();
        /// <summary>
        /// 卖家
        /// </summary>
        public List<Customer> Sellers
        {
            get { return sellers; }
            set
            {
                if (value == null)
                    value = new List<Customer>();
                sellers = value;
            }
        }

        private List<Customer> buyers = new List<Customer>();

        public List<Customer> Buyers
        {
            get { return buyers; }
            set
            {
                if (value == null)
                    value = new List<Customer>();
                buyers = value;
            }
        }

        /// <summary>
        /// 受理人
        /// </summary>
        public long OperatorSysNo { get; set; }

        /// <summary>
        /// 中介
        /// </summary>
        public long AgencySysNo { get; set; }


        /// <summary>
        /// 物业合同标识
        /// </summary>
        public string TenementContract { get; set; }

        /// <summary>
        /// 中介公司
        /// </summary>
        public long CompanySysNo { get; set; }

        /// <summary>
        /// 物业地址
        /// </summary>
        public string TenementAddress { get; set; }

        /// <summary>
        /// 是否资金托管
        /// </summary>
        public bool IsFund { get; set; }

        private string fundTrusteeshipContract;
        /// <summary>
        /// 托管资金合同号
        /// </summary>
        public string FundTrusteeshipContract
        {
            get { return this.fundTrusteeshipContract; }
            set
            {
                if (IsFund && string.IsNullOrWhiteSpace(value))
                    throw new ArgumentNullException("TrusteeshipContract", "TrusteeshipContract is null");
                this.fundTrusteeshipContract = value;
            }
        }

        /// <summary>
        /// 是否佣金托管
        /// </summary>
        public bool IsCommission { get; set; }

        private string commissionTrusteeshipContract;
        /// <summary>
        /// 佣金托管合同
        /// </summary>
        public string CommissionTrusteeshipContract
        {
            get { return this.commissionTrusteeshipContract; }
            set
            {
                if (IsCommission && string.IsNullOrWhiteSpace(value))
                    throw new ArgumentNullException("CommissionTrusteeshipContract", "CommissionTrusteeshipContract is null");
                this.commissionTrusteeshipContract = value;
            }
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 已失效
        /// </summary>
        public bool Disabled { get; set; }

        /// <summary>
        /// 案件状态
        /// </summary>
        public CaseStatus CaseStatus { get; set; }

        /// <summary>
        /// 案件归属地
        /// </summary>
        public ReceptionCenter ReceptionCenter { get; set; }

        #region 查询字段
        /// <summary>
        /// 公司名称
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// 操作者名字
        /// </summary>
        public string OperatorName { get; set; }

        /// <summary>
        /// 中介名字
        /// </summary>
        public string AgencyName { get; set; }

        public string CreatorName { get; set; }

        public string ModifyUserName { get; set; }

        private Dictionary<string, object> otherData = new Dictionary<string, object>();
        public Dictionary<string, object> OtherData
        {
            get { return this.otherData; }
            set { this.otherData = value; }
        }
        #endregion

        /// <summary>
        /// 添加买家
        /// </summary>
        /// <param name="buyer"></param>
        public void AddBuyer(params Customer[] buyer)
        {
            this.Add(buyers, buyer);
        }

        /// <summary>
        /// 添加卖家
        /// </summary>
        /// <param name="buyer"></param>
        public void AddSeller(params Customer[] seller)
        {
            this.Add(sellers, seller);
        }



        private void Add(List<Customer> ps, params Customer[] person)
        {

            if (person == null || person.Length == 0)
                throw new ArgumentException("未提供买家或卖家");
            foreach (var p in person)
            {
                if (!ps.Any(i=>i.SysNo == p.SysNo))
                    ps.Add(p);
            }
        }
    }

    public class CaseAttachmentDto
    {
        public CaseAttachmentDto()
        {
            BuyerAttachments = new List<Tuple<long, string, IEnumerable<AttachmentDto>>>();
            SellerAttachments = new List<Tuple<long, string, IEnumerable<AttachmentDto>>>();
            OtherAttachments=new List<AttachmentDto>();
        }
        public List<Tuple<long, string, IEnumerable<AttachmentDto>>> BuyerAttachments { get; set; }

        public List<Tuple<long, string, IEnumerable<AttachmentDto>>> SellerAttachments { get; set; }

        public List<AttachmentDto> OtherAttachments { get; set; }
    }

    public class CaseRemark : BaseDataTransferObject
    {
        /// <summary>
        /// 物业名称
        /// </summary>
        public string TenementName { get; set; }

        /// <summary>
        /// 环线位置
        /// </summary>
        public string LoopLinePosition { get; set; }

        /// <summary>
        /// 建筑面积
        /// </summary>
        public decimal? CoveredArea { get; set; }

        /// <summary>
        /// 房间数
        /// </summary>
        public int? RoomCount { get; set; }

        /// <summary>
        /// 厅数
        /// </summary>
        public int? HallCount { get; set; }

        /// <summary>
        /// 卫生间数
        /// </summary>
        public int ToiletCount { get; set; }

        /// <summary>
        /// 楼层数
        /// </summary>
        public int? FloorCount { get; set; }

        /// <summary>
        /// 竣工时间
        /// </summary>
        public DateTime? BuiltCompletedDate { get; set; }

        /// <summary>
        /// 居间价格
        /// </summary>
        public decimal? MediatorPrice { get; set; }

        /// <summary>
        /// 网签价格
        /// </summary>
        public decimal? NetlabelPrice { get; set; }

        /// <summary>
        /// 真实成交价格
        /// </summary>
        public decimal? RealPrice { get; set; }

        /// <summary>
        /// 是否做低房价
        /// </summary>
        public bool? IsLowPrice { get; set; }

        /// <summary>
        /// 交易类型
        /// </summary>
        public TradeType TradeType { get; set; }

        /// <summary>
        /// 物业性质
        /// </summary>
        public TenementNature TenementNature { get; set; }

        /// <summary>
        /// 居住类型
        /// </summary>
        public ResideType ResideType { get; set; }

        /// <summary>
        /// 是否首次购买
        /// </summary>
        public bool? FirstTimeBuy { get; set; }

        /// <summary>
        /// 购买年限
        /// </summary>
        public int? PurchaseYears { get; set; }

        /// <summary>
        /// 是否是唯一住房
        /// </summary>
        public bool? OnlyHousing { get; set; }

        /// <summary>
        /// 车位地址
        /// </summary>
        public string CarportAddress { get; set; }

        /// <summary>
        /// 车位面积
        /// </summary>
        public decimal? CarportArea { get; set; }

        /// <summary>
        /// 车位价格
        /// </summary>
        public decimal? CarportPrice { get; set; }

        /// <summary>
        /// 是否补地价
        /// </summary>
        public bool? IsRepairLandPrice { get; set; }

        /// <summary>
        /// 是否国安审批
        /// </summary>
        public bool? IsNSAApproval { get; set; }

        /// <summary>
        /// 是否历史保护建筑
        /// </summary>
        public bool? IsProtectiveBuilding { get; set; }

        /// <summary>
        /// 是否公证
        /// </summary>
        public bool? IsNotarize { get; set; }

        /// <summary>
        /// 公证员
        /// </summary>
        [Obsolete("use NotarySysNo instead")]
        public string Notary { get; set; }

        /// <summary>
        /// 公证员联系方式
        /// </summary>
        [Obsolete("use NotarySysNo instead")]
        public string NotaryContact { get; set; }

        /// <summary>
        /// 公证员系统编号.
        /// </summary>
        public long NotarySysNo { get; set; }

        ///// <summary>
        ///// 获取或设置是否已满2年.
        ///// </summary>
        //public bool? BuyOverTwoYears { get; set; }
        ///// <summary>
        ///// 获取或设置是否已满5年.
        ///// </summary>
        //public bool? BuyOverFiveYears { get; set; }

        /// <summary>
        /// 购买已满年限
        /// </summary>
        public BuyOverYears BuyOverYears { get; set; }

        /// <summary>
        /// 获取或设置车位上手价格.
        /// </summary>
        public decimal? CarportRawPrice { get; set; }

        /// <summary>
        /// 获取或设置物业上手价格.
        /// </summary>
        public decimal? TenementRawPrice { get; set; }

        private Dictionary<string, object> data = new Dictionary<string, object>();
        public Dictionary<string, object> OtherData 
        { 
            get { return this.data; }
            set { this.data = value; }
        }
    }

    public class CaseEvent : BaseDataTransferObject 
    {
        /// <summary>
        /// 案件编号
        /// </summary>
        public string CaseId { get; set; }

        /// <summary>
        /// 源状态
        /// </summary>
        public CaseStatus SourceStatus { get; set; }

        /// <summary>
        /// 事件类型
        /// </summary>
        public CaseStatus EventType { get; set; }

        /// <summary>
        /// 已完成
        /// </summary>
        public bool Finished { get; set; }

        /// <summary>
        /// 操作人
        /// </summary>
        public long OperatorSysNo { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 其他信息集合
        /// </summary>
        public Dictionary<string, object> OtherDatas { get; set; }
    }
}
