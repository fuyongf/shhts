﻿using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.DataTransferObjects
{
    /// <summary>
    /// 表示贷款方案
    /// </summary>
    public class LoanOptimizePlan
    {

        /// <summary>
        /// 获取设置套数认定结果.
        /// </summary>
        public TaoShuRd TaoShu { get; set; }

        /// <summary>
        /// 获取或设置还款方式.
        /// </summary>
        public RepayType RepayType { get; set; }

        /// <summary>
        /// 获取或设置贷款银行代码
        /// </summary>
        public string BankCode { get; set; }

        /// <summary>
        /// 获取或设置贷款银行名称
        /// </summary>
        public string BankName { get; set; }

        /// <summary>
        /// 获取或设置贷款银行Logo图片
        /// </summary>
        public string BankLogo { get; set; }


        /// <summary>
        /// 获取或设置公积金贷款金额（万元）.
        /// </summary>
        public decimal GAmount { get; set; }
        /// <summary>
        /// 获取或设置公积金贷款年限.
        /// </summary>
        public int GYears { get; set; }
        /// <summary>
        /// 获取或设置公积金贷款利率.
        /// </summary>
        public decimal GInterest { get; set; }

        /// <summary>
        /// 获取或设置公积金贷款（首）月还款金额.
        /// </summary>
        public decimal GRepay { get; set; }


        /// <summary>
        /// 获取或设置商业贷款金额（万元）.
        /// </summary>
        public decimal SAmount { get; set; }
        /// <summary>
        /// 获取或设置商业贷款年限.
        /// </summary>
        public int SYears { get; set; }
        /// <summary>
        /// 获取或设置商业贷款利率.
        /// </summary>
        public decimal SInterest { get; set; }

        /// <summary>
        /// 获取或设置商业贷款（首）月还款金额.
        /// </summary>
        public decimal SRepay { get; set; }
    }

    /// <summary>
    /// 用户输入数据
    /// </summary>
    public class OInputs
    {
        /// <summary>
        /// 获取或设置主贷人性别
        /// </summary>
        public Gender UserBasic1 { get; set; }
        /// <summary>
        /// 获取或设置家庭税后（月）工资收入（元）
        /// </summary>
        public decimal UserBasic2 { get; set; }
        /// <summary>
        /// 获取或设置买卖合同价格（万元）
        /// </summary>
        public decimal UserBasic3 { get; set; }
        /// <summary>
        /// 获取或设置竣工时间
        /// </summary>
        public int UserBasic4 { get; set; }
        /// <summary>
        /// 获取或设置房屋类型
        /// </summary>
        public ResideType UserBasic5 { get; set; }
        /// <summary>
        /// 获取或设置主贷人年龄
        /// </summary>
        public int UserBasic6 { get; set; }
        /// <summary>
        /// 获取或设置是否有未结清公积金贷款
        /// </summary>
        public bool UserBasic7 { get; set; }

        /// <summary>
        /// 获取或设置贷款金额.
        /// </summary>
        public decimal? UserRequire1 { get; set; }

        /// <summary>
        /// 获取或设置月还款额.
        /// </summary>
        public decimal? UserRequire2 { get; set; }

        /// <summary>
        /// 获取或设置还款期限.
        /// </summary>
        public int UserRequire3 { get; set; }

        /// <summary>
        /// 获取或设置还款方式.
        /// </summary>
        public RepayType UserRequire4 { get; set; }

        /// <summary>
        /// 获取或设置本人公积金余额.
        /// </summary>
        public decimal Gjj1 { get; set; }
        /// <summary>
        /// 获取或设置本人公积金月缴额.
        /// </summary>
        public decimal Gjj2 { get; set; }
        /// <summary>
        /// 获取或设置本人补充公积金余额.
        /// </summary>
        public decimal Gjj3 { get; set; }
        /// <summary>
        /// 获取或设置配偶公积金余额.
        /// </summary>
        public decimal Gjj4 { get; set; }
        /// <summary>
        /// 获取或设置配偶公积金月缴.
        /// </summary>
        public decimal Gjj5 { get; set; }
        /// <summary>
        /// 获取或设置配偶补充公积金余额.
        /// </summary>
        public decimal Gjj6 { get; set; }
        /// <summary>
        /// 获取或设置本人是否已经连续缴纳6个月.
        /// </summary>
        public bool Gjj7 { get; set; }
        /// <summary>
        /// 获取或设置配偶是否已经连续缴纳6个月.
        /// </summary>
        public bool Gjj8 { get; set; }
        /// <summary>
        /// 获取或设置本人是否有补充公积金.
        /// </summary>
        public bool Gjj9 { get; set; }
        /// <summary>
        /// 获取或设置配偶是否有补充公积金.
        /// </summary>
        public bool Gjj10 { get; set; }
        /// <summary>
        /// 获取或设置本人公积金余额是否有12500.
        /// </summary>
        public bool Gjj11 { get; set; }
        /// <summary>
        /// 获取或设置配偶公积金余额是否有12500.
        /// </summary>
        public bool Gjj12 { get; set; }

        /// <summary>
        /// 获取或设置婚姻状况
        /// </summary>
        public MaritalStatus Xgxd1 { get; set; }

        /// <summary>
        /// 获取或设置现有住房套数.
        /// </summary>
        public int Xgxd2 { get; set; }

        /// <summary>
        /// 获取或设置未结清住房贷款套数.
        /// </summary>
        public int Xgxd3 { get; set; }
        /// <summary>
        /// 获取或设置户籍身份.
        /// </summary>
        public DomicileType Xgxd4 { get; set; }

        /// <summary>
        /// Normalizes this instance.
        /// </summary>
        public void Normalize()
        {
            if (this.UserBasic2 < 0)
                throw new ArgumentException("家庭税后工资收入不能小于0");
            if (this.UserBasic3 < 0)
                throw new ArgumentException("买卖合同价格不能小于0");
            if (this.UserBasic4 < 0)
                throw new ArgumentException("竣工时间不能小于0");
            if (this.UserBasic6 < 18 || this.UserBasic6 > 70)
                throw new ArgumentException("主贷人年龄须介于18-70岁");

            if (!this.UserRequire1.HasValue && !this.UserRequire2.HasValue)
                throw new NotSupportedException("贷款金额和月还款额至少设置一个");

            if (this.UserRequire1.HasValue && this.UserRequire2.HasValue)
                throw new NotSupportedException("贷款金额和月还款额不能同时设置");

            if (this.UserRequire1.HasValue && this.UserRequire1 < 0)
                throw new ArgumentException("贷款金额不能小于0");

            if (this.UserRequire2.HasValue && this.UserRequire2 < 0)
                throw new ArgumentException("月还款额不能小于0");

            if (UserRequire3 < 1 || UserRequire3 > 30)
                throw new ArgumentException("贷款期限必须介于1-30年");

            if (Gjj7)
            {
                if (Gjj11)
                    Gjj1 = 12500;

                if (!Gjj9)
                    Gjj3 = 0;

                if (Gjj1 < 0)
                    throw new ArgumentException("本人公积金余额不能小于0");
                if (Gjj2 < 0)
                    throw new ArgumentException("本人公积金月缴不能小于0");
                if (Gjj3 < 0)
                    throw new ArgumentException("本人补充公积金余额不能小于0");
            }
            else
            {
                Gjj1 = 0;
                Gjj2 = 0;
                Gjj3 = 0;
                Gjj9 = false;
                Gjj11 = false;
            }

            if (Gjj8)
            {
                if (Gjj12)
                    Gjj4 = 12500;

                if (!Gjj10)
                    Gjj6 = 0;

                if (Gjj4 < 0)
                    throw new ArgumentException("本人公积金余额不能小于0");
                if (Gjj5 < 0)
                    throw new ArgumentException("本人公积金月缴不能小于0");
                if (Gjj6 < 0)
                    throw new ArgumentException("本人补充公积金余额不能小于0");
            }
            else
            {
                Gjj4 = 0;
                Gjj5 = 0;
                Gjj6 = 0;
                Gjj10 = false;
                Gjj12 = false;
            }

            if (this.Xgxd2 < 0)
                throw new ArgumentException("现有住房套数不能小于0");
            if (this.Xgxd3 < 0)
                throw new ArgumentException("未结清住房套数不能小于0");
        }
    }
}
