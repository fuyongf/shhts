﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.DataTransferObjects
{
    public class CustomerBaseDto : BaseDataTransferObject
    {
        public CustomerBaseDto() 
        {
            Customers = new List<Customer>();
        }

        /// <summary>
        /// 案件编号
        /// </summary>
        public string CaseId { get; set; }

        /// <summary>
        /// 客户性质
        /// </summary>
        public CustomerNature CustomerNature { get; set; }

        /// <summary>
        /// 地址类型
        /// </summary>
        public AddressType AddressType { get; set; }

        /// <summary>
        /// 邮寄地址(省)
        /// </summary>
        public string PostProvince { get; set; }

        /// <summary>
        /// 邮寄地址(市)
        /// </summary>
        public string PostCity { get; set; }

        /// <summary>
        /// 邮寄地址(区)
        /// </summary>
        public string PostArea { get; set; }

        /// <summary>
        /// 邮寄地址(详细)
        /// </summary>
        public string PostAddress { get; set; }

        /// <summary>
        /// 邮寄时间类型
        /// </summary>
        public PostTimeType PostTimeType { get; set; }

        /// <summary>
        /// 邮寄时间
        /// </summary>
        public PostTime PostTime { get; set; }

        /// <summary>
        /// 收件人
        /// </summary>
        public string Recipients { get; set; }

        /// <summary>
        /// 收件人手机
        /// </summary>
        public string RecipientsMobile { get; set; }

        /// <summary>
        /// 邮编
        /// </summary>
        public string PostCode { get; set; }

        /// <summary>
        /// 客户信息
        /// </summary>
        public IList<Customer> Customers { get; set; }
    }


    public class CustomerDetail : Customer
    {
        /// <summary>
        /// 案件编号
        /// </summary>
        public string CaseId { get; set; }

        /// <summary>
        /// 物业地址
        /// </summary>
        public string TenementAddress { get; set; }

        /// <summary>
        /// 是否需发送短信
        /// </summary>
        public bool IsSendSms { get; set; }
    }

    public class Buyer : CustomerBaseDto
    {
        /// <summary>
        /// 是否贷款
        /// </summary>
        public bool? IsCredit { get; set; }

        /// <summary>
        /// 贷款类型
        /// </summary>
        public CreditType CreditType { get; set; }

        /// <summary>
        ///自办贷款
        /// </summary>
        public bool? SelfCredit { get; set; }

        /// <summary>
        /// 自贷原因
        /// </summary>
        public SelfCreditReson SelfCreditReson { get; set; }
    }

    public class Seller : CustomerBaseDto 
    {
        /// <summary>
        /// 是否有抵押
        /// </summary>
        public bool? IsGuaranty { get; set; }

        /// <summary>
        /// 是否自行预约还款
        /// </summary>
        public bool? IsSelfRefund { get; set; }

        /// <summary>
        /// 还贷银行
        /// </summary>
        public string RepayCreditBank { get; set; }

        /// <summary>
        /// 还贷金额
        /// </summary>
        public decimal RepayCreditAmount { get; set; }
    }
}
