﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.DataTransferObjects
{
    public class User : BaseDataTransferObject
    {
        /// <summary>
        /// 用户业务编号
        /// </summary>
        public string UserId { get; set; }

        #region 基本属性
        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        /// 证件类型
        /// </summary>
        public CertificateType CertificateType { get; set; }

        /// <summary>
        /// 证件号码
        /// </summary>
        public string IdentityNo { get; set; }

        /// <summary>
        /// 指纹数量
        /// </summary>
        public int FingerprintCount { get; set; }
        #endregion

        /// <summary>
        /// 移动电话
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// 固定电话
        /// </summary>
        public string TelPhone { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }
    }

    public class AgentStaff : User
    {
        public AgentStaff() 
        {
            //base.UserType = Enumerations.UserType.AgentStaff;
        }

        /// <summary>
        /// 用户系统编号
        /// </summary>
        public long UserSysNo { get; set; }

        /// <summary>
        /// 所属中介公司系统编号
        /// </summary>
        public long AgentCompanySysNo { get; set; }

        /// <summary>
        /// 是否是管理员
        /// </summary>
        public bool IsManager { get; set; }

        /// <summary>
        /// 手机
        /// </summary>
        public string Mobile1 { get; set; }
    }

    public class AgentStaffQueryInfo : AgentStaff 
    {
        public string CompanyName { get; set; }

        public string CompanyAddress { get; set; }
    }

    public class Customer : User
    {
        public Customer() 
        {
            //base.UserType = Enumerations.UserType.Customer;
        }

        /// <summary>
        /// 用户系统编号
        /// </summary>
        public long UserSysNo { get; set; }

        /// <summary>
        /// 客户类型
        /// </summary>
        public CustomerType CustomerType { get; set; }

        /// <summary>
        /// 国籍
        /// </summary>
        public string Nationality { get; set; }

        /// <summary>
        /// 是否已有微信关注
        /// </summary>
        public bool IsConcern { get; set; }

    }

    public class ThirdpartyUser : User
    {
        public ThirdpartyUser() 
        {
            //base.UserType = Enumerations.UserType.ThirdpartyUser;
        }

        /// <summary>
        /// 用户系统编号
        /// </summary>
        public long UserSysNo { get; set; }

        /// <summary>
        /// 所属第三方机构名称
        /// </summary>
        public string TpName { get; set; }
    }

    public class PinganStaff : User
    {
        public PinganStaff() 
        {
            //base.UserType = Enumerations.UserType.PinganStaff;
        }

        /// <summary>
        /// 用户系统编号
        /// </summary>
        public long UserSysNo { get; set; }

        /// <summary>
        /// UM帐号
        /// </summary>
        public string UMCode { get; set; }

        /// <summary>
        /// 平安工号
        /// </summary>
        public string StaffNo { get; set; }

        /// <summary>
        /// 所属受理中心
        /// </summary>
        public ReceptionCenter ReceptionCenter { get; set; }

        /// <summary>
        /// 岗位
        /// </summary>
        public Duty Duty { get; set; }

        /// <summary>
        /// 平安工卡编号
        /// </summary>
        public string StaffCardNo { get; set; }
    }
}
