﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Core
{
    [ServiceContract]
    public interface IAgentService
    {
        /// <summary>
        /// 创建中介公司
        /// </summary>
        /// <param name="agentSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult AgentJoint(long agentSysNo, long operatorSysNo);

        /// <summary>
        /// 创建中介公司
        /// </summary>
        /// <param name="agentCompany"></param>
        /// <returns></returns>
        [OperationContract(Name = "CreateAgentCompany")]
        OperationResult AgentJoint(AgentCompany agentCompany);

        /// <summary>
        /// 中介公司解约
        /// </summary>
        /// <param name="agentSysNo"></param>
        /// <param name="cancelReson"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult AgentCancel(long agentSysNo, string cancelReson, long operatorSysNo);

        /// <summary>
        /// 根据系统编号获取中介公司信息
        /// </summary>
        /// <param name="agenSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        AgentCompany GetAgentCompany(long agentSysNo);

        /// <summary>
        /// 获取中介公司列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="condition"></param>
        /// <param name="matchingType"></param>
        /// <returns></returns>
        [OperationContract]
        IList<AgentCompany> GetAgentCompanyList(string condition, AgentJointStatus? status, int pageIndex, int pageSize, out int totalCount, ConditionMatchingType matchingType = ConditionMatchingType.FullFuzzy);

        /// <summary>
        /// 修改中介公司信息
        /// </summary>
        /// <param name="companySysNo"></param>
        /// <param name="companyName"></param>
        /// <param name="principal"></param>
        /// <param name="address"></param>
        /// <param name="contactInfo"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult ModifyCompanyInfo(long companySysNo, string companyName, string principal, string address, string contactInfo, long operatorSysNo);
    }
}
