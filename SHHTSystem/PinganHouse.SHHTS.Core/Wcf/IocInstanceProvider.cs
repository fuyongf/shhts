﻿using HTB.DevFx;
using System;
using System.ServiceModel;
using System.ServiceModel.Dispatcher;

namespace PinganHouse.SHHTS.Core.Wcf
{
    public class IocInstanceProvider : IInstanceProvider
    {
        Type _serviceType;
        string _serviceName;
        public IocInstanceProvider(Type serviceType, string serviceName)
        {
            _serviceType = serviceType;
            _serviceName = serviceName;
        }

        public object GetInstance(InstanceContext instanceContext, System.ServiceModel.Channels.Message message)
        {
            return ObjectService.GetObject(_serviceName);
        }

        public object GetInstance(InstanceContext instanceContext)
        {
            return GetInstance(instanceContext, null);
        }

        public void ReleaseInstance(System.ServiceModel.InstanceContext instanceContext, object instance)
        {
            if (instance is IDisposable)
                ((IDisposable)instance).Dispose();
        }
    }
}
