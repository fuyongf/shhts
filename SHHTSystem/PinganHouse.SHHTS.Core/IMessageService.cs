﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Core
{
    public interface IMessageService
    {
        /// <summary>
        /// 发送付账成功消息
        /// </summary>
        /// <param name="trade"></param>
        /// <param name="order"></param>
        /// <param name="account"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        OperationResult SendPaymentMessage(string tradeNo, PaymentStatus status);

        /// <summary>
        /// 发送收款成功消息
        /// </summary>
        /// <param name="trade"></param>
        /// <param name="order"></param>
        /// <param name="account"></param>
        /// <param name="status"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        OperationResult SendCollectionMessage(string tradeNo, ReconciliationStatus status, decimal amount);

        /// <summary>
        /// 发送过户提醒消息
        /// </summary>
        /// <param name="caseEvent"></param>
        /// <param name="bookTime"></param>
        /// <returns></returns>
        OperationResult SendTransferRemindMessage(CaseEventInfo caseEvent, DateTime bookTime);

        /// <summary>
        /// 发送月度账单
        /// </summary>
        /// <param name="companys"></param>
        void SendMonthBillMessage(IList<AgentCompanyInfo> companys);

        /// <summary>
        /// 发送支付电商服务费短信
        /// </summary>
        /// <param name="trade"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        OperationResult SendExpendSmsMessage(List<OrderInfo> orders);
    }
}
