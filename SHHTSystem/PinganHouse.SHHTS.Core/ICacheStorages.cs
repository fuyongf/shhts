﻿using PinganHouse.SHHTS.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.Core
{
    public interface ICacheStorages
    {

        /// <summary>
        /// 以健值方式获取/设置缓存项（值）
        /// </summary>
        /// <param name="key">缓存项的健值</param>
        /// <remarks>
        /// </remarks>
        object this[string key] { get; set; }

        /// <summary>
        /// 设置缓存项（值）
        /// </summary>
        /// <param name="key"></param>
        /// <param name="expireTime"></param>
        object this[string key, TimeSpan expireTime] { set; }

        /// <summary>
        /// 添加/更新项到缓存
        /// 永久
        /// </summary>
        /// <param name="key">缓存项的健值</param>
        /// <param name="value">缓存的对象</param>
        bool Upset<T>(string key, T value);

        /// <summary>
        /// 添加/更新到缓存
        /// </summary>
        /// <param name="key">缓存项的健值</param>
        /// <param name="value">缓存的对象</param>
        /// <expireTime>
        /// 过期时间 如未设置则永不过期
        /// </expireTime>
        bool Upset<T>(string key, T value, TimeSpan expireTime);

        /// <summary>
        /// 获取未过期的缓存项
        /// </summary>
        /// <param name="key">缓存项的健值</param>
        /// <returns>缓存的对象，如果缓存中没有命中，则返回<c>null</c></returns>
        T Get<T>(string key);

        /// <summary>
        /// 尝试获取缓存项，如果存在则返回<c>true</c>
        /// </summary>
        /// <param name="key">缓存项的健值</param>
        /// <param name="value">>缓存的对象，如果缓存中没有命中，则返回<c>null</c></param>
        /// <returns>如果存在则返回<c>true</c></returns>
        bool TryGet<T>(string key, out T value);

        /// <summary>
        /// 移除缓存项
        /// </summary>
        /// <param name="key">缓存项的健值</param>
        bool Remove(string key);
    }
}
