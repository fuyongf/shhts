﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Core
{
    public interface IMailService
    {
        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="message">消息内容</param>
        /// <param name="sysnc">是否同步</param>
        /// <returns></returns>
        OperationResult Send(Message message, bool sysnc = true);
    }
}
