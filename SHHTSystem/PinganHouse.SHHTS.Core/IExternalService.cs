﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.DataTransferObjects.External;

namespace PinganHouse.SHHTS.Core
{
    public interface IExternalService
    {
        /// <summary>
        /// 提交收款交易
        /// </summary>
        /// <returns></returns>
        OperationResult SubmitCollection(string tradeNo, string serialNo, string caseId, string orderNo, decimal orderAmount, int channel, string tradeType, string payer, string payerAccount, decimal tradeAmount, string bankName, DateTime tradeDate, string terminalNo, string store, string pAddr);

        /// <summary>
        /// 取消收款交易
        /// </summary>
        /// <param name="tradeNo"></param>
        /// <returns></returns>
        OperationResult CancelCollection(string tradeNo);

        /// <summary>
        /// 发送收据编号
        /// </summary>
        /// <param name="tradeNo"></param>
        /// <param name="billNo"></param>
        /// <returns></returns>
        OperationResult SendReceipt(string tradeNo, string billNo);

        /// <summary>
        /// 更新物业地址
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="address"></param>
        /// <returns></returns>
        OperationResult UpdatePropertyAddress(string caseId,string address);

        /// <summary>
        /// 提交代付
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="tradeNo"></param>
        /// <param name="pAddr"></param>
        /// <param name="amount"></param>
        /// <param name="bankCode"></param>
        /// <param name="accountNo"></param>
        /// <param name="payee"></param>
        /// <param name="tradeType"></param>
        /// <returns></returns>
        OperationResult SubmitPayment(string caseId, string tradeNo, string pAddr, decimal amount, string bankCode, string accountNo, string payee, string tradeType);

        /// <summary>
        /// 提交销帐
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="tradeNo"></param>
        /// <param name="pAddr"></param>
        /// <param name="amount"></param>
        /// <param name="bankCode"></param>
        /// <param name="accountNo"></param>
        /// <param name="payee"></param>
        /// <param name="tradeType"></param>
        /// <returns></returns>
        OperationResult SubmitPaidOff(string caseId, string tradeNo, string pAddr, decimal amount, string bankName, string accountNo, string payee, string tradeType);

        /// <summary>
        /// 获取银行列表
        /// </summary>
        /// <returns></returns>
        IList<FsBankInfo> GetBanks();

        /// <summary>
        /// 验证银行卡信息
        /// </summary>
        /// <param name="name"></param>
        /// <param name="idcard"></param>
        /// <param name="cellphone"></param>
        /// <param name="cardnumber"></param>
        /// <param name="bankshort"></param>
        /// <returns></returns>
        OperationResult AuthenticateBankCard(string name, string idcard, string cellphone, string cardnumber, string bankshort);

        /// <summary>
        /// 获取支持鉴权银行列表
        /// </summary>
        /// <returns></returns>
        IList<FsIdentifyBankInfo> GetIdentifyBanks();

        /// <summary>
        /// 发送短信
        /// </summary>
        /// <param name="bcode"></param>
        /// <param name="mobile"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        OperationResult SendSMS(string bcode, string mobile, IDictionary<string, object> parameters);
    }
}
