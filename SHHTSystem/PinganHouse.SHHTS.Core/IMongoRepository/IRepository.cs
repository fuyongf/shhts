﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;
using MongoDB.Bson;

namespace PinganHouse.SHHTS.Core.IMongoRepository
{
    /// <summary>
    /// MongoDb普通集合封装类
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// 覆盖已存在的文档
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        bool Save<T>(T item) where T : class,new();
        bool Save<T>(string collectionName, T item) where T : class, new();
        /// <summary>
        /// 批量插入
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items">The documents to insert.</param>
        void InsertBatch<T>(IEnumerable<T> items) where T : class, new();
        /// <summary>
        /// 批量插入
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items">The documents to insert.</param>
        /// <param name="collectionName">collectionName</param>
        /// <param name="options">The options to use for this Insert</param>
        void InsertBatch<T>(IEnumerable<T> items, string collectionName, MongoInsertOptions options) where T : class, new();
        /// <summary>
        /// 添加未存在的文档
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        /// <param name="safeMode">安全模式</param>
        bool Add<T>(T item, bool safeMode=false) where T : class, new();
        /// <summary>
        /// 添加未存在的文档
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        /// <param name="collectionName">集合名</param>
        /// <returns></returns>
        bool Add<T>(string collectionName, T item) where T : class, new();
        /// <summary>
        /// 更新文档 
        /// Updates one matching document in this collection.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="update"></param>
        bool Update<T>(IMongoQuery query, IMongoUpdate update) where T : class, new();
        /// <summary>
        /// 更新文档
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="update"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        bool Update<T>(IMongoQuery query, IMongoUpdate update, UpdateFlags flags) where T : class, new();
        /// <summary>
        /// 查找并修改
        /// </summary>
        /// <typeparam name="T">需要修改的文档类型</typeparam>
        /// <param name="args"></param>
        /// <returns></returns>
        T FindAndModify<T>(FindAndModifyArgs args) where T : class, new();
        /// <summary>
        /// 查找并修改
        /// </summary>
        /// <param name="collectionName">集合名称</param>
        /// <param name="args"></param>
        /// <returns></returns>
        T FindAndModify<T>(string collectionName, FindAndModifyArgs args) where T : class, new();
        /// <summary>
        /// 查找符合条件的文档
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <returns>游标，需对集合遍历得到结果</returns>
        MongoCursor<T> Find<T>(IMongoQuery query) where T : class, new();
        /// <summary>
        /// 查找符合条件的文档
        /// </summary>
        /// <param name="collectionName"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        MongoCursor Find(string collectionName, IMongoQuery query);
        /// <summary>
        /// 查找符合条件的文档
        /// </summary>
        /// <typeparam name="T">映射类型</typeparam>
        /// <param name="collectionName">集合名称</param>
        /// <param name="query"></param>
        /// <returns>游标，需对集合遍历得到结果</returns>
        MongoCursor<T> Find<T>(string collectionName,IMongoQuery query) where T :  new();
        /// <summary>
        /// 查找符合条件的文档
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable<T> FindAs<T>(IMongoQuery query) where T : class, new();
        /// <summary>
        /// 根据id查找文档
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        T FindById<T>(string id) where T :  new();
        /// <summary>
        /// 根据id查找文档
        /// </summary>
        /// <typeparam name="T">转换类型</typeparam>
        /// <param name="collectionName">集合名</param>
        /// <param name="id">文档id</param>
        /// <param name="fields">文档字段</param>
        /// <returns>A TDocument (or null if not found).</returns>
        T FindById<T>(string collectionName, string id, IMongoFields fields) where T :  new();
        /// <summary>
        /// 查找文档并返回头一个文档
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        T Single<T>(IMongoQuery query) where T : class, new();
        /// <summary>
        /// 查找全部文档
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        IQueryable<T> All<T>() where T : class, new();
        /// <summary>
        /// 查找全部文档
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collectionName">集合名称</param>
        /// <returns>游标</returns>
        MongoCursor<T> All<T>(string collectionName) where T : class, new();
        /// <summary>
        /// 删除文档
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        bool Delete<T>(string id) where T : class,new();
        /// <summary>
        /// 删除文档
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        bool Delete<T>(IMongoQuery query) where T : class, new();
        /// <summary>
        /// 删除文档
        /// </summary>
        /// <param name="collectionName">集合名</param>
        /// <param name="query"></param>
        /// <returns></returns>
        bool Delete(string collectionName, IMongoQuery query);
        /// <summary>
        ///  找到并删除文档
        /// </summary>
        /// <param name="collectName">集合名</param>
        /// <param name="args"></param>
        /// <returns>删除文档</returns>
        BsonDocument FindAndRemove(string collectName, FindAndRemoveArgs args);
        /// <summary>
        /// 找到并删除文档
        /// </summary>
        /// <typeparam name="T">返回类型</typeparam>
        /// <param name="collectName">集合名</param>
        /// <param name="args">查找参数</param>
        /// <returns>删除文档</returns>
        T FindAndRemove<T>(string collectName, FindAndRemoveArgs args) where T : class, new();
        /// <summary>
        /// 删除全部
        /// </summary>
        /// <typeparam name="T"></typeparam>
        //void DeleteAll<T>() where T : class, new();

    }

}
