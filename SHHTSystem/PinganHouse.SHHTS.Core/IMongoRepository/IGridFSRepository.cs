﻿using MongoDB.Driver;
using PinganHouse.SHHTS.Entities;
using System.Collections.Generic;
using System.IO;

namespace PinganHouse.SHHTS.Core.IMongoRepository
{
    /// <summary>
    /// MongoGridFileSystem
    /// </summary>
    public interface IGridFSRepository
    {

        /// <summary>
        /// 保存文件对象
        /// </summary>
        /// <param name="stream">文件流</param>
        /// <param name="remoteFileName">文件名</param>
        /// <returns></returns>
        GridFSFileInfo Save(Stream stream, string remoteFileName);

        /// <summary>
        /// 修改已存储的文件
        /// </summary>
        /// <param name="id">需修改的存储文件id</param>
        /// <param name="stream">更新文件流</param>
        /// <param name="remoteFileName">更新文件名</param>
        /// <returns></returns>
        //GridFSFileInfo Update(string id, Stream stream, string remoteFileName);

        /// <summary>
        /// 根据文件名查找文件
        /// </summary>
        /// <param name="remotFileName">文件名</param>
        /// <returns>文件列表</returns>
        IEnumerable<GridFSFileInfo> FindByName(string remotFileName);

        IEnumerable<GridFSFileInfo> Find(IMongoQuery query);

        /// <summary>
        /// 根据唯一id查找文件
        /// </summary>
        /// <param name="objectID">key</param>
        /// <returns>文件</returns>
        GridFSFileInfo FindByID(string objectID);

        /// <summary>
        /// 根据文件名查找最新文件
        /// </summary>
        /// <param name="remotFileName">文件名</param>
        /// <param name="version">The version to find (1 is oldest, -1 is newest, 0 is no sort).</param>
        /// <returns>文件</returns>
        GridFSFileInfo FindOne(string remotFileName, int version = -1);

        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="obectID">唯一id</param>
        void DeleteByID(string obectID);

        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="remoteFileName">文件名（同一文件名可能有多个文件）</param>
        void DeleteByName(string remoteFileName);

        /// <summary>
        /// 是否存在
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        bool Exists(string id);

        /// <summary>
        /// 是否存在
        /// </summary>
        /// <param name="remoteFileName">文件名</param>
        /// <returns></returns>
        bool ExistsByName(string remoteFileName);

        /// <summary>
        /// 打开指定文件的流
        /// </summary>
        /// <param name="id"></param>
        /// <param name="mode"></param>
        /// <param name="access"></param>
        /// <returns></returns>
        Stream OpenStream(string id, FileMode mode, FileAccess access);

        //void Download(Stream stream, string fileName);

    }
}
