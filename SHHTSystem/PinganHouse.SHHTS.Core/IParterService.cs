﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.DataTransferObjects.External;
using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Web;

namespace PinganHouse.SHHTS.Core
{
    [ServiceContract]
    public interface IParterService
    {
        /// <summary>
        /// 接收微信推送消息
        /// </summary>
        /// <param name="ctx"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult AcceptMessage(WXMessage message);

        /// <summary>
        /// 接收企业应用推送消息
        /// </summary>
        /// <param name="ctx"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult AcceptQYMessage(QYWXMessage message);

        /// <summary>
        /// 生成客户二维码
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        string GetQrCodeUrl(long userSysNo);

        /// <summary>
        /// 生成永久二维码
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract(Name = "GetPermanentQrCodeUrl")]
        string GetQrCodeUrl(string param);

        /// <summary>
        /// 获取auth2请求地址
        /// </summary>
        /// <param name="redirectUrl">回调地址</param>
        /// <param name="state">
        /// 回调会传递回来的参数
        /// a-zA-Z0-9的参数值，最多128字节
        /// </param>
        /// <returns></returns>
        [OperationContract]
        OperationResult GetAuthRequestUrl(string redirectUrl, string state, string appNo);

        /// <summary>
        /// 验证微信获取绑定系统用户
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult ValidateAuth2CallResult(string code);

        /// <summary>
        /// 验证微信获取绑定系统用户
        /// </summary>
        /// <param name="code"></param>
        /// <param name="agentid">企业应用id</param>
        /// <returns></returns>
        [OperationContract(Name = "ValidateQYAuth2CallResult")]
        OperationResult ValidateAuth2CallResult(string code,  int agentid);

        /// <summary>
        /// 检测用户
        /// </summary>
        /// <param name="openId">用户唯一标识</param>
        /// <param name="token">服务端分配的key</param>
        /// <param name="identityNo">证件号</param>
        /// <param name="certificateType">证件类型</param>
        /// <param name="captcha">验证码</param>
        /// <returns>if is success， the mobile's value is OtherData["Mobile"] and userId's value is OtherData["UserId"]</returns>
        [OperationContract]
        OperationResult CheckUser(string openId, string identityNo, CertificateType certificateType, string captcha, string token);

        /// <summary>
        /// 发送短信验证码
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="mobile"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendBindCaptcha(string userId, string mobile);

        /// <summary>
        /// 绑定用户
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="userId"></param>
        /// <param name="captcha"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult BindUser(string openId, string userId, string captcha, string token);

        /// <summary>
        /// 解绑
        /// </summary>
        /// <param name="UserSysNo">用户系统编号</param>
        /// <returns></returns>
        [OperationContract]
        OperationResult RelieveBind(long userSysNo, long? modifyUserSysNo);
        
        /// <summary>
        /// 创建企业号部门
        /// </summary>
        /// <param name="name">部门名称</param>
        /// <param name="parentId">
        /// 上级部门id
        /// 如是顶级节点， parentId为1</param>
        /// <param name="id">
        /// 部门id 整形
        /// 如未指定，由系统自动生成
        /// </param>
        /// <param name="order">
        /// 排序顺序
        /// order值小的排序靠前
        /// </param>
        /// <returns></returns>
        [OperationContract]
        OperationResult CreateDepartment(string name, int parentId, int order, int? id=null);

        /// <summary>
        /// 创建企业号成员
        /// </summary>
        /// <param name="userId">
        /// 成员UserID。
        /// 对应管理端的帐号，企业内必须唯一。长度为1~64个字符
        /// </param>
        /// <param name="name">
        /// 成员名称。长度为1~64个字符
        /// </param>
        /// <param name="mobile">
        /// 手机号码。企业内必须唯一，
        /// mobile/weixinid/email三者不能同时为空
        /// </param>
        /// <param name="email">
        /// 邮箱。长度为0~64个字符。企业内必须唯一
        /// </param>
        /// <param name="weixinid">
        /// 微信号。企业内必须唯一。（注意：是微信号，不是微信的名字）
        /// </param>
        /// <param name="position">
        /// 职位信息。长度为0~64个字符
        /// </param>
        /// <param name="departmentId">
        /// 成员所属部门id列表
        /// </param>
        /// <returns></returns>
        [OperationContract]
        OperationResult CreateUser(string userId, string name, string mobile = null, string email = null, string weixinid = null, string position = null, IEnumerable<string> departmentIds = null, string identityNo = null);


        /// <summary>
        /// 更改部门
        /// </summary>
        /// <param name="id">部门id</param>
        /// <param name="name"></param>
        /// <param name="parentId"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult UpdateDepartment(string id, string name=null, string parentId=null, int? order=null);

        /// <summary>
        /// 删除部门
        /// 不能删除根部门；
        /// 不能删除含有子部门、成员的部门
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult DeleteDepartment(string id);

        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [OperationContract]
        IList<WXQYDepartment> GetDepartments(string id=null);

        /// <summary>
        /// 更新成员
        /// 如果字段未指定，则不更新该字段之前的设置值
        /// </summary>
        /// <param name="userId">必需</param>
        /// <param name="name"></param>
        /// <param name="mobile"></param>
        /// <param name="email"></param>
        /// <param name="weixinid"></param>
        /// <param name="position"></param>
        /// <param name="enable">
        /// 启用/禁用成员。
        /// 1表示启用成员，0表示禁用成员
        /// </param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult UpdateUser(string userId, string name=null, string mobile=null, string email=null, string weixinid=null, string position=null, bool? enable=null, params string[] departmentId);

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="userIds"></param>
        /// <returns></returns>
        [OperationContract(Name = "BatchDeleteUser")]
        OperationResult DeleteUser(string[] userIds);

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="userIds"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult DeleteUser(string userId);

        /// <summary>
        /// 获取微信企业号成员
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [OperationContract]
        WXQYUser GetUser(string userId);

        /// <summary>
        /// 获取部门内成员
        /// </summary>
        /// <param name="departmentId">部门id</param>
        /// <param name="fetchChild">
        /// 是否递归获取子部门下面的成员
        /// 默认不获取子部门
        /// </param>
        /// <param name="status"></param>
        /// <returns></returns>
        [OperationContract]
        IList<WXQYUser> GetUserByDepartment(string departmentId, bool fetchChild=false, WXQYUserStatus status= WXQYUserStatus.All);

        /// <summary>
        /// 邀请成员关注
        /// </summary>
        /// <param name="userId">成员id</param>
        /// <param name="invateContent">邀请内容</param>
        /// <returns></returns>
        [OperationContract]
        OperationResult InviteUserSubscribe(string userId, string invateContent=null);

        /// <summary>
        /// 企业号发送文本消息
        /// </summary>
        /// <param name="agentType">应用类型</param>
        /// <param name="content">发送内容</param>
        /// <param name="receiverType">收件类型</param>
        /// <param name="toAddresses">收件人标识</param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SendTextMessage(AgentType agentType, string content, ReceiverType receiverType, params string[] toAddresses);


        /// <summary>
        /// 企业号向关注该企业应用的全部成员发送文本消息
        /// </summary>
        /// <param name="agentType">应用类型</param>
        /// <param name="content">发送内容</param>
        /// <returns></returns>
        [OperationContract(Name = "SendTextMessageToAll")]
        OperationResult SendTextMessage(AgentType agentType, string content);


        /// <summary>
        /// 创建微信菜单
        /// </summary>
        /// <param name="menus"></param>
        /// <param name="agentType">企业号应用id</param>
        /// <returns></returns>
        [OperationContract]
        OperationResult CreateMenus(IList<Dictionary<string, object>> menus, AgentType? agentType = null);

        /// <summary>
        /// 删除菜单
        /// </summary>
        /// <param name="agentType">企业号应用</param>
        /// <returns></returns>
        OperationResult DeleteMenus(AgentType? agentType = null);

        /// <summary>
        /// 更新accessToken
        /// </summary>
        OperationResult RefreshAccessToken(AgentType? agentType=null);

        /// <summary>
        /// 发送模板消息
        /// </summary>
        /// <returns></returns>
        OperationResult SendTemplateMessage(long customerSysNo, string templateId, IDictionary<string, object> data);


        /// <summary>
        /// 上传素材
        /// </summary>
        /// <param name="name"></param>
        /// <param name="fileName"></param>
        /// <param name="fs"></param>
        /// <returns></returns>
        OperationResult UploadMaterial(string name, string fileName, FileStream fs);

        OperationResult SendImageMessage(AgentType agentType, string mediaId);

        /// <summary>
        /// 发送文本消息
        /// </summary>
        /// <param name="touser"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        OperationResult SendCustomerTextMessage(string touser, string message);
    }
}
