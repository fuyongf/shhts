﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.DataTransferObjects;

namespace PinganHouse.SHHTS.Core
{
    [ServiceContract]
    public interface IAccountService
    {
        /// <summary>
        /// 获取账户余额
        /// </summary>
        /// <param name="accountType">账户类型</param>
        /// <param name="accountSubjectId">账户主体编号</param>
        /// <returns></returns>
        [OperationContract]
        decimal GetBalance(AccountType accountType, string accountSubjectId);

        /// <summary>
        /// 获取账户信息
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        [OperationContract]
        Account Get(long sysNo);

        /// <summary>
        /// 根据类型获取账户信息
        /// </summary>
        /// <param name="type"></param>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        [OperationContract(Name = "GetAccount")]
        Account Get(AccountType type, string subjectId);

        /// <summary>
        /// 获取资金台帐
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="center"></param>
        /// <param name="caseId"></param>
        /// <param name="tenementNo"></param>
        /// <param name="tenementAddr"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [OperationContract]
        IList<AccountLedger> GetAccounts(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, string caseId, string tenementNo, string tenementAddr, DateTime? startDate, DateTime? endDate);

        /// <summary>
        /// 获取案件上下家收付总额
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        [OperationContract]
        Tuple<IDictionary<OrderType, decimal>, Dictionary<OrderType, decimal>> GetCaseTradeAmount(string caseId);

        /// <summary>
        /// 获取账户账单
        /// </summary>
        /// <param name="accountType"></param>
        /// <param name="subjectId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [OperationContract]
        AccountBill GetAccountBill(AccountType accountType, string subjectId, DateTime? startDate, DateTime? endDate);

        #region 银行卡鉴权
        /// <summary>
        /// 银行卡鉴权
        /// </summary>
        /// <param name="bankCode"></param>
        /// <param name="accountNo"></param>
        /// <param name="name"></param>
        /// <param name="identityNo"></param>
        /// <param name="mobile"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult AuthenticateBankCard(string caseId, string bankCode, string cardNo, string name, string identityNo, string mobile, long operatorSysNo);

        /// <summary>
        /// 获取银行卡列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="accountNo"></param>
        /// <param name="accountName"></param>
        /// <param name="identityNo"></param>
        /// <param name="mobile"></param>
        /// <param name="isAuth"></param>
        /// <returns></returns>
        //[OperationContract]
        //IList<AuthedBankCard> GetAuthedBankCards(int pageIndex, int pageSize, out int totalCount, string caseId, string cardNo, string accountName, string identityNo, string mobile);

        /// <summary>
        /// 根据案件获取已鉴权的银行卡信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        [OperationContract]
        AuthedBankCard GetAuthedBankCardByCase(string caseId);

        /// <summary>
        /// 注销已鉴权银行卡信息
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult DeleteAuthedBankCard(long sysNo, long operatorSysNo);
        #endregion
    }
}
