﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.DataTransferObjects;

namespace PinganHouse.SHHTS.Core
{
    /// <summary>
    /// 用户认证服务
    /// </summary>
    public interface IUserAuthProvider
    {
        /// <summary>
        /// 用户认证
        /// </summary>
        /// <param name="loginId"></param>
        /// <param name="password"></param>
        /// <param name="userType"></param>
        /// <returns></returns>
        OperationResult Authenticate(string loginId, string password);
    }
}
