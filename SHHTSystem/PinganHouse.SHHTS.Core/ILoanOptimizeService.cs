﻿using PinganHouse.SHHTS.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace PinganHouse.SHHTS.Core
{
    /// <summary>
    /// 根据用户输入计算经过优选的贷款方案
    /// </summary>
    [ServiceContract]
    public interface ILoanOptimizeService
    {
        /// <summary>
        /// 获取全部可行方案.
        /// </summary>
        /// <param name="ips">用户输入数据</param>
        /// <returns></returns>
        [OperationContract]
        IList<LoanOptimizePlan> GetPlans(OInputs ips);
    }
}
