﻿using PinganHouse.SHHTS.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace PinganHouse.SHHTS.Core.External
{
    public interface IPartnerAuthenticator
    {
        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="service"></param>
        /// <param name="appSetting"></param>
        void Init(IPartnerAuthenticationService service);

        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        OperationResult SendData(IDictionary<string, object> data);

        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        OperationResult SendData<TResult>(IDictionary<string, object> data);

        /// <summary>
        /// 验证回调结果
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        OperationResult ValidateCallResult(HttpContextBase ctx, IDictionary<string, object> data);
    }
}
