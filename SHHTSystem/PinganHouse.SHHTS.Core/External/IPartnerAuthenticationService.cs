﻿using PinganHouse.SHHTS.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace PinganHouse.SHHTS.Core.External
{
    public interface IPartnerAuthenticationService
    {
        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        OperationResult SendData(string appNo, IDictionary<string, object> data);

        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        OperationResult SendData<TResult>(string appNo, IDictionary<string, object> data);

        /// <summary>
        /// 验证回调结果
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        OperationResult ValidateCallResult(string appNo, HttpContextBase ctx, IDictionary<string, object> data);

    }
}
