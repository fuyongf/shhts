﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using HTB.DevFx;

using PinganHouse.SHHTS.Core.DataAccess;
using PinganHouse.SHHTS.Core.External;

namespace PinganHouse.SHHTS.Core
{
    public class ServiceDepository
    {
        #region Service
        public static ISettingService SettingService 
        {
            get 
            {
                return ObjectService.GetObject<ISettingService>();
            }
        }

        public static IConfigService ConfigService
        {
            get
            {
                return ObjectService.GetObject<IConfigService>();
            }
        }

        public static IUserService UserService
        {
            get
            {
                return ObjectService.GetObject<IUserService>();
            }
        }

        public static IAgentService AgentService
        {
            get
            {
                return ObjectService.GetObject<IAgentService>();
            }
        }

        public static ICaseService CaseService
        {
            get
            {
                return ObjectService.GetObject<ICaseService>();
            }
        }

        public static IAttachmentService AttachmentService
        {
            get
            {
                return ObjectService.GetObject<IAttachmentService>();
            }
        }

        public static IFileTransferService FileTransferService
        {
            get
            {
                return ObjectService.GetObject<IFileTransferService>();
            }
        }

        public static ICustomerFundFlowService CustomerFundFlowService
        {
            get
            {
                return ObjectService.GetObject<ICustomerFundFlowService>();
            }
        }

        public static ITransactionService TransactionService 
        {
            get 
            {
                return ObjectService.GetObject<ITransactionService>();
            }
        }

        public static IPropertySurveyService PropertySurveyService
        {
            get
            {
                return ObjectService.GetObject<IPropertySurveyService>();
            }
        }

        public static IAccountService AccountService
        {
            get
            {
                return ObjectService.GetObject<IAccountService>();
            }
        }

        public static ITradeService TradeService
        {
            get
            {
                return ObjectService.GetObject<ITradeService>();
            }
        }

        public static IPartnerAuthenticationService PartnerService
        {
            get 
            {
                return ObjectService.GetObject<IPartnerAuthenticationService>();
            }
        }

        public static IExternalService ExternalService
        {
            get
            {
                return ObjectService.GetObject<IExternalService>();
            }
        }

        public static IParterService ParterChannelService
        {
            get
            {
                return ObjectService.GetObject<IParterService>();
            }
        }

        //public static IFundAccountService FundAccountService
        //{
        //    get
        //    {
        //        return ObjectService.GetObject<IFundAccountService>();
        //    }
        //}

        //public static IOrderService OrderService
        //{
        //    get
        //    {
        //        return ObjectService.GetObject<IOrderService>();
        //    }
        //}

        public static IFingerService FingerService
        {
            get
            {
                return ObjectService.GetObject<IFingerService>();
            }
        }

        public static ILoanOptimizeService LoanOptimService
        {
            get
            {
                return ObjectService.GetObject<ILoanOptimizeService>();
            }
        }

        public static IMessageService MessageService
        {
            get
            {
                return ObjectService.GetObject<IMessageService>();
            }
        }

        public static ILoanOptimizeService LoanOptimizeService
        {
            get
            {
                return ObjectService.GetObject<ILoanOptimizeService>();
            }
        }


        public static IReviewService ReviewService
        {
            get
            {
                return ObjectService.GetObject<IReviewService>();
            }
        }


        public static ISmsService SmsService
        {
            get
            {
                return ObjectService.GetObject<ISmsService>();
            }
        }

        public static ICacheStorages CacheStorages
        {
            get
            {
                return ObjectService.GetObject<ICacheStorages>();
            }
        }


        public static ICaptchaService CaptchaService
        {
            get
            {
                return ObjectService.GetObject<ICaptchaService>();
            }
        }

        public static INotaryService NotaryService
        {
            get
            {
                return ObjectService.GetObject<INotaryService>();
            }
        }


        #endregion

        #region Data Access
        public static ISettingDataAccess SettingDataAccess 
        {
            get 
            {
                return ObjectService.GetObject<ISettingDataAccess>();
            }
        }

        public static IUserDataAccess UserDataAccess
        {
            get
            {
                return ObjectService.GetObject<IUserDataAccess>();
            }
        }
        public static ICustomerDataAccess CustomerDataAccess
        {
            get
            {
                return ObjectService.GetObject<ICustomerDataAccess>();
            }
        }
        public static IPinganStaffDataAccess PinganStaffDataAccess
        {
            get
            {
                return ObjectService.GetObject<IPinganStaffDataAccess>();
            }
        }
        public static IAgentStaffDataAccess AgentStaffDataAccess
        {
            get
            {
                return ObjectService.GetObject<IAgentStaffDataAccess>();
            }
        }
        public static IThirdpartyUserDataAccess ThirdpartyUserDataAccess
        {
            get
            {
                return ObjectService.GetObject<IThirdpartyUserDataAccess>();
            }
        }
        public static IAgentCompanyDataAccess AgentCompanyDataAccess
        {
            get
            {
                return ObjectService.GetObject<IAgentCompanyDataAccess>();
            }
        }
        public static ICaseDataAccess CaseDataAccess
        {
            get
            {
                return ObjectService.GetObject<ICaseDataAccess>();
            }
        }
        public static IAttachmentDataAccess AttachmentDataAccess
        {
            get
            {
                return ObjectService.GetObject<IAttachmentDataAccess>();
            }
        }
        public static ICustomerFundFlowDataAccess CustomerFundFlowDataAccess
        {
            get
            {
                return ObjectService.GetObject<ICustomerFundFlowDataAccess>();
            }
        }
        public static IUserIdentityDataAccess UserIdentityDataAccess
        {
            get
            {
                return ObjectService.GetObject<IUserIdentityDataAccess>();
            }
        }
        public static ICaseEventDataAccess CaseEventDataAccess
        {
            get
            {
                return ObjectService.GetObject<ICaseEventDataAccess>();
            }
        }
        public static ICustomerBankAccountDataAccess CustomerBankAccountDataAccess 
        {
            get 
            {
                return ObjectService.GetObject<ICustomerBankAccountDataAccess>();
            }
        }
        public static IAccountDataAccess AccountDataAccess
        {
            get
            {
                return ObjectService.GetObject<IAccountDataAccess>();
            }
        }
        public static IAccountDetailDataAccess AccountDetailDataAccess
        {
            get
            {
                return ObjectService.GetObject<IAccountDetailDataAccess>();
            }
        }
        public static IOrderDataAccess OrderDataAccess
        {
            get
            {
                return ObjectService.GetObject<IOrderDataAccess>();
            }
        }
        public static ITradeDataAccess TradeDataAccess
        {
            get
            {
                return ObjectService.GetObject<ITradeDataAccess>();
            }
        }
        public static IBillDataAccess BillDataAccess
        {
            get
            {
                return ObjectService.GetObject<IBillDataAccess>();
            }
        }
        public static IMissCollectionTradeDataAccess MissCollectionTradeDataAccess
        {
            get
            {
                return ObjectService.GetObject<IMissCollectionTradeDataAccess>();
            }
        }
        public static IReceiptDataAccess ReceiptDataAccess
        {
            get
            {
                return ObjectService.GetObject<IReceiptDataAccess>();
            }
        }
        public static IPropertyCertificateDataAccess PropertyCertificateDataAccess
        {
            get
            {
                return ObjectService.GetObject<IPropertyCertificateDataAccess>();
            }
        }
        public static IPropertySurveyDataAccess PropertySurveyDataAccess
        {
            get
            {
                return ObjectService.GetObject<IPropertySurveyDataAccess>();
            }
        }

        public static IParterUserInfoDataAccess ParterUserInfoDataAccess
        {
            get
            {
                return ObjectService.GetObject<IParterUserInfoDataAccess>();
            }
        }

        public static IWXTemplateMessageDataAccess WXTemplateMessageDataAccess
        {
            get
            {
                return ObjectService.GetObject<IWXTemplateMessageDataAccess>();
            }
        }

        public static IBuyerDataAccess BuyerDataAccess 
        {
            get
            {
                return ObjectService.GetObject<IBuyerDataAccess>();
            }
        }
        public static ISellerDataAccess SellerDataAccess 
        {
            get
            {
                return ObjectService.GetObject<ISellerDataAccess>();
            }
        }

        public static IAuditLogDataAccess AuditLogDataAccess
        {
            get
            {
                return ObjectService.GetObject<IAuditLogDataAccess>();
            }
        }

        public static IBankCardAuthDataAccess BankCardAuthDataAccess
        {
            get
            {
                return ObjectService.GetObject<IBankCardAuthDataAccess>();
            }
        }

        //public static IFundAccountDataAccess FundAccountDataAccess
        //{
        //    get
        //    {
        //        return ObjectService.GetObject<IFundAccountDataAccess>();
        //    }
        //}

        public static IFingerprintDataAccess FingerprintDataAccess
        {
            get
            {
                return ObjectService.GetObject<IFingerprintDataAccess>();
            }
        }

        public static IUserAccountDataAccess UserAccountDataAccess
        {
            get
            {
                return ObjectService.GetObject<IUserAccountDataAccess>();
            }
        }

        public static IReviewDataAccess ReviewDataAccess
        {
            get
            {
                return ObjectService.GetObject<IReviewDataAccess>();
            }
        }

        public static IReviewDetailDataAccess ReviewDetailDataAccess
        {
            get
            {
                return ObjectService.GetObject<IReviewDetailDataAccess>();
            }
        }

        public static IDeductScheduleDataAccess DeductScheduleDataAccess
        {
            get
            {
                return ObjectService.GetObject<IDeductScheduleDataAccess>();
            }
        }

        public static IUserExendDataAccess UserExendDataAccess
        {
            get
            {
                return ObjectService.GetObject<IUserExendDataAccess>();
            }
        }

        public static ICacheDataAccess CacheDataAccess
        {
            get
            {
                return ObjectService.GetObject<ICacheDataAccess>();
            }
        }

        public static INotaryDataAccess NotaryDataAccess
        {
            get
            {
                return ObjectService.GetObject<INotaryDataAccess>();
            }
        }

        #endregion
    }
}
