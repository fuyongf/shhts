﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface IBankCardAuthDataAccess : IDataAccess<BankCardAuthInfo>
    {
        bool Exists(string caseId, string cardNo);

        IList<BankCardAuthInfo> GetByCardNo(string cardNo);

        IList<BankCardAuthInfo> GetByCase(string caseId);

        BankCardAuthInfo GetByCardInCase(string caseId, string cardNo);

        IList<BankCardAuthInfo> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, string caseId, string accountNo, string accountName, string identityNo, string mobile);
    }
}
