﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Entities;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface ISellerDataAccess : IDataAccess<SellerInfo>
    {
        /// <summary>
        /// 根据案件获取上家信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        SellerInfo Get(string caseId);
    }
}
