﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface ITradeDataAccess : IDataAccess<TradeInfo>
    {
        /// <summary>
        /// 根据交易流水编号获取
        /// </summary>
        /// <param name="tradeNo"></param>
        /// <returns></returns>
        TradeInfo Get(string tradeNo);

        /// <summary>
        /// 根据订单编号获取
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        IList<TradeInfo> GetByOrder(string orderNo, params string[] tradeNos);

        /// <summary>
        /// 获取交易流水
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="center"></param>
        /// <param name="channel"></param>
        /// <param name="tradeNo"></param>
        /// <param name="caseId"></param>
        /// <param name="bankAccountNo"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        IList<TradeInfo> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, PaymentChannel? channel, string orderNo, string tradeNo, string caseId, string bankAccountNo, DateTime? startDate, DateTime? endDate);

        /// <summary>
        /// 获取交易流水
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="accountType"></param>
        /// <returns></returns>
        IList<TradeInfo> GetTrades(string caseId, AccountType accountType);

        /// <summary>
        /// 获取客户流水列表(for:wechat)
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="accounts"></param>
        /// <returns></returns>
        IList<TradeInfo> GetPaginatedListByAccount(int pageIndex, int pageSize, out int totalCount, bool isSelf, params long[] accounts);

        /// <summary>
        /// 根据案件查询流水
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="caseId"></param>
        /// <param name="orderType"></param>
        /// <returns></returns>
        IList<TradeInfo> GetByCase(int pageIndex, int pageSize, out int totalCount, string caseId, AccountType? accountType, OrderType? orderType);

        #region 收据相关，待废弃

        /// <summary>
        /// 更改出据收据标识
        /// </summary>
        /// <param name="orderNo"></param>
        /// <param name="modifyUserSysNo"></param>
        /// <param name="sysnos"></param>
        /// <returns></returns>
        bool UpdateReceipt(string orderNo, long modifyUserSysNo, params string[] sysnos);

        /// <summary>
        /// 根据订单编号获取可打印收据的流水
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        IList<TradeInfo> GetPrintReceiptTradesByOrder(string orderNo);
        ///// <summary>
        ///// 根据订单号和交易状态获取流水(不对外使用)
        ///// </summary>
        ///// <param name="order"></param>
        ///// <param name="status"></param>
        ///// <returns></returns>
        //IList<TradeInfo> GetByOrder(string order, TradeStatus status);

        /// <summary>
        /// 获取待发送收据到
        /// </summary>
        /// <returns></returns>
        IList<TradeInfo> GetSendBillTrades();

        #endregion  
    }
}
