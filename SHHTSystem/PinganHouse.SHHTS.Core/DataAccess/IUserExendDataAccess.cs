﻿using PinganHouse.SHHTS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface IUserExendDataAccess : IDataAccess<UserExend>
    {
    }
}
