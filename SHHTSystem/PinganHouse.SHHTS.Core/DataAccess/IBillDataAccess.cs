﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface IBillDataAccess : IDataAccess<BillInfo>
    {
        /// <summary>
        /// 根据订单号获取票据
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        IList<BillQueryInfo> GetBillByOrder(string orderNo, BillCategory? category, BillType? type, bool? isBilled = null);
    }
}
