﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface IAuditLogDataAccess : IDataAccess<AuditLogInfo>
    {
        /// <summary>
        /// 获取审核日志
        /// </summary>
        /// <param name="type"></param>
        /// <param name="subject"></param>
        /// <returns></returns>
        IList<AuditLogInfo> Get(AuditObjectType type, string subject);

        /// <summary>
        /// 根据订单获取交易审核日志
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        IList<AuditLogInfo> GetTradeAuditLogByOrder(string orderNo);
    }
}
