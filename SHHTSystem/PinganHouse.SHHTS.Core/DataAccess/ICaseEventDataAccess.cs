﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface ICaseEventDataAccess : IDataAccess<CaseEventInfo>
    {
        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="preStatus">前置状态</param>
        /// <returns></returns>
        [Obsolete]
        bool Insert(CaseEventInfo entity, CaseStatus preStatus);

        /// <summary>
        /// 根据案件编号获取变更日志
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="sourceStatus"></param>
        /// <param name="eventType"></param>
        /// <returns></returns>
        IEnumerable<CaseEventInfo> Get(string caseId, CaseStatus? sourceStatus = null, CaseStatus? eventType = null, bool? finished = null);

        /// <summary>
        /// 根据案件编号获取变更日志
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        IEnumerable<CaseEventInfo> Get(string caseId, int pageIndex, int pageSize, out int totalCount, IEnumerable<CaseStatus> status = null);

        /// <summary>
        /// 获取案件进程信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="eventType"></param>
        /// <returns></returns>
        IEnumerable<CaseEventInfo> GetCaseEvents(string caseId, CaseStatus eventType);

        /// <summary>
        /// 获取案件进程信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="eventType"></param>
        /// <returns></returns>
        IEnumerable<CaseEventInfo> GetCaseEvents(IEnumerable<string> caseIds, CaseStatus eventType);


        /// <summary>
        /// 获取最新的案件事件
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        CaseEventInfo GetNewByCaseId(string caseId, CaseStatus caseStatus);

        /// <summary>
        /// 获取符合条件案件
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="remark"></param>
        /// <returns></returns>
        IEnumerable<CaseEventInfo> GetCaseEvents(CaseStatus eventType);

        /// <summary>
        /// 更改备注
        /// </summary>
        /// <param name="sysno"></param>
        /// <param name="remark"></param>
        /// <returns></returns>
        bool UpdateRemark(long sysno, string remark);

        ///// <summary>
        ///// 结束案件事件
        ///// </summary>
        ///// <param name="caseId"></param>
        ///// <param name="caseStatus"></param>
        ///// <returns></returns>
        //bool Finish(string caseId, CaseStatus caseStatus);
    }
}
