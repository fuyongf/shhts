﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Entities;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface IUserIdentityDataAccess
    {
        /// <summary>
        /// 新增身份信息
        /// </summary>
        /// <param name="identity"></param>
        /// <returns></returns>
        bool Insert(UserIdentityInfo identity);

        /// <summary>
        /// 修改身份认证信息
        /// </summary>
        /// <param name="identity"></param>
        /// <returns></returns>
        bool Update(UserIdentityInfo identity);

        /// <summary>
        /// 删除身份认证信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Remove(string id);

        /// <summary>
        /// 根据证件获取认证信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        UserIdentityInfo Get(string id);

        /// <summary>
        /// 检查身份认证是否存在
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Exists(string id);
    }

    public interface IFingerprintDataAccess : IDataAccess<UserFingerprintInfo> 
    {
        /// <summary>
        /// 根据用户系统编号获取指纹信息
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        IList<UserFingerprintInfo> GetByUserSysNo(long userSysNo);

        /// <summary>
        /// 获取所有指纹信息
        /// </summary>
        /// <returns></returns>
        IList<UserFingerprintInfo> GetAll();
    }

}
