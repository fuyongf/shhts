﻿using PinganHouse.SHHTS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface IParterUserInfoDataAccess : IDataAccess<ParterUserInfo>
    {
        /// <summary>
        /// 根据openid获取合作用户
        /// </summary>
        /// <param name="openID"></param>
        /// <returns></returns>
        ParterUserInfo Get(string openID, long userSysNo);

        /// <summary>
        /// 根据绑定用户获取合作用户
        /// </summary>
        /// <param name="openID"></param>
        /// <returns></returns>
        IList<ParterUserInfo> GetByUserSysNo(long userSysNo);

        /// <summary>
        /// 根据微信号获取绑定用户数据
        /// </summary>
        /// <param name="openID"></param>
        /// <returns></returns>
        ParterUserInfo Get(string openID);

        /// <summary>
        /// 更改关注状态
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="subscribe"></param>
        /// <returns></returns>
        bool UpdateByOpenId(string openId, bool subscribe);

        /// <summary>
        ///删除关联用户 
        /// </summary>
        /// <param name="openId"></param>
        /// <returns></returns>
        bool DeleteByOpenId(string openId);

        /// <summary>
        /// 解绑
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <param name="modifyUserSysNo"></param>
        /// <returns></returns>
        bool RelieveBind(long userSysNo, long? modifyUserSysNo);
    }
}
