﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface IDataAccess<T> where T : class
    {
        /// <summary>
        /// 根据系统编号获取对象
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        T Get(long sysNo);

        /// <summary>
        /// 添加对象
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Insert(T entity);

        /// <summary>
        /// 修改对象
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Update(T entity);

        /// <summary>
        /// 根据系统编号删除对象
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        bool Delete(long sysNo, long operatorSysNo);

        /// <summary>
        /// 根据系统编号移除对象
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        bool Remove(long sysNo);

        /// <summary>
        /// 指定编号的对象是否存在
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        bool Exists(long sysNo);
    }
}
