﻿using PinganHouse.SHHTS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface IReceiptDataAccess : IDataAccess<ReceiptInfo>
    {
        ReceiptInfo Get(string receiptNo);

        ReceiptSerialInfo GetReceiptSerialInfo(long receiptSynNo);

        IList<ReceiptInfo> GetReceiptSerialInfos(string orderNo);

        IList<ReceiptInfo> GetReceiptByOrderNo(string orderNo);

        bool InsertReceiptSerialInfo(long sysNo, string receiptNo, string receiptSerialNo, string securityCode, string remark, long createUserSysNo);

        int GetReceiptPrintCount(string receiptNo);

        ReceiptSerialInfo GetReceiptSerialBySecurityCode(string securityCode);

        IList<ReceiptInfo> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, ReceiptStatus? status, string receiptNo);
    }
}
