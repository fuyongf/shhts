﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface ICustomerBankAccountDataAccess : IDataAccess<CustomerBankAccountInfo>
    {
        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        bool BatchInsert(IEnumerable<CustomerBankAccountInfo> entities);

        /// <summary>
        /// 根据案件系统编号获取收款帐号
        /// </summary>
        /// <param name="caseSysNo"></param>
        /// <param name="customerType"></param>
        /// <returns></returns>
        IList<CustomerBankAccountInfo> GetByCase(long caseSysNo, CustomerType? customerType = null, BankAccountPurpose? accountType = null);

        /// <summary>
        /// 根据案件编号获取收款帐号
        /// </summary>
        /// <param name="caseSysNo"></param>
        /// <param name="customerType"></param>
        /// <returns></returns>
        IList<CustomerBankAccountInfo> GetByCase(string caseSysNo, CustomerType? customerType = null, BankAccountPurpose? accountType = null);
    }
}
