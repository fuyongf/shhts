﻿using PinganHouse.SHHTS.DataTransferObjects.Filters;
using PinganHouse.SHHTS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface INotaryDataAccess : IDataAccess<Notary>
    {
        IList<Notary> Search(NotaryFilter filter);
    }
}
