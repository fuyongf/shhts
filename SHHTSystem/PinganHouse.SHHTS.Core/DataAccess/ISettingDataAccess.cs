﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Entities;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface ISettingDataAccess
    {
        /// <summary>
        /// 新增配置节点
        /// </summary>
        /// <param name="setting"></param>
        /// <returns></returns>
        bool Insert(Setting setting);

        /// <summary>
        /// 修改节点值
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        bool Update(string id, string value, string desc);

        /// <summary>
        /// 删除配置节点
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Remove(string id);

        /// <summary>
        /// 根据ID获取配置信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Setting Get(string id);

        /// <summary>
        /// 根据ID获取配置值
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        string GetValue(string id);

        /// <summary>
        /// 获取指定ID下级配置节点
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        IEnumerable<Setting> GetItems(string id);

        /// <summary>
        /// 获取指定路径当前节点及下级节点
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        IList<Setting> GetItemsByPath(string path);
    }
}
