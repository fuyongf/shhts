﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface IMissCollectionTradeDataAccess : IDataAccess<MissCollectionTradeInfo>
    {
        /// <summary>
        /// 根据银行流水号获取
        /// </summary>
        /// <param name="serialNo"></param>
        /// <returns></returns>
        MissCollectionTradeInfo GetBySerialNo(string serialNo);

        /// <summary>
        /// 检查流水号是否存在
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="serialNo"></param>
        /// <returns></returns>
        bool Exists(PaymentChannel channel, string serialNo);

        /// <summary>
        ///  获取所有数据
        /// </summary>
        /// <param name="confirmed"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        IList<MissCollectionTradeInfo> Get(string accountNo, PaymentChannel? channel, bool? confirmed, DateTime? startDate, DateTime? endDate);

        /// <summary>
        /// 确认掉单数据
        /// </summary>
        /// <param name="sysNo"></param>
        /// <param name="tradeNo"></param>
        /// <param name="auditorSysNo"></param>
        /// <returns></returns>
        bool Confirm(long sysNo, string tradeNo, long auditorSysNo);
    }
}
