﻿using IBatisNet.DataMapper;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface IAttachmentDataAccess:IDataAccess<AttachmentInfo>
    {

        /// <summary>
        /// 获取附件信息
        /// </summary>
        /// <param name="associationNo">关联业务标识</param>
        /// <param name="attachmentType">附件类型</param>
        /// <returns></returns>
        IList<AttachmentInfo> GetAttachments(string condition, int pageIndex, int pageCount, ref int totalCount, AttachmentType attachmentType= AttachmentType.未知, string orderBy = "[CreateDate] desc", ConditionMatchingType matchingType = ConditionMatchingType.Fuzzy);
        /// <summary>
        /// 事务删除附件信息
        /// </summary>
        /// <param name="associationNo">业务关联标识</param>
        /// <param name="transaction">事务</param>
        void DeleteByAssociationNo(string associationNo, string modifyUserNo);

        /// <summary>
        /// 获取附件信息
        /// </summary>
        /// <param name="associationNos"></param>
        /// <returns></returns>
        IList<AttachmentInfo> GetAttachments(IEnumerable<string> associationNos);

        /// <summary>
        /// 获取附件信息
        /// </summary>
        /// <param name="associationNos"></param>
        /// <returns></returns>
        IList<AttachmentInfo> GetAttachments(string associationNo,  AttachmentType? attachmentType=null);

        /// <summary>
        /// 获取附件信息
        /// </summary>
        /// <param name="associationNo"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        IList<AttachmentInfo> GetAttachments(string associationNo, IEnumerable<AttachmentType> types = null);

        /// <summary>
        ///删除附件关系
        /// </summary>
        /// <param name="fileId"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        bool Delete(string fileId, long operatorSysNo);
    }
}
