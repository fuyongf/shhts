﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Entities;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface IBuyerDataAccess : IDataAccess<BuyerInfo>
    {
        /// <summary>
        /// 根据案件获取下家信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        BuyerInfo Get(string caseId);
    }
}
