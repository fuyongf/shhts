﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface IAccountDataAccess : IDataAccess<AccountInfo>
    {
        AccountInfo Get(long sysNo, OrderBizType bizType);

        /// <summary>
        /// 获取账户信息
        /// </summary>
        /// <param name="subjectId"></param>
        /// <param name="accountType"></param>
        /// <returns></returns>
        AccountInfo Get(string subjectId, AccountType accountType, OrderBizType? orderBizType);

        /// <summary>
        /// 获取用户账户余额
        /// </summary>
        /// <param name="subjectId"></param>
        /// <param name="accountType"></param>
        /// <returns></returns>
        decimal GetBalance(string subjectId, AccountType accountType);

        /// <summary>
        /// 添加操作日志
        /// </summary>
        /// <param name="log"></param>
        /// <returns></returns>
        bool AddOperatorLog(AccountOperationLog log);

        /// <summary>
        /// 获取账户操作日志
        /// </summary>
        /// <param name="accountSysNo"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        IList<AccountOperationLog> GetOperationLogs(long accountSysNo, DateTime? startDate, DateTime? endDate);

        /// <summary>
        /// 获取账户操作日志
        /// </summary>
        /// <param name="subjectId"></param>
        /// <param name="accountType"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        IList<AccountOperationLog> GetOperationLogs(string subjectId, AccountType accountType, DateTime? startDate, DateTime? endDate);

        /// <summary>
        /// 获取资金账户列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="center"></param>
        /// <param name="caseId"></param>
        /// <param name="tenementNo"></param>
        /// <param name="tenementAddr"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        IList<AccountLedgerInfo> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, string caseId, string tenementNo, string tenementAddr, DateTime? startDate, DateTime? endDate);

        /// <summary>
        /// 获取案件上下家收付总额
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        Tuple<IDictionary<OrderType, decimal>, Dictionary<OrderType, decimal>> GetCaseTradeAmount(string caseId);
    }
}
