﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface IDeductScheduleDataAccess : IDataAccess<DeductScheduleInfo>
    {
        bool Delete(long sysNo, long operatorSysNo, long version);
    
        /// <summary>
        /// 获取账户扣款计划
        /// </summary>
        /// <param name="accountSysNo"></param>
        /// <param name="finished"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        IList<DeductScheduleInfo> GetByAccount(long accountSysNo, bool? finished, DateTime? startDate = null, DateTime? endDate = null);

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="center"></param>
        /// <param name="caseId"></param>
        /// <param name="finished"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        IList<DeductScheduleQueryInfo> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, string caseId, bool? finished, DateTime? startDate, DateTime? endDate, string orderBy = "S.[CreateDate] DESC");
    }
}
