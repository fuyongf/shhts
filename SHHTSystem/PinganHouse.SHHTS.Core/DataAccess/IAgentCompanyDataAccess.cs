﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface IAgentCompanyDataAccess : IDataAccess<AgentCompanyInfo>
    {
        bool AgentCancel(long agentSysNo, string cancelReson, long operatorSysNo);

        IList<AgentCompanyInfo> GetPaginatedList(string condition, AgentJointStatus? status, int pageIndex, int pageSize, out int totalCount, string orderBy = "[CompanyName]", ConditionMatchingType matchingType = ConditionMatchingType.FullFuzzy);
    }
}
