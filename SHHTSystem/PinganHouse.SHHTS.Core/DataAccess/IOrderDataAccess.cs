﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Entities;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface IOrderDataAccess : IDataAccess<OrderInfo>
    {
        /// <summary>
        /// 根据订单号获取订单
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        OrderInfo Get(string orderNo);

        /// <summary>
        /// 订单是否存在
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        bool Exists(string orderNo);

        /// <summary>
        /// 获取订单列表
        /// </summary>
        /// <param name="accountType"></param>
        /// <param name="accountSubjectId"></param>
        /// <param name="orderType"></param>
        /// <param name="tradeType"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        IList<OrderInfo> GetOrders(AccountType accountType, string accountSubjectId, OrderType? orderType, OrderBizType? tradeType, DateTime? startDate, DateTime? endDate);

        /// <summary>
        /// 获取账户订单信息
        /// </summary>
        /// <param name="accountSysNo"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        IList<OrderInfo> GetOrderByAccount(long accountSysNo, DateTime? startDate, DateTime? endDate);

        /// <summary>
        ///  获取订单列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="center"></param>
        /// <param name="caseId"></param>
        /// <param name="accountType"></param>
        /// <param name="orderType"></param>
        /// <param name="channel"></param>
        /// <param name="orderNo"></param>
        /// <param name="createUserName"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        IList<OrderQueryInfo> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, string caseId, AccountType? accountType, OrderType? orderType, PaymentChannel? channel, string orderNo, string createUserName, DateTime? startDate, DateTime? endDate, string orderBy = "O.[CreateDate] DESC");

        ///// <summary>
        ///// 获取订单列表
        ///// 用于收据
        ///// </summary>
        ///// <param name="pageIndex"></param>
        ///// <param name="pageSize"></param>
        ///// <param name="totalCount"></param>
        ///// <param name="center"></param>
        ///// <param name="caseId"></param>
        ///// <param name="orderNo"></param>
        ///// <param name="startDate"></param>
        ///// <param name="endDate"></param>
        ///// <param name="data"></param>
        ///// <returns></returns>
        //IList<OrderQueryInfo> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, string caseId = null, string orderNo = null, DateTime? startDate = null, DateTime? endDate = null, IDictionary<string, object> data = null);

        /// <summary>
        /// 获取退款订单列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="center"></param>
        /// <param name="orderNo"></param>
        /// <param name="createUserName"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        IList<RefundOrderQueryInfo> GetRefundPaginatedList(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, string orderNo, string createUserName, DateTime? startDate, DateTime? endDate);

        /// <summary>
        /// 获取客户订单列表(for:wechat)
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="accounts"></param>
        /// <returns></returns>
        IList<OrderQueryInfo> GetPaginatedListByAccount(int pageIndex, int pageSize, out int totalCount, bool isSelf, params long[] accounts);

        /// <summary>
        ///  根据订单状态获取订单（不对外使用）
        /// </summary>
        /// <param name="orderType"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        IList<OrderInfo> GetOrdersByStatus(OrderType orderType, OrderStatus status);

        /// <summary>
        /// 获取订单所属受理中心
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        ReceptionCenter GetReceptionCenterByOrder(string orderNo);

        #region Order Extends
        bool InsertPaymentExtend(PaymentOrderExtend extend);

        PaymentOrderExtend GetPaymentExtend(string orderNo);

        bool InsertPaidOffExtend(PaidOffOrderExtend extend);

        PaidOffOrderExtend GetPaidOffExtend(string orderNo);

        bool InsertTransferExtend(TransferOrderExtend extend);

        TransferOrderExtend GetTransferExtend(string orderNo);

        bool InsertWithdrawExtend(WithdrawOrderExtend extend);

        WithdrawOrderExtend GetWithdrawExtend(string orderNo);
        #endregion
    }
}
