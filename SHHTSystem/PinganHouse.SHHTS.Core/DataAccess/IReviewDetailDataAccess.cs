﻿using PinganHouse.SHHTS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface IReviewDetailDataAccess:IDataAccess<ReviewDetail>
    {
        /// <summary>
        /// 获取有效复核记录
        /// </summary>
        /// <param name="sysno"></param>
        IList<ReviewDetail> GetValidReviewDetail(long reviewSysNo);

        /// <summary>
        /// 获取最新有效复核记录
        /// </summary>
        /// <param name="reviewSysNo"></param>
        /// <returns></returns>
        ReviewDetail GetLastValidReviewDetail(long reviewSysNo);
    }
}
