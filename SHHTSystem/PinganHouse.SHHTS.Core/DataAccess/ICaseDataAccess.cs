﻿using PinganHouse.SHHTS.DataTransferObjects.Filters;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface ICaseDataAccess : IDataAccess<Case>
    {

        /// <summary>
        /// 获取案件信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        Case GetCaseByCaseId(string caseId, ReceptionCenter? receptionCenter=null);

        /// <summary>
        /// 获取获取案件信息 包含最新状态
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="caseStatus"></param>
        /// <returns></returns>
        Case GetCaseByCaseIdAndCaseStatus(string caseId, CaseStatus caseStatus, bool? finished=null);

        /// <summary>
        /// 检验是否已存在
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        bool ExistByCaseId(string caseId);

        /// <summary>
        /// 是否有效案件存在物业标识
        /// </summary>
        /// <param name="tenementContract"></param>
        /// <returns></returns>
        bool ExistByTenementContract(string tenementContract);
        
        /// <summary>
        /// 获取案件
        /// </summary>
        /// <param name="tenementContract"></param>
        /// <returns></returns>
        Case GetCaseByTenementContract(string tenementContract, ReceptionCenter? center=null);

        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        IList<Case> GetCases(Dictionary<string, object> data);

        /// <summary>
        /// 逻辑删除案件
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="modifyUserNo"></param>
        /// <returns></returns>
        bool DeleteCaseByCaseId(string caseId, string modifyUserNo);

        /// <summary>
        /// 获取案件及用户关系
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        IList<CaseUserRelation> GetRelations(params string[] caseId);

        /// <summary>
        /// 获取案件客户
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        CaseUserRelation GetRelation(long userSysNo);

        /// <summary>
        /// 根据客户系统编号获取案件关系
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        IList<CaseUserRelation> GetRelationByCustomerSysNos(params long[] userSysNo);

        /// <summary>
        /// 获取案件及用户关系
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        IList<CaseUserRelation> GetRelation(string caseId);

        /// <summary>
        /// 获取案件及用户关系
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        IList<CaseUserRelation> GetRelation(string caseId, int userType);

        bool Update(string caseId, IDictionary<CaseUserType, List<long>> unBindUsers,  List<long> buyers, List<long> sellers, long? agencySysNo, long modifyUserSysNo, Dictionary<string, object> data = null);


        /// <summary>
        /// 获取案件
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="caseId"></param>
        ///<param name="allCaseStatus"></param> 
        /// <param name="caseStatus"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="affiliationType"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        [Obsolete]
        IList<Case> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, ReceptionCenter receptionCenter, CaseStatus allCaseStatus, long? belongUserSysNo = null, CaseStatus? topStatus = null, string caseId = null,
            DateTime? startDate = null, DateTime? endDate = null, IDictionary<string, object> data = null, CaseStatus? caseStatus = null, bool? finished = null);

        /// <summary>
        /// 获取案件
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="totalCount">The total count.</param>
        /// <returns></returns>
        IList<Case> GetPaginatedList(CaseFilter filter, int pageIndex, int pageSize, out int totalCount);

        /// <summary>
        /// 获取经纪人或者经纪公司名下案件
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="userId"></param>
        /// <param name="agentType"></param>
        /// <param name="allCaseStatus"></param>
        /// <param name="caseId"></param>
        /// <param name="tenementAddress"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        [Obsolete]
        IList<Case> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, IEnumerable<AgentStaffInfo> user, AgentType agentType, CaseStatus allCaseStatus, string caseId = null, string tenementAddress = null,
                    DateTime? startDate = null, DateTime? endDate = null, IDictionary<string, object> data = null, CaseStatus? excludeCaseStatus = null);

        [Obsolete]
        IList<Case> GetCases(int pageIndex, int pageSize, out int totalCount, Dictionary<string, object> data);

        /// <summary>
        /// 修改案件状态
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="disabled"></param>
        /// <param name="modifyUserSysNo"></param>
        /// <returns></returns>
        [Obsolete]
        bool UpdateState(string caseId, bool disabled, long modifyUserSysNo);

        /// <summary>
        /// 解绑人与案件关系
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="userSysNo"></param>
        /// <param name="modifyUserSysNo"></param>
        /// <returns></returns>
        bool UnBindUserRelation(string caseId, long userSysNo, CaseUserType caseUserType, long modifyUserSysNo);

        /// <summary>
        /// 添加案件与人关系
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="caseUserType"></param>
        /// <param name="userSysNos"></param>
        /// <returns></returns>
        bool TransactionInsertCaseUser(string caseId, CaseUserType caseUserType, params long[] userSysNos);

        /// <summary>
        /// 添加流水
        /// </summary>
        /// <param name="caseTrail"></param>
        /// <returns></returns>
        bool InsertCaseTrail(CaseTrail caseTrail);

        /// <summary>
        /// 获取最新案件跟踪
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        CaseTrail GetLastCaseTrailByCaseId(string caseId);

        [Obsolete]
        IList<Case> GetCases(int pageIndex, int pageSize, out int totalCount, ReceptionCenter receptionCenter, ProccessType? proccessType, DateTime? startDate, string lastTrailer, Dictionary<string, object> data = null);

        Case GetCaseByUserSysNo(long userSysNo);

        IList<string> GetIds(int pageIndex, int pageCount, out int totalCount, ReceptionCenter center, string caseId);

        #region Remark
        /// <summary>
        /// 获取案件扩展信息
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        CaseRemarkInfo GetRemark(long sysNo);

        /// <summary>
        /// 添加案件扩展信息
        /// </summary>
        /// <param name="remark"></param>
        /// <returns></returns>
        bool InsertRemark(CaseRemarkInfo remark);

        /// <summary>
        /// 修改扩展信息
        /// </summary>
        /// <param name="remark"></param>
        /// <returns></returns>
        bool UpdateRemark(CaseRemarkInfo remark);

        #endregion

        /// <summary>
        /// 修改案件资金托管信息
        /// </summary>
        /// <param name="sysNo"></param>
        /// <param name="isFundTrusteeship"></param>
        /// <param name="ftContractNo"></param>
        /// <param name="isCommissionTrusteeship"></param>
        /// <param name="ctContractNo"></param>
        /// <returns></returns>
        bool UpdateTrusteeshipInfo(long sysNo, bool? isFundTrusteeship = null, string ftContractNo = null, bool? isCommissionTrusteeship = null, string ctContractNo = null, long? operatorSysNo = null);
    }
}
