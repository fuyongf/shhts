﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Entities;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface IAccountDetailDataAccess : IDataAccess<AccountDetailInfo>
    {
        IList<AccountDetailInfo> GetByAccount(long accountSysNo);

        IList<AccountDetailInfo> GetByOrder(string orderNo);
    }
}
