﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface IPropertySurveyDataAccess : IDataAccess<PropertySurvey>
    {
        /// <summary>
        /// 获取产调记录
        /// </summary>
        /// <param name="tenementContract"></param>
        /// <returns></returns>

        IList<PropertySurvey> GetPropertySurveys(string tenementContract);

        IList<PropertySurvey> GetPropertySurveys(IEnumerable<string> tenementContracts);  
    }
}
