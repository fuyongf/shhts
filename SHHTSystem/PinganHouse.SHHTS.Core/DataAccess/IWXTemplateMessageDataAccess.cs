﻿using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface IWXTemplateMessageDataAccess : IDataAccess<WXTemplateMessage>
    {
        /// <summary>
        /// 更新消息状态
        /// </summary>
        /// <param name="msgId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        bool UpdateByMsgId(string msgId, WXStatus status);

        /// <summary>
        /// 获取消息
        /// </summary>
        /// <param name="msgId"></param>
        /// <returns></returns>
        WXTemplateMessage GetMessageByMsgId(string msgId);
    }
}
