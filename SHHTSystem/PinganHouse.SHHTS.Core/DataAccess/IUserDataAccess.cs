﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface IUserDataAccess : IDataAccess<UserInfo>
    {
        /// <summary>
        /// 根据UserId获取客户信息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        UserInfo Get(string userId);

        /// <summary>
        /// 获取用户真实姓名
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        string GetUserName(long sysNo);

        /// <summary>
        /// 获取用户真实姓名
        /// </summary>
        /// <param name="UserSysNo"></param>
        /// <returns></returns>
        IList<string> GetUserName(string UserSysNo);

        ///// <summary>
        ///// 根据系统编号批量获取用户基本信息
        ///// </summary>
        ///// <param name="sysNos"></param>
        ///// <returns></returns>
        //IList<UserInfo> GetBySysNos(IEnumerable<long> sysNos);

        IList<UserInfo> GetPaginatedList(string condition, UserType? userType, int pageIndex, int pageSize, out int totalCount, string orderBy = "[CreateDate]", ConditionMatchingType matchingType = ConditionMatchingType.Fuzzy);

        /// <summary>
        /// 绑定身份认证信息
        /// </summary>
        /// <param name="sysNo"></param>
        /// <param name="identity"></param>
        /// <returns></returns>
        bool BindIdentity(long sysNo, UserIdentityInfo identity);

        /// <summary>
        /// 修改用户基本信息
        /// </summary>
        /// <param name="sysNo"></param>
        /// <param name="name"></param>
        /// <param name="certType"></param>
        /// <param name="idNo"></param>
        /// <param name="email"></param>
        /// <param name="mobile"></param>
        /// <param name="gender"></param>
        /// <returns></returns>
        //bool UpdateBaseInfo(long sysNo, string name, CertificateType? certType, string idNo, string email, string mobile, Gender? gender, long operatorSysNo);

        /// <summary>
        /// 根据证件号检查用户是否存在
        /// </summary>
        /// <param name="type"></param>
        /// <param name="identityNo"></param>
        /// <returns></returns>
        long? GetSysNoByIdentityNo(CertificateType type, string identityNo);

        /// <summary>
        /// 根据证件获取用户
        /// </summary>
        /// <param name="type"></param>
        /// <param name="identityNo"></param>
        /// <returns></returns>
        UserInfo GetByIdentityNo(CertificateType type, string identityNo);
    }

    public interface IUserAccountDataAccess : IDataAccess<UserAccountInfo>
    {
        /// <summary>
        /// 登录名是否存在
        /// </summary>
        /// <param name="loginId"></param>
        /// <returns></returns>
        bool LoginIdExists(string loginId);

        /// <summary>
        /// 根据登录名获取用户类型
        /// </summary>
        /// <param name="loginId"></param>
        /// <returns></returns>
        UserType? GetUserTypeByLoginId(string loginId);

        /// <summary>
        /// 根据登录名获取登陆信息
        /// </summary>
        /// <param name="loginId"></param>
        /// <returns></returns>
        UserAccountInfo GetByLoginId(string loginId);

        /// <summary>
        /// 重置最后登录时间
        /// </summary>
        /// <param name="loginDate"></param>
        /// <returns></returns>
        bool ResetLoginDate(long userSysNo, DateTime? loginDate);

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="sysNo"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        bool ChangePassword(long sysNo, string newPassword);
    }

    public interface ICustomerDataAccess : IDataAccess<CustomerInfo>
    {
        bool Insert(CustomerInfo customer, string loginName = null, string password = null);
        /// <summary>
        /// 创建客户信息
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="identity"></param>
        /// <returns></returns>
        bool Create(CustomerInfo entity, UserIdentityInfo identity);

        /// <summary>
        /// 根据UserId获取客户信息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IList<CustomerInfo> Get(string userId);

        /// <summary>
        /// 根据usersysno获取客户信息
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        IList<CustomerInfo> GetByUserSysNo(long userSysNo);

        /// <summary>
        /// 获取客户带户籍地址
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        CustomerInfo GetWithAddress(long sysNo);

        /// <summary>
        /// 获取客户
        /// </summary>
        /// <param name="sysnos"></param>
        /// <returns></returns>
        IList<string> GetCustomerNames(IEnumerable<string> sysnos);

        /// <summary>
        /// 根据案件编号获取客户信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        IList<CustomerInfo> GetCustomerByCase(string caseId);

        IList<CustomerInfo> GetPaginatedList(string condition, CustomerType? customerType, int pageIndex, int pageSize, out int totalCount, string orderBy = "C.[CreateDate] DESC", ConditionMatchingType matchingType = ConditionMatchingType.Fuzzy);

        IList<CustomerInfo> GetBySysNos(IEnumerable<long> sysNos);

        IList<CustomerInfo> GetByMobile(string mobile);

        IList<CustomerInfo> GetByIdentityNo(string identityNo);

        string GetRealName(long sysNo);

        bool PrefectCustomer(long sysNo, string email, string mobile, long operatorSysNo);
    }

    public interface IPinganStaffDataAccess : IDataAccess<PinganStaffInfo>
    {
        bool Insert(PinganStaffInfo pinganStaff, string password = null);
        /// <summary>
        /// 根据UserId获取客户信息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IList<PinganStaffInfo> Get(string userId);

        PinganStaffInfo GetByUMCode(string um);

        /// <summary>
        /// 根据员工卡获取信息
        /// </summary>
        /// <param name="staffCardNo"></param>
        /// <returns></returns>
        PinganStaffInfo GetByStaffCardNo(string staffCardNo);

        IList<PinganStaffInfo> GetPaginatedList(string condition, int pageIndex, int pageSize, out int totalCount, string orderBy = "P.[CreateDate] DESC", ConditionMatchingType matchingType = ConditionMatchingType.Fuzzy);
    }

    public interface IAgentStaffDataAccess : IDataAccess<AgentStaffInfo>
    {
        bool Insert(AgentStaffInfo agentStaff, string loginName = null, string password = null);

        /// <summary>
        /// 根据UserId获取客户信息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IList<AgentStaffInfo> Get(string userId);

        IList<AgentStaffInfo> GetPaginatedList(string condition, long? agentCompanySysNo, bool? isManager, int pageIndex, int pageSize, out int totalCount, string identityNo = null, string name = null, string mobile = null, string orderBy = "S.[CreateDate] DESC", ConditionMatchingType matchingType = ConditionMatchingType.Fuzzy);

        IList<AgentStaffPaginatedInfo> GetPaginatedListWithCompany(string condition, long? agentCompanySysNo, bool? isManager, int pageIndex, int pageSize, out int totalCount, string identityNo = null, string name = null, string mobile = null, string orderBy = "S.[CreateDate] DESC", ConditionMatchingType matchingType = ConditionMatchingType.Fuzzy);
        
        string GetRealName(long sysNo);

        bool ExistsByIdentityNo(CertificateType cType, string identityNo);
    }

    public interface IThirdpartyUserDataAccess : IDataAccess<ThirdpartyUserInfo>
    {
        bool Insert(ThirdpartyUserInfo tpUser, string loginName = null, string password = null);
        /// <summary>
        /// 根据UserId获取客户信息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IList<ThirdpartyUserInfo> Get(string userId);

        IList<ThirdpartyUserInfo> GetPaginatedList(string condition, string tpName, int pageIndex, int pageSize, out int totalCount, string orderBy = "T.[CreateDate] DESC", ConditionMatchingType matchingType = ConditionMatchingType.Fuzzy);
    }
}
