﻿using PinganHouse.SHHTS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface IPropertyCertificateDataAccess : IDataAccess<PropertyCertificate>
    {
        /// <summary>
        /// 是否存在产权
        /// </summary>
        /// <param name="tenementContract"></param>
        /// <returns></returns>
        bool Exist(string tenementContract);

        /// <summary>
        /// 获取产证信息
        /// </summary>
        /// <param name="tenementContract"></param>
        /// <returns></returns>
        PropertyCertificate Get(string tenementContract);


        IList<PropertyCertificate> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, string tenementContract = null, string tenementAddress = null, DateTime? startDate=null, DateTime? endDate=null, IDictionary<string, object> data = null);

    }
}
