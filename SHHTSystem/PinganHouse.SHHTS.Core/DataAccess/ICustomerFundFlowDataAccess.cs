﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Core.DataAccess
{
    public interface ICustomerFundFlowDataAccess : IDataAccess<CustomerFundFlowInfo>
    {
        /// <summary>
        /// 根据案件编号获取客户资金流转
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="customerType"></param>
        /// <returns></returns>
        IEnumerable<CustomerFundFlowInfo> Get(string caseId, CustomerType? customerType= null);
    }
}
