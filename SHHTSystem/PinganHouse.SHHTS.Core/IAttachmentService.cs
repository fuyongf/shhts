﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace PinganHouse.SHHTS.Core
{
    [ServiceContract]
    public interface IAttachmentService
    {
        /// <summary>
        /// 添加附件
        /// </summary>
        /// <param name="AttachmentContent">附件信息</param>
        /// <returns></returns>
        [OperationContract]
        UploadAttachmentResult AddAttachment(AttachmentContent attachmentInfo);

        /// <summary>
        /// 获取附件信息
        /// </summary>
        /// <param name="associationNo">关联业务标识</param>
        /// <returns></returns>
        [OperationContract]
        AttachmentDtos GetAttachments(AttachmentQuery query);

        /// <summary>
        /// 下载附件
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns></returns>
        [OperationContract]
        FileDownloadMessage DownloadAttachment(AttachmentQuery query);

        /// <summary>
        /// 获取附件信息
        /// </summary>
        /// <param name="bizNo"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        [OperationContract(Name = "GetAttachmentsByBizNo")]
        IList<AttachmentDto> GetAttachments(string bizNo, IEnumerable<AttachmentType> types);

        /// <summary>
        /// 删除附件
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        [OperationContract]
        DeleteAttachmentResult DeleteAttachment(DeleteAttachmentElement element);
    }
}
