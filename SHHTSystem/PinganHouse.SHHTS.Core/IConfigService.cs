﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.DataTransferObjects;

namespace PinganHouse.SHHTS.Core
{
    [ServiceContract]
    public interface IConfigService
    {
        /// <summary>
        /// 获取银行列表
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        IDictionary GetBanks();

        /// <summary>
        /// 获取贷款银行列表
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        IDictionary GetBankDic();

        /// <summary>
        /// 获取鉴权银行列表
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [Obsolete("use GetIdentifyBankCfgs instead")]
        IDictionary GetIdentifyBanks();

        /// <summary>
        /// 获取鉴权银行配置列表
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        IList<BankIdentifyCfg> GetIdentifyBankCfgs();

        /// <summary>
        /// 根据银行代码获取银行ID
        /// </summary>
        /// <param name="bankCode"></param>
        /// <returns></returns>
        int GetBankIdByCode(string bankCode);

        /// <summary>
        /// 根据银行简码获取银行名称
        /// </summary>
        /// <param name="bankCode"></param>
        /// <returns></returns>
        [OperationContract]
        string GetBankNameByCode(string bankCode);

        /// <summary>
        /// 获取公积金管理中心地址
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        IList<string> GetProvidentFundAddress();

        /// <summary>
        /// 获取材料清单
        /// </summary>
        /// <param name="materialPath"></param>
        /// <returns></returns>
        [OperationContract]
        string GetMaterial(string materialPath);


        /// <summary>
        /// 获取受理中心地址
        /// </summary>
        /// <param name="materialPath"></param>
        /// <returns></returns>
        [OperationContract]
        string GetReceptionCenter(string receptionCenter);

        /// <summary>
        /// 获取贷款专员
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        IList<string> GetCommissioners();

        /// <summary>
        /// 获取省市区信息
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        [OperationContract]
        IList<string> GetRegionInfo(string path = "");

        /// <summary>
        /// 获取自动更新地址
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        string GetAutoUpdateUrl();


        /// <summary>
        /// 获取新版本自动更新地址
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        string GetNewAutoUpdateUrl();

        /// <summary>
        /// 获取二手房交易管理系统关于信息
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        string GetSHHTMSAbooutInfo();

        #region 打印相关
        /// <summary>
        /// 获取E房钱打印模版
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        IList<PrintTemplate> GetEFangqianPrintTemplates();

        /// <summary>
        /// 获取打印模版信息
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        [OperationContract]
        Dictionary<string, PrintTemplateValue> GetPrintTemplate(string templateId, string pageNo);
        #endregion


        /// <summary>
        /// 获取微信应用密钥
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        Tuple<string, string> GetWXKeys(string appNo);

        /// <summary>
        /// 获取客户资金变更条件
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        [OperationContract]
        IDictionary<string, IList<string>> GetChangeFundCondition(ChangeFundType type);

        ///// <summary>
        ///// 根据订单业务类型获取资金走向
        ///// </summary>
        ///// <param name="tradeType"></param>
        ///// <returns></returns>
        //[OperationContract]
        //FundDirection GetFundDirection(OrderBizType tradeType);

        /// <summary>
        /// 获取退款交易流水提交条件
        /// </summary>
        /// <returns></returns>
        AuditStatus GetRefundSubmitCondition();

        /// <summary>
        /// 获取付款交易流水提交条件
        /// </summary>
        /// <returns></returns>
        AuditStatus GetPaymentSubmitCondition();

        /// <summary>
        /// 获取付款交易流水提交条件
        /// </summary>
        /// <returns></returns>
        AuditStatus GetWithdrawSubmitCondition();

        #region 审批相关
        /// <summary>
        /// 获取付款订单审批前置状态
        /// </summary>
        /// <param name="center"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        IList<AuditStatus> GetPaymentOrderAuditPreStatus(ReceptionCenter center, AuditStatus status);

        /// <summary>
        /// 获取提现订单审批前置状态
        /// </summary>
        /// <param name="center"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        IList<AuditStatus> GetWithdrawOrderAuditPreStatus(ReceptionCenter center, AuditStatus status);

        /// <summary>
        /// 获取退款订单审批前置状态
        /// </summary>
        /// <param name="center"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        IList<AuditStatus> GetRefundOrderAuditPreStatus(ReceptionCenter center, AuditStatus status);

        /// <summary>
        /// 获取收款订单审批前置状态
        /// </summary>
        /// <param name="center"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        IList<AuditStatus> GetCollectionOrderAuditPreStatus(ReceptionCenter center, AuditStatus status);

        /// <summary>
        /// 获取出款订单审批人
        /// </summary>
        /// <param name="center"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        IList<long> GetPaymentOrderAuditors(ReceptionCenter center, AuditStatus status);

        /// <summary>
        /// 获取提现订单审批人
        /// </summary>
        /// <param name="center"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        IList<long> GetWithdrawOrderAuditors(ReceptionCenter center, AuditStatus status);

        /// <summary>
        /// 获取收款订单审批前置状态
        /// </summary>
        /// <param name="center"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        IList<long> GetRefundOrderAuditors(ReceptionCenter center, AuditStatus status);

        /// <summary>
        /// 获取收款订单审批前置状态
        /// </summary>
        /// <param name="center"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        IList<long> GetCollectionOrderAuditors(ReceptionCenter center, AuditStatus status);
        #endregion
    }
}
