﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace PinganHouse.SHHTS.Core
{
    [ServiceContract]
    public interface IReviewService
    {
        /// <summary>
        /// 获取复核规则
        /// </summary>
        /// <param name="reviewCategory">复核类别</param>
        /// <param name="affiliationNo">复核行为实例</param>
        /// <param name="reviewDto">复核规则</param>
        /// <returns> success is true need review</returns>
        [OperationContract]
        OperationResult ReviewRule(ReviewCategory reviewCategory, string affiliationNo, long? createUserSysNo, out ReviewDto reviewDto);

        /// <summary>
        /// 验证复核规则
        /// </summary>
        /// <param name="sysno">复核规则系统编号</param>
        /// <param name="reviewSysNo">复核人系统编号</param>
        /// <param name="reviewMode">复核方式</param>
        /// <param name="reviewDto">下一复核规则</param>
        /// <returns>
        /// success is false 代表复核失败
        /// or
        /// reviewDto is not null 代表还需下一个复核
        /// </returns>
        [OperationContract]
        OperationResult ValidateReviewRule(long sysno, long reviewSysNo, ReviewMode reviewMode, out ReviewDto reviewDto);
    }
}
