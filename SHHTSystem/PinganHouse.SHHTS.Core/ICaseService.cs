﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.DataTransferObjects.Filters;
using PinganHouse.SHHTS.Entities;
using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace PinganHouse.SHHTS.Core
{
    [ServiceContract]
    public interface ICaseService
    {
        /// <summary>
        /// 添加案件
        /// </summary>
        /// <param name="caseInfo"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult GenerateCase(CaseDto caseInfo);
        /// <summary>
        /// 获取案件 不包含案件状态
        /// </summary>
        /// <param name="caseId">案件编号</param>
        /// <returns></returns>
        [OperationContract]
        CaseDto GetCaseByCaseId(string caseId);

        /// <summary>
        /// 获取案件 不包含案件状态
        /// </summary>
        /// <param name="caseId">案件编号</param>
        /// <returns></returns>
        [OperationContract(Name = "GetCaseByCaseIdAndReceptionCenter")]
        CaseDto GetCaseByCaseId(string caseId, ReceptionCenter receptionCenter);

        /// <summary>
        /// 获取案件，包含上下家信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="receptionCenter"></param>
        /// <returns></returns>
        [OperationContract]
        CaseWithCustomer GetCaseWithCustomer(string caseId, ReceptionCenter receptionCenter);

        /// <summary>
        /// 获取案件信息，包含案件状态
        /// </summary>
        /// <param name="caseId">案件id</param>
        /// <param name="caseStatus">案件状态</param>
        /// <returns></returns>
        [OperationContract]
        CaseDto GetCaseByCaseIdAndCaseStatus(string caseId, CaseStatus caseStatus);
        /// <summary>
        /// 获取案件
        /// </summary>
        /// <param name="sysno">案件系统编号</param>
        /// <returns></returns>
        [OperationContract]
        CaseDto GetCase(long sysno);
        /// <summary>
        /// 获取案件及相关进程信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="caseStatus"></param>
        /// <param name="process"></param>
        /// <returns></returns>
        [OperationContract]
        CaseDto GetCaseAndProcess(string caseId, CaseStatus caseStatus, out Dictionary<CaseStatus, Tuple<string, DateTime>> process);
        /// <summary>
        /// 是否有效案件存在物业标识
        /// </summary>
        /// <param name="tenementContract"></param>
        /// <returns></returns>
        [OperationContract]
        bool ExistTenementContract(string tenementContract);
        /// <summary>
        /// 更新案件状态
        /// </summary>
        /// <param name="caseId">案件编号</param>
        /// <param name="caseStatus">案件状态</param>
        /// <param name="modifyUserSysNo">修改人</param>
        ///  <param name="otherData">其他信息</param>
        /// <returns></returns>
        [OperationContract]
        [Obsolete]
        OperationResult UpdateCaseStatus(string caseId, CaseStatus caseStatus, long modifyUserSysNo, Dictionary<string, object> otherData = null);

        /// <summary>
        /// 更新案件状态.
        /// </summary>
        /// <param name="caseId">案件编号.</param>
        /// <param name="action">动作名称.</param>
        /// <param name="modifyUserSysNo">修改人编号.</param>
        /// <param name="otherData">其他数据.</param>
        /// <returns></returns>
        [OperationContract(Name = "UpdateCaseStatusNew")]
        OperationResult UpdateCaseStatus(string caseId, string action, long modifyUserSysNo, Dictionary<string, object> otherData = null);
        /// <summary>
        /// 逻辑删除案件
        /// </summary>
        /// <param name="caseId">案件编号</param>
        /// <returns></returns>
        [OperationContract]
        OperationResult DeleteCaseByCaseId(string caseId, string modifyUserNo);
        /// <summary>
        /// 解绑人与案件关系
        /// </summary>
        /// <param name="caseId">案件id</param>
        /// <param name="userSysNo">解绑人</param>
        /// <param name="modifyUserSysNo">操作人</param>
        /// <param name="caseUserType">解绑人类型</param>
        /// <returns></returns>
        [OperationContract]
        OperationResult UnBindUser(string caseId, long userSysNo, CaseUserType caseUserType, long modifyUserSysNo);
        /// <summary>
        /// 绑定人与案件
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="caseUserType"></param>
        /// <param name="userSysNos"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult BindUsers(string caseId, CaseUserType caseUserType, params long[] userSysNos);
        /// <summary>
        /// 更新案件
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="buyers"></param>
        /// <param name="sellers"></param>
        /// <param name="agencySysNo"></param>
        /// <param name="modifyUserSysNo"></param>
        /// <param name="tenementAddress"></param>
        /// <param name="tenementContract"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult Update(string caseId, List<long> buyers, List<long> sellers, long agencySysNo, long modifyUserSysNo, string tenementAddress = null, string tenementContract = null, Dictionary<string, object> data = null);
        /// <summary>
        /// 获取案件列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="condition"></param>
        /// <param name="allCaseStatus">所属状态组</param>
        /// <param name="caseStatus">查某状态</param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="affiliationType"></param>
        /// <param name="matchingType"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        [OperationContract(Name = "GetPaginatedList")]
        [Obsolete]
        IList<CaseDto> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, ReceptionCenter receptionCenter, CaseStatus allCaseStatus, long? belongUserSysNo = null, CaseStatus? topStatus = null, CaseStatus? caseStatus = null, string condition = null,
             DateTime? startDate = null, DateTime? endDate = null, CaseStatus? operationStatus = null, bool? finished = null, IDictionary<string, object> data = null);

        /// <summary>
        /// 获取案件列表.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="totalCount">The total count.</param>
        /// <returns></returns>
        [OperationContract(Name = "GetPaginatedListNew")]
        IList<CaseDto> GetPaginatedList(CaseFilter filter, int pageIndex, int pageSize, out int totalCount);

        /// <summary>
        /// 获取案件列表
        /// </summary>
        /// <param name="queryString"></param>
        /// <returns></returns>
        [OperationContract]
        [Obsolete]
        IList<CaseDto> GetCases(int pageIndex, int pageSize, out int totalCount, ReceptionCenter center, QueryStringType? queryStringType = null, string queryString = null, Dictionary<string, object> data = null);

        /// <summary>
        /// 获取经纪人或者经纪公司名下案件
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="userId"></param>
        /// <param name="agentType"></param>
        /// <param name="allCaseStatus">案件状态</param>
        /// <param name="categorys">案件类别</param>
        /// <param name="caseId"></param>
        /// <param name="tenementAddress"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        [OperationContract(Name = "GetPaginatedListByAgent")]
        [Obsolete]
        IList<CaseDto> GetPaginatedList(int pageIndex, int pageSize, out int totalCount, string userId, AgentType agentType, CaseStatus allCaseStatus, IEnumerable<CaseStatus> categorys, string caseId = null, string tenementAddress = null,
                    DateTime? startDate = null, DateTime? endDate = null, IDictionary<string, object> data = null, CaseStatus? excludeCaseStatus = null);

        /// <summary>
        /// 根据客户获取案件
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        CaseDto GetCaseByUserSysNo(long userSysNo);

        /// <summary>
        /// 获取案件附件信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        [OperationContract]
        CaseAttachmentDto GetCaseAttenment(string caseId);

        /// <summary>
        /// 获取案件事件
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="caseStatus"></param>
        /// <returns></returns>
        [OperationContract(Name = "GetCaseEventsByCaseIdAndCaseStatus")]
        IList<CaseEvent> GetCaseEvents(string caseId, CaseStatus? currentCaseStatus, CaseStatus? preCaseStatus);

        /// <summary>
        /// 添加案件跟进
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="proccessType"></param>
        /// <param name="remark"></param>
        /// <param name="createUserSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult SetCaseTrail(CaseTrailDto caseTrail);

        /// <summary>
        /// 获取案件跟进信息列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="receptionCenter">所属业务受理中心</param>
        /// <param name="operationStatus">所需状态信息</param>
        /// <param name="categorys">状态类别列表</param>
        /// <param name="proccessType">案件进度</param>
        /// <param name="startDate">开始时间</param>
        /// <param name="lastTrailer">最后跟进人</param>
        /// <returns></returns>
        [OperationContract]
        IList<CaseTrailPaged> GetCaseTrailPaged(int pageIndex, int pageSize, out int totalCount, ReceptionCenter receptionCenter, CaseStatus operationStatus, IEnumerable<CaseStatus> categorys = null, ProccessType? proccessType = null, DateTime? startDate = null, string lastTrailer = null, Dictionary<string, object> data = null);


        /// <summary>
        /// 更新案件事件数据
        /// </summary>
        /// <param name="sysno"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        [OperationContract]
        [Obsolete]
        OperationResult UpdateCaseEvent(long sysno, Dictionary<string, object> dic, long modifyUserSysNo);

        /// <summary>
        /// 根据案件编号获取案件事件信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        [OperationContract]
        IList<CaseEvent> GetCaseEvents(string caseId, int pageIndex, int pageSize, out int totalCount, IEnumerable<CaseStatus> status = null);

        /// <summary>
        /// 完善买家信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="customer"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult PerfectBuyer(string caseId, Buyer buyer, IDictionary<long, string> mobiles, IDictionary<long, string> emails, long operatorSysNo);

        /// <summary>
        /// 完善卖家信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="customer"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult PrefectSeller(string caseId, Seller seller, IDictionary<long, string> mobiles, IDictionary<long, string> emails, long operatorSysNo);

        /// <summary>
        /// 获取买家信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        [OperationContract]
        Buyer GetBuyer(string caseId);

        /// <summary>
        /// 获取卖家信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        [OperationContract]
        Seller GetSeller(string caseId);

        /// <summary>
        /// 获取案件上下家信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        [OperationContract]
        Tuple<Seller, Buyer> GetCaseCustomers(string caseId);

        /// <summary>
        /// 模糊查询案件编号
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageCount"></param>
        /// <param name="totalCount"></param>
        /// <param name="center"></param>
        /// <param name="caseId"></param>
        /// <returns></returns>
        [OperationContract]
        IList<string> GetCaseIds(int pageIndex, int pageCount, out int totalCount, ReceptionCenter center, string caseId);

        /// <summary>
        /// 案件完成时
        /// </summary>
        event Action<Case, ICaseService> OnCreated;

        /// <summary>
        /// 状态变更完成
        /// </summary>
        event Action<CaseEventInfo, Case> OnUpdated;

        #region Remark
        /// <summary>
        /// 完善案件扩展信息
        /// </summary>
        /// <param name="remark"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult PerfectCaseInfo(CaseRemark remark, bool? isFundTrusteeship = null, string ftContractNo = null, bool? isCommissionTrusteeship = null, string ctContractNo = null, IEnumerable<CustomerBankAccount> accounts = null);

        /// <summary>
        /// 根据案件系统编号获取扩展信息
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        [OperationContract]
        CaseRemark GetRemarkBySysNo(long sysNo);

        /// <summary>
        /// 根据案件系统编号获取客户托管资金帐号
        /// </summary>
        /// <param name="caseSysNo"></param>
        /// <param name="customerType"></param>
        /// <param name="accountType"></param>
        /// <returns></returns>
        [OperationContract]
        IEnumerable<CustomerBankAccount> GetCustomerBankAccounts(long caseSysNo, CustomerType? customerType = null, BankAccountPurpose? accountType = null);

        /// <summary>
        /// 获取案件扩展信息
        /// </summary>
        /// <param name="tenementContract">产权号</param>
        /// <returns></returns>
        [OperationContract]
        CaseRemark GetRemark(string tenementContract, ReceptionCenter center, Dictionary<string, object> data = null);

        /// <summary>
        /// 获取案件产权号列表
        /// </summary>
        /// <param name="queryString"></param>
        /// <returns></returns>
        [OperationContract]
        IDictionary<string, string> GetTenementContracts(QueryStringType queryStringType, string queryString, ReceptionCenter center, Dictionary<string, object> data = null);

        /// <summary>
        /// 添加客户银行账户信息
        /// </summary>
        /// <param name="caseId">案件编号</param>
        /// <param name="type">客户类型</param>
        /// <param name="purpose">账户用途</param>
        /// <param name="bankCode">银行编码</param>
        /// <param name="bankName">银行名称</param>
        /// <param name="subBankName">支行名称</param>
        /// <param name="bankAccount">卡号|帐号</param>
        /// <param name="accountName">开户名</param>
        /// <param name="isMainAcccount">是否是主帐号</param>
        /// <param name="operatorSysNo">操作人</param>
        /// <param name="needAuth">是否需要鉴权</param>
        /// <param name="identityNo">身份证号，如<see cref="needAuth"/>为false，可不传</param>
        /// <param name="mobile">手机号，如<see cref="needAuth"/>为false或非四要素鉴权，可不传</param>
        /// <returns></returns>
        [OperationContract]
        OperationResult AddCustomerBankAccount(string caseId, CustomerType type, IEnumerable<BankAccountPurpose> purpose, string bankCode, string bankName, string subBankName, string bankAccount, string accountName, bool isMainAcccount, long operatorSysNo, bool needAuth = false, string identityNo = null, string mobile = null);
        #endregion

    }
}
