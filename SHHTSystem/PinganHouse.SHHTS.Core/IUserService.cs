﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Core
{
    [ServiceContract]
    public interface IUserService : ICustomerService, IAgentStaffService, IPinganStaffService, IThirdpartyUserService
    {
        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="loginId"></param>
        /// <param name="password"></param>
        /// <param name="userType"></param>
        /// <returns></returns>
        [OperationContract]
        [ServiceKnownType(typeof(User))]
        OperationResult Login(string loginId, string password);

        /// <summary>
        /// 指纹登陆
        /// </summary>
        /// <param name="fpCode"></param>
        /// <returns></returns>
        [OperationContract]
        [ServiceKnownType(typeof(User))]
        OperationResult LoginByFingerprint(string fpCode);

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult ChangePassword(long userSysNo, string oldPassword, string newPassword);

        /// <summary>
        /// 根据系统编号获取用户
        /// </summary>
        /// <param name="sysNo">用户系统编号(自然人系统编号，如：Customer.UserSysNo)</param>
        /// <returns></returns>
        T GetUser<T>(long sysNo) where T : User, new();

        /// <summary>
        /// 根据用户ID获取用户
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        //T GetUser<T>(string userId) where T : User, new();

        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="condition"></param>
        /// <param name="matchingType"></param>
        /// <returns></returns>
        [OperationContract]
        IList<User> GetUsers(int pageIndex, int pageSize, out int totalCount, UserType? userType = null, string condition = null, ConditionMatchingType matchingType = ConditionMatchingType.Fuzzy);

        /// <summary>
        /// 根据证件号码获取身份认证信息
        /// </summary>
        /// <param name="identityNo"></param>
        /// <returns></returns>
        [OperationContract]
        UserIdentity GetUserIdentity(string identityNo);

        /// <summary>
        /// 根据用户系统编号获取身份认证信息
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        UserIdentity GetUserIdentityByUser(long userSysNo, UserType userType);

        /// <summary>
        /// 绑定用户身份认证信息
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <param name="identity"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult BindUserIdentity(long userSysNo, UserType userType, UserIdentity identity);

        /// <summary>
        /// 获取用户真实姓名
        /// </summary>
        /// <param name="userSysNo">用户系统编号(自然人系统编号，如：Customer.UserSysNo)</param>
        /// <returns></returns>
        [OperationContract]
        string GetUserName(long userSysNo);

        /// <summary>
        /// 采集指纹信息
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <param name="fpCode"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult CollectFinger(long userSysNo, string fpCode, string tag, long operatorSysNo);

        /// <summary>
        /// 删除指纹信息
        /// </summary>
        /// <param name="fpSysNo"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult DeleteFinger(long fpSysNo, long operatorSysNo);

        /// <summary>
        /// 根据用户系统编号获取指纹信息
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        [OperationContract]
        IList<UserFingerprint> GetFingerByUserSysNo(long userSysNo);

        /// <summary>
        /// 根据指纹获取用户信息
        /// </summary>
        /// <param name="fpCode"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult GetStaffByFingerprint(string fpCode);

        /// <summary>
        /// 更新发送短信标识
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <param name="isSendSms"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult UpdateSendSmsFlag(long userSysNo, bool isSendSms, long? createUserSysNo);
    }

    /// <summary>
    /// 客户(上，下家)
    /// </summary>
    [ServiceContract]
    public interface ICustomerService
    {
        /// <summary>
        /// 创建客户信息
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult CreateCustomer(Customer customer);

        /// <summary>
        /// 创建客户信息
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        [OperationContract(Name = "CreateCustomerWithIdentity")]
        OperationResult CreateCustomer(Customer customer, UserIdentity identity);

        /// <summary>
        /// 根据系统编号获取用户
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        [OperationContract]
        Customer GetCustomer(long sysNo);

        /// <summary>
        /// 根据用户ID获取用户
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        //[OperationContract(Name = "GetCustomerByUserId")]
        //Customer GetCustomer(string userId);

        /// <summary>
        /// 根据案件编号获取客户信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        [OperationContract]
        IList<Customer> GetCustomerByCase(string caseId);

        /// <summary>
        /// 分页获取用户信息
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="customerType"></param>
        /// <param name="condition"></param>
        /// <param name="matchingType"></param>
        /// <returns></returns>
        [OperationContract]
        IList<Customer> GetCustomers(int pageIndex, int pageSize, out int totalCount, CustomerType? customerType = null, string condition = null, ConditionMatchingType matchingType = ConditionMatchingType.Fuzzy);

        /// <summary>
        /// 修改客户信息
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult ModifyCustomer(Customer customer);

        /// <summary>
        /// 根据用户系统编号获取证件号码
        /// </summary>
        /// <param name="userSysNos"></param>
        /// <returns></returns>
        [OperationContract]
        IDictionary<long, string> GetUserIdentityNoByCustomerSysNos(IEnumerable<long> userSysNos);

        /// <summary>
        /// 根据手机获取客户及案件
        /// </summary>
        /// <param name="mobile">手机号码</param>
        /// <returns>key:自然人系统编号, value:客户明细</returns>
        [OperationContract]
        Dictionary<long, IList<CustomerDetail>> GetCustomerDetails(string mobile);


        /// <summary>
        /// 根据证件号获取客户及案件
        /// </summary>
        /// <param name="identityNo"></param>
        /// <returns>key:自然人系统编号, value:客户明细</returns>
        [OperationContract(Name = "GetCustomerDetailsByIdentityNo")]
        Dictionary<long, IList<CustomerDetail>> GetCustomerDetailsByIdentityNo(string identityNo);
    }

    /// <summary>
    /// 中介(经纪人)
    /// </summary>
    [ServiceContract]
    public interface IAgentStaffService
    {
        /// <summary>
        /// 创建经纪人信息
        /// </summary>
        /// <param name="agentStaff"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult CreateAgentStaff(AgentStaff agentStaff, string loginName, string password);

        /// <summary>
        /// 根据系统编号获取用户
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        [OperationContract]
        AgentStaff GetAgentSatff(long sysNo);

        /// <summary>
        /// 根据经纪人userid获取用户
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        [OperationContract(Name = "GetAgentSatffByUserId")]
        AgentStaff GetAgentSatff(string userId);

        /// <summary>
        /// 删除中介员工
        /// </summary>
        /// <param name="sysNo"></param>
        /// <param name="mangerUserId"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult DeleteAgentStaff(long sysNo, string operatorUserId);

        /// <summary>
        /// 根据用户ID获取用户
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        //[OperationContract(Name = "GetAgentSatffByUserId")]
        //AgentStaff GetAgentSatff(string userId);

        /// <summary>
        /// 分页获取中介公司员工信息
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="agentCompanySysNo"></param>
        /// <param name="condition"></param>
        /// <param name="matchingType"></param>
        /// <returns></returns>
        [OperationContract]
        IList<AgentStaff> GetAgentStaffs(int pageIndex, int pageSize, out int totalCount, long? agentCompanySysNo, bool? isManager, string condition = null, string identityNo = null, string name = null, string mobile = null, ConditionMatchingType matchingType = ConditionMatchingType.Fuzzy);

        /// <summary>
        /// 分页获取中介公司员工信息
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="agentCompanySysNo"></param>
        /// <param name="condition"></param>
        /// <param name="matchingType"></param>
        /// <returns></returns>
        [OperationContract]
        IList<AgentStaffQueryInfo> GetAgentStaffsWithCompany(int pageIndex, int pageSize, out int totalCount, long? agentCompanySysNo, bool? isManager, string condition = null, string identityNo = null, string name = null, string mobile = null, ConditionMatchingType matchingType = ConditionMatchingType.Fuzzy);

        /// <summary>
        /// 修改中介员工信息
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult ModifyAgentStaff(AgentStaff agentStaff);
    }

    /// <summary>
    /// 平安(好房)员工
    /// </summary>
    [ServiceContract]
    public interface IPinganStaffService
    {
        /// <summary>
        /// 创建平安员工信息
        /// </summary>
        /// <param name="pinganStaff"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult CreatePinganStaff(PinganStaff pinganStaff, string password = null);

        /// <summary>
        /// 根据系统编号获取用户
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        [OperationContract]
        PinganStaff GetPinganStaff(long sysNo);

        /// <summary>
        /// 根据用户ID获取用户
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        //[OperationContract(Name = "GetPinganStaffByUserId")]
        //PinganStaff GetPinganStaff(string userId);

        /// <summary>
        /// 分页获取平安员工信息
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="condition"></param>
        /// <param name="matchingType"></param>
        /// <returns></returns>
        [OperationContract]
        IList<PinganStaff> GetPinganStaffs(int pageIndex, int pageSize, out int totalCount, string condition = null, ConditionMatchingType matchingType = ConditionMatchingType.Fuzzy);

        /// <summary>
        /// 修改平安员工信息
        /// </summary>
        /// <param name="pinganStaff"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult ModifyPinganStaff(PinganStaff pinganStaff);

        /// <summary>
        /// 根据UM帐号获取员工信息
        /// </summary>
        /// <param name="um"></param>
        /// <returns></returns>
        [OperationContract]
        PinganStaff GetByPinganStaffUMCode(string um);

        /// <summary>
        /// 根据扫描员工卡获取员工信息
        /// </summary>
        /// <param name="staffCardNo"></param>
        /// <returns></returns>
        [OperationContract]
        PinganStaff GetByStaffCardNo(string staffCardNo);
    }

    /// <summary>
    /// 第三方公司员工
    /// </summary>
    [ServiceContract]
    public interface IThirdpartyUserService
    {
        /// <summary>
        /// 创建第三方公司员工信息
        /// </summary>
        /// <param name="tUser"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult CreateThirdpartyUser(ThirdpartyUser tUser, string loginName, string password);

        /// <summary>
        /// 根据系统编号获取用户
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        [OperationContract]
        ThirdpartyUser GetThirdpartyUser(long sysNo);

        /// <summary>
        /// 根据用户ID获取用户
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        //[OperationContract(Name = "GetThirdpartyUserByUserId")]
        //ThirdpartyUser GetThirdpartyUser(string userId);

        /// <summary>
        /// 分页获取第三方用户信息
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="condition"></param>
        /// <param name="matchingType"></param>
        /// <returns></returns>
        [OperationContract]
        IList<ThirdpartyUser> GetThirdpartyUsers(int pageIndex, int pageSize, out int totalCount, string condition = null, string tpName = null, ConditionMatchingType matchingType = ConditionMatchingType.Fuzzy);

        /// <summary>
        /// 修改第三方员工信息
        /// </summary>
        /// <param name="tpUser"></param>
        /// <returns></returns>
        [OperationContract]
        OperationResult ModifyThirdpartyUser(ThirdpartyUser tpUser);
    }

}
