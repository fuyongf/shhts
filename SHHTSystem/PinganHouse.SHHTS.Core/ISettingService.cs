﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Entities;

namespace PinganHouse.SHHTS.Core
{
    public interface ISettingService
    {
        /// <summary>
        /// 创建配置节点
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parentId"></param>
        /// <param name="value"></param>
        /// <param name="desc"></param>
        /// <param name="remark"></param>
        /// <returns></returns>
        Setting CreateSettingItem(string name, string parentId, string value, string desc, string remark);

        /// <summary>
        /// 根据ID查找配置节信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Setting GetSettingItem(string id);

        /// <summary>
        /// 根据路径获取配置节点
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        Setting GetSettingItemByPath(string path);

        /// <summary>
        /// 获取指定节点的下级节点
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        IEnumerable<Setting> GetSettingItems(string parentId);

        /// <summary>
        /// 获取指定路径的下级节点
        /// </summary>
        /// <param name="parentPath"></param>
        /// <returns></returns>
        IEnumerable<Setting> GetSettingItemsByPath(string parentPath);

        /// <summary>
        /// 根据路径获取节点值
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        string GetSettingItemValueByPath(string path);

        /// <summary>
        /// 删除指定配置节点
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool DeleteSettingItem(string id);

        /// <summary>
        /// 修改指定节点值
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        bool ModifySetting(string id, string value, string desc);
    }
}
