﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.IO;
using PinganHouse.SHHTS.DataTransferObjects;


namespace PinganHouse.SHHTS.Core
{
    [ServiceContract]
    public interface IFileTransferService
    {
        /// <summary>
        /// 上传包含文件信息的流数据
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        [OperationContract]
        UploadResult Upload(Stream stream);
        /// <summary>
        /// 上传文件
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [OperationContract(Name = "UploadFile")]
        UploadResultMessage Upload(FileUploadMessage file);
        /// <summary>
        /// 断点上传
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [OperationContract]
        UploadResultMessage UploadAppend(FileUploadMessage file);

        /// <summary>
        /// 下载文件
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>]
        [OperationContract]
        FileDownloadMessage Download(FileQueryMessage query);

        /// <summary>
        /// 根据ID删除文件
        /// </summary>
        /// <param name="fileId"></param>
        [OperationContract]
        void DeleteById(string fileId);

        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="fileName"></param>
        [OperationContract]
        void DeleteByFileName(string fileName);
    }
}
