﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Core
{
    public interface IFingerService
    {
        /// <summary>
        /// 采集指纹信息
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <param name="fpCode"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        OperationResult CollectFinger(long userSysNo, string fpCode, string tag, long operatorSysNo);

        /// <summary>
        /// 删除指纹信息
        /// </summary>
        /// <param name="fpSysNo"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        OperationResult DeleteFinger(long fpSysNo, long operatorSysNo);

        /// <summary>
        /// 根据用户系统编号获取指纹信息
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        IList<UserFingerprint> GetFingerByUserSysNo(long userSysNo);

        /// <summary>
        /// 验证指纹特征
        /// </summary>
        /// <param name="fpCode"></param>
        /// <param name="userSysNo">返回指纹对应的用户系统编号</param>
        /// <returns></returns>
        bool VerifyFinger(string fpCode, out long userSysNo);
    }
}
