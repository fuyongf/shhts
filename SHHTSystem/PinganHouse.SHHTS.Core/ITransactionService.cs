﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Core
{
    public interface ITransactionService
    {
        /// <summary>
        /// 开始事务
        /// </summary>
        /// <returns></returns>
        bool Begin();

        /// <summary>
        /// 提交事务
        /// </summary>
        /// <returns></returns>
        bool Commit();

        /// <summary>
        /// 回滚事务
        /// </summary>
        /// <returns></returns>
        bool RollBack();
    }
}
