﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using PinganHouse.SHHTS.Entities;

namespace PinganHouse.SHHTS.MongoServer
{
    public class GridFSRepository : IGridFSRepository
    {

        static readonly MongoUrl url = new MongoUrl(ConfigurationManager.ConnectionStrings["mongoDB"].ConnectionString);
        static readonly MongoClient client = new MongoClient(url); 
        /// <summary>
        /// 数据库
        /// </summary>
        readonly MongoDatabase database;
        /// <summary>
        /// 表设置
        /// </summary>
        readonly MongoGridFSSettings setting;
        readonly MongoGridFS mongoGridFS;



        public GridFSRepository()
            : this("gfs")
        {
        }

        /// <summary>
        /// 文件存储服务
        /// </summary>
        /// <param name="root">应用程序文件类别</param>
        public GridFSRepository(string root)
        {
            var server = client.GetServer();       
            this.database = server.GetDatabase(url.DatabaseName);
            this.setting = new MongoGridFSSettings() { Root = root, };
            this.mongoGridFS = database.GetGridFS(setting);
        }

        /// <summary>
        /// 保存文件对象
        /// </summary>
        /// <param name="stream">文件流</param>
        /// <param name="remoteFileName">文件名</param>
        /// <returns></returns>
        public GridFSFileInfo Save(Stream stream, string remoteFileName)
        {
            return Save(stream, remoteFileName, null);
        }
        /// <summary>
        /// 保存文件对象
        /// </summary>
        /// <param name="stream">文件流</param>
        /// <param name="remoteFileName">文件名</param>
        /// <param name="contentType">文件类型</param>
        /// <returns></returns>
        public GridFSFileInfo Save(Stream stream, string remoteFileName, string contentType)
        {
            if (stream == null)
                throw new ArgumentNullException("stream为空");
            var mongoGridFSCreateOptions = new MongoGridFSCreateOptions {
                ContentType = contentType,
            };
            var fileInfo = mongoGridFS.Upload(stream, remoteFileName, mongoGridFSCreateOptions);
            return Map(fileInfo);
        }

        /// <summary>
        /// 修改已存储的文件
        /// </summary>
        /// <param name="id">需修改的存储文件id</param>
        /// <param name="stream">更新文件流</param>
        /// <param name="remoteFileName">更新文件名</param>
        /// <returns></returns>
        //public GridFSFileInfo Update(string id, Stream stream, string remoteFileName)
        //{
        //    if (stream == null)
        //        throw new ArgumentNullException("stream为空!");
        //    var objectID = this.ConverToObjectID(id);
        //    var options = new MongoGridFSCreateOptions
        //    {
        //        ChunkSize = setting.ChunkSize,
        //        Id = objectID,
        //        UploadDate = DateTime.UtcNow
        //    };
        //    using (database.RequestStart(ReadPreference.Primary))
        //    {
        //        mongoGridFS.DeleteById(objectID);
        //        var file = mongoGridFS.Upload(stream, remoteFileName, options);
        //        return Map(file);
        //    }
        //}

        /// <summary>
        /// 根据文件名查找文件
        /// </summary>
        /// <param name="remotFileName">文件名</param>
        /// <returns>文件列表</returns>
        public IEnumerable<GridFSFileInfo> FindByName(string remotFileName)
        {
            var files = mongoGridFS.Find(remotFileName);
            return Map(files);
        }

        public IEnumerable<GridFSFileInfo> Find(IMongoQuery query)
        {
            var files = mongoGridFS.Find(query);
            return Map(files);
        }

        /// <summary>
        /// 根据唯一id查找文件
        /// </summary>
        /// <param name="id">key</param>
        /// <returns>文件</returns>
        public GridFSFileInfo FindByID(string id)
        {
            var objectID = this.ConverToObjectID(id);
            var file = mongoGridFS.FindOneById(objectID);
            return Map(file);
        }

        /// <summary>
        /// 根据文件名查找最新文件
        /// </summary>
        /// <param name="remotFileName">文件名</param>
        /// <param name="version">The version to find (1 is oldest, -1 is newest, 0 is no sort).</param>
        /// <returns>文件</returns>
        public GridFSFileInfo FindOne(string remotFileName, int version = -1)
        {
            var file = mongoGridFS.FindOne(remotFileName, version);
            return Map(file);
        }

        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="obectID">唯一id</param>
        public void DeleteByID(string id)
        {
            mongoGridFS.DeleteById(this.ConverToObjectID(id));
        }

        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="remoteFileName">文件名（同一文件名可能有多个文件）</param>
        public void DeleteByName(string remoteFileName)
        {
            mongoGridFS.Delete(remoteFileName);
        }

        /// <summary>
        /// 是否存在
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        public bool Exists(string id)
        {
            var objectID = this.ConverToObjectID(id);
            return mongoGridFS.ExistsById(objectID);
        }

        /// <summary>
        /// 是否存在
        /// </summary>
        /// <param name="remoteFileName">文件名</param>
        /// <returns></returns>
        public bool ExistsByName(string remoteFileName)
        {
            return mongoGridFS.Exists(remoteFileName);
        }

        /// <summary>
        /// 打开指定文件的流
        /// </summary>
        /// <param name="id"></param>
        /// <param name="mode"></param>
        /// <param name="access"></param>
        /// <returns></returns>
        public Stream OpenStream(string id, FileMode mode, FileAccess access)
        {
            var objectID = this.ConverToObjectID(id);
            var file = mongoGridFS.FindOneById(objectID);
            if (file == null)
                return null;
            return file.Open(mode, access);
        }

        //public void Download(Stream stream, string fileName)
        //{
        //    mongoGridFS.Download(stream, fileName);
        //}


        /// <summary>
        /// 将mongo对象转换为应用程序对象
        /// </summary>
        /// <param name="mongoGridFSFileInfo"></param>
        /// <returns></returns>
        private GridFSFileInfo Map(MongoGridFSFileInfo mongoGridFSFileInfo)
        {
            if (mongoGridFSFileInfo != null && mongoGridFSFileInfo.Exists)
            {
                return new GridFSFileInfo(mongoGridFSFileInfo.Id.ToString(), mongoGridFSFileInfo.OpenRead(), mongoGridFSFileInfo.Name, mongoGridFSFileInfo.Length, mongoGridFSFileInfo.UploadDate);
            }
            return null;
        }

        private IEnumerable<GridFSFileInfo> Map(MongoCursor<MongoGridFSFileInfo> mongoGridFSFileInfo)
        {
            IList<GridFSFileInfo> fl = null;
            if(mongoGridFSFileInfo != null && mongoGridFSFileInfo.Count()>0)
            {
                fl = new List<GridFSFileInfo>();
                foreach (var file in mongoGridFSFileInfo)
                {
                    fl.Add(Map(file));
                }
            }
            return fl;
        }

        private ObjectId ConverToObjectID(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentNullException("id为空");
            return ObjectId.Parse(id);
        }
    }
}
