﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Entities
{
    public class MailMessageInfo:Message
    {

        public MailMessageInfo(string body, string toAddress)
            : base(body, toAddress)
        {
            this.cc = new List<string>();
            this.Attachments = new Dictionary<string, string>();
        }

        private List<string> cc;
        //public string CC
        //{
        //    get
        //    {
        //        return string.Join(",", cc);
        //    }
        //    set
        //    {
        //        if (!string.IsNullOrWhiteSpace(value))
        //            cc.AddRange(value.Split(',').ToList());

        //    }
        //}

        public List<string> CC { get { return this.cc; } }
        //public void AddCC(string toAddress)
        //{
        //    if (string.IsNullOrWhiteSpace(toAddress) || cc.Contains(toAddress))
        //        return;
        //    cc.Add(toAddress);
        //}

        //public List<string> GetCC()
        //{
        //    return cc;
        //}

        Encoding encoding = Encoding.UTF8;
        /// <summary>
        /// 编码
        /// </summary>
        public Encoding Encoding 
        { 
            get { return this.encoding; }
            set 
            {
                if (value != null)
                    encoding = value;
            }
        }

        /// <summary>
        /// 发送人名
        /// </summary>
        public string FromName { get; set; }

        /// <summary>
        /// 附件集合
        /// key 附件路径,value 附件名称
        /// </summary>
        public Dictionary<string, string> Attachments { get; set; }

        /// <summary>
        /// 主体是否为html
        /// </summary>
        public bool IsBodyHtml { get; set; }

    }
}
