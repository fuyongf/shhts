﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Entities
{
    public class ReceiptInfo : BaseEntity
    {
        /// <summary>
        /// 票据编号
        /// </summary>
        public string ReceiptNo { get; set; }

        /// <summary>
        /// 收款单位
        /// </summary>
        public string Payee { get; set; }

        /// <summary>
        /// 付款单位
        /// </summary>
        public string Payer { get; set; }

        /// <summary>
        /// 出具日期
        /// </summary>
        public DateTime ReceiptDate { get; set; }

        /// <summary>
        /// 收据金额
        /// </summary>
        public decimal Money { get; set; }


        #region 待废弃
        /// <summary>
        /// 所属订单
        /// </summary>
        public string OrderNo { get; set; }

        private IList<string> tradeDetails = new List<string>();
        /// <summary>
        /// 对应交易流水
        /// </summary>
        public IList<string> TradeDetails
        {
            get { return this.tradeDetails; }
            set
            {
                if (value != null)
                    this.tradeDetails = value;
            }
        }

        public string TradeDetail
        {
            get
            {
                return string.Join(",", tradeDetails);
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    foreach (var p in value.Split(','))
                    {
                        tradeDetails.Add(p);
                    }
                }
            }
        }

        /// <summary>
        /// 详细地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 邮编
        /// </summary>
        public string PostCode { get; set; }

        /// <summary>
        /// 联系方式
        /// </summary>
        public string MobilePhone { get; set; }
        #endregion

        #region 待新增
        /// <summary>
        /// 出据账户
        /// </summary>
        public long AccountSysNo { get; set; }

        /// <summary>
        /// 业务类型
        /// </summary>
        public OrderBizType OrderBizType { get; set; }
        #endregion

        /// <summary>
        /// 收据状态
        /// </summary>
        public ReceiptStatus Status { get; set; }

        /// <summary>
        /// 打印次数
        /// </summary>
        public int PrintCount { get; set; }

        #region 冗余字段
        public string CreateName { get; set; }

        public string ModifyName { get; set; }

        public string CaseId { get; set; }

        public string TenementAddress { get; set; }
        #endregion
    }

    public class ReceiptSerialInfo : ReceiptInfo
    {
        /// <summary>
        /// 票据流水号
        /// </summary>
        public string ReceiptSerialNo { get; set; }

        /// <summary>
        /// 验真码
        /// </summary>
        public string SecurityCode { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

    }
}
