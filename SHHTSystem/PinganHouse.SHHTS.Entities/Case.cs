﻿using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;

namespace PinganHouse.SHHTS.Entities
{
    /// <summary>
    /// 案件
    /// </summary>
    public class Case : BaseEntity
    {
        public Case()
        {
            buyers = new List<long>();
            sellers = new List<long>();
        }
        /// <summary>
        /// 案件编号
        /// </summary>
        public string CaseId { get; set; }

        private List<long> sellers;
        /// <summary>
        /// 卖家
        /// </summary>
        public List<long> Sellers
        {
            get { return sellers; }
            set { sellers = value; }
        }


        private List<long> buyers;

        public List<long> Buyers
        {
            get { return buyers; }
            set { buyers = value; }
        }


        /// <summary>
        /// 受理人
        /// </summary>
        public long OperatorSysNo { get; set; }

        /// <summary>
        /// 中介
        /// </summary>
        public long AgencySysNo { get; set; }


        /// <summary>
        /// 物业合同标识
        /// </summary>
        public string TenementContract { get; set; }

        /// <summary>
        /// 中介公司
        /// </summary>
        public long CompanySysNo { get; set; }

        /// <summary>
        /// 物业地址
        /// </summary>
        public string TenementAddress { get; set; }

        /// <summary>
        /// 是否资金托管
        /// </summary>
        public bool IsFund { get; set; }

        private string fundTrusteeshipContract;
        /// <summary>
        /// 托管资金合同号
        /// </summary>
        public string FundTrusteeshipContract
        {
            get { return this.fundTrusteeshipContract; }
            set
            {
                if (IsFund && string.IsNullOrWhiteSpace(value))
                    throw new ArgumentNullException("TrusteeshipContract", "TrusteeshipContract is null");
                this.fundTrusteeshipContract = value;
            }
        }

        /// <summary>
        /// 是否佣金托管
        /// </summary>
        public bool IsCommission { get; set; }

        private string commissionTrusteeshipContract;
        /// <summary>
        /// 佣金托管合同
        /// </summary>
        public string CommissionTrusteeshipContract
        {
            get { return this.commissionTrusteeshipContract; }
            set
            {
                if (IsCommission && string.IsNullOrWhiteSpace(value))
                    throw new ArgumentNullException("CommissionTrusteeshipContract", "CommissionTrusteeshipContract is null");
                this.commissionTrusteeshipContract = value;
            }
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark
        {
            get;
            set;
        }

        /// <summary>
        /// 已失效
        /// </summary>
        public bool Disabled { get; set; }

        /// <summary>
        /// 案件状态
        /// </summary>
        public CaseStatus CaseStatus { get; set; }


        /// <summary>
        /// 所属受理中心
        /// </summary>
        public ReceptionCenter ReceptionCenter { get; set; }

        /// <summary>
        /// 案件来源
        /// </summary>
        public SourceType SourceType { get; set; }

        /// <summary>
        /// 全局可查变量
        /// </summary>
        public bool GlobalFlag { get; set; }


        /// <summary>
        /// 添加买家
        /// </summary>
        /// <param name="buyer"></param>
        public void AddBuyer(params long[] buyer)
        {
            this.Add(buyers, buyer);
        }

        ///// <summary>
        ///// 添加卖家
        ///// </summary>
        ///// <param name="seller"></param>
        public void AddSeller(params long[] seller)
        {
            this.Add(sellers, seller);
        }

        private void Add(List<long> ps, params long[] person)
        {
            if (person == null || person.Length == 0)
                throw new ArgumentException("未提供买家或卖家");
            foreach (var p in person)
            {
                if (!ps.Contains(p))
                    ps.Add(p);
            }
        }
    }

    public class CaseIdQueryMap 
    {
        public string CaseId { get; set; }
    }
}
