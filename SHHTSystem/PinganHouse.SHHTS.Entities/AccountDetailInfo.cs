﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Entities
{
    public class AccountDetailInfo : BaseEntity
    {
        public AccountDetailInfo(AccountDetailType type)
        {
            Type = type;
        }

        /// <summary>
        /// 资金账户系统编号
        /// </summary>
        public long AccountSysNo { get; set; }

        /// <summary>
        /// 订单编号(描述资金明细业务关系)
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 金额(+:入,-:出)
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// 资金明细类型
        /// </summary>
        public AccountDetailType Type { get; private set; }
    }

    ///// <summary>
    ///// 充值明细
    ///// </summary>
    //public class RechargeAccountDetailInfo : AccountDetailInfo
    //{
    //    public RechargeAccountDetailInfo()
    //        : base(AccountDetailType.Recharge)
    //    {
    //    }
    //}

    ///// <summary>
    ///// 转账明细
    ///// </summary>
    //public class TransferAccountDetailInfo : AccountDetailInfo
    //{
    //    public TransferAccountDetailInfo()
    //        : base(AccountDetailType.Transfer)
    //    {
    //    }
    //}

    ///// <summary>
    ///// 提现明细
    ///// </summary>
    //public class WithdrawAccountDetailInfo : AccountDetailInfo
    //{
    //    public WithdrawAccountDetailInfo()
    //        : base(AccountDetailType.Withdraw)
    //    {
    //    }
    //}

    ///// <summary>
    ///// 退款明细
    ///// </summary>
    //public class RefundAccountDetailInfo : AccountDetailInfo
    //{
    //    public RefundAccountDetailInfo()
    //        : base(AccountDetailType.Refund)
    //    {
    //    }
    //}
}
