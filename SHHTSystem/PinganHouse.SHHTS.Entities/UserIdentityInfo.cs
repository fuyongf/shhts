﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Entities
{
    public class UserIdentityInfo : BaseEntity
    {
        /// <summary>
        /// 证件类型
        /// </summary>
        public CertificateType CertificateType { get; set; }

        /// <summary>
        /// 国籍
        /// </summary>
        public string Nationality { get; set; }

        /// <summary>
        /// 证件号码
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        /// 出生年月
        /// </summary>
        public DateTime? Birthday { get; set; }

        /// <summary>
        /// 住址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 签证机构
        /// </summary>
        public string VisaAgency { get; set; }

        /// <summary>
        /// 民族
        /// </summary>
        public Nation Nation { get; set; }

        /// <summary>
        /// 有效日期(开始)
        /// </summary>
        public DateTime? EffectiveDate { get; set; }

        /// <summary>
        /// 失效日期
        /// </summary>
        public DateTime? ExpiryDate { get; set; }

        /// <summary>
        /// 证件照
        /// </summary>
        public byte[] Photo { get; set; }

        /// <summary>
        /// 可信的
        /// </summary>
        public bool IsTrusted { get; set; }
    }

    public class UserFingerprintInfo : BaseEntity 
    {
        /// <summary>
        /// 用户系统编号
        /// </summary>
        public long UserSysNo { get; set; }

        /// <summary>
        /// 指纹
        /// </summary>
        public string Fingerprint { get; set; }

        /// <summary>
        /// 标签
        /// </summary>
        public string Tag { get; set; }
    }
}
