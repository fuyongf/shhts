﻿using Newtonsoft.Json;
using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Entities
{
    public class WXTemplateMessage:BaseEntity
    {
        /// <summary>
        /// 消息id
        /// </summary>
        public string MsgId { get; set; }

        /// <summary>
        /// 发送次数
        /// </summary>
        public int SendsCount { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 发送状态
        /// </summary>
        public WXStatus Status { get; set; }

        private string message;
        /// <summary>
        /// 消息体
        /// </summary>
        public string Message
        {
            get
            {
                if (string.IsNullOrWhiteSpace(message) && data != null && data.Count > 0)
                    message = JsonConvert.SerializeObject(data);
                return message;
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    if (!value.StartsWith("{") && !value.EndsWith("}"))
                        throw new ArgumentException("Message 不符合格式");
                    data = JsonConvert.DeserializeObject<Dictionary<string, object>>(value);
                }
            }
        }

        private IDictionary<string, object> data = new Dictionary<string, object>();
        /// <summary>
        /// 消息集合
        /// </summary>
        public IDictionary<string, object> Data
        {
            get
            {
                return this.data;
            }
            set
            {
                this.data = value;
            }
        }

    }
}
