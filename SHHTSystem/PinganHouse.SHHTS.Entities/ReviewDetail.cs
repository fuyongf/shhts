﻿using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Entities
{
    /// <summary>
    /// 复核记录
    /// </summary>
    public class ReviewDetail:BaseEntity
    {
        /// <summary>
        /// 复核编号
        /// </summary>
        public long ReviewSysNo { get; set; }

        /// <summary>
        /// 是否有效
        /// </summary>
        public bool IsValid { get; set; }

        /// <summary>
        /// 复核人
        /// </summary>
        public long UserSysNo { get; set; }

        /// <summary>
        /// 复核方式
        /// </summary>
        public ReviewMode ReviewMode { get; set; }

        /// <summary>
        /// 复核顺序
        /// </summary>
        public int CurrentOrder { get; set; }
         
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
}
