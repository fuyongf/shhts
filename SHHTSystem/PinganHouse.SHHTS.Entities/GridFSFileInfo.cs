﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Entities
{
    /// <summary>
    /// 文件信息
    /// </summary>
    [Serializable]
    public class GridFSFileInfo
    {

        public GridFSFileInfo(string id, Stream stream, string remoteFileName, long lentgh, DateTime uploadDate)
        {
            this.id = id;
            this.fileStream = stream;
            this.remoteFileName = remoteFileName;
            this.lentgh = lentgh;
            this.uploadDate = uploadDate.ToLocalTime();
        }

        private string id;
        /// <summary>
        /// Gets the GridFS file Id.
        /// 存储文件唯一ID
        /// </summary>
        public string ID
        {
            get { return this.id; }    
        }

        private string remoteFileName;
        /// <summary>
        /// 文件别名，建议为对应系统的唯一标识
        /// 可根据该字段查找同一别名的多个文件
        /// </summary>
        public string RemoteFileName { 
            get { return this.remoteFileName; }
        }

        private long lentgh;
        /// <summary>
        /// Gets the file lenth.
        /// </summary>
        public long Length {
            get { return this.lentgh; }
        }

        private DateTime uploadDate;
        /// <summary>
        /// 上传时间
        /// </summary>
        public DateTime UploadDate 
        {
            get { return this.uploadDate; } 
        }

        private Stream fileStream;
        /// <summary>
        /// 文件流
        /// </summary>
        public Stream FileStream 
        { 
            get { return this.fileStream; }
        }
    }
}
