﻿using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Entities
{
    public class ParterUserInfo:BaseEntity
    {
        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// 用户的标识，对当前公众号唯一
        /// </summary>
        public string OpenID { get; set; }

        /// <summary>
        /// 系统用户标识
        /// </summary>
        public long? UserSysNo { get; set; }

        /// <summary>
        /// 是否关注
        /// false 代表未关注， true 关注
        /// </summary>
        public bool Subscribe { get; set; }

        /// <summary>
        /// 所在城市
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// 所在国家
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// 所在省份
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// 用户性别
        /// </summary>
        public Gender Sex { get; set; }
    }
}
