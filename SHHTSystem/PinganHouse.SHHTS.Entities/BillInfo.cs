﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.Entities
{
    public class BillInfo : BaseEntity
    {
        /// <summary>
        /// 票据编号
        /// </summary>
        public string BillNo { get; set; }

        /// <summary>
        /// 票据类别
        /// </summary>
        public BillCategory BillCategory { get; set; }

        /// <summary>
        /// 票据类型
        /// </summary>
        public BillType BillType { get; set; }

        /// <summary>
        /// 交易流水编号
        /// </summary>
        public string TradeNo { get; set; }

        /// <summary>
        /// 支付银行卡号
        /// </summary>
        public string PayAccount { get; set; }

        /// <summary>
        /// 开户名
        /// </summary>
        public string Payer { get; set; }

        /// <summary>
        /// 票据日期
        /// </summary>
        public DateTime BillDate { get; set; }

        /// <summary>
        /// 金额
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// 终端编号
        /// </summary>
        public string TerminalNo { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Decription { get; set; }
    }

    public class BillQueryInfo : BillInfo
    {
        public ReconciliationStatus ReconciliationStatus { get; set; }

        public TradeStatus TradeStatus { get; set; }

        public bool IsBilled { get; set; }
    }
}
