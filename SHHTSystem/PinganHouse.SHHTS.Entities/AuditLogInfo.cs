﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Entities
{
    public class AuditLogInfo : BaseEntity
    {
        /// <summary>
        /// 审核对象
        /// </summary>
        public AuditObjectType AuditObjectType { get; set; }

        /// <summary>
        /// 对象ID
        /// </summary>
        public string SubjectId { get; set; }

        /// <summary>
        /// 修改值
        /// </summary>
        public int ChangeValue { get; set; }

        /// <summary>
        /// 审批部门
        /// </summary>
        public string Department { get; set; }

        /// <summary>
        /// 审核人编号
        /// </summary>
        public long? AuditSysNo { get; set; }

        /// <summary>
        /// 审核人姓名
        /// </summary>
        public string AuditUserName { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
}
