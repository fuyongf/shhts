﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Entities
{
    public class PaymentOrderExtend : BaseEntity
    {
        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 银行代码
        /// </summary>
        public string BankCode { get; set; }

        /// <summary>
        /// 开户行
        /// </summary>
        public string SubBankName { get; set; }

        /// <summary>
        /// 银行帐号
        /// </summary>
        public string BankAccountNo { get; set; }

        /// <summary>
        /// 开户名
        /// </summary>
        public string AccountName { get; set; }
    }

    public class PaidOffOrderExtend : BaseEntity
    {
        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 银行代码
        /// </summary>
        public string BankCode { get; set; }

        /// <summary>
        /// 开户行
        /// </summary>
        public string SubBankName { get; set; }

        /// <summary>
        /// 银行帐号
        /// </summary>
        public string BankAccountNo { get; set; }

        /// <summary>
        /// 开户名
        /// </summary>
        public string AccountName { get; set; }
    }

    public class TransferOrderExtend : BaseEntity 
    {
        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 收款子账户类型
        /// </summary>
        public OrderBizType ReceiveAccountType { get; set; }
    }

    public class WithdrawOrderExtend : BaseEntity
    {
        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 银行代码
        /// </summary>
        public string BankCode { get; set; }

        /// <summary>
        /// 开户行
        /// </summary>
        public string SubBankName { get; set; }

        /// <summary>
        /// 银行帐号
        /// </summary>
        public string BankAccountNo { get; set; }

        /// <summary>
        /// 开户名
        /// </summary>
        public string AccountName { get; set; }
    }
}
