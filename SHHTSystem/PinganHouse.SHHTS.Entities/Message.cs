﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Entities
{
    public class Message
    {
        public Message()
        {
            this.toAddresses = new List<string>();
        }

        public Message(string body, string toAddress):this()
        {
            if (toAddress == null)
                throw new ArgumentException("toAddress is invalid");
            this.body = body;
            toAddresses.Add(toAddress);
        }

        private string body;
        /// <summary>
        /// 主体
        /// </summary>
        public string Body 
        {
            get { return this.body; }
            set 
            {
                //if (string.IsNullOrWhiteSpace(value))
                //    throw new ArgumentNullException("Body", "Body is null");
                this.body = value;
            }
        }

        /// <summary>
        /// 接收地址
        /// </summary>
        //public string ToAddress 
        //{
        //    get 
        //    { 
        //        return string.Join(",", toAddresses); 
        //    }
        //    set 
        //    {
        //        if (!string.IsNullOrWhiteSpace(value))
        //            toAddresses.AddRange(value.Split(',').ToList());
                
        //    }
        //}


        public string Subject { get; set; }

        /// <summary>
        /// 发送地址
        /// </summary>
        public string FromAddress { get; set; }

        private List<string> toAddresses;
        /// <summary>
        /// 接收地址列表
        /// </summary>
        public  List<string> ToAddresses
        {
            get{return this.toAddresses;}
        }

        /// <summary>
        /// 添加接收地址
        /// </summary>
        /// <param name="toAddress"></param>
        //public void AddAddress(string toAddress)
        //{
        //    if (string.IsNullOrWhiteSpace(toAddress) || toAddresses.Contains(toAddress))
        //        return;
        //    toAddresses.Add(toAddress);
        //}

        //public List<string> GetAddress()
        //{
        //    return toAddresses;
        //}

        public override string ToString()
        {
            return string.Format("【{0}】 {1}", Subject, Body);
        }

        /// <summary>
        /// 是否已发送
        /// </summary>
        public bool IsSend { get; set; }

        /// <summary>
        /// 发送时间
        /// </summary>
        public DateTime SendDate { get; set; }
    }
} 
