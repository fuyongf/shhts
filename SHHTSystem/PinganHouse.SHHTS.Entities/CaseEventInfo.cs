﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PinganHouse.SHHTS.Enumerations;
using Newtonsoft.Json;

namespace PinganHouse.SHHTS.Entities
{
    /// <summary>
    /// CaseEventInfo
    /// </summary>
    public class CaseEventInfo : BaseEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CaseEventInfo"/> class.
        /// </summary>
        public CaseEventInfo()
        {
            OtherDatas = new Dictionary<string, object>();
        }

        /// <summary>
        /// 案件编号
        /// </summary>
        public string CaseId { get; set; }

        /// <summary>
        /// 源状态
        /// </summary>
        public CaseStatus SourceStatus { get; set; }

        /// <summary>
        /// 事件类型
        /// </summary>
        public CaseStatus EventType { get; set; }

        /// <summary>
        /// 已完成
        /// </summary>
        public bool Finished { get; set; }

        /// <summary>
        /// 操作人
        /// </summary>
        public long OperatorSysNo { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 其他信息集合
        /// </summary>
        public IDictionary<string, object> OtherDatas
        {
            get;
            set;
        }

        //private string otherData;
        ///// <summary>
        ///// 其他消息
        ///// </summary>
        //public string OtherData
        //{
        //    get
        //    {
        //        if (string.IsNullOrWhiteSpace(otherData) && OtherDatas!=null && OtherDatas.Count > 0)
        //            otherData = JsonConvert.SerializeObject(data);
        //        return otherData;
        //    }
        //    set
        //    {
        //        if (!string.IsNullOrWhiteSpace(value))
        //        {
        //            if (!value.StartsWith("{") && !value.EndsWith("}"))
        //                throw new ArgumentException("OtherData 不符合格式");
        //            data = JsonConvert.DeserializeObject<Dictionary<string, object>>(value);
        //        }
        //    }
        //}
    }
}
