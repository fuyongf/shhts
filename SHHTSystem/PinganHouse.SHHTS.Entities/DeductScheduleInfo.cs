﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Entities
{
    public class DeductScheduleInfo : BaseEntity
    {
        /// <summary>
        /// 申请方账户
        /// </summary>
        public long AccountSysNo { get; set; }

        /// <summary>
        /// 扣款用途
        /// </summary>
        public OrderBizType DeductType { get; set; }

        /// <summary>
        /// 扣款源
        /// </summary>
        public IList<OrderBizType> LimitSource { get; set; }

        private string LimitSourceValue
        {
            get
            {
                return LimitSource == null ? null : string.Join(",", LimitSource.Select(l => (int)l));
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    LimitSource = new List<OrderBizType>();
                    foreach (var p in value.Split(','))
                    {
                        LimitSource.Add((OrderBizType)int.Parse(p));
                    }
                }
            }
        }

        /// <summary>
        /// 申请金额
        /// </summary>
        public decimal ApplyAmount { get; set; }

        /// <summary>
        /// 已付金额
        /// </summary>
        public decimal DeductedAmount { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 已完成
        /// </summary>
        public bool Finished
        {
            get { return ApplyAmount == DeductedAmount; }
        }

        /// <summary>
        /// 版本号
        /// </summary>
        public long Verson { get; set; }
    }

    public class DeductScheduleQueryInfo : DeductScheduleInfo 
    {
        private string LimitSourceValue
        {
            get
            {
                return LimitSource == null ? null : string.Join(",", LimitSource.Select(l => (int)l));
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    LimitSource = new List<OrderBizType>();
                    foreach (var p in value.Split(','))
                    {
                        LimitSource.Add((OrderBizType)int.Parse(p));
                    }
                }
            }
        }

        /// <summary>
        /// 案件编号
        /// </summary>
        public string CaseId { get; set; }

        /// <summary>
        /// 物业地址
        /// </summary>
        public string TenementAddress { get; set; }

        /// <summary>
        /// 账户类型
        /// </summary>
        public AccountType AccountType { get; set; }

        public string TenementContract { get; set; }
    }

}
