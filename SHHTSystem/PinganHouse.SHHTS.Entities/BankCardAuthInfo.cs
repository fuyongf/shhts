﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Entities
{
    public class BankCardAuthInfo : BaseEntity
    {
        /// <summary>
        /// 案件编号
        /// </summary>
        public string CaseId { get; set; }

        /// <summary>
        /// 账户性质
        /// </summary>
        public BankAccountNature AccountNature { get; set; }

        /// <summary>
        /// 卡号
        /// </summary>
        public string CardNo { get; set; }

        /// <summary>
        /// 银行卡类型
        /// </summary>
        public BankCardType BankCardType { get; set; }

        /// <summary>
        /// 银行代码
        /// </summary>
        public string BankCode { get; set; }

        /// <summary>
        /// 开户名
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// 开户证件号
        /// </summary>
        public string IdentityNo { get; set; }

        /// <summary>
        /// 绑定手机号
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// 开户行名称
        /// </summary>
        public string SubBankName { get; set; }

        /// <summary>
        /// 已认证
        /// </summary>
        public bool IsAuthed { get; set; }
    }
}
