﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Entities
{
    /// <summary>
    /// 产调结果
    /// </summary>
    public class PropertySurvey:BaseEntity
    {
        /// <summary>
        /// 产证编号
        /// </summary>
        public string TenementContract { get; set; }

        /// <summary>
        /// 申请人
        /// </summary>
        public long? ProposerSysNo { get; set; }

        /// <summary>
        /// 是否抵押
        /// </summary>
        public bool? IsGuaranty { get; set; }

        /// <summary>
        /// 是否查封
        /// </summary>
        public bool? IsAttachment { get; set; }

        /// <summary>
        /// 是否租凭
        /// </summary>
        public bool? IsLeasing { get; set; }

        /// <summary>
        /// 产调时间
        /// </summary>
        public DateTime? SurveyDate { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
}
