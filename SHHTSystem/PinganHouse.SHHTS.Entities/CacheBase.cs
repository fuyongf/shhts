﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.Entities
{
    [Serializable]
    public class CacheBase<T>
    {
        const int Year = 100;
        private DateTime expireTime;
        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime ExpireTime 
        {
            get 
            { 
                return expireTime;
            }
            set 
            {
                if (value == default(DateTime))
                    value = DateTime.Now.AddYears(Year);
                expireTime = value;
            }
        }

        /// <summary>
		/// 是否已过期
		/// </summary>
        
        public bool IsExpired { get { return ExpireTime.ToLocalTime() < DateTime.Now; } }

        /// <summary>
        /// 缓存对象
        /// </summary>
        public T TValue { get; set; }

        /// <summary>
        /// 缓存token
        /// </summary>
        public string Id { get; set; }

    }
}
