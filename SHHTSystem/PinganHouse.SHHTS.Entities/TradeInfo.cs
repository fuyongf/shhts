﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.Entities
{
    public class TradeInfo : BaseEntity
    {
        private string m_TradeNo;
        /// <summary>
        /// 交易流水号
        /// </summary>
        public string TradeNo
        {
            get
            {
                if (m_TradeNo == null)
                {
                    DateTime now = DateTime.Now;
                    string ts = TimeStamp.Get(now).ToString();
                    m_TradeNo = string.Concat(now.ToString("yyyyMMdd"),
                        ts.Substring(ts.Length - 5, 5),
                        RandomHelper.GetNumber(999, 3));
                }
                return m_TradeNo;
            }
            set
            {
                if (m_TradeNo != null)
                    throw new Exception("TradeNo can't be modify.");
                m_TradeNo = value;
            }
        }
        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderNo { get; set; }

        ///// <summary>
        ///// 支付方式
        ///// </summary>
        //public PaymentChannel Channel { get; set; }

        /// <summary>
        /// 银行流水号
        /// </summary>
        public string SerialNo { get; set; }

        /// <summary>
        /// 交易金额
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// 支付时间
        /// </summary>
        public DateTime? PaymentDate { get; set; }

        /// <summary>
        /// 银行帐号
        /// </summary>
        public string BankAccountNo { get; set; }

        /// <summary>
        /// 银行代码
        /// </summary>
        public string BankCode { get; set; }

        /// <summary>
        /// 开户行
        /// </summary>
        public string SubBankName { get; set; }

        /// <summary>
        /// 开户名
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// 交易状态
        /// </summary>
        public TradeStatus Status { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 版本号
        /// </summary>
        public long Verson { get; set; }

        #region 入账交易属性

        #region 待废弃
        /// <summary>
        /// 已出收据
        /// </summary>
        public bool IsBilled { get; set; }

        /// <summary>
        /// 收据已提交到财务系统
        /// </summary>
        public bool IsSendBilled { get; set; }
        #endregion

        /// <summary>
        /// 对账状态
        /// </summary>
        public ReconciliationStatus ReconciliationStatus { get; set; }
        #endregion

        #region 冗余查询字段
        /// <summary>
        /// 案件编号
        /// </summary>
        public string CaseId { get; set; }

        /// <summary>
        /// 物业地址
        /// </summary>
        public string TenementAddress { get; set; }

        /// <summary>
        /// 账户类型
        /// </summary>
        public AccountType AccountType { get; set; }

        /// <summary>
        /// 支付方式
        /// </summary>
        public PaymentChannel PaymentChannel { get; set; }

        /// <summary>
        /// 订单类型
        /// </summary>
        public OrderType OrderType { get; set; }

        /// <summary>
        /// 业务类型
        /// </summary>
        public OrderBizType OrderBizType { get; set; }

        /// <summary>
        /// 终端编号
        /// </summary>
        public string TerminalNo { get; set; }
        #endregion
    }
}
