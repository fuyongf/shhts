﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.Utils;

namespace PinganHouse.SHHTS.Entities
{
    public class OrderInfo : BaseEntity
    {
        private string m_OrderNo;
        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderNo
        {
            get
            {
                if (m_OrderNo == null)
                {
                    DateTime now = DateTime.Now;
                    string ts = TimeStamp.Get(now).ToString();
                    m_OrderNo = string.Concat(now.ToString("yyyyMMdd"), (int)OrderType,
                        ts.Substring(ts.Length - 4, 4),
                        RandomHelper.GetNumber(999, 3));
                }
                return m_OrderNo;
            }
            set
            {
                if (m_OrderNo != null)
                    throw new Exception("OrderNo can't be modify.");
                m_OrderNo = value;
            }
        }

        public OrderType OrderType { get; set; }

        /// <summary>
        /// 订单类型
        /// </summary>
        public OrderBizType OrderBizType { get; set; }

        /// <summary>
        /// 订单状态
        /// </summary>
        public OrderStatus OrderStatus { get; set; }

        /// <summary>
        /// 审核状态
        /// </summary>
        public AuditStatus AuditStatus { get; set; }

        /// <summary>
        /// 支付方式
        /// </summary>
        public PaymentChannel PaymentChannel { get; set; }

        /// <summary>
        /// 账户编号
        /// </summary>
        public long AccountSysNo { get; set; }

        /// <summary>
        /// 对方账户(收款订单：空，付款(转账)订单：收款账户，提现(退款)订单：空)
        /// </summary>
        public long? OppositeAccount { get; set; }

        /// <summary>
        /// 订单总金额
        /// </summary>
        public decimal OrderAmount { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 版本号
        /// </summary>
        public long Verson { get; set; }

    }

    public class OrderQueryInfo : OrderInfo 
    {
        /// <summary>
        /// 案件编号
        /// </summary>
        public string CaseId { get; set; }

        /// <summary>
        /// 物业地址
        /// </summary>
        public string TenementAddress { get; set; }

        /// <summary>
        /// 到账金额
        /// </summary>
        public decimal CollectedBalance { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreateUserName { get; set; }

        /// <summary>
        /// 账户类型
        /// </summary>
        public AccountType AccountType { get; set; }

        /// <summary>
        /// 交易状态
        /// </summary>
        public TradeStatus TradeStatus { get; set; }
    }

    public class RefundOrderQueryInfo : BaseEntity
    {
        public string OrderNo { get; set; }

        public string CaseId { get; set; }

        public decimal OrderAmount { get; set; }

        public string CreateUserName { get; set; }

        public OrderStatus OrderStatus { get; set; }

        public AuditStatus AuditStatus { get; set; }

        public string Remark { get; set; }
    }
}
