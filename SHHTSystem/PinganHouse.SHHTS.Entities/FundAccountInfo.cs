﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Entities
{
    public class FundAccountInfo : BaseEntity
    {
        /// <summary>
        /// 主体编号
        /// </summary>
        public string SubjectId { get; set; }

        /// <summary>
        /// 账户类型
        /// </summary>
        public AccountType AccountType { get; set; }

        /// <summary>
        /// 是否已锁定
        /// </summary>
        public bool IsLocked { get; set; }

        /// <summary>
        /// 版本
        /// </summary>
        public long Verson { get; set; }
    }

    public class SubFundAccountInfo : BaseEntity
    {
        /// <summary>
        /// 主帐号系统编号
        /// </summary>
        public long AccountSysNo { get; set; }

        /// <summary>
        /// 子帐号类型
        /// </summary>
        public AccountCategory AccountCategory { get; set; }

        /// <summary>
        /// 余额
        /// </summary>
        public decimal Balance { get; set; }

        /// <summary>
        /// 待入账金额
        /// </summary>
        public decimal PreEntryMoney { get; set; }

        /// <summary>
        /// 锁定金额
        /// </summary>
        public decimal LockedMoney { get; set; }

        /// <summary>
        /// 冻结金额
        /// </summary>
        public decimal FrozenMoney { get; set; }

        /// <summary>
        /// 版本
        /// </summary>
        public long Verson { get; set; }
    }
}
