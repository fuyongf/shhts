﻿using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Entities
{
    public class CaseTrail : BaseEntity
    {

        /// <summary>
        /// 案件编号
        /// </summary>
        public string CaseId { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 进度
        /// </summary>
        public ProccessType ProccessType { get; set; }

        /// <summary>
        /// 跟进人
        /// </summary>
        public string ModifyUserName { get; set; }
    }
}
