﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Entities
{
    public class BaseEntity
    {
        #region Base Properties

        /// <summary>
        /// 系统编号(主键)
        /// </summary>
        public long SysNo { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// 创建人系统编号
        /// </summary>
        public long? CreateUserSysNo { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyDate { get; set; }

        /// <summary>
        /// 修改人系统编号
        /// </summary>
        public long? ModifyUserSysNo { get; set; }

        /// <summary>
        /// 是否已删除
        /// </summary>
        public bool IsDeleted { get; set; }

        #endregion
    }
}
