﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Entities
{
    public class UserExend:BaseEntity
    {
        /// <summary>
        /// 是否需发送短信
        /// </summary>
        public bool IsSendSms { get; set; }
    }
}
