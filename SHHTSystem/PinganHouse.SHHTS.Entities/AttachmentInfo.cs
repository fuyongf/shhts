﻿using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Entities
{
    /// <summary>
    /// 附件信息
    /// </summary>
    public class AttachmentInfo:BaseEntity
    {
        /// <summary>
        /// 关联业务编号
        /// </summary>
        public string AssociationNo { get; set; }
        /// <summary>
        /// 附件id
        /// </summary>
        public string FileId { get; set; }

        /// <summary>
        /// 附件类型
        /// </summary>
        public AttachmentType AttachmentType { get; set; }

        /// <summary>
        /// 附件名称
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

    }
}
