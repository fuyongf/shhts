﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Entities
{
    public class CaseRemarkInfo : BaseEntity
    {
        /// <summary>
        /// 物业名称
        /// </summary>
        public string TenementName { get; set; }

        /// <summary>
        /// 环线位置
        /// </summary>
        public string LoopLinePosition { get; set; }

        /// <summary>
        /// 建筑面积
        /// </summary>
        public decimal? CoveredArea { get; set; }

        /// <summary>
        /// 房间数
        /// </summary>
        public int? RoomCount { get; set; }

        /// <summary>
        /// 厅数
        /// </summary>
        public int? HallCount { get; set; }

        /// <summary>
        /// 卫生间数
        /// </summary>
        public int? ToiletCount { get; set; }

        /// <summary>
        /// 楼层数
        /// </summary>
        public int? FloorCount { get; set; }

        /// <summary>
        /// 竣工时间
        /// </summary>
        public DateTime? BuiltCompletedDate { get; set; }

        /// <summary>
        /// 居间价格
        /// </summary>
        public decimal? MediatorPrice { get; set; }

        /// <summary>
        /// 网签价格
        /// </summary>
        public decimal? NetlabelPrice { get; set; }

        /// <summary>
        /// 真实成交价格
        /// </summary>
        public decimal? RealPrice { get; set; }

        /// <summary>
        /// 是否做低房价
        /// </summary>
        public bool? IsLowPrice { get; set; }

        /// <summary>
        /// 交易类型
        /// </summary>
        public TradeType TradeType { get; set; }

        /// <summary>
        /// 物业性质
        /// </summary>
        public TenementNature TenementNature { get; set; }

        /// <summary>
        /// 居住类型
        /// </summary>
        public ResideType ResideType { get; set; }

        /// <summary>
        /// 是否首次购买
        /// </summary>
        public bool? FirstTimeBuy { get; set; }

        ///// <summary>
        ///// 获取或设置是否已满2年.
        ///// </summary>
        //public bool? BuyOverTwoYears { get; set; }
        ///// <summary>
        ///// 获取或设置是否已满5年.
        ///// </summary>
        //public bool? BuyOverFiveYears { get; set; } 
               
        /// <summary>
        /// 购买已满年限
        /// </summary>
        public BuyOverYears BuyOverYears { get; set; }

        /// <summary>
        /// 购买年限
        /// </summary>
        public int? PurchaseYears { get; set; }

        /// <summary>
        /// 是否是唯一住房
        /// </summary>
        public bool? OnlyHousing { get; set; }

        /// <summary>
        /// 车位地址
        /// </summary>
        public string CarportAddress { get; set; }

        /// <summary>
        /// 车位面积
        /// </summary>
        public decimal? CarportArea { get; set; }

        /// <summary>
        /// 车位价格
        /// </summary>
        public decimal? CarportPrice { get; set; }

        /// <summary>
        /// 获取或设置车位上手价格.
        /// </summary>
        public decimal? CarportRawPrice { get; set; }

        /// <summary>
        /// 是否补地价
        /// </summary>
        public bool? IsRepairLandPrice { get; set; }

        /// <summary>
        /// 是否国安审批
        /// </summary>
        public bool? IsNSAApproval { get; set; }

        /// <summary>
        /// 是否历史保护建筑
        /// </summary>
        public bool? IsProtectiveBuilding { get; set; }

        /// <summary>
        /// 是否公证
        /// </summary>
        public bool? IsNotarize { get; set; }

        /// <summary>
        /// 公证员
        /// </summary>
        [Obsolete("use NotarySysNo instead")]
        public string Notary { get; set; }

        /// <summary>
        /// 公证员联系方式
        /// </summary>
        [Obsolete("use NotarySysNo instead")]
        public string NotaryContact { get; set; }

        /// <summary>
        /// Gets or sets the notary system no.
        /// </summary>
        public long NotarySysNo { get; set; }

        /// <summary>
        /// 获取或设置物业上手价格.
        /// </summary>
        public decimal? TenementRawPrice { get; set; }
    }
}
