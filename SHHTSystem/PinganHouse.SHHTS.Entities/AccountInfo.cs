﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Entities
{
    public class AccountInfo : BaseEntity
    {
        /// <summary>
        /// 主体编号
        /// </summary>
        public string SubjectId { get; set; }

        /// <summary>
        /// 账户类型
        /// </summary>
        public AccountType AccountType { get; set; }

        /// <summary>
        /// 账户类别
        /// </summary>
        public AccountCategory AccountCategory { get; set; }

        /// <summary>
        /// 余额
        /// </summary>
        public decimal Balance { get; set; }

        /// <summary>
        /// 可开收据余额
        /// </summary>
        public decimal ReceiptBalance { get; set; }

        /// <summary>
        /// 待入账金额
        /// </summary>
        public decimal PreEntryMoney { get; set; }

        /// <summary>
        /// 锁定金额
        /// </summary>
        public decimal LockedMoney { get; set; }

        /// <summary>
        /// 冻结金额
        /// </summary>
        public decimal FrozenMoney { get; set; }

        /// <summary>
        /// 是否已锁定
        /// </summary>
        public bool IsLocked { get; set; }

        /// <summary>
        /// 版本
        /// </summary>
        public long Verson { get; set; }
    }

    public class AccountOperationLog : BaseEntity
    {
        /// <summary>
        /// 操作类型
        /// </summary>
        public AccountOperationType OperationType { get; set; }

        /// <summary>
        /// 账户编号
        /// </summary>
        public long AccountSysNo { get; set; }

        /// <summary>
        /// 交易编号
        /// </summary>
        public string RefBizNo { get; set; }

        /// <summary>
        /// 操作金额
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }

    public class AccountLedgerInfo
    {
        public string CaseId { get; set; }

        public string TenementContract { get; set; }

        public string TenementAddress { get; set; }

        public decimal BuyerBalance { get; set; }

        public decimal SellerBalance { get; set; }
    }

    public class CaseAccountTradeInfo
    {
        public AccountType AccountType { get; set; }

        public OrderType OrderType { get; set; }

        public decimal Amount { get; set; }
    }
}
