﻿using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Entities
{
    public class CaseUserRelation
    {
        public long SysNo { get; set; }
        public string CaseId { get; set; }

        public long UserSysNo { get; set; }

        public CaseUserType CaseUserType { get; set; }

        public DateTime CreateDate { get; set; }

        //public string RealName { get; set; }
    }
}
