﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.Entities
{
    /// <summary>
    /// 用户基础信息
    /// </summary>
    public class UserInfo : BaseEntity
    {
        public UserInfo() { }

        /// <summary>
        /// 用户业务编号
        /// </summary>
        public string UserId { get; set; }

        #region 基本属性
        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        /// 证件类型
        /// </summary>
        public CertificateType CertificateType { get; set; }

        /// <summary>
        /// 证件号码
        /// </summary>
        public string IdentityNo { get; set; }

        #endregion

        /// <summary>
        /// 指纹数量
        /// </summary>
        public int FingerprintCount { get; set; }

        /// <summary>
        /// 移动电话
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// 固定电话
        /// </summary>
        public string TelPhone { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }
    }

    public class UserAccountInfo : BaseEntity 
    {
        /// <summary>
        /// 用户类型
        /// </summary>
        public UserType UserType { get; set; }

        #region 登陆相关属性
        /// <summary>
        /// 登录名
        /// </summary>
        public string LoginId { get; set; }

        ///// <summary>
        ///// 登陆方式
        ///// </summary>
        //public LogOnType LogOnType { get; set; }

        /// <summary>
        /// 登陆密码(MD5)
        /// </summary>
        public string PassWord { get; set; }

        /// <summary>
        /// 是否已锁定
        /// </summary>
        public bool IsLocked { get; set; }

        /// <summary>
        /// 是否可以登陆(部分类型的用户不能登陆)
        /// </summary>
        public bool AuthDisabled { get; set; }

        /// <summary>
        /// 最后登陆时间
        /// </summary>
        public DateTime? LastLoginDate { get; set; }
        #endregion
    }

    /// <summary>
    /// 客户（上下家）信息
    /// </summary>
    public class CustomerInfo : UserInfo 
    {
        /// <summary>
        /// 用户系统编号
        /// </summary>
        public long UserSysNo { get; set; }

        /// <summary>
        /// 客户类型
        /// </summary>
        public CustomerType CustomerType { get; set; }

        #region 冗余查询字段
        /// <summary>
        /// 国籍
        /// </summary>
        public string Nationality { get; set; }
        #endregion
    }

    /// <summary>
    /// 平安员工信息
    /// </summary>
    public class PinganStaffInfo : UserInfo 
    {
        /// <summary>
        /// 用户系统编号
        /// </summary>
        public long UserSysNo { get; set; }

        /// <summary>
        /// UM帐号
        /// </summary>
        public string UMCode { get; set; }

        /// <summary>
        /// 平安工号
        /// </summary>
        public string StaffNo { get; set; }

        /// <summary>
        /// 所属受理中心
        /// </summary>
        public ReceptionCenter ReceptionCenter { get; set; }

        /// <summary>
        /// 岗位
        /// </summary>
        public Duty Duty { get; set; }

        /// <summary>
        /// 平安工卡编号
        /// </summary>
        public string StaffCardNo { get; set; }
    }

    /// <summary>
    /// 第三方公司员工信息
    /// </summary>
    public class ThirdpartyUserInfo : UserInfo 
    {
        /// <summary>
        /// 用户系统编号
        /// </summary>
        public long UserSysNo { get; set; }

        /// <summary>
        /// 所属第三方机构名称
        /// </summary>
        public string TpName { get; set; }
    }

    /// <summary>
    /// 中介员工信息
    /// </summary>
    public class AgentStaffInfo : UserInfo 
    {
        /// <summary>
        /// 用户系统编号
        /// </summary>
        public long UserSysNo { get; set; }

        /// <summary>
        /// 所属中介公司系统编号
        /// </summary>
        public long AgentCompanySysNo { get; set; }

        /// <summary>
        /// 是否是管理员
        /// </summary>
        public bool IsManager { get; set; }

        /// <summary>
        /// 手机
        /// </summary>
        public string Mobile1 { get; set; }
    }

    public class AgentStaffPaginatedInfo : AgentStaffInfo 
    {
        public string CompanyName { get; set; }

        public string CompanyAddress { get; set; }
    }
}
