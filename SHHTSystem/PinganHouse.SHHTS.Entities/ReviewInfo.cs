﻿using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Entities
{
    /// <summary>
    /// 主复核信息
    /// </summary>
    public class ReviewInfo:BaseEntity
    {
        /// <summary>
        /// 复核类别
        /// </summary>
        public ReviewCategory ReviewCategory { get; set; }

        /// <summary>
        /// 复核行为对象
        /// </summary>
        public string AffiliationNo { get; set; }

        private string reviewMode;
        /// <summary>
        /// 复核方式
        /// 多种复核方式按顺序以,逗号隔开
        /// </summary>
        public string ReviewMode 
        {
            get { return this.reviewMode; }
            set { this.reviewMode = value; }
        }

        /// <summary>
        /// 复核者
        /// 多个复核人编号，以,逗号隔开
        /// </summary>
        public string Reviewer { get; set; }

        /// <summary>
        /// 是否通过
        /// </summary>
        public bool IsPass { get; set; }

        List<ReviewMode> reviewModes = new List<ReviewMode>();
        public List<ReviewMode> ReviewModes 
        { 
            get 
            {
                if (!string.IsNullOrWhiteSpace(reviewMode) && reviewModes.Count ==0)
                {
                    foreach (var mode in reviewMode.Split(','))
                    {
                        reviewModes.Add((ReviewMode)Enum.Parse(typeof(ReviewMode), mode));
                    }
                }
                return this.reviewModes;
            } 
        }

        /// <summary>
        /// 是否启用
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// 事件描述
        /// </summary>
        public string Description { get; set; }
    }
}
