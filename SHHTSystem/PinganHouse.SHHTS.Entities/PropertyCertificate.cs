﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinganHouse.SHHTS.Entities
{
    /// <summary>
    /// 产证信息
    /// 后期将case中的关于产证信息原子抽离
    /// 目前如已绑定在case中，人已案件为主
    /// </summary>
    public class PropertyCertificate:BaseEntity
    {
        /// <summary>
        /// 产证编号
        /// </summary>
        public string TenementContract { get; set; }

        /// <summary>
        /// 物业地址
        /// </summary>
        public string TenementAddress { get; set; }

        public string Proprietor
        {
            get
            {
                return string.Join(",", proprietors);
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    foreach (var p in value.Split(','))
                    {
                        long sysno;
                        if (long.TryParse(p, out sysno))
                        {
                            if (proprietors.Contains(sysno))
                                proprietors.Add(sysno);                            
                        }                           
                    }
                }
            }
        }

        private List<long> proprietors = new List<long>();
        /// <summary>
        /// 业主
        /// </summary>
        public List<long> Proprietors
        {
            get { return proprietors; }
            set
            {
                if (value == null)
                    value = new List<long>();
                proprietors = value;
            }
        }
    }
}
