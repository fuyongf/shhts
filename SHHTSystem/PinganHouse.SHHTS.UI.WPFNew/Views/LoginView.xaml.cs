﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using System.Threading.Tasks;

using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.DataTransferObjects;

using PinganHouse.SHHTS.UI.WPFNew.ViewModels;

namespace PinganHouse.SHHTS.UI.WPFNew.Views
{
    /// <summary>
    /// Login.xaml 的交互逻辑
    /// </summary>
    public partial class LoginView : UserControl
    {
        public LoginView(LoginViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;

            //NetDetection();
        }
        public LoginViewModel ViewModel
        {
            get
            {
                return (LoginViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
        private void LoginView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (string.IsNullOrEmpty(this.m_UserName.Text.Trim()))
                {
                    this.m_UserName.Focus();
                    return;
                }

                if (string.IsNullOrEmpty(this.m_PassWord.Password.Trim()))
                {
                    this.m_PassWord.Focus();
                    return;
                }
                if (ViewModel.CurrentIndex == -1)
                {
                    return;
                }
                ViewModel.IsOk = true;
            }
        }

        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var image = (Image)sender;
            if ("0".Equals(image.Tag))
            {
                bdContianer.Height = 390;
                image.Tag = "1";
            }
            else
            {
                bdContianer.Height = 328;
                image.Tag = "0";
            }
        }

        private void CbNetwork_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ViewModel.CurrentNetwork = Convert.ToInt32(CbNetwork.SelectedValue);
            //var item = ((ComboBox)e.Source).SelectedItem as ComboBoxItem;
            //if (item != null)
            //{
            //    AppDomain.CurrentDomain.SetData("ServiceSuffix", item.Tag as string);
            //}
        }
    }
}
