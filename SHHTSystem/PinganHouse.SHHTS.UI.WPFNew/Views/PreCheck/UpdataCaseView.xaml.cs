﻿ using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Practices.Prism.PubSubEvents;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using PinganHouse.SHHTS.UI.WPFNew.Common.Dialog;
using PinganHouse.SHHTS.UI.WPFNew.Controls;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;

namespace PinganHouse.SHHTS.UI.WPFNew.Views
{
    /// <summary>
    /// UpdataCaseView.xaml 的交互逻辑
    /// </summary>
    public partial class UpdataCaseView : UserControl
    {
        private AgentStaff agentStaff;

        public UpdataCaseView()
        {
            InitializeComponent();
            agentStaff = new AgentStaff();
        }

        public ShowCaseModel ShowCase
        {
            get { return (ShowCaseModel)GetValue(ShowCaseProperty); }
            set { SetValue(ShowCaseProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowCase.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowCaseProperty =
            DependencyProperty.Register("ShowCase", typeof(ShowCaseModel), typeof(UpdataCaseView),
                new PropertyMetadata(null));

        public static readonly RoutedEvent CancelButtonClickEvent = EventManager.RegisterRoutedEvent(
           "CancelButtonClick", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(UpdataCaseView));



        public PreCheckAddCaseViewModel CaseDetailInfo
        {
            get { return (PreCheckAddCaseViewModel)GetValue(CaseDetailInfoProperty); }
            set { SetValue(CaseDetailInfoProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CaseDetailInfo.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CaseDetailInfoProperty =
            DependencyProperty.Register("CaseDetailInfo", typeof(PreCheckAddCaseViewModel), typeof(UpdataCaseView));



        // Expose this event for this control's container
        public event RoutedEventHandler CancelButtonClick
        {
            add { AddHandler(CancelButtonClickEvent, value); }
            remove { RemoveHandler(CancelButtonClickEvent, value); }
        }

        public static readonly RoutedEvent FileButtonClickEvent = EventManager.RegisterRoutedEvent(
          "FileButtonClick", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(UpdataCaseView));


        // Expose this event for this control's container
        public event RoutedEventHandler FileButtonClick
        {
            add { AddHandler(FileButtonClickEvent, value); }
            remove { RemoveHandler(FileButtonClickEvent, value); }
        }

        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string typeCustomer = string.Empty;
                var button = sender as Button;
                if (button != null)
                {
                    typeCustomer = button.Tag.ToString();
                }
                if (typeCustomer == string.Empty) return;
                CustomerType customerType = (CustomerType)Enum.Parse(typeof(CustomerType), typeCustomer);
                switch (customerType)
                {
                    case CustomerType.Buyer:
                        var modityBuyerResult = ModifyCustomer(ShowCase.IdCardBuyerModel, ShowCase.ListBuyerModel, ShowCase.SelectBuyerUser, ShowCase.IdentityBuyerSelected, CustomerType.Buyer, ShowCase.BuyerNameDic);
                        var tempBuyerList = new ObservableCollection<UserIdentityModel>();
                        foreach (var item in ShowCase.ListBuyerModel)
                        {
                            tempBuyerList.Add(item);
                        }
                        ShowCase.ListBuyerModel = tempBuyerList;
                        //if (!modityBuyerResult)
                        //    return;
                        ShowCase.BuyerTotal = ShowCase.ListBuyerModel.Count;
                        ShowCase.IdCardBuyerModel = new IdInfoModel();
                        break;
                    case CustomerType.Seller:
                        var moditySellerResult = ModifyCustomer(ShowCase.IdCardSellerModel, ShowCase.ListSellerModel, ShowCase.SelectSellerUser, ShowCase.IdentitySellerSelected, CustomerType.Buyer, ShowCase.SellerNameDic);
                        var tempSellerList = new ObservableCollection<UserIdentityModel>();
                        foreach (var item in ShowCase.ListSellerModel)
                        {
                            tempSellerList.Add(item);
                        }
                        ShowCase.ListSellerModel = tempSellerList;
                        //if (!moditySellerResult)
                        //    return;
                        ShowCase.SellerTotal = ShowCase.ListSellerModel.Count;
                        ShowCase.IdCardSellerModel = new IdInfoModel();
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool ModifyCustomer(IdInfoModel idInfos, ObservableCollection<UserIdentityModel> userIdentityModel, UserIdentityModel SelectUser, EnumHelper IdentitySelected, CustomerType customerType, Dictionary<long, string> NameDic)
        {
            try
            {
                //000 获取证件类型
                var certificateType = (CertificateType)Enum.Parse(typeof(CertificateType), Convert.ToString(IdentitySelected.ValueMember));
                var tag = idInfos.Tag as Dictionary<long, string>;
                //if (tag == null||!tag.ContainsKey(0))
                //    return false;
                var user = new UserIdentityModel();
                IdInfosToSelectUser(certificateType, idInfos, user);
                //添加
                //if (tag[0]!="true")

                if (tag == null)
                {
                    OperationResult result = AddUserIdentity(user, customerType);
                    if (result.OtherData.Count == 0)
                    {
                        MessageBox.Show("用户保存失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                        return false;
                    }
                    var userSysNo = result.OtherData["UserSysNo"] == null
                             ? -1
                             : Convert.ToInt64(result.OtherData["UserSysNo"]);

                    if (userSysNo <= 0)
                    {
                        MessageBox.Show("用户保存失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                        return false;
                    }
                    tag = new Dictionary<long, string> { { userSysNo, user.IdentityNo } };
                    idInfos.Tag = tag;
                    userIdentityModel.Add(user);
                    NameDic.Add(userSysNo, user.Name);
                    return true;
                }
                //修改
                else
                {
                    if (tag[0] == "true")
                    {
                       long UserSysNo = 0;
                        foreach (long key in tag.Keys)
                        {
                            if (tag[key].Equals(SelectUser.Name))
                                UserSysNo = key;
                        }
                        IdInfosToSelectUser(certificateType, idInfos, SelectUser);
                        OperationResult result = UserServiceProxy.BindUserIdentity(UserSysNo, UserType.Customer,
                            SelectUser);
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
        }

        /// <summary>
        /// 保存买卖家信息
        /// </summary>     
        /// <param name="customerType">类型</param>        
        private OperationResult AddUserIdentity(UserIdentityModel userIdentity, CustomerType customerType)
        {
            OperationResult result = null;
            try
            {
                switch (customerType)
                {
                    case CustomerType.Buyer:
                        result = CustomerServiceProxy.CreateBuyer(userIdentity, LoginHelper.CurrentUser.SysNo);
                        break;
                    case CustomerType.Seller:
                        result = CustomerServiceProxy.CreateSeller(userIdentity, LoginHelper.CurrentUser.SysNo);
                        break;
                }
                return result;
            }
            catch (Exception ex)
            {
                result.ResultMessage = ex.Message;
                return result;
            }
        }
        #region 保存暂时折叠

        /// <summary>
        /// 保存卖家或买家信息
        /// </summary>
        /// <param name="idIfos">卖家或买家信息详情</param>
        private bool SaveCustomer(IdInfoModel idInfos, ObservableCollection<UserIdentityModel> userIdentityModel,
            UserIdentityModel SelectUser, EnumHelper IdentitySelected, CustomerType customerType)
        {
            try
            {
                long userSysNo;
                //000 获取证件类型
                var certificateType =
                    (CertificateType)
                        Enum.Parse(typeof(CertificateType), Convert.ToString(IdentitySelected.ValueMember));
                if (idInfos.Tag != null)
                {
                    long.TryParse(Convert.ToString(idInfos.Tag), out userSysNo);
                    //修改数据
                    if (userSysNo > 0)
                    {
                        var modifyResult = ModifyUserIdentity(certificateType, userSysNo, idInfos, SelectUser);
                        return modifyResult.Success;
                    }
                }
                //001检查数据合法性
                //checkDataResult = CheckBuyerData(certificateType);
                //checkDataResult = CheckBuyerDataNew(certificateType);
                //if (!checkDataResult) { return; }
                //002检查证件是否已经存在              
                var checkResult = CheckIdCardNoInItemSource(userIdentityModel, idInfos.IdNum);
                if (!checkResult)
                    return false;
                //003 保存数据
                var saveResult = AddUser(certificateType, idInfos, userIdentityModel, customerType);
                if (saveResult.Success && certificateType.Equals(CertificateType.IdCard))
                {

                    string idSym = saveResult.OtherData["UserSysNo"].ToString();
                    MessageBoxResult result = MessageBox.Show("是否上传身份证页面", "系统提示", MessageBoxButton.OKCancel,
                        MessageBoxImage.Question);
                    if (result == MessageBoxResult.OK)
                    {
                        //TODO:弹出高拍仪页面
                        //new CaptureIdCard(idSym);
                    }
                    return true;
                }
                return saveResult.Success;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
        }

        /// <summary>
        /// 修改买方或者卖方数据
        /// </summary>
        /// <param name="certificateType"></param>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        private OperationResult ModifyUserIdentity(CertificateType certificateType, long userSysNo, IdInfoModel idInfos,
            UserIdentityModel SelectUser)
        {
            IdInfosToSelectUser(certificateType, idInfos, SelectUser);
            var result = UserServiceProxy.BindUserIdentity(userSysNo, UserType.Customer, SelectUser);
            if (!result.Success)
            {
                MessageBox.Show(result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                ////清空页面信息
                //idInfos = null;
            }
            return result;
        }

        /// <summary>
        /// 检查证据信息是否存在右边列表
        /// </summary>
        /// <param name="enumerable">数据源对象</param>
        /// <param name="newCardNo">新的证件号码</param>
        /// <returns></returns>
        private bool CheckIdCardNoInItemSource(IEnumerable enumerable, string newCardNo)
        {
            try
            {
                //无对象说明没有任何值
                if (enumerable == null) return true;
                if (enumerable.OfType<UserIdentityModel>().Any(user => user.IdentityNo.Equals(newCardNo)))
                {
                    MessageBox.Show("当前证件信息已经存在.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show("检查证件号是否存在发生异常：" + e.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return false;
        }

        /// <summary>
        ///买方或卖方信息添加
        /// </summary>
        /// <param name="certificateType"></param>
        /// <param name="userSysNo"></param>
        /// <param name="idInfos"></param>
        /// <param name="SelectUser"></param>
        /// <returns></returns>
        private void IdInfosToSelectUser(CertificateType certificateType, IdInfoModel idInfos,
            UserIdentityModel SelectUser)
        {
            SelectUser.CertificateType = certificateType;
            SelectUser.Name = idInfos.IdName;
            if (idInfos.IdSex == null)
            {
                if (idInfos.Male)
                {
                    SelectUser.Gender = Gender.Male;
                }
                else if (idInfos.Female)
                {
                    SelectUser.Gender = Gender.Female;
                }
                else
                {
                    SelectUser.Gender = Gender.Unknown;
                }
            }
            else
            {
                switch (idInfos.IdSex)
                {
                    case "男":
                        SelectUser.Gender = Gender.Male;
                        break;
                    case "女":
                        SelectUser.Gender = Gender.Female;
                        break;
                    default:
                        SelectUser.Gender = Gender.Unknown;
                        break;
                }
            }
            if (idInfos.IdNation != null)
            {
                SelectUser.Nation = (Nation)Nation.Parse(typeof(Nation), idInfos.IdNation);
            }
            if (idInfos.IdBirth == null)
                SelectUser.Birthday = null;
            else
                SelectUser.Birthday = Convert.ToDateTime(idInfos.IdBirth);
            SelectUser.Address = idInfos.IdAddress;
            SelectUser.IdentityNo = idInfos.IdNum;
            SelectUser.VisaAgency = idInfos.IdAgent;
            if (idInfos.EffectiveDate != null)
                SelectUser.EffectiveDate = Convert.ToDateTime(idInfos.EffectiveDate);
            else
                SelectUser.EffectiveDate = null;
            if (idInfos.ExpiryDate != null && idInfos.ExpiryDate != "长期")
            {
                SelectUser.ExpiryDate = Convert.ToDateTime(idInfos.ExpiryDate);
            }
            else
            {
                SelectUser.ExpiryDate = null;
            }
            SelectUser.Nationality = idInfos.Nationality;
            SelectUser.Photo = idInfos.Photo;
            SelectUser.IsTrusted = idInfos.IsTrusted;
        }

        /// <summary>
        /// 添加买方或卖方数据
        /// </summary>
        /// <returns></returns>
        private OperationResult AddUser(CertificateType certificateType, IdInfoModel idInfos,
            ObservableCollection<UserIdentityModel> userIdentityModel, CustomerType customerType)
        {
            var user = new UserIdentityModel();
            IdInfosToSelectUser(certificateType, idInfos, user);
            var result = SaveUserIdentity(user, customerType);
            if (result != null && result.Success)
            {
                user.Tag = result.OtherData["UserSysNo"];
                userIdentityModel.Add(user);
                return result;
            }
            else
            {
                if (result != null)
                    MessageBox.Show(result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }

        /// <summary>
        /// 保存买卖家信息
        /// </summary>     
        /// <param name="customerType">类型</param>        
        private OperationResult SaveUserIdentity(UserIdentityModel userIdentity, CustomerType customerType)
        {
            OperationResult result = null;
            try
            {
                switch (customerType)
                {
                    case CustomerType.Buyer:
                        result = CustomerServiceProxy.CreateBuyer(userIdentity, LoginHelper.CurrentUser.SysNo);
                        break;
                    case CustomerType.Seller:
                        result = CustomerServiceProxy.CreateSeller(userIdentity, LoginHelper.CurrentUser.SysNo);
                        break;
                }
                return result;

            }
            catch (Exception ex)
            {
                result.ResultMessage = ex.Message;
                return result;
            }
        }


        #endregion

        private void ReadCustomerClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var idTypeControl = sender as IdCardTypeControl;
                if (idTypeControl != null)
                {
                    bool flag;
                    var idType = idTypeControl.ReadType;
                    var idCard = new IdCard(Convert.ToInt32(idType), out flag);
                    idCard.ShowDialog();
                    if (idCard.IdentityInfo != null)
                    {
                        switch (idType)
                        {
                            case 1:
                                ShowCase.IdCardBuyerModel = new IdInfoModel();
                                IdCardDisplay(CaseDetailInfo.IdCardBuyerModel, idCard.IdentityInfo);
                                break;
                            case 2:
                                ShowCase.IdCardSellerModel = new IdInfoModel();
                                IdCardDisplay(CaseDetailInfo.IdCardSellerModel, idCard.IdentityInfo);
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ReadAgentClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var idTypeControl = sender as Button;
                if (idTypeControl != null)
                {
                    bool flag;
                    var idType = (string)idTypeControl.CommandParameter;
                    var idTypeInt = Convert.ToInt32(idType);
                    var idCard = new IdCard(idTypeInt, out flag);
                    if (!flag)
                        return;
                    idCard.ShowDialog();
                    if (idCard.IdentityInfo == null) return;
                    GetAgentValue(idCard.IdentityInfo);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void GetAgentValue(UserIdentityModel IdentityInfo)
        {
            int totalCount = 0;
            var idNo = IdentityInfo.IdentityNo;

            IList<AgentStaff> agentStaffList = new List<AgentStaff>();
            if (idNo == null)
            {
                return;
            }
            agentStaffList = AgentServiceProxy.GetAgentStaffs(1, 10000, out totalCount, null, null, null, idNo);
            if (agentStaffList.Count > 0)
            {
                for (int i = 0; i < agentStaffList.Count; i++)
                {
                    AgentCompany agentCompany = AgentServiceProxy.GetAgentCompany(agentStaffList[i].AgentCompanySysNo);
                    if (agentCompany != null)
                    {
                        ShowCase.TblShowInfo = "姓名：" + agentStaffList[i].RealName + "                 所属门店：" +
                                                agentCompany.CompanyName;
                    }
                }
            }
            else
            {
                MessageBox.Show("没有符合该身份证号的经纪人！");
                return;
            }

        }

        private void IdCardDisplay(IdInfoModel idInfo, UserIdentityModel identity)
        {
            idInfo.IdAddress = identity.Address;
            if (identity.Birthday != null)
            {
                var effectTime = (DateTime)identity.Birthday;
                idInfo.IdBirth = effectTime.ToShortDateString();
            }
            if (identity.EffectiveDate != null)
            {
                var effectTime = (DateTime)identity.EffectiveDate;
                idInfo.EffectiveDate = effectTime.ToShortDateString();
            }
            if (identity.ExpiryDate != null)
            {
                var effectTime = (DateTime)identity.ExpiryDate;
                idInfo.ExpiryDate = effectTime.ToShortDateString();
            }

            idInfo.IdSex = EnumHelper.GetEnumDesc(identity.Gender);
            idInfo.IdNum = identity.IdentityNo;
            idInfo.IdName = identity.Name;
            idInfo.IdNation = identity.Nation.ToString();
            idInfo.IdAgent = identity.VisaAgency;
            idInfo.Photo = identity.Photo;
            if (identity.Photo != null)
            {
                BitmapImage bitmap = new BitmapImage();
                MemoryStream ms1 = new MemoryStream(identity.Photo);
                bitmap.BeginInit();
                bitmap.StreamSource = ms1;
                bitmap.EndInit();
                idInfo.Bitmap = bitmap; 
            }           
        }

        private void QueryBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var agentCheck = new AgentQueryDialog();
                agentCheck.ShowDialog();
                agentStaff = agentCheck.AgentStaff;
                var agentCompany = AgentServiceProxy.GetAgentCompany(agentStaff.AgentCompanySysNo);
                if (agentCompany == null)
                    return;
                ShowCase.TblShowInfo = string.Empty;
                ShowCase.TblShowInfo = string.Format("姓名   {0}      所属门店   {1}", agentStaff.RealName, agentCompany.CompanyName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(CancelButtonClickEvent));
        }

        private void SaveModifyBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var result = SaveUpdateCase();
                if (result.Success)
                {
                    ShowCase = new ShowCaseModel();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("系统发生异常：" + ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 保存案件
        /// </summary>
        /// <returns></returns>
        private OperationResult SaveUpdateCase()
        {
            if (string.IsNullOrEmpty(ShowCase.TbTenementContract))
            {
                MessageBox.Show("请填写产权证号", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return null;
            }

            if (string.IsNullOrEmpty(ShowCase.TbTenementAddress))
            {
                MessageBox.Show("请填写房屋坐落", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return null;
            }

            OperationResult result = CaseProxyService.GetInstanse().Update(
                ShowCase.CaseId, ShowCase.BuyerNameDic.Keys.ToList(), ShowCase.SellerNameDic.Keys.ToList(), agentStaff.SysNo,
                LoginHelper.CurrentUser.SysNo, ShowCase.TbTenementAddress,
                ShowCase.TbTenementContractWord + "-" + ShowCase.TbTenementContract);
            if (result.Success)
            {
                MessageBox.Show("保存成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("保存失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            return result;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(FileButtonClickEvent));
        }

        public void DeleteClick(object sender, RoutedEventArgs e)
        {
            CaseDetailInfo.BuyerInfoList.Remove(CaseDetailInfo.BuySelectCurrentItem);
            CaseDetailInfo.IdCardBuyerModel = new IdInfoModel();
        }
        public void DeleteClick1(object sender, RoutedEventArgs e)
        {
            CaseDetailInfo.SellerInfoList.Remove(CaseDetailInfo.SellSelectCurrentItem);
            CaseDetailInfo.IdCardSellerModel = new IdInfoModel();
        }
    }
}
