﻿using PinganHouse.SHHTS.UI.WPFNew.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using PinganHouse.SHHTS.UI.WPFNew.Controls;
using Microsoft.Practices.ServiceLocation;

namespace PinganHouse.SHHTS.UI.WPFNew.Views
{
    /// <summary>
    /// AddCaseView.xaml 的交互逻辑
    /// </summary>
    public partial class AddCaseView : UserControl
    {
        public AddCaseView(PreCheckAddCaseViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
        }

        public PreCheckAddCaseViewModel ViewModel
        {
            get { return (PreCheckAddCaseViewModel)this.DataContext; }
            set { this.DataContext = value; }
        }

        private void ReadCustomerClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var idTypeControl = sender as IdCardTypeControl;
                if (idTypeControl != null)
                {
                    bool flag;
                    var idType = idTypeControl.ReadType;
                    var idCard = new IdCard(Convert.ToInt32(idType), out flag);
                    idCard.ShowDialog();
                    if (idCard.IdentityInfo != null)
                    {
                        switch (idType)
                        {
                            case 1:
                                IdCardDisplay(ViewModel.IdCardBuyerModel, idCard.IdentityInfo);
                                break;
                            case 2:
                                IdCardDisplay(ViewModel.IdCardSellerModel, idCard.IdentityInfo);
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void IdCardDisplay(IdInfoModel idInfo, UserIdentityModel identity)
        {
            idInfo.IdAddress = identity.Address;
            if (identity.Birthday != null)
            {
                var effectTime = (DateTime)identity.Birthday;
                idInfo.IdBirth = effectTime.ToShortDateString();
            }
            if (identity.EffectiveDate != null)
            {
                var effectTime = (DateTime)identity.EffectiveDate;
                idInfo.EffectiveDate = effectTime.ToShortDateString();
            }
            if (identity.ExpiryDate != null)
            {
                var effectTime = (DateTime)identity.ExpiryDate;
                idInfo.ExpiryDate = effectTime.ToShortDateString();
            }

            idInfo.IdSex = EnumHelper.GetEnumDesc(identity.Gender);
            idInfo.IdNum = identity.IdentityNo;
            idInfo.IdName = identity.Name;
            idInfo.IdNation = identity.Nation.ToString();
            idInfo.IdAgent = identity.VisaAgency;
            if (identity.Photo != null)
            {
                BitmapImage bitmap = new BitmapImage();
                MemoryStream ms1 = new MemoryStream(identity.Photo);
                bitmap.BeginInit();
                bitmap.StreamSource = ms1;
                bitmap.EndInit();
                idInfo.Bitmap = bitmap;
            }           
        }

        public void DeleteClick(object sender, RoutedEventArgs e)
        {
            ViewModel.BuyerInfoList.Remove(ViewModel.BuySelectCurrentItem);
            ViewModel.IdCardBuyerModel = new IdInfoModel();
        }
        public void DeleteClick1(object sender, RoutedEventArgs e)
        {
            ViewModel.SellerInfoList.Remove(ViewModel.SellSelectCurrentItem);
            ViewModel.IdCardSellerModel = new IdInfoModel();
        }
    }
}
