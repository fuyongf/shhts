﻿using PinganHouse.SHHTS.UI.WPFNew.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPFNew.Views
{
    /// <summary>
    /// UploadAttachment.xaml 的交互逻辑
    /// </summary>
    public partial class UploadAttachmentView : UserControl
    {
        public UploadAttachmentView(UploadAttachmentsViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
        }

        public UploadAttachmentsViewModel ViewModel
        {
            get
            {
                return (UploadAttachmentsViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
        private void CancelClick(object sender, RoutedEventArgs e)
        {
            ViewModel.IsNavList = true;
        }
        private void FileClick(object sender, RoutedEventArgs e)
        {
            ViewModel.IsNavFile = true;
        }
    }
}
