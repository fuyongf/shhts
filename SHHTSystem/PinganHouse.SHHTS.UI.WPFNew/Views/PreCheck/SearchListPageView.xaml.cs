﻿using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPFNew.Views
{
    /// <summary>
    /// SearchListPageView.xaml 的交互逻辑
    /// </summary>
    public partial class SearchListPageView : UserControl
    {
        public SearchListPageView(SearchListPageViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
        }


        public SearchListPageViewModel ViewModel
        {
            get
            {
                return (SearchListPageViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
                this.GridPager.PageCount = ViewModel.TotalPageCount;
            }
        }
        private void GridPager_PagerIndexChanged(int pageIndex)
        {
            ViewModel.IsPageChanged = true;
            ViewModel.CurrentPageIndex = this.GridPager.PageIndex;
        }

        private void m_CaseStatusList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = ((ComboBox)sender).SelectedItem as EnumHelper;
            if (item == null)
            {
                return;
            }
            if(ViewModel.SearchCaseCondition !=null)
            {
                switch(item.DisplayMember)
                {
                    case "等待签约":
                        ViewModel.SearchCaseCondition.Status = CaseStatus.YJ1;
                        break;
                    case "签约中":
                        ViewModel.SearchCaseCondition.Status = CaseStatus.HA1;
                        break;
                    case "已签约":
                        ViewModel.SearchCaseCondition.Status = CaseStatus.HA2;
                        break;
                    case "签约已取消":
                        ViewModel.SearchCaseCondition.Status = CaseStatus.ZB2;
                        break;
                    case "签约取消":
                        ViewModel.SearchCaseCondition.Status = CaseStatus.ZB5;
                        break;
                    case "全部案件":
                        ViewModel.SearchCaseCondition.Status = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.ZB2 | CaseStatus.ZB5;
                        break;
                    default:
                        break;
                }
                
            }
        }
    }
}
