﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;

namespace PinganHouse.SHHTS.UI.WPFNew.Views
{
    /// <summary>
    /// CheckCaseSearchPageView.xaml 的交互逻辑
    /// </summary>
    public partial class CheckCaseSearchPageView : UserControl
    {
        public CheckCaseSearchPageView(CheckCaseSearchPageViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
        }
        public CheckCaseSearchPageViewModel ViewModel
        {
            get
            {
                return (CheckCaseSearchPageViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
                this.GridPager.PageCount = ViewModel.TotalPageCount;
            }
        }

        private void GridPager_PagerIndexChanged(int pageIndex)
        {
            ViewModel.CurrentPageIndex = this.GridPager.PageIndex;
        }

        private void m_CaseStatusList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = ((ComboBox)sender).SelectedItem as EnumHelper;
            if (item == null)
            {
                return;
            }
            if (ViewModel.SearchCaseCondition != null)
            {
                switch (item.DisplayMember)
                {
                    case "等待签约":
                        ViewModel.SearchCaseCondition.Status = CaseStatus.YJ1;
                        break;
                    case "签约中":
                        ViewModel.SearchCaseCondition.Status = CaseStatus.HA1;
                        break;
                    case "已签约":
                        ViewModel.SearchCaseCondition.Status = CaseStatus.HA2;
                        break;
                    case "立案报告填写中":
                        ViewModel.SearchCaseCondition.Status = CaseStatus.HA3;
                        break;
                    case "立案报告已提交":
                        ViewModel.SearchCaseCondition.Status = CaseStatus.HA4;
                        break;
                    case "立案结束":
                        ViewModel.SearchCaseCondition.Status = CaseStatus.HA5;
                        break;
                    case "立案报告审批不通过":
                        ViewModel.SearchCaseCondition.Status = CaseStatus.HA6;
                        break;
                    case "签约已取消":
                        ViewModel.SearchCaseCondition.Status = CaseStatus.ZB2;
                        break;
                    case "签约取消":
                        ViewModel.SearchCaseCondition.Status = CaseStatus.ZB5;
                        break;
                    case "案件中止":
                        ViewModel.SearchCaseCondition.Status = CaseStatus.ZB6;
                        break;
                    case "案件已中止":
                        ViewModel.SearchCaseCondition.Status = CaseStatus.ZB7;
                        break;
                    case "结案":
                        ViewModel.SearchCaseCondition.Status = CaseStatus.ZB13;
                        break;
                    case "全部案件":
                        ViewModel.SearchCaseCondition.Status = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.HA3 | CaseStatus.HA4 | 
                                                                CaseStatus.HA5 | CaseStatus.HA6 | CaseStatus.ZB2 | CaseStatus.ZB5 | CaseStatus.ZB6 | 
                                                                CaseStatus.ZB7 | CaseStatus.ZB13;

                        break;
                    default:
                        break;
                }

            }
        }
    }
}
