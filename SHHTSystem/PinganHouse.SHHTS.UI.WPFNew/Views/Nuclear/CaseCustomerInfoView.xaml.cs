﻿using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using PinganHouse.SHHTS.UI.WPFNew.Controls.Nuclear;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels.NuclearModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPFNew.Views.Nuclear
{
    /// <summary>
    /// CaseCustomerInfoView.xaml 的交互逻辑
    /// </summary>
    public partial class CaseCustomerInfoView : UserControl
    {
        public CaseCustomerInfoView(CaseCustomerInfoViewModel viewModel)
        {
            InitializeComponent();
            InitComboboxData();
            this.DataContext = viewModel;
        
        }
        public CaseCustomerInfoViewModel ViewModel
        {
            get { return (CaseCustomerInfoViewModel)this.DataContext; }
            set { this.DataContext = value; }
        }

        public void InitComboboxData()
        {
            try
            {
                var provinceList = ConfigServiceProxy.GetRegionInfo(null);
               
                    ProvinceBuy.ItemsSource = provinceList;
                    ProvinceBuy.DisplayMemberPath = null;
                    ProvinceBuy.SelectedValuePath = null;
                    ProvinceBuy.Text = "上海市";
            
                    ProvinceSell.ItemsSource = provinceList;
                    ProvinceSell.DisplayMemberPath = null;
                    ProvinceSell.SelectedValuePath = null;
                    ProvinceSell.Text = "上海市";
                
            }
            catch (Exception e)
            {
                MessageBox.Show("初始化页面基础数据错误：" + e.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ProvinceBuy_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            var province = ProvinceBuy.SelectedValue;
            var cityList = ConfigServiceProxy.GetRegionInfo("/" + province);

            ComCityBuy.ItemsSource = cityList;
            ComCityBuy.DisplayMemberPath = null;
            ComCityBuy.SelectedValuePath = null;
            ComCityBuy.SelectedIndex = 0;
        }

        private void ComCityBuy_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var province = ProvinceBuy.SelectedValue;
            var city = ComCityBuy.SelectedValue;
            var districtList = ConfigServiceProxy.GetRegionInfo("/" + province + "/" + city);

            ComPrefectureBuy.ItemsSource = districtList;
            ComPrefectureBuy.DisplayMemberPath = null;
            ComPrefectureBuy.SelectedValuePath = null;
            ComPrefectureBuy.SelectedIndex = 0;
        }

        private void ProvinceSell_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var province = ProvinceSell.SelectedValue;
            var cityList = ConfigServiceProxy.GetRegionInfo("/" + province);

            ComCitySell.ItemsSource = cityList;
            ComCitySell.DisplayMemberPath = null;
            ComCitySell.SelectedValuePath = null;
            ComCitySell.SelectedIndex = 0;

        }

        private void ComCitySell_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            var province = ProvinceSell.SelectedValue;
            var city = ComCitySell.SelectedValue;
            var districtList = ConfigServiceProxy.GetRegionInfo("/" + province + "/" + city);

            ComPrefectureSell.ItemsSource = districtList;
            ComPrefectureSell.DisplayMemberPath = null;
            ComPrefectureSell.SelectedValuePath = null;
            ComPrefectureSell.SelectedIndex = 0;
        }

        private void ButtonChiose_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var tag = sender as Button;
                var bankDialog = new BankDialog(Convert.ToString(tag));
                bankDialog.SelectBankEvent += bankDialog_SelectBankEvent;
                bankDialog.ClearBankEvent += bankDialog_ClearBankEvent;
                bankDialog.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("选择银行操作发生系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void bankDialog_SelectBankEvent(string bankCode, string bankName, string tag)
        {
            txt_bankName.Text = bankName;
            txt_bankName.Tag = bankCode;
        }
        private void bankDialog_ClearBankEvent(string tag)
        {
            txt_bankName.Text = string.Empty;
            txt_bankName.Tag = string.Empty;
        }

        private void AutoCompleteTextBox_OnSelectionChanged(object sender, RoutedEventArgs e)
        {

        }

    }
}
