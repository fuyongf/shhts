﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using PinganHouse.SHHTS.UI.WPFNew.Controls.Nuclear;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels.NuclearModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace PinganHouse.SHHTS.UI.WPFNew.Views.Nuclear
{
    /// <summary>
    /// BasicInfoView.xaml 的交互逻辑
    /// </summary>
    public partial class BasicInfoView : UserControl
    {
        /// <summary>
        /// 案件编号
        /// </summary>
        private string _mCaseId = string.Empty;

        public BasicInfoView(BaseInfoViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
        }

        public BaseInfoViewModel ViewModel
        {
            get
            {
                return (BaseInfoViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }

        #region 页面状态 -- 核案过来的状态
        CaseStatus m_Status = CaseStatus.HA2;

        #endregion

        #region 各种验证

        public void InitView(BaseInfoViewModel viewModel)
        {
            try
            {
                if (viewModel != null)
                {
                    if (string.IsNullOrEmpty(viewModel.TenementContracts2))
                    {
                        lab_CQZH.Width = 140;
                        lab_CQZZ.Width = 30;
                        txt_Run.Visibility = Visibility.Collapsed;
                    }
                }
                //环线位置
                var linkLocation = EnumHelper.InitLinkLocationToCombobox().OrderBy(item => item.ValueMember).ToList();
                cb_HXWZ.ItemsSource = linkLocation;
                cb_HXWZ.DisplayMemberPath = "DisplayMember";
                cb_HXWZ.SelectedValuePath = "ValueMember";
                cb_HXWZ.SelectedValue = -999;

                //居住类型
                var resideType = EnumHelper.InitResideTypeToCombobox().OrderBy(item => item.ValueMember).ToList();
                cb_FWLX.ItemsSource = resideType;
                cb_FWLX.DisplayMemberPath = "DisplayMember";
                cb_FWLX.SelectedValuePath = "ValueMember";
                cb_FWLX.SelectedValue = -999;

                //满n年
                var buyOverYears = EnumHelper.InitBuyOverYearsToCombobox().OrderBy(item => item.ValueMember).ToList();
                cb_yearChange.ItemsSource = buyOverYears;
                cb_yearChange.DisplayMemberPath = "DisplayMember";
                cb_yearChange.SelectedValuePath = "ValueMember";
                cb_yearChange.SelectedValue = -999;

                tb_XM.Text = "--";
                tb_SJ.Text = "--";
            }
            catch (Exception)
            {
                throw new Exception("初始化页面基础数据错误.");
            }
        }


        #region 银行相关
        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            var checkBox = sender as CheckBox;
            var checkStatus = false;
            if (checkBox.IsChecked != null)
            {
                checkStatus = (bool)checkBox.IsChecked;
            }
            if (checkBox == null) return;
            switch (checkBox.Name)
            {
                case "cb_TGZJ":
                    txt_TGZJZH.IsEnabled = checkStatus;
                    if (!checkStatus)
                        txt_TGZJZH.Text = string.Empty;
                    break;
                case "cb_YJTG":
                    txt_YJTGZH.IsEnabled = checkStatus;
                    if (!checkStatus)
                        txt_YJTGZH.Text = string.Empty;
                    break;
            }
        }
        #endregion

        #region 数字约束

        /// <summary>
        /// 验证输入字符
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;

            //屏蔽非法按键
            if ((e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) || e.Key == Key.Decimal)
            {
                if (txt.Text.Contains(".") && e.Key == Key.Decimal)
                {
                    e.Handled = true;
                    return;
                }
                e.Handled = false;
            }
            else if (((e.Key >= Key.D0 && e.Key <= Key.D9) || e.Key == Key.OemPeriod) && e.KeyboardDevice.Modifiers != ModifierKeys.Shift)
            {
                if (txt.Text.Contains(".") && e.Key == Key.OemPeriod)
                {
                    e.Handled = true;
                    return;
                }
                e.Handled = false;
            }
            else if (e.Key == Key.Tab)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// 数字键移除焦点格式化赋值
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NumText_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                var textBox = sender as TextBox;
                var txt = textBox.Text.Trim();
                decimal num = 0;
                if (decimal.TryParse(txt, out num))
                {
                    txt = string.Format("{0:N}", num);
                    textBox.Text = txt;
                }
            }
            catch (Exception) { }
        }

        /// <summary>
        /// 验证是否数字输入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text_TextChanged(object sender, TextChangedEventArgs e)
        {
            //屏蔽中文输入和非法字符粘贴输入
            TextBox textBox = sender as TextBox;
            TextChange[] change = new TextChange[e.Changes.Count];
            e.Changes.CopyTo(change, 0);

            int offset = change[0].Offset;
            if (change[0].AddedLength > 0)
            {
                double num = 0;
                if (!double.TryParse(textBox.Text, out num))
                {
                    ThreadPool.QueueUserWorkItem((obj) =>
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            textBox.Text = string.Empty;
                            textBox.Select(offset, 0);

                        });
                    });
                }
            }
        }

        #endregion

        private static int clickCount = 0;

        private void Add_ZITG_BUY_Click(object sender, RoutedEventArgs e)
        {
            clickCount++;
            var bankinfo = ViewModel.BuyerBankList;
            if (bankinfo == null)
            {
                ViewModel.BuyerBankList = new ObservableCollection<ViewModels.NuclearModel.Bank>(){(new ViewModels.NuclearModel.Bank()
                {
                    AccountName = "",
                    AccountNo = "",
                    BankName = "",
                    BankCode = "",
                    SubBankName = ""
                })};
            }
            else
            {
                bankinfo.Add(new ViewModels.NuclearModel.Bank()
                {
                    AccountName = "",
                    AccountNo = "",
                    BankName = "",
                    BankCode = "",
                    SubBankName = ""
                });
            }
        }

        #endregion

        private void Notary_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var button = sender as Button;
                var dialog = new NotaryShowDialog();
                long no;
                long.TryParse(notaryNo.Text, out no);
                //带回no。
                dialog.InitDate(no);
                dialog.SelectContentEvent += Notary_Select;
                dialog.ShowDialog();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void Notary_Select(long number, string name, string mobile)
        {
            notaryNo.Text = number.ToString();
            tb_XM.Text = name;
            tb_SJ.Text = mobile;
        }

        private void AddAccount_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var button = sender as Button;

                if (button == null) return;
                var tag = button.Tag.ToString();
                var title = string.Empty;
                var mainAccountExit = false;
                switch (tag)
                {
                    case "Buyer":
                        title = "买方用户添加";
                        if (addBuyer.Text == "true")
                            mainAccountExit = true;
                        break;
                    case "Seller":
                        title = "卖方用户添加";
                        if (addBuyer.Text == "true")
                            mainAccountExit = true;
                        break;
                }

                var dialog = new AccountAddDialog(title, mainAccountExit);
                dialog.ShowDialog();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
