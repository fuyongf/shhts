﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;

namespace PinganHouse.SHHTS.UI.WPFNew.Views
{
    /// <summary>
    /// ApprovalReportView.xaml 的交互逻辑
    /// </summary>
    public partial class ApprovalReportView : UserControl
    {
        public ApprovalReportView(ApprovalReportViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
        }

        public ApprovalReportViewModel ViewModel
        {
            get
            {
                return (ApprovalReportViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }

    
    }
}
