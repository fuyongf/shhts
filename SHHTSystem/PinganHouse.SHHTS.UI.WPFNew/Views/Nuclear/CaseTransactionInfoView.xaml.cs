﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;

namespace PinganHouse.SHHTS.UI.WPFNew.Views.Nuclear
{
    /// <summary>
    /// CaseTransactionInfoView.xaml 的交互逻辑
    /// </summary>
    public partial class CaseTransactionInfoView : UserControl
    {
        public CaseTransactionInfoView(TransactionInfoCtrlViewModel viewModel)
        {
            InitializeComponent();
            //transactionInfo.ViewModel = Microsoft.Practices.ServiceLocation.ServiceLocator.Current.GetInstance<TransactionInfoCtrlViewModel>();
            //transactionInfo.ViewModel = viewModel;
            this.DataContext = viewModel;

        }

         public TransactionInfoCtrlViewModel ViewModel
        {
            get
            {
                return (TransactionInfoCtrlViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}
