﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;

namespace PinganHouse.SHHTS.UI.WPFNew.Views
{
    /// <summary>
    /// TransactionInfoCtrl.xaml 的交互逻辑
    /// </summary>
    public partial class TransactionInfoCtrl : UserControl
    {
        private bool _readOnly;

        public TransactionInfoCtrl()
        {
            InitializeComponent();
        }

        public TransactionInfoCtrlViewModel ViewModel
        {
            get
            {
                return (TransactionInfoCtrlViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }

        public bool ReadOnly
        {
            set { _readOnly = value; }
        }

        private void TransactionInfoCtrl_OnLoaded(object sender, RoutedEventArgs e)
        {
            if (_readOnly == true)
            {
                btnBuyerAddCollection.Visibility = Visibility.Collapsed;
                btnBuyerAddPayment.Visibility = Visibility.Collapsed;
                btnBuyerDel.Visibility = Visibility.Collapsed;
                btnSellerAddCollection.Visibility = Visibility.Collapsed;
                btnSellerAddPayment.Visibility = Visibility.Collapsed;
                btnSellerDel.Visibility = Visibility.Collapsed;
                //colCheckBoxBuyer.Visibility = Visibility.Collapsed;
                colCheckBoxSeller.Visibility = Visibility.Collapsed;
            }
            else
            {
                btnBuyerAddCollection.Visibility = Visibility.Visible;
                btnBuyerAddPayment.Visibility = Visibility.Visible;
                btnBuyerDel.Visibility = Visibility.Visible;
                btnSellerAddCollection.Visibility = Visibility.Visible;
                btnSellerAddPayment.Visibility = Visibility.Visible;
                btnSellerDel.Visibility = Visibility.Visible;
                //colCheckBoxBuyer.Visibility = Visibility.Visible;
                colCheckBoxSeller.Visibility = Visibility.Visible;
            }
        }

        private void DataGrid_Buyer_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if ((e.OriginalSource as ScrollViewer) == null)
            {
                var rowIndex = dataGrid_Buyer.SelectedIndex;
                const int columnIndex = 10;
                if (rowIndex >= 0)
                {
                    FrameworkElement item =
                        dataGrid_Buyer.Columns[columnIndex].GetCellContent(dataGrid_Buyer.Items[rowIndex]);
                    DataGridTemplateColumn temp = (dataGrid_Buyer.Columns[columnIndex] as DataGridTemplateColumn);
                    var ck = temp.CellTemplate.FindName("buyerCheckBox", item) as System.Windows.Controls.CheckBox;
                    if (ck.IsChecked == true)
                    {
                        ck.IsChecked = false;
                    }
                    else
                    {
                        ck.IsChecked = true;
                    }

                }
            }
        }

        private void DataGrid_Seller_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if ((e.OriginalSource as ScrollViewer) == null)
            {
                var rowIndex = dataGrid_Seller.SelectedIndex;
                const int columnIndex = 9;
                if (rowIndex >= 0)
                {
                    FrameworkElement item =
                        dataGrid_Seller.Columns[columnIndex].GetCellContent(dataGrid_Seller.Items[rowIndex]);
                    DataGridTemplateColumn temp = (dataGrid_Seller.Columns[columnIndex] as DataGridTemplateColumn);
                    var ck = temp.CellTemplate.FindName("sellerCheckBox", item) as System.Windows.Controls.CheckBox;
                    if (ck.IsChecked == true)
                    {
                        ck.IsChecked = false;
                    }
                    else
                    {
                        ck.IsChecked = true;
                    }

                }
            }
        }
    }
}
