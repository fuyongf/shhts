﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;

namespace PinganHouse.SHHTS.UI.WPFNew.Views
{
    /// <summary>
    /// CaseDetailInfoNoReportView.xaml 的交互逻辑
    /// </summary>
    public partial class CaseDetailInfoNoReportView : UserControl
    {
        public CaseDetailInfoNoReportView(CaseDetailInfoNoReportViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
        }

        public CaseDetailInfoNoReportViewModel ViewModel
        {
            get
            {
                return (CaseDetailInfoNoReportViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}
