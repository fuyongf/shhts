﻿using PinganHouse.SHHTS.UI.WPFNew.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PinganHouse.SHHTS.UI.WPFNew.Common.Dialog;

namespace PinganHouse.SHHTS.UI.WPFNew.Views
{
    /// <summary>
    /// WindowTitleView.xaml 的交互逻辑
    /// </summary>
    public partial class WindowTitleView : UserControl
    {
        public WindowTitleView(WindowTitleViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
        }
        #region 事件

        public static readonly RoutedEvent HomeButtonClickEvent = EventManager.RegisterRoutedEvent(
        "HomeButtonClick", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(WindowTitleView));


        // Expose this event for this control's container
        public event RoutedEventHandler HomeButtonClick
        {
            add { AddHandler(HomeButtonClickEvent, value); }
            remove { RemoveHandler(HomeButtonClickEvent, value); }
        }

        #endregion

        private void SetButton_Click(object sender, RoutedEventArgs e)
        {
            PrintSetting sp = new PrintSetting();
            sp.ShowDialog();
        }
   
    }
}
