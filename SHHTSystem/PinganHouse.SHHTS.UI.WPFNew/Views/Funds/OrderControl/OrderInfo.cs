﻿using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.UI.WPFNew.Views
{
    public class OrderInfo : BaseViewModel
    {
        /// <summary>
        /// 出款方
        /// </summary>
        private AccountType accountType;
        public AccountType AccountType
        {
            get
            {
                return accountType;
            }
            set
            {
                if (accountType != value)
                {
                    accountType = value;
                    NotifyPropertyChanged("AccountType");
                }
            }
        }


        /// <summary>
        /// 余额
        /// </summary>
        private string balance;
        public string Balance
        {
            get
            {
                return balance;
            }
            set
            {
                if (balance != value)
                {
                    balance = value;
                    NotifyPropertyChanged("Balance");
                }
            }
        }


    }
}
