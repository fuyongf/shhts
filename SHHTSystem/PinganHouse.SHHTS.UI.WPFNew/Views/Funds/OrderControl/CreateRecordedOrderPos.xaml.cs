﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPFNew.Common;

namespace PinganHouse.SHHTS.UI.WPFNew.Views
{
    /// <summary>
    /// CreateRecordedOrderPos.xaml 的交互逻辑
    /// </summary>
    public partial class CreateRecordedOrderPos : UserControl
    {
        public CreateRecordedOrderPos()
        {
            InitializeComponent();

            //付款用途
            List<EnumHelper> listNation = EnumHelper.InitOrderBizTypeToCombobox(true,
                new List<OrderBizType>
                {
                    OrderBizType.Collection,
                    OrderBizType.Compensation,
                    OrderBizType.ServiceCharge,
                    OrderBizType.EFQServiceCharge
                });
            CboPaymentUse.ItemsSource = listNation;
            CboPaymentUse.DisplayMemberPath = "DisplayMember";
            CboPaymentUse.SelectedValuePath = "ValueMember";
            CboPaymentUse.SelectedIndex = 0;

            //付款归属
            CboPaymentBelonging.ItemsSource =
                EnumHelper.InitAccountTypeToCombobox(new List<AccountType>
                {
                    AccountType.Agency,
                    AccountType.Person,
                    AccountType.Thirdparty
                });
            CboPaymentBelonging.DisplayMemberPath = "DisplayMember";
            CboPaymentBelonging.SelectedValuePath = "ValueMember";
            CboPaymentBelonging.SelectedIndex = 1;

        }
    }
}
