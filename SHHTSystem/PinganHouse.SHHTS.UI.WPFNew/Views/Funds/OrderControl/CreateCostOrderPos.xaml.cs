﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPFNew.Common;

namespace PinganHouse.SHHTS.UI.WPFNew.Views
{
    /// <summary>
    /// CreateCostOrderPos.xaml 的交互逻辑
    /// </summary>
    public partial class CreateCostOrderPos : UserControl
    {
        public CreateCostOrderPos()
        {
            InitializeComponent();

            //付款用途
            List<EnumHelper> listNation = EnumHelper.InitOrderBizTypeToCombobox(true,
                new List<OrderBizType>
                {
                    OrderBizType.EFQServiceCharge
                });
            CboPaymentUse.ItemsSource = listNation;
            CboPaymentUse.DisplayMemberPath = "DisplayMember";
            CboPaymentUse.SelectedValuePath = "ValueMember";
            CboPaymentUse.SelectedIndex = 0;

        }
    }
}
