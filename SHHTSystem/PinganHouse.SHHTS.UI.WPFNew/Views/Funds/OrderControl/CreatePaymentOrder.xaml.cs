﻿using Microsoft.Practices.ServiceLocation;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using PinganHouse.SHHTS.UI.WPFNew.Controls;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPFNew.Views
{
    /// <summary>
    /// CreatePaymentOrder.xaml 的交互逻辑
    /// </summary>
    public partial class CreatePaymentOrder : UserControl
    {

        /// <summary>
        /// 附件集合
        /// </summary>
        private Dictionary<string, System.IO.Stream> m_FileList;

        public CreatePaymentOrder()
        {
            InitializeComponent();

            //付款用途
            List<EnumHelper> listNation = EnumHelper.InitOrderBizTypeToCombobox(true, new List<OrderBizType> { OrderBizType.EFQServiceCharge });
            CboPaymentUse.ItemsSource = listNation;
            CboPaymentUse.DisplayMemberPath = "DisplayMember";
            CboPaymentUse.SelectedValuePath = "ValueMember";
            CboPaymentUse.SelectedIndex = 0;

            //出款方
            CboPayment.ItemsSource = EnumHelper.InitAccountTypeToCombobox(new List<AccountType> { AccountType.Agency, AccountType.Person, AccountType.Thirdparty });
            CboPayment.DisplayMemberPath = "DisplayMember";
            CboPayment.SelectedValuePath = "ValueMember";
            CboPayment.SelectedIndex = 1;
            CboPayment.SelectionChanged -= CboPayment_SelectionChanged;

            //收款方
            CboRecipient.ItemsSource = EnumHelper.InitAccountTypeToCombobox(new List<AccountType> { AccountType.Buyer, AccountType.Person, AccountType.Thirdparty });
            CboRecipient.DisplayMemberPath = "DisplayMember";
            CboRecipient.SelectedValuePath = "ValueMember";
            CboRecipient.SelectedIndex = 0;

        }

        public PaymentOrderViewModel PaymentInfo
        {
            get { return (PaymentOrderViewModel)GetValue(PaymentInfoProperty); }
            set { SetValue(PaymentInfoProperty, value); }
        }

        public static readonly DependencyProperty PaymentInfoProperty =
            DependencyProperty.Register("PaymentInfo", typeof(PaymentOrderViewModel), typeof(CreatePaymentOrder), new PropertyMetadata(null));

        private void CboPayment_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (PaymentInfo == null)
                return;
            PaymentInfo.PayerSelectIndex = CboPayment.SelectedIndex;
            PaymentInfo.Payer = (AccountType)Convert.ToInt32(CboPayment.SelectedValue);
        }

        private void CboRecipient_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (PaymentInfo == null)
                return;
            PaymentInfo.RecerSelectIndex = CboRecipient.SelectedIndex;
            PaymentInfo.Recer = (AccountType)Convert.ToInt32(CboRecipient.SelectedValue);
        }


        private void BtnUploadFile_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var uploadFileHelper = new UploadFileHelper();
                var files = uploadFileHelper.SelectUploadFile();
                if (files == null || files.Count == 0) return;
                m_FileList = files;
                //fileNum.Tag = null;
                //fileNum.Tag = files;
                //fileNum.Content = files.Count + "个附件";
            }
            catch (Exception ex)
            {
                MessageBox.Show("选择上传文件发生系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BtnUploadPhotograph_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var captureImge = new CaptureImgeDialog("",AttachmentType.出款票据);
                //captureImge.AllUploadedEvent += captureImge_AllUploadedEvent;
            }
            catch (Exception ex)
            {
                MessageBox.Show("选择上传文件发生系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 接收返回的参数
        /// </summary>
        /// <param name="fileStreamList"></param>
        /// <param name="fileNameList"></param>
        /// <param name="captrueImage"></param>
        //void captureImge_AllUploadedEvent(Dictionary<string, System.IO.Stream> files, string filePath, CaptrueImage captrueImage, CameraInterfaceInvoke cameraInvoke, UInt32 mDeviceHandle)
        //{
        //    cameraInvoke.func_CloseDevice(mDeviceHandle);
        //    captrueImage.Close();
        //    if (files == null || files.Count == 0) return;
        //    m_FileList = files;
        //    //fileNum.Tag = null;
        //    //fileNum.Tag = files;
        //    //fileNum.Content = files.Count + "个附件";
        //}

    }
}
