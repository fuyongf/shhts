﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;

namespace PinganHouse.SHHTS.UI.WPFNew.Views
{
    /// <summary>
    /// CreateWriteOffOrder.xaml 的交互逻辑
    /// </summary>
    public partial class CreateWriteOffOrder : UserControl
    {
        public CreateWriteOffOrder()
        {
            InitializeComponent();

            //销账用途
            List<EnumHelper> listNation = EnumHelper.InitOrderBizTypeToCombobox(true, new List<OrderBizType> { OrderBizType.EFQServiceCharge });
            ComboBox1.ItemsSource = listNation; //CboWriteOffUse
            ComboBox1.DisplayMemberPath = "DisplayMember";
            ComboBox1.SelectedValuePath = "ValueMember";
            ComboBox1.SelectedIndex = 0;

            //销账主体
            CboWriteOffSubject.ItemsSource = EnumHelper.InitAccountTypeToCombobox(new List<AccountType> { AccountType.Agency, AccountType.Person, AccountType.Thirdparty });
            CboWriteOffSubject.DisplayMemberPath = "DisplayMember";
            CboWriteOffSubject.SelectedValuePath = "ValueMember";
            CboWriteOffSubject.SelectedIndex = 1;
            //CboWriteOffSubject.SelectionChanged -= CboWriteOffSubject_SelectionChanged;

            //收款方
            CboRecipient.ItemsSource = EnumHelper.InitAccountTypeToCombobox(new List<AccountType> { AccountType.Buyer, AccountType.Person, AccountType.Thirdparty });
            CboRecipient.DisplayMemberPath = "DisplayMember";
            CboRecipient.SelectedValuePath = "ValueMember";
            CboRecipient.SelectedIndex = 0;

        }

        public WriteOffOrderViemModel WriteOffOrderInfo
        {
            get { return (WriteOffOrderViemModel)GetValue(WriteOffOrderInfoProperty); }
            set { SetValue(WriteOffOrderInfoProperty, value); }
        }

        public static readonly DependencyProperty WriteOffOrderInfoProperty =
            DependencyProperty.Register("WriteOffOrderInfo", typeof(WriteOffOrderViemModel), typeof(CreateWriteOffOrder), new PropertyMetadata(null));

        /// <summary>
        /// 销帐主体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboWriteOffSubject_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (WriteOffOrderInfo == null)
                return;
            WriteOffOrderInfo.PayerSelectIndex = CboWriteOffSubject.SelectedIndex;
            WriteOffOrderInfo.Payer = (AccountType)Convert.ToInt32(CboWriteOffSubject.SelectedValue);
        }

        /// <summary>
        /// 收款方
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboRecipient_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (WriteOffOrderInfo == null)
                return;
            WriteOffOrderInfo.RecerSelectIndex = CboRecipient.SelectedIndex;
            WriteOffOrderInfo.Recer = (AccountType)Convert.ToInt32(CboRecipient.SelectedValue);
        }
    }
}
