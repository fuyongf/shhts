﻿using PinganHouse.SHHTS.UI.WPFNew.ViewModels;

using System.Windows.Controls;

namespace PinganHouse.SHHTS.UI.WPFNew.Views
{
    /// <summary>
    /// DepositFundsView.xaml 的交互逻辑
    /// </summary>
    public partial class DepositFundsListView : UserControl
    {
        public DepositFundsListView(DepositFundsViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
        }
        public DepositFundsViewModel ViewModel
        {
            get
            {
                return (DepositFundsViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
                this.GridPager.PageCount = ViewModel.TotalPageCount;
            }
        }
        private void GridPager_PagerIndexChanged(int pageIndex)
        {
            ViewModel.IsPageChanged = true;
            ViewModel.CurrentPageIndex = this.GridPager.PageIndex;
        }
    }
}
