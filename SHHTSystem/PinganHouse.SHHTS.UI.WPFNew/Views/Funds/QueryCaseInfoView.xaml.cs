﻿using PinganHouse.SHHTS.UI.WPFNew.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPFNew.Views
{
    /// <summary>
    /// QueryCaseInfoView.xaml 的交互逻辑
    /// </summary>
    public partial class QueryCaseInfoView : UserControl
    {
        public QueryCaseInfoView(QueryCaseInfoViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
        }

        public QueryCaseInfoViewModel ViewModel
        {
            get { return (QueryCaseInfoViewModel)this.DataContext; }
            set { this.DataContext = value; }
        }

        private void GridPager_PagerIndexChanged(int pageIndex)
        {
            ViewModel.IsPageChanged = true;
            ViewModel.CurrentPageIndex = this.GridPager.PageIndex;
        }

        private void TbNation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
