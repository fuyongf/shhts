﻿using Microsoft.Practices.ServiceLocation;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPFNew.Views
{
    /// <summary>
    /// CreatOrderView.xaml 的交互逻辑
    /// </summary>
    public partial class CreatOrderView : UserControl
    {
        public CreatOrderView(CreatOrderViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
        }

        public CreatOrderViewModel ViewModel
        {
            get
            {
                return (CreatOrderViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }

        /// <summary>
        /// 下拉框事件（根据订单类型不同显示不同的订单）
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboOrderType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            var selectValue = CboOrderType.SelectedIndex;
            switch (selectValue)
            {
                case 0:
                    CollapsedOrderTypeGrid();
                    SpOrderInfo.Visibility = Visibility.Visible;
                    break;
                case 1:
                    CollapsedOrderTypeGrid();
                    CreatePaymentOrder.Visibility = Visibility.Visible;
                    ViewModel.PaymentOrderInfo = ServiceLocator.Current.GetInstance<PaymentOrderViewModel>();
                    ViewModel.PaymentOrderInfo.CaseId = ViewModel.CaseId;
                    ViewModel.PaymentOrderInfo.CaseSysNo = ViewModel.CaseInfo.SysNo;
                    ViewModel.PaymentOrderInfo.CompanySysNo = ViewModel.CaseInfo.CompanySysNo;
                    ViewModel.PaymentOrderInfo.Payer = ViewModel.PaymentOrderInfo.Payer;
                    break;
                case 2:
                    CollapsedOrderTypeGrid();
                    CreateRecordedOrderPos.Visibility = Visibility.Visible;
                    ViewModel.RecordedOrderPosInfo = ServiceLocator.Current.GetInstance<RecordedOrderPosViemModel>();
                    ViewModel.RecordedOrderPosInfo.CaseId = ViewModel.CaseId;
                    break;
                case 3:
                    CollapsedOrderTypeGrid();
                    CreateRecordedOrderTransferAccounts.Visibility = Visibility.Visible;
                    break;
                case 4:
                    CollapsedOrderTypeGrid();
                    CreateWriteOffOrder.Visibility = Visibility.Visible;
                    break;
                case 5:
                    CollapsedOrderTypeGrid();
                    CreateCostOrderPos.Visibility = Visibility.Visible;
                    ViewModel.CostOrderPosInfo = ServiceLocator.Current.GetInstance<CostOrderPosViemModel>();
                    ViewModel.CostOrderPosInfo.CaseId = ViewModel.CaseId;
                    break;
                case 6:
                    CollapsedOrderTypeGrid();
                    CreateCostOrderTransferAccounts.Visibility = Visibility.Visible;
                    break;
            }
        }

        /// <summary>
        /// 各订单类型隐藏
        /// </summary>
        private void CollapsedOrderTypeGrid()
        {
            SpOrderInfo.Visibility = Visibility.Collapsed; //订单类型选择
            CreatePaymentOrder.Visibility = Visibility.Collapsed; //出账订单
            CreateRecordedOrderPos.Visibility = Visibility.Collapsed; //入账订单（POS）
            CreateRecordedOrderTransferAccounts.Visibility = Visibility.Collapsed; //入账订单（转账）
            CreateWriteOffOrder.Visibility = Visibility.Collapsed; //销账订单
            CreateCostOrderPos.Visibility = Visibility.Collapsed; //费用订单（POS）
            CreateCostOrderTransferAccounts.Visibility = Visibility.Collapsed; //费用订单（转账）
            //IdInfo = new IdInfoModel();
        }


    }
}
