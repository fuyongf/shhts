﻿using PinganHouse.SHHTS.UI.WPFNew.Common;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;

using System.Windows.Controls;

namespace PinganHouse.SHHTS.UI.WPFNew.Views
{
    /// <summary>
    /// AreaSelectedView.xaml 的交互逻辑
    /// </summary>
    public partial class AreaSelectedView : UserControl
    {
        public AreaSelectedView(LoginViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
        }
        public LoginViewModel ViewModel
        {
            get
            {
                return (LoginViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}
