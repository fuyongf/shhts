﻿using System.Windows;

namespace PinganHouse.SHHTS.UI.WPFNew.Views
{
    /// <summary>
    /// ShellView.xaml 的交互逻辑
    /// </summary>
    public partial class ShellView : Window
    {      
        public ShellView()
        {
            InitializeComponent();       
            this.Left = 0; //设置位置
            this.Top = 0;
            Rect rc = SystemParameters.WorkArea ; //获取工作区大小
            this.Width = rc.Width;
            this.Height = rc.Height;
        }

        private void MinButton_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
