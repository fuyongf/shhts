﻿using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;

using PinganHouse.SHHTS.UI.WPFNew.Controllers;

namespace PinganHouse.SHHTS.UI.WPFNew.Modules
{
    public class PreCheckModule:IModule
    {
        private readonly IUnityContainer container;
        private readonly IRegionManager regionManager;

        private PreCheckController preCheckController;

        public PreCheckModule(IUnityContainer container, IRegionManager regionManager)
        {
            this.container = container;
            this.regionManager = regionManager;
        }

        public void Initialize()
        {
            this.preCheckController = this.container.Resolve<PreCheckController>();
        }
    }
}
