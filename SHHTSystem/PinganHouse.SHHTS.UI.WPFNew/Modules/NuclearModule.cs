﻿using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;

using PinganHouse.SHHTS.UI.WPFNew.Controllers;

namespace PinganHouse.SHHTS.UI.WPFNew.Modules
{
    public class NuclearModule : IModule
    {
        private readonly IUnityContainer container;
        private readonly IRegionManager regionManager;
        private NuclearController nuclearController;

        public NuclearModule(IUnityContainer container, IRegionManager regionManager)
        {
            this.container = container;
            this.regionManager = regionManager;
        }

        public void Initialize()
        {
            this.nuclearController = this.container.Resolve<NuclearController>();
        }
    }
}
