﻿using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;

using PinganHouse.SHHTS.UI.WPFNew.Common;
using PinganHouse.SHHTS.UI.WPFNew.Common.CommonClass;
using PinganHouse.SHHTS.UI.WPFNew.Controllers;
using PinganHouse.SHHTS.UI.WPFNew.Controls;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;
using PinganHouse.SHHTS.UI.WPFNew.Views;

using System;
using System.Configuration;

namespace PinganHouse.SHHTS.UI.WPFNew.Modules
{
   public class LoginModule : IModule
    {
        private readonly IUnityContainer container;
        private readonly IRegionManager regionManager;

        private LoginController loginController;

        public LoginModule(IUnityContainer container, IRegionManager regionManager)
        {
            this.container = container;
            this.regionManager = regionManager;
        }

        public void Initialize()
        {
            var viewModel = ServiceLocator.Current.GetInstance<LoginViewModel>();


            #region 初始化界面信息
            try
            {
                var list = EnumHelper.InitReceptionCenterToCombobox();
                list.RemoveAt(0);
                foreach (var item in list)
                {
                    viewModel.AreaList.Add(item);
                }
                //从注册表加载信息
                var register = new Register();
                var softName = ConfigurationManager.AppSettings["SoftName"];
                var rememberFlag = register.ReadRegeditKey("RememberFlag");
                var historyName = register.ReadRegeditKey("HistoryName");
                if (rememberFlag == null && historyName == null)
                {
                    register.WriteRegeditKey("RememberFlag", false);
                    register.WriteRegeditKey("HistoryName", string.Empty);
                }
                else
                {
                    bool IsChecked;
                    bool.TryParse(Convert.ToString(rememberFlag), out IsChecked);
                    viewModel.IsRememberFlag= IsChecked;
                    if (IsChecked)
                    {
                        viewModel.UserName = Convert.ToString(historyName);
                    }
                }

                var rememberArea = PrivateProfileUtils.GetContentValue("Area", "SH");
                if (!string.IsNullOrWhiteSpace(rememberArea))
                    viewModel.CurrentIndex = int.Parse(rememberArea);
            }
            catch (Exception)
            {
                viewModel.IsRememberFlag = false;
            }
            #endregion

            IRegion titleRegion = this.regionManager.Regions[RegionNames.TitleRegion];
            if (titleRegion == null) return;
            AreaSelectedView titleView = titleRegion.GetView(ViewNamesKey.AREA_SELECTED_VIEW) as AreaSelectedView;
            if (titleView == null)
            {
                //Create a new instance of the EmployeeDetailsView using the Unity container.
                titleView = this.container.Resolve<AreaSelectedView>();
                titleView.DataContext = viewModel;
                //Add the view to the main region. This automatically activates the view too.
                titleRegion.Add(titleView, ViewNamesKey.AREA_SELECTED_VIEW);
                titleRegion.Activate(titleView);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                titleView.DataContext = viewModel;
                titleRegion.Activate(titleView);
            }

            IRegion mainRegion = this.regionManager.Regions[RegionNames.MainRegion];
            if (mainRegion == null) return;
            LoginView view = mainRegion.GetView(ViewNamesKey.LOGIN_VIEW) as LoginView;
            if (view == null)
            {
                //Create a new instance of the EmployeeDetailsView using the Unity container.
                view = this.container.Resolve<LoginView>();
                view.DataContext = viewModel;
                //Add the view to the main region. This automatically activates the view too.
                mainRegion.Add(view, ViewNamesKey.LOGIN_VIEW);
                mainRegion.Activate(view);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                view.DataContext = viewModel;
                mainRegion.Activate(view);
            }
            this.loginController = this.container.Resolve<LoginController>();
            if (viewModel.CurrentIndex == -1)
            {
                AreaSelectedDialog windows = new AreaSelectedDialog(viewModel);
                windows.ShowDialog();
            }
        }
    }
}
