﻿using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;

using PinganHouse.SHHTS.UI.WPFNew.Controllers;

namespace PinganHouse.SHHTS.UI.WPFNew.Modules
{
    public class MainModule:IModule
    {
        private readonly IUnityContainer container;
        private readonly IRegionManager regionManager;

        private MainController mainController;

        public MainModule(IUnityContainer container, IRegionManager regionManager)
        {
            this.container = container;
            this.regionManager = regionManager;
        }

        public void Initialize()
        {
            this.mainController = this.container.Resolve<MainController>();
        }
    }
}
