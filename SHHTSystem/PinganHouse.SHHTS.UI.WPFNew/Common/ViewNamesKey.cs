﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.UI.WPFNew.Common
{
    public static class ViewNamesKey
    {
        public static string LOGIN_VIEW = "LoginView";
        public static string MAIN_VIEW = "MainView";
        public static string WINDOW_TITLE_VIEW = "WindowTitleView";
        #region 预检
        public static string ADD_CASE_VIEW = "AddCaseView";
        public static string UPLOAD_ATTACHEMENT_VIEW = "UploadAttachmentView";
        public static string SEARCH_LIST_PAGE_VIEW = "SearchListPageView";
        public static string AREA_SELECTED_VIEW = "AreaSelectedView";
        public static string LOADING_CONTROL_VIEW = "LoadingControl";
        #endregion

        #region 资金
        public static string QUERY_CASEINFO = "QueryCaseInfoView";
        public static string CREATE_ORDER = "CreatOrderView";

        public static string NUCLEAR_APPROVAL_REPORT_VIEW = "ApprovalReportView";
        public static string NUCLEAR_BASICINFO_VIEW = "BasicInfoView";
        public static string NUCLEAR_CASE_CUSTOMER_INFO_VIEW = "CaseCustomerInfoView";
        public static string NUCLEAR_CASE_DETAIL_INFO_NO_REPORT_VIEW = "CaseDetailInfoNoReportView";
        public static string NUCLEAR_CASE_DETAIL_INFO_VIEW = "CaseDetailInfoView";
        public static string NUCLEAR_CASE_TRANSACTION_INFO_VIEW = "CaseTransactionInfoView";
        public static string NUCLEAR_CHECK_CASE_SEARCH_PAGE_VIEW = "CheckCaseSearchPageView";
        public static string DEPOSIT_FUNDS_LIST_VIEW= "DepositFundsListView";
        public static string NUCLEAR_CASE_DETAIL_INFO_NO_REPORT_VIEWView = "CaseDetailInfoNoReportView";

        #endregion
    }
}
