﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.UI.WPFNew.Common.CommonClass
{
    class PrintModel
    {
        public int PrintNo { get; set; }

        public string PrintName { get; set; }
    }

    class PropertyReceiptModel
    {
        /// <summary>
        /// 收据据编号
        /// </summary>
        public string ReceiptNo { get; set; }
        /// <summary>
        /// 票据流水号
        /// </summary>
        public string ReceiptSerialNo { get; set; }

        /// <summary>
        /// 收款单位
        /// </summary>
        public string Payee { get; set; }

        /// <summary>
        /// 付款单位
        /// </summary>
        public string Payer { get; set; }

        /// <summary>
        /// 出具日期
        /// </summary>
        public string ReceiptDate { get; set; }

        /// <summary>
        /// 收据金额
        /// </summary>
        public string Money { get; set; }

        public string OrderNo { get; set; }


        /// <summary>
        /// 票据流水号
        /// </summary>
        //public string ReceiptSerialNo { get; set; }

        /// <summary>
        /// 验真码
        /// </summary>
        public string SecurityCode { get; set; }

        /// <summary>
        /// 案件编号
        /// </summary>
        public string CaseId { get; set; }

        /// <summary>
        /// 物业地址
        /// </summary>
        public string TenementAddress { get; set; }


        /// <summary>
        /// 支付方式--付款方式
        /// </summary>
        public string PaymentChannel { get; set; }

        /// <summary>
        /// 业务类型--付款性质
        /// </summary>
        public string OrderBizType { get; set; }

        /// <summary>
        /// 付款人手机号码
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 详细地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 邮编
        /// </summary>
        public string PostCode { get; set; }

        /// <summary>
        /// 账户类型
        /// </summary>
        public AccountType AccountType { get; set; }

        /// <summary>
        /// 交易性质
        /// </summary>
        public string TradeProperty
        {
            get
            {
                return "买卖";
            }
        }
        /// <summary>
        /// 付款金额（大写）
        /// </summary>
        public string PaymentAmountUpperCase { get; set; }

        /// <summary>
        /// 受理中心
        /// </summary>
        public string AdmissibilityCentral { get; set; }

        /// <summary>
        /// 开具人
        /// </summary>
        public string Creater { get; set; }

        /// <summary>
        /// 条形码
        /// </summary>
        public WriteableBitmap ImageBarCode { get; set; }

        /// <summary>
        /// 案件编号条形码
        /// </summary>
        public string CaseIdBarCode { get; set; }
        /// <summary>
        /// PDF417
        /// </summary>
        public WriteableBitmap ImagePdf { get; set; }

        /// <summary>
        /// 第几联
        /// </summary>
        public string Num { get; set; }

        /// <summary>
        /// 第几联名称字1
        /// </summary>
        public string Unite { get; set; }
        /// <summary>
        /// 第几联名称字2
        /// </summary>
        public string Unite1 { get; set; }
    }
}
