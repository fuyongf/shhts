﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using PinganHouse.SHHTS.UI.WPFNew.Common.CommonClass;

namespace PinganHouse.SHHTS.UI.WPFNew.Common
{
    internal class SettingPage
    {
        #region 公共变量

        /// <summary>
        /// 打印机列表
        /// </summary>
        private List<PrintModel> _ListPrint = new List<PrintModel>();

        /// <summary>
        /// 打印机名字
        /// </summary>
        private string _ReceiptPrintName;

        /// <summary>
        /// 生成条形码数
        /// </summary>
        private int _BarCodeNum = 1;

        #endregion

        /// <summary>
        /// 从注册表加载信息
        /// </summary>
        public void PrintersLoad()
        {
            try
            {
                #region 获得本机所有打印机
                var printers = PrinterSettings.InstalledPrinters; //获取本机上的所有打印机      
                PrintModel pm = new PrintModel {PrintNo = 0, PrintName = "未分配"};
                _ListPrint.Add(pm);
                for (int i = 0; i < printers.Count; i++)
                {
                    PrintModel print = new PrintModel();
                    print.PrintNo = i + 1;
                    print.PrintName = printers[i].ToString();
                    _ListPrint.Add(print);
                }
                #endregion        

                //从注册表加载 条形码打印机信息          
                _ReceiptPrintName = PrivateProfileUtils.GetContentValue("BarCodePrint", "PrintName"); //打印机名
                //从注册表加载 生成条形码数         
                string sBarCodeNum = PrivateProfileUtils.GetContentValue("BarCodeNum", "Num"); //生成条形码数  
                _BarCodeNum = !string.IsNullOrEmpty(sBarCodeNum)
                    ? int.Parse(PrivateProfileUtils.GetContentValue("BarCodeNum", "Num"))
                    : 1;
            }
            catch (Exception ex)
            {
                // ignored
            }
        }

        public  bool SetCodePrint(string message,out string errorMessage)
        {
            errorMessage = null;
            //获取条形码条数
            PrintersLoad();
            #region 条形码打印机信息

            int printCount = 0;

            if (!string.IsNullOrWhiteSpace(_ReceiptPrintName))
            {
                for (int j = 0; j < _ListPrint.Count; j++)
                {
                    if (_ReceiptPrintName == _ListPrint[j].PrintName)
                    {
                        printCount++;
                    }
                }
            }
            else //为空，则 收据打印机未分配
            {
                errorMessage = "该条形码打印机未设置，无法进行打印操作";
                return false;
            }
            if (printCount == 0)
            {
                errorMessage = "该条形码打印机未设置，无法进行打印操作";
                return false;
            }
            if (_ReceiptPrintName.Contains("未分配"))
            {
                errorMessage = "该条形码打印机未设置，无法进行打印操作";
                return false;
            }
            #endregion

            for (int i = 0; i < _BarCodeNum; i++)
            {
                var icp = new CaseCodePrint();
                icp.PrintCaseBarCode(message);
            }
         
            return true;
        }
    }
}

