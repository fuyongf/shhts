﻿using System;
using System.Collections.Generic;
using System.Drawing.Text;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.UI.WPFNew.Common
{
    class BarCodeHelper
    {
        public GCHandle hObject;

        public IntPtr intptr;

        public PrivateFontCollection privateFontCollection;

        public GCHandle hObject128;

        public IntPtr intptr128;

        public PrivateFontCollection privateFontCollection128;

        public BarCodeHelper()
        {
            hObject = GCHandle.Alloc(Properties.Resources.OCRB10N, GCHandleType.Pinned);

            intptr = hObject.AddrOfPinnedObject();

            privateFontCollection = new PrivateFontCollection();

            privateFontCollection.AddMemoryFont(intptr, Properties.Resources.OCRB10N.Length);//从内存加载

            hObject128 = GCHandle.Alloc(Properties.Resources.code128, GCHandleType.Pinned);

            intptr128 = hObject128.AddrOfPinnedObject();

            privateFontCollection128 = new PrivateFontCollection();

            privateFontCollection128.AddMemoryFont(intptr128, Properties.Resources.code128.Length);//从内存加载

        }

    }
}
