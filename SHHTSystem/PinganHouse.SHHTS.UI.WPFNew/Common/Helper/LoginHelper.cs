﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PinganHouse.SHHTS.DataTransferObjects;

namespace PinganHouse.SHHTS.UI.WPFNew.Common
{
    class LoginHelper
    {
        /// <summary>
        /// 当前登录
        /// </summary>
        private static User _CurrentUser = null;

        public static User CurrentUser
        {
            get
            {
                if (_CurrentUser == null)
                    _CurrentUser = new User();
                return _CurrentUser;
            }
        }
    }
}
