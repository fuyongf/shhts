﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Practices.ServiceLocation;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;

namespace PinganHouse.SHHTS.UI.WPFNew.Common
{
    class ConfigHelper
    {
        public static int AreaIndex=-1;
        public static ReceptionCenter GetCurrentReceptionCenter()
        {
            if (AreaIndex == -1)
            {
                MessageBox.Show("请选择所属区域", "系统提示");
                return ReceptionCenter.Unkonw;
            }
            return (ReceptionCenter)(AreaIndex+1);
        }
    }
}
