﻿using System.ComponentModel;

namespace PinganHouse.SHHTS.UI.WPFNew.Common.Enums
{
    public enum NetworkOption
    {
        /// <summary>
        /// 自动检测
        /// </summary>
        [Description("自动检测")]
        Auto = 0,
        /// <summary>
        /// CN2
        /// </summary>
        [Description("CN2网络")]
        CN2 = 1,
        /// <summary>
        /// 互联网
        /// </summary>
        [Description("互联网")]
        Internet = 2
    }
}
