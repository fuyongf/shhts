﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.UI.WPFNew.Common
{
    class ValidationCommon
    {
        /// <summary>
        /// 判断是否为空
        /// </summary>
        /// <param name="str">验证参数</param>
        /// <returns>返回结果</returns>
        public static bool IsEmpty(string str)
        {
            return string.IsNullOrEmpty(str);
        }

        public static bool IsUserMatch(string str)
        {
            return new Regex(@"^[a-zA-Z\s\u4e00-\u9fa5]+$").IsMatch(str);
        }
        /// <summary>
        /// 身份证验证
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsIdMatch(string str)
        {
            return new Regex(@"^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}(\d|x|X)$").IsMatch(str);
        }
        /// <summary>
        /// 港澳居民身份证验证
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsHkMacaoIdMatch(string str)
        {
            return new Regex(@"^[A-Za-z]\d{6}\(\d\)$").IsMatch(str);
        }
        /// <summary>
        /// 港澳居民往来内地通行证验证
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsHkMacaoPermitMatch(string str)
        {
            return new Regex(@"^[A-Za-z]\d{8}$").IsMatch(str);
        }
        /// <summary>
        /// 台湾居民往来内地通行证验证
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsTaiwanPermitMatch(string str)
        {
            return new Regex(@"^[A-Za-z]?(\d{7}|\d{10})(\([A-Za-z]\))?$").IsMatch(str);
        }
        /// <summary>
        /// 军官证验证
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsOfficerMatch(string str)
        {
            return new Regex(@"^[\u4e00-\u9fa5]\u5b57\u7b2c\d{8}\u53f7$").IsMatch(str);
        }
        /// <summary>
        /// 出生证验证
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsBirthCertifiCateMatch(string str)
        {
            return new Regex(@"^[A-Za-z]\d{9}$").IsMatch(str);
        }
        public static bool IsUmIdMatch(string str)
        {
            return new Regex(@"^([A-Za-z]+)\d{3}$").IsMatch(str);
        }
    }
}
