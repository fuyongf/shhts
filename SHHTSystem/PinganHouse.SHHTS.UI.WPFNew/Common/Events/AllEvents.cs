﻿using System;
using Microsoft.Practices.Prism.PubSubEvents;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPFNew.Controls;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels.NuclearModel;

namespace PinganHouse.SHHTS.UI.WPFNew.Common
{
    #region 登录
    /// <summary>
    /// 登录
    /// </summary>
    public class LoginEvents : PubSubEvent<LoginViewModel>
    {
    }

    /// <summary>
    /// 返回主页面
    /// </summary>
    public class BackToMainViewEvents : PubSubEvent<object>
    {
    }

    public class LogOutEvents : PubSubEvent<object>
    {
    }
    #endregion

    #region 预检
    #region 预检
    /// <summary>
    /// 添加新案件
    /// </summary>
    public class PreCheckAddCaseEvents : PubSubEvent<object>
    { }
    /// <summary>
    /// 进入附件上传页面通过附件上传按钮
    /// </summary>
    public class AttachmentsDetailEvents : PubSubEvent<string>
    { }
    /// <summary>
    /// 进入详细信息页面通过案件编号
    /// </summary>
    public class LoadBasicInfoByCaseIdEvents : PubSubEvent<string>
    { }
    /// <summary>
    /// 上传附件
    /// </summary>
    public class UploadAttachmentEvents : PubSubEvent<AttachmentType>
    { }
    /// <summary>
    /// 高拍仪
    /// </summary>
    public class GaoPaiTakePhotosEvents : PubSubEvent<AttachmentType>
    { }
    /// <summary>
    /// 高拍仪上传附件
    /// </summary>
    public class GaoPaiUploadAttachmentEvents : PubSubEvent<GaoPaiPhotosViewModel>
    { }
    public class LoadMateriaListByCustomerIdEvents : PubSubEvent<string>
    { }

    /// <summary>
    /// 刷新附件上传页面
    /// </summary>
    public class PreCheckRefreshedUploadAttachementEvents : PubSubEvent<string>
    { }
    /// <summary>
    /// 添加客户信息
    /// </summary>
    public class AddCustomerInfoEvents : PubSubEvent<PreCheckAddCaseViewModel>
    { }
    /// <summary>
    /// 更新客户信息
    /// </summary>
    public class UpdateCustomerInfoEvents : PubSubEvent<PreCheckAddCaseViewModel>
    { }
    public class NavigateToMainListEvents : PubSubEvent<object>
    { }
    /// <summary>
    /// 生成新案件
    /// </summary>
    public class CreateNewCaseEvents : PubSubEvent<PreCheckAddCaseViewModel>
    { }
    /// <summary>
    /// 生成新案件并且打印条码
    /// </summary>
    public class CreateNewCaseAndPrintBarcardEvents : PubSubEvent<PreCheckAddCaseViewModel>
    { }
    /// <summary>
    /// 翻页
    /// </summary>
    public class PageChangesEvents:PubSubEvent<int>
    { }
    /// <summary>
    /// 显示微信
    /// </summary>
    public class ShowMircroMessageEvents : PubSubEvent<string>
    { }
    /// <summary>
    /// 微信更新客户信息
    /// </summary>
    public class ShowMircroMessageUpdateCustomerInfoEvents : PubSubEvent<ShowMircroMessageViewModel>
    { }
    /// <summary>
    /// load预检案件列表
    /// </summary>
    public class LoadPreCheckCaseListEvents : PubSubEvent<SearchListPageViewModel>
    { }
    /// <summary>
    /// 预检高级搜索
    /// </summary>
    public class PreCheckSeniorSeacheEvents : PubSubEvent<object>
    { }
    /// <summary>
    /// 预检取消签约
    /// </summary>
    public class PreCheckCancerCaseEvents : PubSubEvent<string>
    { }
    /// <summary>
    /// 预检打印条码
    /// </summary>
    public class PreCheckPrintCaseEvents : PubSubEvent<string>
    { }
    #endregion

    #region 预检列表页
    /// <summary>
    /// 返回预检列表页
    /// </summary>
    public class SearchListPageEvents : PubSubEvent<object>
    {
    }
    #endregion
    #endregion

    #region 核案
    public class NuclearEvents : PubSubEvent<CheckCaseSearchPageViewModel>
    { }
    /// <summary>
    /// 核案列表操作里的审核报告按钮
    /// </summary>
    public class ApprovalReportEvents : PubSubEvent<string>
    { }
    /// <summary>
    /// load BasicInfo
    /// </summary>
    public class NuclearLoadBasicInfoEvents : PubSubEvent<string>
    { }
    /// <summary>
    /// submit BasicInfo
    /// </summary>
    public class NuclearSubBasicInfoEvents : PubSubEvent<BaseInfoViewModel> 
    { }
    /// <summary>
    /// laod TransactionInfo
    /// </summary>
    public class NuclearLoadTransactionInfoEvent : PubSubEvent<string>
    { }

    /// <summary>
    /// 确认接单
    /// </summary>
    public class StartContractEvents : PubSubEvent<string>
    { }
    /// <summary>
    /// 取消签约
    /// </summary>
    public class CancelContractEvents : PubSubEvent<string>
    { }
    /// <summary>
    /// 确认完成接单
    /// </summary>
    public class ConfirmContractEvents :PubSubEvent<string>
    { }
    public class ConfirmContractEnterEvents : PubSubEvent<ConfirmContractDialogViewModel>
    { }
    /// <summary>
    /// 核案列表页翻页
    /// </summary>
    public class NuclearPageChangesEvents : PubSubEvent<int>
    { }

    public class FundFlowDialogEnterCommandEvents : PubSubEvent<FundFlowDialogViewModel>
    { }

    public class FundFlowDialogSaveCommandEvents : PubSubEvent<FundFlowDialogViewModel>
    { }
    
    public class AddFundCommandEvents : PubSubEvent<Tuple<CustomerType,ChangeFundType,string>>
    { }
    public class FundFlowDialogTextChangedCommandEvents : PubSubEvent<FundFlowDialogViewModel>
    { }
    public class DeleteFundFlowCommandEvents : PubSubEvent<Tuple<TransactionInfoCtrlViewModel,string>>
    { }
    public class LoadNuclearCaseDetailInfoCommandEvents : PubSubEvent<string>
    { }

    #endregion

    #region 资金结算

    /// <summary>
    /// 进入搜索案件列表页
    /// </summary>
    public class QueryCasePageEvents : PubSubEvent<object>
    {
    }

    /// <summary>
    /// 搜索案件页加载
    /// </summary>
    public class LoadQueryCaseInfoEvents : PubSubEvent<QueryCaseInfoViewModel>
    { }

    public class LoadDepositFundsListEvents : PubSubEvent<DepositFundsViewModel>
    { }
    public class DepositFundPageChangesEvents : PubSubEvent<int>
    { }
    /// <summary>
    /// 进入创建订单页面通过创建订单按钮
    /// </summary>
    public class CreateOrderCaseInfoEvents : PubSubEvent<string>
    { }

    public class AllOrderEvents : PubSubEvent<object>
    { }
    public class MoneyBookEvents : PubSubEvent<object>
    { }
    public class MoneyFlowEvents : PubSubEvent<object>
    { }
    public class BillManageEvents : PubSubEvent<object>
    { }
    public class RefundEvents : PubSubEvent<object>
    { }

    public class PaymentOrderAccountChangeEvents : PubSubEvent<PaymentOrderViewModel>
    { }

    public class GetBankAccountsEvents : PubSubEvent<PaymentOrderViewModel>
    { }

    public class SubmitPaymentOrderEvents : PubSubEvent<CreatOrderViewModel>
    { }

   #endregion
}
