﻿using PinganHouse.SHHTS.Enumerations;
using System;
using System.Windows.Data;

namespace PinganHouse.SHHTS.UI.WPFNew.Common
{
    public class AttachmentTypeToStringConverter:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string result=string.Empty;
            var temp = (AttachmentType)value;
            switch(temp)
            {
                case AttachmentType.未知:
                    result = string.Empty;
                    break;
                case AttachmentType.身份证:
                    result = "身份证";
                    break;
                case AttachmentType.户口本:
                    result = "户口本";
                    break;
                case AttachmentType.出生证明:
                    result = "出生证明";
                    break;
                case AttachmentType.结婚证:
                    result = "结婚证";
                    break;
                case AttachmentType.离婚证:
                    result = "离婚证";
                    break;
                case AttachmentType.单身证明:
                    result = "单身证明";
                    break;
                case AttachmentType.授权委托书:
                    result = "授权委托书";
                    break;
                case AttachmentType.代理人身份证:
                    result = "代理人身份证";
                    break;
                case AttachmentType.军官证:
                    result = "军官证";
                    break;
                case AttachmentType.法人类:
                    result = "法人类";
                    break;
                case AttachmentType.外籍人士:
                    result = "外籍人士";
                    break;
                case AttachmentType.社会保险:
                    result = "社会保险";
                    break;
                case AttachmentType.产证:
                    result = "产证";
                    break;
                case AttachmentType.他证:
                    result = "他证";
                    break;
                case AttachmentType.购房发票:
                    result = "购房发票";
                    break;
                case AttachmentType.原购房发票:
                    result = "原购房发票";
                    break;
                case AttachmentType.原购房合同:
                    result = "原购房合同";
                    break;
                case AttachmentType.原契税发票:
                    result = "原契税发票";
                    break;
                case AttachmentType.预告登记证:
                    result = "预告登记证";
                    break;
                case AttachmentType.法院判决书:
                    result = "法院判决书";
                    break;
                case AttachmentType.拍卖确认书:
                    result = "拍卖确认书";
                    break;
                case AttachmentType.产调证:
                    result = "产调证";
                    break;
                case AttachmentType.核价单:
                    result = "核价单";
                    break;
                case AttachmentType.收件收据房屋状况查询:
                    result = "收件收据房屋状况查询";
                    break;
                case AttachmentType.限购查询结果:
                    result = "限购查询结果";
                    break;
                case AttachmentType.税收受理回执:
                    result = "税收受理回执";
                    break;
                case AttachmentType.契税完税凭证:
                    result = "契税完税凭证";
                    break;
                case AttachmentType.个人所得税单:
                    result = "个人所得税单";
                    break;
                case AttachmentType.房产税认定书:
                    result = "房产税认定书";
                    break;
                case AttachmentType.收件收据注销抵押:
                    result = "收件收据注销抵押";
                    break;
                case AttachmentType.收件收据产权过户:
                    result = "收件收据产权过户";
                    break;
                case AttachmentType.住房情况申请表:
                    result = "住房情况申请表";
                    break;
                case AttachmentType.网签合同:
                    result = "网签合同";
                    break;
                case AttachmentType.资金托管协议:
                    result = "资金托管协议";
                    break;
                case AttachmentType.佣金托管协议:
                    result = "佣金托管协议";
                    break;
                case AttachmentType.装修补偿协议书:
                    result = "装修补偿协议书";
                    break;
                case AttachmentType.房屋交接书:
                    result = "房屋交接书";
                    break;
                case AttachmentType.解约协议书:
                    result = "解约协议书";
                    break;
                case AttachmentType.优先购买切结书:
                    result = "优先购买切结书";
                    break;
                case AttachmentType.限购政策告知书:
                    result = "限购政策告知书";
                    break;
                case AttachmentType.中介定金收据:
                    result = "中介定金收据";
                    break;
                case AttachmentType.房款收据:
                    result = "房款收据";
                    break;
                case AttachmentType.佣金收据:
                    result = "佣金收据";
                    break;
                case AttachmentType.税费发票:
                    result = "税费发票";
                    break;
                case AttachmentType.服务费发票:
                    result = "服务费发票";
                    break;
                case AttachmentType.上家提前还款结算凭证:
                    result = "上家提前还款结算凭证";
                    break;
                case AttachmentType.退款申请书:
                    result = "退款申请书";
                    break;
                case AttachmentType.收据遗失证明:
                    result = "收据遗失证明";
                    break;
                case AttachmentType.权利凭证其他:
                    result = "权利凭证其他";
                    break;
                case AttachmentType.交易材料其他:
                    result = "交易材料其他";
                    break;
                case AttachmentType.合同协议其他:
                    result = "合同协议其他";
                    break;
                case AttachmentType.钱款收据其他:
                    result = "钱款收据其他";
                    break;
                case AttachmentType.上家其他:
                    result = "上家其他";
                    break;
                case AttachmentType.下家其他:
                    result = "下家其他";
                    break;
                case AttachmentType.营业税发票:
                    result = "营业税发票";
                    break;
                case AttachmentType.离婚判决书:
                    result = "离婚判决书";
                    break;
                case AttachmentType.还贷款通知书:
                    result = "还贷款通知书";
                    break;
                case AttachmentType.产证密码条:
                    result = "产证密码条";
                    break;
                case AttachmentType.经济人证书:
                    result = "经济人证书";
                    break;
                case AttachmentType.居间公司营业执照复印件:
                    result = "居间公司营业执照复印件";
                    break;
                case AttachmentType.居间公司委托书:
                    result = "居间公司委托书";
                    break;
                case AttachmentType.租赁合同复印件:
                    result = "租赁合同复印件";
                    break;
                case AttachmentType.权利凭证其他转租约协议:
                    result = "权利凭证其他转租约协议";
                    break;
                case AttachmentType.佣金确认书:
                    result = "佣金确认书";
                    break;
                case AttachmentType.法人身份证:
                    result = "法人身份证";
                    break;
                case AttachmentType.税务登记证:
                    result = "税务登记证";
                    break;
                case AttachmentType.组织机构代码证:
                    result = "组织机构代码证";
                    break;
                case AttachmentType.公司营业执照:
                    result = "公司营业执照";
                    break;
                case AttachmentType.劳动合同:
                    result = "劳动合同";
                    break;
                case AttachmentType.在职证明:
                    result = "在职证明";
                    break;
                case AttachmentType.境外人士就业证:
                    result = "境外人士就业证";
                    break;
                case AttachmentType.台胞证:
                    result = "台胞证";
                    break;
                case AttachmentType.回乡证:
                    result = "回乡证";
                    break;
                case AttachmentType.港澳身份证:
                    result = "港澳身份证";
                    break;
                case AttachmentType.户籍藤本:
                    result = "户籍藤本";
                    break;
                case AttachmentType.社保:
                    result = "社保";
                    break;
                case AttachmentType.个保:
                    result = "个保";
                    break;
                case AttachmentType.出款票据:
                    result = "出款票据";
                    break;
                case AttachmentType.离婚协议书:
                    result = "离婚协议书";
                    break;
                case AttachmentType.预约还贷申请:
                    result = "预约还贷申请";
                    break;
                case AttachmentType.收入证明:
                    result = "收入证明";
                    break;
                case AttachmentType.转账凭条:
                    result = "转账凭条";
                    break;
                case AttachmentType.销账附件:
                    result = "销账附件";
                    break;
                case AttachmentType.居间合同:
                    result = "居间合同";
                    break;
                case AttachmentType.银行卡:
                    result = "银行卡";
                    break;
            }
            return result;
            
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
