﻿using PinganHouse.SHHTS.Enumerations;

using System;
using System.Windows.Data;

namespace PinganHouse.SHHTS.UI.WPFNew.Common.Converters
{
    public class CartificateTypeToStringConverter:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string result = string.Empty;
            CertificateType type = (CertificateType)value;
            switch(type)
            {
                case CertificateType.BirthCertificate:
                    result = "出生证";
                    break;
                case CertificateType.IdCard:
                    result = "身份证";
                    break;
                case CertificateType.CertificateOfOfficers:
                    result = "军官证";
                    break;
                case CertificateType.HKMacaoIdCard:
                    result = "港澳居民身份证";
                    break;
                case CertificateType.MacaoPermit:
                    result = "港澳通行证";
                    break;
                case CertificateType.Passport:
                    result = "护照";
                    break;
                case CertificateType.ResidenceBooklet:
                    result = "户口簿";
                    break;
                default:
                    break;
            }
            
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
