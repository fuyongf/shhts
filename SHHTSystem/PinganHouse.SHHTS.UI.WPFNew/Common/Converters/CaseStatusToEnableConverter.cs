﻿using PinganHouse.SHHTS.Enumerations;

using System;
using System.Windows.Data;

namespace PinganHouse.SHHTS.UI.WPFNew.Common
{
    public class CaseStatusToEnableConverter:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool result = true;
            var status = value is CaseStatus;
            if (status)
            {
                switch ((CaseStatus)value)
                {
                    //签约完成，不可以编辑，其他状态都可以修改
                    case CaseStatus.HA2:
                        result = false;
                        break;
                    default:
                        break;
                }
            }
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
