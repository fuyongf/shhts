﻿using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Globalization;

namespace PinganHouse.SHHTS.UI.WPFNew.Common
{
    public class NuclearTradeTypeToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int data = (int)value;
            string name = parameter.ToString();
            switch (name)
            {
                case "1":
                    return data == 1;
                case "2":
                    return data == 2;
                case "3":
                    return data == 3;
                default:
                    return false;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (TradeType)int.Parse(parameter.ToString());
        }
    }
    public class NuclearSelfCreditResonToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int data = (int)value;
            string name = parameter.ToString();
            switch (name)
            {
                case "1":
                    return data == 1;
                case "2":
                    return data == 2;
                case "3":
                    return data == 3;
                default:
                    return false;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (SelfCreditReson)int.Parse(parameter.ToString());
        }
    }

    public class CheckBoxConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            String val = (String)value;
            if (val == "合计")
            {
                return "Hidden";
            }


            return "Visible";
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return "";
        }
    }

    public class FundFlowColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Decimal val = (Decimal)value;
            if (val >= 0)
            {
                return "Green";
            }


            return "red";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {

            return "";
        }
    }

    public class ContentConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Decimal val = (Decimal)value;
            if (val == 0)
            {
                return "--";
            }


            return val.ToString("N");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return "";
        }
    }

    public class PaymentChannelConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            FundPlanPayChannel val = (FundPlanPayChannel)value;
            if (val == FundPlanPayChannel.Trusteeship)
            {
                return "平安托管";
            }
            else if (val == FundPlanPayChannel.Loan)
            {
                return "银行贷款";
            }
            else if (val == FundPlanPayChannel.SelfPay)
            {
                return "直接支付";
            }
            else if (val == FundPlanPayChannel.Unknown)
            {
                return "";
            }

            return "";
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return "";
        }

    }

}
