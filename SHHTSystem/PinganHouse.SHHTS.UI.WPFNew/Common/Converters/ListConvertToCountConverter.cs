﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;

namespace PinganHouse.SHHTS.UI.WPFNew.Common
{
    class ListConvertToCountConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var list = (IList<string>)value;
            if(list !=null && list.Count>0)
            {
                return list.Count.ToString();
            }
            else
            {
                return string.Empty; ;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
