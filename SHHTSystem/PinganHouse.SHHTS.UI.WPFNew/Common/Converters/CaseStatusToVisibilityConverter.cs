﻿using PinganHouse.SHHTS.Enumerations;
using System;
using System.Windows;
using System.Windows.Data;

namespace PinganHouse.SHHTS.UI.WPFNew.Common
{
    /// <summary>
    /// 预检页面控制是否显示"取消签约"按钮
    /// </summary>
    public class CaseStatusToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility result = Visibility.Collapsed;
            var status = value is CaseStatus;
            if (status)
            {
                switch ((CaseStatus)value)
                {
                    case CaseStatus.YJ1:
                        result = Visibility.Visible;
                        break;
                    case CaseStatus.HA1:
                        result = Visibility.Visible;
                        break;
                    default:
                        break;
                }
            }
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    /// <summary>
    ///预检页面控制是否显示"添加附件"按钮
    /// </summary>
    public class PreCheckAttachedCaseStatusToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility result = Visibility.Visible;
            var status = value is CaseStatus;
            if (status)
            {
                switch ((CaseStatus)value)
                {
                    case CaseStatus.ZB2:
                        result = Visibility.Collapsed;
                        break;
                    case CaseStatus.ZB5:
                        result = Visibility.Collapsed;
                        break;
                    default:
                        break;
                }
            }
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
