﻿using PinganHouse.SHHTS.Enumerations;
using System;
using System.Windows.Data;

namespace PinganHouse.SHHTS.UI.WPFNew.Common
{
    public class CaseStatusToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string result = string.Empty;
            var status = value is CaseStatus;
            if(status)
            {
                switch((CaseStatus)value)
                {
                    case CaseStatus.YJ1:
                        result = "等待签约";
                        break;
                    case CaseStatus.HA1:
                        result = "签约中";
                        break;
                    case CaseStatus.HA2:
                        result = "已签约";
                        break;
                    case CaseStatus.ZB2:
                        result = "签约已取消";
                        break;
                    case CaseStatus.ZB5:
                        result = "签约取消";
                        break;
                    default:
                        break;

                }
            }
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
