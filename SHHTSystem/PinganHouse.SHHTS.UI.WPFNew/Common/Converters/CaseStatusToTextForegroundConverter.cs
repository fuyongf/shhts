﻿using PinganHouse.SHHTS.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace PinganHouse.SHHTS.UI.WPFNew.Common
{
    /// <summary>
    /// 预检页面字体颜色
    /// </summary>
    public class CaseStatusToTextForegroundConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            SolidColorBrush result = new SolidColorBrush(Colors.Black);
            var status = value is CaseStatus;
            if (status)
            {
                switch ((CaseStatus)value)
                {
                    case CaseStatus.YJ1:
                        result = new SolidColorBrush(Colors.Red);
                        break;
                    case CaseStatus.HA1:
                        result = new SolidColorBrush(Colors.Green);
                        break;
                    case CaseStatus.ZB2:
                        result = new SolidColorBrush(Colors.Gray);
                        break;
                    case CaseStatus.ZB5:
                        result = new SolidColorBrush(Colors.Gray);
                        break;
                    default:
                        break;
                }
            }
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
