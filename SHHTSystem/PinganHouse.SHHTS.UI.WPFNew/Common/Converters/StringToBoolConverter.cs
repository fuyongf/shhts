﻿using System;
using System.Windows.Data;

namespace PinganHouse.SHHTS.UI.WPFNew.Common
{
    public class StringToBoolConverter:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool result = false;
            var tem = (string)value;
            if (!string.IsNullOrEmpty(tem))
                result = true;
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
