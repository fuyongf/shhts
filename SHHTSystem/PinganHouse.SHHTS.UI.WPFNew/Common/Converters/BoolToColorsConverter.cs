﻿using System;
using System.Windows.Data;
using System.Windows.Media;

namespace PinganHouse.SHHTS.UI.WPFNew.Common
{
    /// <summary>
    /// 转换程蓝色
    /// </summary>
    public class BoolToColorsConverter:IValueConverter
    {
        private readonly SolidColorBrush Default_Color = new SolidColorBrush(Color.FromArgb(255, 141, 173, 205));
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var color = new SolidColorBrush(Color.FromArgb(255, 173, 173, 173));
            if((bool)value)
            {
                color= Default_Color;
            }
            return color;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    /// <summary>
    /// 转换成黑色
    /// </summary>
    public class BoolToBlackColorConverter:IValueConverter
    {
        private readonly SolidColorBrush Default_Color_Black = new SolidColorBrush(Colors.Black);
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var color = new SolidColorBrush(Color.FromArgb(255, 173, 173, 173));
            if ((bool)value)
            {
                color = Default_Color_Black;
            }
            return color;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
