﻿using PinganHouse.SHHTS.UI.WPFNew.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinganHouse.SHHTS.UI.WPFNew.Common
{
    public class CaseInfo : BaseViewModel
    {
        #region 基本信息

        /// <summary>
        /// 案件编号
        /// </summary>
        private string caseId;
        public string CaseId
        {
            get
            {
                return caseId;
            }
            set
            {
                if (caseId != value)
                {
                    caseId = value;
                    NotifyPropertyChanged("CaseId");
                }
            }
        }

        /// <summary>
        /// 产权证号
        /// </summary>
        private string tenementContract;
        public string TenementContract
        {
            get
            {
                return tenementContract;
            }
            set
            {
                if (tenementContract != value)
                {
                    tenementContract = value;
                    NotifyPropertyChanged("TenementContract");
                }
            }
        }

        /// <summary>
        /// 物业地址
        /// </summary>
        private string address;
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                if (address != value)
                {
                    address = value;
                    NotifyPropertyChanged("Address");
                }
            }
        }

        /// <summary>
        /// 卖方姓名
        /// </summary>
        private string sellerName;
        public string SellerName
        {
            get
            {
                return sellerName;
            }
            set
            {
                if (sellerName != value)
                {
                    sellerName = value;
                    NotifyPropertyChanged("SellerName");
                }
            }
        }

        /// <summary>
        /// 买方姓名
        /// </summary>
        private string buyerName;
        public string BuyerName
        {
            get
            {
                return buyerName;
            }
            set
            {
                if (buyerName != value)
                {
                    buyerName = value;
                    NotifyPropertyChanged("BuyerName");
                }
            }
        }


        /// <summary>
        /// 中介公司
        /// </summary>
        private string agentCompany;
        public string AgentCompany
        {
            get
            {
                return agentCompany;
            }
            set
            {
                if (agentCompany != value)
                {
                    agentCompany = value;
                    NotifyPropertyChanged("AgentCompany");
                }
            }
        }

        /// <summary>
        /// 经纪人
        /// </summary>
        private string agentPerson;
        public string AgentPerson
        {
            get
            {
                return agentPerson;
            }
            set
            {
                if (agentPerson != value)
                {
                    agentPerson = value;
                    NotifyPropertyChanged("AgentPerson");
                }
            }
        }

        private long sysNo;
        public long SysNo 
        {
            get 
            {
                return sysNo;
            }
            set 
            {
                sysNo = value;
                NotifyPropertyChanged("SysNo");
            }
        }

        private long companySysNo;
        public long CompanySysNo
        {
            get
            {
                return companySysNo;
            }
            set
            {
                companySysNo = value;
                NotifyPropertyChanged("CompanySysNo");
            }
        }

        #endregion
    }
}
