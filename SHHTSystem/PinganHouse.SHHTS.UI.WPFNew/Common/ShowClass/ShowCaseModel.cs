﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPFNew.Controls;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;

namespace PinganHouse.SHHTS.UI.WPFNew.Common
{
    public class ShowCaseModel: BaseViewModel
    {
        /// <summary>
        /// 案件来源
        /// </summary>
        private List<EnumHelper> caseSourceType;
        public List<EnumHelper> CaseSourceType
        {
            get
            {
                return caseSourceType;
            }
            set
            {
                if (caseSourceType != value)
                {
                    caseSourceType = value;
                    NotifyPropertyChanged("CaseSourceType");
                }
            }
        }
        /// <summary>
        /// 案件来源选择
        /// </summary>
        private int selectCaseType;
        public int SelectCaseType
        {
            get { return selectCaseType; }
            set
            {
                if (selectCaseType != value)
                {
                    selectCaseType = value;
                    NotifyPropertyChanged("SelectCaseType");
                }
            }
        }


        private string tbTenementContractWord;

        public string TbTenementContractWord
        {
            get { return tbTenementContractWord; }
            set
            {
                if (tbTenementContractWord != value)
                {
                    tbTenementContractWord = value;
                    NotifyPropertyChanged("TbTenementContractWord");
                }
            }
        }

        private string tbTenementContract;

        public string TbTenementContract
        {
            get { return tbTenementContract; }
            set
            {
                if (tbTenementContract != value)
                {
                    tbTenementContract = value;
                    NotifyPropertyChanged("TbTenementContract");
                }
            }
        }

        private string tbTenementAddress;

        public string TbTenementAddress
        {
            get { return tbTenementAddress; }
            set
            {
                if (tbTenementAddress != value)
                {
                    tbTenementAddress = value;
                    NotifyPropertyChanged("TbTenementAddress");
                }
            }
        }

        private string tblShowInfo;

        public string TblShowInfo
        {
            get { return tblShowInfo; }
            set
            {
                if (tblShowInfo != value)
                {
                    tblShowInfo = value;
                    NotifyPropertyChanged("TblShowInfo");
                }
            }
        }
        /// <summary>
        /// 案件编号
        /// </summary>
        private string caseId;
        public string CaseId
        {
            get
            {
                return caseId;
            }
            set
            {
                if (caseId != value)
                {
                    caseId = value;
                    NotifyPropertyChanged("CaseId");
                }
            }
        }
        /// <summary>
        /// 案件状态
        /// </summary>
        private string signingState;
        public string SigningState
        {
            get
            {
                return signingState;
            }
            set
            {
                if (signingState != value)
                {
                    signingState = value;
                    NotifyPropertyChanged("SigningState");
                }
            }
        }
        /// <summary>
        /// 案件来源
        /// </summary>
        private string caseSource;
        public string CaseSource
        {
            get
            {
                return caseSource;
            }
            set
            {
                if (caseSource != value)
                {
                    caseSource = value;
                    NotifyPropertyChanged("CaseSource");
                }
            }
        }
        /// <summary>
        /// 立案日期
        /// </summary>
        private string createDate;
        public string CreateDate
        {
            get
            {
                return createDate;
            }
            set
            {
                if (createDate != value)
                {
                    createDate = value;
                    NotifyPropertyChanged("CreateDate");
                }
            }
        }
        /// <summary>
        /// 接单日期
        /// </summary>
        private string checkCaseDate;
        public string CheckCaseDate
        {
            get
            {
                return checkCaseDate;
            }
            set
            {
                if (checkCaseDate != value)
                {
                    checkCaseDate = value;
                    NotifyPropertyChanged("CheckCaseDate");
                }
            }
        }
        /// <summary>
        /// 最后修改时间
        /// </summary>
        private string modifyDate;
        public string ModifyDate
        {
            get
            {
                return modifyDate;
            }
            set
            {
                if (modifyDate != value)
                {
                    modifyDate = value;
                    NotifyPropertyChanged("ModifyDate");
                }
            }
        }
        /// <summary>
        /// 签约完成时间
        /// </summary>
        private string signingDate;
        public string SigningDate
        {
            get
            {
                return signingDate;
            }
            set
            {
                if (signingDate != value)
                {
                    signingDate = value;
                    NotifyPropertyChanged("SigningDate");
                }
            }
        }
        /// <summary>
        /// 受理人
        /// </summary>
        private string operateName;
        public string OperateName
        {
            get
            {
                return operateName;
            }
            set
            {
                if (operateName != value)
                {
                    operateName = value;
                    NotifyPropertyChanged("OperateName");
                }
            }
        }
        /// <summary>
        /// 创建人
        /// </summary>
        private string createName;
        public string CreateName
        {
            get
            {
                return createName;
            }
            set
            {
                if (createName != value)
                {
                    createName = value;
                    NotifyPropertyChanged("CreateName");
                }
            }
        }
        /// <summary>
        /// 修改人
        /// </summary>
        private string modifyName;
        public string ModifyName
        {
            get
            {
                return modifyName;
            }
            set
            {
                if (modifyName != value)
                {
                    modifyName = value;
                    NotifyPropertyChanged("ModifyName");
                }
            }
        }
        /// <summary>
        /// 附件
        /// </summary>
        private string isExist;
        public string IsExist
        {
            get
            {
                return isExist;
            }
            set
            {
                if (isExist != value)
                {
                    isExist = value;
                    NotifyPropertyChanged("IsExist");
                }
            }
        }
        /// <summary>
        /// 预检—核案接单时间
        /// </summary>
        private string firstTime;
        public string FirstTime
        {
            get
            {
                return firstTime;
            }
            set
            {
                if (firstTime != value)
                {
                    firstTime = value;
                    NotifyPropertyChanged("FirstTime");
                }
            }
        }
        /// <summary>
        /// 核案接单-完成签约时间
        /// </summary>
        private string secondTime;
        public string SecondTime
        {
            get
            {
                return secondTime;
            }
            set
            {
                if (secondTime != value)
                {
                    secondTime = value;
                    NotifyPropertyChanged("SecondTime");
                }
            }
        }
        /// <summary>
        /// 预检—核案接单
        /// </summary>
        private bool firstReady;
        public bool FirstReady
        {
            get
            {
                return firstReady;
            }
            set
            {
                if (firstReady != value)
                {
                    firstReady = value;
                    NotifyPropertyChanged("FirstReady");
                }
            }
        }
        /// <summary>
        /// 核案接单-完成签约
        /// </summary>
        private bool secondReady;
        public bool SecondReady
        {
            get
            {
                return secondReady;
            }
            set
            {
                if (secondReady != value)
                {
                    secondReady = value;
                    NotifyPropertyChanged("SecondReady");
                }
            }
        }
        private bool isBuyerForbid = true;

        public bool IsBuyerForbid
        {
            get { return isBuyerForbid; }
            set
            {
                if (isBuyerForbid != value)
                {
                    isBuyerForbid = value;
                    NotifyPropertyChanged("IsBuyerForbid");
                }
            }
        }
        private bool isSellerForbid = true;

        public bool IsSellerForbid
        {
            get { return isSellerForbid; }
            set
            {
                if (isSellerForbid != value)
                {
                    isSellerForbid = value;
                    NotifyPropertyChanged("IsSellerForbid");
                }
            }
        }
        /// <summary>
        /// 类型详情Model
        /// </summary>
        private IdInfoModel idCardBuyerModel;

        public IdInfoModel IdCardBuyerModel
        {
            get { return idCardBuyerModel; }
            set
            {
                if (idCardBuyerModel != value)
                {
                    idCardBuyerModel = value;
                    NotifyPropertyChanged("IdCardBuyerModel");
                }
            }
        }

        private IdInfoModel idCardSellerModel;

        public IdInfoModel IdCardSellerModel
        {
            get { return idCardSellerModel; }
            set
            {
                if (idCardSellerModel != value)
                {
                    idCardSellerModel = value;
                    NotifyPropertyChanged("IdCardSellerModel");
                }
            }
        }
        private ObservableCollection<UserIdentityModel> listSellerModel;

        public ObservableCollection<UserIdentityModel> ListSellerModel
        {
            get { return listSellerModel; }
            set
            {
                if (listSellerModel != value)
                {
                    listSellerModel = value;
                    NotifyPropertyChanged("ListSellerModel");
                }
            }
        }
        
        private UserIdentityModel selectSellerUser;

        public UserIdentityModel SelectSellerUser
        {
            get { return selectSellerUser; }
            set
            {
                if (selectSellerUser != value)
                {
                    //model转换
                    selectSellerUser = value;
                    if (selectSellerUser == null)
                        return;
                    IndexSellerSelect = (int)SelectSellerUser.CertificateType;
                    if (SelectSellerUser.Tag != null)
                        IsSellerForbid = false;
                    IdCardSellerModel = new IdInfoModel
                    {
                        IdName = selectSellerUser.Name,
                        IdAddress = selectSellerUser.Address,
                        IdAgent = selectSellerUser.VisaAgency
                    };
                    if (SelectSellerUser.Photo != null)
                    {
                        IdCardSellerModel.Bitmap = ByteToBmp(SelectSellerUser.Photo);
                    }
                    if (selectSellerUser.Birthday == null)
                    {
                        IdCardSellerModel.IdBirth = null;
                    }
                    else
                    {
                        var birth = (DateTime)selectSellerUser.Birthday;
                        IdCardSellerModel.IdBirth = birth.ToShortDateString();
                    }

                    IdCardSellerModel.IdNation = EnumHelper.GetEnumDesc(selectSellerUser.Nation);
                    if (IdCardSellerModel.IdNation == "未知")
                        IdCardSellerModel.IdNation = null;
                    IdCardSellerModel.IdNum = selectSellerUser.IdentityNo;
                    switch (selectSellerUser.Gender)
                    {
                        case Gender.Female:
                            IdCardSellerModel.IdSex = "女";
                            IdCardSellerModel.Female = true;
                            break;
                        case Gender.Male:
                            IdCardSellerModel.IdSex = "男";
                            IdCardSellerModel.Male = true;
                            break;
                        default:
                            IdCardSellerModel.IdSex = "";
                            IdCardSellerModel.Male = true;
                            break;
                    }
                    if (selectSellerUser.EffectiveDate == null)
                    {
                        idCardSellerModel.EffectiveDate = null;
                    }
                    else
                    {
                        var effectiveDatetime = (DateTime)selectSellerUser.EffectiveDate;
                        idCardSellerModel.EffectiveDate = effectiveDatetime.ToShortDateString();
                    }

                    if (selectSellerUser.EffectiveDate == null && selectSellerUser.ExpiryDate == null)
                    {
                        IdCardSellerModel.ExpiryDate = null;
                    }
                    else if (selectSellerUser.EffectiveDate != null && selectSellerUser.ExpiryDate == null)
                    {
                        IdCardSellerModel.ExpiryDate = "长期";
                    }
                    else
                    {
                        if (selectSellerUser.ExpiryDate != null)
                        {
                            var expiryDateTime = (DateTime)selectSellerUser.ExpiryDate;
                            IdCardSellerModel.ExpiryDate = expiryDateTime.ToShortDateString();
                        }
                        //IdCardSellerModel.ExpiryDate = (selectSellerUser.ExpiryDate).
                    }


                    if (SelectSellerUser.IdentityNo != null)
                        IdCardSellerModel.IsCancel = true;
                    IdCardSellerModel.Nationality = selectSellerUser.Nationality;
                    IdCardSellerModel.IsTrusted = selectSellerUser.IsTrusted;
                    IdCardSellerModel.CertificateType = SelectSellerUser.CertificateType;
                    IdCardSellerModel.Tag = SelectSellerUser.Tag ?? null;
                    if (SelectSellerUser.IdentityNo != null)
                    {
                        IdCardSellerModel.IsCancel = true;
                    }

                    NotifyPropertyChanged("SelectSellerUser");
                }
            }
        }
        private BitmapImage ByteToBmp(byte[] bytes)
        {
            BitmapImage bitmap = new BitmapImage();
            try
            {
                MemoryStream ms1 = new MemoryStream(bytes);
                bitmap.BeginInit();
                bitmap.StreamSource = ms1;
                bitmap.EndInit();

            }
            catch (Exception e)
            {
                MessageBox.Show("图像转化失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return bitmap;
        }
        private ObservableCollection<UserIdentityModel> listBuyerModel;

        public ObservableCollection<UserIdentityModel> ListBuyerModel
        {
            get { return listBuyerModel; }
            set
            {
                if (listBuyerModel != value)
                {
                    listBuyerModel = value;
                    NotifyPropertyChanged("ListBuyerModel");
                }

            }
        }

        private UserIdentityModel selectBuyerUser;

        public UserIdentityModel SelectBuyerUser
        {
            get { return selectBuyerUser; }
            set
            {
                if (selectBuyerUser != value)
                {
                    //model转换
                    selectBuyerUser = value;
                    if (selectBuyerUser == null)
                        return;
                    IndexBuyerSelect = (int)SelectBuyerUser.CertificateType;
                    //if (SelectBuyerUser.Tag != null)
                    //    IsBuyerForbid = false;
                    IdCardBuyerModel = new IdInfoModel
                    {
                        IdName = selectBuyerUser.Name,
                        IdAddress = selectBuyerUser.Address,
                        IdAgent = selectBuyerUser.VisaAgency
                    };
                     if (SelectBuyerUser.Photo!=null)
                    {
                        IdCardBuyerModel.Bitmap = ByteToBmp(SelectBuyerUser.Photo);
                    }
                    if (selectBuyerUser.Birthday == null)
                    {
                        IdCardBuyerModel.IdBirth = null;
                    }
                    else
                    {
                        var birth = (DateTime)selectBuyerUser.Birthday;
                        IdCardBuyerModel.IdBirth = birth.ToShortDateString();
                    }

                    IdCardBuyerModel.IdNation = EnumHelper.GetEnumDesc(selectBuyerUser.Nation);
                    if (IdCardBuyerModel.IdNation == "未知")
                        IdCardBuyerModel.IdNation = null;
                    IdCardBuyerModel.IdNum = selectBuyerUser.IdentityNo;
                    switch (selectBuyerUser.Gender)
                    {
                        case Gender.Female:
                            IdCardBuyerModel.IdSex = "女";
                            IdCardBuyerModel.Female = true;
                            break;
                        case Gender.Male:
                            IdCardBuyerModel.IdSex = "男";
                            IdCardBuyerModel.Male = true;
                            break;
                        default:
                            IdCardBuyerModel.IdSex = "";
                            IdCardBuyerModel.Male = true;
                            break;
                    }
                    if (selectBuyerUser.EffectiveDate == null)
                    {
                        idCardBuyerModel.EffectiveDate = null;
                    }
                    else
                    {
                        var effectiveDatetime = (DateTime)selectBuyerUser.EffectiveDate;
                        idCardBuyerModel.EffectiveDate = effectiveDatetime.ToShortDateString();
                    }

                    if (selectBuyerUser.EffectiveDate == null && selectBuyerUser.ExpiryDate == null)
                    {
                        IdCardBuyerModel.ExpiryDate = null;
                    }
                    else if (selectBuyerUser.EffectiveDate != null && selectBuyerUser.ExpiryDate == null)
                    {
                        IdCardBuyerModel.ExpiryDate = "长期";
                    }
                    else
                    {
                        if (selectBuyerUser.ExpiryDate != null)
                        {
                            var expiryDateTime = (DateTime)selectBuyerUser.ExpiryDate;
                            IdCardBuyerModel.ExpiryDate = expiryDateTime.ToShortDateString();
                        }
                        //IdCardBuyerModel.ExpiryDate = (selectBuyerUser.ExpiryDate).
                    }


                    if (SelectBuyerUser.IdentityNo != null)
                        IdCardBuyerModel.IsCancel = true;
                    IdCardBuyerModel.Nationality = selectBuyerUser.Nationality;
                    IdCardBuyerModel.IsTrusted = selectBuyerUser.IsTrusted;
                    IdCardBuyerModel.CertificateType = SelectBuyerUser.CertificateType;
                    IdCardBuyerModel.Tag = SelectBuyerUser.Tag ?? null;
                    if (SelectBuyerUser.IdentityNo != null)
                    {
                        IdCardBuyerModel.IsCancel = true;             
                    }
                   
                    NotifyPropertyChanged("SelectBuyerUser");
                }
            }
        }

        private int sellerTotal;
        public int SellerTotal
        {
            get { return sellerTotal; }
            set
            {
                if (sellerTotal != value)
                {
                    sellerTotal = value;
                    NotifyPropertyChanged("SellerTotal");
                }
            }
        }

        private int buyerTotal;
        public int BuyerTotal
        {
            get { return buyerTotal; }
            set
            {
                if (buyerTotal != value)
                {
                    buyerTotal = value;
                    NotifyPropertyChanged("BuyerTotal");
                }
            }
        }
        /// <summary>
        /// 修改或者查看状态控制
        /// </summary>
        private bool isUpdate=true;
        public bool IsUpdate
        {
            get { return isUpdate; }
            set
            {
                if (isUpdate != value)
                {
                    isUpdate = value;
                    NotifyPropertyChanged("IsUpdate");
                }
            }
        }

        /// <summary>
        /// 证件类型
        /// </summary>
        private List<EnumHelper> identitySelleryList;

        public List<EnumHelper> IdentitySellerList
        {
            get { return identitySelleryList; }
            set
            {
                if (identitySelleryList != value)
                {
                    identitySelleryList = value;
                    NotifyPropertyChanged("IdentitySellerList");
                }
            }
        }
        /// <summary>
        ///选择证件类型
        /// </summary>
        private EnumHelper identitySellerSelected;

        public EnumHelper IdentitySellerSelected
        {
            get { return identitySellerSelected; }
            set
            {
                if (identitySellerSelected != value)
                {
                    identitySellerSelected = value;
                    NotifyPropertyChanged("IdentitySellerSelected");
                }
            }
        }
        /// <summary>
        ///选择证件类型Index
        /// </summary>
        private int indexSellerSelect;

        public int IndexSellerSelect
        {
            get { return indexSellerSelect; }
            set
            {
                if (indexSellerSelect != value)
                {
                    indexSellerSelect = value;
                    NotifyPropertyChanged("IndexSellerSelect");
                }
            }
        }
        /// <summary>
        /// 证件类型
        /// </summary>
        private List<EnumHelper> identityBuyerList;

        public List<EnumHelper> IdentityBuyerList
        {
            get { return identityBuyerList; }
            set
            {
                if (identityBuyerList != value)
                {
                    identityBuyerList = value;
                    NotifyPropertyChanged("IdentityBuyerList");
                }
            }
        }
        /// <summary>
        ///选择证件类型
        /// </summary>
        private EnumHelper identityBuyerSelected;

        public EnumHelper IdentityBuyerSelected
        {
            get { return identityBuyerSelected; }
            set
            {
                if (identityBuyerSelected != value)
                {
                    identityBuyerSelected = value;
                    NotifyPropertyChanged("IdentityBuyerSelected");
                }
            }
        }
        /// <summary>
        ///选择证件类型Index
        /// </summary>
        private int indexBuyerSelect;

        public int IndexBuyerSelect
        {
            get { return indexBuyerSelect; }
            set
            {
                if (indexBuyerSelect != value)
                {
                    indexBuyerSelect = value;
                    NotifyPropertyChanged("IndexBuyerSelect");
                }
            }
        }

        /// <summary>
        ///卖家姓名集合
        /// </summary>
        private Dictionary<long, string> buyerNameDic;

        public Dictionary<long, string> BuyerNameDic
        {
            get { return buyerNameDic; }
            set
            {
                if (buyerNameDic != value)
                {
                    buyerNameDic = value;
                    NotifyPropertyChanged("BuyerNameDic");
                }
            }
        }
        /// <summary>
        ///买家姓名集合
        /// </summary>
        private Dictionary<long, string> sellerNameDic;

        public Dictionary<long, string> SellerNameDic
        {
            get { return sellerNameDic; }
            set
            {
                if (sellerNameDic != value)
                {
                    sellerNameDic = value;
                    NotifyPropertyChanged("SellerNameDic");
                }
            }
        }
    }
}
