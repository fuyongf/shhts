﻿using System;

namespace PinganHouse.SHHTS.UI.WPFNew.Common
{
    public static class Code128String
    {
        /// <summary>
        /// Code 128校验位计算
        /// </summary>
        /// <param name="inputStr">输入字符串</param>
        /// <returns>Code128条形码字符串</returns>
        public static string Get(string inputStr)
        {
            int checksum = 104;
            for (int ii = 0; ii < inputStr.Length; ii++)
            {
                if (inputStr[ii] >= 32)
                {
                    checksum += (inputStr[ii] - 32) * (ii + 1);
                }
                else
                {
                    checksum += (inputStr[ii] + 64) * (ii + 1);
                }
            }
            checksum = checksum % 103;
            if (checksum < 95)
            {
                checksum += 32;
            }
            else
            {
                checksum += 100;
            }

            return Convert.ToChar(204) +
                   inputStr +
                   Convert.ToChar(checksum) +
                   Convert.ToChar(206);
        }
    }
}
