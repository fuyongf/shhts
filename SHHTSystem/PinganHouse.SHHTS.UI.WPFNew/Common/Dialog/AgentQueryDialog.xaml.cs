﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy;

namespace PinganHouse.SHHTS.UI.WPFNew.Common.Dialog
{
    /// <summary>
    /// AgentQueryDialog.xaml 的交互逻辑
    /// </summary>
    public partial class AgentQueryDialog : Window
    { 
        private AgentStaff _agentStaff;

        public AgentStaff AgentStaff
        {
            get { return _agentStaff; }
            set { _agentStaff = value; }
        }
   
        public AgentQueryDialog()
        {
            InitializeComponent();
            AgentStaff = new AgentStaff();
        }

        public double ScreenHeigh
        {
            get { return (double)GetValue(ScreenHeighProperty); }
            set { SetValue(ScreenHeighProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScreenHeigh.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScreenHeighProperty =
            DependencyProperty.Register("ScreenHeigh", typeof(double), typeof(AgentQueryDialog), new PropertyMetadata(SystemParameters.PrimaryScreenHeight));



        public double ScreenWidth
        {
            get { return (double)GetValue(ScreenWidthProperty); }
            set { SetValue(ScreenWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScreenWidth.  This enables animation, styling, binding, etc...SystemParameters.WorkArea.Width
        public static readonly DependencyProperty ScreenWidthProperty =
            DependencyProperty.Register("ScreenWidth", typeof(double), typeof(AgentQueryDialog), new PropertyMetadata(SystemParameters.PrimaryScreenWidth));


        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSelect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                IList<AgentStaffQueryInfo> agentStaffList = new List<AgentStaffQueryInfo>();
                int totalCount = 0;
                //查询条件
                var condition = string.IsNullOrEmpty(TbCondition.Text.Trim()) ? null : TbCondition.Text.Trim();

                agentStaffList = AgentServiceProxy.GetAgentStaffsWithCompany(1, 10000, out totalCount, null, null, condition);

                if (agentStaffList != null && agentStaffList.Count > 0)
                {
                    DgAgentList.DataContext = agentStaffList;
                }
                else
                {
                    DgAgentList.DataContext = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("加载数据发生错误：" + ex.ToString());
            }
        }

        /// <summary>
        /// 确认
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnConfirm_Click(object sender, RoutedEventArgs e)
        {
            if (DgAgentList.SelectedItems.Count > 0)
            {
                _agentStaff.AgentCompanySysNo = ((AgentStaff)DgAgentList.SelectedValue).AgentCompanySysNo;
                _agentStaff.SysNo = ((AgentStaff)DgAgentList.SelectedValue).SysNo;
                _agentStaff.RealName = ((AgentStaff)DgAgentList.SelectedValue).RealName;

                Close();
            }
            else
            {
                MessageBox.Show("请选择一行数据");
                return;
            }
        }

        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// 双击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DgAgentList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (DgAgentList.SelectedItems.Count > 0)
            {
                _agentStaff.AgentCompanySysNo = ((AgentStaff)DgAgentList.SelectedValue).AgentCompanySysNo;
                _agentStaff.SysNo = ((AgentStaff)DgAgentList.SelectedValue).SysNo;
                _agentStaff.RealName = ((AgentStaff)DgAgentList.SelectedValue).RealName;
                Close();
            }
            else
            {
                //MessageBox.Show("请选择一行数据");
                return;
            }
        }    
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }      
    }
    
}
