﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PinganHouse.SHHTS.UI.WPFNew.Common.CommonClass;

namespace PinganHouse.SHHTS.UI.WPFNew.Common.Dialog
{
    /// <summary>
    /// PrintSetting.xaml 的交互逻辑
    /// </summary>
    public partial class PrintSetting : Window
    {
        /// <summary>
        /// 打印机列表
        /// </summary>
        private List<PrintModel> _ListPrint = new List<PrintModel>();
        public static int Area = 0;
 
        public PrintSetting()
        {
            InitializeComponent();
        }

        #region 注册属性
        public double ScreenHeigh
        {
            get { return (double)GetValue(ScreenHeighProperty); }
            set { SetValue(ScreenHeighProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScreenHeigh.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScreenHeighProperty =
            DependencyProperty.Register("ScreenHeigh", typeof(double), typeof(PrintSetting), new PropertyMetadata(SystemParameters.PrimaryScreenHeight));



        public double ScreenWidth
        {
            get { return (double)GetValue(ScreenWidthProperty); }
            set { SetValue(ScreenWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScreenWidth.  This enables animation, styling, binding, etc...SystemParameters.WorkArea.Width
        public static readonly DependencyProperty ScreenWidthProperty =
            DependencyProperty.Register("ScreenWidth", typeof(double), typeof(PrintSetting), new PropertyMetadata(SystemParameters.PrimaryScreenWidth));

        #endregion

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {          
            #region 获得本机所有打印机
            var printers = PrinterSettings.InstalledPrinters;//获取本机上的所有打印机         
            PrintModel pm = new PrintModel();
            pm.PrintNo = 0;
            pm.PrintName = "未分配";
            _ListPrint.Add(pm);

            for (int i = 0; i < printers.Count; i++)
            {
                PrintModel print = new PrintModel();
                print.PrintNo = i + 1;
                print.PrintName = printers[i].ToString();
                _ListPrint.Add(print);
            }
            #endregion

            //收据打印
            CboReceiptPrint.ItemsSource = _ListPrint;
            CboReceiptPrint.DisplayMemberPath = "PrintName";
            CboReceiptPrint.SelectedValuePath = "PrintNo";      
            #region 收据打印机信息
            //从注册表加载 收据打印机信息          
            var rememberReceiptPrint = PrivateProfileUtils.GetContentValue("ReceiptPrint", "PrintName");
            if (!string.IsNullOrWhiteSpace(rememberReceiptPrint))
            {
                for (int j = 0; j < _ListPrint.Count; j++)
                {
                    if (rememberReceiptPrint == _ListPrint[j].PrintName)
                    {
                        CboReceiptPrint.SelectedValue = _ListPrint[j].PrintNo;
                        break;
                    }
                    else //本机无该打印机名称，则 收据打印机未分配
                    {
                        CboReceiptPrint.SelectedValue = 0;
                    }
                }
            }
            else //为空，则 收据打印机未分配
            {
                CboReceiptPrint.SelectedValue = 0;
            }
            #endregion

            //条形码打印
            CboBarCodePrint.ItemsSource = _ListPrint;
            CboBarCodePrint.DisplayMemberPath = "PrintName";
            CboBarCodePrint.SelectedValuePath = "PrintNo";
            //CboBarCodePrint.SelectedValue = 0;

            #region 条形码打印机信息
            //从注册表加载 条形码打印机信息          
            var rememberBarCodePrint = PrivateProfileUtils.GetContentValue("BarCodePrint", "PrintName");
            if (!string.IsNullOrWhiteSpace(rememberBarCodePrint))
            {
                for (int j = 0; j < _ListPrint.Count; j++)
                {
                    if (rememberBarCodePrint == _ListPrint[j].PrintName)
                    {
                        CboBarCodePrint.SelectedValue = _ListPrint[j].PrintNo;
                        break;
                    }
                    else //本机无该打印机名称，则 条形码打印机未分配
                    {
                        CboBarCodePrint.SelectedValue = 0;
                    }
                }
            }
            else //为空，则 条形码打印机未分配
            {
                CboBarCodePrint.SelectedValue = 0;
            }
            #endregion

            //E房钱打印
            CboContractPrint.ItemsSource = _ListPrint;
            CboContractPrint.DisplayMemberPath = "PrintName";
            CboContractPrint.SelectedValuePath = "PrintNo";

            #region E房钱打印机信息
            //从注册表加载 E房钱打印机信息          
            var rememberContractPrint = PrivateProfileUtils.GetContentValue("ContractPrint", "PrintName");
            if (!string.IsNullOrWhiteSpace(rememberContractPrint))
            {
                for (int j = 0; j < _ListPrint.Count; j++)
                {
                    if (rememberContractPrint == _ListPrint[j].PrintName)
                    {
                        CboContractPrint.SelectedValue = _ListPrint[j].PrintNo;
                        break;
                    }
                    else //本机无该打印机名称，则 E房钱打印机未分配
                    {
                        CboContractPrint.SelectedValue = 0;
                    }
                }
            }
            else //为空，则 E房钱打印机未分配
            {
                CboContractPrint.SelectedValue = 0;
            }
            #endregion

        }

        /// <summary>
        /// 确定事件
        /// </summary>       
        private void ConfirmBt_Click(object sender, RoutedEventArgs e)
        {         
            //收据打印机
            var receiptPrintName = CboReceiptPrint.Text.Trim();
            //int printSelect = int.Parse(CboReceiptPrint.SelectedValue.ToString());
            //SetAreaConfigFile(value);
            PrivateProfileUtils.Write("ReceiptPrint", "PrintName", receiptPrintName);

            //条形码打印机
            var barCodePrintName = CboBarCodePrint.Text.Trim();
            PrivateProfileUtils.Write("BarCodePrint", "PrintName", barCodePrintName);

            //生成条形码数
            var barCodeNum = TbBarCodeNum.Text.Trim();
            PrivateProfileUtils.Write("BarCodeNum", "Num", barCodeNum);

            // E房钱打印机
            var contractPrintName = CboContractPrint.Text.Trim();
            PrivateProfileUtils.Write("ContractPrint", "PrintName", contractPrintName);
           var text= TbBarCodeNum.Text.Trim();
            if (text != string.Empty)
            {        
               var num = Convert.ToInt32(text);
                if (num > 20)
                {
                    MessageBox.Show("条形码数量不能超过20个", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

            }
            this.Close();
        }
  
        /// <summary>
        /// 只输入数字
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TbBarCodeNum_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9]+");
            e.Handled = re.IsMatch(e.Text);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnRetset_Click(object sender, RoutedEventArgs e)
        {
            CboReceiptPrint.SelectedIndex = 0;
            CboBarCodePrint.SelectedIndex = 0;
            TbBarCodeNum.Text = string.Empty;
            CboContractPrint.SelectedIndex = 0;
        }

        private void TbBarCodeNum_LostFocus(object sender, RoutedEventArgs e)
        {
            var textBox= (e.OriginalSource) as TextBox;
            if (textBox != null)
            {
                var num = Convert.ToInt32(textBox.Text);
                if (num > 20)
                {
                    MessageBox.Show("条形码数量不能超过20个", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                  
            }
        }

    }
}
