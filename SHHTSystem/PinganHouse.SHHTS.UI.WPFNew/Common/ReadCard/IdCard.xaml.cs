﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;

namespace PinganHouse.SHHTS.UI.WPFNew.Common
{
    /// <summary>
    /// IdCard.xaml 的交互逻辑
    /// </summary>
    public partial class IdCard : Window
    {
        #region 设置无框窗体样式变量

        public const long WS_CAPTION = 0x00C00000L;
        public const long WS_CAPTION_2 = 0X00C0000L;
        public const int GWL_STYLE = -16;
        public const int GWL_EXSTYLE = -20;
        public const long WS_CHILD = 0x40000000L;

        [System.Runtime.InteropServices.DllImport("User32.dll")]
        public static extern long GetWindowLong(IntPtr handle, int style);

        [System.Runtime.InteropServices.DllImport("User32.dll")]
        public static extern void SetWindowLong(IntPtr handle, int oldStyle, long newStyle);

        #endregion

        private StringBuilder _name;
        private StringBuilder _gender;
        private StringBuilder _folk;
        private StringBuilder _birthDay;
        private StringBuilder _code;
        private StringBuilder _address;
        private StringBuilder _agency;
        private StringBuilder _expireStart;
        private StringBuilder _expireEnd;
        private UserIdentityModel _identityInfo = new UserIdentityModel();
        private byte[] _photo;
        private bool _isCancel;
        private readonly bool _deviceNotExists;
        private int _type;


        public double ScreenHeigh
        {
            get { return (double)GetValue(ScreenHeighProperty); }
            set { SetValue(ScreenHeighProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScreenHeigh.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScreenHeighProperty =
            DependencyProperty.Register("ScreenHeigh", typeof(double), typeof(IdCard), new PropertyMetadata(SystemParameters.PrimaryScreenHeight));



        public double ScreenWidth
        {
            get { return (double)GetValue(ScreenWidthProperty); }
            set { SetValue(ScreenWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScreenWidth.  This enables animation, styling, binding, etc...SystemParameters.WorkArea.Width
        public static readonly DependencyProperty ScreenWidthProperty =
            DependencyProperty.Register("ScreenWidth", typeof(double), typeof(IdCard), new PropertyMetadata(SystemParameters.PrimaryScreenWidth));


        public IdCard(int type, out bool flag)
        {
            flag = false;
            InitializeComponent();
            string errorMessage;
            DpEf.DisplayDateEnd = DateTime.Now;
            if (!IdCardHelper.OpenDevice(out errorMessage))
            {
                _deviceNotExists = true;
                if (type == 3)
                {
                    IdCardHelper.CloseDevice();
                    MessageBox.Show("没有连接读卡设备", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
            }
            else
            {
                if (type == 3)
                {
                    InputBtn.Visibility = Visibility.Collapsed;                    
                }
            }
            _isCancel = false;
         
            switch (type)
            {
                case 1:
                    TbTip.Text = "请将身份证放到身份证读卡器上,请刷买方身份证...";
                    break;
                case 2:
                    TbTip.Text = "请将身份证放到身份证读卡器上,请刷卖方身份证...";
                    break;
                case 3:
                    TbTip.Text = "请将身份证放到身份证读卡器上,请刷经纪人身份证...";
                    break;
            }
            CbNation.ItemsSource = EnumHelper.InitNationToCombobox();
            CbNation.DisplayMemberPath = "DisplayMember";
            CbNation.SelectedValuePath = "ValueMember";
            flag = true;
        }
        public UserIdentityModel IdentityInfo
        {
            get { return _identityInfo; }
            set { _identityInfo = value; }
        }


        #region 设置无框窗体
        // private void Window_Loaded(object sender, RoutedEventArgs e)
        //{
        //    WindowInteropHelper wndHelper = new WindowInteropHelper(this);
        //    IntPtr wpfHwnd = wndHelper.Handle;
        //    SetWindowNoBorder(wpfHwnd);
        //}

        ///// <summary>
        ///// 设置窗体为无边框风格
        ///// </summary>
        ///// <param name="hWnd"></param>
        //public static void SetWindowNoBorder(IntPtr hWnd)
        //{
        //    long oldstyle = GetWindowLong(hWnd, GWL_STYLE);
        //    SetWindowLong(hWnd, GWL_STYLE, oldstyle & (~(WS_CAPTION | WS_CAPTION_2)));
        //}
        #endregion


        /// <summary>
        /// 获取设备存在性
        /// </summary>
        public bool DeviceNotExists
        {
            get { return _deviceNotExists; }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (_deviceNotExists)
            {
                IsShowInput();
                ScanBtn.IsEnabled = false;
            }
            else
            {
                IsShowId();
            }
            var thread = new Thread(Read);
            thread.IsBackground = true;
            thread.Start();
        }

        private void Read()
        {
            string errorMessage;

            while (!IdCardHelper.FindCard(out errorMessage))
            {
                if (_isCancel)
                    break;
                Thread.Sleep(50);
            }

            if (_isCancel)
            {
                return;
            }

            bool result = IdCardHelper.Read(
                    out _name,
                    out _gender,
                    out _folk,
                    out _birthDay,
                    out _code,
                    out _address,
                    out _agency,
                    out _expireStart,
                    out _expireEnd,
                    out _photo,
                    out errorMessage);

            if (!result)
            {
                if (MessageBox.Show(errorMessage, "错误", MessageBoxButton.OK, MessageBoxImage.Error) == MessageBoxResult.OK)
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {

                        Close();

                    });
                }
                return;
            }

            _identityInfo.Name = _name.ToString();

            _identityInfo.Birthday = DateTime.Parse(
                _birthDay.ToString().Substring(0, 4)
                + "-" + _birthDay.ToString().Substring(4, 2)
                + "-" + _birthDay.ToString().Substring(6, 2));

            _identityInfo.Address = _address.ToString();
            _identityInfo.IdentityNo = _code.ToString();

            _identityInfo.EffectiveDate = DateTime.Parse(
                _expireStart.ToString().Substring(0, 4)
                + "-" + _expireStart.ToString().Substring(4, 2)
                + "-" + _expireStart.ToString().Substring(6, 2));

            if (_expireEnd.ToString().Trim() != "长期")
            {
                _identityInfo.ExpiryDate = DateTime.Parse(_expireEnd.ToString().Substring(0, 4)
                                                            + "-" + _expireEnd.ToString().Substring(4, 2)
                                                            + "-" + _expireEnd.ToString().Substring(6, 2));
            }
            else
            {
                _identityInfo.ExpiryDate = null;
            }


            _identityInfo.Gender = _gender.ToString() == "男" ? Gender.Male : Gender.Female;
            _identityInfo.Nation = (Nation)Enum.Parse(typeof(Nation), _folk + "族");
            _identityInfo.Nationality = "中国";
            _identityInfo.Photo = _photo;
            _identityInfo.CertificateType = (CertificateType)Enum.Parse(typeof(CertificateType), "0");
            _identityInfo.VisaAgency = _agency.ToString();
            _identityInfo.IsTrusted = true;
            //删除本地的身份证图片
            string photoPath = string.Format("{0}" + "\\photo.bmp", AppDomain.CurrentDomain.BaseDirectory);
            if (File.Exists(photoPath))
                File.Delete(photoPath);

            Application.Current.Dispatcher.Invoke(() =>
            {

                Close();

            });

        }

        private void CommitBtn_Click(object sender, RoutedEventArgs e)
        {
            bool isSuccess = false;
            string userName = TbName.Text.Trim();
            string idnum = TbNum.Text.Trim();
            if (string.IsNullOrWhiteSpace(userName))
            {
                MessageBox.Show("姓名不能为空", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (!IsName(userName))
            {
                MessageBox.Show("姓名格式错误", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrWhiteSpace(idnum))
            {
                MessageBox.Show("身份证号码不能为空", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (!IsNum(idnum))
            {
                MessageBox.Show("身份证号码格式错误", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (!CompareTime())
                return;

            SelectSex(idnum);
            if (RbMale.IsChecked == true)
                _identityInfo.Gender = Gender.Male;
            if (RbFeMale.IsChecked == true)
                _identityInfo.Gender = Gender.Female;
            _identityInfo.Birthday = DpBirth.SelectedDate;
            if (!string.IsNullOrWhiteSpace(CbNation.Text))
                _identityInfo.Nation = (Nation)Nation.Parse(typeof(Nation), CbNation.Text);
            if (!string.IsNullOrWhiteSpace(TbAddress.Text))
                _identityInfo.Address = TbAddress.Text.Trim();
            if (!string.IsNullOrWhiteSpace(TbAgent.Text))
                _identityInfo.VisaAgency = TbAgent.Text.Trim();      
            _identityInfo.EffectiveDate = DpEf.SelectedDate;
            _identityInfo.ExpiryDate = CbLong.IsChecked == true ? null : DpEx.SelectedDate;
            _identityInfo.Name = userName;
            _identityInfo.IdentityNo = idnum;
            _identityInfo.IsTrusted = false;
            Close();
        }

        private bool IsNum(string str)
        {
            return new Regex(@"^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}(\d|x|X)$").IsMatch(str);
        }
        private bool IsName(string str)
        {
            return new Regex(@"^[a-zA-Z\s\u4e00-\u9fa5]+$").IsMatch(str);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _isCancel = true;
            IdCardHelper.CloseDevice();
            Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (!_deviceNotExists)
                IdCardHelper.CloseDevice();
        }

        #region 引导按钮的显示隐藏
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            IsShowId();
        }

        private void IsShowInput()
        {
            GridInput.Visibility = Visibility.Visible;
            GridId.Visibility = Visibility.Collapsed;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            IsShowInput();
        }
        private void IsShowId()
        {
            if (!_deviceNotExists)
            {
                ScanBtn.IsEnabled = true;
                GridInput.Visibility = Visibility.Collapsed;
                GridId.Visibility = Visibility.Visible;
            }
            else
            {
                ScanBtn.IsEnabled = false;
            }

        }
        #endregion

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            _isCancel = true;
            _identityInfo = null;
            if (!_deviceNotExists)
                IdCardHelper.CloseDevice();
            Close();
        }

        private void ScanBtn_Click(object sender, RoutedEventArgs e)
        {
            IsShowId();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            _isCancel = true;
            _identityInfo = null;
            if (!_deviceNotExists)
                IdCardHelper.CloseDevice();
            Close();
        }

        private void TbNum_LostFocus(object sender, RoutedEventArgs e)
        {
            var idnum = TbNum.Text.Trim();
            if (string.IsNullOrWhiteSpace(idnum))
                return;
            if (!IsNum(idnum))
            {
                MessageBox.Show("身份证号码格式错误", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (idnum.Length == 18)
                SelectSex(idnum);
        }

        private void SelectSex(string str)
        {
            int result = 0;
            if (!string.IsNullOrWhiteSpace(str))
            {
                result = int.Parse(string.Format("{0}{1}{2}", str[14], str[15], str[16]));
                if (result % 2 == 0)
                {
                    RbFeMale.IsChecked = true;
                }
                else
                {
                    RbMale.IsChecked = true;
                }
            }
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            var preTime = DpEf.SelectedDate;
            if (preTime == null)
            {
                CbLong.IsChecked = false;
                MessageBox.Show("请选择有效时间的开始时间", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            DpEx.IsEnabled = false;
            DpEx.Text = string.Empty;
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
           
            DpEx.IsEnabled = true;
        }

        private void DpEx_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            var preTime = DpEf.SelectedDate;
            var backTime = DpEx.SelectedDate;
            if (preTime != null)
            {
                DateTime time1 = (DateTime)preTime;
                if (backTime != null)
                {
                    var time2 = (DateTime)backTime;
                    var result = DateTime.Compare(time2, time1);
                    if (result < 0 || result == 0)
                    {
                        MessageBox.Show("有效的结束时间不能小于或等于开始时间", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);           
                    }
                }
            }
        }
        private bool CompareTime()
        {
            var preTime = DpEf.SelectedDate;
            var backTime = DpEx.SelectedDate;
            if (preTime == null && backTime == null)
                return true;
            if (preTime == null)
            {               
                MessageBox.Show("请选择有效时间的开始时间", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }       
            DateTime time1 = (DateTime) preTime;
            if (backTime != null)
            {
                DateTime time2 = (DateTime)backTime;
                var result = DateTime.Compare(time2, time1);
                if (result < 0 || result == 0)
                {
                    MessageBox.Show("有效的结束时间不能小于或等于开始时间", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false ;
                }
            }
            return true;
        }

        private void TbName_LostFocus(object sender, RoutedEventArgs e)
        {
            var idname = TbName.Text.Trim();
            if (string.IsNullOrWhiteSpace(idname))
                return;
            if (!IsName(idname))
            {
                MessageBox.Show("名字格式错误", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }
    }

}
