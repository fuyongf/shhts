﻿
namespace PinganHouse.SHHTS.UI.WPFNew.Common
{
    public static class RegionNames
    {
        public const string LeftRegion = "LeftRegion";
        public const string MainRegion = "MainRegion";
        public const string TabRegion = "TabRegion";
        public const string TitleRegion = "TitleRegion";
        public const string LoadingRegion = "LoadingRegion";
    }
}
