﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Practices.ServiceLocation;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.DataTransferObjects.Filters;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using PinganHouse.SHHTS.UI.WPFNew.Controls;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;
using PinganHouse.SHHTS.UI.WPFNew.Views;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels.NuclearModel;
using PinganHouse.SHHTS.UI.WPFNew.Views.Nuclear;
using PinganHouse.SHHTS.UI.WPFNew.Controls.Nuclear;
using MessageBox = System.Windows.MessageBox;

namespace PinganHouse.SHHTS.UI.WPFNew.Controllers
{
    public class NuclearController
    {
        private readonly int PAGE_SIZE = 10;
        private readonly IUnityContainer container;
        private readonly IRegionManager regionManager;
        private readonly IEventAggregator eventAggregator;
        private SeniorSearchDialog _seniorSearchDialog = null;
        private CheckCaseSearchPageViewModel _searchListPageViewModel = null;
        private FundFlowDialog _fundFlowDialog = null;
        private IList<CaseDto> _caseList = null;
        private ConfirmContractDialog _confirmContractDialog = null;

        private List<CaseStatus> CASE_STATUS = new List<CaseStatus>()
        {
            CaseStatus.YJ1, CaseStatus.HA1, CaseStatus.HA2, CaseStatus.HA3
            , CaseStatus.HA4, CaseStatus.HA5, CaseStatus.HA6, CaseStatus.ZB2, CaseStatus.ZB5, CaseStatus.ZB6
            , CaseStatus.ZB7, CaseStatus.ZB13
        };


        public NuclearController(IUnityContainer container, IRegionManager regionManager, IEventAggregator eventAggregator)
        {
            if (container == null) throw new ArgumentNullException("container");
            if (regionManager == null) throw new ArgumentNullException("regionManager");
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");


            this.container = container;
            this.regionManager = regionManager;
            this.eventAggregator = eventAggregator;

            this.eventAggregator.GetEvent<NuclearEvents>().Subscribe(this.OnLoadCaseListEvents, true);
            this.eventAggregator.GetEvent<ApprovalReportEvents>().Subscribe(this.OnApprovalReportEvents, true);
            this.eventAggregator.GetEvent<NuclearLoadBasicInfoEvents>().Subscribe(this.LoadNuclearBasicInfoView, true);
            this.eventAggregator.GetEvent<NuclearSubBasicInfoEvents>().Subscribe(this.SubmitNuclearBasicInfoView, true);
            this.eventAggregator.GetEvent<NuclearLoadTransactionInfoEvent>().Subscribe(this.OnLoadCaseTransactionInfoEvents, true);
            this.eventAggregator.GetEvent<StartContractEvents>().Subscribe(this.OnStartContractEvents, true);
            this.eventAggregator.GetEvent<CancelContractEvents>().Subscribe(this.OnCancelContractEvents, true);
            this.eventAggregator.GetEvent<ConfirmContractEvents>().Subscribe(this.OnConfirmContractEvents, true);
            this.eventAggregator.GetEvent<ConfirmContractEnterEvents>().Subscribe(this.OnConfirmContractEnterEvents, true);
            this.eventAggregator.GetEvent<NuclearPageChangesEvents>().Subscribe(this.OnNuclearPageChangesEvents, true);
            this.eventAggregator.GetEvent<AddFundCommandEvents>().Subscribe(this.OnAddFundCommandEvents, true);
            this.eventAggregator.GetEvent<FundFlowDialogSaveCommandEvents>().Subscribe(this.OnFundFlowDialogSaveCommandEvents, true);
            this.eventAggregator.GetEvent<FundFlowDialogEnterCommandEvents>().Subscribe(this.OnFundFlowDialogEnterCommandEvents, true);
            this.eventAggregator.GetEvent<DeleteFundFlowCommandEvents>().Subscribe(this.OnDeleteFundFlowCommandEvents, true);
            this.eventAggregator.GetEvent<LoadNuclearCaseDetailInfoCommandEvents>().Subscribe(this.OnLoadCaseDetailInfoCommandEvents, true);

        }


        private async void OnNuclearPageChangesEvents(int index)
        {
            IRegion loadingRegion = this.regionManager.Regions[RegionNames.LoadingRegion];
            if (loadingRegion == null) return;
            LoadingControl loadingView = loadingRegion.GetView(ViewNamesKey.LOADING_CONTROL_VIEW) as LoadingControl;
            if (loadingView == null)
            {
                //Create a new instance of the EmployeeDetailsView using the Unity container.
                loadingView = this.container.Resolve<LoadingControl>();
                //Add the view to the main region. This automatically activates the view too.
                loadingRegion.Add(loadingView, ViewNamesKey.LOADING_CONTROL_VIEW);
                loadingRegion.Activate(loadingView);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                loadingRegion.Activate(loadingView);
            }
            await Task.Run(() =>
            {
                //获取案件列表数据
                int cnt = 0;
                int pageCount = 0;
                _searchListPageViewModel.CaseList = GetCaseList(index, _searchListPageViewModel.SearchCaseCondition, CASE_STATUS, out cnt);
                if (cnt % PAGE_SIZE != 0)
                {
                    pageCount = cnt / PAGE_SIZE + 1;
                }
                else
                {
                    pageCount = cnt / PAGE_SIZE;
                }
                _searchListPageViewModel.TotalPageCount = pageCount;
            });

            loadingRegion.Deactivate(loadingView);
            RefreshSearchListPage(_searchListPageViewModel);
        }

        private async void OnLoadCaseListEvents(CheckCaseSearchPageViewModel viewModel)
        {
            IRegion loadingRegion = this.regionManager.Regions[RegionNames.LoadingRegion];
            if (loadingRegion == null) return;
            LoadingControl loadingView = loadingRegion.GetView(ViewNamesKey.LOADING_CONTROL_VIEW) as LoadingControl;
            if (loadingView == null)
            {
                loadingView = this.container.Resolve<LoadingControl>();
                //Add the view to the main region. This automatically activates the view too.
                loadingRegion.Add(loadingView, ViewNamesKey.LOADING_CONTROL_VIEW);
                loadingRegion.Activate(loadingView);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                loadingRegion.Activate(loadingView);
            }

            //把覆盖在最上层的window给关掉
            if (_seniorSearchDialog != null &&
                _seniorSearchDialog.IsActive)
            {
                _seniorSearchDialog.Close();
            }
            if (_confirmContractDialog != null &&
                _confirmContractDialog.IsActive)
            {
                _confirmContractDialog.Close();
            }

            await Task.Run(() =>
            {

                //获取案件列表数据
                int cnt = 0;
                int pageCount = 0;
                var list = EnumHelper.InitCaseStatusToCombobox(CASE_STATUS);

                //第一次进来，添加状态，搜索条件信息，否则就只更新list列表信息，其他信息用缓存的内容
                CheckCaseSearchPageViewModel searchListPageViewModel = Microsoft.Practices.ServiceLocation.ServiceLocator.Current.GetInstance<CheckCaseSearchPageViewModel>();
                if (_searchListPageViewModel == null)
                {
                    var conditionList = new List<string>()
                        {
                            "案件编号","物业地址","主办人","买方姓名","经纪人"
                        };
                    if (viewModel != null)
                    {
                        searchListPageViewModel.SearchCaseCondition = viewModel.SearchCaseCondition;
                    }
                    searchListPageViewModel.CaseList = GetCaseList(1, searchListPageViewModel.SearchCaseCondition, CASE_STATUS, out cnt);
                    foreach (var item in list)
                    {
                        searchListPageViewModel.StatusList.Add(item);
                    }
                    searchListPageViewModel.ConditionList = conditionList;
                    _searchListPageViewModel = searchListPageViewModel;
                }
                else
                {
                    if (viewModel != null)
                    {
                        _searchListPageViewModel.SearchCaseCondition = viewModel.SearchCaseCondition;
                    }
                    _searchListPageViewModel.CaseList = GetCaseList(1, _searchListPageViewModel.SearchCaseCondition, CASE_STATUS, out cnt);
                }
                if (cnt % PAGE_SIZE != 0)
                {
                    pageCount = cnt / PAGE_SIZE + 1;
                }
                else
                {
                    pageCount = cnt / PAGE_SIZE;
                }
                _searchListPageViewModel.TotalPageCount = pageCount;
            });

            RefreshSearchListPage(_searchListPageViewModel);
            loadingRegion.Deactivate(loadingView);
        }

        private async void OnApprovalReportEvents(string caseId)
        {
            IRegion loadingRegion = this.regionManager.Regions[RegionNames.LoadingRegion];
            if (loadingRegion == null) return;
            LoadingControl loadingView = loadingRegion.GetView(ViewNamesKey.LOADING_CONTROL_VIEW) as LoadingControl;
            if (loadingView == null)
            {
                loadingView = this.container.Resolve<LoadingControl>();
                //Add the view to the main region. This automatically activates the view too.
                loadingRegion.Add(loadingView, ViewNamesKey.LOADING_CONTROL_VIEW);
                loadingRegion.Activate(loadingView);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                loadingRegion.Activate(loadingView);
            }

            ApprovalReportViewModel vm = ServiceLocator.Current.GetInstance<ApprovalReportViewModel>();
            vm.CaseId = caseId;

            IRegion mainRegion = this.regionManager.Regions[RegionNames.MainRegion];
            if (mainRegion == null) return;
            ApprovalReportView view = mainRegion.GetView(ViewNamesKey.NUCLEAR_APPROVAL_REPORT_VIEW) as ApprovalReportView;
            if (view == null)
            {
                view = this.container.Resolve<ApprovalReportView>();
                //Add the view to the main region. This automatically activates the view too.
                mainRegion.Add(view, ViewNamesKey.NUCLEAR_APPROVAL_REPORT_VIEW);
                mainRegion.Activate(view);
                view.ViewModel = vm;
            }
            else
            {
                //The view has already been added to the region so just activate it.
                mainRegion.Activate(view);
                view.ViewModel = vm;
            }

            loadingRegion.Deactivate(loadingView);

        }

        private void RefreshSearchListPage(CheckCaseSearchPageViewModel viewModel)
        {
            IRegion mainRegion = this.regionManager.Regions[RegionNames.MainRegion];
            if (mainRegion == null) return;
            CheckCaseSearchPageView view = mainRegion.GetView(ViewNamesKey.NUCLEAR_CHECK_CASE_SEARCH_PAGE_VIEW) as CheckCaseSearchPageView;
            if (view == null)
            {
                view = this.container.Resolve<CheckCaseSearchPageView>();
                //Add the view to the main region. This automatically activates the view too.
                mainRegion.Add(view, ViewNamesKey.NUCLEAR_CHECK_CASE_SEARCH_PAGE_VIEW);
                mainRegion.Activate(view);
                view.ViewModel = viewModel;
            }
            else
            {
                //The view has already been added to the region so just activate it.
                mainRegion.Activate(view);
                view.ViewModel = viewModel;
            }
        }

        private ObservableCollection<CaseDetailInfoViewModel> GetCaseList(int page, CaseDetailInfoViewModel model, List<CaseStatus> status, out int cnt)
        {
            IList<CaseDto> caseList = null;
            if (model == null)
            {
                CaseFilter filter = new CaseFilter();
                filter.Statuses = status;
                filter.ReceptionCenters = new List<ReceptionCenter>() { ConfigHelper.GetCurrentReceptionCenter() };
                caseList = CaseProxyService.GetInstanse().GetPaginatedList(filter, page, PAGE_SIZE, out cnt);

            }
            else
            {
                CaseFilter filter = new CaseFilter();

                filter.ReceptionCenters = new List<ReceptionCenter>() { ConfigHelper.GetCurrentReceptionCenter() };
                if (model.CaseId != null) filter.CaseIds = new List<string>() { model.CaseId };
                if (model.Address != null) filter.TenementAddress = model.Address;
                if (model.BuyerName != null) filter.BuyerName = model.BuyerName;
                if (model.AgentPerson != null) filter.AgencyStaffName = model.AgentPerson;
                if (model.StartTime != null) filter.StartDate = model.StartTime;
                if (model.EndTime != null) filter.EndDate = model.EndTime;
                if (model.Status != null)
                {
                    var enumList = new List<CaseStatus>();
                    var array = System.Enum.GetValues(typeof(CaseStatus));    // 获取枚举的所有值
                    foreach (var arr in array)
                    {
                        if (((CaseStatus)model.Status).HasFlag((CaseStatus)arr))
                        {
                            if ((CaseStatus)arr != CaseStatus.Unkown) enumList.Add((CaseStatus)arr);
                        }
                    }
                    filter.Statuses = enumList;
                }
                else
                {
                    filter.Statuses = status;
                }

                caseList = CaseProxyService.GetInstanse().GetPaginatedList(filter, page, PAGE_SIZE, out cnt);
            }
            _caseList = caseList;
            ObservableCollection<CaseDetailInfoViewModel> caseViewModelList = new ObservableCollection<CaseDetailInfoViewModel>();
            if (caseList != null && caseList.Count > 0)
            {
                foreach (CaseDto Dto in caseList)
                {
                    CaseDetailInfoViewModel caseDetailInfoViewModel = ServiceLocator.Current.GetInstance<CaseDetailInfoViewModel>();
                    caseDetailInfoViewModel.CaseId = Dto.CaseId;
                    caseDetailInfoViewModel.AgentCompany = Dto.CompanyName;
                    caseDetailInfoViewModel.AgentPerson = Dto.AgencyName;
                    caseDetailInfoViewModel.BuyerName = Dto.BuyerName;
                    caseDetailInfoViewModel.Address = Dto.TenementAddress;
                    caseDetailInfoViewModel.CreateTime = Dto.CreateDate;
                    caseDetailInfoViewModel.SignedTime = Dto.CreateDate;
                    caseDetailInfoViewModel.SponsorName = Dto.CreatorName;
                    caseDetailInfoViewModel.AcceptorName = Dto.OperatorName;
                    caseDetailInfoViewModel.Status = Dto.CaseStatus;
                    caseDetailInfoViewModel.NextAction = Dto.NextActions;
                    caseDetailInfoViewModel.NextActionWithCaseId = new Tuple<IList<string>, string>(Dto.NextActions, Dto.CaseId);
                    caseViewModelList.Add(caseDetailInfoViewModel);
                }
            }
            return caseViewModelList;
        }

        #region Nuclear--BasicInfo
        /// <summary>
        /// 加载Nuclear-BasicInfoView中的数据
        /// </summary>
        /// <param name="caseId"></param>
        private async void LoadNuclearBasicInfoView(string caseId)
        {            
            #region loading效果
            IRegion loadingRegion = this.regionManager.Regions[RegionNames.LoadingRegion];
            if (loadingRegion == null) return;
            LoadingControl loadingView = loadingRegion.GetView(ViewNamesKey.LOADING_CONTROL_VIEW) as LoadingControl;
            if (loadingView == null)
            {
                //Create a new instance of the EmployeeDetailsView using the Unity container.
                loadingView = this.container.Resolve<LoadingControl>();
                //Add the view to the main region. This automatically activates the view too.
                loadingRegion.Add(loadingView, ViewNamesKey.LOADING_CONTROL_VIEW);
                loadingRegion.Activate(loadingView);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                loadingRegion.Activate(loadingView);
            }
            #endregion

            //加载进viewmodel
            var vm = ServiceLocator.Current.GetInstance<BaseInfoViewModel>();
            await Task.Run(() =>
            {
                //绑定数据
                BindingDataToNuclearBasicInfoViewModel(caseId, vm);
            });
            #region 激活页面
            IRegion mainRegion = this.regionManager.Regions[RegionNames.MainRegion];
            if (mainRegion == null) return;
            BasicInfoView view = mainRegion.GetView(ViewNamesKey.NUCLEAR_BASICINFO_VIEW) as BasicInfoView;
            if (view == null)
            {

                //Create a new instance of the EmployeeDetailsView using the Unity container.
                view = this.container.Resolve<BasicInfoView>();
                //Add the view to the main region. This automatically activates the view too.
                mainRegion.Add(view, ViewNamesKey.NUCLEAR_BASICINFO_VIEW);
                mainRegion.Activate(view);
                view.ViewModel = vm;
                view.InitView(vm);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                mainRegion.Activate(view);
                view.ViewModel = vm;
                view.InitView(vm);
            }

            loadingRegion.Deactivate(loadingView);
            #endregion
        }

        private void BindingDataToNuclearBasicInfoViewModel(string caseId, BaseInfoViewModel vm)
        {

            CaseStatus m_Status = CaseStatus.HA2;
            //案件对象
            var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseIdAndCaseStatus(caseId, m_Status);
            //案件附属对象
            var caseRemarkDto = CaseProxyService.GetInstanse().GetRemarkBySysNo(caseDto.SysNo);
            //中介信息
            var agencyDto = AgentServiceProxy.GetAgentSatff(caseDto.AgencySysNo);
            //中介公司信息
            var agentCompany = AgentServiceProxy.GetAgentCompany(caseDto.CompanySysNo);

            //银行卡信息
            var bankAccounts = CaseProxyService.GetInstanse().GetCustomerBankAccounts(caseDto.SysNo);
            try
            {

                #region 基本信息
                vm.CaseId = caseDto.CaseId;
                var tenementContracts = caseDto.TenementContract.Split('-');
                if (tenementContracts.Length > 1)
                {
                    vm.TenementContracts1 = tenementContracts[0];
                    vm.TenementContracts2 = tenementContracts[1];
                }
                else
                { vm.TenementContracts1 = caseDto.TenementContract; }
                vm.TenementAddress = caseDto.TenementAddress;
                //资金托管
                vm.IsFund = caseDto.IsFund;
                vm.FundTrusteeshipContract = caseDto.FundTrusteeshipContract;
                //佣金托管
                vm.IsCommission = caseDto.IsCommission;
                vm.CommissionTrusteeshipContract = caseDto.CommissionTrusteeshipContract;
                caseRemarkDto = caseRemarkDto ?? new CaseRemark();

                //物业名称
                vm.TenementName = caseRemarkDto.TenementName;

                //环线位置
                vm.LoopLinePosition = !string.IsNullOrEmpty(caseRemarkDto.LoopLinePosition) ? caseRemarkDto.LoopLinePosition : "请选择";
                //建筑面积
                vm.CoveredArea = caseRemarkDto.CoveredArea;
                //房
                vm.RoomCount = caseRemarkDto.RoomCount;
                //厅
                vm.HallCount = caseRemarkDto.HallCount;
                //卫
                vm.ToiletCount = caseRemarkDto.ToiletCount;

                //层数
                vm.FloorCount = caseRemarkDto.FloorCount;
                //竣工时间
                if (caseRemarkDto.BuiltCompletedDate != null)
                {
                    vm.BuiltCompletedDate = Convert.ToDateTime(caseRemarkDto.BuiltCompletedDate).Year.ToString();
                }
                else
                {
                    vm.BuiltCompletedDate = string.Empty;
                }
                //居间价格
                vm.MediatorPrice = caseRemarkDto.MediatorPrice;
                //网签价格
                vm.NetlabelPrice = caseRemarkDto.NetlabelPrice;
                //真实成交价格
                vm.RealPrice = caseRemarkDto.RealPrice;
                //物业上手价格
                vm.TenementRawPrice = caseRemarkDto.TenementRawPrice;

                //居住类型
                if (caseRemarkDto.ResideType != ResideType.Unknow)
                {
                    var text = EnumHelper.GetEnumDesc(caseRemarkDto.ResideType);
                    var value = (int)caseRemarkDto.ResideType;
                    vm.ResideType = new EnumHelper() { DisplayMember = text, ValueMember = value };
                }
                else
                {
                    vm.ResideType = new EnumHelper { ValueMember = -999, DisplayMember = "请选择" };
                }

                //交易类型
                vm.TradeType = caseRemarkDto.TradeType;
                //是否首次购买
                vm.FirstTimeBuy = caseRemarkDto.FirstTimeBuy;
                //购入年限
                vm.PurchaseYears = caseRemarkDto.PurchaseYears;
                //唯一住房
                vm.OnlyHousing = caseRemarkDto.OnlyHousing;
                //满n年
                vm.BuyOverYears = caseRemarkDto.BuyOverYears;
                if (vm.BuyOverYears != null)
                {
                    vm.BuyOverYearsHelper = new EnumHelper
                    {
                        DisplayMember = EnumHelper.GetEnumDesc(vm.BuyOverYears),
                        ValueMember = (int)vm.BuyOverYears
                    };
                }
                else
                {
                    vm.BuyOverYearsHelper = new EnumHelper { ValueMember = -999, DisplayMember = "请选择" };
                }

                //车位地址？
                vm.CarportAddress = caseRemarkDto.CarportAddress;
                //车位面积？
                vm.CarportArea = caseRemarkDto.CarportArea;
                //车位价格？
                vm.CarportPrice = caseRemarkDto.CarportPrice;
                //车位上手价格
                vm.CarportRawPrice = caseRemarkDto.CarportRawPrice;
                //是否补偿地价
                vm.IsRepairLandPrice = caseRemarkDto.IsRepairLandPrice;
                //是否国安审批
                vm.IsNsaApproval = caseRemarkDto.IsNSAApproval;
                //是否历史保护建筑
                vm.IsProtectiveBuilding = caseRemarkDto.IsProtectiveBuilding;
                //公证员
                vm.NotarySysNo = caseRemarkDto.NotarySysNo;

                if (agencyDto != null)
                {
                    vm.AgentMobile = agencyDto.Mobile;
                    vm.AgentRealName = agencyDto.RealName;
                }
                if (agentCompany != null)
                {
                    vm.AgentCompanyName = agentCompany.CompanyName;
                }

                #endregion
                #region 银行卡信息绑定

                vm.BuyerMainAccount = null;
                vm.SellerMainAccount = null;
                if (bankAccounts == null) return;
                foreach (var bankAccount in bankAccounts)
                {
                    switch (bankAccount.CustomerType)
                    {
                        case CustomerType.Buyer:
                            vm.BuyerBankList.Add(new ViewModels.NuclearModel.Bank()
                            {
                                AccountName = bankAccount.AccountName,
                                AccountNo = bankAccount.AccountNo,
                                BankCode = bankAccount.BankCode,
                                BankName = bankAccount.BankName,
                                SubBankName = bankAccount.SubBankName
                            });
                            if (bankAccount.IsMainAccount)
                            {
                                vm.BuyerMainAccount = bankAccount;
                            }
                            break;
                        case CustomerType.Seller:
                            vm.SellerBankList.Add(new ViewModels.NuclearModel.Bank()
                            {
                                AccountName = bankAccount.AccountName,
                                AccountNo = bankAccount.AccountNo,
                                BankCode = bankAccount.BankCode,
                                BankName = bankAccount.BankName,
                                SubBankName = bankAccount.SubBankName
                            });
                            if (bankAccount.IsMainAccount)
                            {
                                vm.SellerMainAccount = bankAccount;
                            }
                            break;
                    }
                }

                #endregion

            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
            }
        }

        private void SubmitNuclearBasicInfoView(BaseInfoViewModel vm)
        {
            #region loading效果
            IRegion loadingRegion = this.regionManager.Regions[RegionNames.LoadingRegion];
            if (loadingRegion == null) return;
            LoadingControl loadingView = loadingRegion.GetView(ViewNamesKey.LOADING_CONTROL_VIEW) as LoadingControl;
            if (loadingView == null)
            {
                //Create a new instance of the EmployeeDetailsView using the Unity container.
                loadingView = this.container.Resolve<LoadingControl>();
                //Add the view to the main region. This automatically activates the view too.
                loadingRegion.Add(loadingView, ViewNamesKey.LOADING_CONTROL_VIEW);
                loadingRegion.Activate(loadingView);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                loadingRegion.Activate(loadingView);
            }
            #endregion

            #region 执行操作
            try
            {
                var caseStatus = CaseStatus.Unkown;
                var result = SaveCaseRemark(vm, out caseStatus);
                if (result.Success)
                {
                    //完成签约 和 立案不通过再次提交
                    if (caseStatus == CaseStatus.HA2 || caseStatus == CaseStatus.HA6)
                    {
                        //CaseProxyService.GetInstanse().UpdateCaseStatus(vm.CaseId, CaseStatus.HA3, LoginHelper.CurrentUser.SysNo);
                    }
                    OnLoadCaseCustomerInfoView(vm.CaseId);
                }
                else
                {
                    MessageBox.Show("保存数据失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("保存数据发生异常错误：" + ex.Message, "系统异常错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            #endregion

            #region 跳转

            #endregion

            loadingRegion.Deactivate(loadingView);
        }

        private OperationResult SaveCaseRemark(BaseInfoViewModel vm, out CaseStatus caseStatus)
        {
            caseStatus = CaseStatus.Unkown;
            //数据合法性验证
            var checkResult = CheckDataValidity(vm);
            if (!checkResult.Success) return checkResult;

            var m_Status = CaseStatus.HA2;
            //获取案件
            var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseIdAndCaseStatus(vm.CaseId, m_Status);
            if (caseDto == null) return new OperationResult(1, "未能获取正确的案件数据...");
            caseStatus = caseDto.CaseStatus;

            if (caseDto.CaseStatus == CaseStatus.HA5)
            {
                //跳转上下家
            }
            var crm = new CaseRemarkModel();
            //物业名称
            crm.TenementName = vm.TenementName;
            //环线位置
            crm.LoopLinePosition = vm.LoopLinePosition;
            //建筑面积
            if (vm.CoveredArea != null) crm.CoveredArea = (decimal)vm.CoveredArea;
            // 层数
            if (vm.FloorCount != null) crm.FloorCount = (int)vm.FloorCount;
            //房间数
            if (vm.RoomCount != null) crm.RoomCount = (int)vm.RoomCount;
            //厅数
            if (vm.HallCount != null) crm.HallCount = (int)vm.HallCount;
            //卫生间数
            if (vm.ToiletCount != null) crm.ToiletCount = (int)vm.ToiletCount;
            //竣工时间
            DateTime builtCompletedDate;
            crm.BuiltCompletedDate = DateTime.TryParse(vm.BuiltCompletedDate.Trim() + "-01-01", out builtCompletedDate) ?
                builtCompletedDate : new DateTime(1900, 01, 01);

            //居间价格
            if (vm.MediatorPrice != null) crm.MediatorPrice = (decimal)vm.MediatorPrice;
            //网签价格
            if (vm.NetlabelPrice != null) crm.NetlabelPrice = (decimal)vm.NetlabelPrice;
            //真实成交价格
            if (vm.RealPrice != null) crm.RealPrice = (decimal)vm.RealPrice;
            //物业价格
            if (vm.TenementRawPrice != null) crm.TenementRawPrice = (decimal)vm.TenementRawPrice;
            //房屋类型
            crm.ResideType = (ResideType)vm.ResideType.ValueMember;
            //交易类型
            crm.TradeType = vm.TradeType;
            //购入年限
            crm.PurchaseYears = vm.PurchaseYears;
            //满n年
            vm.BuyOverYears = (BuyOverYears)vm.BuyOverYearsHelper.ValueMember;
            if (vm.BuyOverYears != null) crm.BuyOverYears = (BuyOverYears)vm.BuyOverYears;

            crm.FirstTimeBuy = vm.FirstTimeBuy;
            crm.OnlyHousing = vm.OnlyHousing;
            crm.CarportAddress = vm.CarportAddress;
            crm.CarportArea = vm.CarportArea;
            crm.CarportPrice = vm.CarportPrice;
            crm.CarportRawPrice = vm.CarportRawPrice;

            crm.IsRepairLandPrice = vm.IsRepairLandPrice;
            crm.IsNSAApproval = vm.IsNsaApproval;
            crm.IsProtectiveBuilding = vm.IsProtectiveBuilding;
            crm.IsNotarize = vm.IsNotarize;
            crm.NotarySysNo = vm.NotarySysNo;
            crm.IsCommissionTrusteeship = vm.IsCommission;
            crm.IsFundTrusteeship = vm.IsFund;
            crm.FtContractNo = vm.FundTrusteeshipContract;
            crm.CtContractNo = vm.CommissionTrusteeshipContract;
            //var result = CaseProxyService.GetInstanse().PerfectCaseInfo(caseDto.SysNo, crm, LoginHelper.CurrentUser.SysNo);
            var result = new OperationResult(0);
            return result;
        }

        private OperationResult CheckDataValidity(BaseInfoViewModel vm)
        {
            var result = new OperationResult(0, null);

            try
            {
                #region 验证

                //验证环线位置
                var loopLine = vm.LoopLinePosition;
                if (string.IsNullOrEmpty(loopLine) || loopLine.Equals("请选择"))
                {
                    result = new OperationResult(1, "请选择环线位置.");
                    return result;
                }



                //验证竣工日期是否准确
                var completedTime = vm.BuiltCompletedDate;
                if (!string.IsNullOrEmpty(completedTime))
                {
                    DateTime time = DateTime.Now;
                    var timeStr = completedTime.Trim().Split(new char[] { '-' });
                    if (!DateTime.TryParse(timeStr[0] + "-01-01", out time))
                    {
                        result = new OperationResult(1, "请输入正确的竣工日期.");
                        return result;
                    }
                    else if (time < Convert.ToDateTime("1949-10-01"))
                    {
                        result = new OperationResult(1, "请输入正确的竣工日期.");
                        return result;
                    }
                }
                //网签价格
                if (vm.NetlabelPrice <= 0)
                {
                    result = new OperationResult(1, "网签价格必须大于0.");
                    return result;
                }

                //验证物业性质
                var value = vm.ResideType.ValueMember;
                if (value == -999)
                {
                    result = new OperationResult(1, "请选择房屋类型.");
                    return result;
                }


                if (vm.NotarySysNo == 0L)
                {
                    result = new OperationResult(1, "请填写公证员.");
                    return result;
                }


                #region 托管资金
                if (string.IsNullOrEmpty(vm.FundTrusteeshipContract))
                {
                    return new OperationResult(1, "请填写资金托管合同号");
                }
                #endregion

                #region 佣金托管
                if (string.IsNullOrEmpty(vm.CommissionTrusteeshipContract.Trim()))
                {
                    result = new OperationResult(1, "请填写佣金托管合同号.");
                    return result;
                }
                #endregion
                #endregion
            }
            catch (Exception e)
            {
                result = new OperationResult(1, "验证数据合法性异常错误：" + e.Message);
            }
            return result;

        }
        #endregion

        #region Nuclear--CaseCustomerInfo

        private void OnLoadCaseCustomerInfoView(string caseId)
        {
            //加载进viewmodel
            CaseCustomerInfoViewModel vm = ServiceLocator.Current.GetInstance<CaseCustomerInfoViewModel>();

            BindingDataToCaseCustomerInfoModel(caseId, vm);

            IRegion mainRegion = this.regionManager.Regions[RegionNames.MainRegion];
            if (mainRegion == null) return;

            CaseCustomerInfoView view = mainRegion.GetView(ViewNamesKey.NUCLEAR_CASE_CUSTOMER_INFO_VIEW) as CaseCustomerInfoView;
            if (view == null)
            {
                view = this.container.Resolve<CaseCustomerInfoView>();
                //Add the view to the main region. This automatically activates the view too.
                mainRegion.Add(view, ViewNamesKey.NUCLEAR_CASE_CUSTOMER_INFO_VIEW);
                mainRegion.Activate(view);
                view.ViewModel = vm;
            }
            else
            {
                //The view has already been added to the region so just activate it.
                mainRegion.Activate(view);
                view.ViewModel = vm;
            }

        }


        private void BindingDataToCaseCustomerInfoModel(string caseId, CaseCustomerInfoViewModel vm)
        {
            try
            {
                /// <summary>
                /// 页面状态 -- 核案过来的状态
                /// </summary>
                CaseStatus m_Status = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.HA3
                        | CaseStatus.HA4 | CaseStatus.HA5 | CaseStatus.HA6 | CaseStatus.ZB2;
                vm.CaseId = caseId;

                CaseDto m_CaseInfo = null;
                //上下家信息
                Seller seller = null;
                Buyer buyer = null;
                //案件对象
                m_CaseInfo = CaseProxyService.GetInstanse().GetCaseByCaseIdAndCaseStatus(caseId, m_Status);
                //获取客户信息
                Tuple<Seller, Buyer> sb = CaseProxyService.GetInstanse().GetCaseCustomers(caseId);
                if (sb != null) seller = sb.Item1;
                if (sb != null) buyer = sb.Item2;

                if (buyer != null)
                {
                    vm.Buyer = new VMBuyer();
                    vm.Buyer.AddressType = (int)buyer.AddressType;
                    vm.Buyer.PostProvince = buyer.PostProvince;
                    vm.Buyer.PostCity = buyer.PostCity;
                    vm.Buyer.PostArea = buyer.PostArea;
                    vm.Buyer.PostAddress = buyer.PostAddress;
                    vm.Buyer.Recipients = buyer.Recipients;
                    vm.Buyer.RecipientsMobile = buyer.RecipientsMobile;
                    vm.Buyer.SelfCredit = buyer.SelfCredit;
                    vm.Buyer.SelfCreditReson = buyer.SelfCreditReson;
                    vm.Buyer.CusetomersList = new ObservableCollection<Cusetomers>();
                    foreach (var customer in buyer.Customers)
                    {
                        vm.Buyer.CusetomersList.Add(new Cusetomers()
                        {
                            UserSysNo = customer.UserSysNo,
                            RealName = customer.RealName,
                            Gender = EnumHelper.GetEnumDesc(customer.Gender),
                            CertificateType = EnumHelper.GetEnumDesc(customer.CertificateType),
                            IdentityNo = customer.IdentityNo,
                            Nationality = customer.Nationality,
                            Mobile = customer.Mobile,
                            Email = customer.Email
                        });
                    }
                }

                if (seller != null)
                {
                    vm.Seller = new VMSeller();
                    vm.Seller.AddressType = (int)seller.AddressType;
                    vm.Seller.PostProvince = seller.PostProvince;
                    vm.Seller.PostCity = seller.PostCity;
                    vm.Seller.PostArea = seller.PostArea;
                    vm.Seller.PostAddress = seller.PostAddress;
                    vm.Seller.Recipients = seller.Recipients;
                    vm.Seller.RecipientsMobile = seller.RecipientsMobile;
                    vm.Seller.IsGuaranty = seller.IsGuaranty;
                    vm.Seller.BankCode = seller.RepayCreditBank;
                    vm.Seller.BankName = !string.IsNullOrEmpty(seller.RepayCreditBank)
                        ? ConfigServiceProxy.GetBanks()[seller.RepayCreditBank].ToString() : string.Empty;
                    vm.Seller.RepayCreditAmount = seller.RepayCreditAmount.ToString();
                    vm.Seller.CusetomersList = new ObservableCollection<Cusetomers>();
                    foreach (var customer in seller.Customers)
                    {

                        vm.Seller.CusetomersList.Add(new Cusetomers()
                        {
                            UserSysNo = customer.UserSysNo,
                            RealName = customer.RealName,
                            Gender = EnumHelper.GetEnumDesc(customer.Gender),
                            CertificateType = EnumHelper.GetEnumDesc(customer.CertificateType),
                            IdentityNo = customer.IdentityNo,
                            Nationality = customer.Nationality,
                            Mobile = customer.Mobile,
                            Email = customer.Email
                        });
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void SubmitCaseCustomerInfoView(CaseCustomerInfoViewModel vm)
        {
            try
            {
                //var result = CaseProxyService.GetInstanse().PerfectBuyer(m_CaseInfo.CaseId, buyer, moblies, emails, LoginHelper.CurrentUser.SysNo);

                //if (!result.Success) { throw new Exception("买方信息保存失败：" + result.ResultMessage); }
                //return result;
            }
            catch (Exception ex)
            {

                throw new Exception("买方信息保存发生异常.");
            }
        }

        private OperationResult SaveBuyerInfo(CaseCustomerInfoViewModel vm)
        {
            try
            {
                var buyer = new Buyer();
                var buyer1 = vm.Buyer;
                Dictionary<long, string> moblies = new Dictionary<long, string>();
                Dictionary<long, string> emails = new Dictionary<long, string>();

                foreach (var item in buyer1.CusetomersList)
                {
                    moblies.Add(item.UserSysNo, item.Mobile);
                    emails.Add(item.UserSysNo, item.Email);
                }
                buyer.AddressType = (AddressType)buyer1.AddressType;
                buyer.PostAddress = buyer1.PostAddress;
                buyer.PostProvince = buyer1.PostProvince;
                buyer.PostCity = buyer1.PostCity;
                buyer.PostArea = buyer1.PostArea;
                buyer.Recipients = buyer1.Recipients;
                buyer.RecipientsMobile = buyer1.RecipientsMobile;
                buyer.SelfCredit = buyer1.SelfCredit;
                buyer.SelfCreditReson = buyer1.SelfCreditReson;

               // var result = CaseProxyService.GetInstanse().PerfectBuyer(vm.CaseId, buyer, moblies, emails, LoginHelper.CurrentUser.SysNo);
               // if (!result.Success) { throw new Exception("买方信息保存失败：" + result.ResultMessage); }
               // return result;
                return new OperationResult(0, "");
            }
            catch (Exception)
            {
                throw new Exception("买方信息保存发生异常.");
            }
        }

        private OperationResult SaveSellerInfo(CaseCustomerInfoViewModel vm)
        {
            try
            {
                var seller = new Seller();
                var seller1 = vm.Seller;
                Dictionary<long, string> moblies = new Dictionary<long, string>();
                Dictionary<long, string> emails = new Dictionary<long, string>();

                foreach (var item in seller1.CusetomersList)
                {
                    moblies.Add(item.UserSysNo, item.Mobile);
                    emails.Add(item.UserSysNo, item.Email);
                }
                seller.AddressType = (AddressType)seller1.AddressType;
                seller.PostAddress = seller1.PostAddress;
                seller.PostProvince = seller1.PostProvince;
                seller.PostCity = seller1.PostCity;
                seller.PostArea = seller1.PostArea;
                seller.Recipients = seller1.Recipients;
                seller.RecipientsMobile = seller1.RecipientsMobile;
                seller.IsGuaranty = seller1.IsGuaranty;
                seller.RepayCreditBank = seller1.BankCode;

                var result = CaseProxyService.GetInstanse().PrefectSeller(vm.CaseId, seller, moblies, emails, LoginHelper.CurrentUser.SysNo);
                if (!result.Success) { throw new Exception("卖方信息保存失败：" + result.ResultMessage); }
                return result;
            }
            catch (Exception)
            {
                throw new Exception("卖方信息保存发生异常.");
            }
        }


        private OperationResult CheckDataValidity(CaseCustomerInfoViewModel vm)
        {
            var result = new OperationResult(0, null);
            var seller =vm.Seller;
            try
            {
                if (string.IsNullOrEmpty(seller.PostProvince) || string.IsNullOrEmpty(seller.PostCity) || string.IsNullOrEmpty(seller.PostArea))
                {
                    return new OperationResult(1, "请选择卖方邮件地址类型");
                }
                if (string.IsNullOrEmpty(seller.PostAddress))
                {
                    return new OperationResult(1, "请输入卖方邮寄地址");
                }
               // if(string.IsNullOrEmpty())



            }
            catch (Exception e)
            {
                result = new OperationResult(1, "验证数据合法性异常错误：" + e.Message);
            } return result;

        }

        #endregion

        #region Nuclear--TransactionInfoInfo

        private void OnLoadCaseTransactionInfoEvents(string caseId)
        {

            IRegion mainRegion = this.regionManager.Regions[RegionNames.MainRegion];
            if (mainRegion == null) return;
            CaseTransactionInfoView view = mainRegion.GetView(ViewNamesKey.NUCLEAR_CASE_TRANSACTION_INFO_VIEW) as CaseTransactionInfoView;
            TransactionInfoCtrlViewModel vm = Microsoft.Practices.ServiceLocation.ServiceLocator.Current.GetInstance<TransactionInfoCtrlViewModel>();
            vm.CaseId = caseId;
            vm.FlowList_Buyer = GetFlowList(caseId, CustomerType.Buyer);
            vm.FlowList_Seller = GetFlowList(caseId, CustomerType.Seller);

            if (view == null)
            {
                view = this.container.Resolve<CaseTransactionInfoView>();
                //Add the view to the main region. This automatically activates the view too.
                mainRegion.Add(view, ViewNamesKey.NUCLEAR_CASE_TRANSACTION_INFO_VIEW);
                mainRegion.Activate(view);
                view.ViewModel = vm;
                view.transactionInfo.ViewModel = vm;
            }
            else
            {
                //The view has already been added to the region so just activate it.
                mainRegion.Activate(view);
                view.ViewModel = vm;
                view.transactionInfo.ViewModel = vm;
            }
        }

        private ObservableCollection<CustomerFundFlowViewModel> GetFlowList(string caseId, CustomerType customerType)
        {
            var customerFundFlowList = CustomerFundFlowServiceProxy.GetFlows(caseId);
            var flowList = customerFundFlowList.Where(customerFundFlow => customerFundFlow.CustomerType == customerType)
                .OrderByDescending(customerFundFlow => customerFundFlow.CreateDate).ToList();

            ObservableCollection<CustomerFundFlowViewModel> olist = new ObservableCollection<CustomerFundFlowViewModel>();
            decimal houseFundTotal = 0;
            decimal taxesTotal = 0;
            decimal brokerageTotal = 0;
            decimal decorateCompensationTotal = 0;
            decimal otherChargesTotal = 0;
            decimal amountTotal = 0;
            decimal accountBalanceTotal = 0;
            var i = 1;

            foreach (var item in flowList)
            {

                houseFundTotal += item.HouseFund;
                taxesTotal += item.Taxes;
                brokerageTotal += item.Brokerage;
                decorateCompensationTotal += item.DecorateCompensation;
                otherChargesTotal += item.OtherCharges;
                amountTotal += item.Amount;

                CustomerFundFlowViewModel flowViewModel = ServiceLocator.Current.GetInstance<CustomerFundFlowViewModel>();
                flowViewModel.SysNo = item.SysNo;
                flowViewModel.RowNum = i.ToString();
                flowViewModel.ConditionType = item.ConditionType;
                flowViewModel.HouseFund = item.HouseFund;
                flowViewModel.Taxes = item.Taxes;
                flowViewModel.Brokerage = item.Brokerage;
                flowViewModel.DecorateCompensation = item.DecorateCompensation;
                flowViewModel.Earnest = item.Earnest;
                flowViewModel.OtherCharges = item.OtherCharges;
                flowViewModel.Amount = item.Amount;
                flowViewModel.PaymentChannel = item.PaymentChannel;

                olist.Add(flowViewModel);
                i++;
            }
            CustomerFundFlowViewModel totalRow = ServiceLocator.Current.GetInstance<CustomerFundFlowViewModel>();
            totalRow.ConditionType = "合计";
            totalRow.HouseFund = houseFundTotal;
            totalRow.Taxes = taxesTotal;
            totalRow.Brokerage = brokerageTotal;
            totalRow.DecorateCompensation = decorateCompensationTotal;
            totalRow.OtherCharges = otherChargesTotal;
            totalRow.Amount = amountTotal;
            totalRow.CustomerType = customerType;
            totalRow.RowNum = string.Empty;
            olist.Add(totalRow);

            return olist;
        }

        #endregion


        private void OnStartContractEvents(string caseId)
        {
            var confrimReceiveCaseDialog = new ConfrimReceiveCaseDialog(caseId);
            bool? dialogResult = confrimReceiveCaseDialog.ShowDialog();
            if (dialogResult.HasValue && dialogResult.Value)
            {
                var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, "YJ1_HA1", LoginHelper.CurrentUser.SysNo);

                if (result.Success)
                {
                    MessageBox.Show("接单成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    var model = ServiceLocator.Current.GetInstance<CheckCaseSearchPageViewModel>();
                    OnLoadCaseListEvents(model);
                }
                else
                {
                    MessageBox.Show("接单失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        /// <summary>
        /// 取消签约
        /// </summary>
        /// <param name="caseId"></param>
        private void OnCancelContractEvents(string caseId)
        {
            var cancelContractDialog = new CancelContractDialog();
            bool? dialogResult = cancelContractDialog.ShowDialog();
            if (dialogResult.HasValue && dialogResult.Value)
            {
                var dic = new Dictionary<string, object>();
                dic.Add("Reason", cancelContractDialog.Reason);
                var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, "YJ1_ZB5", LoginHelper.CurrentUser.SysNo, dic);
                if (result.Success)
                {
                    MessageBox.Show("取消签约成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    var model = ServiceLocator.Current.GetInstance<CheckCaseSearchPageViewModel>();
                    OnLoadCaseListEvents(model);
                }
                else
                {
                    MessageBox.Show("取消签约失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void OnConfirmContractEvents(string caseId)
        {
            ConfirmContractDialogViewModel viewModel = ServiceLocator.Current.GetInstance<ConfirmContractDialogViewModel>();
            viewModel.CaseId = caseId;
            var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(caseId);
            if (caseDto != null)
            {
                viewModel.TenementContract = caseDto.TenementContract;
            }
            var allegeList = EnumHelper.InitAllegeToCombobox();
            foreach (var item in allegeList)
            {
                viewModel.AllegeList.Add(item);
            }
            var creditTypeList = EnumHelper.InitCreditTypeToCombobox();
            foreach (var item in creditTypeList)
            {
                viewModel.CreditTypeList.Add(item);
            }
            var loanObjectList = EnumHelper.InitLoanObjectToCombobox();
            foreach (var item in loanObjectList)
            {
                viewModel.LoanObjectList.Add(item);
            }
            var banks = ConfigServiceProxy.GetBankDic();
            banks.Add("A-NULL", "请选择");
            foreach (DictionaryEntry item in banks)
            {
                viewModel.BankList.Add(new Tuple<string, string>(item.Key.ToString(), item.Value.ToString()));
            }
            viewModel.ResponseObjectList = viewModel.BankList;
            var commissionerList = ConfigServiceProxy.GetCommissioners();
            viewModel.CommissionersList.Add(new Tuple<string, string>("A-NULL", "请选择"));
            foreach (string item in commissionerList)
            {
                viewModel.CommissionersList.Add(new Tuple<string, string>(item, item));
            }
            _confirmContractDialog = new ConfirmContractDialog(viewModel);
            _confirmContractDialog.ViewModel = viewModel;
            _confirmContractDialog.ShowDialog();

        }

        private void OnConfirmContractEnterEvents(ConfirmContractDialogViewModel viewModel)
        {
            decimal netlabelPrice = 0;
            decimal.TryParse(viewModel.NetLabelPrice, out netlabelPrice);
            if (netlabelPrice <= 0)
            {
                MessageBox.Show("请填写正确的网签价格.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                //是否贷款
                var isLoan = Convert.ToBoolean(viewModel.AllegeCurrentValue);
                var responseObject = viewModel.ResponseObjectCurrentValue;
                if (isLoan)
                {
                    if (string.IsNullOrEmpty(responseObject) || responseObject == "A-NULL")
                    {
                        MessageBox.Show("请选择银行.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }

                }
                var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(viewModel.CaseId);
                if (caseDto != null)
                {
                    //是否立即审税
                    bool isTrialTax;
                    if (viewModel.IsTrialTaxYes)
                    {
                        isTrialTax = true;
                    }
                    else
                    {
                        isTrialTax = false;
                    }

                    var paras = new Dictionary<string, object>();
                    paras["_isTrialTax"] = isTrialTax;
                    paras["_isLoan"] = isLoan;
                    paras["_netlabelPrice"] = netlabelPrice;
                    //贷款类型
                    paras.Add("CreditType", viewModel.CreditTypeCurrentValue);
                    //推送对象类型
                    paras.Add("LoanObject", viewModel.LoanCurrentIndex);
                    //推送对象
                    paras.Add("ResponseObject", viewModel.ResponseObjectCurrentValue);
                    //网签价格
                    paras.Add("NetlabelPrice", netlabelPrice);

                    //////////////////////////////////////////////////////
                    try
                    {
                        var updateCaseResult = CaseProxyService.GetInstanse().UpdateCaseStatus(viewModel.CaseId, "HA1_HA2", LoginHelper.CurrentUser.SysNo, paras);
                        if (updateCaseResult.Success)
                        {
                            MessageBox.Show("确认完成签约操作成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                            var model = ServiceLocator.Current.GetInstance<CheckCaseSearchPageViewModel>();
                            OnLoadCaseListEvents(model);
                        }
                        else
                        {
                            MessageBox.Show("确认完成签约操作失败：" + updateCaseResult.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("确认完成签约操作发生错误：" + ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    MessageBox.Show("案件数据获取失败，无法进行操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);

                }
            }
        }

        private void OnAddFundCommandEvents(Tuple<CustomerType, ChangeFundType, string> param)
        {
            FundFlowDialogViewModel viewModel = ServiceLocator.Current.GetInstance<FundFlowDialogViewModel>();
            viewModel.CaseId = param.Item3;
            viewModel.CurrentCustomerType = param.Item1;
            if (param.Item1 == CustomerType.Buyer)
            {
                viewModel.DialogTitle = "买方 - 收付款添加";
                viewModel.DepositVisibility_In = Visibility.Visible;
                viewModel.DepositVisibility_out = Visibility.Visible;
            }
            else if (param.Item1 == CustomerType.Seller)
            {
                viewModel.DialogTitle = "卖方 - 收付款添加";
                viewModel.DepositVisibility_In = Visibility.Collapsed;
                viewModel.DepositVisibility_out = Visibility.Collapsed;
            }

            if (param.Item2 == ChangeFundType.Collection)
            {
                viewModel.IsTabSelected_in = true;
                viewModel.IsTabSelected_out = false;
            }
            else if (param.Item2 == ChangeFundType.Payment)
            {
                viewModel.IsTabSelected_in = false;
                viewModel.IsTabSelected_out = true;
            }

            foreach (var con in ConfigServiceProxy.GetChangeFundCondition(ChangeFundType.Collection))
            {
                viewModel.ConditionName_in.Add(new Tuple<string, IList<string>>(con.Key, con.Value));
                viewModel.ConditionName1_in.Add(new Tuple<string, IList<string>>(con.Key, con.Value));
            }
            viewModel.ConditionName1SelectedValue_in = viewModel.ConditionName1_in[0].Item1;

            foreach (var con in ConfigServiceProxy.GetChangeFundCondition(ChangeFundType.Payment))
            {
                viewModel.ConditionName_out.Add(new Tuple<string, IList<string>>(con.Key, con.Value));
                viewModel.ConditionName1_out.Add(new Tuple<string, IList<string>>(con.Key, con.Value));
            }
            viewModel.ConditionName1SelectedValue_out = viewModel.ConditionName1_out[0].Item1;


            _fundFlowDialog = new FundFlowDialog();
            _fundFlowDialog.ViewModel = viewModel;
            _fundFlowDialog.ShowDialog();
        }

        private void OnFundFlowDialogSaveCommandEvents(FundFlowDialogViewModel vm)
        {
            ChangeFundType fundType;

            if (vm.IsTabSelected_in == true)
            {
                fundType = ChangeFundType.Collection;
            }
            else
            {
                fundType = ChangeFundType.Payment;
            }

            var flow = GetFundFlowDialogSaveFormData(vm, fundType);

            if (flow != null)
            {
                var result = CustomerFundFlowServiceProxy.Create(flow);
                if (result.Success)
                {
                    vm.Clear();
                }
                else
                {
                    MessageBox.Show(result.ResultMessage);
                }
                //刷新列表
                OnLoadCaseTransactionInfoEvents(vm.CaseId);
            }
        }

        private void OnFundFlowDialogEnterCommandEvents(FundFlowDialogViewModel vm)
        {
            ChangeFundType fundType;

            if (vm.IsTabSelected_in == true)
            {
                fundType = ChangeFundType.Collection;
            }
            else
            {
                fundType = ChangeFundType.Payment;
            }

            var flow = GetFundFlowDialogSaveFormData(vm, fundType);

            if (flow != null)
            {
                var result = CustomerFundFlowServiceProxy.Create(flow);
                if (result.Success)
                {
                    vm.Clear();
                    //把覆盖在最上层的window给关掉
                    if (_fundFlowDialog != null &&
                        _fundFlowDialog.IsActive)
                    {
                        _fundFlowDialog.Close();
                    }
                }
                else
                {
                    MessageBox.Show(result.ResultMessage);
                }
                //刷新列表
                OnLoadCaseTransactionInfoEvents(vm.CaseId);
            }


        }



        private CustomerFundFlow GetFundFlowDialogSaveFormData(FundFlowDialogViewModel vm, ChangeFundType fundType)
        {

            String errorMessage = String.Empty;

            Decimal houseFund = 0;
            Decimal taxes = 0;
            Decimal brokerage = 0;
            Decimal decorateCompensation = 0;
            Decimal deposit = 0;
            Decimal otherCharges = 0;
            var flow = new CustomerFundFlow();

            int errorCondition = 0;
            var regexString = "^[0-9]*[1-9][0-9]*$";

            if (fundType == ChangeFundType.Collection)
            {
                #region 收款
                if (!String.IsNullOrEmpty(vm.HouseFund_in))
                {
                    if (!Decimal.TryParse(vm.HouseFund_in.Trim(), out houseFund) || houseFund < 0)
                    {
                        errorMessage += "房款必须是大于0的数字\r\n";
                        houseFund = 0;
                    }
                }

                if (!String.IsNullOrEmpty(vm.Taxes_in))
                {
                    if (!Decimal.TryParse(vm.Taxes_in.Trim(), out taxes) || taxes < 0)
                    {
                        errorMessage += "税费必须是大于0的数字\r\n";
                        taxes = 0;
                    }
                }

                if (!String.IsNullOrEmpty(vm.Brokerage_in))
                {
                    if (!Decimal.TryParse(vm.Brokerage_in.Trim(), out brokerage) || brokerage < 0)
                    {
                        errorMessage += "中介佣金必须是大于0的数字\r\n";
                        brokerage = 0;
                    }
                }

                if (!String.IsNullOrEmpty(vm.DecorateCompensation_in))
                {
                    if (!Decimal.TryParse(vm.DecorateCompensation_in.Trim(), out decorateCompensation) || decorateCompensation < 0)
                    {
                        errorMessage += "装修补偿必须是大于0的数字\r\n";
                        decorateCompensation = 0;

                    }
                }

                if (vm.CurrentCustomerType == CustomerType.Buyer)
                {
                    if (!String.IsNullOrEmpty(vm.Deposit_in))
                    {
                        if (!Decimal.TryParse(vm.Deposit_in.Trim(), out deposit) || deposit < 0)
                        {
                            errorMessage += "定金必须是大于0的数字\r\n";
                            deposit = 0;

                        }
                    }
                }

                if (!String.IsNullOrEmpty(vm.OtherCharges_in))
                {
                    if (!Decimal.TryParse(vm.OtherCharges_in.Trim(), out otherCharges) || otherCharges < 0)
                    {
                        errorMessage += "其他费用必须是大于0的数字\r\n";
                        otherCharges = 0;
                    }
                }

                if (vm.IsTrusteeShip_in == false && vm.IsLoan_in == false && vm.IsSelfPay_In == false)
                {
                    errorMessage += "请选择支付方式\r\n";
                }

                if (vm.Day1Visibility_in == Visibility.Visible)
                {
                    if (!Regex.IsMatch(vm.Days1_in.Trim(), regexString))
                    {
                        errorCondition += 1;
                    }
                }
                if (vm.Date1Visibility_in == Visibility.Visible)
                {
                    if (vm.Date1_in.ToString() == "")
                    {
                        errorCondition += 1;
                    }
                }
                if (vm.Day2Visibility_in == Visibility.Visible)
                {
                    if (!Regex.IsMatch(vm.Days2_in.Trim(), regexString))
                    {
                        errorCondition += 1;
                    }
                }
                if (vm.Date2Visibility_in == Visibility.Visible)
                {
                    if (vm.Date2_in.ToString() == "")
                    {
                        errorCondition += 1;
                    }
                }

                //if (spSysNo1.Visibility == Visibility.Visible)
                //{
                //    if (!Regex.IsMatch(txtSysNo1.Text.Trim(), regexString))
                //    {
                //        errorCondition += 1;
                //    }
                //}

                //if (spSysNo2.Visibility == Visibility.Visible)
                //{
                //    if (!Regex.IsMatch(txtSysNo2.Text.Trim(), regexString))
                //    {
                //        errorCondition += 1;
                //    }
                //}

                #endregion
            }
            else if (fundType == ChangeFundType.Payment)
            {
                #region 出款
                if (!String.IsNullOrEmpty(vm.HouseFund_out))
                {
                    if (!Decimal.TryParse(vm.HouseFund_out.Trim(), out houseFund) || houseFund < 0)
                    {
                        errorMessage += "房款必须是大于0的数字\r\n";
                        houseFund = 0;
                    }
                }

                if (!String.IsNullOrEmpty(vm.Taxes_out))
                {
                    if (!Decimal.TryParse(vm.Taxes_out.Trim(), out taxes) || taxes < 0)
                    {
                        errorMessage += "税费必须是大于0的数字\r\n";
                        taxes = 0;
                    }
                }

                if (!String.IsNullOrEmpty(vm.Brokerage_out))
                {
                    if (!Decimal.TryParse(vm.Brokerage_out.Trim(), out brokerage) || brokerage < 0)
                    {
                        errorMessage += "中介佣金必须是大于0的数字\r\n";
                        brokerage = 0;
                    }
                }

                if (!String.IsNullOrEmpty(vm.DecorateCompensation_out))
                {
                    if (!Decimal.TryParse(vm.DecorateCompensation_out.Trim(), out decorateCompensation) || decorateCompensation < 0)
                    {
                        errorMessage += "装修补偿必须是大于0的数字\r\n";
                        decorateCompensation = 0;

                    }
                }

                if (vm.CurrentCustomerType == CustomerType.Buyer)
                {
                    if (!String.IsNullOrEmpty(vm.Deposit_out))
                    {
                        if (!Decimal.TryParse(vm.Deposit_out.Trim(), out deposit) || deposit < 0)
                        {
                            errorMessage += "定金必须是大于0的数字\r\n";
                            deposit = 0;

                        }
                    }
                }

                if (!String.IsNullOrEmpty(vm.OtherCharges_out))
                {
                    if (!Decimal.TryParse(vm.OtherCharges_out.Trim(), out otherCharges) || otherCharges < 0)
                    {
                        errorMessage += "其他费用必须是大于0的数字\r\n";
                        otherCharges = 0;
                    }
                }

                if (vm.IsTrusteeShip_out == false && vm.IsLoan_out == false && vm.IsSelfPay_out == false)
                {
                    errorMessage += "请选择支付方式\r\n";
                }

                if (vm.Day1Visibility_out == Visibility.Visible)
                {
                    if (!Regex.IsMatch(vm.Days1_out.Trim(), regexString))
                    {
                        errorCondition += 1;
                    }
                }
                if (vm.Date1Visibility_out == Visibility.Visible)
                {
                    if (vm.Date1_out.ToString() == "")
                    {
                        errorCondition += 1;
                    }
                }
                if (vm.Day2Visibility_out == Visibility.Visible)
                {
                    if (!Regex.IsMatch(vm.Days2_out.Trim(), regexString))
                    {
                        errorCondition += 1;
                    }
                }
                if (vm.Date2Visibility_out == Visibility.Visible)
                {
                    if (vm.Date2_out.ToString() == "")
                    {
                        errorCondition += 1;
                    }
                }

                if (vm.SysNo1Visibility_out == Visibility.Visible)
                {
                    if (!Regex.IsMatch(vm.SysNo1_out, regexString))
                    {
                        errorCondition += 1;
                    }
                }

                if (vm.SysNo2Visibility_out == Visibility.Visible)
                {
                    if (!Regex.IsMatch(vm.SysNo2_out, regexString))
                    {
                        errorCondition += 1;
                    }
                }
                #endregion
            }


            if (houseFund == 0 && taxes == 0 && brokerage == 0 && decorateCompensation == 0 && otherCharges == 0 && deposit == 0)
            {
                errorMessage += "所有数据不能为空并且不能都是0\r\n";
            }


            if (errorCondition > 0)
            {
                errorMessage += "条件不完整\r\n";
            }

            if (!String.IsNullOrEmpty(errorMessage))
            {
                MessageBox.Show(errorMessage);
                return null;

            }
            else
            {
                var conditions = new StringBuilder();
                if (fundType == ChangeFundType.Collection)
                {
                    conditions.Append(vm.ConditionName1SelectedValue_in);
                    //if (spSysNo1.Visibility == Visibility.Visible)
                    //{
                    //    conditions.Append(txtSysNo1.Text.Trim() + "的款项");
                    //}

                    conditions.Append(" ");
                    if (vm.Day1Visibility_in == Visibility.Visible)
                    {
                        conditions.Append(vm.Days1_in + vm.ConditionValue1SelectedValue_in);
                    }
                    else if (vm.Date1Visibility_in == Visibility.Visible)
                    {
                        if (vm.Date1_in.HasValue)
                        {
                            conditions.Append(string.Format("{0:D}", vm.Date1_in));
                            conditions.Append(" " + vm.ConditionValue1SelectedValue_in);
                        }
                    }
                    else
                    {
                        conditions.Append(vm.ConditionValue1SelectedValue_in);
                    }

                    if (vm.Conditions2Visibility_in == Visibility.Visible)
                    {
                        conditions.Append(" 且 ");
                        conditions.Append(vm.ConditionName2SelectedValue_in);
                        //if (spSysNo1.Visibility == Visibility.Visible)
                        //{
                        //    conditions.Append(txtSysNo2.Text.Trim() + "的款项");
                        //}
                        conditions.Append(" ");
                        if (vm.Day2Visibility_in == Visibility.Visible)
                        {
                            conditions.Append(vm.Days2_in + vm.ConditionValue2SelectedValue_in);
                        }
                        else if (vm.Date2Visibility_in == Visibility.Visible)
                        {
                            if (vm.Date2_in.HasValue)
                            {
                                conditions.Append(string.Format("{0:D}", vm.Date2_in));
                                conditions.Append(" " + vm.ConditionValue2SelectedValue_in);
                            }
                        }
                        else
                        {
                            conditions.Append(vm.ConditionValue2SelectedValue_in);
                        }
                    }

                    if (vm.IsTrusteeShip_in == true)
                    {
                        flow.PaymentChannel = FundPlanPayChannel.Trusteeship;
                    }
                    else if (vm.IsLoan_in == true)
                    {
                        flow.PaymentChannel = FundPlanPayChannel.Loan;
                    }
                    else if (vm.IsSelfPay_In == true)
                    {
                        flow.PaymentChannel = FundPlanPayChannel.SelfPay;
                    }
                }
                else if (fundType == ChangeFundType.Payment)
                {
                    conditions.Append(vm.ConditionName1SelectedValue_out);
                    if (vm.SysNo1Visibility_out == Visibility.Visible)
                    {
                        conditions.Append(vm.SysNo1_out + "的款项");
                    }

                    conditions.Append(" ");
                    if (vm.Day1Visibility_out == Visibility.Visible)
                    {
                        conditions.Append(vm.Days1_out + vm.ConditionValue1SelectedValue_out);
                    }
                    else if (vm.Date1Visibility_out == Visibility.Visible)
                    {
                        if (vm.Date1_out.HasValue)
                        {
                            conditions.Append(string.Format("{0:D}", vm.Date1_out));
                            conditions.Append(" " + vm.ConditionValue1SelectedValue_out);
                        }
                    }
                    else
                    {
                        conditions.Append(vm.ConditionValue1SelectedValue_out);
                    }

                    if (vm.Conditions2Visibility_out == Visibility.Visible)
                    {
                        conditions.Append(" 且 ");
                        conditions.Append(vm.ConditionName2SelectedValue_out);
                        if (vm.SysNo2Visibility_out == Visibility.Visible)
                        {
                            conditions.Append(vm.SysNo2_out + "的款项");
                        }
                        conditions.Append(" ");
                        if (vm.Day2Visibility_out == Visibility.Visible)
                        {
                            conditions.Append(vm.Days2_out + vm.ConditionValue2SelectedValue_out);
                        }
                        else if (vm.Date2Visibility_out == Visibility.Visible)
                        {
                            if (vm.Date2_out.HasValue)
                            {
                                conditions.Append(string.Format("{0:D}", vm.Date2_out));
                                conditions.Append(" " + vm.ConditionValue2SelectedValue_out);
                            }
                        }
                        else
                        {
                            conditions.Append(vm.ConditionValue2SelectedValue_out);
                        }
                    }

                    if (vm.IsTrusteeShip_out == true)
                    {
                        flow.PaymentChannel = FundPlanPayChannel.Trusteeship;
                    }
                    else if (vm.IsLoan_out == true)
                    {
                        flow.PaymentChannel = FundPlanPayChannel.Loan;
                    }
                    else if (vm.IsSelfPay_out == true)
                    {
                        flow.PaymentChannel = FundPlanPayChannel.SelfPay;
                    }
                }



                if (fundType == ChangeFundType.Payment)
                {
                    houseFund = houseFund * -1;
                    taxes = taxes * -1;
                    brokerage = brokerage * -1;
                    decorateCompensation = decorateCompensation * -1;
                    otherCharges = otherCharges * -1;
                    deposit = deposit * -1;
                }



                flow.CaseId = vm.CaseId;
                flow.CustomerType = vm.CurrentCustomerType;
                flow.ConditionType = conditions.ToString();
                flow.HouseFund = houseFund;
                flow.Taxes = taxes;
                flow.Brokerage = brokerage;
                flow.Earnest = deposit;
                flow.DecorateCompensation = decorateCompensation;
                flow.OtherCharges = otherCharges;
                flow.Amount = houseFund + taxes + brokerage + decorateCompensation + otherCharges + deposit;
                //flow.AccountBalance = _accountBalance + flow.Amount;
                flow.AccountBalance = flow.Amount;
                flow.CreateDate = DateTime.Now;
                flow.CreateUserSysNo = LoginHelper.CurrentUser.SysNo;


                return flow;
            }

        }

        private void OnDeleteFundFlowCommandEvents(Tuple<TransactionInfoCtrlViewModel, string> param)
        {
            //这里做删除
            if (param.Item2 == "Buyer")
            {
                foreach (var item in param.Item1.FlowList_Buyer)
                {
                    if (item.IsChecked == true)
                    {
                        CustomerFundFlowServiceProxy.DeleteFlows(item.SysNo, LoginHelper.CurrentUser.SysNo);
                    }
                }
            }
            else
            {
                foreach (var item in param.Item1.FlowList_Seller)
                {
                    if (item.IsChecked == true)
                    {
                        CustomerFundFlowServiceProxy.DeleteFlows(item.SysNo, LoginHelper.CurrentUser.SysNo);
                    }
                }
            }

            OnLoadCaseTransactionInfoEvents(param.Item1.CaseId);

        }

        private async void OnLoadCaseDetailInfoCommandEvents(string caseId)
        {
            IRegion loadingRegion = this.regionManager.Regions[RegionNames.LoadingRegion];
            if (loadingRegion == null) return;
            LoadingControl loadingView = loadingRegion.GetView(ViewNamesKey.LOADING_CONTROL_VIEW) as LoadingControl;
            if (loadingView == null)
            {
                loadingView = this.container.Resolve<LoadingControl>();
                //Add the view to the main region. This automatically activates the view too.
                loadingRegion.Add(loadingView, ViewNamesKey.LOADING_CONTROL_VIEW);
                loadingRegion.Activate(loadingView);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                loadingRegion.Activate(loadingView);
            }

            IRegion mainRegion = this.regionManager.Regions[RegionNames.MainRegion];
            if (mainRegion == null) return;



            //没有提交报告的案件显示无报告的页面，否则显示有报告的页面
            var caseEvents = CaseProxyService.GetInstanse().GetCaseEvents(caseId, CaseStatus.HA4, null);
            if (caseEvents == null)
            {
                CaseDetailInfoNoReportViewModel vm = GetCaseDetailInfoNoReportViewModel(caseId);


                CaseDetailInfoNoReportView view =
                    mainRegion.GetView(ViewNamesKey.NUCLEAR_CASE_DETAIL_INFO_NO_REPORT_VIEWView) as CaseDetailInfoNoReportView;
                if (view == null)
                {
                    view = this.container.Resolve<CaseDetailInfoNoReportView>();
                    //Add the view to the main region. This automatically activates the view too.
                    mainRegion.Add(view, ViewNamesKey.NUCLEAR_CASE_DETAIL_INFO_NO_REPORT_VIEWView);
                    mainRegion.Activate(view);
                    view.ViewModel = vm;
                }
                else
                {
                    //The view has already been added to the region so just activate it.
                    mainRegion.Activate(view);
                    view.ViewModel = vm;
                }


            }
            else
            {

            }


            loadingRegion.Deactivate(loadingView);




        }

        private CaseDetailInfoNoReportViewModel GetCaseDetailInfoNoReportViewModel(string caseId)
        {
            CaseDetailInfoNoReportViewModel vm = ServiceLocator.Current.GetInstance<CaseDetailInfoNoReportViewModel>();

            var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(caseId);
            if (caseDto != null)
            {
                #region 买方人员列表
                if (caseDto.BuyerNameDic != null && caseDto.BuyerNameDic.Count > 0)
                {
                    foreach (var key in caseDto.BuyerNameDic.Keys)
                    {
                        CustomerInfoViewModel customerInfoViewModel = ServiceLocator.Current.GetInstance<CustomerInfoViewModel>();
                        customerInfoViewModel.CustomerId = key.ToString();
                        customerInfoViewModel.CustomerName = caseDto.BuyerNameDic[key];
                        CustomerAndMateriaListViewModel OIA = new CustomerAndMateriaListViewModel();
                        OIA.CustomerInfo = customerInfoViewModel;
                        var materiaList = AttachmentProxyService.GetInstanse().GetAttachments(key.ToString(), buyerAttachment);
                        if (materiaList != null && materiaList.Count > 0)
                        {
                            var m = new ObservableCollection<MateriaViewModel>();
                            foreach (var materia in materiaList)
                            {
                                MateriaViewModel model = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                                foreach (var item in materia.Value)
                                {
                                    model.MateriaCount.Add(item);
                                }
                                model.MateriaName = materia.Key;
                                m.Add(model);
                            }
                            OIA.MateriaList = m;
                        }
                        vm.BuyerList.Add(OIA);
                    }
                }
                #endregion

                #region 卖方人员列表
                if (caseDto.SellerNameDic != null && caseDto.SellerNameDic.Count > 0)
                {
                    foreach (var key in caseDto.SellerNameDic.Keys)
                    {
                        CustomerInfoViewModel customerInfoViewModel = ServiceLocator.Current.GetInstance<CustomerInfoViewModel>();
                        customerInfoViewModel.CustomerId = key.ToString();
                        customerInfoViewModel.CustomerName = caseDto.SellerNameDic[key];
                        CustomerAndMateriaListViewModel OIA = new CustomerAndMateriaListViewModel();
                        OIA.CustomerInfo = customerInfoViewModel;
                        var materiaList = AttachmentProxyService.GetInstanse().GetAttachments(key.ToString(), sellerAttachment);
                        if (materiaList != null && materiaList.Count > 0)
                        {
                            var m = new ObservableCollection<MateriaViewModel>();
                            foreach (var materia in materiaList)
                            {
                                MateriaViewModel model = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                                foreach (var item in materia.Value)
                                {
                                    model.MateriaCount.Add(item);
                                }
                                model.MateriaName = materia.Key;
                                m.Add(model);
                            }
                            OIA.MateriaList = m;
                        }
                        vm.SellerList.Add(OIA);
                    }
                }
                #endregion

                #region 权利凭证
                var houseRightVoucher = AttachmentProxyService.GetInstanse().GetAttachments(caseDto.TenementContract, new List<AttachmentType>() { AttachmentType.产证 });

                MateriaViewModel modelHouse = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                if (houseRightVoucher.Keys.Contains(AttachmentType.产证))
                {
                    foreach (var item in houseRightVoucher[AttachmentType.产证])
                    {
                        modelHouse.MateriaCount.Add(item);
                    }
                }
                else
                {
                    modelHouse.MateriaCount = null;
                }
                modelHouse.MateriaName = AttachmentType.产证;
                modelHouse.IsShowItem = true;
                vm.RightsMateriaList.Add(modelHouse);

                var rightVoucher = AttachmentProxyService.GetInstanse().GetAttachments(caseId, VoucherAttachment);
                foreach (var item in VoucherAttachment)
                {
                    MateriaViewModel model = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                    if (rightVoucher.Keys.Contains(item))
                    {
                        foreach (var item1 in rightVoucher[item])
                            model.MateriaCount.Add(item1);
                    }
                    else
                    {
                        model.MateriaCount = null;
                    }
                    model.MateriaName = item;
                    model.IsShowItem = true;
                    vm.RightsMateriaList.Add(model);
                }
                #endregion

                #region 交易材料
                var tradeRsult = AttachmentProxyService.GetInstanse().GetAttachments(caseDto.TenementContract, new List<AttachmentType>() { AttachmentType.产调证 });

                MateriaViewModel modelTrade = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                if (tradeRsult.Keys.Contains(AttachmentType.产调证))
                {
                    foreach (var item in tradeRsult[AttachmentType.产调证])
                    {
                        modelTrade.MateriaCount.Add(item);
                    }
                }
                else
                {
                    modelTrade.MateriaCount = null;
                }
                modelTrade.MateriaName = AttachmentType.产调证;
                modelTrade.IsShowItem = true;
                vm.TranscationList.Add(modelTrade);
                var tradeRsult1 = AttachmentProxyService.GetInstanse().GetAttachments(caseId, TradeAttachment);
                foreach (var item in TradeAttachment)
                {
                    MateriaViewModel model = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                    if (tradeRsult1.Keys.Contains(item))
                    {
                        foreach (var item1 in tradeRsult1[item])
                        {
                            model.MateriaCount.Add(item1);
                        }
                    }
                    else
                    {
                        model.MateriaCount = null;
                    }
                    model.MateriaName = item;
                    model.IsShowItem = true;
                    vm.TranscationList.Add(model);
                }
                #endregion

                #region 合同协议
                var contractResults = AttachmentProxyService.GetInstanse().GetAttachments(caseId, ContractAttachment);
                foreach (var item in ContractAttachment)
                {
                    MateriaViewModel model = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                    if (contractResults.Keys.Contains(item))
                    {
                        foreach (var item1 in contractResults[item])
                        {
                            model.MateriaCount.Add(item1);
                        }
                    }
                    else
                    {
                        model.MateriaCount = null;
                    }
                    model.MateriaName = item;
                    model.IsShowItem = true;
                    vm.ContractList.Add(model);
                }
                #endregion

                #region 钱款收据
                var moneyResults = AttachmentProxyService.GetInstanse().GetAttachments(caseId, MoneyAttachment);
                foreach (var item in MoneyAttachment)
                {
                    MateriaViewModel model = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                    if (moneyResults.Keys.Contains(item))
                    {
                        foreach (var item1 in moneyResults[item])
                        {
                            model.MateriaCount.Add(item1);
                        }
                    }
                    else
                    {
                        model.MateriaCount = null;
                    }
                    model.MateriaName = item;
                    model.IsShowItem = true;
                    vm.MoneyList.Add(model);
                }
                #endregion

            }
            vm.MateriaList = new ObservableCollection<MateriaViewModel>();
            vm.MateriaList1 = new ObservableCollection<MateriaViewModel>();
            var list = new List<MateriaViewModel>();
            foreach (var materia in sellerAttachment)
            {
                MateriaViewModel mvm = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                mvm.MateriaName = materia;
                mvm.IsShowItem = true;
                list.Add(mvm);
            }

            vm.BuyMateriaList = new ObservableCollection<MateriaViewModel>();
            vm.BuyMateriaList1 = new ObservableCollection<MateriaViewModel>();
            var buyList = new List<MateriaViewModel>();
            foreach (var materia in buyerAttachment)
            {
                MateriaViewModel mvm = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                mvm.MateriaName = materia;
                mvm.IsShowItem = true;
                buyList.Add(mvm);
            }
            for (int i = 0; i < 14; i++)
            {
                vm.MateriaList.Add(list[i]);
                vm.BuyMateriaList.Add(buyList[i]);
            }
            for (int j = 14; j < list.Count; j++)
            {
                vm.MateriaList1.Add(list[j]);
            }
            for (int m = 14; m < buyList.Count; m++)
            {
                vm.BuyMateriaList1.Add(buyList[m]);
            }
            for (int k = list.Count; k < 28; k++)
            {
                MateriaViewModel mvm1 = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                vm.MateriaList1.Add(mvm1);
            }
            for (int k = buyList.Count; k < 28; k++)
            {
                MateriaViewModel mvm1 = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                vm.BuyMateriaList1.Add(mvm1);
            }



            //////////////////////////////////

            var status = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.HA3 |
                         CaseStatus.HA4 | CaseStatus.HA5 | CaseStatus.HA6 | CaseStatus.ZB2;
            var dicCaseStatus = new Dictionary<CaseStatus, Tuple<string, DateTime>>();
            var showCaseDto = CaseProxyService.GetInstanse().GetCaseAndProcess(caseId, status, ref dicCaseStatus);
            if (showCaseDto != null)
            {
                //案件流程节点
                if (dicCaseStatus != null && dicCaseStatus.Count > 0)
                {
                    //已签约
                    if (dicCaseStatus.ContainsKey(CaseStatus.HA2))
                    {
                        vm.HA2Ready = true;
                        TimeSpan ts = (TimeSpan)(dicCaseStatus[CaseStatus.HA2].Item2 - dicCaseStatus[CaseStatus.HA1].Item2);
                        if (ts == TimeSpan.Zero)
                        {
                            vm.HA1_HA2Time = string.Empty;
                            vm.HA2Ready = false;
                        }
                        else
                        {
                            vm.HA1_HA2Time = ts.Days + "天" + ts.Hours + "时" + ts.Minutes + "分" +
                                                      ts.Seconds + "秒";
                            vm.HA2Ready = true;
                        }
                    }

                    //填写核案报告
                    if (dicCaseStatus.ContainsKey(CaseStatus.HA3))
                    {
                        vm.HA3Ready = true;
                        TimeSpan ts = (TimeSpan)(dicCaseStatus[CaseStatus.HA3].Item2 - dicCaseStatus[CaseStatus.HA2].Item2);
                        if (ts == TimeSpan.Zero)
                        {
                            vm.HA2_HA3Time = string.Empty;
                            vm.HA3Ready = false;
                        }
                        else
                        {
                            vm.HA2_HA3Time = ts.Days + "天" + ts.Hours + "时" + ts.Minutes + "分" +
                                                      ts.Seconds + "秒";
                            vm.HA3Ready = true;
                        }
                    }

                    //提交核案报告
                    if (dicCaseStatus.ContainsKey(CaseStatus.HA4))
                    {
                        vm.HA4Ready = true;
                        TimeSpan ts = (TimeSpan)(dicCaseStatus[CaseStatus.HA4].Item2 - dicCaseStatus[CaseStatus.HA3].Item2);
                        if (ts == TimeSpan.Zero)
                        {
                            vm.HA3_HA4Time = string.Empty;
                            vm.HA4Ready = false;
                        }
                        else
                        {
                            vm.HA3_HA4Time = ts.Days + "天" + ts.Hours + "时" + ts.Minutes + "分" +
                                                      ts.Seconds + "秒";
                            vm.HA4Ready = true;
                        }
                    }

                    //审核通过完成
                    if (dicCaseStatus.ContainsKey(CaseStatus.HA5))
                    {
                        vm.HA5Ready = true;
                        TimeSpan ts = (TimeSpan)(dicCaseStatus[CaseStatus.HA5].Item2 - dicCaseStatus[CaseStatus.HA4].Item2);
                        if (ts == TimeSpan.Zero)
                        {
                            vm.HA5Ready = false;
                        }
                        else
                        {
                            vm.HA5Ready = true;
                        }
                    }

                }

                if (showCaseDto.TenementContract != null)
                {
                    var temps = showCaseDto.TenementContract.Split('-');
                    if (temps.Length > 1)
                    {
                        vm.TbTenementContractWord = temps[0];
                        vm.TbTenementContract = temps[1];
                    }
                    else
                    {
                        vm.TbTenementContract = temps[0];
                    }
                }
                vm.TbTenementAddress = showCaseDto.TenementAddress;
                vm.TblShowInfo = "姓名：" + showCaseDto.AgencyName + "                 所属门店：" + showCaseDto.CompanyName;
                vm.BuyerInfoList = new ObservableCollection<IdInfoModel>();
                vm.SellerInfoList = new ObservableCollection<IdInfoModel>();
                foreach (var sellerName in showCaseDto.SellerNameDic)
                {
                    UserIdentity userIdentity = UserServiceProxy.GetUserIdentityInfo(sellerName.Key, UserType.Customer);
                    if (userIdentity != null)
                    {
                        userIdentity.SysNo = sellerName.Key;
                        UserIdentityModel userModel = new UserIdentityModel
                        {
                            Name = sellerName.Value,
                            CertificateType = userIdentity.CertificateType,
                            Nationality = userIdentity.Nationality,
                            IdentityNo = userIdentity.Id.ToString(),
                            Gender = userIdentity.Gender,
                            Birthday = userIdentity.Birthday,
                            Address = userIdentity.Address,
                            VisaAgency = userIdentity.VisaAgency,
                            Nation = userIdentity.Nation,
                            Photo = userIdentity.Photo,
                            IsTrusted = userIdentity.IsTrusted,
                        };
                        DateTime? effectiveDate = userIdentity.EffectiveDate;
                        if (effectiveDate != null)
                        {
                            userModel.EffectiveDate = effectiveDate.Value.Date;
                        }
                        DateTime? expiryDate = userIdentity.ExpiryDate;
                        if (expiryDate != null)
                        {
                            userModel.ExpiryDate = expiryDate.Value.Date;
                        }
                        var dic = new Dictionary<long, string>();
                        dic.Add(sellerName.Key, sellerName.Value);
                        dic.Add(0, "true");
                        userModel.Tag = dic;

                        IdInfoModel userIdInfoModel = IdInfoDataModelToViewModel(userModel);
                        vm.SellerInfoList.Add(userIdInfoModel);
                    }
                }

                foreach (var buyerName in showCaseDto.BuyerNameDic)
                {
                    var userIdentity = UserServiceProxy.GetUserIdentityInfo(buyerName.Key, UserType.Customer);
                    if (userIdentity != null)
                    {
                        userIdentity.SysNo = buyerName.Key;
                        UserIdentityModel userModel = new UserIdentityModel
                        {
                            Name = buyerName.Value,
                            CertificateType = userIdentity.CertificateType,
                            Nationality = userIdentity.Nationality,
                            IdentityNo = userIdentity.Id.ToString(),
                            Gender = userIdentity.Gender,
                            Birthday = userIdentity.Birthday,
                            Address = userIdentity.Address,
                            VisaAgency = userIdentity.VisaAgency,
                            Nation = userIdentity.Nation,
                            EffectiveDate = userIdentity.EffectiveDate,
                            ExpiryDate = userIdentity.ExpiryDate,
                            Photo = userIdentity.Photo,
                            IsTrusted = userIdentity.IsTrusted,
                        };
                        var dic = new Dictionary<long, string>();
                        dic.Add(buyerName.Key, buyerName.Value);
                        dic.Add(0, "true");
                        userModel.Tag = dic;

                        IdInfoModel userIdInfoModel = IdInfoDataModelToViewModel(userModel);
                        vm.BuyerInfoList.Add(userIdInfoModel);
                    }
                }

                vm.IdCardBuyerModel = new IdInfoModel();
                vm.IdCardBuyerModel.IsCancel = false;
                vm.IdCardSellerModel = new IdInfoModel();
                vm.IdCardSellerModel.IsCancel = false;
                vm.IdentityBuyerList = EnumHelper.InitCertificateTypeToCombobox();
                vm.IndexBuyerSelect = 0;

                vm.SellerTotal = vm.SellerInfoList.Count;
                vm.BuyerTotal = vm.BuyerInfoList.Count;
                vm.IdentitySellerList = EnumHelper.InitCertificateTypeToCombobox();
                vm.IndexSellerSelect = 0;

                var allStatus = CaseProxyService.GetInstanse().GetCaseEvents(caseId, null, null);
                if (allStatus != null)
                {
                    allStatus = allStatus.OrderBy(x => x.CreateDate).ToList();
                }
                vm.AllStatus = new CaseStatusViewModel(allStatus);
            }

            return vm;

        }

        private IdInfoModel IdInfoDataModelToViewModel(UserIdentityModel model)
        {
            var tempViewModel = new IdInfoModel();
            tempViewModel.IdAddress = model.Address;
            tempViewModel.IdBirth = Convert.ToString(model.Birthday);
            tempViewModel.CertificateType = model.CertificateType;
            if (model.EffectiveDate != null)
            {
                tempViewModel.EffectiveDate = Convert.ToString(model.EffectiveDate);
            }
            else
                tempViewModel.EffectiveDate = null;
            if (model.EffectiveDate != null)
            {
                tempViewModel.ExpiryDate = Convert.ToString(model.ExpiryDate);
            }
            else
            {
                tempViewModel.ExpiryDate = null;
            }
            switch (model.Gender)
            {
                case Gender.Male:
                    tempViewModel.Male = true;
                    tempViewModel.Female = false;
                    if (model.CertificateType == CertificateType.IdCard)
                    {
                        tempViewModel.IdSex = "男";
                    }
                    else
                    {
                        tempViewModel.IdSex = string.Empty;
                    }
                    break;
                case Gender.Female:
                    tempViewModel.Male = false;
                    tempViewModel.Female = true;
                    if (model.CertificateType == CertificateType.IdCard)
                    {
                        tempViewModel.IdSex = "女";
                    }
                    else
                    {
                        tempViewModel.IdSex = string.Empty;
                    }
                    break;
                default:
                    tempViewModel.Female = false;
                    tempViewModel.Male = false;
                    tempViewModel.IdSex = string.Empty;
                    break;
            }
            tempViewModel.IdNum = model.IdentityNo;
            tempViewModel.IsTrusted = model.IsTrusted;
            tempViewModel.IdName = model.Name;
            //if (model.Nation != null)
            //{
            //    tempViewModel.IdNation = (Nation)Nation.Parse(typeof(Nation), model.IdNation);
            //}
            tempViewModel.Nationality = model.Nationality;
            if (model.Photo != null)
            {
                tempViewModel.Bitmap = ByteToBmp(model.Photo);
            }
            tempViewModel.Tag = model.Tag;
            tempViewModel.IdAgent = model.VisaAgency;
            return tempViewModel;
        }

        private BitmapImage ByteToBmp(byte[] bytes)
        {
            BitmapImage bitmap = new BitmapImage();
            try
            {
                MemoryStream ms1 = new MemoryStream(bytes);
                bitmap.BeginInit();
                bitmap.StreamSource = ms1;
                bitmap.EndInit();

            }
            catch (Exception e)
            {
                MessageBox.Show("图像转化失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return bitmap;
        }

        #region 附件类型集合
        private List<AttachmentType> sellerAttachment = new List<AttachmentType>()
        {
            AttachmentType.身份证,
            AttachmentType.户口本,
            AttachmentType.出生证明,
            AttachmentType.结婚证,
            AttachmentType.离婚证,
            AttachmentType.离婚判决书,
            AttachmentType.离婚协议书,
            AttachmentType.单身证明,
            AttachmentType.授权委托书,
            AttachmentType.代理人身份证,
            AttachmentType.军官证,
            AttachmentType.法人身份证,
            AttachmentType.税务登记证,
            AttachmentType.组织机构代码证,
            AttachmentType.公司营业执照,
            AttachmentType.劳动合同,
            AttachmentType.在职证明,
            AttachmentType.境外人士就业证,
            AttachmentType.台胞证,
            AttachmentType.回乡证,
            AttachmentType.港澳身份证,
            AttachmentType.户籍藤本,
            AttachmentType.银行卡,
            AttachmentType.上家其他
        };
        private List<AttachmentType> buyerAttachment = new List<AttachmentType>()
        {
            AttachmentType.身份证,
            AttachmentType.户口本,
            AttachmentType.出生证明,
            AttachmentType.结婚证,
            AttachmentType.离婚证,
            AttachmentType.离婚判决书,
            AttachmentType.离婚协议书,
            AttachmentType.单身证明,
            AttachmentType.授权委托书,
            AttachmentType.代理人身份证,
            AttachmentType.军官证,
            AttachmentType.法人身份证,
            AttachmentType.税务登记证,
            AttachmentType.组织机构代码证,
            AttachmentType.公司营业执照,
            AttachmentType.劳动合同,
            AttachmentType.在职证明,
            AttachmentType.境外人士就业证,
            AttachmentType.台胞证,
            AttachmentType.回乡证,
            AttachmentType.港澳身份证,
            AttachmentType.户籍藤本,
            AttachmentType.银行卡,
            AttachmentType.收入证明,
            AttachmentType.社保,
            AttachmentType.个保,
            AttachmentType.下家其他
        };
        private List<AttachmentType> VoucherAttachment = new List<AttachmentType>()
        {
            AttachmentType.他证,
            AttachmentType.购房发票,
            AttachmentType.原购房发票,
            AttachmentType.原购房合同,
            AttachmentType.原契税发票,
            AttachmentType.预告登记证,
            AttachmentType.法院判决书,
            AttachmentType.拍卖确认书,
            AttachmentType.产证密码条,
            AttachmentType.权利凭证其他,
        };
        private List<AttachmentType> TradeAttachment = new List<AttachmentType>()
        {
            AttachmentType.核价单,
            AttachmentType.限购查询结果,
            AttachmentType.税收受理回执,
            AttachmentType.契税完税凭证,
            AttachmentType.个人所得税单,
            AttachmentType.营业税发票,
            AttachmentType.房产税认定书,
            AttachmentType.收件收据注销抵押,
            AttachmentType.收件收据产权过户,
            AttachmentType.经济人证书,
            AttachmentType.居间公司营业执照复印件,
            AttachmentType.居间公司委托书,
            AttachmentType.交易材料其他
        };
        private List<AttachmentType> ContractAttachment = new List<AttachmentType>()
        {
            AttachmentType.居间合同,
            AttachmentType.网签合同,
            AttachmentType.资金托管协议,
            AttachmentType.佣金托管协议,
            AttachmentType.装修补偿协议书,
            AttachmentType.房屋交接书,
            AttachmentType.解约协议书,
            AttachmentType.优先购买切结书,
            AttachmentType.限购政策告知书,
            AttachmentType.租赁合同复印件,
            AttachmentType.佣金确认书,
            AttachmentType.合同协议其他
        };
        private List<AttachmentType> MoneyAttachment = new List<AttachmentType>()
        {
            AttachmentType.中介定金收据,
            AttachmentType.房款收据,
            AttachmentType.佣金收据,
            AttachmentType.税费发票,
            AttachmentType.服务费发票,
            AttachmentType.上家提前还款结算凭证,
            AttachmentType.退款申请书,
            AttachmentType.收据遗失证明,
            AttachmentType.转账凭条,
            AttachmentType.钱款收据其他
        };
        #endregion



        ///// <summary>
        ///// 获取案件进程时间
        ///// </summary>
        ///// <param name="caseId"></param>
        ///// <returns></returns>
        //public Task<Dictionary<CaseStatus, Tuple<string, DateTime>>> GetCaseProcess(string caseId, CaseStatus status)
        //{
        //    return Task.Run(() =>
        //    {
        //        var dicCaseStatus = new Dictionary<CaseStatus, Tuple<string, DateTime>>();
        //        var caseProcessDto = CaseProxyService.GetInstanse().GetCaseAndProcess(caseId, status, ref dicCaseStatus);
        //        return dicCaseStatus;
        //    });

        //}
    }
}
