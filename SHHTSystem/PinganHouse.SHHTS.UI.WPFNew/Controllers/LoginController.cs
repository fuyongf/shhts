﻿using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;

using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using PinganHouse.SHHTS.UI.WPFNew.Controls;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;
using PinganHouse.SHHTS.UI.WPFNew.Views;

using System;
using System.Threading.Tasks;
using System.Windows;

using PinganHouse.SHHTS.UI.WPFNew.Common.Enums;

namespace PinganHouse.SHHTS.UI.WPFNew.Controllers
{
    public class LoginController
    {
        private readonly IUnityContainer container;
        private readonly IRegionManager regionManager;
        private readonly IEventAggregator eventAggregator;

        public LoginController(IUnityContainer container,
                                    IRegionManager regionManager,
                                    IEventAggregator eventAggregator)
        {
            if (container == null) throw new ArgumentNullException("container");
            if (regionManager == null) throw new ArgumentNullException("regionManager");
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");


            this.container = container;
            this.regionManager = regionManager;
            this.eventAggregator = eventAggregator;

            // Subscribe to the LoginEvents event.
            this.eventAggregator.GetEvent<LoginEvents>().Subscribe(this.OnLogin, true);
        }

        /// <summary>
        /// Called when a new employee is selected. This method uses
        /// view injection to programmatically 
        /// </summary>
        private async void OnLogin(LoginViewModel viewModel)
        {
            IRegion loadingRegion = this.regionManager.Regions[RegionNames.LoadingRegion];
            if (loadingRegion == null) return;
            LoadingControl loadingView = loadingRegion.GetView(ViewNamesKey.LOADING_CONTROL_VIEW) as LoadingControl;
            if (loadingView == null)
            {
                //Create a new instance of the EmployeeDetailsView using the Unity container.
                loadingView = this.container.Resolve<LoadingControl>();
                //Add the view to the main region. This automatically activates the view too.
                loadingRegion.Add(loadingView, ViewNamesKey.LOADING_CONTROL_VIEW);
                loadingRegion.Activate(loadingView);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                loadingRegion.Activate(loadingView);
            }


            try
            {
                var operationResult = await GetLoginResult(viewModel);
                loadingRegion.Deactivate(loadingView);
                //003 验证数据并数据持久化
                if (operationResult.Success)
                {
                    //成功登陆并持久化登陆数据 
                    var user = (User)operationResult.OtherData["UserInfo"];
                    LoginHelper.CurrentUser.SysNo = user.SysNo;
                    LoginHelper.CurrentUser.RealName = user.RealName;
                    LoginHelper.CurrentUser.TelPhone = user.TelPhone;                  
                }
                else
                {
                    MessageBox.Show("登陆失败：" + operationResult.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
            }
            catch (Exception ex)
            {
                loadingRegion.Deactivate(loadingView);
                throw ex;
            }


            MainViewModel mvm = ServiceLocator.Current.GetInstance<MainViewModel>();

            IRegion mainRegion = this.regionManager.Regions[RegionNames.MainRegion];
            if (mainRegion == null) return;
            MainView view = mainRegion.GetView(ViewNamesKey.MAIN_VIEW) as MainView;
            if (view == null)
            {
                //Create a new instance of the EmployeeDetailsView using the Unity container.
                view = this.container.Resolve<MainView>();
                view.DataContext = mvm;
                //Add the view to the main region. This automatically activates the view too.
                mainRegion.Add(view, ViewNamesKey.MAIN_VIEW);
                mainRegion.Activate(view);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                mainRegion.Activate(view);
            }

            WindowTitleViewModel windowTitleViewModel = ServiceLocator.Current.GetInstance<WindowTitleViewModel>();
            windowTitleViewModel.UserName = viewModel.UserName;
            IRegion titleRegion = this.regionManager.Regions[RegionNames.TitleRegion];
            if (titleRegion == null) return;
            WindowTitleView titleView = titleRegion.GetView(ViewNamesKey.WINDOW_TITLE_VIEW) as WindowTitleView;
            if (titleView == null)
            {
                //Create a new instance of the EmployeeDetailsView using the Unity container.
                titleView = this.container.Resolve<WindowTitleView>();
                titleView.DataContext = windowTitleViewModel;
                //Add the view to the main region. This automatically activates the view too.
                titleRegion.Add(titleView, ViewNamesKey.WINDOW_TITLE_VIEW);
                titleRegion.Activate(titleView);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                titleView.DataContext = windowTitleViewModel;
                titleRegion.Activate(titleView);
            }

        }

        public Task<OperationResult> GetLoginResult(LoginViewModel viewModel)
        {
            return Task.Run(() =>
            {
                var result = NetDetection((NetworkOption)viewModel.CurrentNetwork);
                if (result.Success)
                    return UserServiceProxy.Login(viewModel.UserName, viewModel.Password);
                else return result;
            });
        }

        private OperationResult NetDetection(NetworkOption net)
        {
            if (net != NetworkOption.Auto)
                return new OperationResult(0);
            try
            {
                AppDomain.CurrentDomain.SetData("ServiceSuffix", "_CN2Net");
                var result = SystemServiceProxy.NetDetection();
                if (!result.Success)
                {
                    AppDomain.CurrentDomain.SetData("ServiceSuffix", null);
                    result = SystemServiceProxy.NetDetection();
                }
                return result;
            }
            catch
            {
                return new OperationResult(-1, "网络检测失败");
            }
        }
    }
}
