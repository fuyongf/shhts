﻿using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using Microsoft.Win32;

using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using PinganHouse.SHHTS.UI.WPFNew.Common.CommonClass;
using PinganHouse.SHHTS.UI.WPFNew.Controls;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;
using PinganHouse.SHHTS.UI.WPFNew.Views;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;


namespace PinganHouse.SHHTS.UI.WPFNew.Controllers
{
    public class PreCheckController
    {
        private readonly int PAGE_SIZE = 15;
        private StringBuilder _filePath;
        private UploadAttachmentsViewModel _uploadAttachmentsViewModel = null;
        private SearchListPageViewModel _searchListPageViewModel = null;
        private ShowMircroMessageViewModel _showMircroMessageViewModel = null;
        private ShowMircorMessageView _showMircorMessageWindow = null;
        private SeniorSearchDialog _seniorSearchDialog = null;
        private PreCheckAddCaseViewModel _preCheckAddCaseViewModel = null;
        private IList<CaseDto> _caseList = null;
        private Tuple<Seller, Buyer> _sellersAndBuyers = null;
        private readonly IUnityContainer container;
        private readonly IRegionManager regionManager;
        private readonly IEventAggregator eventAggregator;
        private CaseStatus CASE_STATUS = CaseStatus.YJ1 | CaseStatus.HA1 | CaseStatus.HA2 | CaseStatus.ZB2 | CaseStatus.ZB5;
        public PreCheckController(IUnityContainer container,
            IRegionManager regionManager,
            IEventAggregator eventAggregator)
        {
            if (container == null) throw new ArgumentNullException("container");
            if (regionManager == null) throw new ArgumentNullException("regionManager");
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            

            this.container = container;
            this.regionManager = regionManager;
            this.eventAggregator = eventAggregator;

            // Subscribe to the LoginEvents event.
            this.eventAggregator.GetEvent<BackToMainViewEvents>().Subscribe(this.OnBackToMainView, true);
            this.eventAggregator.GetEvent<LogOutEvents>().Subscribe(this.OnLogoutView, true);
            this.eventAggregator.GetEvent<AttachmentsDetailEvents>().Subscribe(this.OnAttachmentsDetail, true);
            this.eventAggregator.GetEvent<UploadAttachmentEvents>().Subscribe(this.OnUploadAttachment, true);
            this.eventAggregator.GetEvent<GaoPaiTakePhotosEvents>().Subscribe(this.OnGaoPaiTakePhotos, true);
            this.eventAggregator.GetEvent<GaoPaiUploadAttachmentEvents>().Subscribe(this.OnGaoPaiUploadAttachment, true);
            this.eventAggregator.GetEvent<NavigateToMainListEvents>().Subscribe(this.OnBackToMainView, true);
            this.eventAggregator.GetEvent<PageChangesEvents>().Subscribe(this.OnPageChangesEvents, true);
            this.eventAggregator.GetEvent<ShowMircroMessageEvents>().Subscribe(this.OnShowMircroMessageEvents, true);
            this.eventAggregator.GetEvent<ShowMircroMessageUpdateCustomerInfoEvents>().Subscribe(this.OnShowMircroMessageUpdateCustomerInfoEvents, true);
            this.eventAggregator.GetEvent<LoadPreCheckCaseListEvents>().Subscribe(this.OnLoadPreCheckCaseListEvents, true);
            this.eventAggregator.GetEvent<PreCheckSeniorSeacheEvents>().Subscribe(this.OnPreCheckSeniorSeacheEvents, true);
            this.eventAggregator.GetEvent<PreCheckCancerCaseEvents>().Subscribe(this.OnPreCheckCancerCaseEvents, true);
            this.eventAggregator.GetEvent<PreCheckPrintCaseEvents>().Subscribe(this.OnPreCheckPrintCaseEvents, true);
            this.eventAggregator.GetEvent<LoadBasicInfoByCaseIdEvents>().Subscribe(this.OnLoadBasicInfoByCaseIdEvents, true);
            this.eventAggregator.GetEvent<AddCustomerInfoEvents>().Subscribe(this.OnAddCustomerInfo, true);
            this.eventAggregator.GetEvent<UpdateCustomerInfoEvents>().Subscribe(this.OnUpdateCustomerInfo, true);
            this.eventAggregator.GetEvent<CreateNewCaseEvents>().Subscribe(this.OnCreateNewCase, true);
            this.eventAggregator.GetEvent<CreateNewCaseAndPrintBarcardEvents>().Subscribe(this.OnCreateNewCaseAndPrintBarcard, true);
            this.eventAggregator.GetEvent<PreCheckRefreshedUploadAttachementEvents>().Subscribe(this.OnPreCheckRefreshedUploadAttachement, true);
        }

        private void OnPreCheckRefreshedUploadAttachement(string materiaName)
        {
            RefreshUI(materiaName);
        }
        /// <summary>
        /// 生成案件并打印条形码
        /// </summary>
        /// <param name="obj"></param>
        private async void OnCreateNewCaseAndPrintBarcard(PreCheckAddCaseViewModel model)
        {
            IRegion loadingRegion = this.regionManager.Regions[RegionNames.LoadingRegion];
            if (loadingRegion == null) return;
            LoadingControl loadingView = loadingRegion.GetView(ViewNamesKey.LOADING_CONTROL_VIEW) as LoadingControl;
            if (loadingView == null)
            {
                //Create a new instance of the EmployeeDetailsView using the Unity container.
                loadingView = this.container.Resolve<LoadingControl>();
                //Add the view to the main region. This automatically activates the view too.
                loadingRegion.Add(loadingView, ViewNamesKey.LOADING_CONTROL_VIEW);
                loadingRegion.Activate(loadingView);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                loadingRegion.Activate(loadingView);
            }
            try
            {
                var result = await CreateCase(model);
                loadingRegion.Deactivate(loadingView);
                if (result.Success)
                {
                    string sMessage = "添加案件成功\n案件编号：" + result.ResultMessage; //案件编号
                    MessageBox.Show(sMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    //打印条码
                    var icp = new CaseCodePrint();
                    icp.PrintCaseBarCode(result.ResultMessage);
                    _preCheckAddCaseViewModel = null;//离开页面，清空本地缓存
                    OnLoadPreCheckCaseListEvents(ServiceLocator.Current.GetInstance<SearchListPageViewModel>());
                }
                else
                {
                    MessageBox.Show("添加案件失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK,
                        MessageBoxImage.Information);
                }
            }
            catch(Exception ex)
            {
                loadingRegion.Deactivate(loadingView);
                throw ex;
            }
        }

        private async void OnCreateNewCase(PreCheckAddCaseViewModel model)
        {
            IRegion loadingRegion = this.regionManager.Regions[RegionNames.LoadingRegion];
            if (loadingRegion == null) return;
            LoadingControl loadingView = loadingRegion.GetView(ViewNamesKey.LOADING_CONTROL_VIEW) as LoadingControl;
            if (loadingView == null)
            {
                //Create a new instance of the EmployeeDetailsView using the Unity container.
                loadingView = this.container.Resolve<LoadingControl>();
                //Add the view to the main region. This automatically activates the view too.
                loadingRegion.Add(loadingView, ViewNamesKey.LOADING_CONTROL_VIEW);
                loadingRegion.Activate(loadingView);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                loadingRegion.Activate(loadingView);
            }
            try
            {
                var result = await CreateCase(model);
                loadingRegion.Deactivate(loadingView);
                if (result.Success)
                {
                    string sMessage = "添加案件成功\n案件编号：" + result.ResultMessage; //案件编号
                    MessageBox.Show(sMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    _preCheckAddCaseViewModel = null;//离开页面，清空本地缓存
                    OnLoadPreCheckCaseListEvents(ServiceLocator.Current.GetInstance<SearchListPageViewModel>());
                }
                else
                {
                    MessageBox.Show("添加案件失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK,
                        MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                loadingRegion.Deactivate(loadingView);
                throw ex;
            }
        }

        private Task<OperationResult> CreateCase(PreCheckAddCaseViewModel model)
        {
            return Task.Run(() =>
            {
                OperationResult result = null;
                List<long> buyers = new List<long>();
                foreach (var item in model.BuyerInfoList)
                {
                    long userSysNo;
                    long.TryParse(Convert.ToString(item.Tag), out userSysNo);
                    buyers.Add(userSysNo);
                }
                List<long> sellers = new List<long>();
                foreach (var item in model.SellerInfoList)
                {
                    long userSysNo;
                    long.TryParse(Convert.ToString(item.Tag), out userSysNo);
                    sellers.Add(userSysNo);
                }
                var caseDto = new CaseDto()
                {
                    Buyers = buyers,
                    Sellers = sellers,
                    TenementContract = model.TbTenementContractWord.Trim() + "-" + model.TbTenementContract.Trim(), //产权证号
                    TenementAddress = model.TbTenementAddress.Trim(), //房屋座落
                    OperatorSysNo = LoginHelper.CurrentUser.SysNo, //受理人
                    AgencySysNo = model.AgentStaff.SysNo,
                    CompanySysNo = model.CompanyAgent.SysNo,
                    CreateUserSysNo = LoginHelper.CurrentUser.SysNo,
                    SourceType = (SourceType)Enum.Parse(typeof(SourceType), Convert.ToString(model.SelectCaseType))
                };

                try
                {
                    caseDto.ReceptionCenter = ConfigHelper.GetCurrentReceptionCenter();
                    result = CaseProxyService.GetInstanse().GenerateCase(caseDto);
                }
                catch (Exception ex)
                {
                    result.ResultMessage = ex.Message;
                }

                return result;
            });
        }

        private async void OnUpdateCustomerInfo(PreCheckAddCaseViewModel model)
        {
            IRegion loadingRegion = this.regionManager.Regions[RegionNames.LoadingRegion];
            if (loadingRegion == null) return;
            LoadingControl loadingView = loadingRegion.GetView(ViewNamesKey.LOADING_CONTROL_VIEW) as LoadingControl;
            if (loadingView == null)
            {
                //Create a new instance of the EmployeeDetailsView using the Unity container.
                loadingView = this.container.Resolve<LoadingControl>();
                //Add the view to the main region. This automatically activates the view too.
                loadingRegion.Add(loadingView, ViewNamesKey.LOADING_CONTROL_VIEW);
                loadingRegion.Activate(loadingView);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                loadingRegion.Activate(loadingView);
            }

            //var result = UserServiceProxy.BindUserIdentity(model.Tag, UserType.Customer, SelectUser);
            
            long userSysNo;
            await Task.Run(() =>
            {
                UserIdentityModel tempDataModel = null; 
                OperationResult result = null;
                try
                {
                    switch (model.CustomerInfoType)
                    {
                        case CustomerType.Buyer:
                            long.TryParse(Convert.ToString(model.IdCardBuyerModel.Tag), out userSysNo);
                            if (userSysNo>0)
                            {
                                tempDataModel = IdInfoViewModelToDataModel(model.IdCardBuyerModel);
                                result = UserServiceProxy.BindUserIdentity(userSysNo, UserType.Customer, tempDataModel);
                            }
                            break;
                        case CustomerType.Seller:
                            long.TryParse(Convert.ToString(model.IdCardSellerModel.Tag), out userSysNo);
                            if (userSysNo>0)
                            {
                                tempDataModel = IdInfoViewModelToDataModel(model.IdCardSellerModel);
                                result = UserServiceProxy.BindUserIdentity(userSysNo, UserType.Customer, tempDataModel);
                            }
                            break;
                    }
                }
                catch (Exception ex)
                {
                    result.ResultMessage = ex.Message;
                }
                if (!result.Success)
                {
                    MessageBox.Show(result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
            });
            var vm = _preCheckAddCaseViewModel;
            if (vm == null)
            {
                vm = ServiceLocator.Current.GetInstance<PreCheckAddCaseViewModel>();
            }
            if (model.CustomerInfoType == CustomerType.Buyer)
            {
                var tempModel = vm.BuyerInfoList.Where(x => x.Tag == model.IdCardBuyerModel.Tag).FirstOrDefault();
                if (tempModel !=null)
                {
                    int index = vm.BuyerInfoList.IndexOf(tempModel);
                    vm.BuyerInfoList.RemoveAt(index);
                    vm.BuyerInfoList.Insert(index, model.IdCardBuyerModel);
                    vm.IdCardBuyerModel = new IdInfoModel();
                    vm.IndexBuyerSelect = 0;
                }
            }
            else
            {
                var tempModel = vm.SellerInfoList.Where(x => x.Tag == model.IdCardSellerModel.Tag).FirstOrDefault();
                if (tempModel != null)
                {
                    int index = vm.SellerInfoList.IndexOf(tempModel);
                    vm.SellerInfoList.RemoveAt(index);
                    vm.SellerInfoList.Insert(index, model.IdCardSellerModel);
                    vm.IdCardSellerModel = new IdInfoModel();
                    vm.IndexSellerSelect = 0;
                }
            }
            _preCheckAddCaseViewModel = vm;
            await Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                IRegion mainRegion = this.regionManager.Regions[RegionNames.MainRegion];
                if (mainRegion == null) return;
                AddCaseView view = mainRegion.GetView(ViewNamesKey.ADD_CASE_VIEW) as AddCaseView;
                if (view == null)
                {
                    //Create a new instance of the EmployeeDetailsView using the Unity container.
                    view = this.container.Resolve<AddCaseView>();
                    //Add the view to the main region. This automatically activates the view too.
                    mainRegion.Add(view, ViewNamesKey.ADD_CASE_VIEW);
                    mainRegion.Activate(view);
                    view.ViewModel = _preCheckAddCaseViewModel;
                }
                else
                {
                    //The view has already been added to the region so just activate it.
                    mainRegion.Activate(view);
                    view.ViewModel = _preCheckAddCaseViewModel;
                }
            }), System.Windows.Threading.DispatcherPriority.Background);
            loadingRegion.Deactivate(loadingView);
        }

        private async void OnAddCustomerInfo(PreCheckAddCaseViewModel model)
        {
            IRegion loadingRegion = this.regionManager.Regions[RegionNames.LoadingRegion];
            if (loadingRegion == null) return;
            LoadingControl loadingView = loadingRegion.GetView(ViewNamesKey.LOADING_CONTROL_VIEW) as LoadingControl;
            if (loadingView == null)
            {
                //Create a new instance of the EmployeeDetailsView using the Unity container.
                loadingView = this.container.Resolve<LoadingControl>();
                //Add the view to the main region. This automatically activates the view too.
                loadingRegion.Add(loadingView, ViewNamesKey.LOADING_CONTROL_VIEW);
                loadingRegion.Activate(loadingView);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                loadingRegion.Activate(loadingView);
            }
            OperationResult result = null;
            await Task.Run(() =>
            {
                UserIdentityModel tempDataModel = null; 
                try
                {
                    switch (model.CustomerInfoType)
                    {
                        case CustomerType.Buyer:
                            tempDataModel = IdInfoViewModelToDataModel(model.IdCardBuyerModel);
                            result = CustomerServiceProxy.CreateBuyer(tempDataModel, LoginHelper.CurrentUser.SysNo);
                            model.IdCardBuyerModel.Tag = result.OtherData["UserSysNo"];
                            break;
                        case CustomerType.Seller:
                            tempDataModel = IdInfoViewModelToDataModel(model.IdCardSellerModel);
                            result = CustomerServiceProxy.CreateSeller(tempDataModel, LoginHelper.CurrentUser.SysNo);
                            model.IdCardSellerModel.Tag = result.OtherData["UserSysNo"];
                            break;
                    }
                }
                catch (Exception ex)
                {
                    result.ResultMessage = ex.Message;
                }
            });

            if (!result.Success)
            {
                MessageBox.Show(result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                loadingRegion.Deactivate(loadingView);
                return;
            }  
                
            switch (model.CustomerInfoType)
            {
                case CustomerType.Buyer:
                    Gaopaiyi(model.IndexBuyerSelect, result);
                    break;
                case CustomerType.Seller:
                    Gaopaiyi(model.IndexSellerSelect, result);
                    break;
            }
                       
            loadingRegion.Deactivate(loadingView);
            if (_preCheckAddCaseViewModel == null)
            {
                _preCheckAddCaseViewModel = model;
            }
            if (model.CustomerInfoType == CustomerType.Buyer)
            {
                _preCheckAddCaseViewModel.BuyerInfoList.Add(model.IdCardBuyerModel);
                _preCheckAddCaseViewModel.IdCardBuyerModel = new IdInfoModel();
                _preCheckAddCaseViewModel.IndexBuyerSelect = 0;
            }
            else
            {
                _preCheckAddCaseViewModel.SellerInfoList.Add(model.IdCardSellerModel);
                _preCheckAddCaseViewModel.IdCardSellerModel = new IdInfoModel();
                _preCheckAddCaseViewModel.IndexSellerSelect = 0;
            }

            ////await Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            ////{
            //IRegion mainRegion = this.regionManager.Regions[RegionNames.MainRegion];
            //if (mainRegion == null) return;
            //AddCaseView view = mainRegion.GetView(ViewNamesKey.ADD_CASE_VIEW) as AddCaseView;
            //if (view == null)
            //{
            //    //Create a new instance of the EmployeeDetailsView using the Unity container.
            //    view = this.container.Resolve<AddCaseView>();
            //    //Add the view to the main region. This automatically activates the view too.
            //    mainRegion.Add(view, ViewNamesKey.ADD_CASE_VIEW);
            //    mainRegion.Activate(view);
            //    view.ViewModel = _preCheckAddCaseViewModel;
            //}
            //else
            //{
            //    //The view has already been added to the region so just activate it.
            //    mainRegion.Activate(view);
            //    view.ViewModel = _preCheckAddCaseViewModel;
            //}
            ////}), System.Windows.Threading.DispatcherPriority.Background);=     
        }
        /// <summary>
        /// 弹出高拍仪
        /// </summary>
        /// <param name="index"></param>
        /// <param name="result"></param>
        private static void Gaopaiyi(int index, OperationResult result)
        {
            if (index.Equals((int)CertificateType.IdCard))
            {
                string idSym = result.OtherData["UserSysNo"].ToString();
                MessageBoxResult resultBox = MessageBox.Show("是否上传身份证页面", "系统提示", MessageBoxButton.OKCancel,
                    MessageBoxImage.Question);
                if (resultBox == MessageBoxResult.OK)
                {
                    //TODO:弹出高拍仪页面                
                    CaptureImgeDialog windouw = new CaptureImgeDialog(idSym, AttachmentType.身份证);
                    windouw.ShowDialog();
                }
            }
        }

        private UserIdentityModel IdInfoViewModelToDataModel(IdInfoModel model)
        {
            var tempDataModel = new UserIdentityModel();
            tempDataModel.Address = model.IdAddress;
            if(model.IdBirth==null)
            {
                tempDataModel.Birthday = null;
            }
            else
            {
                tempDataModel.Birthday = Convert.ToDateTime(model.IdBirth);
            }
            tempDataModel.CertificateType = model.CertificateType;
            if (model.EffectiveDate != null)
            {
                tempDataModel.EffectiveDate = model.CertificateType== CertificateType.IdCard ? Convert.ToDateTime(model.EffectiveDate) : new DateTime(int.Parse(model.EffectiveDate), 1, 1);
            }
            else
                tempDataModel.EffectiveDate = null;
            if (model.EffectiveDate != null && model.ExpiryDate != "长期")
            {
                tempDataModel.ExpiryDate = model.CertificateType == CertificateType.IdCard ? Convert.ToDateTime(model.ExpiryDate) : new DateTime(int.Parse(model.ExpiryDate), 1, 1);
            }
            else
            {
                tempDataModel.ExpiryDate = null;
            }
            if (string.IsNullOrEmpty(model.IdSex))
            {
                if (model.Male)
                {
                    tempDataModel.Gender = Gender.Male;
                }
                else if (model.Female)
                {
                    tempDataModel.Gender = Gender.Female;
                }
                else
                {
                    tempDataModel.Gender = Gender.Unknown;
                }
            }
            else
            {
                switch (model.IdSex)
                {
                    case "男":
                        tempDataModel.Gender = Gender.Male;
                        break;
                    case "女":
                        tempDataModel.Gender = Gender.Female;
                        break;
                    default:
                        tempDataModel.Gender = Gender.Unknown;
                        break;
                }
            }
            tempDataModel.IdentityNo = model.IdNum;
            tempDataModel.IsTrusted = model.IsTrusted;
            tempDataModel.Name = model.IdName;
            if (model.IdNation != null)
            {
                tempDataModel.Nation = (Nation)Nation.Parse(typeof(Nation), model.IdNation);
            }
            tempDataModel.Nationality = model.Nationality;         
            tempDataModel.Nationality = model.Nationality;  
            tempDataModel.Tag = model.Tag;
            tempDataModel.VisaAgency = model.IdAgent;
            return tempDataModel;
        }
        private IdInfoModel IdInfoDataModelToViewModel(UserIdentityModel model)
        {
            var tempViewModel = new IdInfoModel();
            tempViewModel.IdAddress = model.Address;
            tempViewModel.IdBirth =Convert.ToString(model.Birthday);
            tempViewModel.CertificateType = model.CertificateType;
            if (model.EffectiveDate != null)
            {
                tempViewModel.EffectiveDate = Convert.ToString(model.EffectiveDate);
            }
            else
                tempViewModel.EffectiveDate = null;
            if (model.EffectiveDate != null)
            {
                tempViewModel.ExpiryDate = Convert.ToString(model.ExpiryDate);
            }
            else
            {
                tempViewModel.ExpiryDate = null;
            }
            switch(model.Gender)
            {
                case Gender.Male:
                    tempViewModel.Male = true;
                    tempViewModel.Female = false;
                    if (model.CertificateType == CertificateType.IdCard)
                    {
                        tempViewModel.IdSex = "男";
                    }
                    else
                    {
                        tempViewModel.IdSex = string.Empty;
                    }
                    break;
                case Gender.Female:
                    tempViewModel.Male = false;
                    tempViewModel.Female = true;
                    if (model.CertificateType == CertificateType.IdCard)
                    {
                        tempViewModel.IdSex = "女";
                    }
                    else
                    {
                        tempViewModel.IdSex = string.Empty;
                    }
                    break;
                default:
                    tempViewModel.Female=false;
                    tempViewModel.Male=false;
                    tempViewModel.IdSex=string.Empty;
                    break;
            }
            tempViewModel.IdNum = model.IdentityNo;
            tempViewModel.IsTrusted = model.IsTrusted;
            tempViewModel.IdName = model.Name;
            //if (model.Nation != null)
            //{
            //    tempViewModel.IdNation = (Nation)Nation.Parse(typeof(Nation), model.IdNation);
            //}
            tempViewModel.Nationality = model.Nationality;
            if (model.Photo != null)
            {
                tempViewModel.Bitmap = ByteToBmp(model.Photo);
            }
            tempViewModel.Tag = model.Tag;
            tempViewModel.IdAgent = model.VisaAgency;
            return tempViewModel;
        }
        private BitmapImage ByteToBmp(byte[] bytes)
        {
            BitmapImage bitmap = new BitmapImage();
            try
            {
                MemoryStream ms1 = new MemoryStream(bytes);
                bitmap.BeginInit();
                bitmap.StreamSource = ms1;
                bitmap.EndInit();

            }
            catch (Exception e)
            {
                MessageBox.Show("图像转化失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return bitmap;
        }
        public byte[] Bitmapimagetobytearray(BitmapImage bmp)
        {
            byte[] bytearray = null;
            try
            {
                Stream smarket = bmp.StreamSource;
                if (smarket != null && smarket.Length > 0)
                {
                    //很重要，因为position经常位于stream的末尾，导致下面读取到的长度为0。
                    smarket.Position = 0;
                    using (BinaryReader br = new BinaryReader(smarket))
                    {
                        bytearray = br.ReadBytes((int)smarket.Length);
                    }
                }            
            }
            catch
            {
                MessageBox.Show("头像转化失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return bytearray;
        }
        private void OnGaoPaiUploadAttachment(GaoPaiPhotosViewModel gaoPaiPhotosViewModel)
        {
            foreach (var captureImg in gaoPaiPhotosViewModel.Photos)
            {
                try
                {
                    if (captureImg.IsChecked)
                    {
                        FileStream AttachFile;
                        using (AttachFile = new FileStream(captureImg.Tag, FileMode.OpenOrCreate, FileAccess.Read))
                        {
                            byte[] fileByte = new byte[captureImg.ImageSource.StreamSource.Length];
                            captureImg.ImageSource.StreamSource.Read(fileByte, 0, fileByte.Length);

                            AttachFile.Read(fileByte, 0, fileByte.Length);
                        }
                        CaseHelper.AddAttachment(gaoPaiPhotosViewModel.Id, captureImg.Tag, gaoPaiPhotosViewModel.Type, captureImg.ImageSource.StreamSource, Comlpate);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                    continue;
                }
            }

        }

        private void OnGaoPaiTakePhotos(AttachmentType attachmentType)
        {
            string id = string.Empty;
            var tempViewModel = _uploadAttachmentsViewModel;
            //对于卖方和买方需要先选择人
            if (sellerAttachment.Contains(attachmentType) && tempViewModel.CurrentIndexTabItem == 0)
            {
                if (tempViewModel.CurrentItem == null)
                {
                    MessageBox.Show("请选择所属人", "系统提示", MessageBoxButton.OK, MessageBoxImage.Stop);
                    return;
                }
                id = tempViewModel.CurrentCustomer.CustomerId;
            }
            else if (buyerAttachment.Contains(attachmentType) && tempViewModel.CurrentIndexTabItem == 1)
            {
                if (tempViewModel.BuyCurrentItem == null)
                {
                    MessageBox.Show("请选择所属人", "系统提示", MessageBoxButton.OK, MessageBoxImage.Stop);
                    return;
                }
                id = tempViewModel.BuyCurrentCustomer.CustomerId;
            }
            else if (attachmentType == AttachmentType.产调证 || attachmentType == AttachmentType.产证)
            {
                id = _uploadAttachmentsViewModel.HouseContractId;
            }
            else
            {
                id = tempViewModel.CaseId;
            }
            try
            {
                var type = attachmentType;

                CaptureImgeDialog windouw = new CaptureImgeDialog(id, attachmentType);
                windouw.ShowDialog();
                RefreshUI();
            }
            catch (Exception ex)
            {
                MessageBox.Show("本地上传失败原因：" + ex.Message);
            }

        }

        

        private void OnPreCheckCancerCaseEvents(string caseId)
        {
            MessageBoxResult temp=MessageBox.Show("确认要取消签约吗？", "提示", MessageBoxButton.YesNo);
            if (temp == MessageBoxResult.Yes)
            {
                var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.ZB5, LoginHelper.CurrentUser.SysNo);
                if (result.Success)
                {
                    MessageBox.Show("取消签约成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("取消签约失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                OnLoadPreCheckCaseListEvents(_searchListPageViewModel);
            }
            //以下为第一版的弹框，暂时不做
            ////案件编号
            //var cancelContractDialog = new CancelContractDialog(caseId);
            //bool? dialogResult = cancelContractDialog.ShowDialog();
            //if (dialogResult.HasValue && dialogResult.Value)
            //{
            //    var dic = new Dictionary<string, object>();
            //    dic.Add("Reason", cancelContractDialog.Reason);
            //    var result = CaseProxyService.GetInstanse().UpdateCaseStatus(caseId, CaseStatus.ZB5, LoginHelper.CurrentUser.SysNo, dic);
            //    if (result.Success)
            //    {
            //        MessageBox.Show("取消签约成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
            //    }
            //    else
            //    {
            //        MessageBox.Show("取消签约失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            //    }
            //}
        }

        private void OnPreCheckPrintCaseEvents(string caseId)
        {
            try
            {             
               //List<PrintModel> _ListPrint = new List<PrintModel>();
               //#region 获得本机所有打印机
               //var printers = PrinterSettings.InstalledPrinters;//获取本机上的所有打印机             
               //PrintModel pm = new PrintModel();
               //pm.PrintNo = 0;
               //pm.PrintName = "未分配";
               //_ListPrint.Add(pm);
               //for (int i = 0; i < printers.Count; i++)
               //{
               //    PrintModel print = new PrintModel();
               //    print.PrintNo = i + 1;
               //    print.PrintName = printers[i].ToString();
               //    _ListPrint.Add(print);
               //}
               //#endregion
               ////从注册表加载 条形码打印机信息          
               //var  _ReceiptPrintName = PrivateProfileUtils.GetContentValue("BarCodePrint", "PrintName"); //打印机名
               // #region 条形码打印机信息
               // int printCount = 0;

               // if (!string.IsNullOrWhiteSpace(_ReceiptPrintName))
               // {
               //     for (int j = 0; j < _ListPrint.Count; j++)
               //     {
               //         if (_ReceiptPrintName == _ListPrint[j].PrintName)
               //         {
               //             printCount++;
               //         }
               //     }
               // }
               // else //为空，则 收据打印机未分配
               // {
               //     MessageBox.Show("该条形码打印机未设置，无法进行打印操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
               //     return;
               // }
               // if (printCount == 0)
               // {
               //     MessageBox.Show("该条形码打印机未设置，无法进行打印操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
               //     return;
               // }
               // if (_ReceiptPrintName.Contains("未分配"))
               // {
               //     MessageBox.Show("该条形码打印机未设置，无法进行打印操作.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
               //     return;
               // }
               // #endregion
               // //案件编号             
               // var icp = new CaseCodePrint();
                string errorMessage = string.Empty;
                SettingPage settingPrint = new SettingPage();
                var result=settingPrint.SetCodePrint(caseId, out errorMessage);
                if (!result)
                    MessageBox.Show(errorMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                //icp.PrintCaseBarCode(caseId);
            }
            catch (Exception ex)
            {
                MessageBox.Show("打印错误：" + ex.Message, "消息提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void OnPreCheckSeniorSeacheEvents(object obj)
        {
            var window = new SeniorSearchDialog(ServiceLocator.Current.GetInstance<CaseDetailInfoViewModel>());
            _seniorSearchDialog = window;
            window.ShowDialog();
        }

        private async void OnLoadPreCheckCaseListEvents(SearchListPageViewModel viewModel)
        {
            IRegion loadingRegion = this.regionManager.Regions[RegionNames.LoadingRegion];
            if (loadingRegion == null) return;
            LoadingControl loadingView = loadingRegion.GetView(ViewNamesKey.LOADING_CONTROL_VIEW) as LoadingControl;
            if (loadingView == null)
            {
                //Create a new instance of the EmployeeDetailsView using the Unity container.
                loadingView = this.container.Resolve<LoadingControl>();
                //Add the view to the main region. This automatically activates the view too.
                loadingRegion.Add(loadingView, ViewNamesKey.LOADING_CONTROL_VIEW);
                loadingRegion.Activate(loadingView);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                loadingRegion.Activate(loadingView);
            }

            //把覆盖在最上层的window给关掉
            if (_seniorSearchDialog != null &&
                _seniorSearchDialog.IsActive)
            {
                _seniorSearchDialog.Close();
            }
           
            await Task.Run(() =>
            {

                //获取案件列表数据
                int cnt = 0;
                int pageCount = 0;
                var list = EnumHelper.InitCaseStatusToCombobox(CASE_STATUS);

                //第一次进来，添加状态，搜索条件信息，否则就只更新list列表信息，其他信息用缓存的内容
                SearchListPageViewModel searchListPageViewModel = ServiceLocator.Current.GetInstance<SearchListPageViewModel>();
                if (_searchListPageViewModel == null)
                {
                    var conditionList = new List<string>()
                {
                    "案件编号","物业地址","主办人","买方姓名","经纪人"
                };
                    if (viewModel != null)
                    {
                        searchListPageViewModel.SearchCaseCondition = viewModel.SearchCaseCondition;
                    }
                    searchListPageViewModel.CaseList = GetPreCheckCaseList(1, searchListPageViewModel.SearchCaseCondition, CASE_STATUS, out cnt);
                    foreach (var item in list)
                    {
                        searchListPageViewModel.StatusList.Add(item);
                    }
                    searchListPageViewModel.ConditionList = conditionList;
                    _searchListPageViewModel = searchListPageViewModel;
                }
                else
                {
                    if (viewModel != null)
                    {
                        _searchListPageViewModel.SearchCaseCondition = viewModel.SearchCaseCondition;
                    }
                    _searchListPageViewModel.IsPageChanged = false;
                    _searchListPageViewModel.CurrentPageIndex = 1;
                    _searchListPageViewModel.CaseList = GetPreCheckCaseList(1, _searchListPageViewModel.SearchCaseCondition, CASE_STATUS, out cnt);
                }
                if (cnt % PAGE_SIZE != 0)
                {
                    pageCount = cnt / PAGE_SIZE + 1;
                }
                else
                {
                    pageCount = cnt / PAGE_SIZE;
                }
                _searchListPageViewModel.TotalPageCount = pageCount;

            });

            RefreshPreCheckSearchListPage(_searchListPageViewModel);
            loadingRegion.Deactivate(loadingView);
        }

        private ObservableCollection<CaseDetailInfoViewModel> GetPreCheckCaseList(int page,CaseDetailInfoViewModel model, CaseStatus status, out int cnt)
        {
            IList<CaseDto> caseList = null;
            if (model == null)
            {
                caseList = CaseProxyService.GetInstanse().GetPaginatedList(page, PAGE_SIZE, out cnt, ConfigHelper.GetCurrentReceptionCenter(), status, null,
                    null, null, null, null, null, null, operationStatus: CaseStatus.HA1, creatorName: null);
            }
            else
            {
                if (model.Status == CaseStatus.Unkown || model.Status == CASE_STATUS)
                {
                    model.Status = null;
                }
                caseList = CaseProxyService.GetInstanse().GetPaginatedList(page, PAGE_SIZE, out cnt, ConfigHelper.GetCurrentReceptionCenter(), status,model.Status,
                    model.CaseId, model.Address, model.BuyerName, model.AgentPerson, model.StartTime, model.EndTime, operationStatus: CaseStatus.HA1, creatorName: null);
            }
            _caseList = caseList;
            ObservableCollection<CaseDetailInfoViewModel> caseViewModelList = new ObservableCollection<CaseDetailInfoViewModel>();
            if (caseList != null && caseList.Count > 0)
            {
                foreach (CaseDto Dto in caseList)
                {
                    CaseDetailInfoViewModel caseDetailInfoViewModel = ServiceLocator.Current.GetInstance<CaseDetailInfoViewModel>();
                    caseDetailInfoViewModel.CaseId = Dto.CaseId;
                    caseDetailInfoViewModel.AgentCompany = Dto.CompanyName;
                    caseDetailInfoViewModel.AgentPerson = Dto.AgencyName;
                    caseDetailInfoViewModel.BuyerName = Dto.BuyerName;
                    caseDetailInfoViewModel.Address = Dto.TenementAddress;
                    caseDetailInfoViewModel.CreateTime = Dto.CreateDate;
                    caseDetailInfoViewModel.SignedTime = Dto.CreateDate;
                    caseDetailInfoViewModel.SponsorName = Dto.CreatorName;
                    caseDetailInfoViewModel.AcceptorName = Dto.OperatorName;
                    caseDetailInfoViewModel.Status = Dto.CaseStatus;
                    caseViewModelList.Add(caseDetailInfoViewModel);
                }
            }
            return caseViewModelList;
        }

        private void OnShowMircroMessageUpdateCustomerInfoEvents(ShowMircroMessageViewModel showMircroMessageViewMode)
        {
            IDictionary<long, string> sellMoblies = new Dictionary<long, string>();
            IDictionary<long, string> sellEmails = new Dictionary<long, string>(); 
            IDictionary<long, string> buyMoblies = new Dictionary<long, string>();
            IDictionary<long, string> buyEmails = new Dictionary<long, string>();
            if (_sellersAndBuyers != null)
            {
                foreach (var item in _sellersAndBuyers.Item2.Customers)
                {
                    //该接口保存的时候要求电话号码和邮箱地址都不能为空
                    if (!string.IsNullOrEmpty(showMircroMessageViewMode.BuyerList.Where(x => x.CustomerId == item.UserSysNo.ToString()).FirstOrDefault().PhoneNo)
                        && !string.IsNullOrEmpty(showMircroMessageViewMode.BuyerList.Where(x => x.CustomerId == item.UserSysNo.ToString()).FirstOrDefault().EmailAddress))
                    {
                        buyMoblies.Add(item.SysNo, showMircroMessageViewMode.BuyerList.Where(x => x.CustomerId == item.UserSysNo.ToString()).FirstOrDefault().PhoneNo);
                        buyEmails.Add(item.SysNo, showMircroMessageViewMode.BuyerList.Where(x => x.CustomerId == item.UserSysNo.ToString()).FirstOrDefault().EmailAddress);
                    }
                }
                foreach (var item in _sellersAndBuyers.Item1.Customers)
                {
                    //该接口保存的时候要求电话号码和邮箱地址都不能为空
                    if (!string.IsNullOrEmpty(showMircroMessageViewMode.SellerList.Where(x => x.CustomerId == item.UserSysNo.ToString()).FirstOrDefault().PhoneNo)
                        && !string.IsNullOrEmpty(showMircroMessageViewMode.SellerList.Where(x => x.CustomerId == item.UserSysNo.ToString()).FirstOrDefault().EmailAddress))
                    {
                        sellMoblies.Add(item.SysNo, showMircroMessageViewMode.SellerList.Where(x => x.CustomerId == item.UserSysNo.ToString()).FirstOrDefault().PhoneNo);
                        sellEmails.Add(item.SysNo, showMircroMessageViewMode.SellerList.Where(x => x.CustomerId == item.UserSysNo.ToString()).FirstOrDefault().EmailAddress);
                    }
                }
                var resultBuy = CaseProxyService.GetInstanse().PerfectBuyer(showMircroMessageViewMode.CaseId, _sellersAndBuyers.Item2, buyMoblies, buyEmails, LoginHelper.CurrentUser.SysNo);
                var resultSell = CaseProxyService.GetInstanse().PrefectSeller(showMircroMessageViewMode.CaseId, _sellersAndBuyers.Item1, sellMoblies, sellEmails, LoginHelper.CurrentUser.SysNo);
                if (resultBuy.Success && resultSell.Success)
                {
                    MessageBox.Show("保存成功", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("保存失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
                if(_showMircorMessageWindow.IsActive)
                {
                    _showMircorMessageWindow.Close();
                }
            }
            
        }
        private Tuple<ObservableCollection<CustomerInfoViewModel>, ObservableCollection<CustomerInfoViewModel>> GetCustomerInfoByCaseId(string caseId)
        {
            Tuple<ObservableCollection<CustomerInfoViewModel>, ObservableCollection<CustomerInfoViewModel>> sb = null;
            var buyerList = new ObservableCollection<CustomerInfoViewModel>();
            var sellerList = new ObservableCollection<CustomerInfoViewModel>();
            Tuple<Seller, Buyer> sellersAndBuyers = CaseProxyService.GetInstanse().GetCaseCustomers(caseId);
            _sellersAndBuyers = sellersAndBuyers;
            if (sellersAndBuyers == null)
            {
                return sb;
            }
            if (sellersAndBuyers.Item1.Customers != null && sellersAndBuyers.Item1.Customers.Count > 0)
            {
                foreach (Customer item in sellersAndBuyers.Item1.Customers)
                {
                    var customer = ServiceLocator.Current.GetInstance<CustomerInfoViewModel>();
                    customer.CustomerId = item.UserSysNo.ToString();
                    customer.CustomerName = item.RealName;
                    customer.CertificateName = item.CertificateType;
                    customer.CertificateNo = item.IdentityNo;
                    customer.IsBindingMircroMessage = item.IsConcern;
                    customer.PhoneNo = item.Mobile;
                    customer.EmailAddress = item.Email;
                    sellerList.Add(customer);
                }
            }
            if (sellersAndBuyers.Item2.Customers != null && sellersAndBuyers.Item2.Customers.Count > 0)
            {
                foreach (Customer item in sellersAndBuyers.Item2.Customers)
                {
                    var customer = ServiceLocator.Current.GetInstance<CustomerInfoViewModel>();
                    customer.CustomerId = item.UserSysNo.ToString();
                    customer.CustomerName = item.RealName;
                    customer.CertificateName = item.CertificateType;
                    customer.CertificateNo = item.IdentityNo;
                    customer.IsBindingMircroMessage = item.IsConcern;
                    customer.PhoneNo = item.Mobile;
                    customer.EmailAddress = item.Email;
                    buyerList.Add(customer);
                }
            }
            sb = new Tuple<ObservableCollection<CustomerInfoViewModel>, ObservableCollection<CustomerInfoViewModel>>(sellerList, buyerList);
            return sb;
        }

        private void OnShowMircroMessageEvents(string caseId)
        {
            var buyerList = new ObservableCollection<CustomerInfoViewModel>();
            var sellerList = new ObservableCollection<CustomerInfoViewModel>();
            var sb = GetCustomerInfoByCaseId(caseId);
            if(sb !=null)
            {
                sellerList = sb.Item1;
                buyerList = sb.Item2;
            } 
            ShowMircroMessageViewModel viewModel = ServiceLocator.Current.GetInstance<ShowMircroMessageViewModel>();
            viewModel.CaseId = caseId;
            viewModel.SellerList = sellerList;
            viewModel.BuyerList = buyerList;
            _showMircroMessageViewModel = viewModel;
           var window = new ShowMircorMessageView(viewModel);
           window.ViewModel = viewModel;
           _showMircorMessageWindow = window;
           window.ShowDialog();
        }
        
        /// <summary>
        /// Called when a new employee is selected. This method uses
        /// view injection to programmatically 
        /// </summary>
        private void OnBackToMainView(object viewModel)
        {
            IRegion mainRegion = this.regionManager.Regions[RegionNames.MainRegion];
            if (mainRegion == null) return;
            MainView view = mainRegion.GetView(ViewNamesKey.MAIN_VIEW) as MainView;
            if (view == null)
            {
                 //Create a new instance of the EmployeeDetailsView using the Unity container.
                view = this.container.Resolve<MainView>();

                 //Add the view to the main region. This automatically activates the view too.
                mainRegion.Add(view, ViewNamesKey.MAIN_VIEW);
                mainRegion.Activate(view);
            }
            else
            {
                 //The view has already been added to the region so just activate it.
                mainRegion.Activate(view);
            }
        }
        /// <summary>
        /// 注销登录
        /// </summary>
        /// <param name="viewModel"></param>
        private void OnLogoutView(object viewModel)
        {           
            //清除登录数据
            IRegion mainRegion = this.regionManager.Regions[RegionNames.MainRegion];
            if (mainRegion == null) return;
            LoginView view = mainRegion.GetView(ViewNamesKey.LOGIN_VIEW) as LoginView;
            if (view == null)
            {
                //Create a new instance of the EmployeeDetailsView using the Unity container.
                view = this.container.Resolve<LoginView>();
                //Add the view to the main region. This automatically activates the view too.
                mainRegion.Add(view, ViewNamesKey.LOGIN_VIEW);
                mainRegion.Activate(view);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                mainRegion.Activate(view);
            }
            view.ViewModel.Password = string.Empty;

            var LogView = ServiceLocator.Current.GetInstance<LoginViewModel>();
            IRegion titleRegion = this.regionManager.Regions[RegionNames.TitleRegion];
            if (titleRegion == null) return;
            AreaSelectedView titleView = titleRegion.GetView(ViewNamesKey.AREA_SELECTED_VIEW) as AreaSelectedView;
            if (titleView == null)
            {
                //Create a new instance of the EmployeeDetailsView using the Unity container.
                titleView = this.container.Resolve<AreaSelectedView>();
                titleView.DataContext = viewModel;
                //Add the view to the main region. This automatically activates the view too.
                titleRegion.Add(titleView, ViewNamesKey.AREA_SELECTED_VIEW);
                titleRegion.Activate(titleView);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                titleView.DataContext = LogView;
                titleRegion.Activate(titleView);
            }
            var list = EnumHelper.InitReceptionCenterToCombobox();
            list.RemoveAt(0);
            foreach (var item in list)
            {
                LogView.AreaList.Add(item);
            }
            var rememberArea = PrivateProfileUtils.GetContentValue("Area", "SH");
            if (!string.IsNullOrWhiteSpace(rememberArea))
                LogView.CurrentIndex = int.Parse(rememberArea);
            if (LogView.CurrentIndex == -1)
            {
                AreaSelectedDialog windows = new AreaSelectedDialog(LogView);
                windows.ShowDialog();
            }
        }

        private async void OnPageChangesEvents(int index)
        {
            IRegion loadingRegion = this.regionManager.Regions[RegionNames.LoadingRegion];
            if (loadingRegion == null) return;
            LoadingControl loadingView = loadingRegion.GetView(ViewNamesKey.LOADING_CONTROL_VIEW) as LoadingControl;
            if (loadingView == null)
            {
                //Create a new instance of the EmployeeDetailsView using the Unity container.
                loadingView = this.container.Resolve<LoadingControl>();
                //Add the view to the main region. This automatically activates the view too.
                loadingRegion.Add(loadingView, ViewNamesKey.LOADING_CONTROL_VIEW);
                loadingRegion.Activate(loadingView);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                loadingRegion.Activate(loadingView);
            }
            await Task.Run(() =>
            {             
                //获取案件列表数据
                int cnt = 0;
                int pageCount = 0;
                _searchListPageViewModel.CaseList = GetPreCheckCaseList(index, _searchListPageViewModel.SearchCaseCondition, CASE_STATUS, out cnt);
                if (cnt % PAGE_SIZE != 0)
                {
                    pageCount = cnt / PAGE_SIZE + 1;
                }
                else
                {
                    pageCount = cnt / PAGE_SIZE;
                }
                _searchListPageViewModel.TotalPageCount = pageCount;
            });

            loadingRegion.Deactivate(loadingView);
            RefreshPreCheckSearchListPage(_searchListPageViewModel);
        }

        private void RefreshPreCheckSearchListPage(SearchListPageViewModel viewModel)
        {
            IRegion mainRegion = this.regionManager.Regions[RegionNames.MainRegion];
            if (mainRegion == null) return;
            SearchListPageView view = mainRegion.GetView(ViewNamesKey.SEARCH_LIST_PAGE_VIEW) as SearchListPageView;
            if (view == null)
            {
                //Create a new instance of the EmployeeDetailsView using the Unity container.
                view = this.container.Resolve<SearchListPageView>();
                //Add the view to the main region. This automatically activates the view too.
                mainRegion.Add(view, ViewNamesKey.SEARCH_LIST_PAGE_VIEW);
                mainRegion.Activate(view);
                view.ViewModel = viewModel;
            }
            else
            {
                //The view has already been added to the region so just activate it.
                mainRegion.Activate(view);
                view.ViewModel = viewModel;
            }
        }

        private void OnNavigateToMainListEvents(object o)
        {
            //RefreshPreCheckSearchListPage(1);
        }


        private void OnAttachmentsDetail(string caseId)
        {
            EnterCaseDetailInfoPage(caseId, false, true);
        }
        private void OnLoadBasicInfoByCaseIdEvents(string caseId)
        {
            EnterCaseDetailInfoPage(caseId, true, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="caseId">案件编号</param>
        /// <param name="isSelectBasicInfo">定位基本信息页面</param>
        /// <param name="isSelectUploadAttachment">定位附件上传页面</param>
        private async void EnterCaseDetailInfoPage(string caseId, bool isSelectBasicInfo = false, bool isSelectUploadAttachment = false)
        {
            IRegion loadingRegion = this.regionManager.Regions[RegionNames.LoadingRegion];
            if (loadingRegion == null) return;
            LoadingControl loadingView = loadingRegion.GetView(ViewNamesKey.LOADING_CONTROL_VIEW) as LoadingControl;
            if (loadingView == null)
            {
                //Create a new instance of the EmployeeDetailsView using the Unity container.
                loadingView = this.container.Resolve<LoadingControl>();
                //Add the view to the main region. This automatically activates the view too.
                loadingRegion.Add(loadingView, ViewNamesKey.LOADING_CONTROL_VIEW);
                loadingRegion.Activate(loadingView);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                loadingRegion.Activate(loadingView);
            }

            UploadAttachmentsViewModel vm = ServiceLocator.Current.GetInstance<UploadAttachmentsViewModel>();
            vm.CaseId = caseId;
            vm = await GetUploadAttachmentsViewModel(vm, isSelectBasicInfo, isSelectUploadAttachment);
            

            IRegion mainRegion = this.regionManager.Regions[RegionNames.MainRegion];
            if (mainRegion == null) return;
            UploadAttachmentView view = mainRegion.GetView(ViewNamesKey.UPLOAD_ATTACHEMENT_VIEW) as UploadAttachmentView;
            if (view == null)
            {
                //Create a new instance of the EmployeeDetailsView using the Unity container.
                view = this.container.Resolve<UploadAttachmentView>();
                //Add the view to the main region. This automatically activates the view too.
                mainRegion.Add(view, ViewNamesKey.UPLOAD_ATTACHEMENT_VIEW);
                mainRegion.Activate(view);
                view.ViewModel = vm;
            }
            else
            {
                //The view has already been added to the region so just activate it.
                mainRegion.Activate(view);
                view.ViewModel = vm;
            }

            loadingRegion.Deactivate(loadingView);

        }
        public Task<UploadAttachmentsViewModel> GetUploadAttachmentsViewModel(UploadAttachmentsViewModel tempViewModel, bool isSelectBasicInfo, bool isSelectUploadAttachment)
        {
            //获取案件的附件材料，分为3中情况：
            //1.根据案件编号获取相关的卖方和买方，然后相关人员都是根据人员id来获取相关的附件材料
            //2.权利凭证中的"产证编号"根据"房屋编号"获取
            //3.其他都是根据案件编号获取。
            //根据案件编号获取相关买方和卖方人员列表
            UploadAttachmentsViewModel viewModel = tempViewModel;
            return Task.Run(() =>
            {
                // List<CaseStatus> caseStatus=new List<CaseStatus>();        //状态集合
                //int totalCount;
                var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(viewModel.CaseId);
                var caseDtoStatus = CaseProxyService.GetInstanse().GetCaseEvents(viewModel.CaseId, null ,null);
                if (caseDto != null)
                {
                    #region 买方人员列表
                    if (caseDto.BuyerNameDic != null && caseDto.BuyerNameDic.Count > 0)
                    {
                        foreach (var key in caseDto.BuyerNameDic.Keys)
                        {
                            CustomerInfoViewModel customerInfoViewModel = ServiceLocator.Current.GetInstance<CustomerInfoViewModel>();
                            customerInfoViewModel.CustomerId = key.ToString();
                            customerInfoViewModel.CustomerName = caseDto.BuyerNameDic[key];
                            CustomerAndMateriaListViewModel OIA = new CustomerAndMateriaListViewModel();
                            OIA.CustomerInfo = customerInfoViewModel;
                            var materiaList = AttachmentProxyService.GetInstanse().GetAttachments(key.ToString(), buyerAttachment);
                            if (materiaList != null && materiaList.Count > 0)
                            {
                                var m = new ObservableCollection<MateriaViewModel>();
                                foreach (var materia in materiaList)
                                {
                                    MateriaViewModel model = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                                    foreach (var item in materia.Value)
                                    {
                                        model.MateriaCount.Add(item);
                                    }
                                    model.MateriaName = materia.Key;
                                    m.Add(model);
                                }
                                OIA.MateriaList = m;
                            }
                            viewModel.BuyerList.Add(OIA);
                        }
                    }
                    #endregion

                    #region 卖方人员列表
                    if (caseDto.SellerNameDic != null && caseDto.SellerNameDic.Count > 0)
                    {
                        foreach (var key in caseDto.SellerNameDic.Keys)
                        {
                            CustomerInfoViewModel customerInfoViewModel = ServiceLocator.Current.GetInstance<CustomerInfoViewModel>();
                            customerInfoViewModel.CustomerId = key.ToString();
                            customerInfoViewModel.CustomerName = caseDto.SellerNameDic[key];
                            CustomerAndMateriaListViewModel OIA = new CustomerAndMateriaListViewModel();
                            OIA.CustomerInfo = customerInfoViewModel;
                            var materiaList = AttachmentProxyService.GetInstanse().GetAttachments(key.ToString(), sellerAttachment);
                            if (materiaList != null && materiaList.Count > 0)
                            {
                                var m = new ObservableCollection<MateriaViewModel>();
                                foreach (var materia in materiaList)
                                {
                                    MateriaViewModel model = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                                    foreach (var item in materia.Value)
                                    {
                                        model.MateriaCount.Add(item);
                                    }
                                    model.MateriaName = materia.Key;
                                    m.Add(model);
                                }
                                OIA.MateriaList = m;
                            }
                            viewModel.SellerList.Add(OIA);
                        }
                    }
                    #endregion

                    #region 权利凭证
                    var houseRightVoucher = AttachmentProxyService.GetInstanse().GetAttachments(caseDto.TenementContract, new List<AttachmentType>() { AttachmentType.产证 });

                    MateriaViewModel modelHouse = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                    if (houseRightVoucher.Keys.Contains(AttachmentType.产证))
                    {
                        foreach (var item in houseRightVoucher[AttachmentType.产证])
                        {
                            modelHouse.MateriaCount.Add(item);
                        }
                    }
                    else
                    {
                        modelHouse.MateriaCount = null;
                    }
                    modelHouse.MateriaName = AttachmentType.产证;
                    modelHouse.IsShowItem = true;
                    viewModel.RightsMateriaList.Add(modelHouse);

                    var rightVoucher = AttachmentProxyService.GetInstanse().GetAttachments(viewModel.CaseId, VoucherAttachment);
                    foreach (var item in VoucherAttachment)
                    {
                        MateriaViewModel model = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                        if (rightVoucher.Keys.Contains(item))
                        {
                            foreach (var item1 in rightVoucher[item])
                                model.MateriaCount.Add(item1);
                        }
                        else
                        {
                            model.MateriaCount = null;
                        }
                        model.MateriaName = item;
                        model.IsShowItem = true;
                        viewModel.RightsMateriaList.Add(model);
                    }
                    #endregion

                    #region 交易材料
                    var tradeRsult = AttachmentProxyService.GetInstanse().GetAttachments(caseDto.TenementContract, new List<AttachmentType>() { AttachmentType.产调证 });

                    MateriaViewModel modelTrade = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                    if (tradeRsult.Keys.Contains(AttachmentType.产调证))
                    {
                        foreach (var item in tradeRsult[AttachmentType.产调证])
                        {
                            modelTrade.MateriaCount.Add(item);
                        }
                    }
                    else
                    {
                        modelTrade.MateriaCount = null;
                    }
                    modelTrade.MateriaName = AttachmentType.产调证;
                    modelTrade.IsShowItem = true;
                    viewModel.TranscationList.Add(modelTrade);
                    var tradeRsult1 = AttachmentProxyService.GetInstanse().GetAttachments(viewModel.CaseId, TradeAttachment);
                    foreach (var item in TradeAttachment)
                    {
                        MateriaViewModel model = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                        if (tradeRsult1.Keys.Contains(item))
                        {
                            foreach (var item1 in tradeRsult1[item])
                            {
                                model.MateriaCount.Add(item1);
                            }
                        }
                        else
                        {
                            model.MateriaCount = null;
                        }
                        model.MateriaName = item;
                        model.IsShowItem = true;
                        viewModel.TranscationList.Add(model);
                    }
                    #endregion

                    #region 合同协议
                    var contractResults = AttachmentProxyService.GetInstanse().GetAttachments(viewModel.CaseId, ContractAttachment);
                    foreach (var item in ContractAttachment)
                    {
                        MateriaViewModel model = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                        if (contractResults.Keys.Contains(item))
                        {
                            foreach (var item1 in contractResults[item])
                            {
                                model.MateriaCount.Add(item1);
                            }
                        }
                        else
                        {
                            model.MateriaCount = null;
                        }
                        model.MateriaName = item;
                        model.IsShowItem = true;
                        viewModel.ContractList.Add(model);
                    }
                    #endregion

                    #region 钱款收据
                    var moneyResults = AttachmentProxyService.GetInstanse().GetAttachments(viewModel.CaseId, MoneyAttachment);
                    foreach (var item in MoneyAttachment)
                    {
                        MateriaViewModel model = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                        if (moneyResults.Keys.Contains(item))
                        {
                            foreach (var item1 in moneyResults[item])
                            {
                                model.MateriaCount.Add(item1);
                            }
                        }
                        else
                        {
                            model.MateriaCount = null;
                        }
                        model.MateriaName = item;
                        model.IsShowItem = true;
                        viewModel.MoneyList.Add(model);
                    }
                    #endregion
                    viewModel.HouseContractId = caseDto.TenementContract;
                    #region 基本信息

                    ShowCaseModel showCaseModel = new ShowCaseModel();
                    PreCheckAddCaseViewModel CaseDetailInfo = ServiceLocator.Current.GetInstance<PreCheckAddCaseViewModel>();
                    var status = CaseStatus.HA1 | CaseStatus.HA2;
                    var dicCaseStatus = new Dictionary<CaseStatus, Tuple<string, DateTime>>();
                    var showCaseDto = CaseProxyService.GetInstanse().GetCaseAndProcess(viewModel.CaseId, status, ref dicCaseStatus);
                    if (showCaseDto != null)
                    {
                       
                        DateTime? checkCaseDate = null; //接单时间
                        DateTime? signingDate = null; //签约完成时间
                        if (dicCaseStatus != null && dicCaseStatus.Count > 0)
                        {
                            if (dicCaseStatus.ContainsKey(CaseStatus.HA1))
                            {
                                viewModel.CaseStatus = "—签约中";
                                checkCaseDate = dicCaseStatus[CaseStatus.HA1].Item2;
                                showCaseModel.CheckCaseDate = checkCaseDate.ToString();
                                showCaseModel.FirstReady = true;
                                //核案接单用时
                                TimeSpan ts = (TimeSpan) (checkCaseDate - showCaseDto.CreateDate);
                                if (ts == TimeSpan.Zero)
                                {
                                    showCaseModel.FirstTime = string.Empty;
                                    showCaseModel.FirstReady = false;
                                }
                                else
                                {
                                    showCaseModel.FirstTime = ts.Days + "天" + ts.Hours + "时" + ts.Minutes + "分" +
                                                              ts.Seconds + "秒";
                                    showCaseModel.FirstReady = true;
                                }
                            }
                            if (dicCaseStatus.ContainsKey(CaseStatus.HA2))
                            {
                                viewModel.CaseStatus = "—签约完成";
                                showCaseModel.IsUpdate = false;
                                signingDate = dicCaseStatus[CaseStatus.HA2].Item2;
                                showCaseModel.SigningDate = signingDate.ToString();
                                showCaseModel.SecondReady = true;
                                //签约完成用时
                                TimeSpan ts = (TimeSpan) (signingDate - checkCaseDate);

                                if (ts == TimeSpan.Zero)
                                {
                                    showCaseModel.SecondTime = string.Empty;
                                    showCaseModel.SecondReady = false;
                                }
                                else
                                {
                                    showCaseModel.SecondTime = ts.Days + "天" + ts.Hours + "时" + ts.Minutes + "分" +
                                                               ts.Seconds + "秒";
                                    showCaseModel.SecondReady = true;
                                }

                                CaseDetailInfo.CurrentCaseStatus = CaseStatus.HA2;
                                CaseDetailInfo.IsBuyerForbid = false;
                                CaseDetailInfo.IsSellerForbid = false;
                            }
                        }
                        showCaseModel.CaseId = showCaseDto.CaseId;
                        showCaseModel.CreateDate = showCaseDto.CreateDate.ToString();
                        showCaseModel.CreateName = showCaseDto.CreatorName;
                        showCaseModel.ModifyName = showCaseDto.ModifyUserName;
                        showCaseModel.ModifyDate = showCaseDto.ModifyDate.ToString();
                        showCaseModel.OperateName = showCaseDto.OperatorName;
                        //SourceType = (SourceType)Enum.Parse(typeof(SourceType), Convert.ToString(SelectCaseType))
                        //附件个数     
                        //产权证号
                        if (showCaseDto.TenementContract != null)
                        {
                            var temps = showCaseDto.TenementContract.Split('-');
                            if (temps.Length > 1)
                            {
                                CaseDetailInfo.TbTenementContractWord = temps[0];
                                CaseDetailInfo.TbTenementContract = temps[1];
                            }
                            else
                            {
                                CaseDetailInfo.TbTenementContract = temps[0];
                            }
                        }
                        CaseDetailInfo.CaseSourceType = EnumHelper.InitSourceTypeCombobox();
                        CaseDetailInfo.TbTenementAddress = showCaseDto.TenementAddress;
                        CaseDetailInfo.TblShowInfo = "姓名：" + showCaseDto.AgencyName + "                 所属门店：" + showCaseDto.CompanyName;
                        CaseDetailInfo.SelectCaseType = showCaseDto.SourceType == SourceType.ReceptionCenter ? 0 : 1;
                        CaseDetailInfo.BuyerInfoList = new ObservableCollection<IdInfoModel>();
                        CaseDetailInfo.SellerInfoList = new ObservableCollection<IdInfoModel>();
                        showCaseModel.SellerNameDic = showCaseDto.SellerNameDic;
                        foreach (var sellerName in showCaseModel.SellerNameDic)
                        {
                            UserIdentity userIdentity = UserServiceProxy.GetUserIdentityInfo(sellerName.Key, UserType.Customer);
                            if (userIdentity != null)
                            {
                                userIdentity.SysNo = sellerName.Key;
                                UserIdentityModel userModel = new UserIdentityModel
                                {
                                    Name = sellerName.Value,
                                    CertificateType = userIdentity.CertificateType,
                                    Nationality = userIdentity.Nationality,
                                    IdentityNo = userIdentity.Id.ToString(),
                                    Gender = userIdentity.Gender,
                                    Birthday = userIdentity.Birthday,
                                    Address = userIdentity.Address,
                                    VisaAgency = userIdentity.VisaAgency,
                                    Nation = userIdentity.Nation,                                
                                    Photo = userIdentity.Photo,
                                    EffectiveDate = userIdentity.EffectiveDate,
                                    ExpiryDate = userIdentity.ExpiryDate,
                                    IsTrusted = userIdentity.IsTrusted,                               
                                };
                              
                                var dic  = new Dictionary<long, string>();
                                dic.Add(sellerName.Key, sellerName.Value);
                                dic.Add(0, "true");
                                userModel.Tag = dic;

                                IdInfoModel userIdInfoModel = IdInfoDataModelToViewModel(userModel);
                                CaseDetailInfo.SellerInfoList.Add(userIdInfoModel);
                            }                        
                        }
                        showCaseModel.BuyerNameDic  = showCaseDto.BuyerNameDic;
                        foreach (var buyerName in showCaseModel.BuyerNameDic)
                        {
                            var userIdentity = UserServiceProxy.GetUserIdentityInfo(buyerName.Key, UserType.Customer);
                            if (userIdentity != null)
                            {
                                userIdentity.SysNo = buyerName.Key;
                                UserIdentityModel userModel = new UserIdentityModel
                                {
                                    Name = buyerName.Value,
                                    CertificateType = userIdentity.CertificateType,
                                    Nationality = userIdentity.Nationality,
                                    IdentityNo = userIdentity.Id.ToString(),
                                    Gender = userIdentity.Gender,
                                    Birthday = userIdentity.Birthday,
                                    Address = userIdentity.Address,
                                    VisaAgency = userIdentity.VisaAgency,
                                    Nation = userIdentity.Nation,                               
                                    Photo = userIdentity.Photo,
                                    EffectiveDate=userIdentity.EffectiveDate,
                                    ExpiryDate=userIdentity.ExpiryDate,
                                    IsTrusted = userIdentity.IsTrusted,                                    
                                };
                              
                                var dic = new Dictionary<long, string>();
                                dic.Add(buyerName.Key, buyerName.Value);
                                dic.Add(0, "true");
                                userModel.Tag = dic;

                                IdInfoModel userIdInfoModel = IdInfoDataModelToViewModel(userModel);
                                CaseDetailInfo.BuyerInfoList.Add(userIdInfoModel);
                            }                           
                        }
                        CaseDetailInfo.IdCardBuyerModel = new IdInfoModel();
                        CaseDetailInfo.IdCardBuyerModel.IsCancel = false;
                        CaseDetailInfo.IdCardSellerModel = new IdInfoModel();
                        CaseDetailInfo.IdCardSellerModel.IsCancel = false;
                        CaseDetailInfo.IdentityBuyerList = EnumHelper.InitCertificateTypeToCombobox();
                        CaseDetailInfo.IndexBuyerSelect = 0;

                        CaseDetailInfo.SellerTotal = CaseDetailInfo.SellerInfoList.Count;
                        CaseDetailInfo.BuyerTotal = CaseDetailInfo.BuyerInfoList.Count;
                        CaseDetailInfo.IdentitySellerList = EnumHelper.InitCertificateTypeToCombobox();
                        CaseDetailInfo.IndexSellerSelect = 0;
                        viewModel.CaseDetailInfo = CaseDetailInfo;
                        viewModel.ShowCase = showCaseModel;
                    }
                    #endregion
                }                               
                viewModel.MateriaList = new ObservableCollection<MateriaViewModel>();
                viewModel.MateriaList1 = new ObservableCollection<MateriaViewModel>();
                var list = new List<MateriaViewModel>();
                foreach (var materia in sellerAttachment)
                {
                    MateriaViewModel mvm = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                    mvm.MateriaName = materia;
                    mvm.IsShowItem = true;
                    list.Add(mvm);
                }

                viewModel.BuyMateriaList = new ObservableCollection<MateriaViewModel>();
                viewModel.BuyMateriaList1 = new ObservableCollection<MateriaViewModel>();
                var buyList = new List<MateriaViewModel>();
                foreach (var materia in buyerAttachment)
                {
                    MateriaViewModel mvm = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                    mvm.MateriaName = materia;
                    mvm.IsShowItem = true;
                    buyList.Add(mvm);
                }
                for (int i = 0; i < 14; i++)
                {
                    viewModel.MateriaList.Add(list[i]);
                    viewModel.BuyMateriaList.Add(buyList[i]);
                }
                for (int j = 14; j < list.Count; j++)
                {
                    viewModel.MateriaList1.Add(list[j]);
                }
                for (int m = 14; m < buyList.Count; m++)
                {
                    viewModel.BuyMateriaList1.Add(buyList[m]);
                }
                for (int k = list.Count; k < 28; k++)
                {
                    MateriaViewModel mvm1 = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                    viewModel.MateriaList1.Add(mvm1);
                }
                for (int k = buyList.Count; k < 28; k++)
                {
                    MateriaViewModel mvm1 = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                    viewModel.BuyMateriaList1.Add(mvm1);
                }
         
                viewModel.IsSelectBasicInfo = isSelectBasicInfo;
                viewModel.IsSelectUploadInfo = isSelectUploadAttachment;
                var results = CaseProxyService.GetInstanse().GetCaseEvents(viewModel.CaseId, null, null);
                if (results != null)
                {
                    results = results.OrderBy(x => x.CreateDate).ToList();
                }

                viewModel.AllStatus = new CaseStatusViewModel(results);
                _uploadAttachmentsViewModel = viewModel;

                return viewModel;
            });
        }

        private void OnUploadAttachment(AttachmentType attachmentType)
        {
            string id = string.Empty;
            var tempViewModel = _uploadAttachmentsViewModel;
            //对于卖方和买方需要先选择人
            if (sellerAttachment.Contains(attachmentType) && tempViewModel.CurrentIndexTabItem == 0)
            {
                if (tempViewModel.CurrentItem == null)
                {
                    MessageBox.Show("请选择所属人", "系统提示", MessageBoxButton.OK, MessageBoxImage.Stop);
                    return;
                }
                id = tempViewModel.CurrentCustomer.CustomerId;
            }
            else if (buyerAttachment.Contains(attachmentType) && tempViewModel.CurrentIndexTabItem == 1)
            {
                if (tempViewModel.BuyCurrentItem == null)
                {
                    MessageBox.Show("请选择所属人", "系统提示", MessageBoxButton.OK, MessageBoxImage.Stop);
                    return;
                }
                id = tempViewModel.BuyCurrentCustomer.CustomerId;
            }
            else if(attachmentType==AttachmentType.产调证 || attachmentType==AttachmentType.产证)
            {
                id = _uploadAttachmentsViewModel.HouseContractId;
            }
            else
            {
                id = tempViewModel.CaseId;
            }
            try
            {
                var type = attachmentType;

                var openFiles = new OpenFileDialog
                {
                    Filter = "图像文件(*.jpg,*.bmp,*.png)|*.jpg;*.bmp;*.png",
                    Multiselect = true
                };

                var dialogResult = openFiles.ShowDialog();

                var files = openFiles.OpenFiles();

                if (dialogResult == true)
                {
                    foreach (FileStream file in files)
                    {
                        if (file != null)
                        {
                            //把本地附件放到指定的目录
                            CreateFile(id, type);
                            FileStream AttachFile;
                            using (
                                AttachFile =
                                    new FileStream(_filePath + "\\" + DateTime.Now.Ticks + ".bmp", FileMode.OpenOrCreate,
                                        FileAccess.Write))
                            {
                                byte[] fileByte = new byte[file.Length];
                                file.Read(fileByte, 0, fileByte.Length);

                                AttachFile.Write(fileByte, 0, fileByte.Length);
                            }
                            CaseHelper.AddAttachment(id, AttachFile.Name, type, file, Comlpate);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("本地上传失败原因：" + ex.Message);
            }
        }
        private void Comlpate(AttachmentContent attachInfo, UploadAttachmentResult result)
        {
            if (!result.Success)
            {
                MessageBox.Show("上传失败，请重新上传", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                try
                {
                    attachInfo.AttachmentContentData.Dispose();

                    if (File.Exists(attachInfo.FileName))
                    {
                        File.Delete(attachInfo.FileName);
                    }
                    RefreshUI();
                }
                catch
                {

                }
                finally
                {
                    attachInfo.AttachmentContentData.Close();
                    attachInfo.AttachmentContentData.Dispose();
                }
            }
        }

        private void RefreshUI(string materiaName=null)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                #region 刷新一下当前数据
                var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(_uploadAttachmentsViewModel.CaseId);

                if (caseDto != null)
                {
                    switch (_uploadAttachmentsViewModel.CurrentIndexTabItem)
                    {
                        case 0:
                            #region 卖方人员列表
                            Int32 currentCustomerId = Convert.ToInt32(_uploadAttachmentsViewModel.CurrentItem.CustomerInfo.CustomerId);
                            if (caseDto.SellerNameDic != null && caseDto.SellerNameDic.Count > 0)
                            {
                                ObservableCollection<CustomerAndMateriaListViewModel> tempSellerList = new ObservableCollection<CustomerAndMateriaListViewModel>();
                                foreach (var key in caseDto.SellerNameDic.Keys)
                                {
                                    CustomerInfoViewModel customerInfoViewModel = ServiceLocator.Current.GetInstance<CustomerInfoViewModel>();
                                    customerInfoViewModel.CustomerId = key.ToString();
                                    customerInfoViewModel.CustomerName = caseDto.SellerNameDic[key];
                                    CustomerAndMateriaListViewModel OIA = new CustomerAndMateriaListViewModel();
                                    OIA.CustomerInfo = customerInfoViewModel;
                                    var materiaList = AttachmentProxyService.GetInstanse().GetAttachments(key.ToString(), sellerAttachment);
                                    if (materiaList != null && materiaList.Count > 0)
                                    {
                                        var m = new ObservableCollection<MateriaViewModel>();
                                        foreach (var materia in materiaList)
                                        {
                                            MateriaViewModel model = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                                            foreach (var item in materia.Value)
                                            {
                                                model.MateriaCount.Add(item);
                                            }
                                            model.MateriaName = materia.Key;
                                            m.Add(model);
                                        }
                                        OIA.MateriaList = m;
                                    }
                                    else
                                    {
                                        var m = new ObservableCollection<MateriaViewModel>();
                                        MateriaViewModel model = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                                        model.MateriaCount = new ObservableCollection<string>();
                                        model.MateriaName = (AttachmentType)AttachmentType.Parse(typeof(AttachmentType), materiaName);
                                        m.Add(model);
                                        OIA.MateriaList = m;
                                    }
                                    tempSellerList.Add(OIA);
                                }
                                _uploadAttachmentsViewModel.SellerList = tempSellerList;
                                _uploadAttachmentsViewModel.CurrentItem = tempSellerList.Where(x => x.CustomerInfo.CustomerId == currentCustomerId.ToString()).FirstOrDefault();
                                _uploadAttachmentsViewModel.CurrentCustomer = tempSellerList.Where(x => x.CustomerInfo.CustomerId == _uploadAttachmentsViewModel.CurrentCustomer.CustomerId).First().CustomerInfo;
                            }
                            #endregion
                            break;
                        case 1:
                            #region 买方人员列表
                            Int32 currentBuyCustomerId = Convert.ToInt32(_uploadAttachmentsViewModel.BuyCurrentItem.CustomerInfo.CustomerId);
                            if (caseDto.BuyerNameDic != null && caseDto.BuyerNameDic.Count > 0)
                            {
                                ObservableCollection<CustomerAndMateriaListViewModel> tempBuyerList = new ObservableCollection<CustomerAndMateriaListViewModel>();
                                foreach (var key in caseDto.BuyerNameDic.Keys)
                                {
                                    CustomerInfoViewModel customerInfoViewModel = ServiceLocator.Current.GetInstance<CustomerInfoViewModel>();
                                    customerInfoViewModel.CustomerId = key.ToString();
                                    customerInfoViewModel.CustomerName = caseDto.BuyerNameDic[key];
                                    CustomerAndMateriaListViewModel OIA = new CustomerAndMateriaListViewModel();
                                    OIA.CustomerInfo = customerInfoViewModel;
                                    var buyMateriaList = AttachmentProxyService.GetInstanse().GetAttachments(key.ToString(), buyerAttachment);
                                    if (buyMateriaList != null && buyMateriaList.Count > 0)
                                    {
                                        var m = new ObservableCollection<MateriaViewModel>();
                                        foreach (var materia in buyMateriaList)
                                        {
                                            MateriaViewModel model = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                                            foreach (var item in materia.Value)
                                            {
                                                model.MateriaCount.Add(item);
                                            }
                                            model.MateriaName = materia.Key;
                                            m.Add(model);
                                        }
                                        OIA.MateriaList = m;
                                    }
                                    else
                                    {
                                        var m = new ObservableCollection<MateriaViewModel>();
                                        MateriaViewModel model = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                                        model.MateriaCount = new ObservableCollection<string>();
                                        model.MateriaName = (AttachmentType)AttachmentType.Parse(typeof(AttachmentType), materiaName);
                                        m.Add(model);
                                        OIA.MateriaList = m;
                                    }
                                    tempBuyerList.Add(OIA);
                                }
                                _uploadAttachmentsViewModel.BuyerList = tempBuyerList;
                                _uploadAttachmentsViewModel.BuyCurrentItem = tempBuyerList.Where(x => x.CustomerInfo.CustomerId == currentBuyCustomerId.ToString()).FirstOrDefault();
                                _uploadAttachmentsViewModel.BuyCurrentCustomer = tempBuyerList.Where(x => x.CustomerInfo.CustomerId == _uploadAttachmentsViewModel.BuyCurrentCustomer.CustomerId).First().CustomerInfo;

                            }
                            #endregion
                            break;
                        case 2:
                            #region 权利凭证
                            ObservableCollection<MateriaViewModel> temRightsMateriaList = new ObservableCollection<MateriaViewModel>();
                            var houseRightVoucher = AttachmentProxyService.GetInstanse().GetAttachments(caseDto.TenementContract, new List<AttachmentType>() { AttachmentType.产证 });

                            MateriaViewModel modelHouse = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                            if (houseRightVoucher.Keys.Contains(AttachmentType.产证))
                            {
                                foreach (var item in houseRightVoucher[AttachmentType.产证])
                                {
                                    modelHouse.MateriaCount.Add(item);
                                }
                            }
                            else
                            {
                                modelHouse.MateriaCount = null;
                            }
                            modelHouse.MateriaName = AttachmentType.产证;
                            modelHouse.IsShowItem = true;
                            temRightsMateriaList.Add(modelHouse);

                            var rightVoucher = AttachmentProxyService.GetInstanse().GetAttachments(_uploadAttachmentsViewModel.CaseId, VoucherAttachment);
                            foreach (var item in VoucherAttachment)
                            {
                                MateriaViewModel model = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                                if (rightVoucher.Keys.Contains(item))
                                {
                                    foreach (var item1 in rightVoucher[item])
                                        model.MateriaCount.Add(item1);
                                }
                                else
                                {
                                    model.MateriaCount = null;
                                }
                                model.MateriaName = item;
                                model.IsShowItem = true;
                                temRightsMateriaList.Add(model);
                            }
                            _uploadAttachmentsViewModel.RightsMateriaList = temRightsMateriaList;
                            #endregion
                            break;
                        case 3:
                            #region 交易材料
                            ObservableCollection<MateriaViewModel> tempTranscationList = new ObservableCollection<MateriaViewModel>();
                            var tradeRsult = AttachmentProxyService.GetInstanse().GetAttachments(caseDto.TenementContract, new List<AttachmentType>() { AttachmentType.产调证 });

                            MateriaViewModel modelTrade = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                            if (tradeRsult.Keys.Contains(AttachmentType.产调证))
                            {
                                foreach (var item in tradeRsult[AttachmentType.产调证])
                                {
                                    modelTrade.MateriaCount.Add(item);
                                }
                            }
                            else
                            {
                                modelTrade.MateriaCount = null;
                            }
                            modelTrade.MateriaName = AttachmentType.产调证;
                            modelTrade.IsShowItem = true;
                            tempTranscationList.Add(modelTrade);
                            var tradeRsult1 = AttachmentProxyService.GetInstanse().GetAttachments(_uploadAttachmentsViewModel.CaseId, TradeAttachment);
                            foreach (var item in TradeAttachment)
                            {
                                MateriaViewModel model = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                                if (tradeRsult1.Keys.Contains(item))
                                {
                                    foreach (var item1 in tradeRsult1[item])
                                    {
                                        model.MateriaCount.Add(item1);
                                    }
                                }
                                else
                                {
                                    model.MateriaCount = null;
                                }
                                model.MateriaName = item;
                                model.IsShowItem = true;
                                tempTranscationList.Add(model);
                            }
                            _uploadAttachmentsViewModel.TranscationList = tempTranscationList;
                            #endregion
                            break;
                        case 4:
                            #region 合同协议
                            ObservableCollection<MateriaViewModel> tempContractList = new ObservableCollection<MateriaViewModel>();
                            var contractResults = AttachmentProxyService.GetInstanse().GetAttachments(_uploadAttachmentsViewModel.CaseId, ContractAttachment);
                            foreach (var item in ContractAttachment)
                            {
                                MateriaViewModel model = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                                if (contractResults.Keys.Contains(item))
                                {
                                    foreach (var item1 in contractResults[item])
                                    {
                                        model.MateriaCount.Add(item1);
                                    }
                                }
                                else
                                {
                                    model.MateriaCount = null;
                                }
                                model.MateriaName = item;
                                model.IsShowItem = true;
                                tempContractList.Add(model);
                            }
                            _uploadAttachmentsViewModel.ContractList = tempContractList;
                            #endregion
                            break;
                        case 5:
                            #region 钱款收据
                            ObservableCollection<MateriaViewModel> tempMoneyList = new ObservableCollection<MateriaViewModel>();
                            var moneyResults = AttachmentProxyService.GetInstanse().GetAttachments(_uploadAttachmentsViewModel.CaseId, MoneyAttachment);
                            foreach (var item in MoneyAttachment)
                            {
                                MateriaViewModel model = ServiceLocator.Current.GetInstance<MateriaViewModel>();
                                if (moneyResults.Keys.Contains(item))
                                {
                                    foreach (var item1 in moneyResults[item])
                                    {
                                        model.MateriaCount.Add(item1);
                                    }
                                }
                                else
                                {
                                    model.MateriaCount = null;
                                }
                                model.MateriaName = item;
                                model.IsShowItem = true;
                                tempMoneyList.Add(model);
                            }
                            _uploadAttachmentsViewModel.MoneyList = tempMoneyList;
                            #endregion
                            break;
                    }
                }
                #endregion
            });
        }

        /// <summary>
        /// 创建文件夹
        /// </summary>
        private void CreateFile(string id, AttachmentType type)
        {
            _filePath = new StringBuilder();
            //创建文件夹
            _filePath.AppendFormat(Files.FileParh);
            if (!Directory.Exists(_filePath.ToString()))
            {
                Directory.CreateDirectory(_filePath.ToString());
            }
            _filePath.AppendFormat("\\{0}", id);
            if (!Directory.Exists(_filePath.ToString()))
            {
                Directory.CreateDirectory(_filePath.ToString());
            }
            _filePath.AppendFormat("\\{0}", type);
            if (!Directory.Exists(_filePath.ToString()))
            {
                Directory.CreateDirectory(_filePath.ToString());
            }
            //_pathImage = _filePath.ToString();
        }
        #region 附件类型集合
        private List<AttachmentType> sellerAttachment = new List<AttachmentType>()
        {
            AttachmentType.身份证,
            AttachmentType.户口本,
            AttachmentType.出生证明,
            AttachmentType.结婚证,
            AttachmentType.离婚证,
            AttachmentType.离婚判决书,
            AttachmentType.离婚协议书,
            AttachmentType.单身证明,
            AttachmentType.授权委托书,
            AttachmentType.代理人身份证,
            AttachmentType.军官证,
            AttachmentType.法人身份证,
            AttachmentType.税务登记证,
            AttachmentType.组织机构代码证,
            AttachmentType.公司营业执照,
            AttachmentType.劳动合同,
            AttachmentType.在职证明,
            AttachmentType.境外人士就业证,
            AttachmentType.台胞证,
            AttachmentType.回乡证,
            AttachmentType.港澳身份证,
            AttachmentType.户籍藤本,
            AttachmentType.银行卡,
            AttachmentType.上家其他
        };
        private List<AttachmentType> buyerAttachment = new List<AttachmentType>()
        {
            AttachmentType.身份证,
            AttachmentType.户口本,
            AttachmentType.出生证明,
            AttachmentType.结婚证,
            AttachmentType.离婚证,
            AttachmentType.离婚判决书,
            AttachmentType.离婚协议书,
            AttachmentType.单身证明,
            AttachmentType.授权委托书,
            AttachmentType.代理人身份证,
            AttachmentType.军官证,
            AttachmentType.法人身份证,
            AttachmentType.税务登记证,
            AttachmentType.组织机构代码证,
            AttachmentType.公司营业执照,
            AttachmentType.劳动合同,
            AttachmentType.在职证明,
            AttachmentType.境外人士就业证,
            AttachmentType.台胞证,
            AttachmentType.回乡证,
            AttachmentType.港澳身份证,       
            AttachmentType.户籍藤本,
            AttachmentType.银行卡,
            AttachmentType.收入证明,
            AttachmentType.社保,
            AttachmentType.个保,
            AttachmentType.下家其他
        };
        private List<AttachmentType> VoucherAttachment = new List<AttachmentType>()
        {     
            AttachmentType.他证,
            AttachmentType.购房发票,
            AttachmentType.原购房发票,
            AttachmentType.原购房合同,
            AttachmentType.原契税发票,
            AttachmentType.预告登记证,
            AttachmentType.法院判决书,
            AttachmentType.拍卖确认书,
            AttachmentType.产证密码条,
            AttachmentType.权利凭证其他,         
        };
        private List<AttachmentType> TradeAttachment = new List<AttachmentType>()
        {
            AttachmentType.核价单,
            AttachmentType.限购查询结果,
            AttachmentType.税收受理回执,
            AttachmentType.契税完税凭证,
            AttachmentType.个人所得税单,
            AttachmentType.营业税发票,
            AttachmentType.房产税认定书,
            AttachmentType.收件收据注销抵押,
            AttachmentType.收件收据产权过户,
            AttachmentType.经济人证书, 
            AttachmentType.居间公司营业执照复印件, 
            AttachmentType.居间公司委托书,    
            AttachmentType.交易材料其他
        };
        private List<AttachmentType> ContractAttachment = new List<AttachmentType>()
        {
            AttachmentType.居间合同,
            AttachmentType.网签合同,
            AttachmentType.资金托管协议,
            AttachmentType.佣金托管协议,
            AttachmentType.装修补偿协议书,
            AttachmentType.房屋交接书,
            AttachmentType.解约协议书,
            AttachmentType.优先购买切结书,
            AttachmentType.限购政策告知书,
            AttachmentType.租赁合同复印件,
            AttachmentType.佣金确认书,  
            AttachmentType.合同协议其他    
        };
        private List<AttachmentType> MoneyAttachment = new List<AttachmentType>()
        {
            AttachmentType.中介定金收据,
            AttachmentType.房款收据,
            AttachmentType.佣金收据,
            AttachmentType.税费发票,
            AttachmentType.服务费发票,
            AttachmentType.上家提前还款结算凭证,
            AttachmentType.退款申请书,
            AttachmentType.收据遗失证明,          
            AttachmentType.转账凭条,          
            AttachmentType.钱款收据其他            
        };
        #endregion
    }
}
