﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;
using PinganHouse.SHHTS.UI.WPFNew.Controls;
using Microsoft.Practices.ServiceLocation;
using System.Collections.ObjectModel;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPFNew.Views;
using System.Collections;

namespace PinganHouse.SHHTS.UI.WPFNew.Controllers
{
    public class FundController
    {
        private readonly IUnityContainer container;
        private readonly IRegionManager regionManager;
        private readonly IEventAggregator eventAggregator;

        private IList<AccountLedger> _depositFundsCaseList = null;
        private readonly int PAGE_SIZE = 10;
        private QueryCaseInfoViewModel _searchListPageViewModel = null;
        private DepositFundsViewModel _depositFundsViewModel = null;
        private IList<CaseDto> _caseList = null;


        public FundController(IUnityContainer container,
            IRegionManager regionManager,
            IEventAggregator eventAggregator)
        {
            if (container == null) throw new ArgumentNullException("container");
            if (regionManager == null) throw new ArgumentNullException("regionManager");
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");

            this.container = container;
            this.regionManager = regionManager;
            this.eventAggregator = eventAggregator;
            // Subscribe to the LoginEvents event.
            this.eventAggregator.GetEvent<AllOrderEvents>().Subscribe(this.OnAllOrder, true);
            this.eventAggregator.GetEvent<MoneyBookEvents>().Subscribe(this.OnMoneyBook, true);
            this.eventAggregator.GetEvent<MoneyFlowEvents>().Subscribe(this.OnMoneyFlow, true);
            this.eventAggregator.GetEvent<BillManageEvents>().Subscribe(this.OnBillManage, true);
            this.eventAggregator.GetEvent<RefundEvents>().Subscribe(this.OnRefund, true);


            this.eventAggregator.GetEvent<LoadQueryCaseInfoEvents>().Subscribe(this.OnLoadQueryCaseInfoEvents, true);
            this.eventAggregator.GetEvent<LoadDepositFundsListEvents>().Subscribe(this.OnLoadDepositFundsList, true);
            this.eventAggregator.GetEvent<CreateOrderCaseInfoEvents>().Subscribe(this.OnCreateOrderCaseInfo);
            this.eventAggregator.GetEvent<DepositFundPageChangesEvents>().Subscribe(this.OnDepositFundPageChanges, true);


            this.eventAggregator.GetEvent<PaymentOrderAccountChangeEvents>().Subscribe(this.OnPaymentOrderAccountChange, true);
            this.eventAggregator.GetEvent<GetBankAccountsEvents>().Subscribe(this.OnGetBankAccounts, true);

            this.eventAggregator.GetEvent<SubmitPaymentOrderEvents>().Subscribe(this.OnSubmitPaymentOrder, true);

        }

        private async void OnPaymentOrderAccountChange(PaymentOrderViewModel model)
        {
            model.Balance = await Task<decimal>.Run(() =>
            {
                return AccountServiceProxy.GetBalance(model.Payer, model.CaseId);
            }); 
        }

        private async void OnGetBankAccounts(PaymentOrderViewModel model)
        {
            PaymentOrderViewModel vm = model;
            await Task<decimal>.Run(() =>
            {
                switch (model.Recer)
                {
                    case AccountType.Seller:
                        var bankAccounts = CaseProxyService.GetInstanse().GetCustomerBankAccounts(model.CaseSysNo);
                        if (bankAccounts != null)
                        {
                            foreach (var bankAccount in bankAccounts)
                            {
                                vm.AccountNo = bankAccount.AccountNo;
                                vm.AccountName = bankAccount.AccountName;
                                vm.BankCode = bankAccount.BankCode;
                                vm.BankName = bankAccount.BankName;
                                if (!string.IsNullOrEmpty(bankAccount.SubBankName))
                                {
                                    vm.SubBankName = " - " + bankAccount.SubBankName;
                                }
                            }
                        }
                        break;
                    case AccountType.Agency:
                        var agentCompany = AgentServiceProxy.GetAgentCompany(model.CompanySysNo);
                        if (agentCompany != null)
                        {
                            vm.AccountNo = agentCompany.ReceiveAccount;
                            vm.AccountName = agentCompany.ReceiveName;
                            vm.BankCode = agentCompany.ReceiveBank;
                            vm.BankName = ConfigServiceProxy.GetBankNameByCode(agentCompany.ReceiveBank);
                            if (!string.IsNullOrEmpty(agentCompany.ReceiveSubBank))
                            {
                                vm.SubBankName = " - " + agentCompany.ReceiveSubBank;
                            }
                        }
                        break;
                }
              
            });
            
        }


        private async void OnSubmitPaymentOrder(CreatOrderViewModel model)
        {
            switch (model.SelectIndex)
            {
                case 0:
                    break;
                case 1:
                    var resultPaymentOrder =
                        await
                            SavePayMentOrderData(model.PaymentOrderInfo.Payer, model.PaymentOrderInfo.Recer,
                                model.PaymentOrderInfo.CaseId, model.PaymentOrderInfo.CompanySysNo.ToString(),
                                model.PaymentOrderInfo.OrderBizType, model.PaymentOrderInfo.Balance,
                                model.PaymentOrderInfo.AccountNo, model.PaymentOrderInfo.BankName,
                                model.PaymentOrderInfo.AccountName, model.PaymentOrderInfo.BankName,
                                model.PaymentOrderInfo.Remark, LoginHelper.CurrentUser.SysNo);
                    break;
                case 2:
                    var resultRecordedOrder =
                        await
                            SaveRecordedOrderData(model.RecordedOrderPosInfo.PaymentBelonging,
                                model.RecordedOrderPosInfo.CaseId, model.RecordedOrderPosInfo.PayAmount,
                                model.RecordedOrderPosInfo.OrderBizType, PaymentChannel.POS, "",
                                LoginHelper.CurrentUser.SysNo);
                    break;
                case 3:

                    break;
                case 4:

                    break;
                case 5:
                    var resulCostOrder =
                        await
                            SaveCostOrderData(AccountType.Seller, model.CostOrderPosInfo.CaseId,
                                model.CostOrderPosInfo.PayAmount, model.CostOrderPosInfo.OrderBizType,
                                PaymentChannel.POS, "", LoginHelper.CurrentUser.SysNo);
                    break;
                case 6:
                    var resultWriteOffOrder =
                        await
                            SaveWriteOffOrderData(model.WriteOffOrderInfo.Payer,
                                model.WriteOffOrderInfo.CaseId, model.WriteOffOrderInfo.Recer,
                                model.PaymentOrderInfo.CompanySysNo.ToString(),
                                model.PaymentOrderInfo.OrderBizType, model.PaymentOrderInfo.Balance,
                                model.PaymentOrderInfo.AccountNo, model.PaymentOrderInfo.BankName,
                                model.PaymentOrderInfo.AccountName, model.PaymentOrderInfo.BankName,
                                model.PaymentOrderInfo.Remark, LoginHelper.CurrentUser.SysNo);
                    break;
            }
        }

        /// <summary>
        /// 出账订单
        /// </summary>
        /// <param name="accountType"></param>
        /// <param name="payeeType"></param>
        /// <param name="accountSubjectId"></param>
        /// <param name="agencySubjectId"></param>
        /// <param name="tradeType"></param>
        /// <param name="amount"></param>
        /// <param name="bankAccountNo"></param>
        /// <param name="bankCode"></param>
        /// <param name="accountName"></param>
        /// <param name="bankName"></param>
        /// <param name="remark"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        private Task<OperationResult> SavePayMentOrderData(AccountType accountType, AccountType payeeType, string accountSubjectId, string agencySubjectId, OrderBizType tradeType, decimal amount, string bankAccountNo, string bankCode, string accountName, string bankName, string remark, long operatorSysNo)
        {
            return Task.Run(() =>
            {
                //提交表单
                return TradeServiceProxy.SubmitEntrustPay(accountType, accountSubjectId, payeeType, agencySubjectId,
                    tradeType, amount, bankAccountNo, bankCode, accountName, bankName, remark, operatorSysNo);
            });
        }

        /// <summary>
        /// 入账订单（POS）
        /// </summary>
        /// <param name="accountType"></param>
        /// <param name="caseId"></param>
        /// <param name="amount"></param>
        /// <param name="tradeType"></param>
        /// <param name="paymentChannel"></param>
        /// <param name="remark"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        private Task<OperationResult> SaveRecordedOrderData(AccountType accountType,string caseId, decimal amount, OrderBizType tradeType, PaymentChannel paymentChannel,string remark, long operatorSysNo)
        {
            return Task.Run(() =>
            {
                //提交表单
                return TradeServiceProxy.SubmitCollection(accountType, caseId, amount, tradeType, paymentChannel,
                    remark, operatorSysNo);
            });
        }

        /// <summary>
        /// 费用订单（POS）
        /// </summary>
        /// <param name="accountType"></param>
        /// <param name="caseId"></param>
        /// <param name="amount"></param>
        /// <param name="tradeType"></param>
        /// <param name="paymentChannel"></param>
        /// <param name="remark"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        private Task<OperationResult> SaveCostOrderData(AccountType accountType, string caseId, decimal amount, OrderBizType tradeType, PaymentChannel paymentChannel, string remark, long operatorSysNo)
        {
            return Task.Run(() =>
            {
                //提交表单
                return TradeServiceProxy.SubmitExpendOrder(accountType, caseId, amount, tradeType, paymentChannel,
                    remark, operatorSysNo);
            });
        }

        /// <summary>
        /// 销帐订单
        /// </summary>
        /// <param name="payerType"></param>
        /// <param name="payerSubjectId"></param>
        /// <param name="payeeType"></param>
        /// <param name="payeeSubjectId"></param>
        /// <param name="tradeType"></param>
        /// <param name="amount"></param>
        /// <param name="bankAccountNo"></param>
        /// <param name="bankCode"></param>
        /// <param name="accountName"></param>
        /// <param name="bankName"></param>
        /// <param name="remark"></param>
        /// <param name="operatorSysNo"></param>
        /// <returns></returns>
        private Task<OperationResult> SaveWriteOffOrderData(AccountType payerType, string payerSubjectId, AccountType payeeType, string payeeSubjectId, OrderBizType tradeType, decimal amount, string bankAccountNo, string bankCode, string accountName, string bankName, string remark, long operatorSysNo)
        {
            return Task.Run(() =>
            {
                //提交表单
                return TradeServiceProxy.SubmitPaidOff(payerType, payerSubjectId, payeeType, payeeSubjectId, tradeType, amount, bankAccountNo, bankCode, accountName, bankName, remark, operatorSysNo);
            });
        }



        private async void OnDepositFundPageChanges(int index)
        {
            IRegion loadingRegion = this.regionManager.Regions[RegionNames.LoadingRegion];
            if (loadingRegion == null) return;
            LoadingControl loadingView = loadingRegion.GetView(ViewNamesKey.LOADING_CONTROL_VIEW) as LoadingControl;
            if (loadingView == null)
            {
                //Create a new instance of the EmployeeDetailsView using the Unity container.
                loadingView = this.container.Resolve<LoadingControl>();
                //Add the view to the main region. This automatically activates the view too.
                loadingRegion.Add(loadingView, ViewNamesKey.LOADING_CONTROL_VIEW);
                loadingRegion.Activate(loadingView);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                loadingRegion.Activate(loadingView);
            }
            await Task.Run(() =>
            {
                //获取案件列表数据
                int cnt = 0;
                int pageCount = 0;
                _depositFundsViewModel.CaseList = GetDepositFundCaseList(index, _depositFundsViewModel.SearchCaseCondition, out cnt);
                if (cnt % PAGE_SIZE != 0)
                {
                    pageCount = cnt / PAGE_SIZE + 1;
                }
                else
                {
                    pageCount = cnt / PAGE_SIZE;
                }
                _depositFundsViewModel.TotalPageCount = pageCount;
            });

            RefreshDepositFundsListPage(_depositFundsViewModel);
            loadingRegion.Deactivate(loadingView);
        }

        private async void OnLoadDepositFundsList(DepositFundsViewModel viewModel)
        {
            IRegion loadingRegion = this.regionManager.Regions[RegionNames.LoadingRegion];
            if (loadingRegion == null) return;
            LoadingControl loadingView = loadingRegion.GetView(ViewNamesKey.LOADING_CONTROL_VIEW) as LoadingControl;
            if (loadingView == null)
            {
                //使用统一的容器创建一个新的实例。
                loadingView = this.container.Resolve<LoadingControl>();
                //Add the view to the main region. This automatically activates the view too.
                loadingRegion.Add(loadingView, ViewNamesKey.LOADING_CONTROL_VIEW);
                loadingRegion.Activate(loadingView);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                loadingRegion.Activate(loadingView);
            }
            await Task.Run(() =>
            {

                //获取案件列表数据
                int cnt = 0;
                int pageCount = 0;
                //var list = EnumHelper.InitCaseStatusToCombobox(CASE_STATUS);

                //第一次进来，添加状态，搜索条件信息，否则就只更新list列表信息，其他信息用缓存的内容
                DepositFundsViewModel depositFundsViewModel = ServiceLocator.Current.GetInstance<DepositFundsViewModel>();
                if (_depositFundsViewModel == null)
                {
                    var conditionList = new List<string>()
                {
                    "案件编号","物业地址","主办人","买方姓名","经纪人"
                };
                    if (viewModel != null)
                    {
                        depositFundsViewModel.SearchCaseCondition = viewModel.SearchCaseCondition;
                    }


                    depositFundsViewModel.CaseList = GetDepositFundCaseList(1, depositFundsViewModel.SearchCaseCondition, out cnt);

                    //_depositFundsViewModel.ConditionList = conditionList;
                    _depositFundsViewModel = depositFundsViewModel;
                }
                else
                {
                    if (viewModel != null)
                    {
                        _depositFundsViewModel.SearchCaseCondition = viewModel.SearchCaseCondition;
                    }
                    _depositFundsViewModel.IsPageChanged = false;
                    _depositFundsViewModel.CurrentPageIndex = 1;
                    _depositFundsViewModel.CaseList = GetDepositFundCaseList(1, _depositFundsViewModel.SearchCaseCondition, out cnt);
                }
                if (cnt % PAGE_SIZE != 0)
                {
                    pageCount = cnt / PAGE_SIZE + 1;
                }
                else
                {
                    pageCount = cnt / PAGE_SIZE;
                }
                _depositFundsViewModel.TotalPageCount = pageCount;

            });

            RefreshDepositFundsListPage(_depositFundsViewModel);
            loadingRegion.Deactivate(loadingView);
        }

        private ObservableCollection<DepositFundsDetailViewModel> GetDepositFundCaseList(int page, CaseDetailInfoViewModel model, out int pageCount)
        {
            IList<AccountLedger> caseList = null;
            if (model == null)
            {
                caseList = AccountServiceProxy.GetAccounts(page, PAGE_SIZE, out pageCount, ConfigHelper.GetCurrentReceptionCenter(), null, null, null, null, null);
            }
            else
            {
                caseList = AccountServiceProxy.GetAccounts(page, PAGE_SIZE, out pageCount, ConfigHelper.GetCurrentReceptionCenter(), null, null, null, null, null);
            }
            _depositFundsCaseList = caseList;
            ObservableCollection<DepositFundsDetailViewModel> caseViewModelList = new ObservableCollection<DepositFundsDetailViewModel>();
            if (caseList != null && caseList.Count > 0)
            {
                foreach (AccountLedger Dto in caseList)
                {
                    DepositFundsDetailViewModel depositFundsDetailViewModel = ServiceLocator.Current.GetInstance<DepositFundsDetailViewModel>();
                    depositFundsDetailViewModel.CaseId = Dto.CaseId;
                    depositFundsDetailViewModel.BuyerBalance = Dto.BuyerBalance;
                    depositFundsDetailViewModel.SellerBalance = Dto.SellerBalance;
                    depositFundsDetailViewModel.Contract = Dto.TenementContract;
                    depositFundsDetailViewModel.Address = Dto.TenementAddress;
                    caseViewModelList.Add(depositFundsDetailViewModel);
                }
            }
            return caseViewModelList;
        }

        private void RefreshDepositFundsListPage(DepositFundsViewModel viewModel)
        {
            IRegion mainRegion = this.regionManager.Regions[RegionNames.MainRegion];
            if (mainRegion == null) return;
            DepositFundsListView view = mainRegion.GetView(ViewNamesKey.DEPOSIT_FUNDS_LIST_VIEW) as DepositFundsListView;
            if (view == null)
            {
                //Create a new instance of the EmployeeDetailsView using the Unity container.
                view = this.container.Resolve<DepositFundsListView>();
                //Add the view to the main region. This automatically activates the view too.
                mainRegion.Add(view, ViewNamesKey.DEPOSIT_FUNDS_LIST_VIEW);
                mainRegion.Activate(view);
                view.ViewModel = viewModel;
            }
            else
            {
                //The view has already been added to the region so just activate it.
                mainRegion.Activate(view);
                view.ViewModel = viewModel;
            }
        }


        #region 搜索案件页

        /// <summary>
        /// 加载搜索案件页
        /// </summary>
        /// <param name="viewModel"></param>
        private async void OnLoadQueryCaseInfoEvents(QueryCaseInfoViewModel viewModel)
        {
            IRegion loadingRegion = this.regionManager.Regions[RegionNames.LoadingRegion];
            if (loadingRegion == null) return;
            LoadingControl loadingView = loadingRegion.GetView(ViewNamesKey.LOADING_CONTROL_VIEW) as LoadingControl;
            if (loadingView == null)
            {
                //使用统一的容器创建一个新的实例。
                loadingView = this.container.Resolve<LoadingControl>();
                //Add the view to the main region. This automatically activates the view too.
                loadingRegion.Add(loadingView, ViewNamesKey.LOADING_CONTROL_VIEW);
                loadingRegion.Activate(loadingView);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                loadingRegion.Activate(loadingView);
            }

            await Task.Run(() =>
            {
                //获取案件列表数据
                int totalCount = 0;
                int pageCount = 0;

                //第一次进来，添加状态，搜索条件信息，否则就只更新list列表信息，其他信息用缓存的内容
                QueryCaseInfoViewModel searchListPageViewModel = ServiceLocator.Current.GetInstance<QueryCaseInfoViewModel>();
                if (_searchListPageViewModel == null)
                {

                    if (viewModel != null)
                    {
                        searchListPageViewModel.QueryStringType = viewModel.QueryStringType;
                        searchListPageViewModel.Condition = viewModel.Condition;
                    }
                    searchListPageViewModel.CaseList = GetCaseList(1, searchListPageViewModel, out totalCount);

                    _searchListPageViewModel = searchListPageViewModel;
                }
                else
                {
                    if (viewModel != null)
                    {
                        searchListPageViewModel.QueryStringType = viewModel.QueryStringType;
                        searchListPageViewModel.Condition = viewModel.Condition;
                    }
                    _searchListPageViewModel.IsPageChanged = false;
                    _searchListPageViewModel.CurrentPageIndex = 1;
                    _searchListPageViewModel.CaseList = GetCaseList(1, searchListPageViewModel, out totalCount);
                }
                if (totalCount % PAGE_SIZE != 0)
                {
                    pageCount = totalCount / PAGE_SIZE + 1;
                }
                else
                {
                    pageCount = totalCount / PAGE_SIZE;
                }
                _searchListPageViewModel.TotalPageCount = pageCount;

            });
            loadingRegion.Deactivate(loadingView);
            RefreshQueryCaseInfo(_searchListPageViewModel);
        }

        /// <summary>
        /// 搜索案件页 获取案件列表
        /// </summary>
        /// <param name="page"></param>
        /// <param name="viewModel"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        private ObservableCollection<QueryCaseInfoViewModel> GetCaseList(int page, QueryCaseInfoViewModel viewModel, out int totalCount)
        {
            IList<CaseDto> caseList = null;
            if (string.IsNullOrWhiteSpace(viewModel.Condition))
            {
                caseList = CaseProxyService.GetInstanse().GetCases(page, PAGE_SIZE, out totalCount, ConfigHelper.GetCurrentReceptionCenter());
            }
            else
            {
                caseList = CaseProxyService.GetInstanse().GetCases(page, PAGE_SIZE, out totalCount, ConfigHelper.GetCurrentReceptionCenter(), viewModel.QueryStringType, viewModel.Condition);
            }
            _caseList = caseList;
            ObservableCollection<QueryCaseInfoViewModel> caseViewModelList = new ObservableCollection<QueryCaseInfoViewModel>();
            if (caseList != null && caseList.Count > 0)
            {
                foreach (CaseDto Dto in caseList)
                {
                    QueryCaseInfoViewModel caseDetailInfoViewModel = ServiceLocator.Current.GetInstance<QueryCaseInfoViewModel>();
                    caseDetailInfoViewModel.CaseId = Dto.CaseId;
                    caseDetailInfoViewModel.TenementContract = Dto.TenementContract;
                    caseDetailInfoViewModel.Address = Dto.TenementAddress;
                    caseDetailInfoViewModel.AgentCompany = Dto.CompanyName;
                    caseDetailInfoViewModel.AgentPerson = Dto.AgencyName;
                    caseDetailInfoViewModel.BuyerName = Dto.BuyerName;
                    caseDetailInfoViewModel.SellerName = Dto.SellerName;
                    caseDetailInfoViewModel.CreatorName = Dto.CreatorName;
                    caseDetailInfoViewModel.CreateTime = Dto.CreateDate;
                    caseViewModelList.Add(caseDetailInfoViewModel);
                }
            }
            return caseViewModelList;
        }

        /// <summary>
        /// 刷新搜索案件列表页
        /// </summary>
        /// <param name="viewModel"></param>
        private void RefreshQueryCaseInfo(QueryCaseInfoViewModel viewModel)
        {
            IRegion mainRegion = this.regionManager.Regions[RegionNames.MainRegion];
            if (mainRegion == null) return;
            QueryCaseInfoView view = mainRegion.GetView(ViewNamesKey.QUERY_CASEINFO) as QueryCaseInfoView;
            if (view == null)
            {
                //Create a new instance of the EmployeeDetailsView using the Unity container.
                view = this.container.Resolve<QueryCaseInfoView>();
                //Add the view to the main region. This automatically activates the view too.
                mainRegion.Add(view, ViewNamesKey.QUERY_CASEINFO);
                mainRegion.Activate(view);
                view.ViewModel = viewModel;
            }
            else
            {
                //The view has already been added to the region so just activate it.
                mainRegion.Activate(view);
                view.ViewModel = viewModel;
            }
        }

        #endregion

        #region 创建订单

        /// <summary>
        /// 加载创建订单页面
        /// </summary>
        /// <param name="caseId"></param>
        private void OnCreateOrderCaseInfo(string caseId)
        {
            EnterCreateOrderPage(caseId);
        }

        /// <summary>
        /// 进入创建订单页
        /// </summary>
        /// <param name="caseId"></param>
        private async void EnterCreateOrderPage(string caseId)
        {
            IRegion loadingRegion = this.regionManager.Regions[RegionNames.LoadingRegion];
            if (loadingRegion == null) return;
            LoadingControl loadingView = loadingRegion.GetView(ViewNamesKey.LOADING_CONTROL_VIEW) as LoadingControl;
            if (loadingView == null)
            {
                //使用统一的容器创建一个新的实例。
                loadingView = this.container.Resolve<LoadingControl>();
                //Add the view to the main region. This automatically activates the view too.
                loadingRegion.Add(loadingView, ViewNamesKey.LOADING_CONTROL_VIEW);
                loadingRegion.Activate(loadingView);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                loadingRegion.Activate(loadingView);
            }
            CreatOrderViewModel vm = ServiceLocator.Current.GetInstance<CreatOrderViewModel>();
            vm.CaseId = caseId;
            vm = await GetCaseInfo(vm);

            //CaseDto caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(_caseId);
            IRegion mainRegion = this.regionManager.Regions[RegionNames.MainRegion];
            if (mainRegion == null) return;
            CreatOrderView view = mainRegion.GetView(ViewNamesKey.CREATE_ORDER) as CreatOrderView;
            if (view == null)
            {
                //Create a new instance of the EmployeeDetailsView using the Unity container.
                view = this.container.Resolve<CreatOrderView>();
                //Add the view to the main region. This automatically activates the view too.
                mainRegion.Add(view, ViewNamesKey.CREATE_ORDER);
                mainRegion.Activate(view);
                view.ViewModel = vm;
            }
            else
            {
                //The view has already been added to the region so just activate it.
                mainRegion.Activate(view);
                view.ViewModel = vm;
            }

            loadingRegion.Deactivate(loadingView);
        }

        /// <summary>
        /// 创建订单页 显示案件信息
        /// </summary>
        /// <param name="tempViewModel"></param>
        /// <returns></returns>
        public Task<CreatOrderViewModel> GetCaseInfo(CreatOrderViewModel tempViewModel)
        {
            //根据案件编号获取相关买方和卖方人员列表
            CreatOrderViewModel viewModel = tempViewModel;
            return Task.Run(() =>
           {
               CaseInfo caseInfo = new CaseInfo();
               var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(viewModel.CaseId);
               if (caseDto != null)
               {
                   caseInfo.SysNo = caseDto.SysNo;
                   caseInfo.CompanySysNo = caseDto.CompanySysNo;
                   caseInfo.TenementContract = caseDto.TenementContract;
                   caseInfo.Address = caseDto.TenementAddress;
                   caseInfo.SellerName = caseDto.SellerName;
                   caseInfo.BuyerName = caseDto.BuyerName;
                   caseInfo.AgentCompany = caseDto.CompanyName;
                   caseInfo.AgentPerson = caseDto.AgencyName;
               }
               viewModel.CaseInfo = caseInfo;
               return viewModel;
           });
        }


        private async void GetBalanceEvent(AccountType accountType, string id)
        {
            OrderInfo orderInfo = ServiceLocator.Current.GetInstance<OrderInfo>();
            await Task.Run(() =>
            {
                var account = AccountServiceProxy.GetBalance(accountType, id);
                if (account != null)
                {
                    orderInfo.Balance = string.Format("{0:N2}", account);
                }
            });
        }

        //public Task<OrderInfo> GetBalance()
        //{

        //}


        #endregion


        /// <summary>
        /// 全部订单
        /// </summary>
        /// <param name="viewModel"></param>
        private void OnAllOrder(object viewModel)
        {
            //IRegion mainRegion = this.regionManager.Regions[RegionNames.MainRegion];
            //if (mainRegion == null) return;
            //MainView view = mainRegion.GetView(ViewNamesKey.MAIN_VIEW) as MainView;
            //if (view == null)
            //{
            //    //Create a new instance of the EmployeeDetailsView using the Unity container.
            //    view = this.container.Resolve<MainView>();

            //    //Add the view to the main region. This automatically activates the view too.
            //    mainRegion.Add(view, ViewNamesKey.MAIN_VIEW);
            //    mainRegion.Activate(view);
            //}
            //else
            //{
            //    //The view has already been added to the region so just activate it.
            //    mainRegion.Activate(view);
            //}
        }
        /// <summary>
        /// 资金台帐
        /// </summary>
        /// <param name="viewModel"></param>
        private void OnMoneyBook(object viewModel)
        {
        }
        /// <summary>
        /// 资金流水
        /// </summary>
        /// <param name="viewModel"></param>
        private void OnMoneyFlow(object viewModel)
        {
        }
        /// <summary>
        /// 票据管理
        /// </summary>
        /// <param name="viewModel"></param>
        private void OnBillManage(object viewModel)
        {
        }
        /// <summary>
        /// 退款管理
        /// </summary>
        /// <param name="viewModel"></param>
        private void OnRefund(object viewModel)
        {
        }

    }
}
