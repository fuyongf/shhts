﻿using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using PinganHouse.SHHTS.UI.WPFNew.Controls;
using PinganHouse.SHHTS.UI.WPFNew.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;
using Microsoft.Practices.ServiceLocation;

namespace PinganHouse.SHHTS.UI.WPFNew.Controllers
{
    public class MainController
    {
        private readonly IUnityContainer container;
        private readonly IRegionManager regionManager;
        private readonly IEventAggregator eventAggregator;

        public MainController(IUnityContainer container,
                                    IRegionManager regionManager,
                                    IEventAggregator eventAggregator)
        {
            if (container == null) throw new ArgumentNullException("container");
            if (regionManager == null) throw new ArgumentNullException("regionManager");
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            

            this.container = container;
            this.regionManager = regionManager;
            this.eventAggregator = eventAggregator;

            // Subscribe to the LoginEvents event.
            this.eventAggregator.GetEvent<PreCheckAddCaseEvents>().Subscribe(this.OnPreCheckAddCase, true);
            this.eventAggregator.GetEvent<NuclearEvents>().Subscribe(this.OnNuclear, true);
            this.eventAggregator.GetEvent<SearchListPageEvents>().Subscribe(this.OnSearchListAddCase, true);


            this.eventAggregator.GetEvent<QueryCasePageEvents>().Subscribe(this.OnQueryCaseInfo, true);
        }

        /// <summary>
        /// Called when a new employee is selected. This method uses
        /// view injection to programmatically 
        /// </summary>
        private void OnPreCheckAddCase(object viewModel)
        {
            IRegion mainRegion = this.regionManager.Regions[RegionNames.MainRegion];
            if (mainRegion == null) return;
            AddCaseView view = mainRegion.GetView(ViewNamesKey.ADD_CASE_VIEW) as AddCaseView;
            if (view == null)
            {
                 //Create a new instance of the EmployeeDetailsView using the Unity container.
                view = this.container.Resolve<AddCaseView>();
                view.ViewModel = ServiceLocator.Current.GetInstance<PreCheckAddCaseViewModel>();
                 //Add the view to the main region. This automatically activates the view too.
                mainRegion.Add(view, ViewNamesKey.ADD_CASE_VIEW);
                mainRegion.Activate(view);
            } 
            else
            {
                 //The view has already been added to the region so just activate it.
                mainRegion.Activate(view);
            }
        }
        /// <summary>
        /// 核案签约入口
        /// </summary>
        /// <param name="viewModel"></param>
        private void OnNuclear(object viewModel)
        {
            IRegion mainRegion = this.regionManager.Regions[RegionNames.MainRegion];
            if (mainRegion == null) return;
            CheckCaseSearchPageView view = mainRegion.GetView(ViewNamesKey.NUCLEAR_CHECK_CASE_SEARCH_PAGE_VIEW) as CheckCaseSearchPageView;
            if (view == null)
            {
                //Create a new instance of the EmployeeDetailsView using the Unity container.
                view = this.container.Resolve<CheckCaseSearchPageView>();
                view.ViewModel = ServiceLocator.Current.GetInstance<CheckCaseSearchPageViewModel>();
                //Add the view to the main region. This automatically activates the view too.
                mainRegion.Add(view, ViewNamesKey.NUCLEAR_CHECK_CASE_SEARCH_PAGE_VIEW);
                mainRegion.Activate(view);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                mainRegion.Activate(view);
            }
        }

        /// <summary>
        /// Called when a new employee is selected. This method uses
        /// view injection to programmatically 
        /// </summary>
        private void OnSearchListAddCase(object viewModel)
        {
            IRegion mainRegion = this.regionManager.Regions[RegionNames.MainRegion];
            if (mainRegion == null) return;
            SearchListPageView view = mainRegion.GetView(ViewNamesKey.SEARCH_LIST_PAGE_VIEW) as SearchListPageView;
            if (view == null)
            {
                //Create a new instance of the EmployeeDetailsView using the Unity container.
                view = this.container.Resolve<SearchListPageView>();

                //Add the view to the main region. This automatically activates the view too.
                mainRegion.Add(view, ViewNamesKey.SEARCH_LIST_PAGE_VIEW);
                mainRegion.Activate(view);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                mainRegion.Activate(view);
            }
        }


        /// <summary>
        /// 搜索案件页
        /// </summary>
        /// <param name="viewModel"></param>
        private void OnQueryCaseInfo(object viewModel)
        {
            IRegion mainRegion = this.regionManager.Regions[RegionNames.MainRegion];
            if (mainRegion == null) return;
            QueryCaseInfoView view = mainRegion.GetView(ViewNamesKey.QUERY_CASEINFO) as QueryCaseInfoView;
            if (view == null)
            {
                //Create a new instance of the EmployeeDetailsView using the Unity container.
                view = this.container.Resolve<QueryCaseInfoView>();
                view.ViewModel = ServiceLocator.Current.GetInstance<QueryCaseInfoViewModel>();
                //Add the view to the main region. This automatically activates the view too.
                mainRegion.Add(view, ViewNamesKey.QUERY_CASEINFO);
                mainRegion.Activate(view);
            }
            else
            {
                //The view has already been added to the region so just activate it.
                mainRegion.Activate(view);
            }
        }

    }
}
