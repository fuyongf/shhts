﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PinganHouse.SHHTS.RemoteServiceProxy;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls
{
    /// <summary>
    /// ListBoxUserControl.xaml 的交互逻辑
    /// </summary>
    public partial class ListBoxUserControl : UserControl
    {
        public ListBoxUserControl()
        {
            InitializeComponent();
        }  
        #region 属性
        public string UserName
        {
            get { return (string)GetValue(UserNameProperty); }
            set { SetValue(UserNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for UserName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty UserNameProperty =
            DependencyProperty.Register("UserName", typeof(string), typeof(ListBoxUserControl), new PropertyMetadata(string.Empty));


        public string TitleName
        {
            get { return (string)GetValue(TitleNameProperty); }
            set { SetValue(TitleNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TitleName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TitleNameProperty =
            DependencyProperty.Register("TitleName", typeof(string), typeof(ListBoxUserControl), new PropertyMetadata(string.Empty));

        public int Total
        {
            get { return (int)GetValue(TotalProperty); }
            set { SetValue(TotalProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Total.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TotalProperty =
            DependencyProperty.Register("Total", typeof(int), typeof(ListBoxUserControl), new PropertyMetadata(0));

        public ObservableCollection<IdInfoModel> ListUser
        {
            get { return (ObservableCollection<IdInfoModel>)GetValue(ListUserProperty); }
            set { SetValue(ListUserProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ListUser.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ListUserProperty =
            DependencyProperty.Register("ListUser", typeof(ObservableCollection<IdInfoModel>), typeof(ListBoxUserControl), new PropertyMetadata(null));


        public IdInfoModel SelectUser
        {
            get { return (IdInfoModel)GetValue(SelectUserProperty); }
            set { SetValue(SelectUserProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectUser.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectUserProperty =
            DependencyProperty.Register("SelectUser", typeof(IdInfoModel), typeof(ListBoxUserControl), new PropertyMetadata(null));



        public bool DeleteButtonIsEnable
        {
            get { return (bool)GetValue(DeleteButtonIsEnableProperty); }
            set { SetValue(DeleteButtonIsEnableProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DeleteButtonIsEnable.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DeleteButtonIsEnableProperty =
            DependencyProperty.Register("DeleteButtonIsEnable", typeof(bool), typeof(ListBoxUserControl), new PropertyMetadata(true));



        public static readonly RoutedEvent DeleteButtonClickEvent = EventManager.RegisterRoutedEvent(
           "DeleteButtonClick", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ListBoxUserControl));


        // Expose this event for this control's container
        public event RoutedEventHandler DeleteButtonClick
        {
            add { AddHandler(DeleteButtonClickEvent, value); }
            remove { RemoveHandler(DeleteButtonClickEvent, value); }
        }

        public static readonly RoutedEvent SelectDeleteEvent = EventManager.RegisterRoutedEvent(
         "SelectDeleteClick", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ListBoxUserControl));


        // Expose this event for this control's container
        public event RoutedEventHandler SelectDeleteClick
        {
            add { AddHandler(SelectDeleteEvent, value); }
            remove { RemoveHandler(SelectDeleteEvent, value); }
        }
            
        #endregion

        private void button_Click(object sender, RoutedEventArgs e)
        {
            if(DeleteButtonIsEnable)
            {
                RaiseEvent(new RoutedEventArgs(DeleteButtonClickEvent));
            }
        }

        private void ListBoxName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //RaiseEvent(new RoutedEventArgs(SelectDeleteEvent));
        }


    }
}
