﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls
{
    public class ListBoxUserModel : BaseViewModel
    {
        /// <summary>
        /// 证件类型
        /// </summary>
        public CertificateType CertificateType { get; set; }

        /// <summary>
        /// 国籍
        /// </summary>
        public string Nationality { get; set; }

        /// <summary>
        /// 证件号码
        /// </summary>
        public string IdentityNo { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        /// 出生年月
        /// </summary>
        public DateTime? Birthday { get; set; }

        /// <summary>
        /// 住址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 签证机构
        /// </summary>
        public string VisaAgency { get; set; }

        /// <summary>
        /// 民族
        /// </summary>
        public Nation Nation { get; set; }

        /// <summary>
        /// 有效日期(开始)
        /// </summary>
        public DateTime? EffectiveDate { get; set; }

        /// <summary>
        /// 失效日期
        /// </summary>
        public DateTime? ExpiryDate { get; set; }

        /// <summary>
        /// 证件照
        /// </summary>
        public byte[] Photo { get; set; }

        /// <summary>
        /// 可信的
        /// </summary>
        public bool IsTrusted { get; set; }

        /// <summary>
        /// 系统对象其它属性
        /// </summary>
        public object Tag { get; set; }
    }
}
