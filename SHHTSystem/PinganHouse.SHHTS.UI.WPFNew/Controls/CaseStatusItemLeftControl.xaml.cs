﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls
{
    /// <summary>
    /// CaseStatusItemLeftControl.xaml 的交互逻辑
    /// </summary>
    public partial class CaseStatusItemLeftControl : UserControl
    {
        public CaseStatusItemLeftControl()
        {
            InitializeComponent();
        }
        public string StatusName
        {
            get { return (string)GetValue(StatusNameProperty); }
            set { SetValue(StatusNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for StatusName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StatusNameProperty =
            DependencyProperty.Register("StatusName", typeof(string), typeof(CaseStatusItemLeftControl), new PropertyMetadata(string.Empty));

        public string DetailInfo
        {
            get { return (string)GetValue(DetailInfoProperty); }
            set { SetValue(DetailInfoProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DetailInfo.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DetailInfoProperty =
            DependencyProperty.Register("DetailInfo", typeof(string), typeof(CaseStatusItemLeftControl), new PropertyMetadata(null));


        public bool IsReadyable
        {
            get { return (bool)GetValue(IsReadyableProperty); }
            set { SetValue(IsReadyableProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsReadyable.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsReadyableProperty =
            DependencyProperty.Register("IsReadyable", typeof(bool), typeof(CaseStatusItemLeftControl), new PropertyMetadata(false));



        public double RightLineHeight
        {
            get { return (double)GetValue(BottomLineHeightProperty); }
            set { SetValue(BottomLineHeightProperty, value); }
        }

        // Using a DependencyProperty as the backing store for BottomLineHeight.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BottomLineHeightProperty =
            DependencyProperty.Register("BottomLineHeight", typeof(double), typeof(CaseStatusItemLeftControl), new PropertyMetadata(Convert.ToDouble(20)));
    }
}
