﻿using System;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;
using System.Windows;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls
{
    /// <summary>
    /// SeniorSearchDialog.xaml 的交互逻辑
    /// </summary>
    public partial class SeniorSearchDialog : Window
    {
        public SeniorSearchDialog(CaseDetailInfoViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
        }
        public CaseDetailInfoViewModel ViewModel
        {
            get
            {
                return (CaseDetailInfoViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
        public double ScreenHeigh
        {
            get { return (double)GetValue(ScreenHeighProperty); }
            set { SetValue(ScreenHeighProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScreenHeigh.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScreenHeighProperty =
            DependencyProperty.Register("ScreenHeigh", typeof(double), typeof(SeniorSearchDialog), new PropertyMetadata(SystemParameters.PrimaryScreenHeight));



        public double ScreenWidth
        {
            get { return (double)GetValue(ScreenWidthProperty); }
            set { SetValue(ScreenWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScreenWidth.  This enables animation, styling, binding, etc...SystemParameters.WorkArea.Width
        public static readonly DependencyProperty ScreenWidthProperty =
            DependencyProperty.Register("ScreenWidth", typeof(double), typeof(SeniorSearchDialog), new PropertyMetadata(SystemParameters.PrimaryScreenWidth));


        private void CloseButton_Click(object sender,RoutedEventArgs e)
        {
            this.Close();
        }

        private void DatePicker_SelectedDateChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            CompareTime();
        }     
        private void DatePicker_SelectedDateChanged_1(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            CompareTime();
        }
        private void CompareTime()
        { 
            var preTime = PreDp.SelectedDate;
            var backTime = BackDp.SelectedDate;
            if (backTime != null && preTime == null)
            {
                MessageBox.Show("请选择搜索的起始时间", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (preTime != null)
            {
                DateTime time1 = (DateTime)preTime;
                if (backTime != null)
                {
                    var time2 = (DateTime)backTime;
                    var result = DateTime.Compare(time2, time1);
                    if (result < 0 || result == 0)
                    {
                        MessageBox.Show("搜索的结束时间不能小于或等于开始时间", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
            }
        }
    }
}
