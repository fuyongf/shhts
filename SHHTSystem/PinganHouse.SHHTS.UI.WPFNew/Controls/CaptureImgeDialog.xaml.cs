﻿using Microsoft.Practices.ServiceLocation;

using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;

using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.Integration;
using System.Windows.Media.Imaging;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls
{
    /// <summary>
    /// CaptureImgeDialog.xaml 的交互逻辑
    /// </summary>
    public partial class CaptureImgeDialog : Window
    {
        private static AxICaptureImageLib.AxICaptureImage USBImage = new AxICaptureImageLib.AxICaptureImage();
        StringBuilder _filePath;
        String _pathImage;
        GaoPaiPhotosViewModel gaoPaiPhotosViewModel = ServiceLocator.Current.GetInstance<GaoPaiPhotosViewModel>();
        
        int iImgCount = 0;//统计拍了多少张
        
        public double ScreenHeigh
        {
            get { return (double)GetValue(ScreenHeighProperty); }
            set { SetValue(ScreenHeighProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScreenHeigh.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScreenHeighProperty =
            DependencyProperty.Register("ScreenHeigh", typeof(double), typeof(CaptureImgeDialog), new PropertyMetadata(SystemParameters.PrimaryScreenHeight * 0.9));



        public double ScreenWidth
        {
            get { return (double)GetValue(ScreenWidthProperty); }
            set { SetValue(ScreenWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScreenWidth.  This enables animation, styling, binding, etc...SystemParameters.WorkArea.Width
        public static readonly DependencyProperty ScreenWidthProperty =
            DependencyProperty.Register("ScreenWidth", typeof(double), typeof(CaptureImgeDialog), new PropertyMetadata(SystemParameters.PrimaryScreenWidth * 0.9));


        public CaptureImgeDialog(string id, AttachmentType type)
        {
            InitializeComponent();
            gaoPaiPhotosViewModel.Photos.Clear();
            gaoPaiPhotosViewModel.Id = id;
            gaoPaiPhotosViewModel.Type = type;
            CreateFile();
        }


        #region 创建文件夹
        /// <summary>
        /// 创建文件夹
        /// </summary>
        private void CreateFile()
        {
            _filePath = new StringBuilder();
            //创建文件夹
            _filePath.AppendFormat(Files.FileParh);
            if (!Directory.Exists(_filePath.ToString()))
            {
                Directory.CreateDirectory(_filePath.ToString());
            }
            _filePath.AppendFormat("\\{0}", gaoPaiPhotosViewModel.Id);
            if (!Directory.Exists(_filePath.ToString()))
            {
                Directory.CreateDirectory(_filePath.ToString());
            }
            _filePath.AppendFormat("\\{0}", gaoPaiPhotosViewModel.Type);
            if (!Directory.Exists(_filePath.ToString()))
            {
                Directory.CreateDirectory(_filePath.ToString());
            }
            _pathImage = _filePath.ToString();
        }
        #endregion

        private void CaptrueWindow_Loaded(object sender, RoutedEventArgs e)
        {
            WindowsFormsHost host = new WindowsFormsHost();
            host.Child = USBImage;
            this.m_GridPhoto.Children.Add(host);
            USBImage.OpenDevice(0);
            int iResCount = 0;
            iResCount = USBImage.GetResolutionCount(0);
            cbBoxResolution.Items.Clear();
            ArrayList szRes = new ArrayList();
            for (int index = 0; index < iResCount; index++)
            {
                szRes.Add(String.Format("{0}*{1}", USBImage.GetResolutionWidth(0, index), USBImage.GetResolutionHeight(0, index)));
                cbBoxResolution.Items.Add(szRes[index].ToString().Trim());
            }
            if (szRes.Count > 0)
            {
                cbBoxResolution.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// 拍照
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TakePhoto_Click(object sender, RoutedEventArgs e)
        {
            StringBuilder szfilePath = new StringBuilder();
            string path = _pathImage + "\\" + DateTime.Now.Ticks + ".jpg";
            szfilePath.AppendFormat(path);
            int iResult;
            try
            {
                iResult = USBImage.GrabToFile(szfilePath.ToString().Trim());
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("拍照失败，请检查!", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                iResult = -1;
            }
            if (iResult == 0)
            {
                iImgCount++;
                this.m_PhotosCount.Text = iImgCount.ToString();
                // 设置图像控件  
                var uri = string.Format("{0}{1}", AppDomain.CurrentDomain.BaseDirectory, path.Substring(2));
                if (File.Exists(uri))
                {
                    Image img = new Image();
                    var imageSource = new BitmapImage();
                    var fileStream = new FileStream(uri, FileMode.Open, FileAccess.Read);
                    imageSource.BeginInit();
                    imageSource.StreamSource = fileStream;
                    imageSource.EndInit();
                    img.Tag = uri;
                    img.Source = imageSource;
                    //this.m_Img. = uri;
                    this.m_Img.ImageSource = imageSource;
                    PhotosViewModel gpViewModel = new PhotosViewModel();
                    gpViewModel.Tag = uri;
                    gpViewModel.ImageSource = imageSource;
                    gpViewModel.Index = iImgCount - 1;//索引从0起
                    gpViewModel.ImageName = uri.Substring(uri.LastIndexOf('\\') + 1, 18);
                    gpViewModel.IsChecked = true;//默认选中
                    gaoPaiPhotosViewModel.Photos.Add(gpViewModel);
                }
            }
            else
            {

            }
        }
        /// <summary>
        /// 分辨率设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbBoxResolution_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            int iResult;
            try
            {
                iResult = USBImage.SetResolutionIndex(0, cbBoxResolution.SelectedIndex);
            }
            catch (System.Exception ex)
            {
                iResult = -1;
            }
        }
        /// <summary>
        /// 关闭按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DeleteImages();
            ClosedCurrentWindow();
        }

        /// <summary>
        /// 取消按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            DeleteImages();
            ClosedCurrentWindow();
        }
        private void DeleteImages()
        {
            foreach (var item in gaoPaiPhotosViewModel.Photos)
            {
                try
                {

                    Dispatcher.BeginInvoke(new Action(() =>
                    {
                        if (File.Exists(item.Tag))
                        {
                            item.ImageSource.StreamSource.Close();
                            item.ImageSource.StreamSource.Dispose();
                            File.Delete(item.Tag);
                        }

                    }), System.Windows.Threading.DispatcherPriority.Background);
                }
                catch
                {
                    MessageBox.Show("删除失败，请重新删除", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                    item.ImageSource.StreamSource.Close();
                    item.ImageSource.StreamSource.Dispose();
                }
            }
        }

        private void ClosedCurrentWindow()
        {
            //释放高拍仪设备
            USBImage.CloseDevice();
            //关闭窗口
            this.Close();
        }

        private void Upload_Click(object sender, RoutedEventArgs e)
        {
            if (gaoPaiPhotosViewModel.Photos.Count == 0)
            {
                MessageBox.Show("没有要上传的选项!", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            ClosedCurrentWindow();
            var newWindows = new DisplayImageDialog(gaoPaiPhotosViewModel);
            newWindows.ViewModel = gaoPaiPhotosViewModel;
            newWindows.ShowDialog();
        }
    }
}