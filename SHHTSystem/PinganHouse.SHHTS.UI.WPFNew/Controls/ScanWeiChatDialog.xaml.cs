﻿using PinganHouse.SHHTS.RemoteServiceProxy;

using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls
{
    /// <summary>
    /// ScanWeiChatDialog.xaml 的交互逻辑
    /// </summary>
    public partial class ScanWeiChatDialog : Window
    {
        private long userSysNo;
        public ScanWeiChatDialog()
        {
            InitializeComponent();
        }

        public ScanWeiChatDialog(string userSysNo) : this()
        {
            this.userSysNo = long.Parse(userSysNo);
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
            
        }

        private async void ScanWeiChatDialog_OnLoaded(object sender, RoutedEventArgs e)
        {
            try
            {
                string url = await GetQrCodeUrl(userSysNo);
                if (string.IsNullOrEmpty(url))
                {
                    throw new Exception("未正确获取微信生成链接，无法生成.");
                }
                image.Source = new BitmapImage(new Uri(url));
            }
            catch (Exception ex)
            {
                MessageBox.Show("系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {

            }

        }

        /// <summary>
        /// 返回二维码
        /// </summary>
        /// <returns></returns>
        private Task<string> GetQrCodeUrl(long sysNo)
        {
            return Task.Run(() =>
            {
                var url = ParterProxyService.GetInstanse().GetQrCodeUrl(sysNo);
                return url;
            });
        }
    }
}
