﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls
{  
    public delegate void ButtonHandle(object sender, EventArgs e);

    /// <summary>
    /// SHHTS_Navigation.xaml 的交互逻辑
    /// </summary>
    public partial class SHHTS_Navigation : UserControl
    {
        //确定预约事件
        public event ButtonHandle BasicInfoEvent;
        public event ButtonHandle AttachmentEvent;
        public event ButtonHandle UploadEvent;
        public event ButtonHandle OperationEvent;
        public event ButtonHandle LoadCtrlEvent;
        //引用基类Page
        public Page m_Page = null;
        //引用基类Page
        public Page m_New_Page = null;
        //判断基本信息页面
        public int DetailsFlag=0;

        public SHHTS_Navigation()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 基本信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BasicInfo_Click(object sender, RoutedEventArgs e)
        {
            if (BasicInfoEvent != null)
            {
                BasicInfoEvent(sender,e);
                
            }
            else
            {
                if (m_Page != null && m_New_Page != null)
                {
                    //PageHelper.PageNavigateHelper(m_Page, m_New_Page);
                }
            }
        }

        /// <summary>
        /// 上传附件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Attachment_Click(object sender, RoutedEventArgs e)
        {
            if (AttachmentEvent != null)
            {
                AttachmentEvent(sender, e);
            }
            else
            {
                if (m_Page != null && m_New_Page != null)
                {
                   // PageHelper.PageNavigateHelper(m_Page, m_New_Page);
                }
            }
        }

        /// <summary>
        /// 上传附件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Upload_Click(object sender, RoutedEventArgs e)
        {
            if (UploadEvent != null)
            {
                UploadEvent(sender, e);
            }
            else
            {
                if (m_Page != null && m_New_Page != null)
                {
                    //PageHelper.PageNavigateHelper(m_Page, m_New_Page);
                }
            }
        }

        /// <summary>
        /// 操作者信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Operation_Click(object sender, RoutedEventArgs e)
        {
            //this.btnBasicInfo.Style = (Style)FindResource("ShhtsBtGray");
            //this.btnAttachment.Style = (Style)FindResource("ShhtsBtGray");
            //this.btnUpload.Style = (Style)FindResource("ShhtsBtGray");
            //this.btnOperation.Style = (Style)FindResource("ShhtsBtBlue");
            if (OperationEvent != null)
            {
                OperationEvent(sender, e);
            }
            else
            {
                if (m_Page != null && m_New_Page != null)
                {
                    //PageHelper.PageNavigateHelper(m_Page, m_New_Page);
                }
            }
        }

        /// <summary>
        /// Loading
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (LoadCtrlEvent != null)
            {
                LoadCtrlEvent(sender,e);
            }
        }

        public bool IsBasicInfoChecked
        {
            get { return (bool)GetValue(IsBasicInfoCheckedProperty); }
            set { SetValue(IsBasicInfoCheckedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsBasicInfoChecked.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsBasicInfoCheckedProperty =
            DependencyProperty.Register("IsBasicInfoChecked", typeof(bool), typeof(SHHTS_Navigation), new PropertyMetadata(false));



        public bool IsAttachementChecked
        {
            get { return (bool)GetValue(IsAttachementCheckedProperty); }
            set { SetValue(IsAttachementCheckedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsAttachementChecked.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsAttachementCheckedProperty =
            DependencyProperty.Register("IsAttachementChecked", typeof(bool), typeof(SHHTS_Navigation), new PropertyMetadata(false));



        public bool IsUploadChecked
        {
            get { return (bool)GetValue(IsUploadCheckedProperty); }
            set { SetValue(IsUploadCheckedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsUploadChecked.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsUploadCheckedProperty =
            DependencyProperty.Register("IsUploadChecked", typeof(bool), typeof(SHHTS_Navigation), new PropertyMetadata(false));



        public bool IsOperationChecked
        {
            get { return (bool)GetValue(IsOperationCheckedProperty); }
            set { SetValue(IsOperationCheckedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsOperationChecked.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsOperationCheckedProperty =
            DependencyProperty.Register("IsOperationChecked", typeof(bool), typeof(SHHTS_Navigation), new PropertyMetadata(false));


        
    }
}
