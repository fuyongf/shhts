﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls
{
    /// <summary>
    /// PartOneUserControl.xaml 的交互逻辑
    /// </summary>
    public partial class PartOneUserControl : UserControl
    {
        public PartOneUserControl()
        {
            InitializeComponent();
        }


        public int LeftLineWidth
        {
            get { return (int)GetValue(LeftLineWidthProperty); }
            set { SetValue(LeftLineWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LeftLineWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LeftLineWidthProperty =
            DependencyProperty.Register("LeftLineWidth", typeof(int), typeof(PartOneUserControl), new PropertyMetadata(0));
   

        public int RightLineWidth
        {
            get { return (int)GetValue(RightLineWidthProperty); }
            set { SetValue(RightLineWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for RightLineWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RightLineWidthProperty =
            DependencyProperty.Register("RightLineWidth", typeof(int), typeof(PartOneUserControl), new PropertyMetadata(0));

        

        public string StatusTime
        {
            get { return (string)GetValue(StatusTimeProperty); }
            set { SetValue(StatusTimeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for StatusTime.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StatusTimeProperty =
            DependencyProperty.Register("StatusTime", typeof(string), typeof(PartOneUserControl), new PropertyMetadata(null));


        public string StatusName
        {
            get { return (string)GetValue(StatusNameProperty); }
            set { SetValue(StatusNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for StatusName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StatusNameProperty =
            DependencyProperty.Register("StatusName", typeof(string), typeof(PartOneUserControl), new PropertyMetadata(null));



        public bool IsReadyable
        {
            get { return (bool)GetValue(IsReadyableProperty); }
            set { SetValue(IsReadyableProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsReadyable.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsReadyableProperty =
            DependencyProperty.Register("IsReadyable", typeof(bool), typeof(PartOneUserControl), new PropertyMetadata(false));                   
    }
}
