﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls
{
    /// <summary>
    /// PartTwoUserControl.xaml 的交互逻辑
    /// </summary>
    public partial class PartTwoUserControl : UserControl
    {
        public PartTwoUserControl()
        {
            InitializeComponent();
        }


        public string Statusname
        {
            get { return (string)GetValue(StatusnameProperty); }
            set { SetValue(StatusnameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Statusname.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StatusnameProperty =
            DependencyProperty.Register("Statusname", typeof(string), typeof(PartTwoUserControl), new PropertyMetadata(null));


        public bool IsReadyable
        {
            get { return (bool)GetValue(IsReadyableProperty); }
            set { SetValue(IsReadyableProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsReadyable.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsReadyableProperty =
            DependencyProperty.Register("IsReadyable", typeof(bool), typeof(PartTwoUserControl), new PropertyMetadata(false));



        public string TimeInfo
        {
            get { return (string)GetValue(TimeInfoProperty); }
            set { SetValue(TimeInfoProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TimeInfo.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TimeInfoProperty =
            DependencyProperty.Register("TimeInfo", typeof(string), typeof(PartTwoUserControl), new PropertyMetadata(null));


    }
}
