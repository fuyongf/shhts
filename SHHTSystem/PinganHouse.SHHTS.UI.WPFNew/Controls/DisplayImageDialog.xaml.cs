﻿using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls
{
    /// <summary>
    /// DisplayImageDialog.xaml 的交互逻辑
    /// </summary>
    public partial class DisplayImageDialog : Window
    {
        private GaoPaiPhotosViewModel gaoPaiPhotosViewModel =null;
        public DisplayImageDialog(GaoPaiPhotosViewModel viewModel)
        {
            InitializeComponent();
            this.gaoPaiPhotosViewModel = viewModel;
            this.m_imgList.ItemsSource = viewModel.Photos;
        }
        public GaoPaiPhotosViewModel ViewModel
        {
            get
            {
                return (GaoPaiPhotosViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
        public double ScreenHeigh
        {
            get { return (double)GetValue(ScreenHeighProperty); }
            set { SetValue(ScreenHeighProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScreenHeigh.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScreenHeighProperty =
            DependencyProperty.Register("ScreenHeigh", typeof(double), typeof(DisplayImageDialog), new PropertyMetadata(SystemParameters.PrimaryScreenHeight));



        public double ScreenWidth
        {
            get { return (double)GetValue(ScreenWidthProperty); }
            set { SetValue(ScreenWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScreenWidth.  This enables animation, styling, binding, etc...SystemParameters.WorkArea.Width
        public static readonly DependencyProperty ScreenWidthProperty =
            DependencyProperty.Register("ScreenWidth", typeof(double), typeof(DisplayImageDialog), new PropertyMetadata(SystemParameters.PrimaryScreenWidth));

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var s = sender as Button;
            if(s ==null)
            {
                return;
            }
            ImageBrowse img = new ImageBrowse(gaoPaiPhotosViewModel.Photos, (int)s.CommandParameter);
            img.ShowDialog();
        }

        private void AllUnchecked_Click(object sender, RoutedEventArgs e)
        {
            foreach (var ck in gaoPaiPhotosViewModel.Photos)
            {
                var s = ck as PhotosViewModel;
                if (s == null)
                {
                    return;
                }
                s.IsChecked = !s.IsChecked;
            }
        }

        private void CloseCurrentWindow_Click(object sender, RoutedEventArgs e)
        {
            DeleteImageList(gaoPaiPhotosViewModel.Photos.ToList());
            this.Close();
        }

        private void UpLoad_Click(object sender, RoutedEventArgs e)
        {
            int count = gaoPaiPhotosViewModel.Photos.Where(x => x.IsChecked == true).Count();
            if (count == 0)
            {
                MessageBox.Show("请选择要上传的选项!", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            //上传选中的图片
            ViewModel.IsUploadFinished = true;
            var uncheckImageList = gaoPaiPhotosViewModel.Photos.Where(x => x.IsChecked == false).ToList();
            //删除未选择的图片
            DeleteImageList(uncheckImageList);
            this.Close();
        }

        private void DeleteImageList(List<PhotosViewModel> uncheckImageList)
        {
            foreach (var item in uncheckImageList)
            {
                try
                {
                    Dispatcher.BeginInvoke(new Action(() =>
                    {
                        if (File.Exists(item.Tag))
                        {
                            item.ImageSource.StreamSource.Close();
                            item.ImageSource.StreamSource.Dispose();
                            File.Delete(item.Tag);
                        }

                    }), System.Windows.Threading.DispatcherPriority.Background);
                }
                catch
                {
                    MessageBox.Show("删除失败，请重新删除", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                    item.ImageSource.StreamSource.Close();
                    item.ImageSource.StreamSource.Dispose();
                }
            }
        }
    }
}
