﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls.FundControl
{
    /// <summary>
    /// TextBoxControl.xaml 的交互逻辑
    /// </summary>
    public partial class TextBoxControl : UserControl
    {
        public TextBoxControl()
        {
            InitializeComponent();
        }

        public string  Text1
        {
            get { return (string )GetValue(Text1Property); }
            set { SetValue(Text1Property, value); }
        }

        // Using a DependencyProperty as the backing store for Text1.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty Text1Property =
            DependencyProperty.Register("Text1", typeof(string), typeof(TextBoxControl), new PropertyMetadata(string.Empty));



        public string Text2
        {
            get { return (string)GetValue(Text2Property); }
            set { SetValue(Text2Property, value); }
        }

        // Using a DependencyProperty as the backing store for Text2.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty Text2Property =
            DependencyProperty.Register("Text2", typeof(string), typeof(TextBoxControl), new PropertyMetadata(string.Empty));



        public int TbWidth
        {
            get { return (int)GetValue(TbWidthProperty); }
            set { SetValue(TbWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TbWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TbWidthProperty =
            DependencyProperty.Register("TbWidth", typeof(int), typeof(TextBoxControl), new PropertyMetadata(0));

        
    }
}
