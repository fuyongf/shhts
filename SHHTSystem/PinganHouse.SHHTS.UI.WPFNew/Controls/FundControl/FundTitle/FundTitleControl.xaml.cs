﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls
{
    /// <summary>
    /// FundTitleControl.xaml 的交互逻辑
    /// </summary>
    public partial class FundTitleControl : UserControl
    {
        public FundTitleControl()
        {
            InitializeComponent();
        }
        #region 注册属性


        public bool IsAllOrder
        {
            get { return (bool)GetValue(IsAllOrderProperty); }
            set { SetValue(IsAllOrderProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsAllOrder.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsAllOrderProperty =
            DependencyProperty.Register("IsAllOrder", typeof(bool), typeof(FundTitleControl), new PropertyMetadata(false ));



        public bool IsMonBook
        {
            get { return (bool)GetValue(IsMonBookProperty); }
            set { SetValue(IsMonBookProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsMonBook.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsMonBookProperty =
            DependencyProperty.Register("IsMonBook", typeof(bool), typeof(FundTitleControl), new PropertyMetadata(false ));



        public bool IsMonFlow
        {
            get { return (bool)GetValue(IsMonFlowProperty); }
            set { SetValue(IsMonFlowProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsMonFlow.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsMonFlowProperty =
            DependencyProperty.Register("IsMonFlow", typeof(bool), typeof(FundTitleControl), new PropertyMetadata(false));



        public bool IsBillManege
        {
            get { return (bool)GetValue(IsBillManegeProperty); }
            set { SetValue(IsBillManegeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsBillManege.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsBillManegeProperty =
            DependencyProperty.Register("IsBillManege", typeof(bool), typeof(FundTitleControl), new PropertyMetadata(false));


        public bool IsRefund
        {
            get { return (bool)GetValue(IsRefundProperty); }
            set { SetValue(IsRefundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsRefund.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsRefundProperty =
            DependencyProperty.Register("IsRefund", typeof(bool), typeof(FundTitleControl), new PropertyMetadata(false));

        

        

        
        
        #endregion
    }
}
