﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.PubSubEvents;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls
{
  
   public class FundTitleControlModel : BaseViewModel
    {
       private readonly IEventAggregator eventAggregator;

       public FundTitleControlModel(IEventAggregator eventAggregator)
       {
           if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
           this.eventAggregator = eventAggregator;
           this.AllOrderCommand = new DelegateCommand<object>(this.OnAllOrderCommand);
           this.MoneyBookCommand = new DelegateCommand<object>(this.OnMoneyBookCommand);
           this.MoneyFlowCommand = new DelegateCommand<object>(this.OnMoneyFlowCommand);
           this.BillManageCommand = new DelegateCommand<object>(this.OnBillManageCommand);
           this.RefundCommand = new DelegateCommand<object>(this.OnRefundCommand);
       }

       #region Command
       public ICommand AllOrderCommand { get; private set; }

       private void OnAllOrderCommand(object model)
       {
           this.eventAggregator.GetEvent<AllOrderEvents>().Publish(model);
       }

       public ICommand MoneyBookCommand { get; private set; }

       private void OnMoneyBookCommand(object model)
       {
           this.eventAggregator.GetEvent<MoneyBookEvents>().Publish(model);
       }
       public ICommand MoneyFlowCommand { get; private set; }

       private void OnMoneyFlowCommand(object model)
       {
           this.eventAggregator.GetEvent<MoneyFlowEvents>().Publish(model);
       }
       public ICommand BillManageCommand { get; private set; }

       private void OnBillManageCommand(object model)
       {
           this.eventAggregator.GetEvent<BillManageEvents>().Publish(model);
       }
       public ICommand RefundCommand { get; private set; }

       private void OnRefundCommand(object model)
       {
           this.eventAggregator.GetEvent<RefundEvents>().Publish(model);
       }
       #endregion
    }
}
