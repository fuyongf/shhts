﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls.FundControl.BasicInfo
{
    public class BasicClass : BaseViewModel
    {
        /// <summary>
        /// 产权证号
        /// </summary>
        private string num;
        public string Num
        {
            get
            {
                return num;
            }
            set
            {
                if (num != value)
                {
                    num = value;
                    NotifyPropertyChanged("Num");
                }
            }
        }
         /// <summary>
        /// 卖方
        /// </summary>
        private string buyer;
        public string Buyer
        {
            get
            {
                return buyer;
            }
            set
            {
                if (buyer != value)
                {
                    buyer = value;
                    NotifyPropertyChanged("Buyer");
                }
            }
        }
         /// <summary>
        /// 物业地址
        /// </summary>
        private string address;
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                if (address != value)
                {
                    address = value;
                    NotifyPropertyChanged("Address");
                }
            }
        }
         /// <summary>
        /// 买方
        /// </summary>
        private string seller;

        public string Seller
        {
            get { return seller; }
            set
            {
                if (seller != value)
                {
                    seller = value;
                    NotifyPropertyChanged("Seller");
                }
            }
        }

        /// <summary>
        /// 中介公司
        /// </summary>
       private string company;
        public string Company
        {
            get
            {
                return company;
            }
            set
            {
                if (company != value)
                {
                    company = value;
                    NotifyPropertyChanged("Company");
                }
            }
        }
       
         /// <summary>
        /// 经纪人
        /// </summary>
       private string agent;

        public string Agent
        {
            get { return agent; }
            set
            {
                if (agent != value)
                {
                    agent = value;
                    NotifyPropertyChanged("Agent");
                }
            }
        }

    }
}
