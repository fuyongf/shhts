﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls.FundControl.BasicInfo
{
    /// <summary>
    /// BasicInfoControl.xaml 的交互逻辑
    /// </summary>
    public partial class BasicInfoControl : UserControl
    {
        public BasicInfoControl()
        {
            InitializeComponent();
        }


        public BasicClass BasicInfos
        {
            get { return (BasicClass)GetValue(BBasicInfosProperty); }
            set { SetValue(BBasicInfosProperty, value); }
        }

        // Using a DependencyProperty as the backing store for BasicInfos.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BBasicInfosProperty =
            DependencyProperty.Register("BBasicInfos", typeof(BasicClass), typeof(BasicInfoControl), new PropertyMetadata(new BasicClass()));

        
    }
}
