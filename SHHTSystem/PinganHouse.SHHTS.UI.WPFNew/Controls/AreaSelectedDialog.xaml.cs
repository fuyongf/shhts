﻿using PinganHouse.SHHTS.UI.WPFNew.ViewModels;
using System.Windows;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls
{
    /// <summary>
    /// AreaSelectedDialog.xaml 的交互逻辑
    /// </summary>
    public partial class AreaSelectedDialog : Window
    {
        public AreaSelectedDialog(LoginViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
        }
        public LoginViewModel ViewModel
        {
            get
            {
                return (LoginViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
        public double ScreenHeigh
        {
            get { return (double)GetValue(ScreenHeighProperty); }
            set { SetValue(ScreenHeighProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScreenHeigh.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScreenHeighProperty =
            DependencyProperty.Register("ScreenHeigh", typeof(double), typeof(AreaSelectedDialog), new PropertyMetadata(SystemParameters.PrimaryScreenHeight));



        public double ScreenWidth
        {
            get { return (double)GetValue(ScreenWidthProperty); }
            set { SetValue(ScreenWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScreenWidth.  This enables animation, styling, binding, etc...SystemParameters.WorkArea.Width
        public static readonly DependencyProperty ScreenWidthProperty =
            DependencyProperty.Register("ScreenWidth", typeof(double), typeof(AreaSelectedDialog), new PropertyMetadata(SystemParameters.PrimaryScreenWidth));

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
