﻿using System.Windows;
using System.Windows.Controls;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls
{
    public class InfoTextBoxControl : TextBox
    {
        static InfoTextBoxControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(InfoTextBoxControl), new FrameworkPropertyMetadata(typeof(InfoTextBoxControl)));
            TextProperty.OverrideMetadata(typeof(InfoTextBoxControl), new FrameworkPropertyMetadata(new PropertyChangedCallback(TextPropertyChanged)));
        }

        public static readonly DependencyProperty TextBoxInfoProperty = DependencyProperty.Register(
            "TextBoxInfo",
            typeof(string),
            typeof(InfoTextBoxControl),
            new PropertyMetadata(string.Empty));

        public string TextBoxInfo
        {
            get { return (string)GetValue(TextBoxInfoProperty); }
            set { SetValue(TextBoxInfoProperty, value); }
        }

        private static readonly DependencyPropertyKey HasTextPropertyKey = DependencyProperty.RegisterReadOnly(
            "HasText",
            typeof(bool),
            typeof(InfoTextBoxControl),
            new FrameworkPropertyMetadata(false));

        public static readonly DependencyProperty HasTextProperty = HasTextPropertyKey.DependencyProperty;

        public bool HasText
        {
            get
            {
                return (bool)GetValue(HasTextProperty);
            }
        }

        static void TextPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            InfoTextBoxControl itb = (InfoTextBoxControl)sender;

            bool actuallyHasText = itb.Text.Length > 0;
            if (actuallyHasText != itb.HasText)
            {
                itb.SetValue(HasTextPropertyKey, actuallyHasText);
            }
        }
    }
}
