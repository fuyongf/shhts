﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PinganHouse.SHHTS.RemoteServiceProxy;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls.Nuclear
{
    /// <summary>
    /// ConfrimReceiveCaseDialog.xaml 的交互逻辑
    /// </summary>
    public partial class ConfrimReceiveCaseDialog : Window
    {
        public ConfrimReceiveCaseDialog(string caseId)
        {
            InitializeComponent();
            var caseDto = CaseProxyService.GetInstanse().GetCaseByCaseId(caseId);
            if (caseDto != null)
            {
                tb_caseId.Text = caseDto.CaseId;
                tb_cqzh.Text = caseDto.TenementContract;
            }
            
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        public double ScreenHeigh
        {
            get { return (double)GetValue(ScreenHeighProperty); }
            set { SetValue(ScreenHeighProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScreenHeigh.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScreenHeighProperty =
            DependencyProperty.Register("ScreenHeigh", typeof(double), typeof(ConfrimReceiveCaseDialog), new PropertyMetadata(SystemParameters.PrimaryScreenHeight));



        public double ScreenWidth
        {
            get { return (double)GetValue(ScreenWidthProperty); }
            set { SetValue(ScreenWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScreenWidth.  This enables animation, styling, binding, etc...SystemParameters.WorkArea.Width
        public static readonly DependencyProperty ScreenWidthProperty =
            DependencyProperty.Register("ScreenWidth", typeof(double), typeof(ConfrimReceiveCaseDialog), new PropertyMetadata(SystemParameters.PrimaryScreenWidth));


        private void BtnCancle_OnClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void BtnOk_OnClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
