﻿using System.Windows;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls.Nuclear
{
    /// <summary>
    /// ConfirmContractDialog.xaml 的交互逻辑
    /// </summary>
    public partial class ConfirmContractDialog : Window
    {
        public ConfirmContractDialog(ConfirmContractDialogViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
        }

        public ConfirmContractDialogViewModel ViewModel
        {
            get
            {
                return (ConfirmContractDialogViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }


        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        public double ScreenHeigh
        {
            get { return (double)GetValue(ScreenHeighProperty); }
            set { SetValue(ScreenHeighProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScreenHeigh.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScreenHeighProperty =
            DependencyProperty.Register("ScreenHeigh", typeof(double), typeof(ConfirmContractDialog), new PropertyMetadata(SystemParameters.PrimaryScreenHeight));



        public double ScreenWidth
        {
            get { return (double)GetValue(ScreenWidthProperty); }
            set { SetValue(ScreenWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScreenWidth.  This enables animation, styling, binding, etc...SystemParameters.WorkArea.Width
        public static readonly DependencyProperty ScreenWidthProperty =
            DependencyProperty.Register("ScreenWidth", typeof(double), typeof(ConfirmContractDialog), new PropertyMetadata(SystemParameters.PrimaryScreenWidth));


        private void BtnCancle_OnClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void BtnOk_OnClick(object sender, RoutedEventArgs e)
        {
            
            this.DialogResult = true;
            
        }
    }
}
