﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls.Nuclear
{
    /// <summary>
    /// BankChioseControl.xaml 的交互逻辑
    /// </summary>
    public partial class BankChioseControl : UserControl
    {
        public BankChioseControl()
        {
            InitializeComponent();

        }

        public string BankCode
        {
            get { return (string)GetValue(BankCodeProperty); }
            set { SetValue(BankCodeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for BankCode.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BankCodeProperty =
            DependencyProperty.Register("BankCode", typeof(string), typeof(BankChioseControl), new PropertyMetadata(string.Empty));

        public string Text1
        {
            get { return (string)GetValue(Text1Property); }
            set { SetValue(Text1Property, value); }
        }
        public string Text2
        {
            get { return (string)GetValue(Text2Property); }
            set { SetValue(Text2Property, value); }
        }
        public string Text3
        {
            get { return (string)GetValue(Text3Property); }
            set { SetValue(Text3Property, value); }
        }
        public string Text4
        {
            get { return (string)GetValue(Text4Property); }
            set { SetValue(Text4Property, value); }
        }

        public static readonly DependencyProperty Text1Property =
          DependencyProperty.Register("Text1", typeof(string), typeof(BankChioseControl), new PropertyMetadata(string.Empty));
        public static readonly DependencyProperty Text2Property =
          DependencyProperty.Register("Text2", typeof(string), typeof(BankChioseControl), new PropertyMetadata(string.Empty));
        public static readonly DependencyProperty Text3Property =
          DependencyProperty.Register("Text3", typeof(string), typeof(BankChioseControl), new PropertyMetadata(string.Empty));
        public static readonly DependencyProperty Text4Property =
          DependencyProperty.Register("Text4", typeof(string), typeof(BankChioseControl), new PropertyMetadata(string.Empty));


        public string SingleTextWidth
        {
            get { return (string)GetValue(SingleTextWidthProperty); }
            set { SetValue(SingleTextWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SingleTextWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SingleTextWidthProperty =
            DependencyProperty.Register("SingleTextWidth", typeof(string), typeof(BankChioseControl), new PropertyMetadata(string.Empty));
        public string SingleTextWidth1
        {
            get { return (string)GetValue(SingleTextWidth1Property); }
            set { SetValue(SingleTextWidth1Property, value); }
        }


        public string SingleTextWidth2
        {
            get { return (string)GetValue(SingleTextWidth2Property); }
            set { SetValue(SingleTextWidth2Property, value); }
        }

        // Using a DependencyProperty as the backing store for SingleTextWidth2.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SingleTextWidth2Property =
            DependencyProperty.Register("SingleTextWidth2", typeof(string), typeof(BankChioseControl), new PropertyMetadata(string.Empty));

        // Using a DependencyProperty as the backing store for SingleTextWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SingleTextWidth1Property =
            DependencyProperty.Register("SingleTextWidth1", typeof(string), typeof(BankChioseControl), new PropertyMetadata(string.Empty));

        public string TextWidth
        {
            get { return (string)GetValue(TextWidthProperty); }
            set { SetValue(TextWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SingleTextWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextWidthProperty =
            DependencyProperty.Register("TextWidth", typeof(string), typeof(BankChioseControl), new PropertyMetadata(string.Empty));

        public string TextWidth2
        {
            get { return (string)GetValue(TextWidth2Property); }
            set { SetValue(TextWidth2Property, value); }
        }

        // Using a DependencyProperty as the backing store for SingleTextWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextWidth2Property =
            DependencyProperty.Register("TextWidth2", typeof(string), typeof(BankChioseControl), new PropertyMetadata(string.Empty));

        #region 银行卡
        /// <summary>
        /// 格式化银行卡号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BankText_TextChanged(object sender, TextChangedEventArgs e)
        {
            var textBox = sender as TextBox;
            var txt = BankAccountFormat(textBox.Text.Trim());
            textBox.Text = txt.TrimEnd();
            textBox.Select(txt.Length, 0);
        }
        /// <summary>
        /// 格式化银行卡显示
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        private string BankAccountFormat(string txt)
        {
            try
            {
                if (txt == null) return txt;
                txt = txt.Replace(" ", "");
                var size = txt.Length / 4;
                var newstr = new StringBuilder();
                for (int n = 0; n < size; n++)
                {
                    newstr.Append(txt.Substring(n * 4, 4));
                    newstr.Append(" ");
                }
                newstr.Append(txt.Substring(size * 4, txt.Length % 4));
                return newstr.ToString();
            }
            catch (Exception)
            {
                return txt;
            }
        }



        /// <summary>
        /// 验证输入字符
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;

            //屏蔽非法按键
            if ((e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) || e.Key == Key.Decimal)
            {
                if (txt.Text.Contains(".") && e.Key == Key.Decimal)
                {
                    e.Handled = true;
                    return;
                }
                e.Handled = false;
            }
            else if (((e.Key >= Key.D0 && e.Key <= Key.D9) || e.Key == Key.OemPeriod) && e.KeyboardDevice.Modifiers != ModifierKeys.Shift)
            {
                if (txt.Text.Contains(".") && e.Key == Key.OemPeriod)
                {
                    e.Handled = true;
                    return;
                }
                e.Handled = false;
            }
            else if (e.Key == Key.Tab)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }


        private void ButtonChiose_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var tag = sender as Button;
                var bankDialog = new BankDialog(Convert.ToString(tag));
                bankDialog.SelectBankEvent += bankDialog_SelectBankEvent;
                bankDialog.ClearBankEvent += bankDialog_ClearBankEvent;
                bankDialog.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("选择银行操作发生系统异常:" + ex.Message, "系统异常", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void bankDialog_SelectBankEvent(string bankCode, string bankName, string tag)
        {
            Text1 = bankName;
            txt_bankName.Tag = bankCode;
        }
        private void bankDialog_ClearBankEvent(string tag)
        {
            Text1 = string.Empty;
            Text2 = string.Empty;
            Text3 = string.Empty;
            Text4 = string.Empty;
        }

        #endregion



    }
}
