﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Threading;
using System.Windows.Documents;
using NPOI.SS.Formula.Functions;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls.Nuclear
{
    /// <summary>
    /// ComboBoxWithTextControl.xaml 的交互逻辑
    /// </summary>
    public partial class ComboBoxWithTextControl : UserControl
    {
        public ComboBoxWithTextControl()
        {
            InitializeComponent();
            var itemList = new List<int>();
            for (var i = 1; i <= 50; i++)
            {
                itemList.Add(i);
            }
            ComboBox_ex.ItemsSource = itemList;
        }

        public bool IsEditable1
        {
            get { return (bool)GetValue(IsEditable1Property); }
            set { SetValue(IsEditable1Property, value); }
        }

        public static readonly DependencyProperty IsEditable1Property =
            DependencyProperty.Register("IsEditable1", typeof(bool), typeof(ComboBoxWithTextControl), new PropertyMetadata(false));

        public string TempText
        {
            get { return (string)GetValue(TempTextProperty); }
            set { SetValue(TempTextProperty, value); }
        }

        public static readonly DependencyProperty TempTextProperty =
            DependencyProperty.Register("TempText", typeof(string), typeof(ComboBoxWithTextControl), new PropertyMetadata(string.Empty));
        public string ComboWidth
        {
            get { return (string)GetValue(ComboWidthProperty); }
            set { SetValue(ComboWidthProperty, value); }
        }

        public static readonly DependencyProperty ComboWidthProperty =
            DependencyProperty.Register("ComboWidth", typeof(string), typeof(ComboBoxWithTextControl), new PropertyMetadata(string.Empty));


        public string Text1
        {
            get { return (string)GetValue(Text1Property); }
            set { SetValue(Text1Property, value); }
        }

        public static readonly DependencyProperty Text1Property =
          DependencyProperty.Register("Text1", typeof(string), typeof(ComboBoxWithTextControl), new PropertyMetadata(string.Empty));

        public int Text1With
        {
            get { return (int)GetValue(Text1WithProperty); }
            set { SetValue(Text1WithProperty, value); }
        }

        public static readonly DependencyProperty Text1WithProperty =
          DependencyProperty.Register("Text1With", typeof(int), typeof(ComboBoxWithTextControl), new PropertyMetadata(0));

        public string ComboName
        {
            get { return (string)GetValue(ComboNameProperty); }
            set { SetValue(ComboNameProperty, value); }
        }

        public static readonly DependencyProperty ComboNameProperty =
            DependencyProperty.Register("ComboName", typeof(string), typeof(ComboBoxWithTextControl), new PropertyMetadata(string.Empty));


        #region 数字约束
        /// <summary>
        /// 验证输入字符
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;

            //屏蔽非法按键
            if ((e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) || e.Key == Key.Decimal)
            {
                if (txt.Text.Contains(".") && e.Key == Key.Decimal)
                {
                    e.Handled = true;
                    return;
                }
                e.Handled = false;
            }
            else if (((e.Key >= Key.D0 && e.Key <= Key.D9) || e.Key == Key.OemPeriod) && e.KeyboardDevice.Modifiers != ModifierKeys.Shift)
            {
                if (txt.Text.Contains(".") && e.Key == Key.OemPeriod)
                {
                    e.Handled = true;
                    return;
                }
                e.Handled = false;
            }
            else if (e.Key == Key.Tab)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        

        /// <summary>
        /// 验证是否数字输入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text_TextChanged(object sender, TextChangedEventArgs e)
        {
            //屏蔽中文输入和非法字符粘贴输入
            TextBox textBox = sender as TextBox;
            TextChange[] change = new TextChange[e.Changes.Count];
            e.Changes.CopyTo(change, 0);

            int offset = change[0].Offset;
            if (change[0].AddedLength > 0)
            {
                double num = 0;
                if (!double.TryParse(textBox.Text, out num))
                {
                    ThreadPool.QueueUserWorkItem((obj) =>
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            textBox.Text = string.Empty;
                            textBox.Select(offset, 0);

                        });
                    });
                }
            }
        } 
        #endregion

        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Text1 = ComboBox_ex.SelectedValue.ToString();
            //throw new NotImplementedException();
        }
    }
}
