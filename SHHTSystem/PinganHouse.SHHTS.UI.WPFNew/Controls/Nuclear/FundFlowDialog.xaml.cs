﻿using PinganHouse.SHHTS.UI.WPFNew.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls.Nuclear
{
    /// <summary>
    /// FundFlowDiaolg.xaml 的交互逻辑
    /// </summary>
    public partial class FundFlowDialog : Window
    {
        public FundFlowDialog()
        {
            InitializeComponent();
        }

        public FundFlowDialogViewModel ViewModel
        {
            get
            {
                return (FundFlowDialogViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public double ScreenHeigh
        {
            get { return (double)GetValue(ScreenHeighProperty); }
            set { SetValue(ScreenHeighProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScreenHeigh.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScreenHeighProperty =
            DependencyProperty.Register("ScreenHeigh", typeof(double), typeof(CancelContractDialog), new PropertyMetadata(SystemParameters.PrimaryScreenHeight));



        public double ScreenWidth
        {
            get { return (double)GetValue(ScreenWidthProperty); }
            set { SetValue(ScreenWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScreenWidth.  This enables animation, styling, binding, etc...SystemParameters.WorkArea.Width
        public static readonly DependencyProperty ScreenWidthProperty =
            DependencyProperty.Register("ScreenWidth", typeof(double), typeof(CancelContractDialog), new PropertyMetadata(SystemParameters.PrimaryScreenWidth));

        private void btnAddConditions_in_Click(object sender, RoutedEventArgs e)
        {
            if (stkPanelConditions2_in.Visibility == Visibility.Collapsed)
            {
                stkPanelConditions2_in.Visibility = Visibility.Visible;

            }
            else if (stkPanelConditions2_in.Visibility == Visibility.Visible)
            {
                stkPanelConditions2_in.Visibility = Visibility.Collapsed;
            }
        }

        private void btnAddConditions_out_Click(object sender, RoutedEventArgs e)
        {
            if (stkPanelConditions2_out.Visibility == Visibility.Collapsed)
            {
                stkPanelConditions2_out.Visibility = Visibility.Visible;

            }
            else if (stkPanelConditions2_out.Visibility == Visibility.Visible)
            {
                stkPanelConditions2_out.Visibility = Visibility.Collapsed;
            }
        }
    }
}
