﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls.Nuclear
{
    /// <summary>
    /// ProgressTagControl.xaml 的交互逻辑
    /// </summary>
    public partial class ProgressTagControl : UserControl
    {
        public ProgressTagControl()
        {
            InitializeComponent();
        }


        public string BoxText
        {
            get { return (string)GetValue(BoxTextProperty); }
            set { SetValue(BoxTextProperty, value); }
        }

        public string Text1
        {
            get { return (string)GetValue(Text1Property); }
            set { SetValue(Text1Property, value); }
        }

        public int BoxFontSize
        {
            get { return (int)GetValue(BoxFontSizeProperty); }
            set { SetValue(BoxFontSizeProperty, value); }
        }

        public int FontSize1
        {
            get { return (int)GetValue(FontSize1Property); }
            set { SetValue(FontSize1Property, value); }
        }


        public static readonly DependencyProperty IsBoxEnableProperty =
         DependencyProperty.Register("IsBoxEnable", typeof(bool), typeof(ProgressTagControl), new PropertyMetadata(false));

        public static readonly DependencyProperty BoxTextProperty =
            DependencyProperty.Register("BoxText", typeof(string), typeof(ProgressTagControl), new PropertyMetadata(string.Empty));

        public static readonly DependencyProperty Text1Property =
          DependencyProperty.Register("Text1", typeof(string), typeof(ProgressTagControl), new PropertyMetadata(string.Empty));

        public static readonly DependencyProperty BoxFontSizeProperty =
         DependencyProperty.Register("BoxFontSize", typeof(int), typeof(ProgressTagControl), new PropertyMetadata(0));


        public static readonly DependencyProperty FontSize1Property =
       DependencyProperty.Register("FontSize1", typeof(int), typeof(ProgressTagControl), new PropertyMetadata(0));

        public string ActiveColor
        {
            get { return (string)GetValue(ActiveColorProperty); }
            set { SetValue(ActiveColorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ActiveColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ActiveColorProperty =
            DependencyProperty.Register("ActiveColor", typeof(string), typeof(ProgressTagControl), new PropertyMetadata(string.Empty));


    }
}
