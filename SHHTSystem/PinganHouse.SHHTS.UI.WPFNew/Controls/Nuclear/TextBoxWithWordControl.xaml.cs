﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls.Nuclear
{
    /// <summary>
    /// TextBoxWithWordControl.xaml 的交互逻辑
    /// </summary>
    public partial class TextBoxWithWordControl : UserControl
    {
        public TextBoxWithWordControl()
        {
            InitializeComponent();
        }

        #region 属性
        public string Text1
        {
            get { return (string)GetValue(Text1Property); }
            set { SetValue(Text1Property, value); }
        }

        // Using a DependencyProperty as the backing store for Text1.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty Text1Property =
            DependencyProperty.Register("Text1", typeof(string), typeof(TextBoxWithWordControl), new PropertyMetadata(string.Empty));

        public string Text2
        {
            get { return (string)GetValue(Text2Property); }
            set { SetValue(Text2Property, value); }
        }

        // Using a DependencyProperty as the backing store for Text2.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty Text2Property =
            DependencyProperty.Register("Text2", typeof(string), typeof(TextBoxWithWordControl), new PropertyMetadata(string.Empty));

        public int TbWidth
        {
            get { return (int)GetValue(TbWidthProperty); }
            set { SetValue(TbWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TbWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TbWidthProperty =
            DependencyProperty.Register("TbWidth", typeof(int), typeof(TextBoxWithWordControl), new PropertyMetadata(0));
        #endregion

        private void Text_TextChanged(object sender, TextChangedEventArgs e)
        {
            //屏蔽中文输入和非法字符粘贴输入
            TextBox textBox = sender as TextBox;
            TextChange[] change = new TextChange[e.Changes.Count];
            e.Changes.CopyTo(change, 0);

            int offset = change[0].Offset;
            if (change[0].AddedLength > 0)
            {
                double num = 0;
                if (!double.TryParse(textBox.Text, out num))
                {
                    ThreadPool.QueueUserWorkItem((obj) =>
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            textBox.Text = string.Empty;
                            textBox.Select(offset, 0);

                        });
                    });
                }
            }
        }

        private void Text_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;

            //屏蔽非法按键
            if ((e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) || e.Key == Key.Decimal)
            {
                if (txt.Text.Contains(".") && e.Key == Key.Decimal)
                {
                    e.Handled = true;
                    return;
                }
                e.Handled = false;
            }
            else if (((e.Key >= Key.D0 && e.Key <= Key.D9) || e.Key == Key.OemPeriod) && e.KeyboardDevice.Modifiers != ModifierKeys.Shift)
            {
                if (txt.Text.Contains(".") && e.Key == Key.OemPeriod)
                {
                    e.Handled = true;
                    return;
                }
                e.Handled = false;
            }
            else if (e.Key == Key.Tab)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// 数字键移除焦点格式化赋值
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NumText_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                var textBox = sender as TextBox;
                var txt = textBox.Text.Trim();
                decimal num;
                if(this.Text2!="元") return;
                if (decimal.TryParse(txt, out num))
                {
                    txt = string.Format("{0:N}", num);
                    textBox.Text = txt;
                }
            }
            catch (Exception) { }
        }


    }
}
