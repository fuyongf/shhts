﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.RemoteServiceProxy;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls.Nuclear
{
    /// <summary>
    /// AccountAddDialog.xaml 的交互逻辑
    /// </summary>
    public partial class AccountAddDialog : Window
    {
        private static IList<BankIdentifyCfg> BankCfgs = ConfigServiceProxy.GetIdentifyBankCfgs();

        private static IDictionary BankDictionary = ConfigServiceProxy.GetBanks();

      


        public AccountAddDialog(string tag, bool isMainAccountExist = false)
        {
            InitializeComponent();
            tbTitle.Text = tag;
            mainAccount.IsChecked = true;
            mainAccountPanel.IsEnabled = !isMainAccountExist;
            BankList.ItemsSource = BankDictionary;

            BankList.DisplayMemberPath = "Value";
            BankList.SelectedValuePath = "Key";
            // 判定鉴权配置
            if (IsAuthentication.IsChecked ?? false)
                ChangeCfg();

           
        }

        #region 属性


        public double ScreenHeigh
        {
            get { return (double)GetValue(ScreenHeighProperty); }
            set { SetValue(ScreenHeighProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScreenHeigh.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScreenHeighProperty =
            DependencyProperty.Register("ScreenHeigh", typeof(double), typeof(AccountAddDialog), new PropertyMetadata(SystemParameters.PrimaryScreenHeight));

        public double ScreenWidth
        {
            get { return (double)GetValue(ScreenWidthProperty); }
            set { SetValue(ScreenWidthProperty, value); }
        }
        // Using a DependencyProperty as the backing store for ScreenWidth.  This enables animation, styling, binding, etc...SystemParameters.WorkArea.Width
        public static readonly DependencyProperty ScreenWidthProperty =
            DependencyProperty.Register("ScreenWidth", typeof(double), typeof(AccountAddDialog), new PropertyMetadata(SystemParameters.PrimaryScreenWidth));


        #endregion

        private void SK_CheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            var checkbox = sender as CheckBox;
            if (checkbox.IsChecked ?? false)
            {
                mainAccountPanel.Visibility = Visibility.Visible;
            }
            else
            {
                mainAccountPanel.Visibility = Visibility.Hidden;
            }
        }

        private void AccessName_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var comboBox = sender as ComboBox;
            if (comboBox.SelectedValue != null && comboBox.SelectedValue.ToString() == "新增")
            {
                AccessNamePanel.Visibility = Visibility.Visible;
                AccessName.Text = string.Empty;
                AccessIdCard.Text = string.Empty;
            }
            else
            {
                AccessNamePanel.Visibility = Visibility.Hidden;
            }


            // throw new NotImplementedException();
        }
        /// <summary>
        /// Cancle
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancle_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        /// <summary>
        /// OK
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (IsAuthentication.IsChecked ?? false)
            //    ChangeCfg();
        }
        /// <summary>
        /// 变更状态
        /// </summary>
        private void ChangeCfg()
        {
            var config = BankCfgs.FirstOrDefault(t => BankList.SelectedValue != null && t.BankCode == BankList.SelectedValue.ToString());
            if (config == null) return;
            //银行预留手机
            BankMobile.Visibility = config.IsNeedMobile ? Visibility.Visible : Visibility.Hidden;
            //文本提示框
            RemarkEx.Text = "鉴权成功将扣除该账号" + config.Withhold + "元手续费";
            RemarkEx.Visibility = config.IsNeedWithhold ? Visibility.Visible : Visibility.Hidden;

            Remark.Text = string.Format("{0}、{1}、{2}、{3}", config.IsNeedName ? "户名" : string.Empty, config.IsNeedBankAccount ? "账号" : string.Empty, config.IsNeedIdCard ? "身份证号" : string.Empty, config.IsNeedMobile ? "银行预留手机号" : string.Empty);
            RemarkPanel.Visibility = config.IsNeedName ? Visibility.Visible : Visibility.Hidden;
        }

        private void IsAuthentication_OnChecked(object sender, RoutedEventArgs e)
        {
            if (IsAuthentication.IsChecked ?? false)
                ChangeCfg();
            //throw new System.NotImplementedException();
        }
    }
}
