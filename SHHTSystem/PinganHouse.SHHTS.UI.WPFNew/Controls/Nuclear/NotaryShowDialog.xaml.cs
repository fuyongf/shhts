﻿using System;
using System.Dynamic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Microsoft.Practices.Prism.ViewModel;
using NPOI.SS.UserModel;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.DataTransferObjects.Filters;
using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPFNew.Common;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls.Nuclear
{
    /// <summary>
    /// NotaryShowDialog.xaml 的交互逻辑
    /// </summary>
    public partial class NotaryShowDialog : Window
    {

        public NotaryShowDialog()
        {
            InitializeComponent();

            var servers = NotaryProxyService.GetInstanse().Search(null);

            Actb_Notary.AddItem(new AutoCompleteEntry("新增", null, null));

            foreach (var source in servers.ToList())
            {
                Actb_Notary.AddItem(new AutoCompleteEntry(string.Format("{0},{1}", source.Name, source.Mobile), source, source.Name,source.Mobile));
            }
        }

        public void InitDate(long sysNo)
        {
            if (sysNo != 0)
            {
                notaryInfo = NotaryProxyService.GetInstanse().Get(sysNo);
                _notarySysNo = notaryInfo.SysNo;
                NotaryName.Text = notaryInfo.Name;
                NotaryTel.Text = notaryInfo.Mobile;
                NotaryOffice.Text = notaryInfo.OfficeName;

                Actb_Notary.Text = string.Format("{0},{1}", NotaryName.Text, NotaryTel.Text);
            }
        }

        #region 属性

        private static long _notarySysNo;

        private static NotaryDto notaryInfo;

        public double ScreenHeigh
        {
            get { return (double)GetValue(ScreenHeighProperty); }
            set { SetValue(ScreenHeighProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScreenHeigh.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScreenHeighProperty =
            DependencyProperty.Register("ScreenHeigh", typeof(double), typeof(NotaryShowDialog), new PropertyMetadata(SystemParameters.PrimaryScreenHeight));

        public double ScreenWidth
        {
            get { return (double)GetValue(ScreenWidthProperty); }
            set { SetValue(ScreenWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScreenWidth.  This enables animation, styling, binding, etc...SystemParameters.WorkArea.Width
        public static readonly DependencyProperty ScreenWidthProperty =
            DependencyProperty.Register("ScreenWidth", typeof(double), typeof(NotaryShowDialog), new PropertyMetadata(SystemParameters.PrimaryScreenWidth));

        #endregion

        #region 事件
        //声明和注册路由事件
        public static readonly RoutedEvent SelectContentRoutedEvent =
            EventManager.RegisterRoutedEvent("SelectContentChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(AutoCompleteTextBox));

        public event RoutedEventHandler SelectContentChanged
        {
            add { this.AddHandler(SelectContentRoutedEvent, value); }
            remove { this.RemoveHandler(SelectContentRoutedEvent, value); }
        }

        public delegate void SelectContentHandle(long notarySysNo, string notaryName, string notaryMobile);
        //确定预约事件
        public event SelectContentHandle SelectContentEvent;

        protected virtual void OnSelectContentEvent(long notarySysNo, string notaryName, string notaryMobile)
        {
            if (SelectContentEvent != null) { SelectContentEvent.Invoke(notarySysNo, notaryName, notaryMobile); }
        }

        /// <summary>
        /// 点击关闭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        /// <summary>
        /// 点击OK
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //判定是否新增
                if (Actb_Notary.Text == "新增")
                {
                   var result=  NotaryProxyService.GetInstanse().Insert( Name = NotaryName.Text.Trim(),  NotaryTel.Text.Trim(),  NotaryOffice.Text.Trim(),LoginHelper.CurrentUser.SysNo);
                   if (result != null)
                   {
                       _notarySysNo = result.SysNo;
                       this.DialogResult = true;
                   }
                   else
                   {
                       MessageBox.Show("新增公证员失败！！");
                   }
                }
                else
                {
                    if (_notarySysNo != 0)
                    {
                        if (!IsChanged())
                        {
                            this.DialogResult = true;
                            return; 
                        }                         
                       var result= NotaryProxyService.GetInstanse().Update(_notarySysNo,
                               NotaryName.Text.Trim(),
                               NotaryTel.Text.Trim(),
                                NotaryOffice.Text.Trim(),
                                LoginHelper.CurrentUser.SysNo);

                       if (result != null&&result.Success)
                       {
                           this.DialogResult = true;
                       }
                        else
                       {
                           MessageBox.Show("修改公证员失败！！");
                       }

                        
                    }
                    else
                    {
                        MessageBox.Show("请选择公证员！！");
                        this.DialogResult = false;
                    }
                }

            }
            catch (Exception exception)
            {
                MessageBox.Show("系统出现错误，" + exception.Message);
                this.DialogResult = false;
            }
            finally
            {
                OnSelectContentEvent(_notarySysNo, NotaryName.Text, NotaryTel.Text);
                this.Close();
            }

        }

        private bool IsChanged()
        {
            return !(NotaryName.Text == notaryInfo.Name && NotaryTel.Text == notaryInfo.Mobile &&
                   NotaryOffice.Text == notaryInfo.OfficeName);
        }

        /// <summary>
        /// 点击取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancle_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        //private static VisualBrush _vbBrush = new VisualBrush
        //{
        //    TileMode = TileMode.None,
        //    Opacity = 0.3,
        //    Stretch = Stretch.None,
        //    AlignmentX = AlignmentX.Left,
        //    Visual = new TextBlock { FontStyle = FontStyles.Normal, Text = "请选择", Padding = new Thickness(10, 10, 10, 10) }
        //};


        private void AutoCompleteTextBox_OnSelectionChanged(object sender, RoutedEventArgs e)
        {

            var actb = sender as AutoCompleteTextBox;
            notaryInfo = actb.GetAutoCompleteEntry.Parameter as NotaryDto;
            if (notaryInfo != null)
            {
                _notarySysNo = notaryInfo.SysNo;
                NotaryName.Text = notaryInfo.Name;
                NotaryTel.Text = notaryInfo.Mobile;
                NotaryOffice.Text = notaryInfo.OfficeName;
            }
            if (actb.Text != "新增") return;
            NotaryName.Text = string.Empty;
            NotaryTel.Text = string.Empty;
            NotaryOffice.Text = string.Empty;
        }
        #endregion


    }
}
