﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Practices.Prism.PubSubEvents;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels.NuclearModel;
using Button = System.Windows.Controls.Button;
using UserControl = System.Windows.Controls.UserControl;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls
{
    /// <summary>
    /// CaseNextActionControl.xaml 的交互逻辑
    /// </summary>
    public partial class CaseNextActionControl : UserControl
    {

        public CaseNextActionControl()
        {
            InitializeComponent();

        }

       
        #region NextAction
        public Tuple<IList<string>, string> NextAction
        {
            get { return (Tuple<IList<string>, string>)GetValue(NextActionProperty); }
            set { SetValue(NextActionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for NextAction.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NextActionProperty =
            DependencyProperty.Register("NextAction", typeof(Tuple<IList<string>, string>), typeof(CaseNextActionControl),
                new PropertyMetadata(null, new PropertyChangedCallback(OnPropertyChanged)));
                

        static void OnPropertyChanged(object sender, DependencyPropertyChangedEventArgs args)
        {
            if (sender != null && sender is CaseNextActionControl)
            {
                CaseNextActionControl source = (CaseNextActionControl)sender;
                if (source != null)
                {
                    string caseId = source.NextAction.Item2;

                    //预检台：取消签约
                    if (source.NextAction.Item1.Contains("ZB5_ZB2"))
                    {
                        Button btn = new Button();
                        btn.SetResourceReference(StyleProperty, "ButtonStyle");
                        btn.Margin = new Thickness(5, 0, 0, 0);
                        btn.Content = "取消签约";
                        CaseDetailInfoViewModel caseDetail = Microsoft.Practices.ServiceLocation.ServiceLocator.Current.GetInstance<CaseDetailInfoViewModel>();
                        btn.Command = caseDetail.PreCheckCancerCaseCommand;
                        btn.CommandParameter = caseId;
                        source.spContainer.Children.Add(btn);
                    }
                    
                    //核案：取消签约
                    if (source.NextAction.Item1.Contains("YJ1_ZB5"))
                    {
                        Button btn = new Button();
                        btn.SetResourceReference(StyleProperty, "ButtonStyle");
                        btn.Margin = new Thickness(5, 0, 0, 0);
                        btn.Content = "取消签约";
                        CheckCaseSearchPageViewModel vm = Microsoft.Practices.ServiceLocation.ServiceLocator.Current.GetInstance<CheckCaseSearchPageViewModel>();
                        btn.Command = vm.CancelContractCommand;
                        btn.CommandParameter = caseId;
                        source.spContainer.Children.Add(btn);
                    }
                    //核案：确认接单
                    if (source.NextAction.Item1.Contains("YJ1_HA1"))
                    {
                        Button btn = new Button();
                        btn.SetResourceReference(StyleProperty, "ButtonStyle");
                        btn.Margin = new Thickness(5, 0, 0, 0);
                        btn.Content = "确认接单";
                        CheckCaseSearchPageViewModel vm = Microsoft.Practices.ServiceLocation.ServiceLocator.Current.GetInstance<CheckCaseSearchPageViewModel>();
                        btn.Command = vm.StartContractCommand;
                        btn.CommandParameter = caseId;
                        source.spContainer.Children.Add(btn);
                    }
                    //核案：完成签约
                    if (source.NextAction.Item1.Contains("HA1_HA2"))
                    {
                        Button btn = new Button();
                        btn.SetResourceReference(StyleProperty, "ButtonStyle");
                        btn.Margin = new Thickness(5, 0, 0, 0);
                        btn.Content = "完成签约";
                        CheckCaseSearchPageViewModel vm = Microsoft.Practices.ServiceLocation.ServiceLocator.Current.GetInstance<CheckCaseSearchPageViewModel>();
                        btn.Command = vm.ConfirmContractCommand;
                        btn.CommandParameter = caseId;
                        source.spContainer.Children.Add(btn);
                    }
                    //核案：新建报告
                    if (source.NextAction.Item1.Contains("HA2_HA3"))
                    {
                        Button btn = new Button();
                        btn.SetResourceReference(StyleProperty, "ButtonStyle");
                        btn.Margin = new Thickness(5, 0, 0, 0);
                        btn.Content = "新建报告";
                        BaseInfoViewModel vm = Microsoft.Practices.ServiceLocation.ServiceLocator.Current.GetInstance<BaseInfoViewModel>();
                        btn.Command = vm.BaseInfoCommand;
                        btn.CommandParameter = caseId;
                        source.spContainer.Children.Add(btn);
                    }
                    //核案：提交报告
                    if (source.NextAction.Item1.Contains("HA3_HA4") || source.NextAction.Item1.Contains("HA6_HA4"))
                    {
                        Button btn = new Button();
                        btn.SetResourceReference(StyleProperty, "ButtonStyle");
                        btn.Margin = new Thickness(5, 0, 0, 0);
                        btn.Content = "提交报告";
                        source.spContainer.Children.Add(btn);
                    }
                    //核案：审核报告
                    if (source.NextAction.Item1.Contains("HA4_HA5"))
                    {
                        Button btn = new Button();
                        btn.SetResourceReference(StyleProperty, "ButtonStyle");
                        btn.Margin = new Thickness(5, 0, 0, 0);
                        btn.Content = "审核报告";
                        ApprovalReportViewModel vm = Microsoft.Practices.ServiceLocation.ServiceLocator.Current.GetInstance<ApprovalReportViewModel>();
                        btn.Command = vm.ApprovalReportCommand;
                        btn.CommandParameter = caseId;
                        source.spContainer.Children.Add(btn);
                    }

                    //foreach (var item in source.NextAction)
                    //{
                    //    Button btn = new Button();
                    //    btn.Content = item.ToString();
                    //    source.spContainer.Children.Add(btn);
                    //}
                }

            }
        }

        #endregion

    }
}
