﻿using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls
{
    /// <summary>
    /// UserDetailInfoControl.xaml 的交互逻辑
    /// </summary>
    public partial class UserDetailInfoControl : UserControl
    {
        public UserDetailInfoControl()
        {
            InitializeComponent();
        }


        public UserDetailInfoViewModel UserInfo
        {
            get { return (UserDetailInfoViewModel)GetValue(UserInfoProperty); }
            set { SetValue(UserInfoProperty, value); }
        }

        // Using a DependencyProperty as the backing store for UserInfo.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty UserInfoProperty =
            DependencyProperty.Register("UserInfo", typeof(UserDetailInfoViewModel), typeof(UserDetailInfoControl), new PropertyMetadata(null));




        public ObservableCollection<CertificateType> CertificateTypeList
        {
            get { return (ObservableCollection<CertificateType>)GetValue(CertificateTypeListProperty); }
            set { SetValue(CertificateTypeListProperty, value); }
        }

        // Using a DependencyProperty as the backing store for certificateTypeList.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CertificateTypeListProperty =
            DependencyProperty.Register("CertificateTypeList", typeof(ObservableCollection<CertificateType>), typeof(UserDetailInfoControl), new PropertyMetadata(null));




        
    }
}
