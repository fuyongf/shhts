﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls
{
    /// <summary>
    /// CaseMainStatusItemInnerControl.xaml 的交互逻辑
    /// </summary>
    public partial class CaseMainStatusItemInnerControl : UserControl
    {
        public CaseMainStatusItemInnerControl()
        {
            InitializeComponent();
        }
        public string StatusName
        {
            get { return (string)GetValue(StatusNameProperty); }
            set { SetValue(StatusNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for StatusName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StatusNameProperty =
            DependencyProperty.Register("StatusName", typeof(string), typeof(CaseMainStatusItemInnerControl), new PropertyMetadata(string.Empty));



        public string DetailInfo
        {
            get { return (string)GetValue(DetailInfoProperty); }
            set { SetValue(DetailInfoProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DetailInfo.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DetailInfoProperty =
            DependencyProperty.Register("DetailInfo", typeof(string), typeof(CaseMainStatusItemInnerControl), new PropertyMetadata(null));



        public bool IsReadyable
        {
            get { return (bool)GetValue(IsReadyableProperty); }
            set { SetValue(IsReadyableProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsReadyable.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsReadyableProperty =
            DependencyProperty.Register("IsReadyable", typeof(bool), typeof(CaseMainStatusItemInnerControl), new PropertyMetadata(false));

        public Visibility IsShowTopLine
        {
            get { return (Visibility)GetValue(IsShowTopLineProperty); }
            set { SetValue(IsShowTopLineProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsShowTopLine.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsShowTopLineProperty =
            DependencyProperty.Register("IsShowTopLine", typeof(Visibility), typeof(CaseMainStatusItemInnerControl), new PropertyMetadata(Visibility.Visible));




        public double TopLineHeight
        {
            get { return (double)GetValue(TopLineHeightProperty); }
            set { SetValue(TopLineHeightProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TopLineHeight.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TopLineHeightProperty =
            DependencyProperty.Register("TopLineHeight", typeof(double), typeof(CaseMainStatusItemInnerControl), new PropertyMetadata(Convert.ToDouble(40)));



        public double LeftLineWidth
        {
            get { return (double)GetValue(LeftLineWidthProperty); }
            set { SetValue(LeftLineWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LeftLineWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LeftLineWidthProperty =
            DependencyProperty.Register("LeftLineWidth", typeof(double), typeof(CaseMainStatusItemInnerControl), new PropertyMetadata(Convert.ToDouble(40)));



        public Visibility IsShowLeftLine
        {
            get { return (Visibility)GetValue(IsShowLeftLineProperty); }
            set { SetValue(IsShowLeftLineProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsShowLeftLine.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsShowLeftLineProperty =
            DependencyProperty.Register("IsShowLeftLine", typeof(Visibility), typeof(CaseMainStatusItemInnerControl), new PropertyMetadata(Visibility.Visible));



        public static readonly RoutedEvent ContinueButtonClickEvent = EventManager.RegisterRoutedEvent(
            "ContinueButtonClick", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(CaseMainStatusItemInnerControl));

        // Expose this event for this control's container
        public event RoutedEventHandler ContinueButtonClick
        {
            add { AddHandler(ContinueButtonClickEvent, value); }
            remove { RemoveHandler(ContinueButtonClickEvent, value); }
        }


        private void ContinueButton_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(ContinueButtonClickEvent));
        }
    }
}
