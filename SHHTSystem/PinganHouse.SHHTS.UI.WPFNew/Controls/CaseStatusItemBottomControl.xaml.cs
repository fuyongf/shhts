﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls
{
    /// <summary>
    /// CaseStatusItemBottomControl.xaml 的交互逻辑
    /// </summary>
    public partial class CaseStatusItemBottomControl : UserControl
    {
        public CaseStatusItemBottomControl()
        {
            InitializeComponent();
        }
        public string StatusName
        {
            get { return (string)GetValue(StatusNameProperty); }
            set { SetValue(StatusNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for StatusName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StatusNameProperty =
            DependencyProperty.Register("StatusName", typeof(string), typeof(CaseStatusItemBottomControl), new PropertyMetadata(string.Empty));



        public string DetailInfo
        {
            get { return (string)GetValue(DetailInfoProperty); }
            set { SetValue(DetailInfoProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DetailInfo.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DetailInfoProperty =
            DependencyProperty.Register("DetailInfo", typeof(string), typeof(CaseStatusItemBottomControl), new PropertyMetadata(null));



        public bool IsReadyable
        {
            get { return (bool)GetValue(IsReadyableProperty); }
            set { SetValue(IsReadyableProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsReadyable.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsReadyableProperty =
            DependencyProperty.Register("IsReadyable", typeof(bool), typeof(CaseStatusItemBottomControl), new PropertyMetadata(false));



        public Visibility IsShowLine
        {
            get { return (Visibility)GetValue(IsShowLineProperty); }
            set { SetValue(IsShowLineProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsShowLine.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsShowLineProperty =
            DependencyProperty.Register("IsShowLine", typeof(Visibility), typeof(CaseStatusItemBottomControl), new PropertyMetadata(Visibility.Visible));




        public Visibility IsShowRightLine
        {
            get { return (Visibility)GetValue(IsShowRightLineProperty); }
            set { SetValue(IsShowRightLineProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsShowRightLine.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsShowRightLineProperty =
            DependencyProperty.Register("IsShowRightLine", typeof(Visibility), typeof(CaseStatusItemBottomControl), new PropertyMetadata(Visibility.Collapsed));



        public double LeftLineWidth
        {
            get { return (double)GetValue(LeftLineWidthProperty); }
            set { SetValue(LeftLineWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LeftLineWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LeftLineWidthProperty =
            DependencyProperty.Register("LeftLineWidth", typeof(double), typeof(CaseStatusItemBottomControl), new PropertyMetadata(Convert.ToDouble(85)));




        public double RightLineWidth
        {
            get { return (double)GetValue(RightLineWidthProperty); }
            set { SetValue(RightLineWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for RightLineWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RightLineWidthProperty =
            DependencyProperty.Register("RightLineWidth", typeof(double), typeof(CaseStatusItemBottomControl), new PropertyMetadata(Convert.ToDouble(0)));



        public Visibility ShowTopValue
        {
            get { return (Visibility)GetValue(ShowTopValueProperty); }
            set { SetValue(ShowTopValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowTopValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowTopValueProperty =
            DependencyProperty.Register("ShowTopValue", typeof(Visibility), typeof(CaseStatusItemBottomControl), new PropertyMetadata(Visibility.Visible));



        public Visibility ShowBottomValue
        {
            get { return (Visibility)GetValue(ShowBottomValueProperty); }
            set { SetValue(ShowBottomValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowBottomValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowBottomValueProperty =
            DependencyProperty.Register("ShowBottomValue", typeof(Visibility), typeof(CaseStatusItemBottomControl), new PropertyMetadata(Visibility.Collapsed));


    }
}
