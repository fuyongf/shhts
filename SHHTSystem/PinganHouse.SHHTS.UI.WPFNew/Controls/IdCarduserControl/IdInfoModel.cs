﻿using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;

using System.Windows.Media.Imaging;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls
{
    public class IdInfoModel : BaseViewModel
    {
        /// <summary>
        /// 姓名
        /// </summary>
        private string idName;
        public string IdName
        {
            get
            {
                return idName;
            }
            set
            {
                if (idName != value)
                {
                    idName = value;
                    NotifyPropertyChanged("IdName");
                }
            }
        }
        /// <summary>
        /// 性别
        /// </summary>
        private string idSex;
        public string IdSex
        {
            get
            {
                return idSex;
            }
            set
            {
                if (idSex != value)
                {
                    idSex = value;
                    NotifyPropertyChanged("IdSex");
                }
            }
        }
        /// <summary>
        /// 民族
        /// </summary>
        private string idNation;
        public string IdNation
        {
            get
            {
                return idNation;
            }
            set
            {
                if (idNation != value)
                {
                    idNation = value;
                    NotifyPropertyChanged("IdNation");
                }
            }
        }
        /// <summary>
        /// 出生年月
        /// </summary>
        private string idBirth;
        public string IdBirth
        {
            get
            {
                return idBirth;
            }
            set
            {
                if (idBirth != value)
                {
                    idBirth = value;
                    NotifyPropertyChanged("IdBirth");
                }
            }
        }
        /// <summary>
        /// 地址
        /// </summary>
        private string idAddress;
        public string IdAddress
        {
            get
            {
                return idAddress;
            }
            set
            {
                if (idAddress != value)
                {
                    idAddress = value;
                    NotifyPropertyChanged("IdAddress");
                }
            }
        }
        /// <summary>
        /// 证件号码
        /// </summary>
        private string idNum;
        public string IdNum
        {
            get
            {
                return idNum;
            }
            set
            {
                if (idNum != value)
                {
                    idNum = value;
                    //tag = value;
                    NotifyPropertyChanged("IdNum");
                }
            }
        }
        /// <summary>
        /// 签证机构
        /// </summary>
        private string idAgent;
        public string IdAgent
        {
            get
            {
                return idAgent;
            }
            set
            {
                if (idAgent != value)
                {
                    idAgent = value;
                    NotifyPropertyChanged("IdAgent");
                }
            }
        }
       /// <summary>
       /// 有效日期（开始）
       /// </summary>
        private string effectiveDate;
        public string EffectiveDate
        {
            get
            {
                return effectiveDate;
            }
            set
            {
                if (effectiveDate != value)
                {
                    effectiveDate = value;
                    NotifyPropertyChanged("EffectiveDate");
                }
            }
        }
        /// <summary>
        /// 有效日期（失效）
        /// </summary>
        private string expiryDate;
        public string ExpiryDate
        {
            get
            {
                return expiryDate;
            }
            set
            {
                if (expiryDate != value)
                {
                    expiryDate = value;
                    NotifyPropertyChanged("ExpiryDate");
                }
            }
        }
        /// <summary>
        /// 证件照
        /// </summary>
        private byte[] photo;
        public byte[] Photo
        {
            get
            {
                return photo;
            }
            set
            {
                if (photo != value)
                {
                    photo = value;
                    NotifyPropertyChanged("Photo");
                }
            }
        }

        /// <summary>
        /// 证件照
        /// </summary>
        private BitmapImage bitmap;
        public BitmapImage Bitmap
        {
            get
            {
                return bitmap;
            }
            set
            {
                if (bitmap != value)
                {
                    bitmap = value;
                    NotifyPropertyChanged("Bitmap");
                }
            }
        }      
         /// <summary>
        /// 输入信息是否可信
         /// </summary>
        private bool isTrusted;
        public bool IsTrusted
        {
            get
            {
                return isTrusted;
            }
            set
            {
                if (isTrusted != value)
                {
                    isTrusted = value;
                    NotifyPropertyChanged("IsTrusted");
                }
            }
        }
        /// <summary>
        /// 是否修改标志
        /// </summary>
        private object tag;
        public object Tag
        {
            get
            {
                return tag;
            }
            set
            {
                if (tag != value)
                {
                    tag = value;
                    NotifyPropertyChanged("Tag");
                }
            }
        }
        /// <summary>
        /// 系统对象其它属性
        /// </summary>
        private object tagObject;
        public object TagObject
        {
            get
            {
                return tagObject;
            }
            set
            {
                if (tagObject != value)
                {
                    tagObject = value;
                    NotifyPropertyChanged("TagObject");
                }
            }
        }

        /// <summary>
        /// 男性
        /// </summary>
        private bool male;
        public bool Male
        {
            get
            {
                return male;
            }
            set
            {
                if (male != value)
                {
                    male = value;
                    NotifyPropertyChanged("Male");
                }
            }
        }
        /// <summary>
        ///女性
        /// </summary>
        private bool female;
        public bool Female
        {
            get
            {
                return female;
            }
            set
            {
                if (female != value)
                {
                    female = value;
                    NotifyPropertyChanged("Female");
                }
            }
        }
        /// <summary>
        /// 国籍
        /// </summary>
        private string nationality;
        public string Nationality
        {
            get
            {
                return nationality;
            }
            set
            {
                if (nationality != value)
                {
                    nationality = value;
                    NotifyPropertyChanged("Nationality");
                }
            }
        }
        /// <summary>
        /// 部别
        /// </summary>
        private string section;
        public string Section
        {
            get
            {
                return section;
            }
            set
            {
                if (section != value)
                {
                    section = value;
                    NotifyPropertyChanged("Section");
                }
            }
        }
        /// <summary>
        /// 证件类型
        /// </summary> 
        private CertificateType certificateType;
        public CertificateType CertificateType
        {
            get
            {
                return certificateType;
            }
            set
            {
                if (certificateType != value)
                {
                    certificateType = value;
                    NotifyPropertyChanged("CertificateType");
                }
            }
        }

        /// <summary>
        /// 是否撤销
        /// </summary>
        private bool isCancel = false;
        public bool IsCancel
        {
            get
            {
                return isCancel;
            }
            set
            {
                if (isCancel != value)
                {
                    isCancel = value;
                    NotifyPropertyChanged("IsCancel");
                }
            }
        }

        private bool isUpdate;
        public bool IsUpdate
        {
            get
            {
                return isUpdate;
            }
            set
            {
                if (isUpdate != value)
                {
                    isUpdate = value;
                    NotifyPropertyChanged("IsUpdate");
                }
            }
        }
    }
}
