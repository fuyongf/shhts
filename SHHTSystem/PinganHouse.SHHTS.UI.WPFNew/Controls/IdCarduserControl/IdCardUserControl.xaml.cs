﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Practices.ServiceLocation;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using PinganHouse.SHHTS.UI.WPFNew.Controls;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls
{
    /// <summary>
    /// IdCardTypeControl.xaml 的交互逻辑
    /// </summary>
    public partial class IdCardTypeControl : UserControl
    {
        public IdCardTypeControl()
        {
            InitializeComponent();
            List<EnumHelper> listNation = EnumHelper.InitNationToCombobox();
            CbNation.ItemsSource = listNation;
            CbNation.DisplayMemberPath = "DisplayMember";
            CbNation.SelectedValuePath = "ValueMember";
            CbNation.SelectedIndex = 0;
            CbNation1.ItemsSource = listNation;
            CbNation1.DisplayMemberPath = "DisplayMember";
            CbNation1.SelectedValuePath = "ValueMember";
            CbNation1.SelectedIndex = 0;
            CbNation2.ItemsSource = listNation;
            CbNation2.DisplayMemberPath = "DisplayMember";
            CbNation2.SelectedValuePath = "ValueMember";
            CbNation2.SelectedIndex = 0;
        }
        public PreCheckAddCaseViewModel ViewModel
        {
            get
            {
                return (PreCheckAddCaseViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }

        #region 注册属性
        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Title.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TitleProperty =
            DependencyProperty.Register("Title", typeof(string), typeof(IdCardTypeControl), new PropertyMetadata(string.Empty));


        public ImageSource ImgTitle
        {
            get { return (ImageSource)GetValue(ImgTitleProperty); }
            set { SetValue(ImgTitleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ImgTitle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ImgTitleProperty =
            DependencyProperty.Register("ImgTitle", typeof(ImageSource), typeof(IdCardTypeControl), new PropertyMetadata(null));

        public int ReadType
        {
            get { return (int)GetValue(ReadTypeProperty); }
            set { SetValue(ReadTypeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ReadType.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ReadTypeProperty =
            DependencyProperty.Register("ReadType", typeof(int), typeof(IdCardTypeControl), new PropertyMetadata(0));

        public IdInfoModel IdInfo
        {
            get { return (IdInfoModel)GetValue(IdInfoProperty); }
            set { SetValue(IdInfoProperty, value); }
        }

        public List<EnumHelper> ListIdentity
        {
            get { return (List<EnumHelper>)GetValue(ListIdentityProperty); }
            set { SetValue(ListIdentityProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ListIdentity.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ListIdentityProperty =
            DependencyProperty.Register("ListIdentity", typeof(List<EnumHelper>), typeof(IdCardTypeControl), new PropertyMetadata(null));

        public EnumHelper SelectedIdentity
        {
            get { return (EnumHelper)GetValue(SelectedIdentityProperty); }
            set { SetValue(SelectedIdentityProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedIdentity.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedIdentityProperty =
            DependencyProperty.Register("SelectedIdentity", typeof(EnumHelper), typeof(IdCardTypeControl), new PropertyMetadata(null));


        public int Selectindex
        {
            get { return (int)GetValue(SelectindexProperty); }
            set { SetValue(SelectindexProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Selectindex.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectindexProperty =
            DependencyProperty.Register("Selectindex", typeof(int), typeof(IdCardTypeControl), new PropertyMetadata(0));
    

        // Using a DependencyProperty as the backing store for IdInfo.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IdInfoProperty =
            DependencyProperty.Register("IdInfo", typeof(IdInfoModel), typeof(IdCardTypeControl), new PropertyMetadata(null));

        public static readonly RoutedEvent ReadButtonClickEvent = EventManager.RegisterRoutedEvent(
            "ReadButtonClick", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(IdCardTypeControl));


        // Expose this event for this control's container
        public event RoutedEventHandler ReadButtonClick
        {
            add { AddHandler(ReadButtonClickEvent, value); }
            remove { RemoveHandler(ReadButtonClickEvent, value); }
        }

        public bool IsForbid
        {
            get { return (bool)GetValue(IsForbidProperty); }
            set { SetValue(IsForbidProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsForbid.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsForbidProperty =
            DependencyProperty.Register("IsForbid", typeof(bool), typeof(IdCardTypeControl), new PropertyMetadata(true));

        #endregion
        bool _isInput = false;
        /// <summary>
        /// ComboBox下拉事件（控制不同类型显示不同表格）
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CertificateTypeCb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectValue = CertificateTypeCb.SelectedIndex ;
            var typeName = (CertificateType)selectValue;           
           switch (typeName)
           {
               case CertificateType.BirthCertificate:
                   CollapsedTypeGrid(typeName);                 
                   BirthCertifiCateType.Visibility = Visibility.Visible;
                   break;
               case CertificateType.CertificateOfOfficers:
                   CollapsedTypeGrid(typeName);
                   OfficerType.Visibility = Visibility.Visible;
                   break;
               case CertificateType.HKMacaoIdCard:
                   CollapsedTypeGrid(typeName);
                   HkMacaoIdType.Visibility = Visibility.Visible;
                   break;
               case CertificateType.IdCard:
                   CollapsedTypeGrid(typeName);
                   //if (!_isInput)
                   //{
                    IdType.Visibility = Visibility.Visible;
                    ReadBtn.Visibility = Visibility.Visible;
                   //}
                   //else
                   //{
                   //    IdTypeInput.Visibility = Visibility.Visible;
                   //}               
                   break;
               case CertificateType.MacaoPermit:
                   CollapsedTypeGrid(typeName);
                   HkMacaoPermitType.Visibility = Visibility.Visible;
                   break;
               case CertificateType.Passport:
                   CollapsedTypeGrid(typeName);
                   PassportType.Visibility = Visibility.Visible;
                   break;
               case CertificateType.ResidenceBooklet:
                   CollapsedTypeGrid(typeName);
                   BookLetType.Visibility = Visibility.Visible;
                   break;
               case CertificateType.TaiwanPermit:
                   CollapsedTypeGrid(typeName);
                   TaiwanPermitType.Visibility = Visibility.Visible;
                   break;
               //default:
               //    _isInput = true;
               //    CollapsedTypeGrid(typeName);
               //    IdTypeInput.Visibility = Visibility.Visible;
               //    CertificateTypeCb.SelectedIndex = 0;                 
               //    break;
           }
        }

        private void CollapsedTypeGrid(CertificateType typeName)
        {
            IdType.Visibility = Visibility.Collapsed;
            HkMacaoIdType.Visibility = Visibility.Collapsed;
            PassportType.Visibility = Visibility.Collapsed;
            BirthCertifiCateType.Visibility = Visibility.Collapsed;
            HkMacaoPermitType.Visibility = Visibility.Collapsed;
            OfficerType.Visibility = Visibility.Collapsed;
            BookLetType.Visibility = Visibility.Collapsed;
            ReadBtn.Visibility = Visibility.Collapsed;
            TaiwanPermitType.Visibility = Visibility.Collapsed;
            IdTypeInput.Visibility = Visibility.Collapsed;
            if (IdInfo == null)
            {
                IdInfo = new IdInfoModel();
            }
            IdInfo.CertificateType = typeName;
        }

        private void ReadBtn_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(ReadButtonClickEvent));
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            IdInfo = new IdInfoModel();
            CertificateTypeCb.SelectedIndex = 0;//默认选择身份证
            IsForbid = true;
        }

        private void PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9]+");     
            e.Handled = re.IsMatch(e.Text);
        }
        
    }
}
