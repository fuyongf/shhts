﻿using PinganHouse.SHHTS.RemoteServiceProxy;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using PinganHouse.SHHTS.UI.WPFNew.ViewModels;

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls
{
    /// <summary>
    /// ImageBrowse.xaml 的交互逻辑
    /// </summary>
    public partial class ImageBrowse : Window
    {
        TransformGroup st = null;

        private bool m_IsMouseLeftButtonDown;
        private Point m_PreviousMousePoint;
        private ScaleTransform _scaleTransform;
        #region 公共变量

        private double rotateAngle;

        private BitmapImage bitmapImage;

        private List<string> imageId = new List<string>();
        private ObservableCollection<PhotosViewModel> imageList = new ObservableCollection<PhotosViewModel>();
        private Dictionary<string, Stream> imgstream = new Dictionary<string, Stream>();
        private bool isPreview = false;
        //private List<Stream> imgstream=new List<Stream>();
        private int Index = 0;
        private int count = 0;

        #endregion

        public ImageBrowse(List<string> imageIds)
        {
            InitializeComponent();
            imageId = imageIds;
            this.Index = 0;
            isPreview = false;
            this.btn_Delete.Visibility = Visibility.Visible;
        }

        public ImageBrowse(ObservableCollection<PhotosViewModel> imageList, int index)
        {
            InitializeComponent();
            this.imageList=imageList;
            this.Index = index;
            isPreview = true;
            this.btn_Delete.Visibility = Visibility.Collapsed;
        }
        public double ScreenHeigh
        {
            get { return (double)GetValue(ScreenHeighProperty); }
            set { SetValue(ScreenHeighProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScreenHeigh.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScreenHeighProperty =
            DependencyProperty.Register("ScreenHeigh", typeof(double), typeof(ImageBrowse), new PropertyMetadata(SystemParameters.PrimaryScreenHeight));



        public double ScreenWidth
        {
            get { return (double)GetValue(ScreenWidthProperty); }
            set { SetValue(ScreenWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScreenWidth.  This enables animation, styling, binding, etc...SystemParameters.WorkArea.Width
        public static readonly DependencyProperty ScreenWidthProperty =
            DependencyProperty.Register("ScreenWidth", typeof(double), typeof(ImageBrowse), new PropertyMetadata(SystemParameters.PrimaryScreenWidth));

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //从数据库拿数据预览
            if (!isPreview)
            {
                count = 1;
                ShowPicture(imageId[count - 1]);
            }
            //从本地拿
            else
            {
                if (Index > 0)
                    UpBtn.Visibility = Visibility.Visible;
                if (Index < imageList.Count-1)
                    DownBtn.Visibility = Visibility.Visible;
                this.ImgShow.Source = imageList[Index].ImageSource;
            }
        }
        /// <summary>
        /// 展示图片
        /// </summary>
        /// <param name="fileName"></param>
        private async void ShowPicture(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return;
            if (count > 1)
            {
                UpBtn.Visibility = Visibility.Visible;
            }
            else
            {
                UpBtn.Visibility = Visibility.Collapsed;
            }
            if (count < imageId.Count)
            {
                DownBtn.Visibility = Visibility.Visible;
            }
            else
            {
                DownBtn.Visibility = Visibility.Collapsed;
            }
         
            #region 下载图片
            //Loading_Ctrl.IsBusy = true;
            //Loading_Ctrl.Text = "正在下载...";

            Stream fileStream = await Task.Run(() =>
            {
                return DownImage(id);
            });

            //Loading_Ctrl.IsBusy = false;
            #endregion

            if (fileStream == null)
            {
                MessageBox.Show("图片下载失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            fileStream.Seek(0, SeekOrigin.Begin);
            bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.StreamSource = fileStream;

            if (rotateAngle == 0)
            {
                bitmapImage.Rotation = Rotation.Rotate0;
            }
            else if (rotateAngle == 90 || rotateAngle == -270)
            {
                bitmapImage.Rotation = Rotation.Rotate90;
            }
            else if (rotateAngle == 180 || rotateAngle == -180)
            {
                bitmapImage.Rotation = Rotation.Rotate180;
            }
            else if (rotateAngle == 270||rotateAngle == -90)
            {
                bitmapImage.Rotation = Rotation.Rotate270;
            }      
            bitmapImage.EndInit();
            ResizePicture();
        }

        private void ResizePicture()
        {
            this.ImgShow.Source = bitmapImage;
        }

        private void ZoomIn_Click(object sender, RoutedEventArgs e)
        {
            if (st == null)
                st = (TransformGroup)FindResource("m_zoomKey");
            _scaleTransform = st.Children[0] as ScaleTransform;
            if (_scaleTransform.ScaleX > 3)
            {
                _scaleTransform.ScaleX = _scaleTransform.ScaleX;
            }
            else
            {
                _scaleTransform.ScaleX += 0.1;
            }
            if (_scaleTransform.ScaleY > 3)
            {
                _scaleTransform.ScaleY = _scaleTransform.ScaleY;
            }
            else
            {
                _scaleTransform.ScaleY += 0.1;
            }
            ReMovedCenter();
        }

        private void ZoomOut_Click(object sender, RoutedEventArgs e)
        {
            if (st == null)
                st = (TransformGroup)FindResource("m_zoomKey");
            _scaleTransform = st.Children[0] as ScaleTransform;
            if (_scaleTransform.ScaleX <= 0.7)
            {
                _scaleTransform.ScaleX = _scaleTransform.ScaleX;
            }
            else
            {
                _scaleTransform.ScaleX -= 0.1;
            }
            if (_scaleTransform.ScaleY <= 0.7)
            {
                _scaleTransform.ScaleY = _scaleTransform.ScaleY;
            }
            else
            {
                _scaleTransform.ScaleY -= 0.1;
            }
            ReMovedCenter();
        }

        /// <summary>
        /// 前一页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpBtn_Click(object sender, RoutedEventArgs e)
        {
            if (st == null)
                st = (TransformGroup)FindResource("m_zoomKey");
            _scaleTransform = st.Children[0] as ScaleTransform;
            var transform = st.Children[1] as TranslateTransform;
            transform.X = 0;
            transform.Y = 0;
            _scaleTransform.ScaleX = 1;
            _scaleTransform.ScaleY = 1;
            if (!isPreview)
            {
                count--;
                if (count == 1)
                {
                    UpBtn.Visibility = Visibility.Collapsed;
                }
                ShowPicture(imageId[count - 1]);
            }
            else
            {
                Index--;
                if (Index == imageList.Count - 1)
                {
                    DownBtn.Visibility = Visibility.Collapsed;
                }
                else
                {
                    DownBtn.Visibility = Visibility.Visible;
                }
                if (Index == 0)
                {
                    UpBtn.Visibility = Visibility.Collapsed;
                }
                else
                {
                    UpBtn.Visibility = Visibility.Visible;
                }
                this.ImgShow.Source = imageList[Index].ImageSource;
            }
        }
        /// <summary>
        /// 后一页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DownBtn_Click(object sender, RoutedEventArgs e)
        {
            if (st == null)
                st = (TransformGroup)FindResource("m_zoomKey");
            _scaleTransform = st.Children[0] as ScaleTransform;
            var transform = st.Children[1] as TranslateTransform;
            transform.X = 0;
            transform.Y = 0;
            _scaleTransform.ScaleX = 1;
            _scaleTransform.ScaleY = 1;
            if (!isPreview)
            {
                count++;
                if (count == imageId.Count)
                {
                    DownBtn.Visibility = Visibility.Collapsed;
                }
                ShowPicture(imageId[count - 1]);
            }
            else
            {
                Index++;
                if (Index == imageList.Count-1)
                {
                    DownBtn.Visibility = Visibility.Collapsed;
                }
                else
                {
                    DownBtn.Visibility = Visibility.Visible;
                }
                if (Index == 0)
                {
                    UpBtn.Visibility = Visibility.Collapsed;
                }
                else
                {
                    UpBtn.Visibility = Visibility.Visible;
                }
                this.ImgShow.Source = imageList[Index].ImageSource;
            }
        }

        private void LeftBtn_Click(object sender, RoutedEventArgs e)
        {
            rotateAngle -= 90;
            rotateAngle = rotateAngle % 360;
            doRotate();
        }

        private void rightBtn_Click(object sender, RoutedEventArgs e)
        {          
            rotateAngle += 90;
            rotateAngle = rotateAngle % 360;
            doRotate();
        }

        private void doRotate()
        {
            if(!isPreview)
            {
                ShowPicture(imageId[count - 1]);
            }
            else
            {
                TransformedBitmap tb = new TransformedBitmap();
                tb.BeginInit();
                BitmapSource bs = (BitmapSource)imageList[Index].ImageSource;
                tb.Source = bs;
                RotateTransform transform = null;
                if (rotateAngle == 0)
                {
                    transform = new RotateTransform (0);
                }
                else if (rotateAngle == 90 || rotateAngle == -270)
                {
                    transform = new RotateTransform(90);
                }
                else if (rotateAngle == 180 || rotateAngle == -180)
                {
                    transform = new RotateTransform(180);
                }
                else if (rotateAngle == 270 || rotateAngle == -90)
                {
                    transform = new RotateTransform(270);
                }
                tb.Transform = transform;
                tb.EndInit();
                this.ImgShow.Source = tb;
            }
        }

        private async Task<Stream> DownImage(string fileId)
        {
            if (imgstream.ContainsKey(fileId))
                return imgstream[fileId];
            Stream imagStream = null;
            var file = AttachmentProxyService.GetInstanse().DownloadAttachment(fileId); 
            using (file.FileData)
            {
                imagStream = new MemoryStream();
                file.FileData.CopyTo(imagStream);
            }
            imgstream.Add(fileId, imagStream);
            return imagStream;
        }


        private void DoImageMove(Rectangle rectangle, MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed) return;
            var group = FindResource("m_zoomKey") as TransformGroup;

            _scaleTransform = group.Children[0] as ScaleTransform;
            TranslateTransform transform = group.Children[1] as TranslateTransform;
            var position = e.GetPosition(rectangle);

            if (transform.X + position.X - m_PreviousMousePoint.X <= 0 && (transform.X + position.X - m_PreviousMousePoint.X) >= this.m_boder1.Width * (-_scaleTransform.ScaleX + 1))
            {
                transform.X += position.X - m_PreviousMousePoint.X;
            }


            if (transform.Y + position.Y - m_PreviousMousePoint.Y <= 0 && (transform.Y + position.Y - m_PreviousMousePoint.Y) >= this.m_boder1.Height * (-_scaleTransform.ScaleY + 1))
            {
                transform.Y += position.Y - m_PreviousMousePoint.Y;
            }

            m_PreviousMousePoint = position;
        }


        private void MasterImage_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Rectangle rectangle = sender as Rectangle;
            if (rectangle == null)
                return;
            rectangle.ReleaseMouseCapture();
            m_IsMouseLeftButtonDown = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MasterImage_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Rectangle rectangle = sender as Rectangle;
            if (rectangle == null)
                return;
            rectangle.CaptureMouse();
            m_IsMouseLeftButtonDown = true;
            m_PreviousMousePoint = e.GetPosition(rectangle);
        }

        private void MasterImage_MouseMove(object sender, MouseEventArgs e)
        {
            var rectangle = sender as Rectangle;
            if (rectangle == null) return;
            if (m_IsMouseLeftButtonDown)
                DoImageMove(rectangle, e);
                //ReMovedCenter();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private bool isDelete;

        public bool IsDelete
        {
            get { return isDelete; }
            set { isDelete = value; }
        }


        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            isDelete = true;
            var result = MessageBox.Show("确定要删除该图片!", "系统提示", MessageBoxButton.OKCancel, MessageBoxImage.Information);
            if (result !=MessageBoxResult.OK)
            {
                return;
            }
            //删除图片
            if (!isPreview)
            {
                //服务器删除
                string fileId = imageId[count - 1];
                var deleteResult=false;
                try
                {
                    deleteResult = AttachmentProxyService.GetInstanse().DeleteAttachment(fileId, LoginHelper.CurrentUser.SysNo);
                }
                catch
                {
                    return;
                }
                if(!deleteResult)//删除不成功
                {
                    return;
                }
                
                //先本地删除,如果只有一张图片，则同时关闭该窗口
                if (imageId.Count == 1)
                {
                    this.Close();
                }
                else
                {
                    imageId.RemoveAt(count - 1);
                    if (count-1 == imageId.Count)//最后一张，则显示第一张
                    {
                        count = 1;//设置当前项
                        ShowPicture(imageId[count - 1]);
                    }
                    else//否则显示下一张图片，
                    {
                        ShowPicture(imageId[count - 1]);
                    }
                }
            }
        }

        private void ReMovedCenter()
        {
            var group = FindResource("m_zoomKey") as TransformGroup;
            _scaleTransform = group.Children[0] as ScaleTransform;
            TranslateTransform transform = group.Children[1] as TranslateTransform;
            double widthHalfDelta = (m_boder1.RenderSize.Width - m_boder1.RenderSize.Width *_scaleTransform.ScaleX) / 2;
            transform.X = widthHalfDelta;
            double heightHalfDleta = (m_boder1.RenderSize.Height - m_boder1.RenderSize.Height * _scaleTransform.ScaleY) / 2;
            transform.Y = heightHalfDleta;
        }
    }
}
