﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls
{
    /// <summary>
    /// AutoCompleteTextBox.xaml 的交互逻辑
    /// </summary>
    public partial class AutoCompleteTextBox : Canvas
    {

        #region 方法改写

        //声明和注册路由事件
        public static readonly RoutedEvent SelectionChangedRoutedEvent =
            EventManager.RegisterRoutedEvent("SelectionChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(AutoCompleteTextBox));

        public event RoutedEventHandler SelectionChanged
        {
            add { this.AddHandler(SelectionChangedRoutedEvent, value); }
            remove { this.RemoveHandler(SelectionChangedRoutedEvent, value); }
        }

        #endregion

        #region Members
        private readonly VisualCollection _controls;
        private readonly TextBox _textBox;
        private readonly ComboBox _comboBox;
        private readonly ObservableCollection<AutoCompleteEntry> _autoCompletionList;
        private readonly System.Timers.Timer _keypressTimer;
        private delegate void TextChangedCallback();
        private bool _insertText;
        #endregion

        #region Constructor
        public AutoCompleteTextBox()
        {

            _controls = new VisualCollection(this);

            InitializeComponent();

            _autoCompletionList = new ObservableCollection<AutoCompleteEntry>();
            Threshold = 0;        // default threshold to 2 char
            DelayTime = 100;

            // set up the key press timer
            _keypressTimer = new System.Timers.Timer();
            _keypressTimer.Elapsed += OnTimedEvent;
            //控制下拉框大小
            // set up the text box and the combo box
            _comboBox = new ComboBox
            {
                Width = this.Width,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                IsSynchronizedWithCurrentItem = true,
                IsTabStop = false,
                MaxDropDownHeight = 125
            };
            Panel.SetZIndex(_comboBox, -1);
            _comboBox.SelectionChanged += comboBox_SelectionChanged;

            _textBox = new TextBox()
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Width = _comboBox.Width + 2,
            };

            _textBox.TextChanged += textBox_TextChanged;
            _textBox.GotFocus += textBox_GotFocus;
            _textBox.KeyUp += textBox_KeyUp;
            _textBox.KeyDown += textBox_KeyDown;
            _textBox.VerticalContentAlignment = VerticalAlignment.Center;

            _controls.Add(_comboBox);
            _controls.Add(_textBox);
        }
        #endregion

        #region Methods
        private static VisualBrush _vbBrush = new VisualBrush
        {
            TileMode = TileMode.None,
            Opacity = 0.3,
            Stretch = Stretch.None,
            AlignmentX = AlignmentX.Left,

            Visual = new TextBlock { FontStyle = FontStyles.Normal, Text = "请选择", Padding = new Thickness(10, 10, 10, 10) }
        };

        public string Text
        {
            get { return _textBox.Text; }
            set
            {
                _insertText = true;
                _textBox.Text = value;
            }
        }

        public int DelayTime { get; set; }

        public int Threshold { get; set; }

        public AutoCompleteEntry GetAutoCompleteEntry { get; set; }

        public void AddItem(AutoCompleteEntry entry)
        {
            _autoCompletionList.Add(entry);
        }
        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (null != _comboBox.SelectedItem)
            {
                _insertText = true;
                ComboBoxItem cbItem = (ComboBoxItem)_comboBox.SelectedItem;
                var content = cbItem.Content as AutoCompleteEntry;
                if (content != null) _textBox.Text = content.DisplayName;
                GetAutoCompleteEntry = content;
                this.RaiseEvent(new RoutedEventArgs(SelectionChangedRoutedEvent));
            }
        }

        private void TextChanged()
        {
            try
            {
                _comboBox.Items.Clear();

                if (_textBox.Text.Length >= Threshold)
                {
                    //int count=0;
                    //默认大小为20个
                    foreach (AutoCompleteEntry entry in _autoCompletionList)
                    {
                        if (entry.KeywordStrings.Any(word => word.StartsWith(_textBox.Text, StringComparison.CurrentCultureIgnoreCase)))
                        {
                            ComboBoxItem cbItem = new ComboBoxItem { Content = entry };
                            //装入模型
                            //if (count >= 20) break;
                            _comboBox.Items.Add(cbItem);
                            //count++;
                        }
                    }
                    _comboBox.IsDropDownOpen = _comboBox.HasItems;
                }
                else
                {
                    _comboBox.IsDropDownOpen = false;
                }
            }
            catch
            {
                // ignored
            }
        }

        private void OnTimedEvent(object source, System.Timers.ElapsedEventArgs e)
        {
            _keypressTimer.Stop();
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                new TextChangedCallback(this.TextChanged));
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            // text was not typed, do nothing and consume the flag
            if (_insertText == true) _insertText = false;

            // if the delay time is set, delay handling of text changed
            else
            {
                if (DelayTime > 0)
                {
                    _keypressTimer.Interval = DelayTime;
                    _keypressTimer.Start();
                }
                else TextChanged();
            }
        }
        //获得焦点时
        public void textBox_GotFocus(object sender, RoutedEventArgs e)
        {
            // text was not typed, do nothing and consume the flag
            if (_insertText == true) _insertText = false;

            // if the delay time is set, delay handling of text changed
            else
            {
                if (DelayTime > 0)
                {
                    _keypressTimer.Interval = DelayTime;
                    _keypressTimer.Start();
                }
                else TextChanged();
            }
        }
        public void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (_textBox.IsInputMethodEnabled == true)
            {
                _comboBox.IsDropDownOpen = false;
            }
        }
        //按向下的箭头时
        public void textBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down && _comboBox.IsDropDownOpen == true)
            {
                _comboBox.Focus();
            }
        }

        protected override Size ArrangeOverride(Size arrangeSize)
        {
            _textBox.Arrange(new Rect(arrangeSize));
            _comboBox.Arrange(new Rect(arrangeSize));
            return base.ArrangeOverride(arrangeSize);
        }

        protected override Visual GetVisualChild(int index)
        {
            return _controls[index];
        }

        protected override int VisualChildrenCount
        {
            get { return _controls.Count; }
        }
        #endregion
    }

    /// <summary>
    /// 模型
    /// </summary>
    public class AutoCompleteEntry
    {
        private string[] _keywordStrings;

        public string[] KeywordStrings
        {
            get
            {
                if (_keywordStrings == null)
                {
                    _keywordStrings = new string[] { DisplayName };
                }
                return _keywordStrings;
            }
        }

        public string DisplayName { get; set; }

        public object Parameter { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name">名称</param>
        /// <param name="parameter">参数</param>
        /// <param name="keywords">关键key</param>
        public AutoCompleteEntry(string name, object parameter = null, params string[] keywords)
        {
            DisplayName = name;
            _keywordStrings = keywords;
            Parameter = parameter;
        }

        public override string ToString()
        {
            return DisplayName;
        }
    }
}
