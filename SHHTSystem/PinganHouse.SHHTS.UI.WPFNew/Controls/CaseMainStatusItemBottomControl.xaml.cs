﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PinganHouse.SHHTS.UI.WPFNew.Controls
{
    /// <summary>
    /// CaseMainStatusItemBottomControl.xaml 的交互逻辑
    /// </summary>
    public partial class CaseMainStatusItemBottomControl : UserControl
    {
        public CaseMainStatusItemBottomControl()
        {
            InitializeComponent();
        }
        public string StatusName
        {
            get { return (string)GetValue(StatusNameProperty); }
            set { SetValue(StatusNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for StatusName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StatusNameProperty =
            DependencyProperty.Register("StatusName", typeof(string), typeof(CaseMainStatusItemBottomControl), new PropertyMetadata(string.Empty));



        public string DetailInfo
        {
            get { return (string)GetValue(DetailInfoProperty); }
            set { SetValue(DetailInfoProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DetailInfo.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DetailInfoProperty =
            DependencyProperty.Register("DetailInfo", typeof(string), typeof(CaseMainStatusItemBottomControl), new PropertyMetadata(null));



        public bool IsReadyable
        {
            get { return (bool)GetValue(IsReadyableProperty); }
            set { SetValue(IsReadyableProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsReadyable.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsReadyableProperty =
            DependencyProperty.Register("IsReadyable", typeof(bool), typeof(CaseMainStatusItemBottomControl), new PropertyMetadata(false));





        public double LeftLineWidth
        {
            get { return (double)GetValue(LeftLineWidthProperty); }
            set { SetValue(LeftLineWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LeftLineWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LeftLineWidthProperty =
            DependencyProperty.Register("LeftLineWidth", typeof(double), typeof(CaseMainStatusItemBottomControl), new PropertyMetadata(Convert.ToDouble(50)));




        public double RightLineWidth
        {
            get { return (double)GetValue(RightLineWidthProperty); }
            set { SetValue(RightLineWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for RightLineWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RightLineWidthProperty =
            DependencyProperty.Register("RightLineWidth", typeof(double), typeof(CaseMainStatusItemBottomControl), new PropertyMetadata(Convert.ToDouble(0)));



        public static readonly RoutedEvent ContinueButtonClickEvent = EventManager.RegisterRoutedEvent(
            "ContinueButtonClick", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(CaseMainStatusItemBottomControl));


        // Expose this event for this control's container
        public event RoutedEventHandler ContinueButtonClick
        {
            add { AddHandler(ContinueButtonClickEvent, value); }
            remove { RemoveHandler(ContinueButtonClickEvent, value); }
        }


        private void ContinueButton_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(ContinueButtonClickEvent));
        }
    }
}
