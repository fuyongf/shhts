﻿using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.UnityExtensions;
using PinganHouse.SHHTS.UI.WPFNew.Modules;
using PinganHouse.SHHTS.UI.WPFNew.Views;
using System.Windows;

namespace PinganHouse.SHHTS.UI.WPFNew
{
    public class Bootstrapper : UnityBootstrapper
    {
        protected override void ConfigureModuleCatalog()
        {
            base.ConfigureModuleCatalog();

            ModuleCatalog moduleCatalog = (ModuleCatalog)this.ModuleCatalog;
            moduleCatalog.AddModule(typeof(LoginModule));
            moduleCatalog.AddModule(typeof(MainModule));
            moduleCatalog.AddModule(typeof(PreCheckModule));
            moduleCatalog.AddModule(typeof(NuclearModule));
            moduleCatalog.AddModule(typeof(FundModule));
        }

        protected override DependencyObject CreateShell()
        {
            return this.Container.TryResolve<ShellView>();
        }

        protected override void InitializeShell()
        {
            base.InitializeShell();
            App.Current.MainWindow = (Window)this.Shell;
            App.Current.MainWindow.Show();
        }
    }
}
