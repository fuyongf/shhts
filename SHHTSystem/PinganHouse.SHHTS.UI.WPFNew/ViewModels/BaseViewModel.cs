﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PinganHouse.SHHTS.RemoteServiceProxy;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels
{
    public class BaseViewModel:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)     //当变量改变时（必须是内存地址改变），才可以触发PropertyChanged事件
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
      
    }
}
