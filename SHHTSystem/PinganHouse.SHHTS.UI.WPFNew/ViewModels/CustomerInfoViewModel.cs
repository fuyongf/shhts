﻿using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.PubSubEvents;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPFNew.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels
{
    public class CustomerInfoViewModel:BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;

        public CustomerInfoViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;
            this.ShowMircroMessageCommand = new DelegateCommand<string>(this.OnShowMircroMessageCommand);
        }

        public ICommand ShowMircroMessageCommand { get; private set; }

        private void OnShowMircroMessageCommand(string model)
        {
            new ScanWeiChatDialog(model).ShowDialog();
        }

        private string customerId;
        public string CustomerId
        {
            get
            {
                return customerId;
            }
            set
            {
                if (customerId != value)
                {
                    customerId = value;
                    NotifyPropertyChanged("CustomerId");
                }
            }
        }

        private string customerName;
        public string CustomerName
        {
            get
            {
                return customerName;
            }
            set
            {
                if (customerName != value)
                {
                    customerName = value;
                    NotifyPropertyChanged("CustomerName");
                }
            }
        }
        /// <summary>
        /// 证件名称
        /// </summary>
        private CertificateType certificateName;
        public CertificateType CertificateName
        {
            get
            {
                return certificateName;
            }
            set
            {
                if (certificateName != value)
                {
                    certificateName = value;
                    NotifyPropertyChanged("CertificateName");
                }
            }
        }
        /// <summary>
        /// 证件号码
        /// </summary>
        private string certificateNo;
        public string CertificateNo
        {
            get
            {
                return certificateNo;
            }
            set
            {
                if (certificateNo != value)
                {
                    certificateNo = value;
                    NotifyPropertyChanged("CertificateNo");
                }
            }
        }
        /// <summary>
        /// 电话号码
        /// </summary>
        private string phoneNo;
        public string PhoneNo
        {
            get
            {
                return phoneNo;
            }
            set
            {
                if (phoneNo != value)
                {
                    phoneNo = value;
                    NotifyPropertyChanged("PhoneNo");
                }
            }
        }
        /// <summary>
        /// 邮箱地址
        /// </summary>
        private string emailAddress;
        public string EmailAddress
        {
            get
            {
                return emailAddress;
            }
            set
            {
                if (emailAddress != value)
                {
                    emailAddress = value;
                    NotifyPropertyChanged("EmailAddress");
                }
            }
        }
        private bool isBindingMircroMessage;
        public bool IsBindingMircroMessage
        {
            get
            {
                return isBindingMircroMessage;
            }
            set
            {
                if (isBindingMircroMessage != value)
                {
                    isBindingMircroMessage = value;
                    NotifyPropertyChanged("IsBindingMircroMessage");
                }
            }
        }

    }

}
