﻿using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.PubSubEvents;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;

        public MainViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;

            this.PreCheckAddCaseCommand = new DelegateCommand<object>(this.OnPreCheckAddCase);
            this.NuclearCommand = new DelegateCommand<object>(this.OnNuclear);
            this.QueryCaseCommand = new DelegateCommand<object>(this.OnQueryCase);
        }
        
        public ICommand PreCheckAddCaseCommand { get; private set; }

        private void OnPreCheckAddCase(object model)
        {
            this.eventAggregator.GetEvent<PreCheckAddCaseEvents>().Publish(model);
        }

        public ICommand NuclearCommand { get; private set; }

        private void OnNuclear(object model)
        {
            this.eventAggregator.GetEvent<NuclearEvents>().Publish(Microsoft.Practices.ServiceLocation.ServiceLocator.Current.GetInstance<CheckCaseSearchPageViewModel>());
        }

        public ICommand QueryCaseCommand { get; private set; }

        private void OnQueryCase(object model)
        {
            this.eventAggregator.GetEvent<QueryCasePageEvents>().Publish(model);
        }

    }
}
