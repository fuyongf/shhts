﻿using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.PubSubEvents;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels
{
    public class ShowMircroMessageViewModel : BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;

        public ShowMircroMessageViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;
            SellerList = new ObservableCollection<CustomerInfoViewModel>();
            BuyerList = new ObservableCollection<CustomerInfoViewModel>();
            this.UpdateCustomerInfoInShowMircroMessageCommand = new DelegateCommand<ShowMircroMessageViewModel>(this.OnUpdateCustomerInfoInShowMircroMessageCommand);
        }

        private string caseId;
        public string CaseId
        {
            get
            {
                return caseId;
            }
            set
            {
                if (caseId != value)
                {
                    caseId = value;
                    NotifyPropertyChanged("CaseId");
                }
            }
        }

        private ObservableCollection<CustomerInfoViewModel> sellerList;
        public ObservableCollection<CustomerInfoViewModel> SellerList
        {
            get
            {
                return sellerList;
            }
            set
            {
                if (sellerList != value)
                {
                    sellerList = value;
                    NotifyPropertyChanged("SellerList");
                }
            }
        }

        private ObservableCollection<CustomerInfoViewModel> buyerList;
        public ObservableCollection<CustomerInfoViewModel> BuyerList
        {
            get
            {
                return buyerList;
            }
            set
            {
                if (buyerList != value)
                {
                    buyerList = value;
                    NotifyPropertyChanged("BuyerList");
                }
            }
        }

        public ICommand UpdateCustomerInfoInShowMircroMessageCommand { get; private set; }


        private void OnUpdateCustomerInfoInShowMircroMessageCommand(ShowMircroMessageViewModel showMircroMessageViewModel)
        {
            this.eventAggregator.GetEvent<ShowMircroMessageUpdateCustomerInfoEvents>().Publish(this);
        }
        
    }
}
