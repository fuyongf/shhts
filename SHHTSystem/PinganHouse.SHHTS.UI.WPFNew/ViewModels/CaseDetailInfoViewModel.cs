﻿using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.ServiceLocation;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using PinganHouse.SHHTS.UI.WPFNew.Views;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels
{
    public class CaseDetailInfoViewModel:BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;

        #region Ctor
        public CaseDetailInfoViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;
            this.CaseDetailInfoCommand = new DelegateCommand<string>(this.OnCaseDetailInfoCommand);
            this.EnterUploadAttachedPageCommand = new DelegateCommand<string>(this.OnEnterUploadAttachedPageCommand);
            this.PreCheckCancerCaseCommand = new DelegateCommand<string>(this.OnPreCheckCancerCaseCommand);
            this.PreCheckPrintCaseCommand = new DelegateCommand<string>(this.OnPreCheckPrintCaseCommand);
            this.ShowMircroMessageCommand = new DelegateCommand<string>(this.OnShowMircroMessageCommand);
            this.SetSearchConditionCommand = new DelegateCommand<SearchListPageViewModel>(this.OnSetSearchConditionCommand);
            this.ResetSearchConditionCommand = new DelegateCommand<SearchListPageViewModel>(this.OnResetSearchConditionCommand);
            this.NuclearCaseDetailInfoCommand = new DelegateCommand<string>(this.OnNuclearCaseDetailInfoCommand);
        }

        
        
        #endregion

        #region Private method
        /// <summary>
        /// 取消签约
        /// </summary>
        /// <param name="obj"></param>
        private void OnPreCheckCancerCaseCommand(string caseId)
        {
            this.eventAggregator.GetEvent<PreCheckCancerCaseEvents>().Publish(caseId);
        }
        /// <summary>
        /// 打印条码
        /// </summary>
        /// <param name="obj"></param>
        private void OnPreCheckPrintCaseCommand(string caseId)
        {
            this.eventAggregator.GetEvent<PreCheckPrintCaseEvents>().Publish(caseId);
        }
        /// <summary>
        /// 进入上传附件页面
        /// </summary>
        /// <param name="caseId"></param>
        private void OnEnterUploadAttachedPageCommand(string caseId)
        {
            this.eventAggregator.GetEvent<AttachmentsDetailEvents>().Publish(caseId);
        }
        /// <summary>
        /// 设置高级搜索
        /// </summary>
        /// <param name="model"></param>
        private void OnSetSearchConditionCommand(SearchListPageViewModel model)
        {
             //搜素时间的验证
            CaseDetailInfoViewModel condition =this ;
            if (condition.StartTime == null && condition.EndTime != null)
            {
                MessageBox.Show("请选择搜索的开始时间", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (condition.StartTime != null)
            {
                var time1 = (DateTime) condition.StartTime;
                if (condition.EndTime != null)
                {
                    var time2 = (DateTime) condition.EndTime;
                    var result = DateTime.Compare(time2, time1);
                    if (result < 0 || result == 0)
                    {
                        MessageBox.Show("搜索的结束时间不能小于或等于开始时间", "系统提示", MessageBoxButton.OK,
                            MessageBoxImage.Warning);
                        return;
                    }
                }
            }
            if (model == null)
            {
                model = ServiceLocator.Current.GetInstance<SearchListPageViewModel>();
                model.SearchCaseCondition = this;
            }

            this.eventAggregator.GetEvent<LoadPreCheckCaseListEvents>().Publish(model);
        }
        private void OnResetSearchConditionCommand(SearchListPageViewModel model)
        {
            //if (model == null)
            //{
            //    model = ServiceLocator.Current.GetInstance<SearchListPageViewModel>();
            //    model.SearchCaseCondition.CaseId = null;
            //    model.SearchCaseCondition.Address = null;
            //    model.SearchCaseCondition.SponsorName = null;
            //    model.SearchCaseCondition.BuyerName = null;
            //    model.SearchCaseCondition.AgentPerson = null;
            //}
            CaseId = null;
            SponsorName = null;
            Address = null;
            AgentPerson = null;
            BuyerName = null;
        }

        /// <summary>
        /// 详细信息
        /// </summary>
        /// <param name="model"></param>
        private void OnCaseDetailInfoCommand(string caseId)
        {
            this.eventAggregator.GetEvent<LoadBasicInfoByCaseIdEvents>().Publish(caseId);
        }
        /// <summary>
        /// 显示微信界面
        /// </summary>
        /// <param name="model"></param>
        private void OnShowMircroMessageCommand(string model)
        {
            this.eventAggregator.GetEvent<ShowMircroMessageEvents>().Publish(model);
        }

        /// <summary>
        /// 核案列表里的案件详情
        /// </summary>
        /// <param name="caseId"></param>
        private void OnNuclearCaseDetailInfoCommand(string caseId)
        {
            this.eventAggregator.GetEvent<LoadNuclearCaseDetailInfoCommandEvents>().Publish(caseId);
        }

        #endregion

        #region IComands
        public ICommand SetSearchConditionCommand { get; private set;}
        public ICommand ResetSearchConditionCommand { get; private set; }
        public ICommand CaseDetailInfoCommand { get; private set; }
        public ICommand ShowMircroMessageCommand { get; private set; }
        public ICommand EnterUploadAttachedPageCommand { get; private set; }
        public ICommand PreCheckCancerCaseCommand { get; private set; }
        public ICommand PreCheckPrintCaseCommand { get; private set; }

        public ICommand NuclearCaseDetailInfoCommand { get; private set; }
        #endregion


        #region Property Members
        /// <summary>
        /// 案件编号
        /// </summary>
        private string caseId;
        public string CaseId
        {
            get
            {
                return caseId;
            }
            set
            {
                if (caseId != value)
                {
                    caseId = value;
                    NotifyPropertyChanged("CaseId");
                }
            }
        }
        /// <summary>
        /// 中介公司
        /// </summary>
        private string agentCompany;
        public string AgentCompany
        {
            get
            {
                return agentCompany;
            }
            set
            {
                if (agentCompany != value)
                {
                    agentCompany = value;
                    NotifyPropertyChanged("AgentCompany");
                }
            }
        }
        /// <summary>
        /// 经纪人
        /// </summary>
        private string agentPerson;
        public string AgentPerson
        {
            get
            {
                return agentPerson;
            }
            set
            {
                if (agentPerson != value)
                {
                    agentPerson = value;
                    NotifyPropertyChanged("AgentPerson");
                }
            }
        }
        /// <summary>
        /// 买方姓名
        /// </summary>
        private string buyerName;
        public string BuyerName
        {
            get
            {
                return buyerName;
            }
            set
            {
                if (buyerName != value)
                {
                    buyerName = value;
                    NotifyPropertyChanged("BuyerName");
                }
            }
        }
        private string address;
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                if (address != value)
                {
                    address = value;
                    NotifyPropertyChanged("Address");
                }
            }
        }
        /// <summary>
        /// 创建事件
        /// </summary>
        private DateTime createTime;
        public DateTime CreateTime
        {
            get
            {
                return createTime;
            }
            set
            {
                if (createTime != value)
                {
                    createTime = value;
                    NotifyPropertyChanged("CreateTime");
                }
            }
        }
        /// <summary>
        /// 签约事件
        /// </summary>
        private DateTime signedTime;
        public DateTime SignedTime
        {
            get
            {
                return signedTime;
            }
            set
            {
                if (signedTime != value)
                {
                    signedTime = value;
                    NotifyPropertyChanged("SignedTime");
                }
            }
        }
        /// <summary>
        /// 主办人
        /// </summary>
        private string sponsorName;
        public string SponsorName
        {
            get
            {
                return sponsorName;
            }
            set
            {
                if (sponsorName != value)
                {
                    sponsorName = value;
                    NotifyPropertyChanged("SponsorName");
                }
            }
        }
        /// <summary>
        /// 受理人
        /// </summary>
        private string acceptorName;
        public string AcceptorName
        {
            get
            {
                return acceptorName;
            }
            set
            {
                if (acceptorName != value)
                {
                    acceptorName = value;
                    NotifyPropertyChanged("AcceptorName");
                }
            }
        }
        /// <summary>
        /// 案件状态
        /// </summary>
        private CaseStatus? status;
        public CaseStatus? Status
        {
            get
            {
                return status;
            }
            set
            {
                if (status != value)
                {
                    status = value;
                    NotifyPropertyChanged("Status");
                }
            }
        }
        /// <summary>
        /// 操作
        /// </summary>
        private string operation;
        public string Operation
        {
            get
            {
                return operation;
            }
            set
            {
                if (operation != value)
                {
                    operation = value;
                    NotifyPropertyChanged("Operation");
                }
            }
        }

        private DateTime? startTime;
        public DateTime? StartTime
        {           
            get
            {  
                return startTime;             
            }
            set
            {
                if (startTime != value)
                {
                    startTime = value;
                    
                    NotifyPropertyChanged("StartTime");
                }
            }
        }   
        private DateTime? endTime;
        public DateTime? EndTime
        {
            get
            {              
                return endTime;
            }
            set
            {
                if (endTime != value)
                {
                    endTime = value;                  
                    NotifyPropertyChanged("EndTime");
                }
            }
        }

        private IList<string> nextAction;
        public IList<string> NextAction
        {
            get
            {
                return nextAction;
            }
            set
            {
                if (nextAction != value)
                {
                    nextAction = value;
                    NotifyPropertyChanged("NextAction");
                }
            }
        }

        private Tuple<IList<string>, string> nextWithCaseId;
        public Tuple<IList<string>, string> NextActionWithCaseId
        {
            get
            {
                return nextWithCaseId;
            }
            set
            {
                if (nextAction != value)
                {
                    nextWithCaseId = value;
                    NotifyPropertyChanged("NextActionWithCaseId");
                }
            }
        }

        #endregion
    }
}
