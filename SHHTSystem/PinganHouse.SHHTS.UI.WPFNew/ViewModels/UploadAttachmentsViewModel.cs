﻿using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.PubSubEvents;

using Microsoft.Practices.ServiceLocation;
using PinganHouse.SHHTS.UI.WPFNew.Common;

using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels
{
    public class UploadAttachmentsViewModel : BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;
       
        public UploadAttachmentsViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;
            //集合必须要求初始化
            RightsMateriaList = new ObservableCollection<MateriaViewModel>();
            TranscationList = new ObservableCollection<MateriaViewModel>();
            ContractList = new ObservableCollection<MateriaViewModel>();
            MoneyList = new ObservableCollection<MateriaViewModel>();
            BuyerList = new ObservableCollection<CustomerAndMateriaListViewModel>();
            SellerList = new ObservableCollection<CustomerAndMateriaListViewModel>();
            this.AddNewCaseCommand = new DelegateCommand<object>(this.OnAddNewCaseCommand);
            this.SearchCaseListCommand = new DelegateCommand<SearchListPageViewModel>(this.OnSearchCaseListCommand);

           
            
        }

        private void OnSearchCaseListCommand(SearchListPageViewModel obj)
        {
            this.eventAggregator.GetEvent<LoadPreCheckCaseListEvents>().Publish(obj);
        }

        private void OnAddNewCaseCommand(object obj)
        {
            this.eventAggregator.GetEvent<PreCheckAddCaseEvents>().Publish(obj);
        }
        public ICommand AddNewCaseCommand { get; private set; }

        public ICommand SearchCaseListCommand { get; private set; }
        private CustomerInfoViewModel currentCustomer;
        public CustomerInfoViewModel CurrentCustomer
        {
            get
            {
                return currentCustomer;
            }
            set
            {
                if (currentCustomer != value)
                {
                    currentCustomer = value;
                    NotifyPropertyChanged("CurrentCustomer");
                }
            }
        }

        private CustomerAndMateriaListViewModel  currentItem;
        public CustomerAndMateriaListViewModel  CurrentItem
        {
            get
            {
                return currentItem;
            }
            set
            {
                if (currentItem != value)
                {
                    currentItem = value;
                    NotifyPropertyChanged("CurrentItem");
                    if(CurrentItem !=null)
                    { 
                    var list = (SellerList.Where(x => x == value).FirstOrDefault()).MateriaList;
                    foreach (var m in MateriaList)
                    {
                        foreach(var i in list)
                        {
                            if(i.MateriaName ==m.MateriaName)
                            {
                                m.MateriaCount = i.MateriaCount;
                            }
                        }
                    } 
                    foreach (var m in MateriaList1)
                    {
                        foreach (var i in list)
                        {
                            if (i.MateriaName == m.MateriaName)
                            {
                                m.MateriaCount = i.MateriaCount;
                            }
                        }
                    }
                    CurrentCustomer = (SellerList.Where(x => x == value).FirstOrDefault()).CustomerInfo;
                    }
                }
            }
        }
        private CustomerInfoViewModel buyCurrentCustomer;
        public CustomerInfoViewModel BuyCurrentCustomer
        {
            get
            {
                return buyCurrentCustomer;
            }
            set
            {
                if (buyCurrentCustomer != value)
                {
                    buyCurrentCustomer = value;
                    NotifyPropertyChanged("BuyCurrentCustomer");
                }
            }
        }
        private CustomerAndMateriaListViewModel buyCurrentItem;
        public CustomerAndMateriaListViewModel BuyCurrentItem
        {
            get
            {
                return buyCurrentItem;
            }
            set
            {
                if (buyCurrentItem != value)
                {
                    buyCurrentItem = value;
                    NotifyPropertyChanged("BuyCurrentItem");
                    if (buyCurrentItem != null)
                    {
                        var list = (BuyerList.Where(x => x == value).FirstOrDefault()).MateriaList;
                        foreach (var m in BuyMateriaList)
                        {
                            foreach (var i in list)
                            {
                                if (i.MateriaName == m.MateriaName)
                                {
                                    m.MateriaCount = i.MateriaCount;
                                }
                            }
                        }
                        foreach (var m in BuyMateriaList1)
                        {
                            foreach (var i in list)
                            {
                                if (i.MateriaName == m.MateriaName)
                                {
                                    m.MateriaCount = i.MateriaCount;
                                }
                            }
                        }
                        BuyCurrentCustomer = (BuyerList.Where(x => x == value).FirstOrDefault()).CustomerInfo;
                    }
                }
            }
        }

        private bool isNavList;

        public bool IsNavList
        {
            get { return isNavList; }
            set
            {
                if (isNavList != value)
                {
                    isNavList = value;
                    if (isNavList == true)
                    {
                        var model = ServiceLocator.Current.GetInstance<SearchListPageViewModel>();
                        this.eventAggregator.GetEvent<LoadPreCheckCaseListEvents>().Publish(model);
                        NotifyPropertyChanged("IsNavList");
                    }
                }
            }
        }

        private bool isNavFile;

        public bool IsNavFile
        {
            get { return isNavFile; }
            set
            {
                if (isNavFile != value)
                {
                    isNavFile = value;
                    if (isNavFile == true)
                    {                   
                        this.eventAggregator.GetEvent<AttachmentsDetailEvents>().Publish(this.caseId);
                        NotifyPropertyChanged("IsNavFile");
                    }
                }
            }
        }

        private string caseId;
        public string CaseId
        {
            get
            {
                return caseId;
            }
            set
            {
                if (caseId != value)
                {
                    caseId = value;
                    NotifyPropertyChanged("CaseId");
                }
            }
        }

        private string caseStatus;
        public string CaseStatus
        {
            get
            {
                return caseStatus;
            }
            set
            {
                if (caseStatus != value)
                {
                    caseStatus = value;
                    NotifyPropertyChanged("CaseStatus");
                }
            }
        }

        private ShowCaseModel showCase;
        public ShowCaseModel ShowCase
        {
            get
            {
                return showCase;
            }
            set
            {
                if (showCase != value)
                {
                    showCase = value;
                    NotifyPropertyChanged("ShowCase");
                }
            }
        }

        private PreCheckAddCaseViewModel caseDetailInfo;
        public PreCheckAddCaseViewModel CaseDetailInfo
        {
            get
            {
                return caseDetailInfo;
            }
            set
            {
                if (caseDetailInfo != value)
                {
                    caseDetailInfo = value;
                    NotifyPropertyChanged("CaseDetailInfo");
                }
            }
        }

        /// <summary>
        /// 卖方
        /// </summary>
        private ObservableCollection<CustomerAndMateriaListViewModel> sellerList;
        public ObservableCollection<CustomerAndMateriaListViewModel> SellerList
        {
            get
            {
                return sellerList;
            }
            set
            {
                if (sellerList != value)
                {
                    sellerList = value;
                    NotifyPropertyChanged("SellerList");
                }
            }
        }

        /// <summary>
        /// 买方
        /// </summary>
        private ObservableCollection<CustomerAndMateriaListViewModel> buyerList;
        public ObservableCollection<CustomerAndMateriaListViewModel> BuyerList
        {
            get
            {
                return buyerList;
            }
            set
            {
                if (buyerList != value)
                {
                    buyerList = value;
                    NotifyPropertyChanged("BuyerList");
                }
            }
        }

        /// <summary>
        /// 权利凭证
        /// </summary>
        private ObservableCollection<MateriaViewModel> rightsMateriaList;
        public ObservableCollection<MateriaViewModel> RightsMateriaList
        {
            get
            {
                return rightsMateriaList;
            }
            set
            {
                if (rightsMateriaList != value)
                {
                    rightsMateriaList = value;
                    NotifyPropertyChanged("RightsMateriaList");
                }
            }
        }

        /// <summary>
        /// 交易材料
        /// </summary>
        private ObservableCollection<MateriaViewModel> transactionList;
        public ObservableCollection<MateriaViewModel> TranscationList
        {
            get
            {
                return transactionList;
            }
            set
            {
                if (transactionList != value)
                {
                    transactionList = value;
                    NotifyPropertyChanged("TranscationList");
                }
            }
        }

        /// <summary>
        /// 合同协议
        /// </summary>
        private ObservableCollection<MateriaViewModel> contractList;
        public ObservableCollection<MateriaViewModel> ContractList
        {
            get
            {
                return contractList;
            }
            set
            {
                if (contractList != value)
                {
                    contractList = value;
                    NotifyPropertyChanged("ContractList");
                }
            }
        }

        /// <summary>
        /// 钱款收据
        /// </summary>
        private ObservableCollection<MateriaViewModel> moneyList;
        public ObservableCollection<MateriaViewModel> MoneyList
        {
            get
            {
                return moneyList;
            }
            set
            {
                if (moneyList != value)
                {
                    moneyList = value;
                    NotifyPropertyChanged("MoneyList");
                }
            }
        }

        /// <summary>
        /// 附件列表信息
        /// </summary>
        private ObservableCollection<MateriaViewModel> materiaList;
        public ObservableCollection<MateriaViewModel> MateriaList
        {
            get
            {
                return materiaList;
            }
            set
            {
                if (materiaList != value)
                {
                    materiaList = value;
                    NotifyPropertyChanged("MateriaList");
                }
            }
        }
        /// <summary>
        /// 附件列表信息
        /// </summary>
        private ObservableCollection<MateriaViewModel> materiaList1;
        public ObservableCollection<MateriaViewModel> MateriaList1
        {
            get
            {
                return materiaList1;
            }
            set
            {
                if (materiaList1 != value)
                {
                    materiaList1 = value;
                    NotifyPropertyChanged("MateriaList1");
                }
            }
        }

        private ObservableCollection<MateriaViewModel> buyMateriaList;
        public ObservableCollection<MateriaViewModel> BuyMateriaList
        {
            get
            {
                return buyMateriaList;
            }
            set
            {
                if (buyMateriaList != value)
                {
                    buyMateriaList = value;
                    NotifyPropertyChanged("BuyMateriaList");
                }
            }
        }

        private ObservableCollection<MateriaViewModel> buyMateriaList1;
        public ObservableCollection<MateriaViewModel> BuyMateriaList1
        {
            get
            {
                return buyMateriaList1;
            }
            set
            {
                if (buyMateriaList1 != value)
                {
                    buyMateriaList1 = value;
                    NotifyPropertyChanged("BuyMateriaList1");
                }
            }
        }

        /// <summary>
        /// 产证编号
        /// </summary>
        private string houseContractId;
        public string HouseContractId
        {
            get
            {
                return houseContractId;
            }
            set
            {
                if (houseContractId != value)
                {
                    houseContractId = value;
                    NotifyPropertyChanged("HouseContractId");
                }
            }
        }

        /// <summary>
        /// 当前选择的tabitem
        /// </summary>
        private int currentIndexTabItem;
        public int CurrentIndexTabItem
        {
            get
            {
                return currentIndexTabItem;
            }
            set
            {
                if (currentIndexTabItem != value)
                {
                    currentIndexTabItem = value;
                    NotifyPropertyChanged("CurrentIndexTabItem");
                }
            }
        }
        /// <summary>
        /// 选择基本信息TabItem
        /// </summary>
        private bool isSelectBasicInfo;
        public bool IsSelectBasicInfo
        {
            get
            {
                return isSelectBasicInfo;
            }
            set
            {
                if (isSelectBasicInfo != value)
                {
                    isSelectBasicInfo = value;
                    NotifyPropertyChanged("IsSelectBasicInfo");
                }
            }
        }
        /// <summary>
        /// 选择附件TabItem
        /// </summary>
        private bool isSelectUploadInfo;
        public bool IsSelectUploadInfo
        {
            get
            {
                return isSelectUploadInfo;
            }
            set
            {
                if (isSelectUploadInfo != value)
                {
                    isSelectUploadInfo = value;
                    NotifyPropertyChanged("IsSelectUploadInfo");
                }
            }
        }


        private CaseStatusViewModel allStatus;
        public CaseStatusViewModel AllStatus
        {
            get
            {
                return allStatus;
            }
            set
            {
                if (allStatus != value)
                {
                    allStatus = value;
                    NotifyPropertyChanged("AllStatus");
                }
            }
        }

    }

    public class CustomerAndMateriaListViewModel : BaseViewModel
    {
        public CustomerAndMateriaListViewModel()
        {
            MateriaList = new ObservableCollection<MateriaViewModel>();
        }
        private CustomerInfoViewModel customerInfo;
        public CustomerInfoViewModel CustomerInfo
        {
            get
            {
                return customerInfo;
            }
            set
            {
                if (customerInfo != value)
                {
                    customerInfo = value;
                    NotifyPropertyChanged("CustomerInfo");
                }
            }
        }

        private ObservableCollection<MateriaViewModel> materiaList;
        public ObservableCollection<MateriaViewModel> MateriaList
        {
            get
            {
                return materiaList;
            }
            set
            {
                if (materiaList != value)
                {
                    materiaList = value;
                    NotifyPropertyChanged("MateriaList");
                }
            }
        }

    }
}
