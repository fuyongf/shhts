﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.PubSubEvents;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels
{
    public class CaseTransactionInfoViewModel : BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;

        public CaseTransactionInfoViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;

        }


        private string caseId;
        public string CaseId
        {
            get
            {
                return caseId;
            }

            set
            {
                if (caseId != value)
                {
                    caseId = value;
                    NotifyPropertyChanged("string CaseId");
                }
            }
        }

    }
}
