﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.PubSubEvents;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPFNew.Common;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels
{
    public class ConfirmContractDialogViewModel : BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;

        public ConfirmContractDialogViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;

            this.ConfirmContractEnterCommand = new DelegateCommand<ConfirmContractDialogViewModel>(this.OnConfirmContractEnterCommand);
            AllegeList = new ObservableCollection<EnumHelper>();
            CreditTypeList = new ObservableCollection<EnumHelper>();
            LoanObjectList = new ObservableCollection<EnumHelper>();
            ResponseObjectList = new ObservableCollection<Tuple<string, string>>();
            CommissionersList = new ObservableCollection<Tuple<string, string>>();
            BankList = new ObservableCollection<Tuple<string, string>>();
        }

        private string caseId;
        public string CaseId
        {
            get
            {
                return caseId;
            }
            set
            {
                if (caseId != value)
                {
                    caseId = value;
                    NotifyPropertyChanged("CaseId");
                }
            }
        }

        /// <summary>
        /// 产权证号
        /// </summary>
        private string tenementContract;
        public string TenementContract
        {
            get
            {
                return tenementContract;
            }
            set
            {
                if (tenementContract != value)
                {
                    tenementContract = value;
                    NotifyPropertyChanged("TenementContract");
                }
            }
        }

        /// <summary>
        /// 网签价格
        /// </summary>
        private decimal netLabelPrice;
        public string NetLabelPrice
        {
            get
            {
                if (netLabelPrice == 0)
                {
                    return string.Empty;
                }
                else
                {
                    return netLabelPrice.ToString();
                }

            }
            set
            {
                if (netLabelPrice.ToString() != value)
                {
                    decimal.TryParse(value, out netLabelPrice);
                    NotifyPropertyChanged("NetLabelPrice");
                }
            }
        }
        
        /// <summary>
        /// 是否立即审税
        /// </summary>
        private bool isTrialTaxYes;
        public bool IsTrialTaxYes
        {
            get { return isTrialTaxYes; }
            set
            {
                if (isTrialTaxYes != value)
                {
                    isTrialTaxYes = value;
                    NotifyPropertyChanged("IsTrialTaxYes");
                }
            }
        }
        private bool isTrialTaxNo;
        public bool IsTrialTaxNo
        {
            get { return isTrialTaxNo; }
            set
            {
                if (isTrialTaxNo != value)
                {
                    isTrialTaxNo = value;
                    NotifyPropertyChanged("IsTrialTaxNo");
                }
            }
        }
        /// <summary>
        /// 是否申请贷款
        /// </summary>
        private ObservableCollection<EnumHelper> allegeList;
        public ObservableCollection<EnumHelper> AllegeList
        {
            get
            {
                return allegeList;
            }
            set
            {
                if (allegeList != value)
                {
                    allegeList = value;
                    NotifyPropertyChanged("AllegeList");
                }
            }
        }

        /// <summary>
        /// 贷款类型
        /// </summary>
        private ObservableCollection<EnumHelper> creditTypeList;
        public ObservableCollection<EnumHelper> CreditTypeList
        {
            get
            {
                return creditTypeList;
            }
            set
            {
                if (creditTypeList != value)
                {
                    creditTypeList = value;
                    NotifyPropertyChanged("CreditTypeList");
                }
            }
        }

        /// <summary>
        /// 推送对象
        /// </summary>
        private ObservableCollection<EnumHelper> loanObjectList;
        public ObservableCollection<EnumHelper> LoanObjectList
        {
            get
            {
                return loanObjectList;
            }
            set
            {
                if (loanObjectList != value)
                {
                    loanObjectList = value;
                    NotifyPropertyChanged("LoanObjectList");
                }
            }
        }

        private ObservableCollection<Tuple<string, string>> responseObjectList;
        public ObservableCollection<Tuple<string, string>> ResponseObjectList
        {
            get
            {
                return responseObjectList;
            }
            set
            {
                if (responseObjectList != value)
                {
                    responseObjectList = value;
                    NotifyPropertyChanged("ResponseObjectList");
                }
            }
        }

        private ObservableCollection<Tuple<string, string>> commissionersList;
        public ObservableCollection<Tuple<string, string>> CommissionersList
        {
            get
            {
                return commissionersList;
            }
            set
            {
                if (commissionersList != value)
                {
                    commissionersList = value;
                    NotifyPropertyChanged("CommissionersList");
                }
            }
        }

        private ObservableCollection<Tuple<string, string>> bankList;
        public ObservableCollection<Tuple<string, string>> BankList
        {
            get
            {
                return bankList;
            }
            set
            {
                if (bankList != value)
                {
                    bankList = value;
                    NotifyPropertyChanged("BankList");
                }
            }
        }

        private string responseObjectCurrentValue = "A-NULL";

        public string ResponseObjectCurrentValue
        {
            get { return responseObjectCurrentValue; }
            set
            {
                if (responseObjectCurrentValue != value)
                {
                    responseObjectCurrentValue = value;
                    NotifyPropertyChanged("ResponseObjectCurrentValue");
                }
            }
        }

        private int creditTypeCurrentValue = 0;
        public int CreditTypeCurrentValue
        {
            get { return creditTypeCurrentValue; }
            set
            {
                if (creditTypeCurrentValue != value)
                {
                    creditTypeCurrentValue = value;
                    NotifyPropertyChanged("CreditTypeCurrentValue");
                }
               
            }
        }

        private int allegeCurrentValue = 1;
        public int AllegeCurrentValue
        {
            get { return allegeCurrentValue; }
            set
            {
                if (allegeCurrentValue != value)
                {
                    allegeCurrentValue = value;
                    NotifyPropertyChanged("AllegeCurrentValue");
                }
                if (allegeCurrentValue == 0)
                {
                    ComboBoxIsEnabled = false;
                }
                else
                {
                    ComboBoxIsEnabled = true;
                }
            }
        }

        private bool comboBoxIsEnabled = true;
        public bool ComboBoxIsEnabled
        {
            get { return comboBoxIsEnabled; }
            set
            {
                if (comboBoxIsEnabled != value)
                {
                    comboBoxIsEnabled = value;
                    NotifyPropertyChanged("ComboBoxIsEnabled");
                }
                
            }
        }

        private int loanCurrentIndex = 0;
        public int LoanCurrentIndex
        {
            get
            {
                return loanCurrentIndex;
            }
            set
            {
                if (loanCurrentIndex != value)
                {
                    loanCurrentIndex = value;
                    NotifyPropertyChanged("loanCurrentIndex");
                }
                if (loanCurrentIndex == 1)
                {
                    ResponseObjectList = commissionersList;
                    ResponseObjectCurrentValue = "A-NULL";
                }
                else
                {
                    ResponseObjectList = bankList;
                    ResponseObjectCurrentValue = "A-NULL";
                }


            }
        }

        public ICommand ConfirmContractEnterCommand { get; private set; }
        private void OnConfirmContractEnterCommand(ConfirmContractDialogViewModel vm)
        {
            this.eventAggregator.GetEvent<ConfirmContractEnterEvents>().Publish(this);
        }

    }
}
