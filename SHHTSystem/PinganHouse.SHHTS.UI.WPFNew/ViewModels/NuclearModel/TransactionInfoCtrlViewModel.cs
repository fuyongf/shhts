﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.PubSubEvents;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPFNew.Controls.Nuclear;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using System.Collections.ObjectModel;
using PinganHouse.SHHTS.DataTransferObjects;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels
{
    public class TransactionInfoCtrlViewModel : BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;

        public TransactionInfoCtrlViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;

            this.AddBuyerCollectionFundCommand = new DelegateCommand<string>(this.OnAddBuyerCollectionFundCommand);
            this.AddBuyerPaymentFundCommand = new DelegateCommand<string>(this.OnAddBuyerPaymentFundCommand);
            this.AddSellerCollectionFundCommand = new DelegateCommand<string>(this.OnAddSellerCollectionFundCommand);
            this.AddSellerPaymentFundCommand = new DelegateCommand<string>(this.OnAddSellerPaymentFundCommand);
            this.DeleteFundFlowCommand = new DelegateCommand<string>(this.OnDeleteFundFlowCommand);
        }

        private string caseId;
        public string CaseId
        {
            get
            {
                return caseId;
            }

            set
            {
                if (caseId != value)
                {
                    caseId = value;
                    NotifyPropertyChanged("string CaseId");
                }
            }
        }

        
        private ObservableCollection<CustomerFundFlowViewModel> flowList_Buyer;

        public ObservableCollection<CustomerFundFlowViewModel> FlowList_Buyer
        {
            get
            {
                return flowList_Buyer;
            }

            set
            {
                if (flowList_Buyer != value)
                {
                    flowList_Buyer = value;
                    NotifyPropertyChanged("FlowList_Buyer");
                    foreach (var item in flowList_Buyer)
                    {
                        if (item.SysNo != 0)
                        {
                            if (item.Amount > 0)
                            {
                                buyerIncomeAmount += item.Amount;
                                IncomeAmount += item.Amount;
                            }
                            else
                            {
                                buyerExpensesAmount += item.Amount;
                                expensesAmount += item.Amount;
                            }
                        }
                    }
                }
            }
        }

        private ObservableCollection<CustomerFundFlowViewModel> flowList_Seller;

        public ObservableCollection<CustomerFundFlowViewModel> FlowList_Seller
        {
            get
            {
                return flowList_Seller;
            }

            set
            {
                if (flowList_Seller != value)
                {
                    flowList_Seller = value;
                    NotifyPropertyChanged("FlowList_Seller");
                    foreach (var item in flowList_Seller)
                    {
                        if (item.SysNo != 0)
                        {
                            if (item.Amount > 0)
                            {
                                sellerIncomeAmount += item.Amount;
                                IncomeAmount += item.Amount;
                            }
                            else
                            {
                                sellerExpensesAmount += item.Amount;
                                expensesAmount += item.Amount;
                            }
                        }
                    }
                }
            }
        }


        private decimal buyerIncomeAmount=0;
        /// <summary>
        /// 买方收入合计
        /// </summary>
        public decimal BuyerIncomeAmount
        {
            get
            {
                return buyerIncomeAmount;
            }

            //set
            //{
            //    if (buyerIncomeAmount != value)
            //    {
            //        buyerIncomeAmount = value;
            //        NotifyPropertyChanged("BuyerIncomeAmount");
            //    }
            //}
        }


        private decimal buyerExpensesAmount = 0;
        /// <summary>
        /// 买方支出合计
        /// </summary>
        public decimal BuyerExpensesAmount
        {
            get
            {
                return buyerExpensesAmount;
            }

            set
            {
                if (buyerExpensesAmount != value)
                {
                    buyerExpensesAmount = value;
                    NotifyPropertyChanged("BuyerExpensesAmount");
                }
            }
        }


        private decimal sellerIncomeAmount = 0;
        /// <summary>
        /// 卖方收入合计
        /// </summary>
        public decimal SellerIncomeAmount
        {
            get
            {
                return sellerIncomeAmount;
            }

            set
            {
                if (sellerIncomeAmount != value)
                {
                    sellerIncomeAmount = value;
                    NotifyPropertyChanged("SellerIncomeAmount");
                }
            }
        }


        private decimal sellerExpensesAmount = 0;
        /// <summary>
        /// 卖方支出合计
        /// </summary>
        public decimal SellerExpensesAmount
        {
            get
            {
                return sellerExpensesAmount;
            }

            set
            {
                if (sellerExpensesAmount != value)
                {
                    sellerExpensesAmount = value;
                    NotifyPropertyChanged("SellerExpensesAmount");
                }
            }
        }


        private decimal incomeAmount = 0;
        /// <summary>
        /// 买卖方总收入合计
        /// </summary>
        public decimal IncomeAmount
        {
            get
            {
                return incomeAmount;
            }

            set
            {
                if (incomeAmount != value)
                {
                    incomeAmount = value;
                    NotifyPropertyChanged("MyProperty");
                }
            }
        }


        private decimal expensesAmount = 0;
        /// <summary>
        /// 买卖方总支出合计
        /// </summary>
        public decimal ExpensesAmount
        {
            get
            {
                return expensesAmount;
            }

            set
            {
                if (expensesAmount != value)
                {
                    expensesAmount = value;
                    NotifyPropertyChanged("ExpensesAmount");
                }
            }
        }


        /// <summary>
        /// 买家收款命令
        /// </summary>
        public ICommand AddBuyerCollectionFundCommand { get; private set; }

        private void OnAddBuyerCollectionFundCommand(string caseId)
        {
            OnAddFundCommand(CustomerType.Buyer, ChangeFundType.Collection, caseId);
        }
        /// <summary>
        /// 买家出款命令
        /// </summary>
        public ICommand AddBuyerPaymentFundCommand { get; private set; }

        private void OnAddBuyerPaymentFundCommand(string caseId)
        {
            OnAddFundCommand(CustomerType.Buyer, ChangeFundType.Payment, caseId);
        }
        /// <summary>
        /// 卖家收款命令
        /// </summary>
        public ICommand AddSellerCollectionFundCommand { get; private set; }

        private void OnAddSellerCollectionFundCommand(string caseId)
        {
            OnAddFundCommand(CustomerType.Seller, ChangeFundType.Collection, caseId);
        }
        /// <summary>
        /// 卖家出款命令
        /// </summary>
        public ICommand AddSellerPaymentFundCommand { get; private set; }

        private void OnAddSellerPaymentFundCommand(string caseId)
        {
            OnAddFundCommand(CustomerType.Seller, ChangeFundType.Payment, caseId);
        }

        private void OnAddFundCommand(CustomerType customerType, ChangeFundType fundType, string caseId)
        {
            //FundFlowDialog win = new FundFlowDialog();
            //win.ShowDialog();

            this.eventAggregator.GetEvent<AddFundCommandEvents>().Publish(new Tuple<CustomerType, ChangeFundType, string>(customerType, fundType, caseId));
        }

        public ICommand DeleteFundFlowCommand { get; set; }
        private void OnDeleteFundFlowCommand(string customer)
        {
            this.eventAggregator.GetEvent<DeleteFundFlowCommandEvents>().Publish(new Tuple<TransactionInfoCtrlViewModel, string>(this,customer));

        }



    }
}
