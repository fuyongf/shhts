﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.PubSubEvents;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels
{
    public class CaseInfoViewModel : BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;

        public CaseInfoViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;

        }
    }
}
