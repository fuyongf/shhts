﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.PubSubEvents;
using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPFNew.Common;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels
{
    public class FundFlowDialogViewModel : BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;

        public FundFlowDialogViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;

            this.EnterCommand = new DelegateCommand<FundFlowDialogViewModel>(this.OnEnterCommand);
            this.SaveCommand = new DelegateCommand<FundFlowDialogViewModel>(this.OnSaveCommand);
            this.TextChangedCommand = new DelegateCommand<FundFlowDialogViewModel>(this.OnTextChangedCommand);

        }

        public void Clear()
        {
            ConditionName1_in = new ObservableCollection<Tuple<string, IList<string>>>(conditionName_in);
            ConditionName1SelectedValue_in = ConditionName1_in[0].Item1;
            Conditions2Visibility_in = Visibility.Collapsed;
            HouseFund_in = string.Empty;
            Brokerage_in = string.Empty;
            Deposit_in = string.Empty;
            Taxes_in = string.Empty;
            DecorateCompensation_in = string.Empty;
            OtherCharges_in = string.Empty;
            IsTrusteeShip_in = false;
            IsLoan_in = false;
            IsSelfPay_In = false;

            ConditionName1_out = new ObservableCollection<Tuple<string, IList<string>>>(conditionName_out);
            ConditionName1SelectedValue_out = ConditionName1_out[0].Item1;
            Conditions2Visibility_out = Visibility.Collapsed;
            HouseFund_out = string.Empty;
            Brokerage_out = string.Empty;
            Deposit_out = string.Empty;
            Taxes_out = string.Empty;
            DecorateCompensation_out = string.Empty;
            OtherCharges_out = string.Empty;
            IsTrusteeShip_out = false;
            IsLoan_out = false;
            IsSelfPay_out = false;

        }

        private decimal GetAmount(string houseFund, string taxes, string brokerage, string decorateCompensation, string deposit, string otherCharges)
        {
            decimal _houseFund = 0;
            decimal.TryParse(houseFund.Trim(), out _houseFund);

            decimal _taxes = 0;
            decimal.TryParse(taxes.Trim(), out _taxes);

            decimal _brokerage = 0;
            decimal.TryParse(brokerage.Trim(), out _brokerage);

            decimal _decorateCompensation = 0;
            decimal.TryParse(decorateCompensation.Trim(), out _decorateCompensation);

            decimal _deposit = 0;
            decimal.TryParse(deposit.Trim(), out _deposit);

            decimal _otherCharges = 0;
            decimal.TryParse(otherCharges.Trim(), out _otherCharges);

            return _houseFund + _taxes + _brokerage + _decorateCompensation + _deposit + _otherCharges;

        }

        /// <summary>
        /// 窗口Title
        /// </summary>
        private string dialogTitle;
        public string DialogTitle
        {
            get
            {
                return dialogTitle;
            }

            set
            {
                if (dialogTitle != value)
                {
                    dialogTitle = value;
                    NotifyPropertyChanged("DialogTitle");
                }
            }
        }

        private string caseId;
        public string CaseId
        {
            get
            {
                return caseId;
            }

            set
            {
                if (caseId != value)
                {
                    caseId = value;
                    NotifyPropertyChanged("CaseId");
                }
            }
        }

        private CustomerType customerType;

        public CustomerType CurrentCustomerType
        {
            get
            {
                return customerType;
            }

            set
            {
                if (customerType != value)
                {
                    customerType = value;
                    NotifyPropertyChanged("CurrentCustomerType");
                }
            }
        }

        
        #region 收款
        private bool isTabSelected_in;
        public bool IsTabSelected_in
        {
            get
            {
                return isTabSelected_in;
            }

            set
            {
                if (isTabSelected_in != value)
                {
                    isTabSelected_in = value;
                    NotifyPropertyChanged("IsTabSelected_in");
                }
            }
        }


        /// <summary>
        /// 收款：条件一
        /// </summary>
        private ObservableCollection<Tuple<string, IList<string>>> conditionName_in = new ObservableCollection<Tuple<string, IList<string>>>();
        public ObservableCollection<Tuple<string, IList<string>>> ConditionName_in
        {
            get
            {
                return conditionName_in;
            }
            set
            {
                if (conditionName_in != value)
                {
                    conditionName_in = value;
                    NotifyPropertyChanged("ConditionName_in");
                }
            }
        }

        private ObservableCollection<Tuple<string, IList<string>>> conditionName1_in = new ObservableCollection<Tuple<string, IList<string>>>();
        public ObservableCollection<Tuple<string, IList<string>>> ConditionName1_in
        {
            get
            {
                return conditionName1_in;
            }
            set
            {
                if (conditionName1_in != value)
                {
                    conditionName1_in = value;
                    NotifyPropertyChanged("ConditionName1_in");
                }
            }
        }

        /// <summary>
        /// 收款：条件一
        /// </summary>
        private string conditionName1SelectedValue_in;
        public string ConditionName1SelectedValue_in
        {
            get
            {
                return conditionName1SelectedValue_in;
            }

            set
            {
                if (conditionName1SelectedValue_in != value)
                {
                    conditionName1SelectedValue_in = value;
                    NotifyPropertyChanged("ConditionName1SelectedValue_in");
                }
                if (conditionName1SelectedValue_in == "指定日期")
                {
                    Date1Visibility_in = Visibility.Visible;
                }
                else
                {
                    Date1Visibility_in = Visibility.Collapsed;
                }

                foreach (var x in conditionName_in)
                {
                    if (x.Item1 == conditionName1SelectedValue_in)
                    {
                        ConditionValue1_in.Clear();
                        foreach (var item in x.Item2)
                        {
                            ConditionValue1_in.Add(item);
                        }
                        ConditionValue1SelectedValue_in = conditionValue1_in[0];
                        if (conditions2Visibility_in == Visibility.Visible)
                        {
                            ConditionName2_in = new ObservableCollection<Tuple<string, IList<string>>>(conditionName_in);
                            ConditionName2_in.Remove(new Tuple<string, IList<string>>(x.Item1, x.Item2));
                        }
                    }
                }
            }
        }

        private int conditionName1SelectedIndex_in;
        public int ConditionName1SelectedIndex_in
        {
            get
            {
                return conditionName1SelectedIndex_in;
            }

            set
            {
                if (conditionName1SelectedIndex_in != value)
                {
                    conditionName1SelectedIndex_in = value;
                    NotifyPropertyChanged("ConditionName1SelectedIndex_in");
                }
            }
        }

        private Visibility conditions2Visibility_in = Visibility.Collapsed;
        public Visibility Conditions2Visibility_in
        {
            get
            {
                return conditions2Visibility_in;
            }

            set
            {
                if (conditions2Visibility_in != value)
                {
                    conditions2Visibility_in = value;
                    NotifyPropertyChanged("Conditions2Visibility_in");
                }

                foreach (var x in conditionName_in)
                {
                    if (x.Item1 == conditionName1SelectedValue_in)
                    {
                        if (conditions2Visibility_in == Visibility.Visible)
                        {
                            ConditionName2_in = new ObservableCollection<Tuple<string, IList<string>>>(conditionName_in);
                            ConditionName2_in.Remove(new Tuple<string, IList<string>>(x.Item1, x.Item2));
                            ConditionName2SelectedValue_in = ConditionName2_in[0].Item1;
                        }
                        else
                        {
                            ConditionName1_in = new ObservableCollection<Tuple<string, IList<string>>>(conditionName_in);
                            ConditionName1SelectedValue_in = x.Item1;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 收款：条件二
        /// </summary>
        private ObservableCollection<Tuple<string, IList<string>>> conditionName2_in = new ObservableCollection<Tuple<string, IList<string>>>();
        public ObservableCollection<Tuple<string, IList<string>>> ConditionName2_in
        {
            get
            {
                return conditionName2_in;
            }
            set
            {
                if (conditionName2_in != value)
                {
                    conditionName2_in = value;
                    NotifyPropertyChanged("ConditionName2_in");
                }
            }
        }

        /// <summary>
        /// 收款：条件二
        /// </summary>
        private string conditionName2SelectedValue_in;
        public string ConditionName2SelectedValue_in
        {
            get
            {
                return conditionName2SelectedValue_in;
            }

            set
            {
                if (conditionName2SelectedValue_in != value)
                {
                    conditionName2SelectedValue_in = value;
                    NotifyPropertyChanged("ConditionName2SelectedValue_in");
                }
                if (conditionName2SelectedValue_in == "指定日期")
                {
                    Date2Visibility_in = Visibility.Visible;
                }
                else
                {
                    Date2Visibility_in = Visibility.Collapsed;
                }

                foreach (var x in conditionName_in)
                {
                    if (x.Item1 == conditionName2SelectedValue_in)
                    {
                        ConditionValue2_in.Clear();
                        foreach (var item in x.Item2)
                        {
                            ConditionValue2_in.Add(item);
                        }
                        ConditionValue2SelectedValue_in = conditionValue2_in[0];

                        ConditionName1_in = new ObservableCollection<Tuple<string, IList<string>>>(conditionName_in);
                        ConditionName1_in.Remove(new Tuple<string, IList<string>>(x.Item1, x.Item2));
                    }
                }

            }
        }

        private int conditionName2SelectedIndex_in;
        public int ConditionName2SelectedIndex_in
        {
            get
            {
                return conditionName2SelectedIndex_in;
            }

            set
            {
                if (conditionName2SelectedIndex_in != value)
                {
                    conditionName2SelectedIndex_in = value;
                    NotifyPropertyChanged("ConditionName2SelectedIndex_in");
                }
            }
        }

        /// <summary>
        /// 收款：条件值一
        /// </summary>
        private ObservableCollection<string> conditionValue1_in = new ObservableCollection<string>();
        public ObservableCollection<string> ConditionValue1_in
        {
            get
            {
                return conditionValue1_in;
            }
            set
            {
                if (conditionValue1_in != value)
                {
                    conditionValue1_in = value;
                    NotifyPropertyChanged("ConditionValue1_in");
                }
            }
        }

        /// <summary>
        /// 收款：条件值一
        /// </summary>
        private string conditionValue1SelectedValue_in;
        public string ConditionValue1SelectedValue_in
        {
            get
            {
                return conditionValue1SelectedValue_in;
            }

            set
            {
                if (conditionValue1SelectedValue_in != value)
                {
                    conditionValue1SelectedValue_in = value;
                    NotifyPropertyChanged("ConditionValue1SelectedValue_in");
                }
                if (conditionValue1SelectedValue_in == "当日" || conditionValue1SelectedValue_in == "之前")
                {
                    Day1Visibility_in = Visibility.Collapsed;
                }
                else
                {
                    Day1Visibility_in = Visibility.Visible;
                }
            }
        }
        private int conditionValue1SelectedIndex_in;
        public int ConditionValue1SelectedIndex_in
        {
            get
            {
                return conditionValue1SelectedIndex_in;
            }

            set
            {
                if (conditionValue1SelectedIndex_in != value)
                {
                    conditionValue1SelectedIndex_in = value;
                    NotifyPropertyChanged("ConditionValue1SelectedIndex_in");
                }
            }
        }

        /// <summary>
        /// 收款：条件值二
        /// </summary>
        private ObservableCollection<string> conditionValue2_in = new ObservableCollection<string>();
        public ObservableCollection<string> ConditionValue2_in
        {
            get
            {
                return conditionValue2_in;
            }
            set
            {
                if (conditionValue2_in != value)
                {
                    conditionValue2_in = value;
                    NotifyPropertyChanged("ConditionValue2_in");
                }
            }
        }

        /// <summary>
        /// 收款：条件值二
        /// </summary>
        private string conditionValue2SelectedValue_in;
        public string ConditionValue2SelectedValue_in
        {
            get
            {
                return conditionValue2SelectedValue_in;
            }

            set
            {
                if (conditionValue2SelectedValue_in != value)
                {
                    conditionValue2SelectedValue_in = value;
                    NotifyPropertyChanged("ConditionValue2SelectedValue_in");
                }
                if (conditionValue2SelectedValue_in == "当日" || conditionValue2SelectedValue_in == "之前")
                {
                    Day2Visibility_in = Visibility.Collapsed;
                }
                else
                {
                    Day2Visibility_in = Visibility.Visible;
                }
            }
        }
        private int conditionValue2SelectedIndex_in;
        public int ConditionValue2SelectedIndex_in
        {
            get
            {
                return conditionValue2SelectedIndex_in;
            }

            set
            {
                if (conditionValue2SelectedIndex_in != value)
                {
                    conditionValue2SelectedIndex_in = value;
                    NotifyPropertyChanged("ConditionValue2SelectedIndex_in");
                }
            }
        }
        /// <summary>
        /// 收款：n日
        /// </summary>
        private string days1_in = string.Empty;
        public string Days1_in
        {
            get
            {
                return days1_in;
            }

            set
            {
                if (days1_in != value)
                {
                    days1_in = value;
                    NotifyPropertyChanged("Days1_in");
                }
            }
        }

        private Visibility day1Visibility_in = Visibility.Collapsed;

        public Visibility Day1Visibility_in
        {
            get
            {
                return day1Visibility_in;
            }

            set
            {
                if (day1Visibility_in != value)
                {
                    day1Visibility_in = value;
                    NotifyPropertyChanged("Day1Visibility_in");
                }
            }
        }

        private string days2_in = string.Empty;
        public string Days2_in
        {
            get
            {
                return days2_in;
            }

            set
            {
                if (days2_in != value)
                {
                    days2_in = value;
                    NotifyPropertyChanged("Days2_in");
                }
            }
        }

        private Visibility day2Visibility_in = Visibility.Collapsed;

        public Visibility Day2Visibility_in
        {
            get
            {
                return day2Visibility_in;
            }

            set
            {
                if (day2Visibility_in != value)
                {
                    day2Visibility_in = value;
                    NotifyPropertyChanged("Day2Visibility_in");
                }
            }
        }

        
        /// <summary>
        /// 收款：指定日期
        /// </summary>
        private DateTime? date1_in ;
        public DateTime? Date1_in
        {
            get
            {
                return date1_in;
            }

            set
            {
                if (date1_in != value)
                {
                    date1_in = value;
                    NotifyPropertyChanged("Date1_in");
                }
            }
        }

        private Visibility date1Visibility_in = Visibility.Collapsed;
        public Visibility Date1Visibility_in
        {
            get
            {
                return date1Visibility_in;
            }

            set
            {
                if (date1Visibility_in != value)
                {
                    date1Visibility_in = value;
                    NotifyPropertyChanged("Date1Visibility_in");
                }
            }
        }

        private DateTime? date2_in;
        public DateTime? Date2_in
        {
            get
            {
                return date2_in;
            }

            set
            {
                if (date2_in != value)
                {
                    date2_in = value;
                    NotifyPropertyChanged("Date2_in");
                }
            }
        }

        private Visibility date2Visibility_in = Visibility.Collapsed;
        public Visibility Date2Visibility_in
        {
            get
            {
                return date2Visibility_in;
            }

            set
            {
                if (date2Visibility_in != value)
                {
                    date2Visibility_in = value;
                    NotifyPropertyChanged("Date2Visibility_in");
                }
            }
        }

        /// <summary>
        /// 收款：款项编号
        /// </summary>
        private string sysNo1_in;
        public string SysNo1_in
        {
            get
            {
                return sysNo1_in;
            }

            set
            {
                if (sysNo1_in != value)
                {
                    sysNo1_in = value;
                    NotifyPropertyChanged("SysNo1_in");
                }
            }
        }

        /// <summary>
        /// 收款：房款
        /// </summary>
        private string houseFund_in = string.Empty;
        public string HouseFund_in
        {
            get
            {
                return houseFund_in;
            }

            set
            {
                if (houseFund_in != value)
                {
                    houseFund_in = value;
                    NotifyPropertyChanged("HouseFund_in");
                }
                Amount_in = GetAmount(houseFund_in, taxes_in, brokerage_in, decorateCompensation_in, deposit_in, otherCharges_in);

            }
        }


        /// <summary>
        /// 收款：税费
        /// </summary>
        private string taxes_in = string.Empty;
        public string Taxes_in
        {
            get
            {
                return taxes_in;
            }

            set
            {
                if (taxes_in != value)
                {
                    taxes_in = value;
                    NotifyPropertyChanged("Taxes_in");
                }
                Amount_in = GetAmount(houseFund_in, taxes_in, brokerage_in, decorateCompensation_in, deposit_in, otherCharges_in);

            }
        }

        /// <summary>
        /// 收款：中介佣金
        /// </summary>
        private string brokerage_in = string.Empty;
        public string Brokerage_in
        {
            get
            {
                return brokerage_in;
            }

            set
            {
                if (brokerage_in != value)
                {
                    brokerage_in = value;
                    NotifyPropertyChanged("Brokerage_in");
                }
                Amount_in = GetAmount(houseFund_in, taxes_in, brokerage_in, decorateCompensation_in, deposit_in, otherCharges_in);

            }
        }

        /// <summary>
        /// 收款：装修补偿
        /// </summary>
        private string decorateCompensation_in = string.Empty;
        public string DecorateCompensation_in
        {
            get
            {
                return decorateCompensation_in;
            }

            set
            {
                if (decorateCompensation_in != value)
                {
                    decorateCompensation_in = value;
                    NotifyPropertyChanged("DecorateCompensation_in");
                }
                Amount_in = GetAmount(houseFund_in, taxes_in, brokerage_in, decorateCompensation_in, deposit_in, otherCharges_in);

            }
        }

        /// <summary>
        /// 收款：定金
        /// </summary>
        private string deposit_in = string.Empty;
        public string Deposit_in
        {
            get
            {
                return deposit_in;
            }

            set
            {
                if (deposit_in != value)
                {
                    deposit_in = value;
                    NotifyPropertyChanged("Deposit_in");
                }
                Amount_in = GetAmount(houseFund_in, taxes_in, brokerage_in, decorateCompensation_in, deposit_in, otherCharges_in);

            }
        }

        /// <summary>
        /// 收款：定金是否可填
        /// </summary>
        private Visibility depositVisibility_in;
        public Visibility DepositVisibility_In
        {
            get
            {
                return depositVisibility_in;
            }

            set
            {
                if (depositVisibility_in != value)
                {
                    depositVisibility_in = value;
                    NotifyPropertyChanged("DepositVisibility_in");
                }
            }
        }

        /// <summary>
        /// 收款：其他费用
        /// </summary>
        private string otherCharges_in = string.Empty;
        public string OtherCharges_in
        {
            get
            {
                return otherCharges_in;
            }

            set
            {
                if (otherCharges_in != value)
                {
                    otherCharges_in = value;
                    NotifyPropertyChanged("OtherCharges_in");
                }
                Amount_in = GetAmount(houseFund_in, taxes_in, brokerage_in, decorateCompensation_in, deposit_in, otherCharges_in);

            }
        }

        /// <summary>
        /// 收款：本次合计
        /// </summary>
        private decimal amount_in = 0;
        public decimal Amount_in
        {
            get
            {
                return amount_in;
            }

            set
            {
                if (amount_in != value)
                {
                    amount_in = value;
                    NotifyPropertyChanged("Amount_in");
                }
            }
        }

        /// <summary>
        /// 收款：是否平安托管
        /// </summary>
        private bool isTrusteeShip_in;
        public bool IsTrusteeShip_in
        {
            get
            {
                return isTrusteeShip_in;
            }

            set
            {
                if (isTrusteeShip_in != value)
                {
                    isTrusteeShip_in = value;
                    NotifyPropertyChanged("IsTrusteeShip_in");
                }
            }
        }

        /// <summary>
        /// 收款：是否银行贷款
        /// </summary>
        private bool isLoan_in;
        public bool IsLoan_in
        {
            get
            {
                return isLoan_in;
            }

            set
            {
                if (isLoan_in != value)
                {
                    isLoan_in = value;
                    NotifyPropertyChanged("IsLoan_in");
                }
            }
        }

        /// <summary>
        /// 是否直接支付
        /// </summary>
        private bool isSelfPay_in;
        public bool IsSelfPay_In
        {
            get
            {
                return isSelfPay_in;
            }

            set
            {
                if (isSelfPay_in != value)
                {
                    isSelfPay_in = value;
                    NotifyPropertyChanged("IsSelfPay_In");
                }
            }
        }


        #endregion

        #region 出款
        private bool isTabSelected_out;
        public bool IsTabSelected_out
        {
            get
            {
                return isTabSelected_out;
            }

            set
            {
                if (isTabSelected_out != value)
                {
                    isTabSelected_out = value;
                    NotifyPropertyChanged("IsTabSelected_out");
                }
            }
        }


        /// <summary>
        /// 出款：条件一
        /// </summary>
        private ObservableCollection<Tuple<string, IList<string>>> conditionName_out = new ObservableCollection<Tuple<string, IList<string>>>();
        public ObservableCollection<Tuple<string, IList<string>>> ConditionName_out
        {
            get
            {
                return conditionName_out;
            }
            set
            {
                if (conditionName_out != value)
                {
                    conditionName_out = value;
                    NotifyPropertyChanged("ConditionName_out");
                }
            }
        }

        private ObservableCollection<Tuple<string, IList<string>>> conditionName1_out = new ObservableCollection<Tuple<string, IList<string>>>();
        public ObservableCollection<Tuple<string, IList<string>>> ConditionName1_out
        {
            get
            {
                return conditionName1_out;
            }
            set
            {
                if (conditionName1_out != value)
                {
                    conditionName1_out = value;
                    NotifyPropertyChanged("ConditionName1_out");
                }
            }
        }

        /// <summary>
        /// 出款：条件一
        /// </summary>
        private string conditionName1SelectedValue_out;
        public string ConditionName1SelectedValue_out
        {
            get
            {
                return conditionName1SelectedValue_out;
            }

            set
            {
                if (conditionName1SelectedValue_out != value)
                {
                    conditionName1SelectedValue_out = value;
                    NotifyPropertyChanged("ConditionName1SelectedValue_out");
                }
                if (conditionName1SelectedValue_out == "指定日期")
                {
                    Date1Visibility_out = Visibility.Visible;
                }
                else
                {
                    Date1Visibility_out = Visibility.Collapsed;
                }
                if (conditionName1SelectedValue_out == "收到编号为")
                {
                    SysNo1Visibility_out = Visibility.Visible;
                }
                else
                {
                    SysNo1Visibility_out = Visibility.Collapsed;
                }

                foreach (var x in conditionName_out)
                {
                    if (x.Item1 == conditionName1SelectedValue_out)
                    {
                        ConditionValue1_out.Clear();
                        foreach (var item in x.Item2)
                        {
                            ConditionValue1_out.Add(item);
                        }
                        ConditionValue1SelectedValue_out = conditionValue1_out[0];
                        if (conditions2Visibility_out == Visibility.Visible)
                        {
                            ConditionName2_out = new ObservableCollection<Tuple<string, IList<string>>>(conditionName_out);
                            ConditionName2_out.Remove(new Tuple<string, IList<string>>(x.Item1, x.Item2));
                        }
                    }
                }
            }
        }

        private int conditionName1SelectedIndex_out;
        public int ConditionName1SelectedIndex_out
        {
            get
            {
                return conditionName1SelectedIndex_out;
            }

            set
            {
                if (conditionName1SelectedIndex_out != value)
                {
                    conditionName1SelectedIndex_out = value;
                    NotifyPropertyChanged("ConditionName1SelectedIndex_out");
                }
            }
        }

        private Visibility conditions2Visibility_out = Visibility.Collapsed;
        public Visibility Conditions2Visibility_out
        {
            get
            {
                return conditions2Visibility_out;
            }

            set
            {
                if (conditions2Visibility_out != value)
                {
                    conditions2Visibility_out = value;
                    NotifyPropertyChanged("Conditions2Visibility_out");
                }

                foreach (var x in conditionName_out)
                {
                    if (x.Item1 == conditionName1SelectedValue_out)
                    {
                        if (conditions2Visibility_out == Visibility.Visible)
                        {
                            ConditionName2_out = new ObservableCollection<Tuple<string, IList<string>>>(conditionName_out);
                            ConditionName2_out.Remove(new Tuple<string, IList<string>>(x.Item1, x.Item2));
                            ConditionName2SelectedValue_out = ConditionName2_out[0].Item1;
                        }
                        else
                        {
                            ConditionName1_out = new ObservableCollection<Tuple<string, IList<string>>>(conditionName_out);
                            ConditionName1SelectedValue_out = x.Item1;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 出款：条件二
        /// </summary>
        private ObservableCollection<Tuple<string, IList<string>>> conditionName2_out = new ObservableCollection<Tuple<string, IList<string>>>();
        public ObservableCollection<Tuple<string, IList<string>>> ConditionName2_out
        {
            get
            {
                return conditionName2_out;
            }
            set
            {
                if (conditionName2_out != value)
                {
                    conditionName2_out = value;
                    NotifyPropertyChanged("ConditionName2_out");
                }
            }
        }

        /// <summary>
        /// 出款：条件二
        /// </summary>
        private string conditionName2SelectedValue_out;
        public string ConditionName2SelectedValue_out
        {
            get
            {
                return conditionName2SelectedValue_out;
            }

            set
            {
                if (conditionName2SelectedValue_out != value)
                {
                    conditionName2SelectedValue_out = value;
                    NotifyPropertyChanged("ConditionName2SelectedValue_out");
                }
                if (conditionName2SelectedValue_out == "指定日期")
                {
                    Date2Visibility_out = Visibility.Visible;
                }
                else
                {
                    Date2Visibility_out = Visibility.Collapsed;
                }
                if (conditionName2SelectedValue_out == "收到编号为")
                {
                    SysNo2Visibility_out = Visibility.Visible;
                }
                else
                {
                    SysNo2Visibility_out = Visibility.Collapsed;
                }
                foreach (var x in conditionName_out)
                {
                    if (x.Item1 == conditionName2SelectedValue_out)
                    {
                        ConditionValue2_out.Clear();
                        foreach (var item in x.Item2)
                        {
                            ConditionValue2_out.Add(item);
                        }
                        ConditionValue2SelectedValue_out = conditionValue2_out[0];

                        ConditionName1_out = new ObservableCollection<Tuple<string, IList<string>>>(conditionName_out);
                        ConditionName1_out.Remove(new Tuple<string, IList<string>>(x.Item1, x.Item2));
                    }
                }

            }
        }

        private int conditionName2SelectedIndex_out;
        public int ConditionName2SelectedIndex_out
        {
            get
            {
                return conditionName2SelectedIndex_out;
            }

            set
            {
                if (conditionName2SelectedIndex_out != value)
                {
                    conditionName2SelectedIndex_out = value;
                    NotifyPropertyChanged("ConditionName2SelectedIndex_out");
                }
            }
        }

        /// <summary>
        /// 出款：条件值一
        /// </summary>
        private ObservableCollection<string> conditionValue1_out = new ObservableCollection<string>();
        public ObservableCollection<string> ConditionValue1_out
        {
            get
            {
                return conditionValue1_out;
            }
            set
            {
                if (conditionValue1_out != value)
                {
                    conditionValue1_out = value;
                    NotifyPropertyChanged("ConditionValue1_out");
                }
            }
        }

        /// <summary>
        /// 出款：条件值一
        /// </summary>
        private string conditionValue1SelectedValue_out;
        public string ConditionValue1SelectedValue_out
        {
            get
            {
                return conditionValue1SelectedValue_out;
            }

            set
            {
                if (conditionValue1SelectedValue_out != value)
                {
                    conditionValue1SelectedValue_out = value;
                    NotifyPropertyChanged("ConditionValue1SelectedValue_out");
                }
                if (conditionValue1SelectedValue_out == "当日" || conditionValue1SelectedValue_out == "之前")
                {
                    Day1Visibility_out = Visibility.Collapsed;
                }
                else
                {
                    Day1Visibility_out = Visibility.Visible;
                }
            }
        }
        private int conditionValue1SelectedIndex_out;
        public int ConditionValue1SelectedIndex_out
        {
            get
            {
                return conditionValue1SelectedIndex_out;
            }

            set
            {
                if (conditionValue1SelectedIndex_out != value)
                {
                    conditionValue1SelectedIndex_out = value;
                    NotifyPropertyChanged("ConditionValue1SelectedIndex_out");
                }
            }
        }

        /// <summary>
        /// 出款：条件值二
        /// </summary>
        private ObservableCollection<string> conditionValue2_out = new ObservableCollection<string>();
        public ObservableCollection<string> ConditionValue2_out
        {
            get
            {
                return conditionValue2_out;
            }
            set
            {
                if (conditionValue2_out != value)
                {
                    conditionValue2_out = value;
                    NotifyPropertyChanged("ConditionValue2_out");
                }
            }
        }

        /// <summary>
        /// 出款：条件值二
        /// </summary>
        private string conditionValue2SelectedValue_out;
        public string ConditionValue2SelectedValue_out
        {
            get
            {
                return conditionValue2SelectedValue_out;
            }

            set
            {
                if (conditionValue2SelectedValue_out != value)
                {
                    conditionValue2SelectedValue_out = value;
                    NotifyPropertyChanged("ConditionValue2SelectedValue_out");
                }
                if (conditionValue2SelectedValue_out == "当日" || conditionValue2SelectedValue_out == "之前")
                {
                    Day2Visibility_out = Visibility.Collapsed;
                }
                else
                {
                    Day2Visibility_out = Visibility.Visible;
                }
            }
        }
        private int conditionValue2SelectedIndex_out;
        public int ConditionValue2SelectedIndex_out
        {
            get
            {
                return conditionValue2SelectedIndex_out;
            }

            set
            {
                if (conditionValue2SelectedIndex_out != value)
                {
                    conditionValue2SelectedIndex_out = value;
                    NotifyPropertyChanged("ConditionValue2SelectedIndex_out");
                }
            }
        }
        /// <summary>
        /// 出款：n日
        /// </summary>
        private string days1_out = string.Empty;
        public string Days1_out
        {
            get
            {
                return days1_out;
            }

            set
            {
                if (days1_out != value)
                {
                    days1_out = value;
                    NotifyPropertyChanged("Days1_out");
                }
            }
        }

        private Visibility day1Visibility_out = Visibility.Collapsed;

        public Visibility Day1Visibility_out
        {
            get
            {
                return day1Visibility_out;
            }

            set
            {
                if (day1Visibility_out != value)
                {
                    day1Visibility_out = value;
                    NotifyPropertyChanged("Day1Visibility_out");
                }
            }
        }

        private string days2_out = string.Empty;
        public string Days2_out
        {
            get
            {
                return days2_out;
            }

            set
            {
                if (days2_out != value)
                {
                    days2_out = value;
                    NotifyPropertyChanged("Days2_out");
                }
            }
        }

        private Visibility day2Visibility_out = Visibility.Collapsed;

        public Visibility Day2Visibility_out
        {
            get
            {
                return day2Visibility_out;
            }

            set
            {
                if (day2Visibility_out != value)
                {
                    day2Visibility_out = value;
                    NotifyPropertyChanged("Day2Visibility_out");
                }
            }
        }

        /// <summary>
        /// 出款：款项编号
        /// </summary>
        private string sysNo1_out = string.Empty;
        public string SysNo1_out
        {
            get
            {
                return sysNo1_out;
            }

            set
            {
                if (sysNo1_out != value)
                {
                    sysNo1_out = value;
                    NotifyPropertyChanged("SysNo1_out");
                }
            }
        }

        private Visibility sysNo1Visibility_out = Visibility.Collapsed;

        public Visibility SysNo1Visibility_out
        {
            get
            {
                return sysNo1Visibility_out;
            }

            set
            {
                if (sysNo1Visibility_out != value)
                {
                    sysNo1Visibility_out = value;
                    NotifyPropertyChanged("SysNo1Visibility_out");
                }
            }
        }

        private string sysNo2_out = string.Empty;
        public string SysNo2_out
        {
            get
            {
                return sysNo2_out;
            }

            set
            {
                if (sysNo2_out != value)
                {
                    sysNo2_out = value;
                    NotifyPropertyChanged("SysNo2_out");
                }
            }
        }

        private Visibility sysNo2Visibility_out = Visibility.Collapsed;

        public Visibility SysNo2Visibility_out
        {
            get
            {
                return sysNo2Visibility_out;
            }

            set
            {
                if (sysNo2Visibility_out != value)
                {
                    sysNo2Visibility_out = value;
                    NotifyPropertyChanged("SysNo2Visibility_out");
                }
            }
        }

        /// <summary>
        /// 出款：指定日期
        /// </summary>
        private DateTime? date1_out;
        public DateTime? Date1_out
        {
            get
            {
                return date1_out;
            }

            set
            {
                if (date1_out != value)
                {
                    date1_out = value;
                    NotifyPropertyChanged("Date1_out");
                }
            }
        }

        private Visibility date1Visibility_out = Visibility.Collapsed;
        public Visibility Date1Visibility_out
        {
            get
            {
                return date1Visibility_out;
            }

            set
            {
                if (date1Visibility_out != value)
                {
                    date1Visibility_out = value;
                    NotifyPropertyChanged("Date1Visibility_out");
                }
            }
        }

        private DateTime? date2_out;
        public DateTime? Date2_out
        {
            get
            {
                return date2_out;
            }

            set
            {
                if (date2_out != value)
                {
                    date2_out = value;
                    NotifyPropertyChanged("Date2_out");
                }
            }
        }

        private Visibility date2Visibility_out = Visibility.Collapsed;
        public Visibility Date2Visibility_out
        {
            get
            {
                return date2Visibility_out;
            }

            set
            {
                if (date2Visibility_out != value)
                {
                    date2Visibility_out = value;
                    NotifyPropertyChanged("Date2Visibility_out");
                }
            }
        }

        /// <summary>
        /// 出款：房款
        /// </summary>
        private string houseFund_out = string.Empty;
        public string HouseFund_out
        {
            get
            {
                return houseFund_out;
            }

            set
            {
                if (houseFund_out != value)
                {
                    houseFund_out = value;
                    NotifyPropertyChanged("HouseFund_out");
                }
                Amount_out = GetAmount(houseFund_out, taxes_out, brokerage_out, decorateCompensation_out, deposit_out, otherCharges_out);

            }
        }


        /// <summary>
        /// 出款：税费
        /// </summary>
        private string taxes_out = string.Empty;
        public string Taxes_out
        {
            get
            {
                return taxes_out;
            }

            set
            {
                if (taxes_out != value)
                {
                    taxes_out = value;
                    NotifyPropertyChanged("Taxes_out");
                }
                Amount_out = GetAmount(houseFund_out, taxes_out, brokerage_out, decorateCompensation_out, deposit_out, otherCharges_out);

            }
        }

        /// <summary>
        /// 出款：中介佣金
        /// </summary>
        private string brokerage_out = string.Empty;
        public string Brokerage_out
        {
            get
            {
                return brokerage_out;
            }

            set
            {
                if (brokerage_out != value)
                {
                    brokerage_out = value;
                    NotifyPropertyChanged("Brokerage_out");
                }
                Amount_out = GetAmount(houseFund_out, taxes_out, brokerage_out, decorateCompensation_out, deposit_out, otherCharges_out);

            }
        }

        /// <summary>
        /// 出款：装修补偿
        /// </summary>
        private string decorateCompensation_out = string.Empty;
        public string DecorateCompensation_out
        {
            get
            {
                return decorateCompensation_out;
            }

            set
            {
                if (decorateCompensation_out != value)
                {
                    decorateCompensation_out = value;
                    NotifyPropertyChanged("DecorateCompensation_out");
                }
                Amount_out = GetAmount(houseFund_out, taxes_out, brokerage_out, decorateCompensation_out, deposit_out, otherCharges_out);

            }
        }

        /// <summary>
        /// 出款：定金
        /// </summary>
        private string deposit_out = string.Empty;
        public string Deposit_out
        {
            get
            {
                return deposit_out;
            }

            set
            {
                if (deposit_out != value)
                {
                    deposit_out = value;
                    NotifyPropertyChanged("Deposit_out");
                }
                Amount_out = GetAmount(houseFund_out, taxes_out, brokerage_out, decorateCompensation_out, deposit_out, otherCharges_out);

            }
        }

        /// <summary>
        /// 出款：定金是否可填
        /// </summary>
        private Visibility depositVisibility_out;
        public Visibility DepositVisibility_out
        {
            get
            {
                return depositVisibility_out;
            }

            set
            {
                if (depositVisibility_out != value)
                {
                    depositVisibility_out = value;
                    NotifyPropertyChanged("DepositVisibility_out");
                }
            }
        }

        /// <summary>
        /// 出款：其他费用
        /// </summary>
        private string otherCharges_out = string.Empty;
        public string OtherCharges_out
        {
            get
            {
                return otherCharges_out;
            }

            set
            {
                if (otherCharges_out != value)
                {
                    otherCharges_out = value;
                    NotifyPropertyChanged("OtherCharges_out");
                }
                Amount_out = GetAmount(houseFund_out, taxes_out, brokerage_out, decorateCompensation_out, deposit_out, otherCharges_out);

            }
        }

        /// <summary>
        /// 出款：本次合计
        /// </summary>
        private decimal amount_out = 0;
        public decimal Amount_out
        {
            get
            {
                return amount_out;
            }

            set
            {
                if (amount_out != value)
                {
                    amount_out = value;
                    NotifyPropertyChanged("Amount_out");
                }
            }
        }

        /// <summary>
        /// 出款：是否平安托管
        /// </summary>
        private bool isTrusteeShip_out;
        public bool IsTrusteeShip_out
        {
            get
            {
                return isTrusteeShip_out;
            }

            set
            {
                if (isTrusteeShip_out != value)
                {
                    isTrusteeShip_out = value;
                    NotifyPropertyChanged("IsTrusteeShip_out");
                }
            }
        }

        /// <summary>
        /// 出款：是否银行贷款
        /// </summary>
        private bool isLoan_out;
        public bool IsLoan_out
        {
            get
            {
                return isLoan_out;
            }

            set
            {
                if (isLoan_out != value)
                {
                    isLoan_out = value;
                    NotifyPropertyChanged("IsLoan_out");
                }
            }
        }

        /// <summary>
        /// 是否直接支付
        /// </summary>
        private bool isSelfPay_out;
        public bool IsSelfPay_out
        {
            get
            {
                return isSelfPay_out;
            }

            set
            {
                if (isSelfPay_out != value)
                {
                    isSelfPay_out = value;
                    NotifyPropertyChanged("IsSelfPay_out");
                }
            }
        }


        #endregion

        public ICommand EnterCommand { get; private set; }

        private void OnEnterCommand(FundFlowDialogViewModel viewModel)
        {
            this.eventAggregator.GetEvent<FundFlowDialogEnterCommandEvents>().Publish(this);
        }

        public ICommand SaveCommand { get; private set; }
        private void OnSaveCommand(FundFlowDialogViewModel viewModel)
        {
            this.eventAggregator.GetEvent<FundFlowDialogSaveCommandEvents>().Publish(this);
        }

        public ICommand TextChangedCommand { get; private set; }
        private void OnTextChangedCommand(FundFlowDialogViewModel viewModel)
        {
            this.eventAggregator.GetEvent<FundFlowDialogTextChangedCommandEvents>().Publish(this);
        }


    }
}
