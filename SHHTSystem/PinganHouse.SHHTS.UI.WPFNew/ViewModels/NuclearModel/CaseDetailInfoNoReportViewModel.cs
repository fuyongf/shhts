﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.PubSubEvents;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using PinganHouse.SHHTS.UI.WPFNew.Controls;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels
{
    public class CaseDetailInfoNoReportViewModel : BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;

        public CaseDetailInfoNoReportViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;

            RightsMateriaList = new ObservableCollection<MateriaViewModel>();
            TranscationList = new ObservableCollection<MateriaViewModel>();
            ContractList = new ObservableCollection<MateriaViewModel>();
            MoneyList = new ObservableCollection<MateriaViewModel>();
            BuyerList = new ObservableCollection<CustomerAndMateriaListViewModel>();
            SellerList = new ObservableCollection<CustomerAndMateriaListViewModel>();

            this.NuclearCommand = new DelegateCommand<object>(this.OnNuclear);

        }

        public ICommand NuclearCommand { get; private set; }
        private void OnNuclear(object model)
        {
            this.eventAggregator.GetEvent<NuclearEvents>().Publish(Microsoft.Practices.ServiceLocation.ServiceLocator.Current.GetInstance<CheckCaseSearchPageViewModel>());
        }

        private string caseId;
        public string CaseId
        {
            get
            {
                return caseId;
            }
            set
            {
                if (caseId != value)
                {
                    caseId = value;
                    NotifyPropertyChanged("CaseId");
                }
            }
        }

        private string tbTenementContractWord;
        public string TbTenementContractWord
        {
            get { return tbTenementContractWord; }
            set
            {
                if (tbTenementContractWord != value)
                {
                    tbTenementContractWord = value;
                    NotifyPropertyChanged("TbTenementContractWord");
                }
            }
        }

        private string tbTenementContract;
        public string TbTenementContract
        {
            get { return tbTenementContract; }
            set
            {
                if (tbTenementContract != value)
                {
                    tbTenementContract = value;
                    NotifyPropertyChanged("TbTenementContract");
                }
            }
        }


        private string tbTenementAddress;

        public string TbTenementAddress
        {
            get { return tbTenementAddress; }
            set
            {
                if (tbTenementAddress != value)
                {
                    tbTenementAddress = value;
                    NotifyPropertyChanged("TbTenementAddress");
                }
            }
        }
        
        private string tblShowInfo;

        public string TblShowInfo
        {
            get { return tblShowInfo; }
            set
            {
                if (tblShowInfo != value)
                {
                    tblShowInfo = value;
                    NotifyPropertyChanged("TblShowInfo");
                }
            }
        }

        /// <summary>
        /// 已签约
        /// </summary>
        private bool hA2Ready;
        public bool HA2Ready
        {
            get
            {
                return hA2Ready;
            }
            set
            {
                if (hA2Ready != value)
                {
                    hA2Ready = value;
                    NotifyPropertyChanged("HA2Ready");
                }
            }
        }

        /// <summary>
        /// 签约时间
        /// </summary>
        private string hA1_HA2Time;
        public string HA1_HA2Time
        {
            get
            {
                return hA1_HA2Time;
            }
            set
            {
                if (hA1_HA2Time != value)
                {
                    hA1_HA2Time = value;
                    NotifyPropertyChanged("HA1_HA2Time");
                }
            }
        }

        /// <summary>
        /// 填写立案报告
        /// </summary>
        private bool hA3Ready;
        public bool HA3Ready
        {
            get
            {
                return hA3Ready;
            }
            set
            {
                if (hA3Ready != value)
                {
                    hA3Ready = value;
                    NotifyPropertyChanged("HA3Ready");
                }
            }
        }

        /// <summary>
        /// 填写立案报告时间
        /// </summary>
        private string hA2_HA3Time;
        public string HA2_HA3Time
        {
            get
            {
                return hA2_HA3Time;
            }
            set
            {
                if (hA2_HA3Time != value)
                {
                    hA2_HA3Time = value;
                    NotifyPropertyChanged("HA2_HA3Time");
                }
            }
        }

        /// <summary>
        /// 提交立案报告
        /// </summary>
        private bool hA4Ready;
        public bool HA4Ready
        {
            get
            {
                return hA4Ready;
            }
            set
            {
                if (hA4Ready != value)
                {
                    hA4Ready = value;
                    NotifyPropertyChanged("HA4Ready");
                }
            }
        }

        /// <summary>
        /// 提交立案报告时间
        /// </summary>
        private string hA3_HA4Time;
        public string HA3_HA4Time
        {
            get
            {
                return hA3_HA4Time;
            }
            set
            {
                if (hA3_HA4Time != value)
                {
                    hA3_HA4Time = value;
                    NotifyPropertyChanged("HA3_HA4Time");
                }
            }
        }

        /// <summary>
        /// 审核通过完成
        /// </summary>
        private bool hA5Ready;
        public bool HA5Ready
        {
            get
            {
                return hA5Ready;
            }
            set
            {
                if (hA5Ready != value)
                {
                    hA5Ready = value;
                    NotifyPropertyChanged("HA5Ready");
                }
            }
        }


        /// <summary>
        /// 证件类型
        /// </summary>
        private List<EnumHelper> identityBuyerList;

        public List<EnumHelper> IdentityBuyerList
        {
            get { return identityBuyerList; }
            set
            {
                if (identityBuyerList != value)
                {
                    identityBuyerList = value;
                    NotifyPropertyChanged("IdentityBuyerList");
                }
            }
        }

        private bool isBuyerForbid = true;

        public bool IsBuyerForbid
        {
            get { return isBuyerForbid; }
            set
            {
                if (isBuyerForbid != value)
                {
                    isBuyerForbid = value;
                    NotifyPropertyChanged("IsBuyerForbid");
                }
            }
        }

        /// <summary>
        ///选择证件类型
        /// </summary>
        private EnumHelper identityBuyerSelected;

        public EnumHelper IdentityBuyerSelected
        {
            get { return identityBuyerSelected; }
            set
            {
                if (identityBuyerSelected != value)
                {
                    identityBuyerSelected = value;
                    NotifyPropertyChanged("IdentityBuyerSelected");
                }
            }
        }

        /// <summary>
        ///选择证件类型Index
        /// </summary>
        private int indexBuyerSelect;

        public int IndexBuyerSelect
        {
            get { return indexBuyerSelect; }
            set
            {
                if (indexBuyerSelect != value)
                {
                    indexBuyerSelect = value;
                    NotifyPropertyChanged("IndexBuyerSelect");
                }
            }
        }

        /// <summary>
        /// 类型详情Model
        /// </summary>
        private IdInfoModel idCardBuyerModel;

        public IdInfoModel IdCardBuyerModel
        {
            get { return idCardBuyerModel; }
            set
            {
                if (idCardBuyerModel != value)
                {
                    idCardBuyerModel = value;
                    NotifyPropertyChanged("IdCardBuyerModel");
                }
            }
        }

        private ObservableCollection<IdInfoModel> buyerInfoList;
        public ObservableCollection<IdInfoModel> BuyerInfoList
        {
            get
            {
                return buyerInfoList;
            }
            set
            {
                if (buyerInfoList != value)
                {
                    buyerInfoList = value;
                    NotifyPropertyChanged("BuyerInfoList");
                }
            }
        }

        private IdInfoModel buySelectCurrentItem;
        public IdInfoModel BuySelectCurrentItem
        {
            get
            {
                return buySelectCurrentItem;
            }
            set
            {
                if (buySelectCurrentItem != value)
                {
                    buySelectCurrentItem = value;
                    NotifyPropertyChanged("BuySelectCurrentItem");
                    if (buySelectCurrentItem != null)
                    {
                        //要深复制
                        IdCardBuyerModel = new IdInfoModel()
                        {
                            IdName = buySelectCurrentItem.IdName,
                            IdSex = buySelectCurrentItem.IdSex,
                            IdNation = buySelectCurrentItem.IdNation,
                            IdBirth = buySelectCurrentItem.IdBirth,
                            IdAddress = buySelectCurrentItem.IdAddress,
                            IdNum = buySelectCurrentItem.IdNum,
                            IdAgent = buySelectCurrentItem.IdAgent,

                            Photo = buySelectCurrentItem.Photo,
                            Bitmap = buySelectCurrentItem.Bitmap,
                            IsTrusted = buySelectCurrentItem.IsTrusted,
                            Tag = buySelectCurrentItem.Tag,
                            TagObject = buySelectCurrentItem.TagObject,
                            Male = buySelectCurrentItem.Male,
                            Female = buySelectCurrentItem.Female,
                            Nationality = buySelectCurrentItem.Nationality,
                            Section = buySelectCurrentItem.Section,
                            CertificateType = buySelectCurrentItem.CertificateType,
                            IsCancel = true,//显示撤销按钮
                            IsUpdate = true//修改数据
                        };
                        IdCardBuyerModel.EffectiveDate = buySelectCurrentItem.EffectiveDate;
                        if (IdCardBuyerModel.EffectiveDate != null && buySelectCurrentItem.ExpiryDate == null)
                            IdCardBuyerModel.ExpiryDate = "长期";
                        else
                            IdCardBuyerModel.ExpiryDate = buySelectCurrentItem.ExpiryDate;
                        NotifyPropertyChanged("IdCardBuyerModel");
                        IsBuyerForbid = false;
                        NotifyPropertyChanged("IsBuyerForbid");
                        //选中证书类型
                        IndexBuyerSelect = GetIndexByCertificateType(buySelectCurrentItem.CertificateType);
                    }
                    else
                    {
                        if (IdCardBuyerModel.IsUpdate)
                        {
                            IsBuyerForbid = false;
                            NotifyPropertyChanged("IsBuyerForbid");
                        }
                        else
                        {
                            IsBuyerForbid = true;
                            NotifyPropertyChanged("IsBuyerForbid");
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 根据证书类型获取相应的Index
        /// </summary>
        /// <param name="certificateType"></param>
        /// <returns></returns>
        private int GetIndexByCertificateType(CertificateType certificateType)
        {
            int index = -1;
            switch (certificateType)
            {
                case CertificateType.IdCard:
                    index = 0;
                    break;
                case CertificateType.Passport:
                    index = 1;
                    break;
                case CertificateType.HKMacaoIdCard:
                    index = 2;
                    break;
                case CertificateType.BirthCertificate:
                    index = 3;
                    break;
                case CertificateType.MacaoPermit:
                    index = 4;
                    break;
                case CertificateType.TaiwanPermit:
                    index = 5;
                    break;
                case CertificateType.CertificateOfOfficers:
                    index = 6;
                    break;
                case CertificateType.ResidenceBooklet:
                    index = 7;
                    break;
                default:
                    index = 0;
                    break;
            }
            return index;
        }

        private int buyerTotal;

        public int BuyerTotal
        {
            get { return buyerTotal; }
            set
            {
                if (buyerTotal != value)
                {
                    buyerTotal = value;
                    NotifyPropertyChanged("BuyerTotal");
                }
            }
        }

        /// <summary>
        /// 证件类型
        /// </summary>
        private List<EnumHelper> identitySelleryList;

        public List<EnumHelper> IdentitySellerList
        {
            get { return identitySelleryList; }
            set
            {
                if (identitySelleryList != value)
                {
                    identitySelleryList = value;
                    NotifyPropertyChanged("IdentitySellerList");
                }
            }
        }

        /// <summary>
        ///选择证件类型
        /// </summary>
        private EnumHelper identitySellerSelected;

        public EnumHelper IdentitySellerSelected
        {
            get { return identitySellerSelected; }
            set
            {
                if (identitySellerSelected != value)
                {
                    identitySellerSelected = value;
                    NotifyPropertyChanged("IdentitySellerSelected");
                }
            }
        }

        /// <summary>
        ///选择证件类型Index
        /// </summary>
        private int indexSellerSelect;

        public int IndexSellerSelect
        {
            get { return indexSellerSelect; }
            set
            {
                if (indexSellerSelect != value)
                {
                    indexSellerSelect = value;
                    NotifyPropertyChanged("IndexSellerSelect");
                }
            }
        }

        private bool isSellerForbid = true;

        public bool IsSellerForbid
        {
            get { return isSellerForbid; }
            set
            {
                if (isSellerForbid != value)
                {
                    isSellerForbid = value;
                    NotifyPropertyChanged("IsSellerForbid");
                }
            }
        }

        private IdInfoModel idCardSellerModel;

        public IdInfoModel IdCardSellerModel
        {
            get { return idCardSellerModel; }
            set
            {
                if (idCardSellerModel != value)
                {
                    idCardSellerModel = value;
                    NotifyPropertyChanged("IdCardSellerModel");
                }
            }
        }

        private int sellerTotal;

        public int SellerTotal
        {
            get { return sellerTotal; }
            set
            {
                if (sellerTotal != value)
                {
                    sellerTotal = value;
                    NotifyPropertyChanged("SellerTotal");
                }
            }
        }

        private ObservableCollection<IdInfoModel> sellerInfoList;
        public ObservableCollection<IdInfoModel> SellerInfoList
        {
            get
            {
                return sellerInfoList;
            }
            set
            {
                if (sellerInfoList != value)
                {
                    sellerInfoList = value;
                    NotifyPropertyChanged("SellerInfoList");
                    SellerTotal = sellerInfoList.Count;
                    NotifyPropertyChanged("SellerTotal");
                }
            }
        }

        private IdInfoModel sellSelectCurrentItem;
        public IdInfoModel SellSelectCurrentItem
        {
            get
            {
                return sellSelectCurrentItem;
            }
            set
            {
                if (sellSelectCurrentItem != value)
                {
                    sellSelectCurrentItem = value;
                    NotifyPropertyChanged("SellSelectCurrentItem");
                    if (sellSelectCurrentItem != null)
                    {
                        //要深复制
                        IdCardSellerModel = new IdInfoModel()
                        {
                            IdName = sellSelectCurrentItem.IdName,
                            IdSex = sellSelectCurrentItem.IdSex,
                            IdNation = sellSelectCurrentItem.IdNation,
                            IdBirth = sellSelectCurrentItem.IdBirth,
                            IdAddress = sellSelectCurrentItem.IdAddress,
                            IdNum = sellSelectCurrentItem.IdNum,
                            IdAgent = sellSelectCurrentItem.IdAgent,

                            Photo = sellSelectCurrentItem.Photo,
                            Bitmap = sellSelectCurrentItem.Bitmap,
                            IsTrusted = sellSelectCurrentItem.IsTrusted,
                            Tag = sellSelectCurrentItem.Tag,
                            TagObject = sellSelectCurrentItem.TagObject,
                            Male = sellSelectCurrentItem.Male,
                            Female = sellSelectCurrentItem.Female,
                            Nationality = sellSelectCurrentItem.Nationality,
                            Section = sellSelectCurrentItem.Section,
                            CertificateType = sellSelectCurrentItem.CertificateType,
                            IsCancel = true,//显示撤销按钮
                            IsUpdate = true//修改数据
                        };
                        IdCardSellerModel.EffectiveDate = sellSelectCurrentItem.EffectiveDate;
                        if (IdCardSellerModel.EffectiveDate != null && sellSelectCurrentItem.ExpiryDate == null)
                            IdCardSellerModel.ExpiryDate = "长期";
                        else
                            IdCardSellerModel.ExpiryDate = sellSelectCurrentItem.ExpiryDate;
                        NotifyPropertyChanged("IdCardSellerModel");
                        IsSellerForbid = false;
                        NotifyPropertyChanged("IsSellerForbid");
                        //选中证书类型
                        IndexSellerSelect = GetIndexByCertificateType(sellSelectCurrentItem.CertificateType);
                    }
                    else
                    {
                        if (IdCardSellerModel.IsUpdate)
                        {
                            IsSellerForbid = false;
                            NotifyPropertyChanged("IsSellerForbid");
                        }
                        else
                        {
                            IsSellerForbid = true;
                            NotifyPropertyChanged("IsSellerForbid");
                        }
                    }
                }
            }
        }



        private CaseStatusViewModel allStatus;
        public CaseStatusViewModel AllStatus
        {
            get
            {
                return allStatus;
            }
            set
            {
                if (allStatus != value)
                {
                    allStatus = value;
                    NotifyPropertyChanged("AllStatus");
                }
            }
        }


        #region 附件信息

        /// <summary>
        /// 当前选择的tabitem
        /// </summary>
        private int currentIndexTabItem;
        public int CurrentIndexTabItem
        {
            get
            {
                return currentIndexTabItem;
            }
            set
            {
                if (currentIndexTabItem != value)
                {
                    currentIndexTabItem = value;
                    NotifyPropertyChanged("CurrentIndexTabItem");
                }
            }
        }

        private CustomerInfoViewModel customerInfo;
        public CustomerInfoViewModel CustomerInfo
        {
            get
            {
                return customerInfo;
            }
            set
            {
                if (customerInfo != value)
                {
                    customerInfo = value;
                    NotifyPropertyChanged("CustomerInfo");
                }
            }
        }

        /// <summary>
        /// 卖方
        /// </summary>
        private ObservableCollection<CustomerAndMateriaListViewModel> sellerList;
        public ObservableCollection<CustomerAndMateriaListViewModel> SellerList
        {
            get
            {
                return sellerList;
            }
            set
            {
                if (sellerList != value)
                {
                    sellerList = value;
                    NotifyPropertyChanged("SellerList");
                }
            }
        }

        /// <summary>
        /// 买方
        /// </summary>
        private ObservableCollection<CustomerAndMateriaListViewModel> buyerList;
        public ObservableCollection<CustomerAndMateriaListViewModel> BuyerList
        {
            get
            {
                return buyerList;
            }
            set
            {
                if (buyerList != value)
                {
                    buyerList = value;
                    NotifyPropertyChanged("BuyerList");
                }
            }
        }

        private CustomerAndMateriaListViewModel currentItem;
        public CustomerAndMateriaListViewModel CurrentItem
        {
            get
            {
                return currentItem;
            }
            set
            {
                if (currentItem != value)
                {
                    currentItem = value;
                    NotifyPropertyChanged("CurrentItem");
                    if (CurrentItem != null)
                    {
                        var list = (SellerList.Where(x => x == value).FirstOrDefault()).MateriaList;
                        foreach (var m in MateriaList)
                        {
                            foreach (var i in list)
                            {
                                if (i.MateriaName == m.MateriaName)
                                {
                                    m.MateriaCount = i.MateriaCount;
                                }
                            }
                        }
                        foreach (var m in MateriaList1)
                        {
                            foreach (var i in list)
                            {
                                if (i.MateriaName == m.MateriaName)
                                {
                                    m.MateriaCount = i.MateriaCount;
                                }
                            }
                        }
                        CurrentCustomer = (SellerList.Where(x => x == value).FirstOrDefault()).CustomerInfo;
                    }
                }
            }
        }

        private CustomerInfoViewModel currentCustomer;
        public CustomerInfoViewModel CurrentCustomer
        {
            get
            {
                return currentCustomer;
            }
            set
            {
                if (currentCustomer != value)
                {
                    currentCustomer = value;
                    NotifyPropertyChanged("CurrentCustomer");
                }
            }
        }

        private CustomerInfoViewModel buyCurrentCustomer;
        public CustomerInfoViewModel BuyCurrentCustomer
        {
            get
            {
                return buyCurrentCustomer;
            }
            set
            {
                if (buyCurrentCustomer != value)
                {
                    buyCurrentCustomer = value;
                    NotifyPropertyChanged("BuyCurrentCustomer");
                }
            }
        }

        private CustomerAndMateriaListViewModel buyCurrentItem;
        public CustomerAndMateriaListViewModel BuyCurrentItem
        {
            get
            {
                return buyCurrentItem;
            }
            set
            {
                if (buyCurrentItem != value)
                {
                    buyCurrentItem = value;
                    NotifyPropertyChanged("BuyCurrentItem");
                    if (buyCurrentItem != null)
                    {
                        var list = (BuyerList.Where(x => x == value).FirstOrDefault()).MateriaList;
                        foreach (var m in BuyMateriaList)
                        {
                            foreach (var i in list)
                            {
                                if (i.MateriaName == m.MateriaName)
                                {
                                    m.MateriaCount = i.MateriaCount;
                                }
                            }
                        }
                        foreach (var m in BuyMateriaList1)
                        {
                            foreach (var i in list)
                            {
                                if (i.MateriaName == m.MateriaName)
                                {
                                    m.MateriaCount = i.MateriaCount;
                                }
                            }
                        }
                        BuyCurrentCustomer = (BuyerList.Where(x => x == value).FirstOrDefault()).CustomerInfo;
                    }
                }
            }
        }

        /// <summary>
        /// 附件列表信息
        /// </summary>
        private ObservableCollection<MateriaViewModel> materiaList;
        public ObservableCollection<MateriaViewModel> MateriaList
        {
            get
            {
                return materiaList;
            }
            set
            {
                if (materiaList != value)
                {
                    materiaList = value;
                    NotifyPropertyChanged("MateriaList");
                }
            }
        }

        /// <summary>
        /// 附件列表信息
        /// </summary>
        private ObservableCollection<MateriaViewModel> materiaList1;
        public ObservableCollection<MateriaViewModel> MateriaList1
        {
            get
            {
                return materiaList1;
            }
            set
            {
                if (materiaList1 != value)
                {
                    materiaList1 = value;
                    NotifyPropertyChanged("MateriaList1");
                }
            }
        }

        private ObservableCollection<MateriaViewModel> buyMateriaList;
        public ObservableCollection<MateriaViewModel> BuyMateriaList
        {
            get
            {
                return buyMateriaList;
            }
            set
            {
                if (buyMateriaList != value)
                {
                    buyMateriaList = value;
                    NotifyPropertyChanged("BuyMateriaList");
                }
            }
        }

        private ObservableCollection<MateriaViewModel> buyMateriaList1;
        public ObservableCollection<MateriaViewModel> BuyMateriaList1
        {
            get
            {
                return buyMateriaList1;
            }
            set
            {
                if (buyMateriaList1 != value)
                {
                    buyMateriaList1 = value;
                    NotifyPropertyChanged("BuyMateriaList1");
                }
            }
        }

        /// <summary>
        /// 权利凭证
        /// </summary>
        private ObservableCollection<MateriaViewModel> rightsMateriaList;
        public ObservableCollection<MateriaViewModel> RightsMateriaList
        {
            get
            {
                return rightsMateriaList;
            }
            set
            {
                if (rightsMateriaList != value)
                {
                    rightsMateriaList = value;
                    NotifyPropertyChanged("RightsMateriaList");
                }
            }
        }

        /// <summary>
        /// 交易材料
        /// </summary>
        private ObservableCollection<MateriaViewModel> transactionList;
        public ObservableCollection<MateriaViewModel> TranscationList
        {
            get
            {
                return transactionList;
            }
            set
            {
                if (transactionList != value)
                {
                    transactionList = value;
                    NotifyPropertyChanged("TranscationList");
                }
            }
        }

        /// <summary>
        /// 合同协议
        /// </summary>
        private ObservableCollection<MateriaViewModel> contractList;
        public ObservableCollection<MateriaViewModel> ContractList
        {
            get
            {
                return contractList;
            }
            set
            {
                if (contractList != value)
                {
                    contractList = value;
                    NotifyPropertyChanged("ContractList");
                }
            }
        }

        /// <summary>
        /// 钱款收据
        /// </summary>
        private ObservableCollection<MateriaViewModel> moneyList;
        public ObservableCollection<MateriaViewModel> MoneyList
        {
            get
            {
                return moneyList;
            }
            set
            {
                if (moneyList != value)
                {
                    moneyList = value;
                    NotifyPropertyChanged("MoneyList");
                }
            }
        }


        #endregion



    }
}
