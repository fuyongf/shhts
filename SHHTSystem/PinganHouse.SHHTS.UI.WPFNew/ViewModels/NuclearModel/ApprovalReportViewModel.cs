﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.PubSubEvents;
using PinganHouse.SHHTS.UI.WPFNew.Common;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels
{
    public class ApprovalReportViewModel : BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;

        public ICommand ApprovalReportCommand { get; private set; }
        public ICommand NuclearCommand { get; private set; }

        public ApprovalReportViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;

            this.ApprovalReportCommand = new DelegateCommand<string>(this.OnApprovalReportCommand);
            this.NuclearCommand = new DelegateCommand<object>(this.OnNuclear);
        }

        private void OnApprovalReportCommand(string caseId)
        {
            this.eventAggregator.GetEvent<ApprovalReportEvents>().Publish(caseId);
        }

        private void OnNuclear(object model)
        {
            this.eventAggregator.GetEvent<NuclearEvents>().Publish(Microsoft.Practices.ServiceLocation.ServiceLocator.Current.GetInstance<CheckCaseSearchPageViewModel>());
        }

        private string caseId;
        public string CaseId
        {
            get
            {
                return caseId;
            }
            set
            {
                if (caseId != value)
                {
                    caseId = value;
                    NotifyPropertyChanged("CaseId");
                }
            }
        }
    }
}
