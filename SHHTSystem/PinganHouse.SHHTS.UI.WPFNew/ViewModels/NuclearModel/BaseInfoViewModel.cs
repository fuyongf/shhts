﻿using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.PubSubEvents;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using PinganHouse.SHHTS.DataTransferObjects;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels.NuclearModel
{
    public class BaseInfoViewModel : BaseViewModel
    {
        private readonly IEventAggregator _eventAggregator;
        /// <summary>
        /// 加载
        /// </summary>
        public ICommand BaseInfoCommand { get; private set; }
        /// <summary>
        /// 下一步
        /// </summary>
        public ICommand NextToCustomerInfoCommand { get; private set; }
        /// <summary>
        /// 取消
        /// </summary>
        public ICommand BackSearchInfoCommand { get; private set; }

        public BaseInfoViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this._eventAggregator = eventAggregator;
            this.BaseInfoCommand = new DelegateCommand<string>(this.OnBaseInfoCommand);
            this.BackSearchInfoCommand = new DelegateCommand(OnBackSearchInfoCommand);
            this.NextToCustomerInfoCommand = new DelegateCommand(this.OnNextToCustomerInfoCommand);
        }

        private void OnBaseInfoCommand(string caseId)
        {
            this._eventAggregator.GetEvent<NuclearLoadBasicInfoEvents>().Publish(caseId);
        }

        private void OnBackSearchInfoCommand()
        {
            this._eventAggregator.GetEvent<NuclearEvents>().Publish(Microsoft.Practices.ServiceLocation.ServiceLocator.Current.GetInstance<CheckCaseSearchPageViewModel>());
        }
        private void OnNextToCustomerInfoCommand()
        {
            this._eventAggregator.GetEvent<NuclearSubBasicInfoEvents>().Publish(this);
            //this.eventAggregator.GetEvent<NuclearLoadTransactionInfoEvent>().Publish(caseId);
        }

        #region 字段

        private string _caseId;
        public string CaseId
        {
            get
            {
                return _caseId;
            }
            set
            {
                if (_caseId != value)
                {
                    _caseId = value;
                    NotifyPropertyChanged("CaseId");
                }
            }
        }

        private string _tenementContracts1;
        /// <summary>
        /// 产权证号-1
        /// </summary>
        public string TenementContracts1
        {
            get { return _tenementContracts1; }
            set
            {
                if (_tenementContracts1 != value)
                {
                    _tenementContracts1 = value;
                    NotifyPropertyChanged("TenementContracts1");
                }
            }
        }

        private string _tenementContracts2;
        /// <summary>
        /// 产权证号-2
        /// </summary>
        public string TenementContracts2
        {
            get { return _tenementContracts2; }
            set
            {
                if (_tenementContracts2 != value)
                {
                    _tenementContracts2 = value;
                    NotifyPropertyChanged("TenementContracts2");
                }
            }
        }

        private string _tenementAddress;

        public string TenementAddress
        {
            get { return _tenementAddress; }
            set
            {
                if (_tenementAddress != value)
                {
                    _tenementAddress = value;
                    NotifyPropertyChanged("TenementAddress");
                }
            }
        }


        private string _tenementName;
        /// <summary>
        /// 物业名称
        /// </summary>
        public string TenementName
        {
            get
            {
                return _tenementName;
            }
            set
            {
                if (_tenementName != value)
                {
                    _tenementName = value;
                    NotifyPropertyChanged("TenementName");
                }
            }
        }

        private string _loopLinePosition;
        /// <summary>
        /// 环线位置
        /// </summary>
        public string LoopLinePosition
        {
            get { return _loopLinePosition; }
            set
            {
                if (_loopLinePosition != value)
                {
                    _loopLinePosition = value;
                    NotifyPropertyChanged("LoopLinePosition");
                }
            }
        }

        private decimal? _coveredArea;
        /// <summary>
        /// 建筑面积
        /// </summary>
        public decimal? CoveredArea
        {
            get { return _coveredArea ?? 0; }
            set
            {
                if (_coveredArea != value)
                {
                    _coveredArea = value;
                    NotifyPropertyChanged("CoveredArea");
                }
            }
        }
        private int? _roomCount;
        /// <summary>
        /// 房
        /// </summary>
        public int? RoomCount
        {
            get { return _roomCount ?? 0; }
            set
            {
                if (_roomCount != value)
                {
                    _roomCount = value;
                    NotifyPropertyChanged("RoomCount");
                }
            }
        }
        private int? _hallCount;
        /// <summary>
        /// 厅
        /// </summary>
        public int? HallCount
        {
            get { return _hallCount ?? 0; }
            set
            {
                if (_hallCount != value)
                {
                    _hallCount = value;
                    NotifyPropertyChanged("HallCount");
                }
            }
        }

        private int? _toiletCount;
        /// <summary>
        /// 卫
        /// </summary>
        public int? ToiletCount
        {
            get { return _toiletCount ?? 0; }
            set
            {
                if (_toiletCount != value)
                {
                    _toiletCount = value;
                    NotifyPropertyChanged("ToiletCount");
                }
            }
        }
        private int? _floorCount;
        /// <summary>
        /// 层数
        /// </summary>
        public int? FloorCount
        {
            get { return _floorCount ?? 0; }
            set
            {
                if (_floorCount != value)
                {
                    _floorCount = value;
                    NotifyPropertyChanged("FloorCount");
                }
            }
        }

        private string _builtCompletedDate;
        /// <summary>
        /// 竣工时间
        /// </summary>
        public string BuiltCompletedDate
        {
            get { return _builtCompletedDate ?? string.Empty; }
            set
            {
                if (_builtCompletedDate != value)
                {
                    _builtCompletedDate = value;
                    NotifyPropertyChanged("BuiltCompletedDate");
                }
            }
        }
        private decimal? _mediatorPrice;
        /// <summary>
        /// 房屋价格
        /// </summary>
        public decimal? MediatorPrice
        {
            get { return _mediatorPrice ?? 0.00m; }
            set
            {
                if (_mediatorPrice != value)
                {
                    _mediatorPrice = value;
                    NotifyPropertyChanged("BuiltCompletedDate");
                }
            }
        }


        private decimal? _netlabelPrice;
        /// <summary>
        /// 网签价格
        /// </summary>
        public decimal? NetlabelPrice
        {
            get { return _netlabelPrice ?? 0.00m; }
            set
            {
                if (_netlabelPrice != value)
                {
                    _netlabelPrice = value;
                    NotifyPropertyChanged("NetlabelPrice");
                }
            }
        }

        private decimal? _realPrice;
        /// <summary>
        /// 真实成交价格
        /// </summary>
        public decimal? RealPrice
        {
            get { return _realPrice ?? 0.00m; }
            set
            {
                if (_realPrice != value)
                {
                    _realPrice = value;
                    NotifyPropertyChanged("RealPrice");
                }
            }
        }


        private decimal? _tenementRawPrice;
        /// <summary>
        /// 物业价格
        /// </summary>
        public decimal? TenementRawPrice
        {
            get { return _tenementRawPrice ?? 0.00m; }
            set
            {
                if (_tenementRawPrice != value)
                {
                    _tenementRawPrice = value;
                    NotifyPropertyChanged("TenementRawPrice");
                }
            }
        }


        private TradeType _tradeType;
        /// <summary>
        /// 交易类型
        /// </summary>
        public TradeType TradeType
        {
            get { return _tradeType == TradeType.Unknow ? TradeType.Other : _tradeType; }
            set
            {
                if (_tradeType != value)
                {
                    _tradeType = value;
                    NotifyPropertyChanged("TradeType");
                }
            }
        }

        private EnumHelper _resideType;
        /// <summary>
        /// 房屋类型
        /// </summary>
        public EnumHelper ResideType
        {
            get { return _resideType; }
            set
            {
                if (_resideType != value)
                {
                    _resideType = value;
                    NotifyPropertyChanged("ResideType");
                }
            }
        }

        private bool? _firstTimeBuy;
        /// <summary>
        /// 是否首次购买
        /// </summary>
        public bool? FirstTimeBuy
        {
            get
            {
                return _firstTimeBuy ?? false;
            }
            set
            {
                if (_firstTimeBuy != value)
                {
                    _firstTimeBuy = value;
                    NotifyPropertyChanged("FirstTimeBuy");
                }
            }
        }

        private bool? _onlyHousing;
        /// <summary>
        /// 唯一住房
        /// </summary>
        public bool? OnlyHousing
        {
            get { return _onlyHousing ?? false; }
            set
            {
                if (_onlyHousing != value)
                {
                    _onlyHousing = value;
                    NotifyPropertyChanged("OnlyHousing");
                }
            }
        }
        private BuyOverYears? _buyOverYears;
        /// <summary>
        /// 满2年
        /// </summary>
        public BuyOverYears? BuyOverYears
        {
            get { return _buyOverYears; }
            set
            {
                if (_buyOverYears != value)
                {
                    _buyOverYears = value;
                    NotifyPropertyChanged("BuyOverYears");
                }
            }
        }
        private EnumHelper _buyOverYearsHelper;

        public EnumHelper BuyOverYearsHelper
        {
            get
            {
                return _buyOverYearsHelper;
            }

            set
            {
                if (_buyOverYearsHelper != value)
                {
                    _buyOverYearsHelper = value;
                    NotifyPropertyChanged("BuyOverYearsHelper");
                }
            }
        }

        private int? _purchaseYears;
        /// <summary>
        /// 购入年限
        /// </summary>
        public int? PurchaseYears
        {
            get { return _purchaseYears ?? 0; }
            set
            {
                if (_purchaseYears != value)
                {
                    _purchaseYears = value;
                    NotifyPropertyChanged("PurchaseYears");
                }
            }
        }

        private string _carportAddress;
        /// <summary>
        /// 车位地址
        /// </summary>
        public string CarportAddress
        {
            get { return _carportAddress; }
            set
            {
                if (_carportAddress != value)
                {
                    _carportAddress = value; NotifyPropertyChanged("CarportAddress");
                }
            }
        }

        private decimal? _carportArea;
        /// <summary>
        /// 车位面积
        /// </summary>
        public decimal? CarportArea
        {
            get
            {
                return _carportArea;
            }
            set
            {
                if (_carportArea != value)
                {
                    _carportArea = value; NotifyPropertyChanged("CarportArea");
                }
            }
        }

        private decimal? _carportPrice;
        /// <summary>
        /// 车位价格
        /// </summary>
        public decimal? CarportPrice
        {
            get { return _carportPrice; }
            set
            {
                if (_carportPrice != value)
                {
                    _carportPrice = value; NotifyPropertyChanged("CarportArea");
                }
            }
        }

        private decimal? _carportRawPrice;
        /// <summary>
        /// 车位上手价格
        /// </summary>
        public decimal? CarportRawPrice
        {
            get { return _carportRawPrice ?? 0.00m; }
            set
            {
                if (_carportRawPrice != value)
                {
                    _carportRawPrice = value; NotifyPropertyChanged("CarportRawPrice");
                }
            }
        }
        private bool? _isRepairLandPrice;
        /// <summary>
        /// 是否补偿地价
        /// </summary>
        public bool? IsRepairLandPrice
        {
            get { return _isRepairLandPrice ?? false; }
            set
            {
                if (_isRepairLandPrice != value)
                {
                    _isRepairLandPrice = value;
                    NotifyPropertyChanged("IsRepairLandPrice");
                }
            }
        }

        private bool? _isNsaApproval;
        /// <summary>
        /// 是否国安审批
        /// </summary>
        public bool? IsNsaApproval
        {
            get { return _isNsaApproval ?? false; }
            set
            {
                if (_isNsaApproval != value)
                {
                    _isNsaApproval = value;
                    NotifyPropertyChanged("IsNSAApproval");
                }
            }
        }
        private bool? _isProtectiveBuilding;
        /// <summary>
        /// 是否历史保护建筑
        /// </summary>
        public bool? IsProtectiveBuilding
        {
            get { return _isProtectiveBuilding ?? false; }
            set
            {
                if (_isProtectiveBuilding != value)
                {
                    _isProtectiveBuilding = value;
                    NotifyPropertyChanged("IsProtectiveBuilding");
                }
            }
        }
        private bool? _isNotarize;
        /// <summary>
        /// 是否公正
        /// </summary>
        public bool? IsNotarize
        {
            get { return NotarySysNo == 0; }
        }

        private long _notarySysNo;
        /// <summary>
        /// 公证员
        /// </summary>
        public long NotarySysNo
        {
            get { return _notarySysNo; }
            set
            {
                if (_notarySysNo != value)
                {
                    _notarySysNo = value;
                    NotifyPropertyChanged("Notary");
                }
            }
        }

        #region 经济公司信息

        private string _agentMobile;
        public string AgentMobile
        {
            get { return _agentMobile; }
            set
            {
                if (_agentMobile != value)
                {
                    _agentMobile = value;
                    NotifyPropertyChanged("AgentMobile");
                }
            }
        }
        private string _agentRealName;
        public string AgentRealName
        {
            get { return _agentRealName; }
            set
            {
                if (_agentRealName != value)
                {
                    _agentRealName = value;
                    NotifyPropertyChanged("AgentRealName");
                }
            }
        }
        private string _agentCompanyName;
        public string AgentCompanyName
        {
            get { return _agentCompanyName; }
            set
            {
                if (_agentCompanyName != value)
                {
                    _agentCompanyName = value;
                    NotifyPropertyChanged("AgentCompanyName");
                }
            }
        }
        #endregion

        #region 银行相关
        private bool _isFund;
        /// <summary>
        /// 资金托管
        /// </summary>
        public bool IsFund
        {
            get { return _isFund; }
            set
            {
                if (_isFund != value)
                {
                    _isFund = value;
                    NotifyPropertyChanged("IsFund");
                }
            }
        }

        private string _fundTrusteeshipContract;
        /// <summary>
        /// 资金托管合同
        /// </summary>
        public string FundTrusteeshipContract
        {
            get { return _fundTrusteeshipContract; }
            set
            {
                if (_fundTrusteeshipContract != value)
                {
                    _fundTrusteeshipContract = value;
                    NotifyPropertyChanged("FundTrusteeshipContract");
                }
            }
        }

        private bool _isCommission;
        /// <summary>
        /// 佣金托管
        /// </summary>
        public bool IsCommission
        {
            get { return _isCommission; }
            set
            {
                if (_isCommission != value)
                {
                    _isCommission = value;
                    NotifyPropertyChanged("IsCommission");
                }
            }
        }
        private string _commissionTrusteeshipContract;
        /// <summary>
        /// 佣金托管合同
        /// </summary>
        public string CommissionTrusteeshipContract
        {
            get { return _commissionTrusteeshipContract; }
            set
            {
                if (_commissionTrusteeshipContract != value)
                {
                    _commissionTrusteeshipContract = value;
                    NotifyPropertyChanged("CommissionTrusteeshipContract");
                }
            }
        }

        /// <summary>
        /// 卖方主账户
        /// </summary>
        public bool IsSellerMainAccountExist
        {
            get
            {
                return SellerMainAccount!=null;
            }

        }

        private CustomerBankAccount _sellerMainAccount;
        public CustomerBankAccount SellerMainAccount
        {
            get
            {
                return _sellerMainAccount;
            }

            set
            {
                if (_sellerMainAccount != value)
                {
                    _sellerMainAccount = value;
                    NotifyPropertyChanged("SellerMainAccount");
                }
            }
        }

      
        /// <summary>
        /// 买方主账户
        /// </summary>
        public bool IsBuyerMainAccountExist
        {
            get
            {
                return _buyerMainAccount!=null;
            }
        }

        private CustomerBankAccount _buyerMainAccount;

        public CustomerBankAccount BuyerMainAccount
        {
            get
            {
                return _buyerMainAccount;
            }

            set
            {
                if (_buyerMainAccount != value)
                {
                    _buyerMainAccount = value;
                    NotifyPropertyChanged("BuyerMainAccount");
                }
            }
        }








        private ObservableCollection<Bank> _buyBankList;
        /// <summary>
        /// 买方账户
        /// </summary>
        public ObservableCollection<Bank> BuyerBankList
        {
            get { return _buyBankList ?? new ObservableCollection<Bank>(); }
            set
            {
                if (_buyBankList != value)
                {
                    _buyBankList = value;
                    NotifyPropertyChanged("BuyerBankList");
                }
            }
        }

        private ObservableCollection<Bank> _sellerBankList;
        /// <summary>
        /// 卖方账户
        /// </summary>
        public ObservableCollection<Bank> SellerBankList
        {
            get
            {
                return _sellerBankList ?? new ObservableCollection<Bank>();
            }

            set
            {
                if (_sellerBankList != value)
                {
                    _sellerBankList = value;
                    NotifyPropertyChanged("SellerBankList");
                }
            }
        }

        #endregion

        #endregion

        #region

        private List<EnumHelper> _loopLinePositionList;

        public List<EnumHelper> LoopLinePositionList
        {
            get
            {
                return _loopLinePositionList;
            }

            set
            {
                if (_loopLinePositionList != value)
                {
                    _loopLinePositionList = value;
                    NotifyPropertyChanged("LoopLinePositionList");
                }
            }
        }

        #endregion

    }

    public class Bank : BaseViewModel
    {
        private string _accountNo;

        public string AccountNo
        {

            get { return _accountNo; }
            set
            {
                if (_accountNo != value)
                {
                    _accountNo = value;
                    NotifyPropertyChanged("AccountNo");
                }


            }
        }

        private string _bankName;

        public string BankName
        {

            get { return _bankName; }
            set
            {
                if (_bankName != value)
                {
                    _bankName = value;
                    NotifyPropertyChanged("BankName");
                }


            }
        }

        private string _bankCode;

        public string BankCode
        {

            get { return _bankCode; }
            set
            {
                if (_bankCode != value)
                {
                    _bankCode = value;
                    NotifyPropertyChanged("BankCode");
                }


            }
        }

        private string _subBankName;

        public string SubBankName
        {

            get { return _subBankName; }
            set
            {
                if (_subBankName != value)
                {
                    _subBankName = value;
                    NotifyPropertyChanged("SubBankName");
                }


            }
        }

        private string _accountName;

        public string AccountName
        {

            get { return _accountName; }
            set
            {
                if (_accountName != value)
                {
                    _accountName = value;
                    NotifyPropertyChanged("AccountName");
                }
            }
        }

    }
}
