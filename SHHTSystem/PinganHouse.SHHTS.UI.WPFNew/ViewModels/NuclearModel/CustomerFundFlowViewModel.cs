﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.PubSubEvents;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels
{
    public class CustomerFundFlowViewModel : BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;

        public CustomerFundFlowViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;

        }


        private long sysNo;

        public long SysNo
        {
            get
            {
                return sysNo;
            }

            set
            {
                if (sysNo != value)
                {
                    sysNo = value;
                    NotifyPropertyChanged("SysNo");
                }
            }
        }


        private string rowNum;
        /// <summary>
        /// 第几行
        /// </summary>
        public string RowNum
        {
            get
            {
                return rowNum;
            }

            set
            {
                if (rowNum != value)
                {
                    rowNum = value;
                    NotifyPropertyChanged("RowNum");
                }
            }
        }

        
        private string conditionType;
        /// <summary>
        /// 首付款达成条件
        /// </summary>
        public string ConditionType
        {
            get
            {
                return conditionType;
            }

            set
            {
                if (conditionType != value)
                {
                    conditionType = value;
                    NotifyPropertyChanged("ConditionType");
                }
            }
        }


        private decimal houseFund;
        /// <summary>
        /// 房款
        /// </summary>
        public decimal HouseFund
        {
            get
            {
                return houseFund;
            }

            set
            {
                if (houseFund != value)
                {
                    houseFund = value;
                    NotifyPropertyChanged("HouseFund");
                }
            }
        }


        private decimal taxes;
        /// <summary>
        /// 税费
        /// </summary>
        public decimal Taxes
        {
            get
            {
                return taxes;
            }

            set
            {
                if (taxes != value)
                {
                    taxes = value;
                    NotifyPropertyChanged("Taxes");
                }
            }
        }



        private decimal brokerage;
        /// <summary>
        /// 中介佣金
        /// </summary>
        public decimal Brokerage
        {
            get
            {
                return brokerage;
            }

            set
            {
                if (brokerage != value)
                {
                    brokerage = value;
                    NotifyPropertyChanged("Brokerage");
                }
            }
        }


        private decimal decorateCompensation;
        /// <summary>
        /// 装修补偿
        /// </summary>
        public decimal DecorateCompensation
        {
            get
            {
                return decorateCompensation;
            }

            set
            {
                if (decorateCompensation != value)
                {
                    decorateCompensation = value;
                    NotifyPropertyChanged("DecorateCompensation");
                }
            }
        }


        private decimal earnest;
        /// <summary>
        /// 定金
        /// </summary>
        public decimal Earnest
        {
            get
            {
                return earnest;
            }

            set
            {
                if (earnest != value)
                {
                    earnest = value;
                    NotifyPropertyChanged("Earnest");
                }
            }
        }


        private decimal otherCharges;
        /// <summary>
        /// 其他费用
        /// </summary>
        public decimal OtherCharges
        {
            get
            {
                return otherCharges;
            }

            set
            {
                if (otherCharges != value)
                {
                    otherCharges = value;
                    NotifyPropertyChanged("OtherCharges");
                }
            }
        }


        private decimal amount;
        /// <summary>
        /// 本次合计
        /// </summary>
        public decimal Amount
        {
            get
            {
                return amount;
            }

            set
            {
                if (amount != value)
                {
                    amount = value;
                    NotifyPropertyChanged(" Amount");
                }
            }
        }


        private FundPlanPayChannel paymentChannel;

        public FundPlanPayChannel PaymentChannel
        {
            get
            {
                return paymentChannel;
            }

            set
            {
                if (paymentChannel != value)
                {
                    paymentChannel = value;
                    NotifyPropertyChanged("PaymentChannel");
                }
            }
        }

        private CustomerType customerType;

        public CustomerType CustomerType
        {
            get
            {
                return customerType;
            }

            set
            {
                if (customerType != value)
                {
                    customerType = value;
                    NotifyPropertyChanged("CustomerType");
                }
            }
        }


        private bool isChecked =false;

        public bool IsChecked
        {
            get
            {
                return isChecked;
            }

            set
            {
                if (isChecked != value)
                {
                    isChecked = value;
                    NotifyPropertyChanged("IsChecked");
                }
            }
        }


    }
}
