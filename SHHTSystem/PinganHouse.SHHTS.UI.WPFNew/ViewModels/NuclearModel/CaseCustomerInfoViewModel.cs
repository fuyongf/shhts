﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.PubSubEvents;
using System.Collections.ObjectModel;
using Microsoft.Practices.Prism.Commands;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using System.Windows.Input;
using PinganHouse.SHHTS.Enumerations;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels.NuclearModel
{
    public class CaseCustomerInfoViewModel : BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;

        /// <summary>
        /// 取消
        /// </summary>
        public ICommand BackSearchInfoCommand { get; private set; }

        /// <summary>
        /// 下一步
        /// </summary>
        public ICommand NextToTransactionInfoCommand { get; private set; }


        public ICommand BackToBasicInfoViewCommand { get; private set; }

        public CaseCustomerInfoViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;

            this.BackSearchInfoCommand = new DelegateCommand(OnBackSearchInfoCommand);
            this.NextToTransactionInfoCommand = new DelegateCommand(OnNextToTransactionInfoCommand);
            this.BackToBasicInfoViewCommand = new DelegateCommand(OnBackToBasicInfoViewCommand);


        }

        private void OnBackSearchInfoCommand()
        {
            this.eventAggregator.GetEvent<NuclearEvents>().Publish(Microsoft.Practices.ServiceLocation.ServiceLocator.Current.GetInstance<CheckCaseSearchPageViewModel>());
        }

        private void OnBackToBasicInfoViewCommand()
        {
            this.eventAggregator.GetEvent<NuclearLoadBasicInfoEvents>().Publish(this.CaseId);
        }

        private void OnNextToTransactionInfoCommand()
        {
            this.eventAggregator.GetEvent<NuclearLoadTransactionInfoEvent>().Publish(this.CaseId);
        }
        /// <summary>
        /// CaseId
        /// </summary>
        public string CaseId { get; set; }

        private VMSeller seller;
        /// <summary>
        /// 卖家
        /// </summary>
        public VMSeller Seller
        {
            get
            {
                return seller;
            }

            set
            {
                if (seller != value)
                {
                    seller = value;
                    NotifyPropertyChanged("Seller");
                }
            }
        }

        private VMBuyer buyer;
        /// <summary>
        /// 买家
        /// </summary>
        public VMBuyer Buyer
        {
            get
            {
                return buyer;
            }

            set
            {
                if (buyer != value)
                {
                    buyer = value;
                    NotifyPropertyChanged("Buyer");
                }
            }
        }
    }
    public class VMBuyer : BaseViewModel
    {
        private bool personOrCompany;
        /// <summary>
        /// 公司或个人
        /// </summary>
        public bool PersonOrCompany
        {
            get
            {
                return personOrCompany;
            }

            set
            {
                if (personOrCompany != value)
                {
                    personOrCompany = value;
                    NotifyPropertyChanged("PersonOrCompany");
                }
            }
        }

        private int addressType;
        /// <summary>
        /// 地址类型
        /// </summary>
        public int AddressType
        {
            get
            {
                return addressType;
            }

            set
            {
                if (addressType != value)
                {
                    addressType = value;
                    NotifyPropertyChanged("AddressType");
                }
            }
        }

        private string postProvince;
        /// <summary>
        /// 邮寄-省
        /// </summary>
        public string PostProvince
        {
            get
            {
                return string.IsNullOrEmpty(postProvince) ? "上海市" : postProvince;
            }

            set
            {
                if (postProvince != value)
                {
                    postProvince = value;
                    NotifyPropertyChanged("PostProvince");
                }
            }
        }

        private string postCity;
        /// <summary>
        /// 邮寄-市
        /// </summary>
        public string PostCity
        {
            get
            {
                return string.IsNullOrEmpty(postCity) ? "上海市" : postCity;
            }

            set
            {
                if (postCity != value)
                {
                    postCity = value;
                    NotifyPropertyChanged("PostCity");
                }
            }
        }
        private string postArea;
        /// <summary>
        /// 邮寄-县
        /// </summary>
        public string PostArea
        {
            get
            {
                return string.IsNullOrEmpty(postArea) ? "黄浦区" : postArea;
            }

            set
            {
                if (postArea != value)
                {
                    postArea = value;
                    NotifyPropertyChanged("PostArea");
                }
            }
        }

        private string postAddress;
        /// <summary>
        /// 邮寄地址
        /// </summary>
        public string PostAddress
        {
            get
            {
                return postAddress ?? string.Empty;
            }

            set
            {
                if (postAddress != value)
                {
                    postAddress = value;
                    NotifyPropertyChanged("PostAddress");
                }
            }
        }

        private string recipients;
        /// <summary>
        /// 收件人
        /// </summary>
        public string Recipients
        {
            get
            {
                return recipients ?? string.Empty;
            }

            set
            {
                if (recipients != value)
                {
                    recipients = value;
                    NotifyPropertyChanged("Recipients");
                }
            }
        }

        private string recipientsMobile;
        /// <summary>
        /// 收件人电话
        /// </summary>
        public string RecipientsMobile
        {
            get
            {
                return recipientsMobile ?? string.Empty;
            }

            set
            {
                if (recipientsMobile != value)
                {
                    recipientsMobile = value;
                    NotifyPropertyChanged("RecipientsMobile");
                }
            }
        }

        private bool? selfCredit;
        /// <summary>
        /// 自办贷款
        /// </summary>
        public bool? SelfCredit
        {
            get
            {
                return selfCredit ?? false;
            }

            set
            {
                if (selfCredit != value)
                {
                    selfCredit = value;
                    NotifyPropertyChanged("SelfCredit");
                }
            }
        }

        private SelfCreditReson selfCreditReson;
        /// <summary>
        /// 自办原因
        /// </summary>
        public SelfCreditReson SelfCreditReson
        {
            get
            {
                return selfCreditReson;
            }

            set
            {
                if (selfCreditReson != value)
                {
                    selfCreditReson = value;
                    NotifyPropertyChanged("SelfCreditReson");
                }
            }
        }


        private ObservableCollection<Cusetomers> cusetomersList;
        /// <summary>
        /// 用户列表
        /// </summary>
        public ObservableCollection<Cusetomers> CusetomersList
        {
            get
            {
                return cusetomersList;
            }

            set
            {
                if (cusetomersList != value)
                {
                    cusetomersList = value;
                    NotifyPropertyChanged("CusetomersList");
                }
            }
        }

    }


    public class Cusetomers : BaseViewModel
    {
        private long userSysNo;
        /// <summary>
        /// 用户编号
        /// </summary>
        public long UserSysNo
        {
            get
            {
                return userSysNo;
            }

            set
            {
                if (userSysNo != value)
                {
                    userSysNo = value;
                    NotifyPropertyChanged("UserSysNo");
                }
            }
        }

        private string realName;
        /// <summary>
        /// 姓名
        /// </summary>
        public string RealName
        {
            get
            {
                return realName ?? string.Empty;
            }

            set
            {
                if (realName != value)
                {
                    realName = value;
                    NotifyPropertyChanged("RealName");
                }
            }
        }

        private string gender;
        /// <summary>
        /// 性别
        /// </summary>
        public string Gender
        {
            get
            {
                return gender ?? string.Empty;
            }

            set
            {
                if (gender != value)
                {
                    gender = value;
                    NotifyPropertyChanged("Gender");
                }
            }
        }

        private string certificateType;
        /// <summary>
        /// 证件类型
        /// </summary>
        public string CertificateType
        {
            get
            {
                return certificateType ?? string.Empty;
            }

            set
            {
                if (certificateType != value)
                {
                    certificateType = value;
                    NotifyPropertyChanged("CertificateType");
                }
            }
        }

        private string identityNo;
        /// <summary>
        /// 证件号
        /// </summary>
        public string IdentityNo
        {
            get
            {
                return identityNo;
            }

            set
            {
                if (identityNo != value)
                {
                    identityNo = value;
                    NotifyPropertyChanged("IdentityNo");
                }
            }
        }


        private string nationality;
        /// <summary>
        /// 住址
        /// </summary>
        public string Nationality
        {
            get
            {
                return nationality ?? string.Empty;
            }

            set
            {
                if (nationality != value)
                {
                    nationality = value;
                    NotifyPropertyChanged("Nationality");
                }
            }
        }

        private string mobile;
        /// <summary>
        /// 手机
        /// </summary>
        public string Mobile
        {
            get
            {
                return mobile ?? string.Empty;
            }

            set
            {
                if (mobile != value)
                {
                    mobile = value;
                    NotifyPropertyChanged("Mobile");
                }
            }
        }

        private string email;
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email
        {
            get
            {
                return email ?? string.Empty;
            }

            set
            {
                if (email != value)
                {
                    email = value;
                    NotifyPropertyChanged("Email");
                }
            }
        }



    }

    public class VMSeller : VMBuyer
    {
        private bool? isGuaranty;
        /// <summary>
        /// 自办抵押
        /// </summary>
        public bool? IsGuaranty
        {
            get
            {
                return isGuaranty ?? false;
            }

            set
            {
                if (isGuaranty != value)
                {
                    isGuaranty = value;
                    NotifyPropertyChanged("IsGuaranty");
                }
            }
        }

        private string bankCode;
        /// <summary>
        /// 银行
        /// </summary>
        public string BankCode
        {
            get
            {
                return bankCode ?? string.Empty;
            }

            set
            {
                if (bankCode != value)
                {
                    bankCode = value;
                    NotifyPropertyChanged("BankCode");
                }
            }
        }

        private string bankName;
        /// <summary>
        /// 银行名字
        /// </summary>
        public string BankName
        {
            get
            {
                return bankName ?? string.Empty;
            }

            set
            {
                if (bankName != value)
                {
                    bankName = value;
                    NotifyPropertyChanged("BankName");
                }
            }
        }

        private string repayCreditAmount;
        /// <summary>
        /// 还款金额
        /// </summary>
        public string RepayCreditAmount
        {
            get
            {
                return repayCreditAmount;
            }

            set
            {
                if (repayCreditAmount != value)
                {
                    repayCreditAmount = value;
                    NotifyPropertyChanged("RepayCreditAmount");
                }
            }
        }
    }
}
