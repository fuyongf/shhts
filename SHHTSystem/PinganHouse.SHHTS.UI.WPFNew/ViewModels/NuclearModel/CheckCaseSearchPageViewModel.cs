﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using FSLib.App.SimpleUpdater.Annotations;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.PubSubEvents;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using Microsoft.Practices.ServiceLocation;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels
{
    public class CheckCaseSearchPageViewModel : BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;

        public CheckCaseSearchPageViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;

            //this.BackToMainViewCommand = new DelegateCommand<object>(this.OnBackToMainViewCommand);
            //this.NuclearCommand = new DelegateCommand<object>(this.OnLoadCheckCaseListCommand);
            this.SearchCommand = new DelegateCommand<CheckCaseSearchPageViewModel>(this.OnSearchCommand);
            this.StartContractCommand = new DelegateCommand<string>(this.OnStartContractCommand);
            this.CancelContractCommand = new DelegateCommand<string>(this.OnCancelContractCommand);
            this.ConfirmContractCommand = new DelegateCommand<string>(this.OnConfirmContractDialogCommand);
            StatusList = new ObservableCollection<EnumHelper>();
            ConditionList = new List<string>();
            CaseList = new ObservableCollection<CaseDetailInfoViewModel>();
            SearchCaseCondition = ServiceLocator.Current.GetInstance<CaseDetailInfoViewModel>();
        }

        private CaseDetailInfoViewModel searchCaseCondition;
        public CaseDetailInfoViewModel SearchCaseCondition
        {
            get
            {
                return searchCaseCondition;
            }
            set
            {
                if (searchCaseCondition != value)
                {
                    searchCaseCondition = value;
                    NotifyPropertyChanged("SearchCaseCondition");
                }
            }
        }

        private int currentPageIndex;
        public int CurrentPageIndex
        {
            get
            {
                return currentPageIndex;
            }
            set
            {
                if (currentPageIndex != value)
                {
                    currentPageIndex = value;
                    NotifyPropertyChanged("CurrentPageIndex");
                    this.eventAggregator.GetEvent<NuclearPageChangesEvents>().Publish(CurrentPageIndex);
                }
            }
        }

        private int totalPageCount;
        public int TotalPageCount
        {
            get
            {
                return totalPageCount;
            }
            set
            {
                if (totalPageCount != value)
                {
                    totalPageCount = value;
                    NotifyPropertyChanged("TotalPageCount");
                }
            }
        }

        private ObservableCollection<CaseDetailInfoViewModel> caseList;
        public ObservableCollection<CaseDetailInfoViewModel> CaseList
        {
            get
            {
                return caseList;
            }
            set
            {
                if (caseList != value)
                {
                    caseList = value;
                    NotifyPropertyChanged("CaseList");
                }
            }
        }

        public ICommand NuclearCommand { get; private set; }
        public ICommand SearchCommand { get; private set; }
        
        
        private ObservableCollection<EnumHelper> statusList;
        public ObservableCollection<EnumHelper> StatusList
        {
            get
            {
                return statusList;
            }
            set
            {
                if (statusList != value)
                {
                    statusList = value;
                    NotifyPropertyChanged("StatusList");
                }
            }
        }

        private List<string> conditionList;
        public List<string> ConditionList
        {
            get
            {
                return conditionList;
            }
            set
            {
                if (conditionList != value)
                {
                    conditionList = value;
                    NotifyPropertyChanged("ConditionList");
                }
            }
        }

        private void OnSearchCommand(CheckCaseSearchPageViewModel model)
        {
            if (string.IsNullOrEmpty(InputText))
            {
                //如果没有输入条件，要清除之前高级搜索里面的赋值
                this.SearchCaseCondition.CaseId = null;
                this.SearchCaseCondition.Address = null;
                this.SearchCaseCondition.SponsorName = null;
                this.SearchCaseCondition.BuyerName = null;
                this.SearchCaseCondition.AgentPerson = null;
                this.SearchCaseCondition.StartTime = null;
                this.SearchCaseCondition.EndTime = null;
            }
            else
            {
                if (SelectIndex != -1)
                {
                    switch (SelectIndex)
                    {
                        //"案件编号","物业地址","主办人","买方姓名","经纪人"
                        case 0:
                            this.SearchCaseCondition.CaseId = InputText;
                            this.SearchCaseCondition.Address = null;
                            this.SearchCaseCondition.SponsorName = null;
                            this.SearchCaseCondition.BuyerName = null;
                            this.SearchCaseCondition.AgentPerson = null;
                            this.SearchCaseCondition.StartTime = null;
                            this.SearchCaseCondition.EndTime = null;
                            break;
                        case 1:
                            this.SearchCaseCondition.Address = InputText;
                            this.SearchCaseCondition.CaseId = null;
                            this.SearchCaseCondition.SponsorName = null;
                            this.SearchCaseCondition.BuyerName = null;
                            this.SearchCaseCondition.AgentPerson = null;
                            this.SearchCaseCondition.StartTime = null;
                            this.SearchCaseCondition.EndTime = null;
                            break;
                        case 2:
                            this.SearchCaseCondition.SponsorName = InputText;
                            this.SearchCaseCondition.CaseId = null;
                            this.SearchCaseCondition.Address = null;
                            this.SearchCaseCondition.BuyerName = null;
                            this.SearchCaseCondition.AgentPerson = null;
                            this.SearchCaseCondition.StartTime = null;
                            this.SearchCaseCondition.EndTime = null;
                            break;
                        case 3:
                            this.SearchCaseCondition.BuyerName = InputText;
                            this.SearchCaseCondition.CaseId = null;
                            this.SearchCaseCondition.Address = null;
                            this.SearchCaseCondition.SponsorName = null;
                            this.SearchCaseCondition.AgentPerson = null;
                            this.SearchCaseCondition.StartTime = null;
                            this.SearchCaseCondition.EndTime = null;
                            break;
                        case 4:
                            this.SearchCaseCondition.AgentPerson = InputText;
                            this.SearchCaseCondition.CaseId = null;
                            this.SearchCaseCondition.Address = null;
                            this.SearchCaseCondition.SponsorName = null;
                            this.SearchCaseCondition.BuyerName = null;
                            this.SearchCaseCondition.StartTime = null;
                            this.SearchCaseCondition.EndTime = null;
                            break;
                    }
                    NotifyPropertyChanged("SearchCaseCondition");
                }
            }
            this.eventAggregator.GetEvent<NuclearEvents>().Publish(this);
        }

        /// <summary>
        /// 确认接单
        /// </summary>
        public ICommand StartContractCommand { get; private set; }
        
        private void OnStartContractCommand(string caseId)
        {
            this.eventAggregator.GetEvent<StartContractEvents>().Publish(caseId);
        }

        /// <summary>
        /// 取消签约
        /// </summary>
        public ICommand CancelContractCommand { get; private set; }

        private void OnCancelContractCommand(string caseId)
        {
            this.eventAggregator.GetEvent<CancelContractEvents>().Publish(caseId);
        }

        /// <summary>
        /// 确认完成签约
        /// </summary>
        public ICommand ConfirmContractCommand { get; private set; }

        private void OnConfirmContractDialogCommand(string caseId)
        {
            this.eventAggregator.GetEvent<ConfirmContractEvents>().Publish(caseId);
        }

        private string inputText;
        public string InputText
        {
            get
            {
                return inputText;
            }
            set
            {
                if (inputText != value)
                {
                    inputText = value;
                    NotifyPropertyChanged("InputText");
                }
            }
        }
        private int selectIndex;
        public int SelectIndex
        {
            get
            {
                return selectIndex;
            }
            set
            {
                if (selectIndex != value)
                {
                    selectIndex = value;
                    NotifyPropertyChanged("SelectIndex");
                }
            }
        }
    }
}
