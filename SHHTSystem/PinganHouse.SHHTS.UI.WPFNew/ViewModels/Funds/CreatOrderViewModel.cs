﻿using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.PubSubEvents;

using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using PinganHouse.SHHTS.UI.WPFNew.Views;

using System;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels
{
    public class CreatOrderViewModel : BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;

        public CreatOrderViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;


            this.CreateOrderCommand = new DelegateCommand<string>(this.OnCreateOrderCommand);
            this.SubmitPaymentOrderCommand = new DelegateCommand<object>(this.OnSubmitPaymentOrderCommand);

        }

        public ICommand CreateOrderCommand { get; private set; }


        private void OnCreateOrderCommand(string caseId)
        {
            this.eventAggregator.GetEvent<CreateOrderCaseInfoEvents>().Publish(caseId);
        }


        public ICommand SubmitPaymentOrderCommand { get; private set; }

        private void OnSubmitPaymentOrderCommand(object obj)
        {
            this.eventAggregator.GetEvent<SubmitPaymentOrderEvents>().Publish(this);
        }


        public ICommand UploadFileCommand { get; private set; }

        private void OnUploadFileCommand()
        {

        }


        private ObservableCollection<string> orderTypeList = new ObservableCollection<string>()
                    {
                    "订单类型选择","出账订单","入账订单（POS）","入账订单（转账）","销账订单","费用订单（POS）","费用订单（转账）"
                    };
        public ObservableCollection<string> OrderTypeList
        {
            get
            {
                return orderTypeList;
            }
            set
            {
                if (orderTypeList != value)
                {
                    orderTypeList = value;
                    NotifyPropertyChanged("OrderTypeList");
                }
            }
        }

       

        private int selectIndex;
        public int SelectIndex
        {
            get
            {
                return selectIndex;
            }
            set
            {
                if (selectIndex != value)
                {
                    selectIndex = value;
                    NotifyPropertyChanged("SelectIndex");
                }
            }
        }


        /// <summary>
        /// 案件信息 部分显示
        /// </summary>
        private CaseInfo caseInfo;
        public CaseInfo CaseInfo
        {
            get
            {
                return caseInfo;
            }
            set
            {
                if (caseInfo != value)
                {
                    caseInfo = value;
                    NotifyPropertyChanged("CaseInfo");
                }
            }
        }

        //private OrderInfo orderInfo;
        //public OrderInfo OrderInfo
        //{
        //    get
        //    {
        //        return orderInfo;
        //    }
        //    set
        //    {
        //        if (orderInfo != value)
        //        {
        //            orderInfo = value;
        //            NotifyPropertyChanged("OrderInfo");
        //        }
        //    }
        //}

        #region 基本信息

        /// <summary>
        /// 案件编号
        /// </summary>
        private string caseId;
        public string CaseId
        {
            get
            {
                return caseId;
            }
            set
            {
                if (caseId != value)
                {
                    caseId = value;
                    NotifyPropertyChanged("CaseId");
                }
            }
        }

        /// <summary>
        /// 产权证号
        /// </summary>
        private string tenementContract;
        public string TenementContract
        {
            get
            {
                return tenementContract;
            }
            set
            {
                if (tenementContract != value)
                {
                    tenementContract = value;
                    NotifyPropertyChanged("TenementContract");
                }
            }
        }

        /// <summary>
        /// 物业地址
        /// </summary>
        private string address;
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                if (address != value)
                {
                    address = value;
                    NotifyPropertyChanged("Address");
                }
            }
        }

        /// <summary>
        /// 卖方姓名
        /// </summary>
        private string sellerName;
        public string SellerName
        {
            get
            {
                return sellerName;
            }
            set
            {
                if (sellerName != value)
                {
                    sellerName = value;
                    NotifyPropertyChanged("SellerName");
                }
            }
        }

        /// <summary>
        /// 买方姓名
        /// </summary>
        private string buyerName;
        public string BuyerName
        {
            get
            {
                return buyerName;
            }
            set
            {
                if (buyerName != value)
                {
                    buyerName = value;
                    NotifyPropertyChanged("BuyerName");
                }
            }
        }


        /// <summary>
        /// 中介公司
        /// </summary>
        private string agentCompany;
        public string AgentCompany
        {
            get
            {
                return agentCompany;
            }
            set
            {
                if (agentCompany != value)
                {
                    agentCompany = value;
                    NotifyPropertyChanged("AgentCompany");
                }
            }
        }

        /// <summary>
        /// 经纪人
        /// </summary>
        private string agentPerson;
        public string AgentPerson
        {
            get
            {
                return agentPerson;
            }
            set
            {
                if (agentPerson != value)
                {
                    agentPerson = value;
                    NotifyPropertyChanged("AgentPerson");
                }
            }
        }


        #endregion

        #region 出账订单

        private PaymentOrderViewModel m_PaymentOrderInfo;

        public PaymentOrderViewModel PaymentOrderInfo 
        {
            get 
            {
                return m_PaymentOrderInfo;
            }
            set 
            {
                m_PaymentOrderInfo = value;
                NotifyPropertyChanged("PaymentOrderInfo");
            }
        }

        #endregion

        #region 入账订单（POS）

        private RecordedOrderPosViemModel m_RecordedOrderPosInfo;

        public RecordedOrderPosViemModel RecordedOrderPosInfo
        {
            get
            {
                return m_RecordedOrderPosInfo;
            }
            set
            {
                m_RecordedOrderPosInfo = value;
                NotifyPropertyChanged("RecordedOrderPosInfo");
            }
        }
        #endregion

        #region 费用订单（POS）

        private CostOrderPosViemModel m_CostOrderPosInfo;

        public CostOrderPosViemModel CostOrderPosInfo
        {
            get
            {
                return m_CostOrderPosInfo;
            }
            set
            {
                m_CostOrderPosInfo = value;
                NotifyPropertyChanged("CostOrderPosInfo");
            }
        }
        #endregion

        #region 销账订单

        private WriteOffOrderViemModel m_WriteOffOrderInfo;

        public WriteOffOrderViemModel WriteOffOrderInfo
        {
            get
            {
                return m_WriteOffOrderInfo;
            }
            set
            {
                m_WriteOffOrderInfo = value;
                NotifyPropertyChanged("WriteOffOrderInfo");
            }
        }

        #endregion

    }

    /// <summary>
    /// 出账订单
    /// </summary>
    public class PaymentOrderViewModel : BaseViewModel 
    {
        private readonly IEventAggregator eventAggregator;

        public PaymentOrderViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;
        }

        private void OnPaymentOrderAccountChangeCommand()
        {
            this.eventAggregator.GetEvent<PaymentOrderAccountChangeEvents>().Publish(this);
        }

        private void OnGetBankAccountsCommand()
        {
            this.eventAggregator.GetEvent<GetBankAccountsEvents>().Publish(this);
        }


        public string CaseId
        {
            get;
            set;
        }

        public long CaseSysNo
        {
            get;
            set;
        }

        public long CompanySysNo
        {
            get;
            set;
        }

        private int m_PayerSelectIndex = 1;
        public int PayerSelectIndex 
        {
            get
            {
                return m_PayerSelectIndex;
            }
            set
            {
                m_PayerSelectIndex = value;
                NotifyPropertyChanged("PayerSelectIndex");
            }
        }

        /// <summary>
        /// 出款方
        /// </summary>
        private AccountType m_Payer = AccountType.Buyer;
        public AccountType Payer 
        {
            get
            {
                return m_Payer;
            }
            set
            {
                m_Payer = value;
                //NotifyPropertyChanged("Payer");
                OnPaymentOrderAccountChangeCommand();
            }
        }

        /// <summary>
        /// 出款方余额
        /// </summary>
        private decimal m_Balance;
        public decimal Balance 
        {
            get
            {
                return m_Balance;
            }
            set
            {
                m_Balance = value;
                NotifyPropertyChanged("Balance");
            }
        }

        /// <summary>
        /// 收款方
        /// </summary>
        private AccountType m_Recer = AccountType.Seller;
        public AccountType Recer
        {
            get
            {
                return m_Recer;
            }
            set
            {
                m_Recer = value;
                OnGetBankAccountsCommand();
            }
        }

        private int m_RecerSelectIndex = 0;
        public int RecerSelectIndex
        {
            get
            {
                return m_RecerSelectIndex;
            }
            set
            {
                m_RecerSelectIndex = value;
                NotifyPropertyChanged("RecerSelectIndex");
            }
        }

        private string accountNo;
        public string AccountNo
        {
            get
            {
                return accountNo;
            }
            set
            {
                accountNo = value;
                NotifyPropertyChanged("AccountNo");
            }
        }

        private string accountName;
        public string AccountName
        {
            get
            {
                return accountName;
            }
            set
            {
                accountName = value;
                NotifyPropertyChanged("AccountName");
            }
        }

        private string bankCode;
        public string BankCode
        {
            get
            {
                return bankCode;
            }
            set
            {
                bankCode = value;
                NotifyPropertyChanged("BankCode");
            }
        }

        private string bankName;
        public string BankName
        {
            get
            {
                return bankName;
            }
            set
            {
                bankName = value;
                NotifyPropertyChanged("BankName");
            }
        }

        private string subBankName;
        public string SubBankName
        {
            get
            {
                return subBankName;
            }
            set
            {
                subBankName = value;
                NotifyPropertyChanged("SubBankName");
            }
        }

        /// <summary>
        /// 出款用途
        /// </summary>
        private OrderBizType orderBizType;
        public OrderBizType OrderBizType
        {
            get
            {
                return orderBizType;
            }
            set
            {
                if (orderBizType != value)
                {
                    orderBizType = value;
                    NotifyPropertyChanged("OrderBizType");
                }
            }
        }

        /// <summary>
        /// 备注
        /// </summary>
        private string remark;
        public string Remark
        {
            get
            {
                return remark;
            }
            set
            {
                if (remark != value)
                {
                    remark = value;
                    NotifyPropertyChanged("Remark");
                }
            }
        }

    }

    /// <summary>
    /// 入账订单（POS）
    /// </summary>
    public class RecordedOrderPosViemModel : BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;

        public RecordedOrderPosViemModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;
        }

        public string CaseId
        {
            get;
            set;
        }

        /// <summary>
        /// 付款归属
        /// </summary>
        private AccountType paymentBelonging;
        public AccountType PaymentBelonging
        {
            get
            {
                return paymentBelonging;
            }
            set
            {
                paymentBelonging = value;
                NotifyPropertyChanged("PaymentBelonging");
            }
        }

        /// <summary>
        /// 付款用途
        /// </summary>
        private OrderBizType orderBizType;
        public OrderBizType OrderBizType
        {
            get
            {
                return orderBizType;
            }
            set
            {
                if (orderBizType != value)
                {
                    orderBizType = value;
                    NotifyPropertyChanged("OrderBizType");
                }
            }
        }

        /// <summary>
        /// 付款金额
        /// </summary>
        private decimal payAmount;
        public decimal PayAmount
        {
            get
            {
                return payAmount;
            }
            set
            {
                payAmount = value;
                NotifyPropertyChanged("PayAmount");
            }
        }

    }

    /// <summary>
    /// 入账订单（转账）
    /// </summary>
    public class RecordedOrderTransferAccountsViemModel : BaseViewModel
    {

    }
    
    /// <summary>
    /// 销账订单
    /// </summary>
    public class WriteOffOrderViemModel : BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;

        public WriteOffOrderViemModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;
        }


        public string CaseId
        {
            get;
            set;
        }

        public long CaseSysNo
        {
            get;
            set;
        }

        public long CompanySysNo
        {
            get;
            set;
        }

        private int m_PayerSelectIndex = 1;
        public int PayerSelectIndex
        {
            get
            {
                return m_PayerSelectIndex;
            }
            set
            {
                m_PayerSelectIndex = value;
                NotifyPropertyChanged("PayerSelectIndex");
            }
        }

        /// <summary>
        /// 出款方
        /// </summary>
        private AccountType m_Payer = AccountType.Buyer;
        public AccountType Payer
        {
            get
            {
                return m_Payer;
            }
            set
            {
                m_Payer = value;
                //OnPaymentOrderAccountChangeCommand();
            }
        }

        /// <summary>
        /// 出款方余额
        /// </summary>
        private decimal m_Balance;
        public decimal Balance
        {
            get
            {
                return m_Balance;
            }
            set
            {
                m_Balance = value;
                NotifyPropertyChanged("Balance");
            }
        }

        /// <summary>
        /// 收款方
        /// </summary>
        private AccountType m_Recer = AccountType.Seller;
        public AccountType Recer
        {
            get
            {
                return m_Recer;
            }
            set
            {
                m_Recer = value;
                //OnGetBankAccountsCommand();
            }
        }

        private int m_RecerSelectIndex = 0;
        public int RecerSelectIndex
        {
            get
            {
                return m_RecerSelectIndex;
            }
            set
            {
                m_RecerSelectIndex = value;
                NotifyPropertyChanged("RecerSelectIndex");
            }
        }

    }

    /// <summary>
    /// 费用订单（POS）
    /// </summary>
    public class CostOrderPosViemModel : BaseViewModel
    {
        public string CaseId
        {
            get;
            set;
        }

        /// <summary>
        /// 付款用途
        /// </summary>
        private OrderBizType orderBizType;
        public OrderBizType OrderBizType
        {
            get
            {
                return orderBizType;
            }
            set
            {
                if (orderBizType != value)
                {
                    orderBizType = value;
                    NotifyPropertyChanged("OrderBizType");
                }
            }
        }

        /// <summary>
        /// 付款金额
        /// </summary>
        private decimal payAmount;
        public decimal PayAmount
        {
            get
            {
                return payAmount;
            }
            set
            {
                payAmount = value;
                NotifyPropertyChanged("PayAmount");
            }
        }
    }

    /// <summary>
    /// 费用订单（转账）
    /// </summary>
    public class CostOrderTransferAccountsViemModel : BaseViewModel
    {

    }
}
