﻿using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.ServiceLocation;

using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPFNew.Common;

using System;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels
{
    public class QueryCaseInfoViewModel : BaseViewModel
    {

        private readonly IEventAggregator eventAggregator;
        #region ctor
        public QueryCaseInfoViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;

            CaseList = new ObservableCollection<QueryCaseInfoViewModel>();
            this.SearchCommand = new DelegateCommand<QueryCaseInfoViewModel>(this.OnSearchCommand);
            this.LoadQueryCaseInfoCommand = new DelegateCommand<object>(this.OnLoadQueryCaseInfoEvents);
            this.CreateOrderCommand = new DelegateCommand<string>(this.OnCreateOrderCommand);
            this.LoadDepositFundsListCommand = new DelegateCommand<DepositFundsViewModel>(this.OnLoadDepositFundsListCommand);
        }
        #endregion

        #region public Method
        public ICommand SearchCommand { get; private set; }
        public ICommand LoadDepositFundsListCommand { get; private set; }
        public ICommand LoadQueryCaseInfoCommand { get; private set; }
        public ICommand CreateOrderCommand { get; private set; }
        #endregion 

        #region private Method
        private void OnSearchCommand(QueryCaseInfoViewModel model)
        {
            if (SelectIndex != -1)
            {
                QueryStringType = (SHHTS.Enumerations.QueryStringType)selectIndex;
            }
            this.eventAggregator.GetEvent<LoadQueryCaseInfoEvents>().Publish(this);
        }


        private void OnLoadQueryCaseInfoEvents(object obj)
        {
            this.eventAggregator.GetEvent<LoadQueryCaseInfoEvents>().Publish(this);
        }


        private void OnCreateOrderCommand(string caseId)
        {
            this.eventAggregator.GetEvent<CreateOrderCaseInfoEvents>().Publish(caseId);
        }

        private void OnLoadDepositFundsListCommand(DepositFundsViewModel model)
        {
            this.eventAggregator.GetEvent<LoadDepositFundsListEvents>().Publish(model);
        }
        #endregion



        private ObservableCollection<string> conditionList = new ObservableCollection<string>()
                    {
                    "案件编号","产权号","物业地址"
                    };
        public ObservableCollection<string> ConditionList
        {
            get
            {
                return conditionList;
            }
            set
            {
                if (conditionList != value)
                {
                    conditionList = value;

                    NotifyPropertyChanged("ConditionList");
                }
            }
        }

        private ObservableCollection<QueryCaseInfoViewModel> caseList;
        public ObservableCollection<QueryCaseInfoViewModel> CaseList
        {
            get
            {
                return caseList;
            }
            set
            {
                if (caseList != value)
                {
                    caseList = value;
                    NotifyPropertyChanged("CaseList");
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private string condition;
        public string Condition
        {
            get
            {
                return condition;
            }
            set
            {
                if (condition != value)
                {
                    condition = value;
                    NotifyPropertyChanged("Condition");
                }
            }
        }


        private int selectIndex;
        public int SelectIndex
        {
            get
            {
                return selectIndex;
            }
            set
            {
                if (selectIndex != value)
                {
                    selectIndex = value;
                    NotifyPropertyChanged("SelectIndex");
                }
            }
        }

        /// <summary>
        /// 查询条件类型
        /// </summary>
        private QueryStringType? queryStringType;
        public QueryStringType? QueryStringType
        {
            get
            {
                return queryStringType;
            }
            set
            {
                if (queryStringType != value)
                {
                    queryStringType = value;
                    NotifyPropertyChanged("QueryStringType");
                }
            }
        }

        private bool isPageChanged;
        public bool IsPageChanged
        {
            get
            {
                return isPageChanged;
            }
            set
            {
                if (isPageChanged != value)
                {
                    isPageChanged = value;
                    NotifyPropertyChanged("IsPageChanged");
                }
            }
        }

        private int currentPageIndex;
        public int CurrentPageIndex
        {
            get
            {
                return currentPageIndex;
            }
            set
            {
                if (currentPageIndex != value)
                {
                    currentPageIndex = value;
                    NotifyPropertyChanged("CurrentPageIndex");
                    if (IsPageChanged)
                    {
                        this.eventAggregator.GetEvent<PageChangesEvents>().Publish(CurrentPageIndex);
                    }
                }
            }
        }

        private int totalPageCount;
        public int TotalPageCount
        {
            get
            {
                return totalPageCount;
            }
            set
            {
                if (totalPageCount != value)
                {
                    totalPageCount = value;
                    NotifyPropertyChanged("TotalPageCount");
                }
            }
        }


        #region Property Members
        /// <summary>
        /// 案件编号
        /// </summary>
        private string caseId;
        public string CaseId
        {
            get
            {
                return caseId;
            }
            set
            {
                if (caseId != value)
                {
                    caseId = value;
                    NotifyPropertyChanged("CaseId");
                }
            }
        }

        /// <summary>
        /// 产权证号
        /// </summary>
        private string tenementContract;
        public string TenementContract
        {
            get
            {
                return tenementContract;
            }
            set
            {
                if (tenementContract != value)
                {
                    tenementContract = value;
                    NotifyPropertyChanged("TenementContract");
                }
            }
        }

        /// <summary>
        /// 物业地址
        /// </summary>
        private string address;
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                if (address != value)
                {
                    address = value;
                    NotifyPropertyChanged("Address");
                }
            }
        }

        /// <summary>
        /// 中介公司
        /// </summary>
        private string agentCompany;
        public string AgentCompany
        {
            get
            {
                return agentCompany;
            }
            set
            {
                if (agentCompany != value)
                {
                    agentCompany = value;
                    NotifyPropertyChanged("AgentCompany");
                }
            }
        }
        /// <summary>
        /// 经纪人
        /// </summary>
        private string agentPerson;
        public string AgentPerson
        {
            get
            {
                return agentPerson;
            }
            set
            {
                if (agentPerson != value)
                {
                    agentPerson = value;
                    NotifyPropertyChanged("AgentPerson");
                }
            }
        }
        /// <summary>
        /// 买方姓名
        /// </summary>
        private string buyerName;
        public string BuyerName
        {
            get
            {
                return buyerName;
            }
            set
            {
                if (buyerName != value)
                {
                    buyerName = value;
                    NotifyPropertyChanged("BuyerName");
                }
            }
        }

        /// <summary>
        /// 卖方姓名
        /// </summary>
        private string sellerName;
        public string SellerName
        {
            get
            {
                return sellerName;
            }
            set
            {
                if (sellerName != value)
                {
                    sellerName = value;
                    NotifyPropertyChanged("SellerName");
                }
            }
        }

        /// <summary>
        /// 创建人员
        /// </summary>
        private string creatorName;
        public string CreatorName
        {
            get
            {
                return creatorName;
            }
            set
            {
                if (creatorName != value)
                {
                    creatorName = value;
                    NotifyPropertyChanged("CreatorName");
                }
            }
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        private DateTime createTime;
        public DateTime CreateTime
        {
            get
            {
                return createTime;
            }
            set
            {
                if (createTime != value)
                {
                    createTime = value;
                    NotifyPropertyChanged("CreateTime");
                }
            }
        }

        /// <summary>
        /// 操作
        /// </summary>
        private string operation;
        public string Operation
        {
            get
            {
                return operation;
            }
            set
            {
                if (operation != value)
                {
                    operation = value;
                    NotifyPropertyChanged("Operation");
                }
            }
        }

        #endregion


    }
}
