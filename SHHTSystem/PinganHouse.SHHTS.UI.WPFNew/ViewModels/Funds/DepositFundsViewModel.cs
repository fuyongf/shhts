﻿using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.PubSubEvents;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels
{
    public class DepositFundsViewModel:BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;
        public DepositFundsViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;
            CaseList = new ObservableCollection<DepositFundsDetailViewModel>();
            this.SearchCommand = new DelegateCommand<QueryCaseInfoViewModel>(this.OnSearchCommand);
        }

        public ICommand SearchCommand { get; private set; }

        private void OnSearchCommand(QueryCaseInfoViewModel model)
        {
            //this.eventAggregator.GetEvent<LoadQueryCaseInfoEvents>().Publish(this);
        }
        private CaseDetailInfoViewModel searchCaseCondition;
        public CaseDetailInfoViewModel SearchCaseCondition
        {
            get
            {
                return searchCaseCondition;
            }
            set
            {
                if (searchCaseCondition != value)
                {
                    searchCaseCondition = value;
                    NotifyPropertyChanged("SearchCaseCondition");
                }
            }
        }
        private ObservableCollection<DepositFundsDetailViewModel> caseList;
        public ObservableCollection<DepositFundsDetailViewModel> CaseList
        {
            get
            {
                return caseList;
            }
            set
            {
                if (caseList != value)
                {
                    caseList = value;
                    NotifyPropertyChanged("CaseList");
                }
            }
        }
        private int currentPageIndex;
        public int CurrentPageIndex
        {
            get
            {
                return currentPageIndex;
            }
            set
            {
                if (currentPageIndex != value)
                {
                    currentPageIndex = value;
                    NotifyPropertyChanged("CurrentPageIndex");
                } 
                if (IsPageChanged)
                {
                    this.eventAggregator.GetEvent<DepositFundPageChangesEvents>().Publish(CurrentPageIndex);
                }
            }
        }
        private int totalPageCount;
        public int TotalPageCount
        {
            get
            {
                return totalPageCount;
            }
            set
            {
                if (totalPageCount != value)
                {
                    totalPageCount = value;
                    NotifyPropertyChanged("TotalPageCount");
                }
            }
        }
        private bool isPageChanged;
        public bool IsPageChanged
        {
            get
            {
                return isPageChanged;
            }
            set
            {
                if (isPageChanged != value)
                {
                    isPageChanged = value;
                    NotifyPropertyChanged("IsPageChanged");
                }
            }
        }


    }

    public class DepositFundsDetailViewModel:BaseViewModel
    {

        private string caseId;
        public string CaseId
        {
            get
            {
                return caseId;
            }
            set
            {
                if (caseId != value)
                {
                    caseId = value;
                    NotifyPropertyChanged("CaseId");
                }
            }
        }
        private decimal buyerBalance;
        public decimal BuyerBalance
        {
            get
            {
                return buyerBalance;
            }
            set
            {
                if (buyerBalance != value)
                {
                    buyerBalance = value;
                    NotifyPropertyChanged("BuyerBalance");
                }
            }
        }
        private decimal sellerBalance;
        public decimal SellerBalance
        {
            get
            {
                return sellerBalance;
            }
            set
            {
                if (sellerBalance != value)
                {
                    sellerBalance = value;
                    NotifyPropertyChanged("SellerBalance");
                }
            }
        }
        private string address;
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                if (address != value)
                {
                    address = value;
                    NotifyPropertyChanged("Address");
                }
            }
        }
        private string contract;
        public string Contract
        {
            get
            {
                return contract;
            }
            set
            {
                if (contract != value)
                {
                    contract = value;
                    NotifyPropertyChanged("Contract");
                }
            }
        }
        
    }
}
