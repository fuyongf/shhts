﻿using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.ServiceLocation;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using PinganHouse.SHHTS.UI.WPFNew.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels
{
    public class SearchListPageViewModel : BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;

        public SearchListPageViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;
            this.AddNewCaseCommand = new DelegateCommand<object>(this.OnAddNewCaseCommand);
            this.SearchCommand = new DelegateCommand<SearchListPageViewModel>(this.OnSearchCommand);
            this.SeniorSearchCommand = new DelegateCommand<object>(this.OnSeniorSearchCommand);
            StatusList = new ObservableCollection<EnumHelper>();
            ConditionList = new List<string>();
            CaseList = new ObservableCollection<CaseDetailInfoViewModel>();
            SearchCaseCondition = ServiceLocator.Current.GetInstance<CaseDetailInfoViewModel>();
        }

        private void OnSeniorSearchCommand(object model)
        {
            this.eventAggregator.GetEvent<PreCheckSeniorSeacheEvents>().Publish(model);
        }
        public ICommand SeniorSearchCommand { get; private set; }
        public ICommand AddNewCaseCommand { get; private set; }

        public ICommand SearchCommand { get; private set; }
        private void OnAddNewCaseCommand(object model)
        {
            this.eventAggregator.GetEvent<PreCheckAddCaseEvents>().Publish(model);
        }
        private void OnSearchCommand(SearchListPageViewModel model)
        {
            if (string.IsNullOrEmpty(InputText))
            {
                //如果没有输入条件，要清除之前高级搜索里面的赋值
                this.SearchCaseCondition.CaseId = null;
                this.SearchCaseCondition.Address = null;
                this.SearchCaseCondition.SponsorName = null;
                this.SearchCaseCondition.BuyerName = null;
                this.SearchCaseCondition.AgentPerson = null;
                this.SearchCaseCondition.StartTime = null;
                this.SearchCaseCondition.EndTime = null;
            }
            else
            {
                if (SelectIndex != -1)
                {
                    switch (SelectIndex)
                    {
                        //"案件编号","物业地址","主办人","买方姓名","经纪人"
                        case 0:
                            this.SearchCaseCondition.CaseId = InputText;
                            this.SearchCaseCondition.Address = null;
                            this.SearchCaseCondition.SponsorName = null;
                            this.SearchCaseCondition.BuyerName = null;
                            this.SearchCaseCondition.AgentPerson = null;
                            this.SearchCaseCondition.StartTime = null;
                            this.SearchCaseCondition.EndTime = null;
                            break;
                        case 1:
                            this.SearchCaseCondition.Address = InputText;
                            this.SearchCaseCondition.CaseId = null;
                            this.SearchCaseCondition.SponsorName = null;
                            this.SearchCaseCondition.BuyerName = null;
                            this.SearchCaseCondition.AgentPerson = null;
                            this.SearchCaseCondition.StartTime = null;
                            this.SearchCaseCondition.EndTime = null;
                            break;
                        case 2:
                            this.SearchCaseCondition.SponsorName = InputText;
                            this.SearchCaseCondition.CaseId = null;
                            this.SearchCaseCondition.Address = null;
                            this.SearchCaseCondition.BuyerName = null;
                            this.SearchCaseCondition.AgentPerson = null;
                            this.SearchCaseCondition.StartTime = null;
                            this.SearchCaseCondition.EndTime = null;
                            break;
                        case 3:
                            this.SearchCaseCondition.BuyerName = InputText;
                            this.SearchCaseCondition.CaseId = null;
                            this.SearchCaseCondition.Address = null;
                            this.SearchCaseCondition.SponsorName = null;
                            this.SearchCaseCondition.AgentPerson = null;
                            this.SearchCaseCondition.StartTime = null;
                            this.SearchCaseCondition.EndTime = null;
                            break;
                        case 4:
                            this.SearchCaseCondition.AgentPerson = InputText;
                            this.SearchCaseCondition.CaseId = null;
                            this.SearchCaseCondition.Address = null;
                            this.SearchCaseCondition.SponsorName = null;
                            this.SearchCaseCondition.BuyerName = null;
                            this.SearchCaseCondition.StartTime = null;
                            this.SearchCaseCondition.EndTime = null;
                            break;
                    }
                    NotifyPropertyChanged("SearchCaseCondition");
                }
            }
            this.eventAggregator.GetEvent<LoadPreCheckCaseListEvents>().Publish(this);
        }
        private ObservableCollection<EnumHelper> statusList;
        public ObservableCollection<EnumHelper> StatusList
        {
            get
            {
                return statusList;
            }
            set
            {
                if (statusList != value)
                {
                    statusList = value;
                    NotifyPropertyChanged("StatusList");
                }
            }
        }

        private List<string> conditionList;
        public List<string> ConditionList
        {
            get
            {
                return conditionList;
            }
            set
            {
                if (conditionList != value)
                {
                    conditionList = value;
                    NotifyPropertyChanged("ConditionList");
                }
            }
        }

        private ObservableCollection<CaseDetailInfoViewModel> caseList;
        public ObservableCollection<CaseDetailInfoViewModel> CaseList
        {
            get
            {
                return caseList;
            }
            set
            {
                if (caseList != value)
                {
                    caseList = value;
                    NotifyPropertyChanged("CaseList");
                }
            }
        }

        private int currentPageIndex;
        public int CurrentPageIndex
        {
            get
            {
                return currentPageIndex;
            }
            set
            {
                if (currentPageIndex != value)
                {
                    currentPageIndex = value;
                    NotifyPropertyChanged("CurrentPageIndex");
                    if(IsPageChanged)
                    {
                        this.eventAggregator.GetEvent<PageChangesEvents>().Publish(CurrentPageIndex);
                    }
                }
            }
        }

        private int totalPageCount;
        public int TotalPageCount
        {
            get
            {
                return totalPageCount;
            }
            set
            {
                if (totalPageCount != value)
                {
                    totalPageCount = value;
                    NotifyPropertyChanged("TotalPageCount");
                }
            }
        }

        private CaseDetailInfoViewModel searchCaseCondition;
        public CaseDetailInfoViewModel SearchCaseCondition
        {
            get
            {
                return searchCaseCondition;
            }
            set
            {
                if (searchCaseCondition != value)
                {
                    searchCaseCondition = value;
                    NotifyPropertyChanged("SearchCaseCondition");
                }
            }
        }
        private bool isBusy;
        public bool IsBusy
        {
            get
            {
                return isBusy;
            }
            set
            {
                if (isBusy != value)
                {
                    isBusy = value;
                    NotifyPropertyChanged("IsBusy");
                }
            }
        }

        private string inputText;
        public string InputText
        {
            get
            {
                return inputText;
            }
            set
            {
                if (inputText != value)
                {
                    inputText = value;
                    NotifyPropertyChanged("InputText");
                }
            }
        }
        private int caseSelectIndex;
        public int CaseSelectIndex
        {
            get
            {
                return caseSelectIndex;
            }
            set
            {
                if (caseSelectIndex != value)
                {
                    caseSelectIndex = value;
                    NotifyPropertyChanged("CaseSelectIndex");
                }
            }
        }

        private int selectIndex;
        public int SelectIndex
        {
            get
            {
                return selectIndex;
            }
            set
            {
                if (selectIndex != value)
                {
                    selectIndex = value;
                    NotifyPropertyChanged("SelectIndex");
                } 
            }
        }
        private bool isPageChanged;
        public bool IsPageChanged
        {
            get
            {
                return isPageChanged;
            }
            set
            {
                if (isPageChanged != value)
                {
                    isPageChanged = value;
                    NotifyPropertyChanged("IsPageChanged");
                }
            }
        }

    }
}
