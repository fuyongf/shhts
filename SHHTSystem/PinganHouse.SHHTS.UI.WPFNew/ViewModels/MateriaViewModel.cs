﻿using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.ServiceLocation;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPFNew.Common;
using PinganHouse.SHHTS.UI.WPFNew.Controls;
using PinganHouse.SHHTS.UI.WPFNew.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels
{
    public class MateriaViewModel : BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;

        public MateriaViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;
            this.UploadAttachmentCommand = new DelegateCommand<object>(this.OnUploadAttachmentCommand);
            this.UploadGaoPaiCommand = new DelegateCommand<object>(this.OnUploadGaoPaiCommand);
            this.CountLinkCommand = new DelegateCommand<object>(this.OnCountLinkCommand);
            MateriaCount = new ObservableCollection<string>();
        }

        public ICommand UploadAttachmentCommand { get; private set; }
        public ICommand UploadGaoPaiCommand { get; private set; }

        public ICommand CountLinkCommand { get; private set; }
        private void OnUploadAttachmentCommand(object model)
        {
            this.eventAggregator.GetEvent<UploadAttachmentEvents>().Publish(this.MateriaName);
        }
        private void OnUploadGaoPaiCommand(object model)
        {
            this.eventAggregator.GetEvent<GaoPaiTakePhotosEvents>().Publish(this.MateriaName);
        }
        private void OnCountLinkCommand(object model)
        {
            IsShowBorder = true;
            ObservableCollection<string> ss = model as ObservableCollection<string>;
            if(ss !=null)
            {
                List<string> countList = new List<string>();
                foreach(var item in ss)
                {
                    countList.Add(item);
                }
                var window = new ImageBrowse(countList);
                window.ShowDialog();
                if(window.IsDelete)
                {
                    this.eventAggregator.GetEvent<PreCheckRefreshedUploadAttachementEvents>().Publish(this.MateriaName.ToString());
                }

            }
        }
        /// <summary>
        /// 用字符串型，是在页面不显示附件数量为0的项
        /// </summary>
        private ObservableCollection<string> mateiraCount;
        public ObservableCollection<string> MateriaCount
        {
            get
            {
                return mateiraCount;
            }
            set
            {
                if (mateiraCount != value)
                {
                    mateiraCount = value;
                    NotifyPropertyChanged("MateriaCount");
                }
            }
        }

        /// <summary>
        /// 材料名称
        /// </summary>
        private AttachmentType materiaName;
        public AttachmentType MateriaName
        {
            get
            {
                return materiaName;
            }
            set
            {
                if (materiaName != value)
                {
                    materiaName = value;
                    NotifyPropertyChanged("MaterialName");
                }
            }
        }

        /// <summary>
        /// 是否有该项内容
        /// </summary>
        public bool IsShowItem { get; set; }

        private bool isShowBorder;
        public bool IsShowBorder
        {
            get
            {
                return isShowBorder;
            }
            set
            {
                if (isShowBorder != value)
                {
                    isShowBorder = value;
                    NotifyPropertyChanged("IsShowBorder");
                }
            }
        }

    }
}
