﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.PubSubEvents;
using PinganHouse.SHHTS.UI.WPFNew.Common;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels
{
    public class WindowTitleViewModel:BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;

        public WindowTitleViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;

            this.BackToMainViewCommand = new DelegateCommand<object>(this.OnBackToMainViewCommand);
            this.LogoutViewCommand = new DelegateCommand<object>(this.OnLogoutViewCommand);
        }
       
        private string userName;
        public string UserName
        {
            get
            {
                return userName;
            }
            set
            {
                if (userName != value)
                {
                    userName = value;
                    NotifyPropertyChanged("UserName");
                }
            }
        }

        public ICommand BackToMainViewCommand { get; private set; }
        private void OnBackToMainViewCommand(object model)
        {
            this.eventAggregator.GetEvent<NavigateToMainListEvents>().Publish(model);
        }
        public ICommand LogoutViewCommand { get; private set; }
        private void OnLogoutViewCommand(object model)
        {
            this.eventAggregator.GetEvent<LogOutEvents>().Publish(model);
        }
    }
}
