﻿using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.ServiceLocation;

using PinganHouse.SHHTS.DataTransferObjects;
using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.RemoteServiceProxy;

using PinganHouse.SHHTS.UI.WPFNew.Common;
using PinganHouse.SHHTS.UI.WPFNew.Common.CommonClass;
using PinganHouse.SHHTS.UI.WPFNew.Common.Dialog;
using PinganHouse.SHHTS.UI.WPFNew.Controls;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing.Printing;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels
{
    public class PreCheckAddCaseViewModel : BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;

        public PreCheckAddCaseViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;

            this.BackToMainViewCommand = new DelegateCommand<object>(this.OnBackToMainViewCommand);
            this.LogoutViewCommand = new DelegateCommand<object>(this.OnLogoutViewCommand);
            this.AgentReadCommand = new DelegateCommand<object>(this.OnAgentReadCommand);
            this.AgentCheckCommand = new DelegateCommand<object>(this.OnAgentCheckCommand);
            this.SaveCommand = new DelegateCommand<object>(this.OnSaveCommand);
            this.CreateCaseCommand = new DelegateCommand<object>(this.OnCreateCaseCommand);
            this.CreateCaseAndPrintBarcodeCommand = new DelegateCommand<object>(this.OnCreateCaseAndPrintBarcodeCommand);
            this.CancelCommand = new DelegateCommand<object>(this.OnCancelCommand);
            this.LoadPreCheckCaseListCommand = new DelegateCommand<object>(this.OnLoadPreCheckCaseListCommand);
            this.SearchListPageCommand = new DelegateCommand<object>(this.OnSearchListPageCommand);

            BuyerInfoList = new ObservableCollection<IdInfoModel>();
            BuyerInfoList.CollectionChanged += BuyerInfoList_CollectionChanged;
            SellerInfoList = new ObservableCollection<IdInfoModel>();
            SellerInfoList.CollectionChanged += SellerInfoList_CollectionChanged;
            IdCardSellerModel = new IdInfoModel();
            IdentitySellerList = EnumHelper.InitCertificateTypeToCombobox();
            IdCardBuyerModel = new IdInfoModel();
            IdentityBuyerList = EnumHelper.InitCertificateTypeToCombobox();
            IndexBuyerSelect = 0;
            IndexSellerSelect = 0;

            ListSellerModel = new ObservableCollection<UserIdentityModel>();
            ListBuyerModel = new ObservableCollection<UserIdentityModel>();
            //SelectBuyerUser = new UserIdentityModel();
            //SelectSellerUser = new UserIdentityModel();
            AgentStaff = new AgentStaff();
            CompanyAgent = new AgentCompany();

            ShowCase = new ShowCaseModel();

            #region 获得本机所有打印机
            var printers = PrinterSettings.InstalledPrinters;//获取本机上的所有打印机
            //PrintModel pm1 = new PrintModel();
            //pm1.PrintNo = -1;
            //pm1.PrintName = "请选择打印机";
            //_ListPrint.Add(pm1);
            PrintModel pm = new PrintModel();
            pm.PrintNo = 0;
            pm.PrintName = "未分配";
            _ListPrint.Add(pm);

            for (int i = 0; i < printers.Count; i++)
            {
                PrintModel print = new PrintModel();
                print.PrintNo = i + 1;
                print.PrintName = printers[i].ToString();
                _ListPrint.Add(print);
            }
            #endregion
           
        }

        

        private void OnLoadPreCheckCaseListCommand(object obj)
        {
            this.eventAggregator.GetEvent<LoadPreCheckCaseListEvents>().Publish(ServiceLocator.Current.GetInstance<SearchListPageViewModel>());
        }

        #region 属性
        private List<EnumHelper> caseSourceType;
        public List<EnumHelper> CaseSourceType
        {
            get { return caseSourceType; }
            set
            {
                if (caseSourceType != value)
                {
                    caseSourceType = value;
                    NotifyPropertyChanged("CaseSourceType");
                }
            }
        }

        private int selectCaseType;
        public int SelectCaseType
        {
            get { return selectCaseType; }
            set
            {
                if (selectCaseType != value)
                {
                    selectCaseType = value;
                    NotifyPropertyChanged("SelectCaseType");
                }
            }
        }
        /// <summary>
        /// 打印机列表
        /// </summary>
        private List<PrintModel> _ListPrint = new List<PrintModel>();

        /// <summary>
        /// 打印机名字
        /// </summary>
        private string _ReceiptPrintName;

        /// <summary>
        /// 生成条形码数
        /// </summary>
        private int _BarCodeNum;

        private string tbTenementContractWord;

        public string TbTenementContractWord
        {
            get { return tbTenementContractWord; }
            set
            {
                if (tbTenementContractWord != value)
                {
                    tbTenementContractWord = value;
                    NotifyPropertyChanged("TbTenementContractWord");
                }
            }
        }

        private string tbTenementContract;

        public string TbTenementContract
        {
            get { return tbTenementContract; }
            set
            {
                if (tbTenementContract != value)
                {
                    tbTenementContract = value;
                    NotifyPropertyChanged("TbTenementContract");
                }
            }
        }

        private string tbTenementAddress;

        public string TbTenementAddress
        {
            get { return tbTenementAddress; }
            set
            {
                if (tbTenementAddress != value)
                {
                    tbTenementAddress = value;
                    NotifyPropertyChanged("TbTenementAddress");
                }
            }
        }

        private string tblShowInfo;

        public string TblShowInfo
        {
            get { return tblShowInfo; }
            set
            {
                if (tblShowInfo != value)
                {
                    tblShowInfo = value;
                    NotifyPropertyChanged("TblShowInfo");
                }
            }
        }

        private UserIdentityModel identityInfo;

        public UserIdentityModel IdentityInfo
        {
            get { return identityInfo; }
            set
            {
                if (identityInfo != value)
                {
                    identityInfo = value;
                    NotifyPropertyChanged("IdentityInfo");
                }
            }
        }

        private AgentStaff agentStaff;

        public AgentStaff AgentStaff
        {
            get { return agentStaff; }
            set
            {
                if (agentStaff != value)
                {
                    agentStaff = value;
                    NotifyPropertyChanged("AgentStaff");
                }
            }
        }

        private int sellerTotal;

        public int SellerTotal
        {
            get { return sellerTotal; }
            set
            {
                if (sellerTotal != value)
                {
                    sellerTotal = value;
                    NotifyPropertyChanged("SellerTotal");
                }
            }
        }
        private int buyerTotal;

        public int BuyerTotal
        {
            get { return buyerTotal; }
            set
            {
                if (buyerTotal != value)
                {
                    buyerTotal = value;
                    NotifyPropertyChanged("BuyerTotal");
                }
            }
        }

        /// <summary>
        /// 类型详情Model
        /// </summary>
        private IdInfoModel idCardBuyerModel;

        public IdInfoModel IdCardBuyerModel
        {
            get { return idCardBuyerModel; }
            set
            {
                if (idCardBuyerModel != value)
                {
                    idCardBuyerModel = value;
                    NotifyPropertyChanged("IdCardBuyerModel");
                }
            }
        }

        private IdInfoModel idCardSellerModel;

        public IdInfoModel IdCardSellerModel
        {
            get { return idCardSellerModel; }
            set
            {
                if (idCardSellerModel != value)
                {
                    idCardSellerModel = value;
                    NotifyPropertyChanged("IdCardSellerModel");
                }
            }
        }

        private ObservableCollection<IdInfoModel> buyerInfoList;
        public ObservableCollection<IdInfoModel> BuyerInfoList
        {
            get
            {
                return buyerInfoList;
            }
            set
            {
                if (buyerInfoList != value)
                {
                    buyerInfoList = value;
                    NotifyPropertyChanged("BuyerInfoList");
                }
            }
        }


        void BuyerInfoList_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            BuyerTotal = buyerInfoList.Count;
            NotifyPropertyChanged("BuyerTotal");
            NotifyPropertyChanged("BuyerInfoList");
        }
        private ObservableCollection<IdInfoModel> sellerInfoList;
        public ObservableCollection<IdInfoModel> SellerInfoList
        {
            get
            {
                return sellerInfoList;
            }
            set
            {
                if (sellerInfoList != value)
                {
                    sellerInfoList = value;
                    NotifyPropertyChanged("SellerInfoList");
                    SellerTotal = sellerInfoList.Count;
                    NotifyPropertyChanged("SellerTotal");
                }
            }
        }
        void SellerInfoList_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            SellerTotal = sellerInfoList.Count;
            NotifyPropertyChanged("SellerTotal");
        }
        private IdInfoModel buySelectCurrentItem;
        public IdInfoModel BuySelectCurrentItem
        {
            get
            {
                return buySelectCurrentItem;
            }
            set
            {
                if (buySelectCurrentItem != value)
                {
                    buySelectCurrentItem = value;
                    NotifyPropertyChanged("BuySelectCurrentItem");
                    if (buySelectCurrentItem != null)
                    {
                        //要深复制
                        IdCardBuyerModel = new IdInfoModel()
                        {
                            IdName=buySelectCurrentItem.IdName,
                            IdSex=buySelectCurrentItem.IdSex,
                            IdNation = buySelectCurrentItem.IdNation,
                            IdBirth = buySelectCurrentItem.IdBirth,
                            IdAddress = buySelectCurrentItem.IdAddress,
                            IdNum = buySelectCurrentItem.IdNum,
                            IdAgent = buySelectCurrentItem.IdAgent,                       
                            Photo = buySelectCurrentItem.Photo,
                            Bitmap = buySelectCurrentItem.Bitmap,
                            IsTrusted = buySelectCurrentItem.IsTrusted,
                            Tag = buySelectCurrentItem.Tag,
                            TagObject = buySelectCurrentItem.TagObject,
                            Male = buySelectCurrentItem.Male,
                            Female = buySelectCurrentItem.Female,
                            Nationality = buySelectCurrentItem.Nationality,
                            Section = buySelectCurrentItem.Section,
                            CertificateType = buySelectCurrentItem.CertificateType,
                            IsCancel = true,//显示撤销按钮
                            IsUpdate = true//修改数据
                        };
                        //if (!IdCardBuyerModel.IsTrusted &&
                        //    IdCardBuyerModel.CertificateType.Equals(CertificateType.IdCard))
                        //    IndexBuyerSelect = -1;
                        //else
                        IndexBuyerSelect = GetIndexByCertificateType(buySelectCurrentItem.CertificateType);
                        IdCardBuyerModel.EffectiveDate = buySelectCurrentItem.EffectiveDate;                                               
                        if( IdCardBuyerModel.EffectiveDate!=null&& buySelectCurrentItem.ExpiryDate==null  )
                                IdCardBuyerModel.ExpiryDate="长期";
                        else
                                IdCardBuyerModel.ExpiryDate=  buySelectCurrentItem.ExpiryDate;
                        NotifyPropertyChanged("IdCardBuyerModel");
                        IsBuyerForbid = false;
                        NotifyPropertyChanged("IsBuyerForbid");
                        //选中证书类型
                
                    }
                    else
                    {
                        if(IdCardBuyerModel.IsUpdate)
                        {
                            IsBuyerForbid = false;
                            NotifyPropertyChanged("IsBuyerForbid");
                        }
                        else
                        {
                            IsBuyerForbid = true;
                            NotifyPropertyChanged("IsBuyerForbid");
                        }
                    }
                }
            }
        }
        /// <summary>
        /// 根据证书类型获取相应的Index
        /// </summary>
        /// <param name="certificateType"></param>
        /// <returns></returns>
        private int GetIndexByCertificateType(CertificateType certificateType)
        {
            int index = -1;
            switch (certificateType)
            {
                case CertificateType.IdCard:
                    index = 0;
                    break;
                case CertificateType.Passport:
                    index = 1;
                    break;
                case CertificateType.HKMacaoIdCard:
                    index = 2;
                    break;
                case CertificateType.BirthCertificate:
                    index = 3;
                    break;
                case CertificateType.MacaoPermit:
                    index = 4;
                    break;
                case CertificateType.TaiwanPermit:
                    index = 5;
                    break;
                case CertificateType.CertificateOfOfficers:
                    index = 6;
                    break;
                case CertificateType.ResidenceBooklet:
                    index = 7;
                    break;
                default:
                    index = 0;
                    break;
            }
            return index;
        }

        private IdInfoModel sellSelectCurrentItem;
        public IdInfoModel SellSelectCurrentItem
        {
            get
            {
                return sellSelectCurrentItem;
            }
            set
            {
                if (sellSelectCurrentItem != value)
                {
                    sellSelectCurrentItem = value;
                    NotifyPropertyChanged("SellSelectCurrentItem");
                    if (sellSelectCurrentItem != null)
                    {
                        //要深复制
                        IdCardSellerModel = new IdInfoModel()
                        {
                            IdName = sellSelectCurrentItem.IdName,
                            IdSex = sellSelectCurrentItem.IdSex,
                            IdNation = sellSelectCurrentItem.IdNation,
                            IdBirth = sellSelectCurrentItem.IdBirth,
                            IdAddress = sellSelectCurrentItem.IdAddress,
                            IdNum = sellSelectCurrentItem.IdNum,
                            IdAgent = sellSelectCurrentItem.IdAgent,
                            
                            Photo = sellSelectCurrentItem.Photo,
                            Bitmap = sellSelectCurrentItem.Bitmap,
                            IsTrusted = sellSelectCurrentItem.IsTrusted,
                            Tag = sellSelectCurrentItem.Tag,
                            TagObject = sellSelectCurrentItem.TagObject,
                            Male = sellSelectCurrentItem.Male,
                            Female = sellSelectCurrentItem.Female,
                            Nationality = sellSelectCurrentItem.Nationality,
                            Section = sellSelectCurrentItem.Section,
                            CertificateType = sellSelectCurrentItem.CertificateType,
                            IsCancel = true,//显示撤销按钮
                            IsUpdate = true//修改数据
                        };
                        IdCardSellerModel.EffectiveDate = sellSelectCurrentItem.EffectiveDate;
                        if (IdCardSellerModel.EffectiveDate != null && sellSelectCurrentItem.ExpiryDate == null)
                            IdCardSellerModel.ExpiryDate = "长期";
                        else
                            IdCardSellerModel.ExpiryDate = sellSelectCurrentItem.ExpiryDate;
                    NotifyPropertyChanged("IdCardSellerModel");
                        IsSellerForbid = false;
                        NotifyPropertyChanged("IsSellerForbid");
                        //选中证书类型
                        IndexSellerSelect = GetIndexByCertificateType(sellSelectCurrentItem.CertificateType);
                    }
                    else
                    {
                        if (IdCardSellerModel.IsUpdate)
                        {
                            IsSellerForbid = false;
                            NotifyPropertyChanged("IsSellerForbid");
                        }
                        else
                        {
                            IsSellerForbid = true;
                            NotifyPropertyChanged("IsSellerForbid");
                        }
                    }
                }
            }
        }


        private ObservableCollection<UserIdentityModel> listSellerModel;

        public ObservableCollection<UserIdentityModel> ListSellerModel
        {
            get { return listSellerModel; }
            set
            {
                if (listSellerModel != value)
                {
                    listSellerModel = value;
                    NotifyPropertyChanged("ListSellerModel");
                }             
            }
        }    

        private UserIdentityModel selectSellerUser;

        public UserIdentityModel SelectSellerUser
        {
            get { return selectSellerUser; }
            set
            {
                if (selectSellerUser != value)
                {
                    selectSellerUser = value;
                    if (selectSellerUser == null)
                        return;                   
                    IndexSellerSelect = (int)SelectSellerUser.CertificateType;
                    if (SelectSellerUser.Tag != null)
                        IsSellerForbid = false;
                    IdCardSellerModel = new IdInfoModel();
                    if (SelectSellerUser.IdentityNo != null)
                    {
                        IdCardSellerModel.IsCancel = true;              
                    }
                    IdCardSellerModel.IdName = selectSellerUser.Name;
                    IdCardSellerModel.IdAddress = selectSellerUser.Address;
                    IdCardSellerModel.IdAgent = selectSellerUser.VisaAgency;

                    if (SelectSellerUser.Photo != null)
                    {
                        IdCardSellerModel.Bitmap = ByteToBmp(SelectSellerUser.Photo);
                    }

                    if (selectSellerUser.Birthday == null)
                    {
                        IdCardSellerModel.IdBirth = null;
                    }
                    else
                    {
                        var birth = (DateTime)selectSellerUser.Birthday;
                        IdCardSellerModel.IdBirth = birth.ToShortDateString();
                    }

                    IdCardSellerModel.IdNation = EnumHelper.GetEnumDesc(selectSellerUser.Nation);
                    if (IdCardSellerModel.IdNation == "未知")
                        IdCardSellerModel.IdNation = null;

                    IdCardSellerModel.IdNum = selectSellerUser.IdentityNo;
                    switch (selectSellerUser.Gender)
                    {
                        case Gender.Female:
                            IdCardSellerModel.IdSex = "女";
                            IdCardSellerModel.Female = true;
                            break;
                        case Gender.Male:
                            IdCardSellerModel.IdSex = "男";
                            IdCardSellerModel.Male = true;
                            break;
                        default:
                            IdCardSellerModel.IdSex = "";
                            IdCardSellerModel.Male = true;
                            break;
                    }
                    if (selectSellerUser.EffectiveDate == null)
                    {
                        idCardSellerModel.EffectiveDate = null;
                    }
                    else
                    {
                        var effectiveDatetime = (DateTime)selectSellerUser.EffectiveDate;
                        idCardSellerModel.EffectiveDate = effectiveDatetime.ToShortDateString();
                    }
                    if (selectSellerUser.EffectiveDate == null && selectSellerUser.ExpiryDate == null)
                    {
                        idCardSellerModel.ExpiryDate = null;
                    }
                    else if (selectSellerUser.EffectiveDate != null && selectSellerUser.ExpiryDate == null)
                    {
                        idCardSellerModel.ExpiryDate = "长期";
                    }
                    else
                    {
                        if (selectSellerUser.ExpiryDate != null)
                        {
                            var expiryDateTime = (DateTime)selectSellerUser.ExpiryDate;
                            idCardSellerModel.ExpiryDate = expiryDateTime.ToShortDateString();
                        }
                    }
                    IdCardSellerModel.Nationality = selectSellerUser.Nationality;
                    IdCardSellerModel.IsTrusted = selectSellerUser.IsTrusted;
                    IdCardSellerModel.CertificateType = SelectSellerUser.CertificateType;
                    IdCardSellerModel.Tag = SelectSellerUser.Tag == null ? null : SelectSellerUser.Tag.ToString();
                  
                    NotifyPropertyChanged("SelectSellerUser");
                }
            }
        }

        private ObservableCollection<UserIdentityModel> listBuyerModel;

        public ObservableCollection<UserIdentityModel> ListBuyerModel
        {
            get { return listBuyerModel; }
            set
            {
                if (listBuyerModel != value)
                {
                    listBuyerModel = value;
                    NotifyPropertyChanged("ListBuyerModel");
                }
                
            }
        }

        private UserIdentityModel selectBuyerUser;

        //public UserIdentityModel SelectBuyerUser
        //{
        //    get { return selectBuyerUser; }
        //    set
        //    {
        //        if (selectBuyerUser != value)
        //        {
        //            //model转换
        //            selectBuyerUser = value;
        //            if (selectBuyerUser == null)
        //                return;
        //            IndexBuyerSelect = (int)SelectBuyerUser.CertificateType;
        //            if (SelectBuyerUser.Tag != null)
        //                IsBuyerForbid = false;
        //            IdCardBuyerModel = new IdInfoModel
        //            {
        //                IdName = selectBuyerUser.Name,                     
        //                IdAddress = selectBuyerUser.Address,
        //                IdAgent = selectBuyerUser.VisaAgency
        //            };
        //            if (SelectBuyerUser.Photo!=null)
        //            {
        //                IdCardBuyerModel.Bitmap = ByteToBmp(SelectBuyerUser.Photo);
        //            }
                  
 
        //            if (SelectBuyerUser.IdentityNo != null)
        //            {
        //                IdCardBuyerModel.IsCancel = true;                   
        //            }

        //            if (selectBuyerUser.Birthday == null)
        //            {
        //                IdCardBuyerModel.IdBirth = null;
        //            }
        //            else
        //            {
        //                var birth = (DateTime) selectBuyerUser.Birthday;
        //                IdCardBuyerModel.IdBirth = birth.ToShortDateString();
        //            }
                      
        //            IdCardBuyerModel.IdNation = EnumHelper.GetEnumDesc(selectBuyerUser.Nation);
        //            if (IdCardBuyerModel.IdNation == "未知")
        //                IdCardBuyerModel.IdNation = null;

        //            IdCardBuyerModel.IdNum = selectBuyerUser.IdentityNo;
        //            switch (selectBuyerUser.Gender)
        //            {
        //                case Gender.Female:
        //                    IdCardBuyerModel.IdSex = "女";
        //                    IdCardBuyerModel.Female = true;
        //                    break;
        //                case Gender.Male:
        //                    IdCardBuyerModel.IdSex = "男";
        //                    IdCardBuyerModel.Male=true;
        //                    break;
        //                default:
        //                    IdCardBuyerModel.IdSex = "";
        //                    IdCardBuyerModel.Male = true;
        //                    break;
        //            }
                 
        //            if (selectBuyerUser.EffectiveDate == null)
        //            {
        //                idCardBuyerModel.EffectiveDate = null;
        //            }
        //            else
        //            {
        //                var effectiveDatetime = (DateTime)selectBuyerUser.EffectiveDate;
        //                idCardBuyerModel.EffectiveDate = effectiveDatetime.ToShortDateString();
        //            }
                    
        //            if (selectBuyerUser.EffectiveDate == null && selectBuyerUser.ExpiryDate == null)
        //            {
        //                IdCardBuyerModel.ExpiryDate = null;                        
        //            }
        //            else if (selectBuyerUser.EffectiveDate != null && selectBuyerUser.ExpiryDate == null)
        //            {
        //                IdCardBuyerModel.ExpiryDate = "长期";
        //            }
        //            else
        //            {
        //                if (selectBuyerUser.ExpiryDate != null)
        //                {
        //                    var expiryDateTime =(DateTime) selectBuyerUser.ExpiryDate;
        //                    IdCardBuyerModel.ExpiryDate = expiryDateTime.ToShortDateString();
        //                }
        //                //IdCardBuyerModel.ExpiryDate = (selectBuyerUser.ExpiryDate).
        //            }

        //            IdCardBuyerModel.Nationality = selectBuyerUser.Nationality;
        //            IdCardBuyerModel.IsTrusted = selectBuyerUser.IsTrusted;
        //            IdCardBuyerModel.CertificateType = SelectBuyerUser.CertificateType;
        //            IdCardBuyerModel.Tag = SelectBuyerUser.Tag == null ? null : SelectBuyerUser.Tag.ToString();
                   
        //            NotifyPropertyChanged("SelectBuyerUser");
        //        }
        //    }
        //}

        private BitmapImage ByteToBmp(byte[] bytes)
        {
            BitmapImage bitmap = new BitmapImage();
            try
            {              
                MemoryStream ms1 = new MemoryStream(bytes);
                bitmap.BeginInit();
                bitmap.StreamSource = ms1;
                bitmap.EndInit();
              
            }
            catch (Exception e)
            {
                MessageBox.Show("图像转化失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return bitmap;
        }
        /// <summary>
        /// 记录经纪人
        /// </summary>
        private AgentStaff staffAgent;

        public AgentStaff StaffAgent
        {
            get { return staffAgent; }
            set
            {
                if (staffAgent != value)
                {
                    staffAgent = value;
                    NotifyPropertyChanged("StaffAgent");
                }
            }
        }

        /// <summary>
        /// 记录经纪公司
        /// </summary>
        private AgentCompany companyAgent;

        public AgentCompany CompanyAgent
        {
            get { return companyAgent; }
            set
            {
                if (companyAgent != value)
                {
                    companyAgent = value;
                    NotifyPropertyChanged("CompanyAgent");
                }
            }
        }

        /// <summary>
        /// 证件类型
        /// </summary>
        private List<EnumHelper> identitySelleryList;

        public List<EnumHelper> IdentitySellerList
        {
            get { return identitySelleryList; }
            set
            {
                if (identitySelleryList != value)
                {
                    identitySelleryList = value;
                    NotifyPropertyChanged("IdentitySellerList");
                }
            }
        }
        /// <summary>
        ///选择证件类型
        /// </summary>
        private EnumHelper identitySellerSelected;

        public EnumHelper IdentitySellerSelected
        {
            get { return identitySellerSelected; }
            set
            {
                if (identitySellerSelected != value)
                {
                    identitySellerSelected = value;
                    NotifyPropertyChanged("IdentitySellerSelected");
                }
            }
        }
        /// <summary>
        ///选择证件类型Index
        /// </summary>
        private int indexSellerSelect;

        public int IndexSellerSelect
        {
            get { return indexSellerSelect; }
            set
            {
                if (indexSellerSelect != value)
                {
                    indexSellerSelect = value;
                    NotifyPropertyChanged("IndexSellerSelect");
                }
            }
        }
        /// <summary>
        /// 证件类型
        /// </summary>
        private List<EnumHelper> identityBuyerList;

        public List<EnumHelper> IdentityBuyerList
        {
            get { return identityBuyerList; }
            set
            {
                if (identityBuyerList != value)
                {
                    identityBuyerList = value;
                    NotifyPropertyChanged("IdentityBuyerList");
                }
            }
        }
        /// <summary>
        ///选择证件类型
        /// </summary>
        private EnumHelper identityBuyerSelected;

        public EnumHelper IdentityBuyerSelected
        {
            get { return identityBuyerSelected; }
            set
            {
                if (identityBuyerSelected != value)
                {
                    identityBuyerSelected = value;
                    NotifyPropertyChanged("IdentityBuyerSelected");
                }
            }
        }
        /// <summary>
        ///选择证件类型Index
        /// </summary>
        private int indexBuyerSelect;
           
        public int IndexBuyerSelect
        {
            get { return indexBuyerSelect; }
            set
            {
                if (indexBuyerSelect != value)
                {
                    indexBuyerSelect = value;
                    NotifyPropertyChanged("IndexBuyerSelect");
                }
            }
        }

        private bool isBuyerForbid =true;

        public bool IsBuyerForbid
        {
            get { return isBuyerForbid; }
            set
            {
                if (isBuyerForbid != value)
                {
                    isBuyerForbid = value;
                    NotifyPropertyChanged("IsBuyerForbid");
                }
            }
        }
        private bool isSellerForbid = true;

        public bool IsSellerForbid 
        {
            get { return isSellerForbid; }
            set
            {
                if (isSellerForbid != value)
                {
                    isSellerForbid = value;
                    NotifyPropertyChanged("IsSellerForbid");
                }
            }
        }

        private CaseStatus? currentCaseStatus;
        public CaseStatus? CurrentCaseStatus
        {
            get
            {
                return currentCaseStatus;
            }
            set
            {
                if (currentCaseStatus != value)
                {
                    currentCaseStatus = value;
                    NotifyPropertyChanged("CurrentCaseStatus");
                }
            }
        }

        private CustomerType customerInfoType;
        public CustomerType CustomerInfoType
        {
            get
            {
                return customerInfoType;
            }
            set
            {
                if (customerInfoType != value)
                {
                    customerInfoType = value;
                    NotifyPropertyChanged("CustomerInfoType");
                }
            }
        }

        /// <summary>
        /// 记录经纪公司
        /// </summary>
        private AgentCompany agentCompany = new AgentCompany();
        #endregion

         #region 基本信息（查看案件）属性
        private ShowCaseModel showCase;
        public ShowCaseModel ShowCase
        {
            get { return showCase; }
            set
            {
                if (showCase != value)
                {
                    showCase = value;
                    NotifyPropertyChanged("ShowCase");
                }
            }
        }
        #endregion
       
        public ICommand LoadPreCheckCaseListCommand { get; private set; }
        public ICommand BackToMainViewCommand { get; private set; }

        private void OnBackToMainViewCommand(object model)
        {
            this.eventAggregator.GetEvent<NavigateToMainListEvents>().Publish(model);
        }

        public ICommand LogoutViewCommand { get; private set; }

        private void OnLogoutViewCommand(object model)
        {
            this.eventAggregator.GetEvent<NavigateToMainListEvents>().Publish(model);
        }
     
        /// <summary>
        /// 读取身份证
        /// </summary>
        public ICommand AgentReadCommand { get; private set; }

        private void OnAgentReadCommand(object model)
        {
            bool flag;
            var idCard = new IdCard(Convert.ToInt32(model), out flag);
            if (!flag)
                return;
            idCard.Show();
            IdentityInfo = idCard.IdentityInfo;
            if (IdentityInfo == null)
                return;
            TblShowInfo = string.Empty;
            GetAgentValue();
        }
        private void GetAgentValue()
        {
            int totalCount = 0;
            var idNo = IdentityInfo.IdentityNo;

            IList<AgentStaff> agentStaffList = new List<AgentStaff>();
            if (idNo == null)
            {
                return;
            }
            agentStaffList = AgentServiceProxy.GetAgentStaffs(1, 10000, out totalCount, null, null, null, idNo);
            if (agentStaffList.Count > 0)
            {
                for (int i = 0; i < agentStaffList.Count; i++)
                {
                    agentCompany = AgentServiceProxy.GetAgentCompany(agentStaffList[i].AgentCompanySysNo);
                    if (agentCompany != null)
                    {                      
                        TblShowInfo = string.Format("姓名   {0}      所属门店   {1}", agentStaffList[i].RealName, agentCompany.CompanyName);
                    }
                }
            }
            else
            {
                MessageBox.Show("没有符合该身份证号的经纪人！");
                return;
            }

        }
        /// <summary>
        /// 模糊查询经纪人
        /// </summary>
        public ICommand AgentCheckCommand { get; private set; }

        private void OnAgentCheckCommand(object model)
        {
            try
            {
                var agentCheck = new AgentQueryDialog();
                agentCheck.ShowDialog();
                AgentStaff = agentCheck.AgentStaff;
                var agentCompany = AgentServiceProxy.GetAgentCompany(AgentStaff.AgentCompanySysNo);
                if (agentCompany == null)
                    return;
                TblShowInfo = string.Empty;
                TblShowInfo = string.Format("姓名   {0}      所属门店   {1}", AgentStaff.RealName, agentCompany.CompanyName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }

        /// <summary>
        /// 保存
        /// </summary>
        public ICommand SaveCommand { get; private set; }

        private void OnSaveCommand(object model)
        {
            try
            {
                CustomerType customerType = (CustomerType) Enum.Parse(typeof (CustomerType), Convert.ToString(model));
                switch (customerType)
                {
                    case CustomerType.Buyer:
                        CustomerInfoType = CustomerType.Buyer;
                        var saveBuyerResult = SaveCustomer(IdCardBuyerModel, BuyerInfoList, IdentityBuyerSelected, CustomerType.Buyer);
                        if (!saveBuyerResult)
                        {                           
                            return;
                        }
                        //IdCardBuyerModel = new IdInfoModel();
                        //IndexBuyerSelect = 0;//证件类型默认第一项
                        break;
                    case CustomerType.Seller:
                        CustomerInfoType = CustomerType.Seller;
                        var saveSellerResult = SaveCustomer(IdCardSellerModel, SellerInfoList, IdentitySellerSelected, CustomerType.Seller);
                        if (!saveSellerResult)
                        {                          
                            return;
                        }
                        //IdCardSellerModel = new IdInfoModel();
                        //IndexSellerSelect = 0;//证件类型默认第一项
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }

        /// <summary>
        /// 取消
        /// </summary>
        public ICommand CancelCommand { get; private set; }

        private void OnCancelCommand(object model)
        {
            //跳转列表页面
            eventAggregator.GetEvent<NavigateToMainListEvents>().Publish(this);    
        }
        /// <summary>
        /// 生成案件
        /// </summary>
        public ICommand CreateCaseCommand { get; private set; }

        private void OnCreateCaseCommand(object model)
        {
            if (!CheckDataValid())
            {
                return;
            }
            try
            {
                this.eventAggregator.GetEvent<CreateNewCaseEvents>().Publish(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show("生成案件系统发生异常：" + ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 生成案件
        /// </summary>
        public ICommand CreateCaseAndPrintBarcodeCommand { get; private set; }

        private void OnCreateCaseAndPrintBarcodeCommand(object model)
        {
            if (!CheckDataValid())
            {
                return;
            }
            try
            {
                this.eventAggregator.GetEvent<CreateNewCaseAndPrintBarcardEvents>().Publish(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show("生成案件系统发生异常：" + ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            }
        /// <summary>
        /// 取消
        /// </summary>
        public ICommand SearchListPageCommand { get; private set; }

        private void OnSearchListPageCommand(object model)
        {
            //跳转列表页面
            this.eventAggregator.GetEvent<SearchListPageEvents>().Publish(this);
        }
        #region 保存暂时折叠

        /// <summary>
        /// 保存卖家或买家信息
        /// </summary>
        /// <param name="idIfos">卖家或买家信息详情</param>
        private bool SaveCustomer(IdInfoModel idInfos, ObservableCollection<IdInfoModel> userIdentityModel, EnumHelper IdentitySelected, CustomerType customerType)
        {
            try
            {
                if(idInfos ==null)
                {
                    idInfos = new IdInfoModel();
                    idInfos.IsCancel = false;
                }
                //000 获取证件类型
                var certificateType = idInfos.CertificateType;

                //001检查数据的合法性
                var checkDataResult = CheckData(certificateType, idInfos);
                if (!checkDataResult) { return false; }
                //存在用户，更新
                if(idInfos.IsUpdate)
                {
                    this.eventAggregator.GetEvent<UpdateCustomerInfoEvents>().Publish(this);
                }
                //新用户，添加
                else
                {
                    //002检查证件是否已经存在,客户端也需要校验              
                    var checkResult = CheckIdCardNoInItemSource(userIdentityModel, idInfos.IdNum, idInfos.CertificateType);
                    if (!checkResult)
                        return false;
                    this.eventAggregator.GetEvent<AddCustomerInfoEvents>().Publish(this);
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
        }

        private bool CheckData(CertificateType certificate, IdInfoModel idInfo)
        {         
            //根据具体需求来添加条件
            if (string.IsNullOrEmpty(idInfo.IdName))
            {
                MessageBox.Show("姓名不能为空.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (string.IsNullOrEmpty(idInfo.IdNum))
            {
                MessageBox.Show("证件编号不能为空.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            var result = true;
            switch (certificate)
            {               
                case CertificateType.IdCard:
                    result=ValidationCommon.IsIdMatch(idInfo.IdNum);
                    if (!result)
                    {
                        MessageBox.Show("身份证号格式不正确", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return false; 
                    }                      
                    break;
                case CertificateType.HKMacaoIdCard:
                   result = ValidationCommon.IsHkMacaoIdMatch(idInfo.IdNum);
                    if (!result)
                    {
                        MessageBox.Show("身份证号格式不正确", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return false;
                    }
                    break;
                case CertificateType.MacaoPermit:
                    result = ValidationCommon.IsHkMacaoPermitMatch(idInfo.IdNum);
                    if (!result)
                    {
                        MessageBox.Show("证件号码格式不正确", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return false;
                    }
                    break;
                case CertificateType.TaiwanPermit:
                    result = ValidationCommon.IsTaiwanPermitMatch(idInfo.IdNum);
                    if (!result)
                    {
                        MessageBox.Show("证件号码格式不正确", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return false;
                    }
                    break;
                case CertificateType.CertificateOfOfficers :
                    result = ValidationCommon.IsOfficerMatch (idInfo.IdNum);
                    if (!result)
                    {
                        MessageBox.Show("身份证号格式不正确", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return false;
                    }
                    break;
                case CertificateType.BirthCertificate :
                    result = ValidationCommon.IsBirthCertifiCateMatch (idInfo.IdNum);
                    if (!result)
                    {
                        MessageBox.Show("出生证编号格式不正确", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return false;
                    }
                    break;
            }
            if (idInfo.EffectiveDate == null  && idInfo.ExpiryDate==null )
                return true;
            if (idInfo.EffectiveDate != null  && (certificate == CertificateType.MacaoPermit || certificate == CertificateType.TaiwanPermit || certificate == CertificateType.CertificateOfOfficers))
            {
                try
                {
                    if (int.Parse(idInfo.EffectiveDate) < 1900)
                    {
                        MessageBox.Show("有效期限输入不正确.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return false;
                    }
                    if (int.Parse(idInfo.ExpiryDate) < 1900)
                    {
                        MessageBox.Show("有效期限输入不正确.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return false;
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("有效期限只能是数字.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 检查证据信息是否存在右边列表
        /// </summary>
        /// <param name="enumerable">数据源对象</param>
        /// <param name="newCardNo">新的证件号码</param>
        /// <returns></returns>
        private bool CheckIdCardNoInItemSource(ObservableCollection<IdInfoModel> enumerable, string newCardNo,CertificateType type)
        {
            //根据证件类型和证件号码来判断
            try
            {
                //无对象说明没有任何值
                if (enumerable == null || enumerable.Count == 0) return true;
                foreach (var item in enumerable)
                {
                    if (item.IdNum.Equals(newCardNo) && item.CertificateType==type)
                    {
                        MessageBox.Show("当前证件信息已经存在.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return false;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show("检查证件号是否存在发生异常：" + e.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return false;
        }

        /// <summary>
        ///买方或卖方信息添加
        /// </summary>
        /// <param name="certificateType"></param>
        /// <param name="userSysNo"></param>
        /// <param name="idInfos"></param>
        /// <param name="SelectUser"></param>
        /// <returns></returns>
        private bool IdInfosToSelectUser(CertificateType certificateType, IdInfoModel idInfos,UserIdentityModel SelectUser)
        {
            try
            {
                SelectUser.CertificateType = certificateType;
                SelectUser.Name = idInfos.IdName;
                if (idInfos.IdSex == null)
                {
                    if (idInfos.Male)
                    {
                        SelectUser.Gender = Gender.Male;                      
                    }
                    else if (idInfos.Female)
                    {
                        SelectUser.Gender = Gender.Female;
                    }
                    else
                    {
                        SelectUser.Gender = Gender.Unknown;
                    }     
                }
                else
                {
                    switch (idInfos.IdSex)
                        {
                            case "男":
                                SelectUser.Gender = Gender.Male;
                                break;
                            case "女":
                                SelectUser.Gender = Gender.Female;
                                break;
                            default:
                                SelectUser.Gender = Gender.Unknown;
                                break;
                        }
                }
                if (idInfos.IdNation != null)
                {
                    SelectUser.Nation = (Nation) Nation.Parse(typeof (Nation), idInfos.IdNation);
                }
                if (idInfos.IdBirth == null)
                    SelectUser.Birthday = null;
                else
                    SelectUser.Birthday = Convert.ToDateTime(idInfos.IdBirth);
                SelectUser.Address = idInfos.IdAddress;
                SelectUser.IdentityNo = idInfos.IdNum;
                SelectUser.VisaAgency = idInfos.IdAgent;

                if (idInfos.EffectiveDate != null)
                {
                    SelectUser.EffectiveDate = certificateType == CertificateType.IdCard ? Convert.ToDateTime(idInfos.EffectiveDate) : new DateTime(int.Parse(idInfos.EffectiveDate), 1, 1);                  
                }
                else
                    SelectUser.EffectiveDate = null;
                if (idInfos.EffectiveDate != null && idInfos.ExpiryDate != "长期")
                {
                    SelectUser.ExpiryDate = certificateType == CertificateType.IdCard ? Convert.ToDateTime(idInfos.ExpiryDate) : new DateTime(int.Parse(idInfos.ExpiryDate), 1, 1);
                }
                else
                {
                    SelectUser.ExpiryDate = null;
                }
                SelectUser.Nationality = idInfos.Nationality;
                if (idInfos.Bitmap != null)
                {              
                    SelectUser.Photo = Bitmapimagetobytearray(idInfos.Bitmap);   
                }                             
                SelectUser.IsTrusted = idInfos.IsTrusted;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,"系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
        }
        #region bitmapimage转化为type数组
        public  byte[] Bitmapimagetobytearray(BitmapImage bmp)
        {
            byte[] bytearray = null;

            try
            {
                Stream smarket = bmp.StreamSource;
                if (smarket != null && smarket.Length > 0)
                {
                    //很重要，因为position经常位于stream的末尾，导致下面读取到的长度为0。
                    smarket.Position = 0;

                    using (BinaryReader br = new BinaryReader(smarket))
                    {
                        bytearray = br.ReadBytes((int)smarket.Length);
                    }
                }
            }
            catch
            {
                MessageBox.Show("头像转化失败", "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return bytearray;
        }
        #endregion
        /// <summary>
        /// 添加买方或卖方数据
        /// </summary>
        /// <returns></returns>
        private OperationResult AddUser(CertificateType certificateType, IdInfoModel idInfos,
            ObservableCollection<UserIdentityModel> userIdentityModel, CustomerType customerType)
        {
            var user = new UserIdentityModel();
           var convertResult= IdInfosToSelectUser(certificateType, idInfos, user);
            if (!convertResult) return null;
            var result = SaveUserIdentity(user,customerType);
            if (result != null && result.Success)
            {
                user.Tag = result.OtherData["UserSysNo"];
                userIdentityModel.Add(user);
                return result;
            }
            else
            {
                if (result != null)
                    MessageBox.Show(result.ResultMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return null ;
            }
        }

        /// <summary>
        /// 保存买卖家信息
        /// </summary>     
        /// <param name="customerType">类型</param>        
        private OperationResult SaveUserIdentity(UserIdentityModel userIdentity, CustomerType customerType)
        {
            OperationResult result = null;
            try
            {
                switch (customerType)
                {
                    case CustomerType.Buyer:
                       result = CustomerServiceProxy.CreateBuyer(userIdentity, LoginHelper.CurrentUser.SysNo);
                        break;
                    case CustomerType.Seller:
                        result = CustomerServiceProxy.CreateSeller(userIdentity, LoginHelper.CurrentUser.SysNo);
                        break;
                }
                return result;

            }
            catch (Exception ex)
            {
                result.ResultMessage = ex.Message;
                return result;
            }
        }


        #endregion

        private bool CheckDataValid()
        {
            #region 信息验证

            if (string.IsNullOrEmpty(TbTenementContractWord) ||
                string.IsNullOrEmpty(TbTenementContract))
            {
                MessageBox.Show("请填写产权证号", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (string.IsNullOrEmpty(TbTenementContract))
            {
                MessageBox.Show("请填写产权证号", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            if (string.IsNullOrEmpty(TbTenementAddress))
            {
                MessageBox.Show("请填写房屋坐落", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            if (AgentStaff.SysNo == 0 && agentCompany.SysNo == 0)
            {
                MessageBox.Show("请选择经纪人", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            //买方
            if (BuyerInfoList.Count == 0)
            {
                MessageBox.Show("至少有一个买方.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            //卖家
            if (SellerInfoList.Count == 0)
            {
                MessageBox.Show("至少有一个卖方.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            #endregion

            return true;
        }


        /// <summary>
        /// 保存案件
        /// </summary>
        /// <returns></returns>
        private OperationResult SaveCase()
        {
            //买方
            var listBuyersUserSysNo = new List<long>();
            if (ListBuyerModel == null || ListBuyerModel.Count==0)
            {
                MessageBox.Show("至少有一个买方.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return null;
            }
            BuyerTotal = listBuyerModel.Count;
            foreach (var item in listBuyerModel)
            {
                var user = item as UserIdentityModel;
                if (user == null) continue;
                long userSysNo;
                long.TryParse(Convert.ToString(user.Tag), out userSysNo);
                listBuyersUserSysNo.Add(userSysNo);
            }

            //卖家
            var listSellersUserSysNo = new List<long>();
            if (ListSellerModel == null || ListSellerModel.Count==0)
            {
                MessageBox.Show("至少有一个卖方.", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return null;
            }
            SellerTotal = ListSellerModel.Count;
            foreach (var item in ListSellerModel)
            {
                var user = item as UserIdentityModel;
                if (user == null) continue;
                long userSysNo;
                long.TryParse(Convert.ToString(user.Tag), out userSysNo);
                listSellersUserSysNo.Add(userSysNo);
            }

            #region 信息验证

            if (string.IsNullOrEmpty(TbTenementContractWord) ||
                string.IsNullOrEmpty(TbTenementContract))
            {
                MessageBox.Show("请填写产权证号", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return null;
            }
            if (string.IsNullOrEmpty(TbTenementContract))
            {
                MessageBox.Show("请填写产权证号", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return null;
            }

            if (string.IsNullOrEmpty(TbTenementAddress))
            {
                MessageBox.Show("请填写房屋坐落", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return null;
            }

            if (AgentStaff.SysNo == 0 && agentCompany.SysNo == 0)
            {
                MessageBox.Show("请选择经纪人", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return null;
            }

            #endregion

            var caseDto = new CaseDto()
            {
                Buyers = listBuyersUserSysNo,
                Sellers = listSellersUserSysNo,
                TenementContract = TbTenementContractWord.Trim() + "-" + TbTenementContract.Trim(), //产权证号
                TenementAddress = TbTenementAddress.Trim(), //房屋座落
                OperatorSysNo = LoginHelper.CurrentUser.SysNo, //受理人
                AgencySysNo = AgentStaff.SysNo,
                CompanySysNo = agentCompany.SysNo,
                CreateUserSysNo = LoginHelper.CurrentUser.SysNo,
                SourceType = (SourceType)Enum.Parse(typeof(SourceType), Convert.ToString(SelectCaseType))
            };
            OperationResult result = AddNewCase(caseDto);

            if (result.Success)
            {
                string sMessage = "添加案件成功\n案件编号：" + result.ResultMessage; //案件编号
                MessageBox.Show(sMessage, "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                 //跳转列表页面
                eventAggregator.GetEvent<NavigateToMainListEvents>().Publish(this);
            }
            else
            {
                MessageBox.Show("添加案件失败：" + result.ResultMessage, "系统提示", MessageBoxButton.OK,
                    MessageBoxImage.Information);
            }
            return result;
        }
    
        /// <summary>
        /// 增加新的案件
        /// </summary>
        /// <param name="caseDto"></param>
        /// <returns></returns>
        private OperationResult AddNewCase(CaseDto caseDto)
        {
            OperationResult result = null;
            try
            {
                caseDto.ReceptionCenter = ConfigHelper.GetCurrentReceptionCenter();
                result = CaseProxyService.GetInstanse().GenerateCase(caseDto);
            }
            catch (Exception ex)
            {
                result.ResultMessage = ex.Message;
            }
            return result;
        }
    }
        
}      
   

