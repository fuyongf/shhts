﻿using Microsoft.Practices.Prism.PubSubEvents;

using PinganHouse.SHHTS.Enumerations;
using PinganHouse.SHHTS.UI.WPFNew.Common;

using System;
using System.Collections.ObjectModel;
using System.Windows.Media.Imaging;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels
{
    public class GaoPaiPhotosViewModel:BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;

        #region Ctor
        public GaoPaiPhotosViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;
            Photos = new ObservableCollection<PhotosViewModel>();
        }

        
        
        #endregion

        /// <summary>
        /// 案件编号
        /// </summary>
        private string id;
        public string Id
        {
            get
            {
                return id;
            }
            set
            {
                if (id != value)
                {
                    id = value;
                    NotifyPropertyChanged("Id");
                }
            }
        }
        /// <summary>
        /// 附件类型
        /// </summary>
        private AttachmentType type;
        public AttachmentType Type
        {
            get
            {
                return type;
            }
            set
            {
                if (type != value)
                {
                    type = value;
                    NotifyPropertyChanged("Type");
                }
            }
        }
        
        private ObservableCollection<PhotosViewModel> photos;
        public ObservableCollection<PhotosViewModel> Photos
        {
            get
            {
                return photos;
            }
            set
            {
                if (photos != value)
                {
                    photos = value;
                    NotifyPropertyChanged("Photos");
                }
            }
        }


        private bool isUploadFinished;
        public bool IsUploadFinished
        {
            get
            {
                return isUploadFinished;
            }
            set
            {
                if (isUploadFinished != value)
                {
                    isUploadFinished = value;
                    NotifyPropertyChanged("IsUploadFinished");
                }
                if (isUploadFinished)
                {
                    this.eventAggregator.GetEvent<GaoPaiUploadAttachmentEvents>().Publish(this);
                }
            }
        }

    }

    public class PhotosViewModel:BaseViewModel
    {
        private string tag;
        public string Tag
        {
            get
            {
                return tag;
            }
            set
            {
                if (tag != value)
                {
                    tag = value;
                    NotifyPropertyChanged("Tag");
                }
            }
        }

        private BitmapImage imageSource;
        public BitmapImage ImageSource
        {
            get
            {
                return imageSource;
            }
            set
            {
                if (imageSource != value)
                {
                    imageSource = value;
                    NotifyPropertyChanged("ImageSource");
                }
            }
        }
        private int index;
        public int Index
        {
            get
            {
                return index;
            }
            set
            {
                if (index != value)
                {
                    index = value;
                    NotifyPropertyChanged("Index");
                }
            }
        }
        private string imageName;
        public string ImageName
        {
            get
            {
                return imageName;
            }
            set
            {
                if (imageName != value)
                {
                    imageName = value;
                    NotifyPropertyChanged("ImageName");
                }
            }
        }
        private bool isChecked;
        public bool IsChecked
        {
            get
            {
                return isChecked;
            }
            set
            {
                if (isChecked != value)
                {
                    isChecked = value;
                    NotifyPropertyChanged("IsChecked");
                }
            }
        }
    }
}
