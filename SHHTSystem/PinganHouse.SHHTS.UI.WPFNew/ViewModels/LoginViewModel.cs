﻿using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.PubSubEvents;

using PinganHouse.SHHTS.UI.WPFNew.Common;
using PinganHouse.SHHTS.UI.WPFNew.Common.CommonClass;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {

        private readonly IEventAggregator eventAggregator;

        public LoginViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.eventAggregator = eventAggregator;
            this.LoginCommand = new DelegateCommand<LoginViewModel>(this.OnLogin);
            AreaList = new ObservableCollection<EnumHelper>();
        }

        private bool isRememberFlag;
        public bool IsRememberFlag
        {
            get
            {
                return isRememberFlag;
            }
            set
            {
                if (isRememberFlag != value)
                {
                    isRememberFlag = value;
                    NotifyPropertyChanged("IsRememberFlag");
                }
            }
        }

        private string userName;

        public string UserName
        {
            get
            {
                return userName;
            }
            set
            {
                if (userName != value)
                {
                    userName = value;
                    NotifyPropertyChanged("UserName");
                }
            }
        }

        private string password;

        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                if (password != value)
                {
                    password = value;
                    NotifyPropertyChanged("Password");
                }
            }
        }

        private ObservableCollection<EnumHelper> areaList;
        public ObservableCollection<EnumHelper> AreaList
        {
            get
            {
                return areaList;
            }
            set
            {
                if (areaList != value)
                {
                    areaList = value;
                    NotifyPropertyChanged("AreaList");
                }
            }
        }
        private int currentIndex = -1;
        public int CurrentIndex
        {
            get
            {
                return currentIndex;
            }
            set
            {
                if (currentIndex != value)
                {
                    currentIndex = value;
                    NotifyPropertyChanged("CurrentIndex");
                }
                SetAreaConfigFile(CurrentIndex);
            }
        }

        private List<EnumHelper> m_Network;
        public List<EnumHelper> Network
        {
            get
            {
                if (m_Network == null)
                    m_Network = EnumHelper.InitNetworkOptionToCombobox();
                return m_Network;
            }
            set
            {
                m_Network = value;
                NotifyPropertyChanged("Network");
            }
        }

        private int m_CurrentNetwork;
        public int CurrentNetwork
        {
            get
            {
                return m_CurrentNetwork;
            }
            set
            {
                m_CurrentNetwork = value;
                NotifyPropertyChanged("CurrentNetwork");
            }
        }

        private bool isOk;
        public bool IsOk
        {
            get
            {
                return isOk;
            }
            set
            {
                if (isOk != value)
                {
                    isOk = value;
                    NotifyPropertyChanged("IsOk");
                }
                if (IsOk)
                {
                    this.eventAggregator.GetEvent<LoginEvents>().Publish(this);
                }
            }
        }

        public DelegateCommand<LoginViewModel> LoginCommand { get; private set; }

        private void OnLogin(LoginViewModel model)
        {
            if (string.IsNullOrEmpty(UserName))
            {
                MessageBox.Show("请输入用户名!", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            if (string.IsNullOrEmpty(Password))
            {
                MessageBox.Show("请输入用密码!", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            if (CurrentIndex == -1)
            {
                MessageBox.Show("请选择区域!", "系统提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            this.eventAggregator.GetEvent<LoginEvents>().Publish(this);
        }

        private bool SetAreaConfigFile(int area)
        {
            bool result = false;
            try
            {
                //从注册表写入信息
                PrivateProfileUtils.Write("Area", "SH", area.ToString());
                result = true;
                //固化区域选择
                ConfigHelper.AreaIndex = area;
            }
            catch (Exception)
            {

            }
            return result;
        }


    }
}
