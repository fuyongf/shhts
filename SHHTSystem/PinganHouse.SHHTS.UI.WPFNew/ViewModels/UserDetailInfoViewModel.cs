﻿using PinganHouse.SHHTS.Enumerations;

using System;

namespace PinganHouse.SHHTS.UI.WPFNew.ViewModels
{
    public class UserDetailInfoViewModel:BaseViewModel
    {
        /// <summary>
        /// 证件类型
        /// </summary>
        private CertificateType certificateType;
        public CertificateType CertificateType
        {
            get
            {
                return certificateType;
            }
            set
            {
                if (certificateType != value)
                {
                    certificateType = value;
                    NotifyPropertyChanged("CertificateType");
                }
            }
        }

        /// <summary>
        /// 国籍
        /// </summary>
        private string nationality;
        public string Nationality
        {
            get
            {
                return nationality;
            }
            set
            {
                if (nationality != value)
                {
                    nationality = value;
                    NotifyPropertyChanged("Nationality");
                }
            }
        }

        /// <summary>
        /// 证件号码
        /// </summary>
        private string identityNo;
        public string IdentityNo
        {
            get
            {
                return identityNo;
            }
            set
            {
                if (identityNo != value)
                {
                    identityNo = value;
                    NotifyPropertyChanged("IdentityNo");
                }
            }
        }

        /// <summary>
        /// 姓名
        /// </summary>
        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (name != value)
                {
                    name = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }

        /// <summary>
        /// 性别
        /// </summary>
        private Gender gender;
        public Gender Gender
        {
            get
            {
                return gender;
            }
            set
            {
                if (gender != value)
                {
                    gender = value;
                    NotifyPropertyChanged("Gender");
                }
            }
        }

        /// <summary>
        /// 出生年月
        /// </summary>
        private DateTime? birthday;
        public DateTime? Birthday
        {
            get
            {
                return birthday;
            }
            set
            {
                if (birthday != value)
                {
                    birthday = value;
                    NotifyPropertyChanged("Birthday");
                }
            }
        }

        /// <summary>
        /// 住址
        /// </summary>
        private string address;
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                if (address != value)
                {
                    address = value;
                    NotifyPropertyChanged("Address");
                }
            }
        }

        /// <summary>
        /// 签证机构
        /// </summary>
        private string visaAgency;
        public string VisaAgency
        {
            get
            {
                return visaAgency;
            }
            set
            {
                if (visaAgency != value)
                {
                    visaAgency = value;
                    NotifyPropertyChanged("VisaAgency");
                }
            }
        }
        /// <summary>
        /// 民族
        /// </summary>
        private Nation nation;
        public Nation Nation
        {
            get
            {
                return nation;
            }
            set
            {
                if (nation != value)
                {
                    nation = value;
                    NotifyPropertyChanged("Nation");
                }
            }
        }

        /// <summary>
        /// 有效日期(开始)
        /// </summary>
        private DateTime? effectiveDate;
        public DateTime? EffectiveDate
        {
            get
            {
                return effectiveDate;
            }
            set
            {
                if (effectiveDate != value)
                {
                    effectiveDate = value;
                    NotifyPropertyChanged("EffectiveDate");
                }
            }
        }

        /// <summary>
        /// 失效日期
        /// </summary>
        private DateTime? expiryDate;
        public DateTime? ExpiryDate
        {
            get
            {
                return expiryDate;
            }
            set
            {
                if (expiryDate != value)
                {
                    expiryDate = value;
                    NotifyPropertyChanged("ExpiryDate");
                }
            }
        }

        /// <summary>
        /// 证件照
        /// </summary>
        private byte[] photo;
        public byte[] Photo
        {
            get
            {
                return photo;
            }
            set
            {
                if (photo != value)
                {
                    photo = value;
                    NotifyPropertyChanged("Photo");
                }
            }
        }

        /// <summary>
        /// 可信的
        /// </summary>
        private bool isTrusted;
        public bool IsTrusted
        {
            get
            {
                return isTrusted;
            }
            set
            {
                if (isTrusted != value)
                {
                    isTrusted = value;
                    NotifyPropertyChanged("IsTrusted");
                }
            }
        }

        ///// <summary>
        ///// 系统对象其它属性
        ///// </summary>
        //public object Tag { get; set; }
    }
}
