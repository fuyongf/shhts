declare @province table
(
	[ProvinceName] [varchar](50) NULL
)
declare @city table
(
	[CityName] [varchar](50) NULL,
	[ProvinceName] [varchar](50) NULL
)
declare @district table
(
	[DistrictName] [varchar](50) NULL,
	[CityName] [varchar](50) NULL,
	[ProvinceName] [varchar](50) NULL
)

use regionconfig_tempdb
insert into @province(ProvinceName) select ProvinceName from dbo.S_Province;
insert into @city(CityName,ProvinceName) select c.CityName,p.ProvinceName from dbo.S_City c left join dbo.S_Province p on c.ProvinceID=p.ProvinceID;
insert into @district(DistrictName,CityName,ProvinceName) select d.DistrictName,c.CityName,p.ProvinceName from dbo.S_District d left join dbo.S_City c on c.CityID=d.CityID left join dbo.S_Province p on p.ProvinceID=c.ProvinceID;

--插入数据
use [SHHTS]
declare @ParentPath varchar(150)
declare @ProvinceName varchar(50)
declare @CityName varchar(50)
declare @DistrictName varchar(1000)
declare @ParentID nvarchar(32)
declare @ID nvarchar(32)
declare @Path varchar(200)
declare @Pre varchar(30)
declare @Gap varchar
set @Gap='/'
set @Pre='/Biz/Static/RegionConfig'
--插入省一级
declare province_cursor cursor for select * from @province
open province_cursor                       --打开游标
fetch next from province_cursor into @ProvinceName  
while @@fetch_status=0          
begin
	set @Path=@Pre+@Gap+@ProvinceName
	set @ID= substring(sys.fn_sqlvarbasetostr(HashBytes('MD5',@Path)),3,32)
	set @ParentID='e4bb8a0d058a5efdb112a7a1b1c86698'
	--select @Pre,@ProvinceName,@Path,@ID
	insert into Setting(Id,Name,ParentID,Path,Description) values(@ID,@ProvinceName,@ParentID,@Path,@ProvinceName)
	fetch next from province_cursor into @ProvinceName   
end
close province_cursor
deallocate province_cursor

--插入市一级
declare city_cursor cursor for select * from @city
open city_cursor                       --打开游标
fetch next from city_cursor into @CityName,@ProvinceName
while @@fetch_status=0          
begin
    set @ParentPath=@Pre+@Gap+@ProvinceName
	set @Path=@ParentPath+@Gap+@CityName
	set @ID= substring(sys.fn_sqlvarbasetostr(HashBytes('MD5',@Path)),3,32)
	set @ParentID=substring(sys.fn_sqlvarbasetostr(HashBytes('MD5',@ParentPath)),3,32)
	--select @Pre as Pre,@ProvinceName as ProvinceName,@CityName as CityName,@ParentPath as ParentPath,@Path as Path
	insert into Setting(Id,Name,ParentID,Path,Description) values(@ID,@CityName,@ParentID,@Path,@CityName)
	fetch next from city_cursor into @CityName,@ProvinceName  
end
close city_cursor
deallocate city_cursor

--插入县一级
declare district_cursor cursor for select * from @district
open district_cursor                       --打开游标
fetch next from district_cursor into @DistrictName,@CityName,@ProvinceName
while @@fetch_status=0          
begin
    set @ParentPath=@Pre+@Gap+@ProvinceName+@Gap+@CityName
	set @Path=@ParentPath+@Gap+@DistrictName
	set @ID= substring(sys.fn_sqlvarbasetostr(HashBytes('MD5',@Path)),3,32)
	set @ParentID=substring(sys.fn_sqlvarbasetostr(HashBytes('MD5',@ParentPath)),3,32)
	insert into Setting(Id,Name,ParentID,Path,Description) values(@ID,@DistrictName,@ParentID,@Path,@DistrictName)
	fetch next from district_cursor into @DistrictName,@CityName,@ProvinceName
end
close district_cursor
deallocate district_cursor




